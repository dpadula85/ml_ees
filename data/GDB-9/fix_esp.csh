#!/bin/tcsh

set list = `cat molslist_full.txt`

foreach i ( $list )
    sed -i "s/\*\^/E/" ./mols/$i
end
