%nproc=16
%mem=2GB
%chk=mol_288_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.39200       -0.60910        0.00020
C         -0.42180        0.66720        0.13800
O         -1.67040        0.42740        0.64940
O          1.63390       -0.36490       -0.50910
H         -0.13670       -1.29170       -0.72520
H          0.37340       -1.11970        0.97860
H         -0.51640        1.19460       -0.83580
H          0.12360        1.30820        0.85500
H         -1.94580       -0.50400        0.68950
H          2.16830        0.29190       -0.02740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

