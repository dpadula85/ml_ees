%nproc=16
%mem=2GB
%chk=mol_288_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.59880       -0.40700       -0.09700
C         -0.90270       -0.25170       -0.28220
O         -1.39650        0.71790        0.55750
O          1.18120        0.82040       -0.39350
H          1.01890       -1.14660       -0.78320
H          0.78580       -0.71720        0.96070
H         -1.38240       -1.20830       -0.02050
H         -1.06790        0.04820       -1.33760
H         -0.68550        1.03100        1.14210
H          1.85050        1.11340        0.25360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

