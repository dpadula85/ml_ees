%nproc=16
%mem=2GB
%chk=mol_288_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.68850       -0.44350        0.13470
C          0.61520        0.21810       -0.32860
O          1.72230       -0.48930        0.03620
O         -1.76290        0.34300       -0.27910
H         -0.66990       -0.60810        1.22100
H         -0.78430       -1.45050       -0.34960
H          0.60550        0.33220       -1.41740
H          0.65450        1.21980        0.15240
H          1.83940       -0.42360        1.00590
H         -1.53140        1.30190       -0.17560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

