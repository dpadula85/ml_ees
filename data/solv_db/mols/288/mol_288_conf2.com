%nproc=16
%mem=2GB
%chk=mol_288_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.54880        0.16660       -0.56530
C         -0.69740       -0.52090       -0.06560
O         -1.78500        0.21910       -0.52990
O          1.70880       -0.45770       -0.18090
H          0.49960        0.20690       -1.67000
H          0.55060        1.19870       -0.18520
H         -0.78530       -1.52870       -0.55050
H         -0.70850       -0.68750        1.01320
H         -1.75740        1.15830       -0.23530
H          2.42570        0.24520       -0.16460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

