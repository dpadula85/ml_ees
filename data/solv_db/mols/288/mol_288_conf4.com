%nproc=16
%mem=2GB
%chk=mol_288_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.73460       -0.20350        0.41290
C          0.64420       -0.42830       -0.13200
O          1.25920        0.74150       -0.54870
O         -1.59410        0.36600       -0.51630
H         -0.74620        0.42360        1.32220
H         -1.13840       -1.19350        0.71300
H          1.21930       -1.00290        0.60390
H          0.52280       -1.06240       -1.05370
H          1.75550        1.20830        0.15540
H         -1.18780        1.15110       -0.95680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_288_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

