%nproc=16
%mem=2GB
%chk=mol_515_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.83150        1.27150        0.18790
C         -1.07170        0.01700       -0.17340
C         -1.66060       -1.10390        0.65190
C         -1.41170       -0.29120       -1.63310
C          0.39310        0.10060        0.04080
C          1.08890       -1.09600        0.16720
C          2.47840       -1.15220        0.18780
C          3.17580        0.04020        0.05630
C          2.50920        1.23630       -0.04910
C          1.09220        1.27690       -0.05950
H         -2.84950        1.17510       -0.26560
H         -1.99650        1.37230        1.29930
H         -1.44220        2.21460       -0.20270
H         -2.77460       -1.02640        0.68800
H         -1.19960       -1.17820        1.64420
H         -1.45910       -2.04850        0.09580
H         -0.48290       -0.44970       -2.20390
H         -1.96880        0.54150       -2.09910
H         -2.01000       -1.21230       -1.73710
H          0.55010       -2.04910        0.25550
H          3.05530       -2.00570        0.48640
H          4.27680        0.03880       -0.05380
H          3.00840        2.13700       -0.26310
H          0.53060        2.19150       -0.08650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

