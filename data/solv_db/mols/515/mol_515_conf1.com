%nproc=16
%mem=2GB
%chk=mol_515_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.98800        0.37710       -1.17700
C         -1.15380       -0.08030       -0.05670
C         -1.38030        0.78970        1.20350
C         -1.47180       -1.49250        0.41100
C          0.32120       -0.01570       -0.38900
C          0.94680        1.22560       -0.33990
C          2.32740        1.36230       -0.13840
C          3.08610        0.21010        0.00660
C          2.47340       -1.01820       -0.10040
C          1.11270       -1.13800       -0.28630
H         -2.56480        1.28340       -0.90660
H         -1.40860        0.64800       -2.08550
H         -2.73860       -0.40540       -1.50180
H         -2.37830        1.12830        1.33750
H         -0.90910        0.24800        2.06860
H         -0.73350        1.70680        1.01090
H         -2.41100       -1.58770        0.93510
H         -1.43120       -2.17080       -0.48630
H         -0.65200       -1.77700        1.12130
H          0.34070        2.08790       -0.59010
H          2.75660        2.35860       -0.05400
H          4.09550        0.28800        0.48430
H          3.10290       -1.89320       -0.04250
H          0.65780       -2.13520       -0.42430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

