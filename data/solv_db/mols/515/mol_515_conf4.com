%nproc=16
%mem=2GB
%chk=mol_515_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.42360        0.74340        1.58840
C         -1.09910        0.01770        0.28490
C         -1.47320        0.96550       -0.86420
C         -1.94110       -1.18120        0.20940
C          0.35550       -0.17070        0.24530
C          1.13050        0.95240        0.45300
C          2.51290        0.90290        0.36300
C          3.14320       -0.27480        0.03140
C          2.37610       -1.32590       -0.27240
C          1.01530       -1.30460       -0.12710
H         -2.53320        0.72490        1.69470
H         -0.99870        0.13720        2.40100
H         -1.12530        1.79520        1.58590
H         -0.67030        1.73440       -1.00440
H         -2.45270        1.44010       -0.70450
H         -1.46520        0.34810       -1.78280
H         -2.63450       -1.13750       -0.68360
H         -1.45350       -2.15270        0.15050
H         -2.65930       -1.27110        1.08080
H          0.71310        1.92220        0.70960
H          3.14160        1.78070        0.56100
H          4.23440       -0.30270        0.00480
H          2.86160       -2.19380       -0.78750
H          0.44540       -2.14940       -0.49270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

