%nproc=16
%mem=2GB
%chk=mol_515_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43230        1.59490       -0.20860
C         -1.08380        0.13530        0.02550
C         -1.40490       -0.11980        1.51310
C         -1.95640       -0.68800       -0.85970
C          0.35930       -0.09990       -0.10790
C          0.88400       -1.36600        0.04190
C          2.24080       -1.58850        0.14440
C          3.09700       -0.53010        0.09800
C          2.57900        0.73770       -0.05550
C          1.22380        0.96260       -0.16320
H         -2.55430        1.68850       -0.29060
H         -1.00490        1.96240       -1.15450
H         -1.15420        2.22610        0.67420
H         -2.41330        0.20340        1.75210
H         -0.59970        0.44880        2.06860
H         -1.18670       -1.19000        1.70780
H         -2.10060       -0.13890       -1.81230
H         -1.65430       -1.73100       -0.96480
H         -2.97510       -0.71120       -0.39000
H          0.21310       -2.21840        0.07630
H          2.62230       -2.57900        0.25300
H          4.18120       -0.59670        0.19180
H          3.25170        1.60880       -0.07060
H          0.86830        1.98900       -0.32860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

