%nproc=16
%mem=2GB
%chk=mol_515_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.63330        0.25550        1.27670
C         -1.09400       -0.05700       -0.10990
C         -1.75830        0.89720       -1.06370
C         -1.50170       -1.47080       -0.51930
C          0.37660        0.05630       -0.06230
C          1.00380        1.23220        0.25630
C          2.39240        1.30540        0.42040
C          3.13450        0.17070        0.25850
C          2.52650       -1.02360       -0.06550
C          1.14790       -1.07220       -0.22460
H         -0.82810       -0.04800        2.00250
H         -1.79500        1.33680        1.42460
H         -2.50450       -0.35170        1.54970
H         -1.95850        0.37710       -2.05020
H         -2.74540        1.20460       -0.64470
H         -1.13420        1.77040       -1.32460
H         -1.10930       -1.61030       -1.54010
H         -1.19310       -2.21750        0.21550
H         -2.62600       -1.45060       -0.58670
H          0.40350        2.12660        0.37430
H          2.83270        2.26150        0.66630
H          4.21990        0.21220        0.40380
H          3.13380       -1.89830       -0.18700
H          0.71000       -2.00660       -0.46980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_515_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

