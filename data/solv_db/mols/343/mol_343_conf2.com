%nproc=16
%mem=2GB
%chk=mol_343_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26940        0.31520       -0.31140
C         -0.99730        0.10420        0.51810
C          0.03330       -0.46290       -0.40350
C          1.35050       -0.72900        0.26340
C          1.98280        0.49060        0.84970
H         -2.01020        0.67950       -1.33380
H         -2.79110       -0.66190       -0.34170
H         -2.90740        1.11290        0.13320
H         -0.70340        1.08000        0.90820
H         -1.27150       -0.54070        1.35860
H         -0.35270       -1.44420       -0.77180
H          0.11940        0.21460       -1.27850
H          2.04280       -1.11230       -0.53050
H          1.20250       -1.52070        1.00560
H          3.10680        0.39240        0.86350
H          1.70290        0.66380        1.91760
H          1.76180        1.41830        0.29080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

