%nproc=16
%mem=2GB
%chk=mol_343_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.31320       -0.30590       -0.33220
C          0.89640        0.20410       -0.55420
C          0.14270        0.01730        0.71780
C         -1.28170        0.47450        0.66330
C         -2.11430       -0.21860       -0.35610
H          3.04720        0.16780       -0.99160
H          2.32940       -1.40040       -0.49440
H          2.64830       -0.12060        0.71990
H          0.41840       -0.42760       -1.34030
H          0.90860        1.22890       -0.93230
H          0.25470       -1.03870        1.03620
H          0.65300        0.62340        1.50430
H         -1.71150        0.33300        1.68370
H         -1.27490        1.57340        0.49430
H         -1.86620       -1.26380       -0.53370
H         -2.18770        0.36630       -1.31860
H         -3.17550       -0.21320        0.03370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

