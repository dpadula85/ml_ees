%nproc=16
%mem=2GB
%chk=mol_343_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04240        0.04460       -0.30150
C          1.30890       -0.23560        0.99110
C         -0.13490       -0.55910        0.75090
C         -0.87420        0.57100        0.06590
C         -2.31760        0.16660       -0.13920
H          1.59580        0.84290       -0.89690
H          2.22680       -0.89490       -0.86590
H          3.06220        0.42020        0.00370
H          1.46190        0.58780        1.71460
H          1.78080       -1.13750        1.43250
H         -0.63620       -0.71750        1.72590
H         -0.17350       -1.47390        0.12310
H         -0.79210        1.48200        0.69740
H         -0.44480        0.82380       -0.92380
H         -2.99380        0.74140        0.54100
H         -2.64890        0.25260       -1.18440
H         -2.46280       -0.91460        0.14600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

