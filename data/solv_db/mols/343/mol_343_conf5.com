%nproc=16
%mem=2GB
%chk=mol_343_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11040        0.23210        0.26690
C         -1.24430       -0.55730       -0.65100
C          0.16750       -0.08260       -0.73100
C          0.87500       -0.12280        0.60900
C          2.30230        0.38800        0.37460
H         -1.66470        0.42880        1.25430
H         -2.50580        1.15730       -0.21210
H         -3.02830       -0.39340        0.47400
H         -1.22790       -1.61030       -0.29300
H         -1.67270       -0.55400       -1.66030
H          0.27890        0.91510       -1.19230
H          0.72260       -0.78910       -1.39360
H          0.85560       -1.11770        1.04120
H          0.36690        0.60960        1.26350
H          2.88620       -0.42410       -0.13700
H          2.23100        1.23990       -0.34330
H          2.76810        0.68050        1.33030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

