%nproc=16
%mem=2GB
%chk=mol_343_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30070       -0.08640        0.35780
C          0.88990        0.49560        0.20810
C          0.10830       -0.48940       -0.59950
C         -1.31700       -0.05630       -0.83650
C         -2.08870        0.13220        0.44160
H          2.24590       -0.97650        1.00830
H          2.68030       -0.37430       -0.65580
H          2.96420        0.65530        0.84320
H          0.49930        0.65950        1.21810
H          0.98790        1.42150       -0.38160
H          0.59020       -0.55960       -1.60180
H          0.17810       -1.47090       -0.10940
H         -1.84960       -0.75360       -1.51320
H         -1.27730        0.91870       -1.39030
H         -2.96650       -0.56680        0.49810
H         -1.46360       -0.12090        1.34390
H         -2.48200        1.17190        0.54360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_343_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

