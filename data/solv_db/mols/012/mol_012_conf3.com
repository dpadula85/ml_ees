%nproc=16
%mem=2GB
%chk=mol_012_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.02290        0.54740        0.21830
S          1.97700       -0.89870       -0.02440
C          0.25350       -0.47290       -0.13740
C         -0.64510       -1.50230       -0.32410
C         -2.01260       -1.27840       -0.42550
C         -2.50390        0.01500       -0.33830
C         -1.60000        1.02780       -0.15290
C         -0.22700        0.81030       -0.05020
H          2.58830        1.27270        0.94760
H          3.17240        1.11620       -0.72550
H          4.03120        0.20470        0.53600
H         -0.30310       -2.53170       -0.39800
H         -2.67000       -2.12540       -0.57160
H         -3.57250        0.16550       -0.42010
H         -1.99280        2.03830       -0.08560
H          0.48190        1.61130        0.09590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

