%nproc=16
%mem=2GB
%chk=mol_012_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.97240       -0.09500       -0.32250
S          1.86320       -0.00900        1.12920
C          0.19710       -0.00030        0.51280
C         -0.39950        1.20680        0.23050
C         -1.69440        1.26850       -0.25160
C         -2.37470        0.07780       -0.44240
C         -1.79280       -1.14120       -0.16490
C         -0.50010       -1.16960        0.31470
H          2.93860       -1.12930       -0.72650
H          4.01750        0.15920       -0.04920
H          2.54220        0.63000       -1.06470
H          0.17720        2.12770        0.39690
H         -2.15060        2.22610       -0.46930
H         -3.39990        0.08100       -0.82190
H         -2.33700       -2.08320       -0.31800
H         -0.05910       -2.14950        0.52810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

