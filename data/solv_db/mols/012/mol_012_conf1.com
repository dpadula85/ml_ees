%nproc=16
%mem=2GB
%chk=mol_012_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.00940       -0.29480        0.29560
S          1.94110        1.06730       -0.26410
C          0.22410        0.55500       -0.18460
C         -0.35780       -0.04330       -1.26930
C         -1.67610       -0.44340       -1.22610
C         -2.43860       -0.24240       -0.06950
C         -1.86520        0.35880        1.03000
C         -0.54480        0.74360        0.94510
H          2.78050       -0.59390        1.33810
H          2.86320       -1.21120       -0.32730
H          4.05390        0.05100        0.24400
H          0.20530       -0.20930       -2.16990
H         -2.15400       -0.91380       -2.06230
H         -3.47560       -0.54870       -0.01370
H         -2.46900        0.51340        1.93400
H         -0.09620        1.21180        1.80000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

