%nproc=16
%mem=2GB
%chk=mol_012_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.05880        0.18350        0.26320
S          1.90470       -0.78030       -0.73100
C          0.21850       -0.37150       -0.42160
C         -0.47740       -1.05110        0.56180
C         -1.80010       -0.78590        0.85880
C         -2.47460        0.18090        0.17090
C         -1.79260        0.88200       -0.82830
C         -0.47310        0.61490       -1.12220
H          4.08560       -0.17800        0.01230
H          2.89510        0.02640        1.34450
H          3.01610        1.24430       -0.04250
H          0.04700       -1.82790        1.12240
H         -2.34010       -1.33420        1.64330
H         -3.51300        0.40130        0.39160
H         -2.38150        1.63460       -1.33650
H          0.02650        1.16100       -1.88680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

