%nproc=16
%mem=2GB
%chk=mol_012_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.94940       -0.09560       -0.34990
S         -1.80760        1.27140       -0.08740
C         -0.15560        0.56550        0.13470
C          0.87560        1.41720        0.34930
C          2.15190        0.93230        0.52460
C          2.38220       -0.42630        0.48160
C          1.31580       -1.28590        0.26100
C          0.04110       -0.78980        0.08660
H         -3.85820        0.28720       -0.89190
H         -3.24690       -0.51590        0.65390
H         -2.54530       -0.89990       -0.98200
H          0.72950        2.52050        0.39050
H          2.98400        1.63970        0.69900
H          3.40000       -0.78260        0.62320
H          1.48900       -2.35480        0.22590
H         -0.80630       -1.48300       -0.08920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_012_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

