%nproc=16
%mem=2GB
%chk=mol_109_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.01970       -0.96000       -0.23250
O          1.25440       -0.65260        0.88490
C          0.03460       -0.08230        0.56620
O         -0.01090        1.22510        1.05050
C         -0.11850        2.16210        0.01140
O         -1.04170       -0.77360        1.09370
C         -1.92200       -1.19550        0.07380
H          2.96560       -1.40570        0.10580
H          1.50110       -1.56890       -0.97070
H          2.28990        0.01780       -0.71900
H         -0.02940       -0.01050       -0.54590
H         -1.05060        1.95250       -0.54730
H          0.75960        2.12690       -0.64520
H         -0.21250        3.15510        0.49200
H         -2.76010       -1.73830        0.52920
H         -2.27600       -0.35050       -0.53220
H         -1.40320       -1.90160       -0.61480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

