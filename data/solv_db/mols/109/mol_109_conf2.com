%nproc=16
%mem=2GB
%chk=mol_109_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.15830       -0.16620       -0.30210
O         -1.34250       -0.10720        0.81380
C         -0.02920       -0.04880        0.41740
O          0.74030       -1.10730        0.86650
C          1.29950       -1.83270       -0.19340
O          0.47710        1.18650        0.80960
C          0.85560        1.97880       -0.25560
H         -3.23890       -0.21530       -0.03990
H         -2.06310        0.76610       -0.93190
H         -1.89570       -1.00600       -0.97030
H          0.00910       -0.06540       -0.68840
H          1.90790       -2.67070        0.20280
H          2.01100       -1.14670       -0.69930
H          0.52340       -2.17430       -0.90780
H          1.65260        1.50550       -0.88130
H         -0.05300        2.20180       -0.87800
H          1.30420        2.90210        0.16750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

