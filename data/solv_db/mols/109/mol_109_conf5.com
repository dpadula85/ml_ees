%nproc=16
%mem=2GB
%chk=mol_109_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.00780       -0.22470       -0.84690
O         -1.14790       -1.15770       -0.28780
C          0.03620       -0.58750        0.17010
O          0.24800       -0.75480        1.51240
C          0.31070        0.49110        2.17030
O          1.06250       -1.08840       -0.62190
C          1.65840       -0.10290       -1.40270
H         -2.31170        0.56660       -0.14930
H         -2.93180       -0.77180       -1.13190
H         -1.60960        0.22580       -1.78190
H         -0.03040        0.49940       -0.04380
H          1.16000        1.07120        1.72370
H          0.54280        0.35720        3.24670
H         -0.58820        1.08800        2.03890
H          2.45770       -0.60850       -1.99040
H          0.96490        0.37420       -2.10430
H          2.18620        0.62290       -0.73820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

