%nproc=16
%mem=2GB
%chk=mol_109_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.58860       -1.85690       -0.96640
O          1.37030       -0.90090       -0.35870
C          0.60250        0.12120        0.18230
O          0.73620        0.16130        1.55700
C         -0.47050       -0.10310        2.20120
O          0.84720        1.34920       -0.42130
C         -0.25650        1.87020       -1.06740
H          1.18320       -2.67290       -1.39960
H         -0.11070       -2.26370       -0.20420
H          0.01530       -1.32670       -1.78190
H         -0.46020       -0.11760       -0.02820
H         -0.36180       -0.07080        3.29210
H         -1.19180        0.72200        1.94240
H         -0.88520       -1.10220        1.89910
H          0.05190        2.84070       -1.50740
H         -1.01530        2.10750       -0.29770
H         -0.64300        1.24280       -1.88290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

