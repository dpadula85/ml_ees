%nproc=16
%mem=2GB
%chk=mol_109_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21310        0.36840       -0.15380
O          1.36400        0.31140        0.94600
C          0.05250        0.09350        0.53760
O         -0.39450       -1.09710        1.14840
C         -0.77340       -2.03770        0.20290
O         -0.81030        1.09000        0.97030
C         -1.42560        1.63430       -0.15020
H          3.24900        0.54190        0.19120
H          2.10730       -0.57710       -0.73590
H          1.84110        1.17700       -0.81490
H          0.00470        0.02000       -0.54220
H         -1.10930       -2.93670        0.75760
H         -1.61460       -1.66610       -0.38240
H          0.04370       -2.27610       -0.51850
H         -0.62280        2.05690       -0.81660
H         -1.98520        0.85870       -0.72670
H         -2.13980        2.43870        0.08730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_109_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

