%nproc=16
%mem=2GB
%chk=mol_154_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.48940       -0.69200        0.10260
C          0.87140        0.00100       -0.02640
C         -0.37260        0.76480       -0.14430
H         -0.79440       -1.01590        1.11770
H         -0.79280       -1.40390       -0.68400
H          1.44940       -0.35320       -0.91160
H          1.47950        0.06880        0.89740
H         -0.60730        1.43430        0.73600
H         -0.74390        1.19610       -1.08730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

