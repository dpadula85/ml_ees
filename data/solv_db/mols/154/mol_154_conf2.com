%nproc=16
%mem=2GB
%chk=mol_154_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.82890       -0.29700       -0.01630
C         -0.11260        0.85620       -0.03710
C         -0.63020       -0.58010        0.04230
H          1.40760       -0.40870        0.91860
H          1.31780       -0.59510       -0.95370
H         -0.23640        1.41090       -0.98610
H         -0.33930        1.43620        0.86140
H         -1.01770       -0.95390        0.99980
H         -1.21810       -0.86830       -0.82880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

