%nproc=16
%mem=2GB
%chk=mol_154_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.47730       -0.70890        0.00100
C          0.85730       -0.05210       -0.03510
C         -0.33930        0.79320       -0.01840
H         -0.85750       -1.12030        0.96430
H         -0.88490       -1.23780       -0.87610
H          1.44020       -0.16080        0.91660
H          1.51330       -0.14730       -0.93140
H         -0.56590        1.35260        0.92670
H         -0.68600        1.28120       -0.94770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

