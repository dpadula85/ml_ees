%nproc=16
%mem=2GB
%chk=mol_154_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.66750       -0.53290       -0.07800
C          0.78980       -0.34620       -0.06900
C         -0.09670        0.86280        0.09760
H         -1.13350       -1.00650        0.82920
H         -1.25160       -0.73770       -0.99230
H          1.42510       -0.45020       -0.96090
H          1.32720       -0.64370        0.86870
H         -0.17510        1.53340       -0.79540
H         -0.21770        1.32110        1.10010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

