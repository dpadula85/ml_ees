%nproc=16
%mem=2GB
%chk=mol_154_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.15530        0.82700       -0.05420
C         -0.65970       -0.54910        0.00670
C          0.79660       -0.30010        0.07390
H         -0.12030        1.39910       -1.01960
H         -0.34940        1.50590        0.81710
H         -1.25560       -0.81140        0.90770
H         -1.03720       -1.04600       -0.90570
H          1.40370       -0.56300       -0.83260
H          1.37720       -0.46240        1.00670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_154_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

