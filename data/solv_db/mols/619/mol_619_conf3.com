%nproc=16
%mem=2GB
%chk=mol_619_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.91350       -1.38570        0.77130
C          0.26560       -0.82830       -0.44400
C          0.36910        0.64510       -0.61670
C          1.07130        1.37940        0.46230
O          2.44030        1.29280        0.24720
S          3.14290       -0.09860        0.83270
O          4.51150       -0.26580        0.13680
O          2.28450       -1.49950        0.60210
C         -1.05870        1.12940       -0.69760
C         -1.66510        0.87370        0.63480
C         -1.75990       -0.44150        0.79080
C         -1.22080       -1.09510       -0.43110
C         -1.67550       -0.05120       -1.47160
Cl        -3.42590        0.02830       -1.45960
Cl        -0.95700       -0.29100       -3.02950
Cl        -1.72420       -2.69250       -0.74140
Cl        -2.39430       -1.31760        2.16170
Cl        -2.14160        2.13550        1.74130
Cl        -1.31860        2.66500       -1.38650
H          0.54120       -2.44720        0.87650
H          0.63020       -0.89320        1.71080
H          0.72240       -1.34040       -1.33270
H          0.86580        0.89550       -1.58570
H          0.81860        2.47570        0.33710
H          0.76490        1.12750        1.48040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

