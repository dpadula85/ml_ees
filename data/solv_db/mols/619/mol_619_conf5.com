%nproc=16
%mem=2GB
%chk=mol_619_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.86540       -1.40510       -0.62190
C          0.35190       -0.00290       -0.82720
C          0.36430        0.84090        0.39850
C          1.20110        0.34050        1.51890
O          2.46040       -0.04410        1.21540
S          2.87770       -0.00450       -0.40760
O          4.40120       -0.21510       -0.49770
O          2.18020       -1.40990       -1.12580
C         -1.09360        0.91620        0.77740
C         -1.72740       -0.45160        0.87980
C         -1.75820       -1.00730       -0.32580
C         -1.12780       -0.02590       -1.24360
C         -1.67050        1.26920       -0.60990
Cl        -3.42120        1.27660       -0.60800
Cl        -0.93660        2.69970       -1.28690
Cl        -1.42360       -0.20680       -2.90570
Cl        -2.40230       -2.55320       -0.82880
Cl        -2.34440       -1.24320        2.31590
Cl        -1.51090        2.00350        2.01940
H          0.24750       -2.17890       -1.09520
H          0.95480       -1.66940        0.44660
H          0.87910        0.50550       -1.66100
H          0.70940        1.85450        0.13200
H          0.65370       -0.46820        2.06690
H          1.26980        1.17940        2.27440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

