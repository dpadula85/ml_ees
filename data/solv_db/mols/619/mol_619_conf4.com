%nproc=16
%mem=2GB
%chk=mol_619_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.27640       -0.99760        1.18640
C          0.51960       -0.81770       -0.05500
C          0.39510        0.51160       -0.65430
C          0.68870        1.68660        0.20280
O          2.02340        2.07590       -0.06220
S          2.96540        0.68480        0.21340
O          3.26270       -0.08040       -1.03770
O          2.31460       -0.18300        1.45890
C         -1.09150        0.62340       -1.03200
C         -1.79240        0.85300        0.26250
C         -1.67900       -0.28510        0.94550
C         -0.91290       -1.28190        0.11230
C         -1.47400       -0.84890       -1.25710
Cl        -0.61190       -1.57240       -2.58620
Cl        -3.21670       -1.04010       -1.32180
Cl        -1.14800       -2.91620        0.50740
Cl        -2.34030       -0.51360        2.55120
Cl        -2.59250        2.33500        0.75140
Cl        -1.45220        1.68280       -2.30830
H          0.56440       -0.92930        2.04850
H          1.65280       -2.05340        1.24130
H          0.95410       -1.50600       -0.84180
H          0.93840        0.60740       -1.63780
H          0.69970        1.39450        1.29180
H          0.05610        2.57060        0.02090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

