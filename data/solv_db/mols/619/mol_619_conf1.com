%nproc=16
%mem=2GB
%chk=mol_619_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.08260        1.58170       -0.01230
C          0.14490        0.75610        0.74440
C          0.30690       -0.69470        0.78790
C          1.22680       -1.33220       -0.17590
O          2.55830       -0.98250       -0.03810
S          2.90410        0.30360       -1.13100
O          3.93650        1.18600       -0.39990
O          1.42810        1.18350       -1.26430
C         -1.09360       -1.26220        0.51640
C         -1.35560       -0.99060       -0.92770
C         -1.47770        0.33690       -1.06050
C         -1.30340        0.95410        0.27690
C         -1.94880       -0.14730        1.14170
Cl        -3.65340       -0.34490        0.78340
Cl        -1.59320        0.06880        2.84480
Cl        -1.94180        2.51590        0.50240
Cl        -1.78850        1.15120       -2.56820
Cl        -1.46990       -2.22140       -2.16480
Cl        -1.38940       -2.83280        1.08090
H          2.02270        1.73320        0.58220
H          0.64610        2.61520       -0.10140
H          0.13570        1.13600        1.81180
H          0.56420       -1.07690        1.81520
H          0.88300       -1.19490       -1.23970
H          1.17570       -2.44150       -0.01600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

