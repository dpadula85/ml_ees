%nproc=16
%mem=2GB
%chk=mol_619_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.16030        1.34080       -0.28530
C          0.23900        0.74910        0.71020
C          0.14990       -0.70500        0.81080
C          1.19690       -1.56120        0.25940
O          1.71230       -1.26500       -0.96110
S          3.08230       -0.25310       -0.70370
O          3.82110        0.00220       -1.99630
O          2.50850        1.17340        0.01810
C         -1.25220       -1.04110        0.26930
C         -1.40330       -0.55630       -1.12020
C         -1.35460        0.76830       -1.09990
C         -1.16750        1.19240        0.33490
C         -2.01040        0.08850        1.00150
Cl        -1.76200        0.02410        2.73110
Cl        -3.69160        0.16340        0.51180
Cl        -1.60050        2.78530        0.71490
Cl        -1.47610        1.88080       -2.43600
Cl        -1.60790       -1.57660       -2.51820
Cl        -1.80670       -2.61690        0.61500
H          0.93640        0.95350       -1.31740
H          0.97750        2.43080       -0.33670
H          0.44450        1.24920        1.70200
H          0.05200       -0.96070        1.90970
H          2.05770       -1.64950        0.98290
H          0.79440       -2.61630        0.20320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_619_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

