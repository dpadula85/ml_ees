%nproc=16
%mem=2GB
%chk=mol_225_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.41730       -0.72270        0.02680
C         -0.70260        0.47060       -0.49530
C          0.54490        0.86630        0.21670
C          1.60680       -0.17360        0.21010
Cl         1.09720       -1.66380        0.99080
H         -2.50730       -0.59900       -0.25710
H         -1.41900       -0.77680        1.12570
H         -1.13110       -1.67690       -0.47750
H         -0.37880        0.20050       -1.54680
H         -1.40780        1.31940       -0.61640
H          0.31300        1.16010        1.26360
H          0.95390        1.75560       -0.33980
H          1.91270       -0.32860       -0.85150
H          2.53550        0.16890        0.75070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

