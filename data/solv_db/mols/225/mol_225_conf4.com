%nproc=16
%mem=2GB
%chk=mol_225_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.40080        0.61210        0.23170
C          0.58500       -0.60790        0.50400
C         -0.52830       -0.81730       -0.49880
C         -1.43600        0.41650       -0.42120
Cl        -2.06480        0.52810        1.23030
H          0.86420        1.54970        0.47630
H          2.29450        0.57580        0.88830
H          1.76650        0.69270       -0.80450
H          1.23920       -1.49010        0.49270
H          0.17680       -0.50280        1.52970
H         -0.15390       -0.84490       -1.53390
H         -1.08420       -1.74010       -0.23520
H         -0.80100        1.27550       -0.70440
H         -2.25870        0.35260       -1.15490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

