%nproc=16
%mem=2GB
%chk=mol_225_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93490       -0.27970        0.09370
C          0.62470        0.38370       -0.23970
C         -0.55030       -0.32550        0.38580
C         -1.85570        0.32430        0.06140
Cl        -3.14140       -0.62970        0.87210
H          2.09550       -1.22600       -0.45590
H          1.98400       -0.44640        1.19280
H          2.73840        0.43110       -0.15570
H          0.55290        0.46550       -1.32750
H          0.67500        1.41660        0.16180
H         -0.60560       -1.39050        0.08820
H         -0.43580       -0.31970        1.48880
H         -1.94430        1.35600        0.41490
H         -2.07210        0.24020       -1.03540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

