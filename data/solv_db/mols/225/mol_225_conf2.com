%nproc=16
%mem=2GB
%chk=mol_225_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.56860        0.30980       -0.13340
C          0.54230       -0.53140        0.58220
C         -0.68880       -0.71140       -0.27710
C         -1.35390        0.57820       -0.63180
Cl        -1.88990        1.48800        0.78300
H          1.15540        0.54210       -1.14830
H          2.47630       -0.27830       -0.30770
H          1.76820        1.24980        0.43750
H          1.02160       -1.49740        0.82920
H          0.24760       -0.06450        1.56660
H         -1.37950       -1.36780        0.27360
H         -0.42360       -1.22700       -1.22040
H         -2.28120        0.30330       -1.21430
H         -0.76300        1.20670       -1.32850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

