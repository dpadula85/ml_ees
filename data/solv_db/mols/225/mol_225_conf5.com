%nproc=16
%mem=2GB
%chk=mol_225_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.33260        0.64420        0.55550
C          0.83680       -0.54680       -0.19600
C         -0.61580       -0.82760        0.00420
C         -1.52430        0.26180       -0.45140
Cl        -1.31300        1.80790        0.37500
H          0.83080        0.80130        1.52560
H          2.43360        0.52560        0.75830
H          1.28140        1.58920       -0.02040
H          0.95720       -0.37010       -1.31180
H          1.44870       -1.44030        0.09550
H         -0.79890       -1.02270        1.09100
H         -0.82950       -1.79950       -0.52370
H         -1.43860        0.45520       -1.55620
H         -2.60100       -0.07840       -0.34560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_225_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

