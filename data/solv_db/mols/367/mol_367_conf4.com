%nproc=16
%mem=2GB
%chk=mol_367_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.80760       -0.09680        0.06300
O          1.55280        0.26580        0.60590
C          0.60400       -0.17340       -0.31660
C         -0.80560        0.10140        0.04550
O         -1.19550       -0.53940        1.17430
C         -0.61030       -0.28110        2.36170
O         -1.01850        1.47550        0.28140
C         -0.65360        2.24700       -0.81310
O         -1.64180       -0.30020       -0.99260
C         -1.64270       -1.63800       -1.26480
H          3.63990        0.20380        0.71400
H          2.83630        0.36570       -0.94370
H          2.80430       -1.20850       -0.09480
H          0.73300       -1.28380       -0.45690
H          0.86160        0.25960       -1.31970
H         -0.64250        0.77050        2.70810
H          0.39900       -0.70870        2.41490
H         -1.23940       -0.81100        3.16030
H         -1.23560        1.98020       -1.72940
H         -0.92260        3.29440       -0.56000
H          0.43090        2.22760       -1.00130
H         -0.67690       -2.06840       -1.57890
H         -2.41740       -1.81760       -2.06000
H         -1.96710       -2.26480       -0.39750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

