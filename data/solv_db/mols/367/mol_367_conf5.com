%nproc=16
%mem=2GB
%chk=mol_367_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.01040       -0.48230       -0.55100
O          1.64850       -0.81240       -0.55040
C          0.97260        0.35200       -0.24040
C         -0.50620        0.28260       -0.17230
O         -1.11490       -0.06320       -1.33530
C         -0.93090       -1.30640       -1.84130
O         -0.93430       -0.58310        0.83380
C         -0.39640       -0.36270        2.06320
O         -0.90430        1.56330        0.26450
C         -2.24390        1.68050        0.48370
H          3.64860       -1.35060       -0.78980
H          3.12530        0.35690       -1.26030
H          3.21370       -0.10140        0.49240
H          1.42820        0.72460        0.71140
H          1.25470        1.07230       -1.06910
H          0.07900       -1.61730       -2.11590
H         -1.52590       -1.38620       -2.79900
H         -1.31800       -2.11550       -1.16420
H         -0.84390       -1.12880        2.75810
H         -0.50570        0.62280        2.50400
H          0.69850       -0.61350        2.05480
H         -2.87150        1.48810       -0.40880
H         -2.44970        2.73150        0.78750
H         -2.53380        1.04870        1.34430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

