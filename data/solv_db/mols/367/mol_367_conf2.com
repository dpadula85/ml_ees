%nproc=16
%mem=2GB
%chk=mol_367_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.93260        1.08550       -0.11130
O          1.67850        0.75060       -0.63240
C          0.91480        0.24330        0.43750
C         -0.43700       -0.11220       -0.14970
O         -1.31260       -0.62980        0.78760
C         -0.84850       -1.72670        1.45850
O         -0.97720        1.00130       -0.79150
C         -1.27530        2.02630        0.08960
O         -0.16450       -1.04720       -1.16820
C         -1.31900       -1.41860       -1.83820
H          2.76130        1.84330        0.66880
H          3.42630        0.20220        0.36320
H          3.62480        1.41910       -0.90940
H          0.86380        0.96310        1.25420
H          1.39900       -0.71700        0.71050
H          0.09210       -1.54760        2.03630
H         -1.60950       -2.00270        2.22110
H         -0.69780       -2.61870        0.78180
H         -1.69570        2.85840       -0.51330
H         -2.09240        1.73600        0.80850
H         -0.40290        2.43150        0.62160
H         -1.01370       -2.15000       -2.61560
H         -1.82700       -0.59860       -2.34600
H         -2.02040       -1.99160       -1.16370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

