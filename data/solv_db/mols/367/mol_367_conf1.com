%nproc=16
%mem=2GB
%chk=mol_367_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.59980       -0.91210        0.84580
O          1.53810       -0.08610        0.45660
C          0.55350       -0.79680       -0.19650
C         -0.56270        0.19020       -0.56650
O         -1.60840       -0.51430       -1.12220
C         -2.42550       -1.27850       -0.40170
O         -0.79560        1.10370        0.33100
C         -1.34280        1.10790        1.51620
O         -0.05990        0.88270       -1.71410
C          0.99520        1.69550       -1.51900
H          2.27750       -1.71670        1.53250
H          2.97680       -1.37470       -0.10780
H          3.35010       -0.26580        1.32810
H          0.96540       -1.15850       -1.15980
H          0.16480       -1.65540        0.36190
H         -3.04560       -0.82650        0.37840
H         -3.19510       -1.71500       -1.12840
H         -1.95480       -2.22720       -0.01080
H         -2.40540        0.83530        1.65630
H         -1.33310        2.19490        1.89230
H         -0.74670        0.61150        2.34240
H          1.16400        2.19740       -2.54250
H          0.89500        2.50820       -0.79610
H          1.99530        1.20040       -1.37620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

