%nproc=16
%mem=2GB
%chk=mol_367_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.34200        0.07640       -0.06540
O          2.00090        0.31160        0.24070
C          1.17180       -0.62310       -0.36360
C         -0.28350       -0.17520        0.11020
O         -1.16430       -1.07130       -0.46080
C         -1.37750       -1.03510       -1.78620
O         -0.22800       -0.43060        1.44890
C         -1.30970       -0.38770        2.23120
O         -0.32470        1.10430       -0.21200
C         -1.32290        1.96180       -0.06490
H          3.45850        0.14010       -1.15130
H          4.00310        0.84410        0.38210
H          3.68920       -0.90350        0.29850
H          1.19660       -0.53410       -1.44480
H          1.38170       -1.62080        0.04010
H         -1.77570       -0.05910       -2.10080
H         -0.52840       -1.33920       -2.42890
H         -2.22620       -1.75360       -2.00260
H         -1.89890        0.49740        2.37100
H         -0.97500       -0.67240        3.28710
H         -1.97090       -1.25870        1.93970
H         -1.68740        2.19680        0.95450
H         -0.96250        2.97020       -0.47780
H         -2.20810        1.76180       -0.74500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_367_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

