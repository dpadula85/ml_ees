%nproc=16
%mem=2GB
%chk=mol_186_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.30490        0.45370       -0.18680
C         -0.23000       -0.58760       -0.41690
C          0.92520       -0.29500        0.51410
O          1.49140        0.92790        0.33430
H         -2.22320        0.26210       -0.74580
H         -0.87380        1.45980       -0.35050
H         -1.57470        0.37310        0.90240
H          0.05780       -0.56930       -1.47300
H         -0.64690       -1.58360       -0.15890
H          1.70490       -1.09710        0.41810
H          0.55230       -0.36830        1.55620
H          2.12190        1.02440       -0.39300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

