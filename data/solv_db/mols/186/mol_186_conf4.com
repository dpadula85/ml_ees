%nproc=16
%mem=2GB
%chk=mol_186_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23500        0.37150       -0.11880
C         -0.25170       -0.77170       -0.27450
C          1.06470       -0.32570        0.33410
O          1.48010        0.79820       -0.36010
H         -0.66670        1.32180       -0.04700
H         -1.84050        0.28340        0.81030
H         -1.87320        0.45770       -1.02310
H         -0.63070       -1.70100        0.21950
H         -0.13880       -0.94150       -1.36500
H          1.83940       -1.09650        0.28440
H          0.90160       -0.01680        1.38560
H          1.35090        1.62070        0.15460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

