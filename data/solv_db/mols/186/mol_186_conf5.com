%nproc=16
%mem=2GB
%chk=mol_186_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.48950        0.16640       -0.25580
C          0.22740       -0.21530        0.46420
C         -1.00830       -0.03410       -0.38780
O         -2.10300       -0.42560        0.40610
H          2.33060       -0.01070        0.44610
H          1.67110       -0.46720       -1.14420
H          1.53080        1.24120       -0.52010
H          0.14950        0.38520        1.38880
H          0.30760       -1.29440        0.71790
H         -0.98960       -0.74840       -1.23660
H         -1.15030        1.00100       -0.71100
H         -2.45530        0.40170        0.83250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

