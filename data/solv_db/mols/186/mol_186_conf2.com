%nproc=16
%mem=2GB
%chk=mol_186_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.48960       -0.01570        0.48710
C         -0.07330        0.46790        0.38520
C          0.93140       -0.64150        0.28920
O          2.19800       -0.05010        0.20060
H         -2.12110        0.88670        0.70570
H         -1.88390       -0.42180       -0.47970
H         -1.59180       -0.72550        1.31670
H          0.15000        1.04790        1.29710
H          0.05400        1.10260       -0.52350
H          0.72670       -1.27240       -0.59950
H          0.95720       -1.26520        1.21930
H          2.14220        0.88710       -0.11940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

