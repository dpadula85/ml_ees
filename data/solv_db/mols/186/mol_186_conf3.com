%nproc=16
%mem=2GB
%chk=mol_186_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24760        0.41420        0.02080
C         -0.15190       -0.46040        0.54230
C          1.01300       -0.50830       -0.42980
O          1.55110        0.74770       -0.64840
H         -1.21970        1.43470        0.49710
H         -1.18290        0.54050       -1.09470
H         -2.24910       -0.00890        0.24100
H          0.25380        0.00730        1.48520
H         -0.51500       -1.45750        0.81200
H          1.81770       -1.15240       -0.02190
H          0.68910       -0.98850       -1.38830
H          1.24150        1.43180       -0.01530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_186_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

