%nproc=16
%mem=2GB
%chk=mol_174_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.68310       -0.61390        0.00510
C         -0.66890       -0.62810        0.00640
C         -1.29930        0.58920       -0.00430
S         -0.00700        1.69740       -0.01570
C          1.29750        0.60950       -0.00680
H          1.26640       -1.57630        0.01350
H         -1.26500       -1.58020        0.01570
H         -2.39480        0.73670       -0.00470
H          2.38800        0.76580       -0.00920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_174_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_174_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_174_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

