%nproc=16
%mem=2GB
%chk=mol_214_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.09660       -0.06530        0.00730
F         -0.83050        1.10600       -0.09970
Cl         1.65550        0.26200       -0.06260
H         -0.41480       -0.74020       -0.82420
H         -0.31370       -0.56240        0.97920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_214_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_214_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_214_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

