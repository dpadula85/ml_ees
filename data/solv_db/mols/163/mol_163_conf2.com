%nproc=16
%mem=2GB
%chk=mol_163_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.63350       -0.25170        0.93390
C          1.90070        0.26150       -0.00850
C          0.46910        0.13200       -0.10600
C         -0.23970        0.71790       -1.15570
C         -1.60710        0.61940       -1.28750
C         -2.34870       -0.08130       -0.36180
C         -1.66170       -0.66410        0.67850
C         -0.27050       -0.56350        0.81200
H          2.19470       -0.82340        1.72320
H          3.69760       -0.10720        0.93160
H          2.41260        0.84040       -0.79330
H          0.36680        1.27500       -1.88810
H         -2.10430        1.09040       -2.11650
H         -3.43500       -0.17230       -0.44660
H         -2.22730       -1.22940        1.43220
H          0.21930       -1.04360        1.65260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

