%nproc=16
%mem=2GB
%chk=mol_163_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.68750        0.69820        0.19770
C         -1.89270       -0.29970       -0.04960
C         -0.45600       -0.22090       -0.04730
C          0.17320        0.96130        0.22770
C          1.57350        1.05010        0.23250
C          2.32130       -0.05040       -0.03910
C          1.72980       -1.25860       -0.32070
C          0.35250       -1.32530       -0.32050
H         -2.31850        1.67650        0.42710
H         -3.76510        0.59140        0.18430
H         -2.34280       -1.27740       -0.27790
H         -0.35800        1.89050        0.45630
H          2.07990        1.98480        0.44980
H          3.40220       -0.03230       -0.04760
H          2.35280       -2.13420       -0.53690
H         -0.16470       -2.25410       -0.53620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

