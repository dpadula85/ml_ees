%nproc=16
%mem=2GB
%chk=mol_163_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.74580       -0.32930        0.38760
C          1.83170        0.57910        0.17910
C          0.42530        0.24820        0.03020
C         -0.50740        1.27230       -0.19230
C         -1.85300        1.03450       -0.34250
C         -2.34670       -0.25060       -0.27830
C         -1.43930       -1.25900       -0.06070
C         -0.07170       -1.03520        0.09380
H          3.80580       -0.05190        0.49700
H          2.50650       -1.36400        0.45960
H          2.14050        1.61250        0.11640
H         -0.09750        2.29080       -0.24090
H         -2.50770        1.88120       -0.51140
H         -3.39750       -0.47350       -0.39190
H         -1.83730       -2.27350       -0.01110
H          0.60250       -1.88170        0.26530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_163_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

