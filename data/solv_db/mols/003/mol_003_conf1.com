%nproc=16
%mem=2GB
%chk=mol_003_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.64840        0.23180        0.39480
C         -1.21180       -1.10900       -0.06830
C          0.25520       -1.34720       -0.07300
C          1.05990       -0.09750        0.29210
C          0.50740        0.96730       -0.66670
C         -0.82090        1.37520       -0.06770
N          2.45520       -0.32970        0.09670
H         -1.80290        0.23600        1.51540
H         -2.69260        0.39210       -0.00920
H         -1.66930       -1.87000        0.63120
H         -1.69400       -1.34660       -1.06140
H          0.55300       -1.56770       -1.14080
H          0.58100       -2.21690        0.51620
H          0.80610        0.13860        1.33810
H          0.26450        0.49940       -1.66050
H          1.17980        1.82190       -0.74310
H         -0.56220        1.99370        0.84200
H         -1.38090        2.08340       -0.72730
H          2.93100       -0.31110        1.03570
H          2.88970        0.45620       -0.44410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

