%nproc=16
%mem=2GB
%chk=mol_003_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.76290        0.18630        0.10470
C         -1.13160       -1.13590        0.39420
C          0.34160       -1.10280        0.57720
C          1.09420       -0.28200       -0.40780
C          0.29750        0.85180       -1.00140
C         -0.70310        1.28830        0.06380
N          2.30900        0.14590        0.22690
H         -2.60320        0.44450        0.76590
H         -2.19220        0.16570       -0.93570
H         -1.57490       -1.49960        1.36800
H         -1.43440       -1.90880       -0.35200
H          0.61690       -0.80210        1.62560
H          0.71880       -2.16020        0.49110
H          1.39800       -0.93380       -1.27500
H          0.92360        1.71090       -1.31190
H         -0.28580        0.46780       -1.88680
H         -1.14610        2.26060       -0.13610
H         -0.17350        1.26390        1.04210
H          2.19580        0.77470        1.02140
H          3.11250        0.26470       -0.37410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

