%nproc=16
%mem=2GB
%chk=mol_003_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.71190       -0.41860        0.05110
C         -1.24430        0.91810       -0.39240
C          0.15560        1.26170       -0.05440
C          1.10750        0.10200       -0.16790
C          0.55740       -1.05170        0.66820
C         -0.69500       -1.51110       -0.06040
N          2.42740        0.45880        0.23010
H         -2.17750       -0.38680        1.07440
H         -2.57100       -0.71170       -0.62270
H         -1.91110        1.68490        0.09900
H         -1.41140        1.08480       -1.49530
H          0.49820        2.05170       -0.78690
H          0.20400        1.76560        0.93710
H          1.06950       -0.24770       -1.22050
H          1.34680       -1.82140        0.68120
H          0.23550       -0.72500        1.66880
H         -1.12530       -2.42770        0.36470
H         -0.43100       -1.62650       -1.13210
H          3.14340        0.19360       -0.46940
H          2.53340        1.40700        0.62760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

