%nproc=16
%mem=2GB
%chk=mol_003_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.61160       -0.60820       -0.18600
C          0.36460       -1.50420       -0.20020
C         -0.53060       -0.92340        0.89230
C         -0.93360        0.47970        0.51980
C          0.20360        1.36410        0.16370
C          1.15410        0.69100       -0.81120
N         -1.91300        0.50440       -0.54200
H          2.45970       -1.06830       -0.69000
H          1.84640       -0.46990        0.90000
H          0.67790       -2.51800        0.02980
H         -0.14280       -1.38990       -1.18900
H          0.06210       -0.86900        1.83330
H         -1.39460       -1.56600        1.00060
H         -1.43590        0.90520        1.43900
H          0.73720        1.80490        1.02140
H         -0.22120        2.24480       -0.40260
H          0.54160        0.46510       -1.72060
H          1.96530        1.33740       -1.12170
H         -2.45630        1.40400       -0.44750
H         -2.59610       -0.28360       -0.48910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

