%nproc=16
%mem=2GB
%chk=mol_003_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.75870       -0.11280       -0.11600
C         -0.85910       -1.29290       -0.19400
C          0.50640       -1.09160        0.36350
C          1.10160        0.21900       -0.13650
C          0.16230        1.30010        0.36590
C         -1.07940        1.17650       -0.52440
N          2.45430        0.40940        0.25220
H         -2.13220        0.02830        0.90930
H         -2.61620       -0.29340       -0.82420
H         -0.75270       -1.61590       -1.25330
H         -1.33630       -2.12960        0.38660
H          1.16340       -1.89530       -0.04550
H          0.47410       -1.12630        1.47760
H          1.07510        0.14110       -1.25840
H          0.59030        2.29630        0.35490
H         -0.20570        1.01100        1.39480
H         -0.75030        1.05320       -1.58080
H         -1.74620        2.02350       -0.38990
H          2.65120        0.07340        1.20800
H          3.05800       -0.17410       -0.38980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_003_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

