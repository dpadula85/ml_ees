%nproc=16
%mem=2GB
%chk=mol_418_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.26920       -0.03460       -0.06200
C          1.82560       -0.06140       -0.52150
C          0.97330        0.09210        0.72850
C         -0.47990        0.07790        0.38140
C         -1.16900        1.23020        0.08320
C         -2.50670        1.13740       -0.22710
C         -3.18980       -0.05910       -0.25300
C         -2.50200       -1.22120        0.04600
C         -1.16150       -1.11890        0.35590
H          3.96940       -0.25080       -0.89400
H          3.45770        0.99440        0.30990
H          3.38700       -0.74450        0.78440
H          1.65020        0.77720       -1.22990
H          1.62650       -1.02930       -1.01520
H          1.25370       -0.71860        1.41030
H          1.22720        1.07200        1.19450
H         -0.67200        2.19480        0.09120
H         -3.08690        2.04240       -0.47070
H         -4.24880       -0.13870       -0.49800
H         -3.00080       -2.19980        0.03920
H         -0.62260       -2.04150        0.59150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

