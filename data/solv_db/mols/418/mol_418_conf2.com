%nproc=16
%mem=2GB
%chk=mol_418_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.25010        0.19060        0.59530
C          1.79180       -0.20600        0.84410
C          0.98320        0.38410       -0.27250
C         -0.45240        0.09500       -0.18430
C         -1.06430       -1.00080       -0.74530
C         -2.43230       -1.20400       -0.61720
C         -3.18670       -0.27750        0.09230
C         -2.60450        0.83290        0.66790
C         -1.23980        0.98560        0.50930
H          3.69140       -0.43460       -0.21080
H          3.87390        0.02790        1.49290
H          3.32460        1.27340        0.34550
H          1.73290       -1.32180        0.78250
H          1.46180        0.07160        1.84600
H          1.34300       -0.10850       -1.22050
H          1.18110        1.47510       -0.34450
H         -0.44550       -1.70320       -1.29190
H         -2.89750       -2.08340       -1.07220
H         -4.26430       -0.40500        0.21320
H         -3.22810        1.52600        1.20960
H         -0.81830        1.88260        0.98320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

