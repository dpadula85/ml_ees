%nproc=16
%mem=2GB
%chk=mol_418_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.28590       -0.02840        0.45450
C          1.81180        0.25630        0.65880
C          0.99020       -0.31010       -0.44010
C         -0.44750       -0.09920       -0.35230
C         -1.20900       -1.04420        0.31090
C         -2.57870       -0.94610        0.45410
C         -3.24390        0.13900       -0.08210
C         -2.48300        1.08080       -0.74320
C         -1.12580        0.97600       -0.88080
H          3.93230        0.57960        1.10300
H          3.59960        0.21840       -0.59960
H          3.50440       -1.11500        0.57300
H          1.56320       -0.04710        1.66260
H          1.70590        1.37820        0.61330
H          1.17970       -1.40490       -0.48190
H          1.32010        0.07880       -1.43300
H         -0.74420       -1.93460        0.76330
H         -3.18510       -1.67860        0.97040
H         -4.32770        0.24330        0.01590
H         -3.01140        1.94330       -1.16880
H         -0.53670        1.71460       -1.39800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

