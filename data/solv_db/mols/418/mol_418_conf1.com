%nproc=16
%mem=2GB
%chk=mol_418_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19890       -0.25860        1.18050
C          2.09430       -0.61450       -0.28810
C          1.10340        0.24850       -0.99100
C         -0.25900        0.19290       -0.50120
C         -1.20390       -0.69830       -0.98080
C         -2.48860       -0.71230       -0.47750
C         -2.88610        0.15250        0.51670
C         -1.95160        1.04560        1.00230
C         -0.65440        1.06180        0.49600
H          1.22860       -0.40780        1.70540
H          2.92360       -0.92330        1.68480
H          2.52820        0.78950        1.32970
H          1.94260       -1.68740       -0.39920
H          3.09960       -0.38650       -0.72870
H          1.11890        0.04710       -2.08480
H          1.46250        1.30980       -0.89430
H         -0.88350       -1.36200       -1.74960
H         -3.21190       -1.42950       -0.87860
H         -3.90890        0.11880        0.89550
H         -2.27910        1.71910        1.78360
H          0.02630        1.79470        0.92920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

