%nproc=16
%mem=2GB
%chk=mol_418_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.28010       -0.16450       -0.09080
C          1.84500       -0.00200       -0.59130
C          0.94010       -0.19580        0.58590
C         -0.48320       -0.07370        0.28680
C         -1.25380       -1.14810       -0.09020
C         -2.60380       -0.98460       -0.36490
C         -3.21340        0.25050       -0.27010
C         -2.44140        1.32800        0.10770
C         -1.10410        1.15010        0.37680
H          3.31980       -0.17910        1.02520
H          3.87290        0.71650       -0.44660
H          3.72280       -1.12490       -0.43430
H          1.71520        0.98550       -1.06410
H          1.71080       -0.75760       -1.38400
H          1.15970       -1.18580        1.04040
H          1.23810        0.55860        1.35520
H         -0.82620       -2.15930       -0.18420
H         -3.23710       -1.80910       -0.66360
H         -4.25090        0.43120       -0.47080
H         -2.86240        2.33570        0.20220
H         -0.52840        2.02840        0.67270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_418_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

