%nproc=16
%mem=2GB
%chk=mol_580_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.65000        1.21820        0.40170
C         -0.35710        0.57110        0.12280
C          0.82880        1.30360        0.02640
C          2.05000        0.71720       -0.23440
C          2.10280       -0.65580       -0.40800
C          0.97410       -1.40390       -0.32240
C         -0.25980       -0.78410       -0.05560
C         -1.46320       -1.60450        0.03320
O         -2.60510       -1.15520        0.26540
H         -2.09490        0.94000        1.38700
H         -1.42990        2.32940        0.49310
H         -2.38920        1.17030       -0.40330
H          0.76390        2.37830        0.16630
H          2.96500        1.29180       -0.30770
H          3.04350       -1.13870       -0.61250
H          0.94690       -2.48560       -0.44860
H         -1.42570       -2.69200       -0.10350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

