%nproc=16
%mem=2GB
%chk=mol_580_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.71470        1.14950       -0.09610
C         -0.36550        0.56420       -0.03910
C          0.76450        1.37640       -0.08280
C          2.00770        0.80090       -0.02810
C          2.14990       -0.56700        0.06920
C          1.01780       -1.35110        0.11090
C         -0.23850       -0.81440        0.05880
C         -1.40330       -1.65560        0.10560
O         -2.58900       -1.30520        0.06870
H         -2.27540        0.86550       -1.03370
H         -2.30840        1.01480        0.82020
H         -1.59030        2.26650       -0.21770
H          0.63380        2.44930       -0.15950
H          2.89890        1.44510       -0.06280
H          3.13160       -1.05030        0.11430
H          1.13160       -2.44370        0.18860
H         -1.25070       -2.74510        0.18350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

