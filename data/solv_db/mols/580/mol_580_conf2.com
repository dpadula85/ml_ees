%nproc=16
%mem=2GB
%chk=mol_580_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.50530       -1.41690        0.30840
C         -0.26410       -0.62430        0.10740
C          0.96550       -1.24300        0.10270
C          2.10420       -0.47660       -0.08690
C          2.01730        0.89040       -0.26950
C          0.79810        1.51830       -0.26690
C         -0.36200        0.75510       -0.07620
C         -1.66760        1.37610       -0.06580
O         -1.75490        2.60580       -0.22950
H         -1.24780       -2.50610        0.20070
H         -2.28220       -1.16970       -0.45830
H         -1.92690       -1.30580        1.32210
H          1.00520       -2.31740        0.24810
H          3.08550       -0.94060       -0.09470
H          2.91110        1.49400       -0.41860
H          0.66300        2.57800       -0.40360
H         -2.53900        0.78250        0.08050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

