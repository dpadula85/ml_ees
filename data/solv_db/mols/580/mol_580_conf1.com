%nproc=16
%mem=2GB
%chk=mol_580_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72920        0.93880        0.67200
C         -0.38510        0.49960        0.21810
C          0.65610        1.41470        0.14460
C          1.90770        1.04450       -0.26950
C          2.10490       -0.27630       -0.61450
C          1.06470       -1.19240       -0.54150
C         -0.20110       -0.82370       -0.12390
C         -1.29940       -1.77880       -0.04420
O         -1.16530       -2.97220       -0.34410
H         -2.46950        0.63660       -0.12170
H         -1.97490        0.45420        1.62100
H         -1.77410        2.03680        0.77080
H          0.51290        2.45850        0.41390
H          2.69720        1.78080       -0.31570
H          3.09480       -0.56370       -0.94100
H          1.22490       -2.22730       -0.81330
H         -2.26460       -1.43000        0.28910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_580_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

