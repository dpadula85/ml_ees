%nproc=16
%mem=2GB
%chk=mol_319_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73200       -0.42620        0.28350
O          1.68770        0.48000        0.07450
C          0.35340        0.09760        0.01900
C          0.03090       -1.23090        0.17590
C         -1.30250       -1.59650        0.11830
C         -2.32140       -0.68710       -0.08940
C         -1.97660        0.64760       -0.24520
C         -0.65680        1.02110       -0.18990
N         -0.29550        2.38780       -0.34840
H          2.55590       -1.34010       -0.35520
H          2.72680       -0.81820        1.33470
H          3.72810       -0.00260        0.07850
H          0.76290       -2.00880        0.34420
H         -1.57500       -2.63410        0.23910
H         -3.36260       -1.00360       -0.13010
H         -2.77850        1.34840       -0.40660
H         -0.01130        2.68330       -1.30910
H         -0.29770        3.08220        0.40630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

