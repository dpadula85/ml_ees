%nproc=16
%mem=2GB
%chk=mol_319_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.77180        0.13490        0.04790
O          1.53600        0.80830       -0.10220
C          0.33350        0.11930       -0.00810
C          0.24040       -1.22900        0.23050
C         -0.98060       -1.88790        0.31870
C         -2.13740       -1.15500        0.15990
C         -2.06730        0.20070       -0.08070
C         -0.83480        0.85140       -0.16720
N         -0.78130        2.24830       -0.41580
H          2.74960       -0.80980       -0.52750
H          3.04790       -0.02840        1.10600
H          3.58190        0.78200       -0.37750
H          1.15790       -1.79470        0.35360
H         -1.00910       -2.95180        0.50800
H         -3.09910       -1.64030        0.22380
H         -2.96710        0.78680       -0.20740
H         -1.18230        2.62480       -1.29910
H         -0.36000        2.94010        0.23700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

