%nproc=16
%mem=2GB
%chk=mol_319_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.72500       -0.48970        0.14940
O          1.67810        0.46180        0.08000
C          0.33530        0.06560        0.01740
C         -0.02990       -1.26290        0.02040
C         -1.35730       -1.66690       -0.04130
C         -2.30660       -0.66830       -0.10650
C         -1.96470        0.68290       -0.11110
C         -0.63460        1.04350       -0.04860
N         -0.26310        2.39600       -0.05170
H          2.80750       -1.08890       -0.77620
H          2.58660       -1.17570        1.00190
H          3.66520        0.11660        0.30730
H          0.74340       -2.03830        0.07290
H         -1.65700       -2.70660       -0.03970
H         -3.35600       -0.91510       -0.15640
H         -2.73880        1.46060       -0.16360
H          0.20130        2.77020       -0.92560
H         -0.43440        3.01540        0.77150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

