%nproc=16
%mem=2GB
%chk=mol_319_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.42130       -1.25500        0.15930
O          1.74430       -0.03660        0.02450
C          0.35930       -0.04680        0.00570
C         -0.37140       -1.21150        0.11500
C         -1.75310       -1.13380        0.08730
C         -2.38890        0.09210       -0.04780
C         -1.66120        1.25500       -0.15690
C         -0.28880        1.17130       -0.12870
N          0.47270        2.36000       -0.23990
H          1.98480       -1.97340       -0.56030
H          3.51650       -1.16080        0.03180
H          2.28130       -1.72020        1.17210
H          0.11080       -2.17240        0.22070
H         -2.33240       -2.04170        0.17240
H         -3.44940        0.10180       -0.06390
H         -2.17510        2.19240       -0.26040
H          1.29930        2.47770        0.40740
H          0.23030        3.10190       -0.93840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

