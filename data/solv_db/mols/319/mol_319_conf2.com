%nproc=16
%mem=2GB
%chk=mol_319_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73530       -0.43810        0.10070
O          1.66430        0.45920       -0.10670
C          0.34070        0.07100       -0.01530
C          0.00340       -1.22790        0.28660
C         -1.32760       -1.56310        0.36560
C         -2.35540       -0.66820        0.15880
C         -2.00920        0.62870       -0.14260
C         -0.66910        0.98180       -0.22580
N         -0.28590        2.30530       -0.53340
H          2.56740       -1.43210       -0.32600
H          3.02660       -0.50970        1.17890
H          3.61280        0.00680       -0.44840
H          0.77120       -1.99080        0.46320
H         -1.60400       -2.60030        0.60660
H         -3.41070       -0.98120        0.23240
H         -2.78440        1.37000       -0.31390
H          0.48390        2.74640        0.00060
H         -0.75940        2.84220       -1.28140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_319_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

