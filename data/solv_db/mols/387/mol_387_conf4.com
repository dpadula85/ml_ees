%nproc=16
%mem=2GB
%chk=mol_387_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.74570        1.15040       -0.07780
C         -0.63130        1.21670       -0.09390
C         -1.40150        0.06610       -0.02110
C         -0.71750       -1.12690        0.06690
C          0.66090       -1.19060        0.08280
C          1.42660       -0.04650        0.01030
N          2.85500       -0.08660        0.02490
N         -2.81510        0.10450       -0.03550
O         -3.51360       -1.06550        0.03920
O         -3.43030        1.17340       -0.11410
H          1.32840        2.05550       -0.13530
H         -1.17730        2.16520       -0.16390
H         -1.31910       -2.04780        0.12550
H          1.18910       -2.13060        0.15210
H          3.34260       -0.45950        0.88600
H          3.45750        0.22240       -0.74600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

