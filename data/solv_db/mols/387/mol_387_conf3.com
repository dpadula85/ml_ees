%nproc=16
%mem=2GB
%chk=mol_387_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.73340        1.16580       -0.06090
C         -0.64440        1.20440       -0.01040
C         -1.39200        0.05070        0.05250
C         -0.70590       -1.14460        0.06310
C          0.67120       -1.18110        0.01260
C          1.43040       -0.02990       -0.05070
N          2.84710       -0.06520       -0.10370
N         -2.82100        0.07280        0.10600
O         -3.55430       -1.08340        0.16930
O         -3.43880        1.15070        0.09610
H          1.33640        2.06860       -0.11070
H         -1.16850        2.14840       -0.01940
H         -1.29130       -2.05210        0.11240
H          1.22610       -2.11560        0.02040
H          3.31230       -0.75750       -0.72450
H          3.45920        0.56790        0.44780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

