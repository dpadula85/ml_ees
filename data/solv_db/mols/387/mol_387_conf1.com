%nproc=16
%mem=2GB
%chk=mol_387_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.60600       -0.91590       -0.86060
C         -0.76430       -0.77290       -0.83860
C         -1.39890        0.13250        0.01030
C         -0.57360        0.87740        0.82740
C          0.79430        0.73200        0.80270
C          1.41520       -0.16110       -0.03500
N          2.83510       -0.28740       -0.03370
N         -2.80130        0.28680        0.04040
O         -3.34150        1.19690        0.90150
O         -3.55090       -0.37590       -0.68740
H          1.06550       -1.62870       -1.53260
H         -1.42640       -1.35660       -1.48010
H         -1.05380        1.58850        1.49680
H          1.42260        1.32990        1.45500
H          3.33080       -1.20950       -0.03400
H          3.44130        0.56400       -0.03230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

