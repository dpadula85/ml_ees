%nproc=16
%mem=2GB
%chk=mol_387_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.63920       -0.46400       -1.12890
C         -0.73320       -0.49870       -1.03180
C         -1.39020       -0.03610        0.08900
C         -0.58540        0.46820        1.11710
C          0.78650        0.50420        1.02300
C          1.42590        0.03980       -0.09950
N          2.84680        0.05810       -0.23970
N         -2.79920       -0.05820        0.21870
O         -3.45710        0.39940        1.32770
O         -3.48360       -0.51340       -0.71370
H          1.12670       -0.84170       -2.03770
H         -1.36080       -0.89730       -1.84550
H         -1.11670        0.83170        1.99960
H          1.37140        0.89940        1.83440
H          3.33400        0.31860       -1.11830
H          3.39580       -0.21010        0.60560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_387_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

