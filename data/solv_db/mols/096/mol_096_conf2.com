%nproc=16
%mem=2GB
%chk=mol_096_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.23680        0.18590        0.30390
C         -0.13660        0.44280       -0.30140
C         -1.02180       -0.76540       -0.24150
I         -0.96850        2.05880        0.83800
H          1.79480        1.15820        0.19300
H          1.73510       -0.64730       -0.20720
H          1.09290       -0.01310        1.37520
H         -0.00620        0.74770       -1.34360
H         -1.03780       -1.34300       -1.19590
H         -0.63690       -1.42360        0.56280
H         -2.05170       -0.40090        0.01660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

