%nproc=16
%mem=2GB
%chk=mol_096_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21020       -0.01690       -0.30950
C          0.00800        0.37950        0.50480
C          1.23220       -0.11910       -0.25370
I         -0.11920       -0.58720        2.41000
H         -1.86070        0.86530       -0.46790
H         -1.73730       -0.86440        0.17940
H         -0.92090       -0.38970       -1.32480
H          0.04420        1.46850        0.68380
H          0.96390       -0.34240       -1.32270
H          2.02400        0.66020       -0.31570
H          1.57600       -1.05400        0.21650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

