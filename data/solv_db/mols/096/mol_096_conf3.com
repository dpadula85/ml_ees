%nproc=16
%mem=2GB
%chk=mol_096_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.14730       -0.39890        0.27720
C          0.09360        0.55000       -0.25640
C         -1.23640       -0.14130       -0.09840
I          0.17440        2.46330        0.58400
H          2.14260        0.04490        0.34650
H          1.21950       -1.22680       -0.47950
H          0.84670       -0.80340        1.25690
H          0.25670        0.64200       -1.36960
H         -1.16980       -1.08180       -0.72020
H         -1.41170       -0.50860        0.93940
H         -2.06290        0.46080       -0.47990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

