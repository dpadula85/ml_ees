%nproc=16
%mem=2GB
%chk=mol_096_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.23880        0.14220        0.07790
C         -0.14930        0.42650       -0.41560
C         -1.07090       -0.63130        0.17990
I         -0.83160        2.36380        0.08280
H          1.28660       -0.78000        0.70700
H          1.63980        0.97490        0.68400
H          1.88920       -0.07980       -0.80720
H         -0.21700        0.31270       -1.52470
H         -0.97090       -1.53320       -0.45140
H         -0.70650       -0.93360        1.19800
H         -2.10820       -0.26200        0.26930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

