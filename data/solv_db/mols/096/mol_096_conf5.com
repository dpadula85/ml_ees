%nproc=16
%mem=2GB
%chk=mol_096_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.22990       -0.38940        0.03760
C          0.02800        0.30840       -0.40320
C          1.24030       -0.08410        0.38150
I         -0.27340        2.40630       -0.39360
H         -1.09390       -1.49120        0.05190
H         -1.63820        0.01040        0.97770
H         -1.99820       -0.19290       -0.74640
H          0.21010       -0.00600       -1.46510
H          2.13390        0.19760       -0.24230
H          1.27950       -1.19020        0.45480
H          1.34160        0.43130        1.34710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_096_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

