%nproc=16
%mem=2GB
%chk=mol_074_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.10470        0.03880        0.18720
C         -1.37160        1.20730        0.18660
C          0.01240        1.20470        0.03030
C          0.69280        0.01650       -0.12990
C         -0.04190       -1.14770       -0.12880
C         -1.42990       -1.15350        0.02730
C          2.17020       -0.03870       -0.30030
Br         3.02630       -0.19110        1.44830
H         -3.17300        0.08600        0.31110
H         -1.86560        2.15930        0.30930
H          0.59910        2.11440        0.02800
H          0.50370       -2.09520       -0.25700
H         -1.98950       -2.09080        0.02460
H          2.56630        0.84500       -0.84660
H          2.40520       -0.95500       -0.89030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_074_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_074_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_074_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

