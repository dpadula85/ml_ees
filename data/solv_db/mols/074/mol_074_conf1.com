%nproc=16
%mem=2GB
%chk=mol_074_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09450       -0.09460       -0.21000
C          1.43800        1.11530       -0.22030
C          0.05880        1.17650       -0.12060
C         -0.71060        0.03680       -0.00760
C         -0.04210       -1.17130        0.00170
C          1.34430       -1.24590       -0.09790
C         -2.20390        0.12020        0.09950
Br        -2.64130        0.24330        2.00050
H          3.17110       -0.18200       -0.28580
H          2.04430        2.00710       -0.30900
H         -0.46690        2.12820       -0.12790
H         -0.65790       -2.06460        0.09120
H          1.81920       -2.22680       -0.08530
H         -2.61580        0.97340       -0.45570
H         -2.63150       -0.81550       -0.27270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_074_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_074_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_074_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

