%nproc=16
%mem=2GB
%chk=mol_001_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11570       -0.20920        0.14000
C         -0.71290        0.26550       -0.16760
C          0.31810       -0.36720        0.74060
C          1.66850        0.18710        0.33200
O          1.96270       -0.11440       -0.99610
H         -2.19430       -1.31130        0.19660
H         -2.53080        0.29740        1.04700
H         -2.75540        0.14370       -0.70490
H         -0.71270        1.36880       -0.05250
H         -0.46150        0.01100       -1.21060
H          0.35160       -1.45750        0.53970
H          0.11420       -0.14890        1.80720
H          2.49090       -0.15250        0.96790
H          1.66110        1.31390        0.38770
H          2.91640        0.17340       -1.13030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

