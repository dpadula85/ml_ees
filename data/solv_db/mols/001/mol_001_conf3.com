%nproc=16
%mem=2GB
%chk=mol_001_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.59230        0.12910        0.68830
C         -0.97220        0.07260       -0.65890
C          0.40370       -0.50450       -0.69480
C          1.35930        0.30340        0.16320
O          2.63270       -0.27680        0.09640
H         -2.53540       -0.47290        0.75190
H         -0.95410       -0.19310        1.52000
H         -1.91920        1.17960        0.93520
H         -1.61910       -0.58670       -1.30630
H         -1.01640        1.07210       -1.12960
H          0.76540       -0.41950       -1.75560
H          0.36270       -1.55800       -0.41030
H          1.35370        1.34540       -0.19260
H          1.02510        0.20480        1.22710
H          2.70600       -1.01200        0.76600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

