%nproc=16
%mem=2GB
%chk=mol_001_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04300        0.28870        0.18410
C         -0.64410        0.46540        0.75570
C          0.24130       -0.51610        0.04010
C          1.67440       -0.45930        0.50330
O          2.24820        0.79820        0.30330
H         -2.21200       -0.72590       -0.19170
H         -2.09540        0.96550       -0.70820
H         -2.81500        0.50700        0.92910
H         -0.67080        0.30050        1.85590
H         -0.30530        1.52160        0.58140
H          0.12530       -0.35120       -1.03850
H         -0.13480       -1.52800        0.26400
H          2.22820       -1.23050       -0.08130
H          1.70420       -0.78740        1.55260
H          2.69870        0.75140       -0.58640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

