%nproc=16
%mem=2GB
%chk=mol_001_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.68160        0.64870        0.10930
C          0.93770       -0.64100        0.00330
C         -0.47450       -0.58320        0.51160
C         -1.31510        0.42700       -0.23760
O         -2.60000        0.41390        0.29560
H          1.67900        1.06710        1.14030
H          2.74690        0.44170       -0.12880
H          1.34890        1.40260       -0.64220
H          1.48840       -1.41820        0.53950
H          0.90620       -0.92900       -1.06670
H         -0.93190       -1.57690        0.32360
H         -0.52650       -0.39620        1.60030
H         -0.81910        1.42000       -0.07790
H         -1.38110        0.17000       -1.30120
H         -2.74040       -0.44650        0.77040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

