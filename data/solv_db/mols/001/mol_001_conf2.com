%nproc=16
%mem=2GB
%chk=mol_001_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.95310        0.58360       -0.08030
C         -0.65870       -0.03150       -0.48600
C          0.34330       -0.10720        0.65170
C          1.62330       -0.74380        0.14700
O          2.10800        0.06160       -0.88400
H         -2.05360        0.69200        1.03560
H         -2.08470        1.60540       -0.53440
H         -2.79260       -0.01970       -0.50350
H         -0.83220       -1.09060       -0.80930
H         -0.24890        0.51520       -1.34080
H          0.53680        0.88990        1.10000
H         -0.02840       -0.75680        1.46700
H          2.38200       -0.80950        0.93220
H          1.38200       -1.75590       -0.22050
H          2.27680        0.96720       -0.47460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_001_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

