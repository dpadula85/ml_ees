%nproc=16
%mem=2GB
%chk=mol_407_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.35490        1.21280       -0.48720
C          0.76460        1.00290        0.53870
C          1.34840       -0.36450        0.36300
N          0.41870       -1.42450        0.32090
C         -0.66990       -1.17150       -0.57110
C         -1.37750        0.11750       -0.15620
H         -0.79470        2.20480       -0.43170
H          0.09790        1.00080       -1.48790
H          0.23260        1.00080        1.53210
H          1.54210        1.76100        0.46670
H          2.07020       -0.51590        1.21840
H          1.95620       -0.37910       -0.57270
H          0.16520       -1.86390        1.21440
H         -1.39150       -1.97570       -0.65660
H         -0.22700       -0.99130       -1.58730
H         -2.32520        0.26690       -0.66580
H         -1.45530        0.11880        0.96230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

