%nproc=16
%mem=2GB
%chk=mol_407_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.89460       -0.96040       -0.38540
C          0.47130       -1.28250        0.23760
C          1.43390       -0.18040       -0.01970
N          0.94940        1.12740        0.05690
C         -0.38100        1.39590       -0.27480
C         -1.30510        0.33630        0.32820
H         -0.73150       -0.73940       -1.47090
H         -1.61240       -1.76310       -0.23330
H          0.79490       -2.22570       -0.21420
H          0.26740       -1.47570        1.30620
H          1.92530       -0.39230       -1.01460
H          2.27500       -0.29230        0.72730
H          1.45520        1.80320        0.63780
H         -0.66830        2.40300        0.09260
H         -0.60280        1.39150       -1.37800
H         -2.35950        0.56840        0.19350
H         -1.01730        0.28610        1.41070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

