%nproc=16
%mem=2GB
%chk=mol_407_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.64210        1.08370       -0.66430
C         -0.56520        1.21940        0.26350
C         -1.40230       -0.03380        0.03670
N         -0.63700       -1.22180        0.32290
C          0.54970       -1.32770       -0.48090
C          1.42720       -0.10980       -0.16410
H          1.23120        2.00260       -0.72490
H          0.20400        0.87160       -1.67610
H         -1.16530        2.10380        0.01200
H         -0.24370        1.22820        1.33070
H         -2.26240        0.02450        0.70180
H         -1.74690       -0.07110       -1.00540
H         -1.25120       -2.03940        0.06370
H          1.05380       -2.26670       -0.25830
H          0.27280       -1.20510       -1.55730
H          2.42340       -0.19120       -0.59910
H          1.46990       -0.06720        0.95260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

