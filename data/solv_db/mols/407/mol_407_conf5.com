%nproc=16
%mem=2GB
%chk=mol_407_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.01020        0.92160       -0.01640
C          0.37860        1.33850       -0.28460
C          1.39870        0.23600       -0.22030
N          0.94610       -0.66890        0.84810
C         -0.13810       -1.41920        0.25610
C         -1.29460       -0.52370        0.02350
H         -1.67050        1.38800       -0.80580
H         -1.36880        1.38930        0.94690
H          0.71810        2.10210        0.47740
H          0.50930        1.87080       -1.26700
H          2.41410        0.60780       -0.04010
H          1.37840       -0.28220       -1.20250
H          1.70480       -1.24080        1.22860
H         -0.32910       -2.28690        0.89200
H          0.22810       -1.82120       -0.72620
H         -2.06380       -0.75180        0.81970
H         -1.80100       -0.85940       -0.92940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

