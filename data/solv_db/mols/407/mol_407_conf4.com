%nproc=16
%mem=2GB
%chk=mol_407_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.56940        1.19890       -0.33410
C         -0.57990        1.04310        0.67130
C         -1.45940       -0.00870        0.06930
N         -0.80360       -1.22430       -0.07800
C          0.57960       -1.29420       -0.24240
C          1.37220       -0.06260       -0.20290
H          1.13760        2.10310       -0.17040
H          0.10810        1.18970       -1.36370
H         -1.10000        1.99370        0.84070
H         -0.17320        0.62650        1.63750
H         -2.33360       -0.13670        0.77080
H         -1.91850        0.42270       -0.85770
H         -1.31910       -2.02350       -0.41850
H          0.99820       -2.00680        0.53160
H          0.80090       -1.82480       -1.21710
H          2.17520       -0.02540       -0.99030
H          1.94630        0.02960        0.76610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_407_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

