%nproc=16
%mem=2GB
%chk=mol_129_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45180       -0.07880        0.00090
C         -0.91670        1.18370        0.03570
C          0.44820        1.30980        0.03620
C          1.26930        0.17690        0.00220
C          0.72700       -1.08740       -0.03260
C         -0.64600       -1.21410       -0.03310
Cl         1.78470       -2.49730       -0.07510
Cl         3.01770        0.31870        0.00260
H         -2.52460       -0.20370       -0.00020
H         -1.55670        2.03380        0.06160
H          0.92310        2.26650        0.06220
H         -1.07410       -2.20800       -0.06030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_129_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_129_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_129_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

