%nproc=16
%mem=2GB
%chk=mol_153_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06660       -0.89530        0.02990
C          2.16430        0.46230        0.15860
C          1.04130        1.29300        0.17360
C         -0.21070        0.72410        0.05440
C         -0.32780       -0.65850       -0.07760
C          0.79920       -1.45500       -0.08930
C         -1.77240       -0.94490       -0.18150
O         -2.20000       -2.12300       -0.31170
N         -2.51000        0.24450       -0.11300
C         -1.57360        1.29940        0.03340
O         -1.81420        2.52350        0.13180
H          2.91410       -1.57550        0.01410
H          3.14730        0.91900        0.25280
H          1.13150        2.37170        0.27670
H          0.68460       -2.51450       -0.19180
H         -3.54030        0.32930       -0.16060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_153_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_153_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_153_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

