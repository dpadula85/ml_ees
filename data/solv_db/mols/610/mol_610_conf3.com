%nproc=16
%mem=2GB
%chk=mol_610_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.25170       -0.25440       -0.03390
C          0.03260        0.53920       -0.24210
C          1.23140       -0.29930        0.10290
Br        -0.04860        2.15390        0.82850
H         -1.04650       -0.92480        0.83990
H         -1.46680       -0.87450       -0.92950
H         -2.07840        0.43210        0.19430
H          0.09350        0.87310       -1.30350
H          1.52910       -0.16870        1.16290
H          2.05590       -0.09610       -0.60230
H          0.94950       -1.38050       -0.01720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

