%nproc=16
%mem=2GB
%chk=mol_610_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.19320       -0.30950        0.33600
C          0.05900        0.57030       -0.16550
C         -1.21410       -0.23380       -0.29120
Br        -0.12360        2.05830        1.03870
H          0.88350       -1.37000        0.18050
H          1.38450       -0.17510        1.41190
H          2.10050       -0.13020       -0.27420
H          0.32840        0.98540       -1.16790
H         -2.04060        0.18100        0.33790
H         -1.54310       -0.26280       -1.36720
H         -1.02770       -1.31370       -0.03910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

