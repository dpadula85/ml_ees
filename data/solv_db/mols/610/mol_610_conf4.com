%nproc=16
%mem=2GB
%chk=mol_610_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.22090       -0.36510        0.17080
C          0.00220        0.54650        0.14490
C         -1.25370       -0.19970       -0.22130
Br         0.29610        2.06950       -1.00580
H          1.77810       -0.35850       -0.77330
H          0.84230       -1.38160        0.38670
H          1.91310       -0.10580        0.99220
H         -0.07740        0.95680        1.19040
H         -1.69580        0.12750       -1.17320
H         -2.02760       -0.00270        0.56510
H         -0.99830       -1.28690       -0.27640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

