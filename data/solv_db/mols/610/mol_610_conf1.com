%nproc=16
%mem=2GB
%chk=mol_610_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.20340       -0.17770       -0.15350
C         -0.00530        0.54680        0.40700
C          1.20280       -0.35170        0.20300
Br         0.23500        2.29590       -0.30490
H         -1.03880       -0.61410       -1.14280
H         -1.40090       -1.03660        0.54390
H         -2.06900        0.50130       -0.08410
H         -0.18000        0.63270        1.51170
H          0.87690       -1.37130        0.54810
H          1.53940       -0.34500       -0.84040
H          2.04340       -0.08030        0.88650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

