%nproc=16
%mem=2GB
%chk=mol_610_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17250       -0.49030       -0.09840
C         -0.06840        0.55500       -0.19070
C          1.26790       -0.09770        0.08940
Br        -0.45700        1.90060        1.13960
H         -0.76660       -1.50190       -0.26040
H         -1.89660       -0.22960       -0.91130
H         -1.68170       -0.43180        0.88860
H         -0.02890        1.03420       -1.17790
H          1.32130       -1.14190       -0.23440
H          2.07270        0.48390       -0.42890
H          1.40960       -0.08050        1.18440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_610_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

