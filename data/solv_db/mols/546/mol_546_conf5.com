%nproc=16
%mem=2GB
%chk=mol_546_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.92520       -2.12370        0.46010
C         -1.07800       -0.64520        0.24180
N         -0.03470        0.21100        0.60310
C         -0.53430        1.57960        0.56960
C         -0.86210        2.00720       -0.79780
C          1.21840        0.18770       -0.01360
C          2.11960       -0.98630        0.22280
H         -1.65370       -2.48290        1.26870
H          0.05690       -2.35870        0.83620
H         -1.21460       -2.71840       -0.43890
H         -1.98710       -0.34330        0.85360
H         -1.43600       -0.48930       -0.81750
H          0.33570        2.19870        0.93000
H         -1.38810        1.65040        1.28360
H         -0.15950        1.67760       -1.55360
H         -1.87470        1.72210       -1.16580
H         -0.85060        3.13280       -0.85220
H          1.79790        1.08060        0.39720
H          1.22270        0.40770       -1.11870
H          3.17420       -0.63070        0.00550
H          2.11240       -1.26920        1.27230
H          1.96070       -1.80770       -0.48780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

