%nproc=16
%mem=2GB
%chk=mol_546_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.53560        2.25800        0.42640
C         -0.59490        1.24460       -0.69400
N          0.04750        0.01750       -0.39260
C          1.45380        0.09980       -0.33660
C          2.07590       -1.10720        0.29560
C         -0.60200       -0.91990        0.38580
C         -1.83870       -1.53610       -0.21810
H         -0.45840        3.30070        0.05880
H          0.38310        2.05410        1.00680
H         -1.44110        2.18200        1.07490
H         -1.62930        1.14460       -1.07490
H         -0.01890        1.71210       -1.53450
H          1.80660        0.16650       -1.43010
H          1.76600        1.06230        0.13690
H          3.21010       -0.94960        0.27040
H          1.90210       -2.02050       -0.28600
H          1.76880       -1.20230        1.37120
H          0.08110       -1.80590        0.57580
H         -0.87090       -0.53950        1.38720
H         -2.73820       -1.12910        0.30440
H         -1.82960       -2.62620       -0.02810
H         -1.93750       -1.40590       -1.29910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

