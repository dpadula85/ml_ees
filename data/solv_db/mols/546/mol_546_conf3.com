%nproc=16
%mem=2GB
%chk=mol_546_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.21200       -1.49530       -0.55620
C         -0.76730       -1.49580       -0.20100
N         -0.09900       -0.23300       -0.63800
C         -0.59010        0.83740        0.17790
C         -0.15260        2.15230       -0.47910
C          1.29820       -0.53150       -0.43670
C          2.30370        0.48900       -0.77080
H         -2.63440       -0.51000       -0.84130
H         -2.41300       -2.17260       -1.44270
H         -2.80980       -1.94290        0.29050
H         -0.60500       -1.66460        0.87760
H         -0.22770       -2.27650       -0.79190
H         -0.17670        0.78220        1.21990
H         -1.67980        0.81100        0.30050
H          0.71760        2.52180        0.05910
H          0.02900        2.03360       -1.56030
H         -0.95830        2.92900       -0.39710
H          1.47990       -0.91540        0.58580
H          1.47720       -1.44120       -1.10230
H          2.59740        1.13630        0.08380
H          2.14690        1.05680       -1.70290
H          3.27580       -0.07050       -0.97320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

