%nproc=16
%mem=2GB
%chk=mol_546_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.33530        2.11440       -0.31860
C         -0.23740        1.03820        0.57640
N         -0.10830       -0.26850        0.11020
C          1.14270       -0.85130        0.13180
C          2.20030       -0.33660       -0.81590
C         -1.16110       -1.20210        0.35260
C         -2.41620       -0.86920       -0.43680
H          0.51750        1.72190       -1.34540
H         -0.39020        2.99090       -0.39200
H          1.27000        2.57000        0.06520
H         -1.28410        1.31880        0.89610
H          0.33280        1.18060        1.56320
H          1.14640       -1.98170        0.06350
H          1.59300       -0.67340        1.16540
H          2.49730        0.68960       -0.65410
H          3.12900       -0.95410       -0.54040
H          1.88100       -0.60300       -1.84860
H         -0.81060       -2.18090       -0.02620
H         -1.42620       -1.33380        1.42200
H         -2.15080       -0.44530       -1.42340
H         -3.01600       -1.78700       -0.56060
H         -3.04450       -0.13740        0.12530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

