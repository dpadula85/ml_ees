%nproc=16
%mem=2GB
%chk=mol_546_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.94500        1.98820       -0.45520
C          0.18640        1.26990        0.18020
N          0.01960       -0.07140        0.55500
C          1.17120       -0.66250        1.12780
C          2.32760       -0.86000        0.17110
C         -0.66580       -0.95550       -0.29760
C         -2.13600       -0.76970       -0.44870
H         -0.52680        2.99500       -0.77490
H         -1.77140        2.25420        0.22700
H         -1.26440        1.53240       -1.41330
H          1.03120        1.31330       -0.58360
H          0.49860        1.87810        1.07230
H          1.58730       -0.15170        2.02780
H          0.91920       -1.69530        1.51450
H          2.72510        0.08140       -0.23240
H          2.07370       -1.57220       -0.64620
H          3.16170       -1.31480        0.78320
H         -0.20590       -0.97010       -1.34120
H         -0.48980       -2.02320        0.04290
H         -2.59280        0.02490        0.15800
H         -2.69290       -1.71140       -0.14780
H         -2.41040       -0.57930       -1.51880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_546_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

