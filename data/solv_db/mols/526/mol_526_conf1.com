%nproc=16
%mem=2GB
%chk=mol_526_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.73870        1.39960        0.06270
C          0.58220        1.14600       -0.24030
C          1.08350       -0.13680       -0.32190
C          0.20600       -1.19200       -0.08600
C         -1.11880       -0.98380        0.21920
C         -1.57680        0.30740        0.29030
Cl        -2.17970       -2.34390        0.50660
O          2.40910       -0.40800       -0.62560
H         -1.07160        2.42600        0.11190
H          1.28270        1.96630       -0.42790
H          0.63640       -2.18870       -0.15900
H         -2.60970        0.48930        0.52770
H          3.09520       -0.48150        0.14220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

