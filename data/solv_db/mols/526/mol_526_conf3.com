%nproc=16
%mem=2GB
%chk=mol_526_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.06980        1.55680        0.00340
C         -1.03580        0.73550       -0.17140
C         -0.93070       -0.64050       -0.21330
C          0.32880       -1.18720       -0.07370
C          1.42910       -0.37180        0.10030
C          1.32930        1.00500        0.14280
Cl         3.01650       -1.08200        0.27560
O         -2.07470       -1.42470       -0.39160
H         -0.05880        2.63850        0.03040
H         -2.04630        1.15030       -0.28450
H          0.39620       -2.26370       -0.10840
H          2.20310        1.64520        0.28070
H         -2.62640       -1.76130        0.40960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

