%nproc=16
%mem=2GB
%chk=mol_526_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.98210       -1.26820        0.32860
C          0.38370       -1.29260        0.29250
C          1.14880       -0.14480        0.13620
C          0.48770        1.03650        0.01650
C         -0.89750        1.08560        0.05020
C         -1.63230       -0.06190        0.20570
Cl        -1.75800        2.61700       -0.10520
O          2.52980       -0.17530        0.10040
H         -1.51800       -2.21130        0.45440
H          0.93780       -2.22980        0.38650
H          0.98260        1.99640       -0.11040
H         -2.72650       -0.02320        0.23240
H          3.04400        0.67150       -0.01340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_526_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

