%nproc=16
%mem=2GB
%chk=mol_364_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31310        1.31760        0.24910
O         -1.30700        0.92790       -0.69480
C         -0.38600       -0.06480       -0.40750
C         -0.71920       -1.36450       -0.73810
C          0.20820       -2.36620       -0.44910
C          1.41510       -2.09940        0.14150
C          1.73520       -0.79950        0.46620
C          0.81670        0.21070        0.18240
C          1.16040        1.56340        0.52480
O          0.90070        2.11180        1.63560
O          1.83270        2.35030       -0.42730
Cl         3.27390       -0.45800        1.21950
Cl        -2.25920       -1.71570       -1.49290
H         -2.17370        0.68220        1.15130
H         -2.23740        2.40660        0.47830
H         -3.33070        1.12430       -0.16670
H         -0.05710       -3.37060       -0.70840
H          2.11100       -2.90040        0.35180
H          1.32940        2.44450       -1.31580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

