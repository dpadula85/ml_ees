%nproc=16
%mem=2GB
%chk=mol_364_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.81900        0.21160        0.03200
O          1.64030       -0.13240        0.57860
C          0.41010       -0.48920        0.19750
C         -0.02760       -1.78530       -0.03830
C         -1.31320       -2.07210       -0.42880
C         -2.23690       -1.07540       -0.60630
C         -1.83920        0.22210       -0.38180
C         -0.54710        0.49420        0.00930
C         -0.21040        1.90900        0.22590
O          0.22630        2.64460       -0.66640
O         -0.39570        2.43310        1.49710
Cl        -3.03190        1.50560       -0.61090
Cl         1.13110       -3.06460        0.18120
H          3.32370       -0.49850       -0.64260
H          2.70600        1.14620       -0.56200
H          3.55940        0.46060        0.83430
H         -1.62390       -3.09760       -0.60510
H         -3.25900       -1.27940       -0.91470
H         -1.33090        2.46760        1.90130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

