%nproc=16
%mem=2GB
%chk=mol_364_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.60320        0.48390        0.22970
O         -1.66070        0.14310       -0.74100
C         -0.41330       -0.41520       -0.45790
C         -0.25280       -1.78800       -0.38920
C          1.01010       -2.28490       -0.10430
C          2.09530       -1.46080        0.10950
C          1.93350       -0.08780        0.04050
C          0.67950        0.41310       -0.24260
C          0.53190        1.85860       -0.31020
O          0.69990        2.43140       -1.40000
O          0.20620        2.56700        0.84670
Cl         3.30940        0.98950        0.31060
Cl        -1.62040       -2.86850       -0.65770
H         -2.12370        0.50310        1.23110
H         -3.44530       -0.21990        0.15900
H         -3.02490        1.50020        0.06560
H          1.10260       -3.36810       -0.05760
H          3.06220       -1.89790        0.32910
H          0.51380        3.50110        1.03870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

