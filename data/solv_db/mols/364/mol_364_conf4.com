%nproc=16
%mem=2GB
%chk=mol_364_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.48980        0.84720       -0.03560
O         -1.55140        0.27110        0.82740
C         -0.41090       -0.38690        0.33770
C         -0.47690       -1.73620        0.07580
C          0.62890       -2.39500       -0.40340
C          1.79190       -1.68090       -0.61320
C          1.86310       -0.30770       -0.34790
C          0.74270        0.32330        0.13000
C          0.77850        1.73860        0.41480
O          1.11540        2.13980        1.58000
O          0.46880        2.73350       -0.49610
Cl         3.37550        0.52430       -0.64060
Cl        -1.95320       -2.60930        0.34910
H         -2.29600        1.92410       -0.16320
H         -2.48590        0.38340       -1.04160
H         -3.50370        0.66390        0.42610
H          0.60990       -3.46770       -0.62180
H          2.65210       -2.21360       -0.99030
H          1.14100        3.24800       -1.03440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

