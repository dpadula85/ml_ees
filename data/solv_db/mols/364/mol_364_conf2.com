%nproc=16
%mem=2GB
%chk=mol_364_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93660       -1.70720       -0.30640
O         -0.96590       -1.29550        0.63110
C         -0.35470       -0.07960        0.46030
C         -0.90600        1.03380        1.02570
C         -0.28740        2.24970        0.85110
C          0.88790        2.32090        0.10170
C          1.46100        1.20800       -0.47770
C          0.79910        0.00260       -0.27280
C          1.35940       -1.19430       -0.85100
O          1.05180       -1.59000       -1.99330
O          2.26640       -1.92140       -0.10890
Cl         2.93450        1.27160       -1.42220
Cl        -2.37200        0.91460        1.95520
H         -1.99120       -2.81000       -0.39580
H         -2.91860       -1.27620        0.04240
H         -1.72530       -1.20070       -1.26680
H         -0.69400        3.16800        1.28620
H          1.36790        3.29650       -0.02790
H          2.02360       -2.39080        0.76910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_364_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

