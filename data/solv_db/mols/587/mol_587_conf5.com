%nproc=16
%mem=2GB
%chk=mol_587_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.39470       -0.89100        0.69780
N          3.40810       -0.82190       -0.35850
C          2.19810       -0.12660       -0.17090
O          1.94490        0.45840        0.93620
O          1.22750       -0.02980       -1.13990
C          0.03510        0.66440       -0.93310
C         -0.11520        2.00110       -1.21800
C         -1.31960        2.66910       -1.00080
C         -2.40020        1.98130       -0.48530
C         -2.27640        0.63720       -0.19010
C         -1.05750       -0.00450       -0.41850
C         -0.93130       -1.34760       -0.12430
C         -2.01500       -2.05790        0.39780
C         -3.18350       -1.39030        0.60370
C         -3.35840       -0.05520        0.32700
H          4.75310       -1.94680        0.78410
H          3.97150       -0.52030        1.64250
H          5.24600       -0.21580        0.40980
H          3.60080       -1.28940       -1.25500
H          0.74830        2.51470       -1.62100
H         -1.37210        3.73190       -1.24870
H         -3.34540        2.47720       -0.30770
H          0.01620       -1.85030       -0.30100
H         -1.83860       -3.09320        0.59940
H         -4.02260       -1.94400        1.00880
H         -4.30860        0.44930        0.50400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

