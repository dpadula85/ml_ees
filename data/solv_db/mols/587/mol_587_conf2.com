%nproc=16
%mem=2GB
%chk=mol_587_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.46450        0.40660        0.69880
N          3.50030        0.24590       -0.35210
C          2.19540       -0.20460       -0.01790
O          1.89080       -0.45730        1.17310
O          1.21530       -0.37780       -1.01210
C         -0.04960       -0.81750       -0.66490
C         -0.36730       -2.14880       -0.62960
C         -1.63320       -2.61890       -0.28330
C         -2.57180       -1.67930        0.02830
C         -2.28770       -0.32990        0.00340
C         -1.01210        0.11040       -0.34700
C         -0.79600        1.46390       -0.35190
C         -1.75200        2.38690       -0.03600
C         -3.00600        1.92560        0.30720
C         -3.27600        0.56540        0.32750
H          5.05480       -0.48280        0.91770
H          5.18490        1.20970        0.38070
H          3.93030        0.82550        1.58650
H          3.73390        0.45340       -1.36880
H          0.41330       -2.86980       -0.88610
H         -1.84710       -3.69040       -0.26700
H         -3.57450       -2.03840        0.30310
H          0.19080        1.81180       -0.62270
H         -1.56030        3.46960       -0.04540
H         -3.78360        2.64620        0.56290
H         -4.25710        0.19480        0.59560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

