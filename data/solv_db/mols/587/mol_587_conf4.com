%nproc=16
%mem=2GB
%chk=mol_587_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.45700        0.52100       -0.07960
N          3.21350        1.23200        0.14170
C          2.05030        0.48190        0.43130
O          2.02150       -0.76140        0.51010
O          0.86560        1.17170        0.64140
C         -0.29500        0.46250        0.92820
C         -0.63730        0.17010        2.23090
C         -1.79200       -0.53150        2.45650
C         -2.59050       -0.93380        1.39880
C         -2.23560       -0.63320        0.08510
C         -1.07490        0.07300       -0.15640
C         -0.69580        0.38740       -1.45940
C         -1.47660       -0.00330       -2.52840
C         -2.63990       -0.71000       -2.30300
C         -3.00520       -1.01670       -0.99360
H          4.52970       -0.33540        0.65420
H          4.41820        0.10680       -1.12320
H          5.30830        1.19320        0.08150
H          3.21670        2.25340        0.08110
H          0.02540        0.50980        3.02520
H         -2.05420       -0.75730        3.48080
H         -3.48900       -1.48020        1.57900
H          0.22390        0.94280       -1.58920
H         -1.18440        0.24080       -3.55830
H         -3.25150       -1.01620       -3.14490
H         -3.90820       -1.56720       -0.78980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

