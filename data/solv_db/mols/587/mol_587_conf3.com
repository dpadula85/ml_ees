%nproc=16
%mem=2GB
%chk=mol_587_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.43800       -0.67700        0.65970
N          3.43770       -0.63800       -0.38350
C          2.20620        0.00940       -0.09380
O          1.96520        0.53540        1.01160
O          1.20330        0.08030       -1.05600
C         -0.01720        0.69850       -0.83020
C         -0.20280        2.03720       -1.10210
C         -1.43140        2.61230       -0.86320
C         -2.47030        1.86560       -0.35780
C         -2.28800        0.52730       -0.08500
C         -1.06020       -0.05530       -0.32190
C         -0.87860       -1.41190       -0.04430
C         -1.89000       -2.19890        0.46240
C         -3.10960       -1.59790        0.69220
C         -3.30260       -0.25340        0.42120
H          3.91130       -0.74440        1.62900
H          5.15340       -1.51660        0.52280
H          4.99340        0.29970        0.59000
H          3.58480       -1.05940       -1.32150
H          0.62820        2.60430       -1.49890
H         -1.60530        3.67590       -1.07110
H         -3.44470        2.34040       -0.17290
H          0.07740       -1.86550       -0.22880
H         -1.75450       -3.25860        0.68120
H         -3.89340       -2.22700        1.08960
H         -4.25010        0.21780        0.59850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

