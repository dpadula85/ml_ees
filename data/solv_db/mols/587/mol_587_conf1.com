%nproc=16
%mem=2GB
%chk=mol_587_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.49120        0.56720       -0.29900
N          3.45660       -0.05220        0.50300
C          2.16920       -0.27000       -0.02420
O          1.91220        0.06980       -1.18670
O          1.14120       -0.86190        0.70790
C         -0.11230       -1.06350        0.16560
C         -0.47450       -2.20400       -0.50600
C         -1.74820       -2.33780       -1.02310
C         -2.69400       -1.35390       -0.89190
C         -2.33150       -0.18790       -0.20820
C         -1.05380       -0.05840        0.30840
C         -0.72180        1.11210        0.98340
C         -1.63360        2.13620        1.14590
C         -2.88460        1.94890        0.61130
C         -3.26650        0.81870       -0.06230
H          4.10230        0.83370       -1.30510
H          5.30370       -0.17750       -0.46130
H          4.93810        1.45620        0.19850
H          3.70910       -0.32650        1.48050
H          0.27470       -3.00050       -0.61790
H         -1.97450       -3.25670       -1.54090
H         -3.70090       -1.47290       -1.30690
H          0.28730        1.21340        1.39090
H         -1.34840        3.05100        1.68240
H         -3.59690        2.75080        0.73910
H         -4.24390        0.66550       -0.48310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_587_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

