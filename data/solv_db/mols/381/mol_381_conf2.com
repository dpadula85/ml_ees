%nproc=16
%mem=2GB
%chk=mol_381_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.98680        1.28810        0.27930
C         -0.47990        0.12920       -0.52410
C         -1.67620       -0.76420       -0.84150
C          0.50880       -0.72400        0.20500
C          1.74530        0.00180        0.64060
O          2.45910        0.55060       -0.41290
H         -0.21470        1.94140        0.68610
H         -1.73410        1.82350       -0.34550
H         -1.55530        0.87520        1.15790
H         -0.11590        0.51810       -1.48770
H         -1.63970       -1.09070       -1.90370
H         -1.67010       -1.68320       -0.22960
H         -2.62490       -0.25480       -0.66790
H          0.77230       -1.57790       -0.42140
H          0.00260       -1.06390        1.14690
H          2.40720       -0.74670        1.12670
H          1.50830        0.75380        1.43140
H          3.29410        0.02380       -0.61920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

