%nproc=16
%mem=2GB
%chk=mol_381_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69410        0.43780        0.99590
C         -0.46390       -0.24290        0.37830
C         -0.84470       -1.05140       -0.83390
C          0.48230        0.88270        0.05890
C          1.77060        0.41350       -0.56490
O          2.42700       -0.45100        0.30940
H         -2.46630       -0.27980        1.26470
H         -1.31100        0.86480        1.96870
H         -2.02980        1.29300        0.40340
H         -0.03500       -0.88320        1.15450
H         -0.28760       -0.75760       -1.74840
H         -1.94680       -0.98770       -1.03260
H         -0.64020       -2.13230       -0.68370
H          0.74930        1.36300        1.02630
H          0.00070        1.66620       -0.55150
H          2.40760        1.31850       -0.69250
H          1.61700       -0.05110       -1.56050
H          2.26470       -1.40270        0.10780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

