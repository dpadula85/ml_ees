%nproc=16
%mem=2GB
%chk=mol_381_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.62760        1.44940       -0.20360
C         -0.76870       -0.01160       -0.46430
C         -1.50330       -0.59700        0.72940
C          0.51210       -0.72170       -0.74950
C          1.51280       -0.64020        0.36770
O          1.79760        0.71060        0.58010
H         -0.47770        1.67800        0.89140
H         -1.61060        1.92240       -0.49620
H          0.14510        1.95410       -0.77170
H         -1.43540       -0.17110       -1.34110
H         -0.98280       -0.37570        1.68360
H         -2.49820       -0.06190        0.71440
H         -1.62910       -1.67740        0.53870
H          0.27660       -1.79310       -0.90420
H          0.96390       -0.27960       -1.64670
H          1.13280       -1.04850        1.32350
H          2.41510       -1.19420        0.10820
H          2.77740        0.85760        0.54210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

