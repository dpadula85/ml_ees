%nproc=16
%mem=2GB
%chk=mol_381_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79510        1.24050        0.39850
C         -0.48210       -0.24280        0.35270
C         -1.77700       -0.94410       -0.02840
C          0.57930       -0.42700       -0.70130
C          1.78090        0.39180       -0.27430
O          2.17800       -0.12170        0.97520
H         -0.09810        1.81800        1.00410
H         -0.77500        1.57830       -0.67540
H         -1.80960        1.38990        0.82040
H         -0.08720       -0.62600        1.30010
H         -2.17500       -0.36690       -0.90910
H         -2.44620       -0.86240        0.84710
H         -1.60370       -1.98390       -0.33880
H          0.86400       -1.48470       -0.77150
H          0.21710       -0.01760       -1.68080
H          1.54460        1.44900       -0.14750
H          2.60730        0.31300       -1.01180
H          2.27790       -1.10330        0.84080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

