%nproc=16
%mem=2GB
%chk=mol_381_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.02060        0.37640       -1.28280
C         -0.64080        0.40570        0.18650
C         -1.73010       -0.33630        0.93200
C          0.65580       -0.27300        0.44510
C          1.82120        0.33850       -0.28960
O          2.99740       -0.34970        0.00240
H         -2.03700        0.80980       -1.37690
H         -1.11390       -0.69150       -1.57770
H         -0.28040        0.91260       -1.91880
H         -0.63610        1.44510        0.57120
H         -2.69670        0.07120        0.55380
H         -1.62830       -0.21720        2.03040
H         -1.67100       -1.40140        0.60770
H          0.58350       -1.34690        0.16880
H          0.87950       -0.19370        1.52800
H          1.97730        1.39210        0.05850
H          1.67490        0.35270       -1.39150
H          2.86530       -1.29430       -0.24710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_381_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

