%nproc=16
%mem=2GB
%chk=mol_538_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.27270       -0.92830        0.96990
C          2.61590       -0.79590        1.22200
C          3.40640       -0.11070        0.30230
C          2.86800        0.41570       -0.84240
C          1.51710        0.27570       -1.06160
C          0.67340       -0.38450       -0.15750
C         -0.75340       -0.11680       -0.20680
C         -1.65180       -1.12520       -0.34250
C         -3.02070       -0.86470       -0.24090
C         -3.52190        0.39620       -0.00710
C         -2.61240        1.42330        0.13440
C         -1.25270        1.16920        0.03050
Cl        -0.13340        2.50130        0.12640
Cl        -3.23840        3.01610        0.50620
Cl        -4.16180       -2.20380       -0.30180
Cl        -1.10610       -2.76340       -0.61990
Cl         3.82620        1.29810       -2.03670
Cl         5.11010        0.19590        0.60740
H          0.68230       -1.52760        1.63980
H          3.04740       -1.18150        2.13710
H          1.02400        0.71090       -1.92950
H         -4.59080        0.60000        0.07090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

