%nproc=16
%mem=2GB
%chk=mol_538_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.43870       -1.08640       -0.12130
C          2.81350       -0.96120       -0.09680
C          3.46120        0.22460        0.14320
C          2.64170        1.33620        0.39070
C          1.25690        1.23220        0.36640
C          0.66210        0.02720        0.06750
C         -0.76890       -0.01140       -0.21690
C         -1.54250        1.05520        0.21240
C         -2.88260        0.84140        0.46100
C         -3.46880       -0.39070        0.27410
C         -2.69000       -1.42230       -0.19830
C         -1.33920       -1.24420       -0.46870
Cl        -0.58240       -2.31110       -1.69410
Cl        -3.34890       -3.03060       -0.47270
Cl        -3.85790        2.26620        0.84580
Cl        -1.03310        2.71310       -0.26200
Cl         3.35400        2.89060        0.73330
Cl         5.20030        0.41510        0.12780
H          1.03610       -2.10840        0.10250
H          3.40480       -1.82790       -0.33030
H          0.76510        1.94640        1.06880
H         -4.52030       -0.55400        0.49290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

