%nproc=16
%mem=2GB
%chk=mol_538_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.41020        0.91430       -0.80320
C          2.78150        0.76710       -0.79630
C          3.43160       -0.15850       -0.00560
C          2.65360       -0.96850        0.81490
C          1.27830       -0.84510        0.83940
C          0.67890        0.09590        0.02930
C         -0.76950        0.13540       -0.02060
C         -1.44980        1.19300        0.54390
C         -2.82310        1.16340        0.52970
C         -3.52590        0.10780       -0.03830
C         -2.80270       -0.93790       -0.59810
C         -1.42770       -0.93210       -0.59100
Cl        -0.53360       -2.30300       -1.27300
Cl        -3.61480       -2.28020       -1.37810
Cl        -3.70400        2.45910        1.26410
Cl        -0.57820        2.59360        1.21220
Cl         3.38850       -2.22150        1.80460
Cl         5.19190       -0.35500       -0.03440
H          0.93650        1.62280       -1.49990
H          3.38480        1.39170       -1.43910
H          0.70920       -1.53630        1.48450
H         -4.61600        0.09400       -0.04500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

