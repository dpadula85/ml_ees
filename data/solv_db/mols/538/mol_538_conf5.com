%nproc=16
%mem=2GB
%chk=mol_538_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21060        0.36080       -1.35990
C         -2.59960        0.37300       -1.51930
C         -3.45570        0.02130       -0.49860
C         -2.89110       -0.35010        0.70530
C         -1.50000       -0.36220        0.86180
C         -0.65850       -0.00590       -0.17080
C          0.78760       -0.03190        0.07570
C          1.42860        1.14720        0.38540
C          2.77470        1.18300        0.59300
C          3.50600        0.01480        0.49330
C          2.89090       -1.18680        0.18580
C          1.52620       -1.18720       -0.01960
Cl         0.74180       -2.70260       -0.40590
Cl         3.86760       -2.63950        0.06280
Cl         3.56550        2.72010        0.96820
Cl         0.52810        2.66200        0.53830
Cl        -3.97660       -0.78840        2.00760
Cl        -5.18870        0.04940       -0.74800
H         -0.56390        0.63440       -2.17080
H         -3.03850        0.66440       -2.47380
H         -1.11410       -0.66550        1.82320
H          4.58030        0.08980        0.66630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

