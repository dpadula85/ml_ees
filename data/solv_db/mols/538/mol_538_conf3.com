%nproc=16
%mem=2GB
%chk=mol_538_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.34140        0.35040        1.28250
C          2.69970        0.34530        1.40000
C          3.44800       -0.00010        0.29230
C          2.81580       -0.32630       -0.88540
C          1.45220       -0.32060       -1.00120
C          0.70460        0.01920        0.08670
C         -0.74920        0.02660       -0.03430
C         -1.48900       -1.04860        0.40790
C         -2.87070       -1.05510        0.29280
C         -3.55320       -0.00160       -0.26070
C         -2.82470        1.08130       -0.70690
C         -1.45060        1.08510       -0.59130
Cl        -0.50350        2.44890       -1.14740
Cl        -3.67060        2.44040       -1.41780
Cl        -3.77800       -2.45850        0.87180
Cl        -0.69320       -2.43960        1.12830
Cl         3.77450       -0.76590       -2.29790
Cl         5.21220       -0.01730        0.40560
H          0.66680        0.61000        2.09740
H          3.14760        0.61070        2.35000
H          0.96290       -0.57600       -1.92020
H         -4.64310       -0.00840       -0.35200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_538_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

