%nproc=16
%mem=2GB
%chk=mol_259_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90190        0.18010       -0.17950
C         -1.11350        1.31040       -0.06690
C          0.27110        1.18690        0.06400
C          0.87630       -0.05260        0.08370
C          0.07260       -1.15780       -0.02960
C         -1.30840       -1.06330       -0.16110
N          2.29760       -0.19370        0.21760
O          3.06160        0.76020        0.32230
O          2.84050       -1.45530        0.23060
H         -2.99160        0.23260       -0.28410
H         -1.58760        2.28560       -0.08220
H          0.90420        2.06680        0.15340
H          0.51110       -2.14850       -0.01840
H         -1.93210       -1.95130       -0.24980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_259_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_259_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_259_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

