%nproc=16
%mem=2GB
%chk=mol_259_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93570       -0.05240        0.15450
C         -1.24230        1.13580        0.24500
C          0.14640        1.14990        0.13120
C          0.86760       -0.00430       -0.07270
C          0.17790       -1.19030       -0.16330
C         -1.20580       -1.20340       -0.04980
N          2.28710        0.05110       -0.18380
O          2.98150       -0.96630       -0.36850
O          2.96570        1.24340       -0.09150
H         -3.02910       -0.08700        0.24120
H         -1.77200        2.06490        0.40500
H          0.71080        2.08610        0.20100
H          0.76080       -2.08770       -0.32370
H         -1.71300       -2.13990       -0.12450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_259_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_259_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_259_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

