%nproc=16
%mem=2GB
%chk=mol_420_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.12350        0.03720        0.22450
C          0.27980       -1.10390       -0.36400
C         -1.10240       -0.63560        0.11360
N         -1.10680        0.78540       -0.11470
C          0.26020        1.22010       -0.22470
H          2.12760        0.06700       -0.18420
H          1.06680       -0.01520        1.33150
H          0.36730       -1.01430       -1.46130
H          0.55400       -2.07430        0.04290
H         -1.91230       -1.17410       -0.39090
H         -1.09940       -0.82880        1.22400
H         -1.49630        1.25540        0.75780
H          0.47340        1.35640       -1.31050
H          0.46450        2.12460        0.35600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

