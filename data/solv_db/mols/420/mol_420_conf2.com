%nproc=16
%mem=2GB
%chk=mol_420_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97260       -0.48690        0.48480
C          0.35800       -1.11530        0.04880
C          1.24330        0.13100        0.16460
N          0.41700        1.24060       -0.20970
C         -0.92580        0.78940       -0.36350
H         -1.82690       -1.11360        0.25730
H         -0.85890       -0.16750        1.52140
H          0.21020       -1.40000       -1.01350
H          0.67580       -1.92030        0.70130
H          1.44460        0.24000        1.27030
H          2.17680        0.03480       -0.38270
H          0.78990        1.80910       -0.98680
H         -1.68410        1.51460       -0.06160
H         -1.04730        0.44400       -1.43070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

