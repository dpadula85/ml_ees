%nproc=16
%mem=2GB
%chk=mol_420_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97100       -0.48280        0.36540
C          0.36770       -1.10760       -0.04800
C          1.26500        0.11680        0.06210
N          0.42880        1.26800       -0.09950
C         -0.89360        0.79570       -0.48090
H         -1.82400       -1.08700        0.07470
H         -0.94270       -0.16940        1.42050
H          0.66700       -1.92400        0.60470
H          0.24690       -1.42190       -1.11750
H          1.83390        0.10450        1.01270
H          1.98240        0.07320       -0.76280
H          0.39380        1.84600        0.77460
H         -0.86050        0.49840       -1.55610
H         -1.69380        1.49030       -0.24990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

