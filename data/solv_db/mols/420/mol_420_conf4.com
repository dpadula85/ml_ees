%nproc=16
%mem=2GB
%chk=mol_420_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.86300       -0.76760       -0.10010
C         -0.89510        0.71880        0.27350
C          0.46310        1.14000       -0.29640
N          1.36420        0.05710       -0.00870
C          0.56950       -1.09570        0.32530
H         -0.99040       -0.83840       -1.19180
H         -1.60730       -1.34790        0.45320
H         -0.88980        0.80710        1.36850
H         -1.70140        1.25700       -0.22940
H          0.74420        2.05470        0.23590
H          0.30630        1.29590       -1.39090
H          2.08080       -0.11160       -0.73390
H          0.53280       -1.14570        1.44320
H          0.88610       -2.02370       -0.14840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_420_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

