%nproc=16
%mem=2GB
%chk=mol_436_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.01320        0.00170       -0.00510
F          0.49890       -0.17240        1.26400
F         -0.81900       -1.07310       -0.29380
F         -0.70450        1.19520       -0.08300
F          1.03780        0.04850       -0.88210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_436_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_436_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_436_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

