%nproc=16
%mem=2GB
%chk=mol_436_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01420       -0.01770       -0.01240
F          0.37110        0.03420        1.30920
F         -0.73160        1.11720       -0.29140
F         -0.79940       -1.10120       -0.20660
F          1.14580       -0.03250       -0.79880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_436_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_436_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_436_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

