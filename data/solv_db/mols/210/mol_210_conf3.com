%nproc=16
%mem=2GB
%chk=mol_210_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.20680       -0.22650        1.25450
C         -2.94480       -0.65720       -0.15200
C         -2.68130        0.47080       -1.10090
C         -1.49460        1.31150       -0.74530
C         -0.20580        0.51550       -0.70770
C          0.89550        1.47910       -0.34030
C          2.24270        0.80460       -0.27100
C          2.21650       -0.30070        0.78270
C          3.57000       -0.98600        0.86250
O          3.93750       -1.56060       -0.33070
H         -2.83380       -1.05290        1.92450
H         -2.75890        0.71740        1.54820
H         -4.30200       -0.15960        1.47620
H         -3.84920       -1.18960       -0.51600
H         -2.12050       -1.42790       -0.19620
H         -2.53440        0.06390       -2.10200
H         -3.56750        1.12430       -1.11510
H         -1.37280        2.06670       -1.55020
H         -1.58180        1.85370        0.19940
H         -0.31550       -0.21300        0.12660
H          0.00130       -0.00540       -1.64300
H          0.98790        2.30540       -1.07390
H          0.66990        1.95170        0.63210
H          3.03900        1.50380        0.05290
H          2.56450        0.40430       -1.25210
H          2.04760        0.18370        1.76290
H          1.47280       -1.08610        0.54300
H          4.35790       -0.25050        1.19440
H          3.48860       -1.74420        1.66710
H          4.27790       -0.84650       -0.93040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_210_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_210_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_210_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

