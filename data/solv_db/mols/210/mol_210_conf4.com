%nproc=16
%mem=2GB
%chk=mol_210_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.08650        0.23260       -1.65600
C         -3.16790        0.71640       -0.55350
C         -2.00970       -0.25200       -0.48920
C         -1.01310        0.12620        0.58370
C          0.08180       -0.91580        0.53850
C          1.14300       -0.64760        1.57760
C          1.80010        0.68010        1.38630
C          2.47570        0.82340        0.05340
C          3.55850       -0.20470       -0.15900
O          4.11070        0.02380       -1.41890
H         -4.96620        0.88050       -1.79480
H         -3.46790        0.23210       -2.59120
H         -4.32600       -0.81720       -1.44860
H         -2.78540        1.70790       -0.85550
H         -3.69410        0.81050        0.41710
H         -1.50980       -0.24490       -1.46430
H         -2.42530       -1.24610       -0.24640
H         -0.55490        1.11380        0.37780
H         -1.49790        0.11900        1.56390
H         -0.33710       -1.93420        0.78940
H          0.58560       -0.95480       -0.43970
H          1.84040       -1.49650        1.55360
H          0.64230       -0.68870        2.57080
H          2.52790        0.81470        2.22640
H          1.07760        1.50870        1.49520
H          2.95870        1.82170        0.03320
H          1.72080        0.75560       -0.73760
H          3.18680       -1.24080       -0.13570
H          4.32440       -0.11960        0.63860
H          3.80750        0.86950       -1.81530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_210_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_210_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_210_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

