%nproc=16
%mem=2GB
%chk=mol_302_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.96880        0.09630       -0.04800
S          0.76950       -0.22640        0.02980
O          1.15680       -0.60150        1.43700
O          1.19760       -1.31450       -0.89730
Cl         1.86350        1.45920       -0.45980
H         -1.54930       -0.58700        0.61920
H         -1.12110        1.11800        0.39590
H         -1.34810        0.05590       -1.07690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

