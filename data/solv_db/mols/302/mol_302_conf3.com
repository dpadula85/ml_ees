%nproc=16
%mem=2GB
%chk=mol_302_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.94550        0.08130        0.00490
S          0.79120       -0.16850        0.02980
O          1.20040       -1.05780       -1.12670
O          1.23840       -0.83700        1.28700
Cl         1.81850        1.60470       -0.15810
H         -1.24610        1.13120        0.21190
H         -1.49340       -0.54510        0.74310
H         -1.36350       -0.20880       -0.99200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

