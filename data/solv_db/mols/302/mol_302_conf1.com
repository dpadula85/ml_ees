%nproc=16
%mem=2GB
%chk=mol_302_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.96090        0.00930       -0.02140
S         -0.81520       -0.19200        0.00220
O         -1.30600       -0.88180       -1.23380
O         -1.22590       -0.95030        1.24080
Cl        -1.58180        1.74140        0.10790
H          1.23250        0.86860       -0.68810
H          1.46350       -0.88110       -0.41040
H          1.27200        0.28600        1.00280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

