%nproc=16
%mem=2GB
%chk=mol_302_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97410        0.09740       -0.04470
S          0.77310       -0.22420        0.10360
O          1.18800       -1.38110       -0.75130
O          1.17630       -0.41040        1.52860
Cl         1.84180        1.41280       -0.61530
H         -1.48320       -0.05580        0.94020
H         -1.40190       -0.58770       -0.79690
H         -1.11990        1.14910       -0.36410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

