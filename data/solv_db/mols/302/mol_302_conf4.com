%nproc=16
%mem=2GB
%chk=mol_302_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.96540        0.04450        0.04360
S          0.81390       -0.11270       -0.00220
O          1.32020       -0.85830        1.21150
O          1.10600       -1.02210       -1.18710
Cl         1.73780        1.69880       -0.22670
H         -1.32290       -0.68090        0.79360
H         -1.40740       -0.13410       -0.96940
H         -1.28220        1.06470        0.33660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_302_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

