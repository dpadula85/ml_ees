%nproc=16
%mem=2GB
%chk=mol_493_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.68990        1.44210       -0.21420
C         -3.03450        0.12870       -0.54220
C         -1.93790       -0.18850        0.46380
C         -1.27730       -1.51720        0.12920
C         -0.19390       -1.86730        1.09310
C          0.91550       -0.84020        1.12740
C          1.58490       -0.66710       -0.19800
C          2.65160        0.34950       -0.07710
O          2.84480        0.89520        1.03450
O          3.46510        0.74470       -1.13000
C          4.48240        1.73090       -0.96440
H         -3.27460        2.23860       -0.85330
H         -3.58480        1.70540        0.86870
H         -4.78040        1.35130       -0.47780
H         -2.55770        0.21340       -1.53550
H         -3.78920       -0.68850       -0.58040
H         -2.43570       -0.25390        1.45010
H         -1.22500        0.64250        0.47380
H         -2.06260       -2.29680        0.20400
H         -0.90750       -1.48120       -0.91370
H          0.17610       -2.87590        0.84480
H         -0.62800       -1.90920        2.11480
H          0.48500        0.12220        1.45530
H          1.68940       -1.16160        1.85440
H          1.95370       -1.62110       -0.62300
H          0.82090       -0.26210       -0.90340
H          5.36860        1.52030       -1.59830
H          4.11680        2.74790       -1.23360
H          4.82410        1.79780        0.08910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

