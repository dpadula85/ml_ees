%nproc=16
%mem=2GB
%chk=mol_493_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.66570        0.80640       -0.31480
C         -2.87150        1.19550        0.91270
C         -1.99830        0.02080        1.37220
C         -1.07070       -0.29390        0.19870
C         -0.15590       -1.46540        0.57500
C          0.77170       -1.80010       -0.56160
C          1.66900       -0.67490       -0.96940
C          2.56390       -0.18800        0.08090
O          2.66850       -0.59370        1.24460
O          3.41310        0.87710       -0.27940
C          4.32060        1.43090        0.65030
H         -4.65560        1.32510       -0.29870
H         -3.16260        1.08440       -1.24820
H         -3.88440       -0.27020       -0.30780
H         -3.54370        1.48490        1.75730
H         -2.23390        2.05900        0.62770
H         -2.66490       -0.83400        1.58050
H         -1.41560        0.29220        2.26730
H         -0.41710        0.57260       -0.02230
H         -1.69090       -0.54850       -0.67880
H         -0.86220       -2.34460        0.65720
H          0.32800       -1.27100        1.53240
H          0.09850       -2.03230       -1.43900
H          1.32940       -2.72370       -0.37990
H          2.29960       -1.03230       -1.81240
H          1.08210        0.18720       -1.37220
H          4.07120        1.13330        1.68250
H          4.33520        2.52580        0.52110
H          5.34250        1.07700        0.34640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

