%nproc=16
%mem=2GB
%chk=mol_493_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.61310        0.57110        0.59740
C         -3.15600        0.69310        0.11450
C         -2.80960       -0.49170       -0.75930
C         -1.40280       -0.43990       -1.28370
C         -0.42360       -0.43390       -0.16280
C          1.01580       -0.39420       -0.63630
C          1.89640       -0.39050        0.61420
C          3.33500       -0.35170        0.24420
O          3.89820       -1.40790       -0.14170
O          4.11310        0.79690        0.29710
C          5.48350        0.81640       -0.06040
H         -4.58720        0.30380        1.67180
H         -5.15830       -0.16150       -0.02320
H         -5.12830        1.56200        0.52040
H         -3.11450        1.58870       -0.52760
H         -2.47820        0.75680        0.99210
H         -2.87970       -1.44850       -0.16250
H         -3.56200       -0.62820       -1.56700
H         -1.33310        0.45120       -1.94300
H         -1.29050       -1.35510       -1.93070
H         -0.53890       -1.38450        0.40190
H         -0.57520        0.39450        0.54720
H          1.20010       -1.26210       -1.26390
H          1.25990        0.52010       -1.19650
H          1.59910        0.47930        1.23370
H          1.64810       -1.29580        1.22440
H          5.64420        0.66340       -1.14410
H          6.07300        0.03510        0.46740
H          5.88460        1.81310        0.23850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

