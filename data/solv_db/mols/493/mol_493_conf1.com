%nproc=16
%mem=2GB
%chk=mol_493_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.40820        1.60670       -0.12430
C         -2.31530        0.64310       -0.59590
C         -2.59580       -0.73510       -0.14510
C         -1.64180       -1.78550       -0.51930
C         -0.26530       -1.82670        0.00630
C          0.65320       -0.70080       -0.21510
C          2.06740       -1.02350        0.32600
C          2.95600        0.14210        0.07850
O          3.46790        0.31450       -1.08080
O          3.27550        1.07590        1.03550
C          4.12810        2.16640        0.73710
H         -3.76090        1.34010        0.89120
H         -4.23200        1.65980       -0.86290
H         -2.93970        2.60960       -0.07550
H         -2.42260        0.67430       -1.73350
H         -1.34740        1.05360       -0.39710
H         -3.60840       -1.04090       -0.57550
H         -2.80420       -0.78020        0.96900
H         -1.55030       -1.77240       -1.67060
H         -2.14010       -2.82630       -0.40200
H         -0.29150       -2.13790        1.10570
H          0.19980       -2.77510       -0.45580
H          0.76730       -0.40750       -1.25580
H          0.34220        0.13380        0.48270
H          2.49140       -1.90080       -0.19420
H          2.01350       -1.21350        1.41660
H          4.85190        2.37840        1.53870
H          3.46240        3.06860        0.67840
H          4.64690        2.05950       -0.23100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

