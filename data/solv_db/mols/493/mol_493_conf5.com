%nproc=16
%mem=2GB
%chk=mol_493_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.98430        1.28270       -0.40870
C          2.52820        0.21550       -1.40690
C          2.62430       -1.14010       -0.82960
C          1.90080       -1.48290        0.39440
C          0.44410       -1.44250        0.48980
C         -0.29260       -0.17700        0.29620
C         -1.78740       -0.53900        0.52130
C         -2.62640        0.66890        0.35000
O         -2.20720        1.81280        0.06720
O         -3.99110        0.50500        0.52020
C         -4.88990        1.57410        0.38410
H          2.55570        2.25670       -0.70900
H          2.61890        0.98560        0.57440
H          4.10720        1.32860       -0.40570
H          1.60760        0.54390       -1.85120
H          3.31770        0.28460       -2.24950
H          2.42170       -1.90520       -1.65970
H          3.73640       -1.33790       -0.63630
H          2.20160       -2.56510        0.64620
H          2.34080       -0.90850        1.26780
H          0.16520       -1.80520        1.53690
H         -0.04210       -2.23350       -0.15330
H         -0.25870        0.25120       -0.69910
H         -0.07280        0.57630        1.09700
H         -1.84720       -0.88540        1.57310
H         -2.08550       -1.36490       -0.13880
H         -4.67250        2.22530       -0.48030
H         -5.91640        1.13070        0.34460
H         -4.86460        2.14530        1.35050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_493_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

