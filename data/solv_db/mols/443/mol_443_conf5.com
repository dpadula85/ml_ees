%nproc=16
%mem=2GB
%chk=mol_443_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.19290        0.92260        0.36300
C          1.99110        0.49610       -0.45810
C          1.41800       -0.80090        0.08410
C          0.22420       -1.23100       -0.72780
C         -0.89650       -0.23790       -0.73350
C         -2.01210       -0.80180       -1.57890
C         -1.35580        0.09240        0.66690
C         -2.47300        1.08020        0.69970
H          4.09190        0.95590       -0.29810
H          3.01950        1.92590        0.76510
H          3.36160        0.23470        1.21830
H          2.27540        0.35570       -1.51530
H          1.19800        1.29610       -0.40470
H          2.20680       -1.58060       -0.08880
H          1.20210       -0.71390        1.15100
H         -0.11520       -2.23050       -0.41760
H          0.55930       -1.34510       -1.79420
H         -0.56300        0.69190       -1.22820
H         -2.54930       -1.61690       -1.07070
H         -2.69380       -0.01160       -1.93730
H         -1.52250       -1.22970       -2.49750
H         -1.63260       -0.84030        1.20590
H         -0.49980        0.52540        1.22240
H         -3.13700        1.02090       -0.19400
H         -3.13890        0.90640        1.59320
H         -2.15130        2.13600        0.77140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

