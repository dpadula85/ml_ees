%nproc=16
%mem=2GB
%chk=mol_443_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.69550       -0.24340        0.21420
C          2.48490        0.63820        0.01330
C          1.28390       -0.25860       -0.09970
C         -0.00190        0.52340       -0.30550
C         -1.11270       -0.49410       -0.40080
C         -1.20320       -1.31380        0.84600
C         -2.42380        0.11570       -0.82150
C         -2.92650        1.16600        0.10990
H          4.27570       -0.31190       -0.72780
H          3.41020       -1.26390        0.49090
H          4.35060        0.15780        1.00350
H          2.38730        1.27010        0.90920
H          2.66040        1.25820       -0.86350
H          1.38770       -0.91320       -1.00110
H          1.19980       -0.89470        0.78090
H          0.04220        1.13410       -1.21060
H         -0.20420        1.17220        0.57770
H         -0.81870       -1.19770       -1.23280
H         -1.08360       -0.68190        1.76060
H         -2.19440       -1.80780        0.89950
H         -0.44000       -2.11980        0.78940
H         -3.16790       -0.70250       -0.85410
H         -2.30310        0.52980       -1.84050
H         -3.45740        0.65680        0.94620
H         -2.12960        1.83220        0.44320
H         -3.71090        1.74860       -0.42690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

