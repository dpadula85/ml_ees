%nproc=16
%mem=2GB
%chk=mol_443_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.72760        0.35610        0.14940
C         -2.58470       -0.62380        0.20240
C         -1.27560        0.13830        0.18340
C         -0.11430       -0.81720        0.23560
C          1.21730       -0.15900        0.22110
C          2.28040       -1.23480        0.27740
C          1.42150        0.78500       -0.92750
C          2.81220        1.38300       -0.82100
H         -3.96600        0.64730       -0.88570
H         -3.46770        1.25350        0.71800
H         -4.62950       -0.14160        0.55790
H         -2.64250       -1.15790        1.17140
H         -2.62480       -1.37370       -0.62000
H         -1.29300        0.86180        0.99370
H         -1.27550        0.71530       -0.78050
H         -0.20250       -1.53100       -0.60260
H         -0.20310       -1.40030        1.17520
H          1.33370        0.44290        1.16190
H          1.96980       -1.93620        1.08840
H          3.28410       -0.84350        0.43180
H          2.23280       -1.76540       -0.71060
H          1.21960        0.34570       -1.91020
H          0.71400        1.64440       -0.76380
H          3.21100        1.22180        0.21070
H          2.85260        2.42920       -1.14230
H          3.45780        0.76010       -1.50250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

