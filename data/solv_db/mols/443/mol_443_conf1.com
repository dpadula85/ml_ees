%nproc=16
%mem=2GB
%chk=mol_443_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.55280       -0.56440        0.95590
C          2.60200       -0.39400       -0.18810
C          1.26640        0.09410        0.34300
C          0.32440        0.26070       -0.80890
C         -1.03540        0.74350       -0.39730
C         -1.91810        0.88490       -1.60670
C         -1.68820       -0.23170        0.54830
C         -3.04610        0.22880        0.97270
H          3.63850        0.39300        1.50490
H          3.20620       -1.34390        1.64620
H          4.55260       -0.78670        0.54420
H          2.49070       -1.32110       -0.77470
H          2.98780        0.38510       -0.87590
H          1.47220        1.09020        0.81020
H          0.94580       -0.61760        1.10220
H          0.27680       -0.67570       -1.38950
H          0.78720        1.03020       -1.49010
H         -0.97540        1.71050        0.10580
H         -2.92960        0.46250       -1.42000
H         -1.45070        0.33290       -2.44350
H         -2.00620        1.96360       -1.88960
H         -1.05500       -0.45580        1.44210
H         -1.79130       -1.19200        0.00880
H         -3.35130        1.15780        0.44950
H         -3.05860        0.49960        2.06680
H         -3.79750       -0.54950        0.78370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

