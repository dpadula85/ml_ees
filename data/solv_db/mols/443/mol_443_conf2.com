%nproc=16
%mem=2GB
%chk=mol_443_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.08430       -0.70360       -0.54020
C          2.52280        0.65360       -0.25750
C          1.30060        0.64070        0.60330
C          0.19560       -0.15630       -0.06270
C         -1.04160       -0.16760        0.81200
C         -1.54870        1.22040        1.05520
C         -2.08780       -1.02700        0.13230
C         -2.45370       -0.49610       -1.22810
H          2.50510       -1.53600       -0.10130
H          4.12390       -0.81630       -0.12420
H          3.18610       -0.89770       -1.62850
H          3.29890        1.24620        0.26320
H          2.24000        1.18580       -1.21240
H          1.48780        0.27830        1.62620
H          0.94700        1.68850        0.70010
H          0.50290       -1.19180       -0.28410
H         -0.07760        0.33100       -1.02180
H         -0.80040       -0.67330        1.77930
H         -1.00920        1.73490        1.87420
H         -2.62790        1.14910        1.30990
H         -1.42480        1.79810        0.11100
H         -2.98010       -1.07800        0.80240
H         -1.62700       -2.05230        0.04360
H         -2.04050       -1.18430       -1.99820
H         -2.11170        0.53570       -1.34630
H         -3.56410       -0.48210       -1.30760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_443_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

