%nproc=16
%mem=2GB
%chk=mol_280_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23210        1.09280       -0.53950
C         -0.77690        0.01920        0.36120
C         -1.51260       -1.26330        0.02950
C          0.69440       -0.22140        0.42220
C          1.46160        0.99450        0.88690
C          1.31610       -0.72420       -0.82820
H         -1.46300        2.05440        0.00630
H         -2.18570        0.83800       -1.06050
H         -0.52510        1.38490       -1.33540
H         -1.10090        0.28530        1.40860
H         -2.51290       -1.27560        0.50660
H         -1.68590       -1.37000       -1.06610
H         -0.90690       -2.11440        0.43580
H          0.86030       -1.00700        1.21190
H          2.22010        1.31950        0.12120
H          2.04290        0.81530        1.81130
H          0.71450        1.79850        1.05900
H          1.45880        0.05790       -1.60880
H          0.78620       -1.61570       -1.23870
H          2.34720       -1.06870       -0.58330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

