%nproc=16
%mem=2GB
%chk=mol_280_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.14250        1.52270        0.31650
C          0.60460        0.12040        0.37650
C          1.70850       -0.81510       -0.01710
C         -0.61720       -0.03630       -0.48720
C         -1.09200       -1.48840       -0.36340
C         -1.73700        0.80720        0.10870
H          1.85670        1.62720        1.17360
H          0.38490        2.30350        0.30820
H          1.78980        1.57280       -0.60730
H          0.29620       -0.16380        1.41620
H          1.74520       -1.02330       -1.10640
H          2.70200       -0.33710        0.22740
H          1.68790       -1.74260        0.59880
H         -0.46100        0.18420       -1.53910
H         -0.36860       -2.19530       -0.77310
H         -1.39340       -1.71240        0.68500
H         -2.06140       -1.51210       -0.94470
H         -1.58340        1.87790       -0.01760
H         -1.94970        0.47550        1.13510
H         -2.65460        0.53510       -0.49010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

