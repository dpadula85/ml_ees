%nproc=16
%mem=2GB
%chk=mol_280_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.46180       -0.93110       -0.90770
C         -0.71500        0.29470       -0.40310
C         -1.28430        0.76310        0.88060
C          0.74080       -0.03980       -0.38540
C          1.12760       -1.18140        0.47900
C          1.56010        1.20390       -0.04720
H         -1.26910       -1.77900       -0.21970
H         -1.04700       -1.15390       -1.91690
H         -2.52460       -0.69160       -0.92230
H         -0.86880        1.08490       -1.16840
H         -0.98830        0.17200        1.76630
H         -2.39190        0.70850        0.81790
H         -1.05150        1.84340        1.05420
H          1.02360       -0.29630       -1.43900
H          1.65460       -1.99190       -0.07340
H          1.88380       -0.82240        1.23840
H          0.29760       -1.60550        1.08250
H          1.96760        1.69130       -0.94860
H          0.92290        1.87110        0.53510
H          2.42360        0.86000        0.57770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

