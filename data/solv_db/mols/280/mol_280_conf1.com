%nproc=16
%mem=2GB
%chk=mol_280_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.63390        0.66390        0.87930
C          0.76840       -0.38010        0.21100
C          1.00360       -0.33680       -1.27340
C         -0.68180       -0.16420        0.57600
C         -1.52600       -1.22400       -0.10710
C         -1.18990        1.17790        0.09580
H          2.70060        0.30800        0.83880
H          1.37130        0.81510        1.94000
H          1.50970        1.63950        0.37760
H          1.06010       -1.36950        0.62000
H          1.81240       -1.05700       -1.52240
H          0.11920       -0.63290       -1.86640
H          1.37950        0.66950       -1.53050
H         -0.75910       -0.28130        1.67860
H         -2.44840       -1.32910        0.48800
H         -1.77100       -0.86260       -1.13250
H         -0.98950       -2.20470       -0.17250
H         -1.62110        1.72540        0.96080
H         -2.00120        1.08450       -0.65630
H         -0.37090        1.75820       -0.40470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

