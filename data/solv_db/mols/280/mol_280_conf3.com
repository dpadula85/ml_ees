%nproc=16
%mem=2GB
%chk=mol_280_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.01620        1.07730       -0.83980
C         -0.82740       -0.25490       -0.21270
C         -1.57280       -0.25850        1.11570
C          0.61000       -0.60930        0.06110
C          1.39970       -0.61930       -1.24210
C          1.26380        0.30330        1.04480
H         -0.06590        1.59660       -1.10220
H         -1.56880        0.98560       -1.81980
H         -1.67110        1.76250       -0.22830
H         -1.29600       -1.03490       -0.83930
H         -1.19180       -1.15600        1.68140
H         -1.33460        0.62470        1.73360
H         -2.66180       -0.40340        0.95640
H          0.67650       -1.64250        0.46060
H          0.93170        0.02100       -2.01440
H          1.47560       -1.66540       -1.64910
H          2.40250       -0.20850       -1.02870
H          1.53160        1.28730        0.61230
H          0.67620        0.37290        1.97020
H          2.23870       -0.17850        1.34040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_280_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

