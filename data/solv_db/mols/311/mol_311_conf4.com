%nproc=16
%mem=2GB
%chk=mol_311_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90970       -0.09900        0.23490
C         -1.08050        0.11120       -0.98460
O         -1.68920        0.27940       -2.02730
C          0.39340        0.12320       -0.97870
O          0.94700       -0.08320        0.29100
N          2.31250       -0.09030        0.40590
O          2.90470       -1.18010        0.27390
O          3.06230        1.03350        0.65770
H         -2.47540        0.84740        0.47720
H         -2.68990       -0.85780        0.02400
H         -1.32530       -0.45610        1.09730
H          0.74670       -0.72710       -1.63040
H          0.80350        1.09870       -1.35590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

