%nproc=16
%mem=2GB
%chk=mol_311_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92640       -0.31480        0.36620
C         -0.51380       -0.28220        0.25330
O          0.19860       -0.74400        1.20430
C          0.23560        0.26890       -0.92760
O          1.53220       -0.00150       -0.71680
N          2.49060        0.44950        0.09120
O          3.12330       -0.52070        0.64430
O          2.10730        1.46590        1.02050
H         -2.36820       -1.02170       -0.36200
H         -2.24470       -0.64190        1.40200
H         -2.35120        0.69470        0.20160
H         -0.13690        1.17470       -1.39420
H         -0.14640       -0.52690       -1.78280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

