%nproc=16
%mem=2GB
%chk=mol_311_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.95730        0.31700        0.27820
C         -0.80350        0.14010       -0.61140
O         -0.95340        0.49980       -1.78780
C          0.37290       -0.73320       -0.29270
O          1.07660       -0.22290        0.82720
N          2.37560        0.21740        0.68140
O          2.74480        1.19330        1.37800
O          2.97460        0.09810       -0.54870
H         -1.80660       -0.12650        1.27890
H         -2.84450       -0.18540       -0.21030
H         -2.21570        1.39140        0.35410
H         -0.00010       -1.79230       -0.10520
H          1.03670       -0.79680       -1.13420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

