%nproc=16
%mem=2GB
%chk=mol_311_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25830       -0.14080        0.41240
C         -0.92560        0.43140        0.03730
O         -0.86800        1.42670       -0.65860
C          0.32730       -0.19310        0.49880
O          1.42320        0.51710        0.02240
N          2.73040        0.24040        0.24130
O          3.48300        1.13790        0.64360
O          3.24300       -1.00740        0.02940
H         -3.02470        0.12670       -0.35220
H         -2.23680       -1.25390        0.43650
H         -2.61040        0.30030        1.37100
H          0.37850       -1.22850        0.05290
H          0.33830       -0.35680        1.59460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

