%nproc=16
%mem=2GB
%chk=mol_311_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.20730        0.19400       -0.10430
C         -0.93130        0.17170        0.62670
O         -0.92710        0.32440        1.81740
C          0.32820       -0.03660       -0.10020
O          1.46130       -0.03210        0.73750
N          2.68560       -0.22290        0.12210
O          3.23240        0.80680       -0.32910
O          3.26170       -1.47200        0.01570
H         -2.17860        0.99080       -0.87730
H         -2.31980       -0.77250       -0.62360
H         -3.07550        0.30600        0.57310
H          0.38920        0.77200       -0.88290
H          0.28120       -1.02950       -0.62680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_311_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

