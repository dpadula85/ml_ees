%nproc=16
%mem=2GB
%chk=mol_139_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.55580       -1.36080       -0.61090
C          1.22910       -0.24180        0.28880
C         -0.01130        0.55070       -0.01550
C         -0.06510        1.63320        1.08770
C          0.08130        1.33180       -1.29340
C         -1.27160       -0.21340        0.15300
C         -1.49780       -1.41680       -0.65000
H          1.43540       -2.33160       -0.04460
H          2.65310       -1.38910       -0.89390
H          1.01850       -1.44650       -1.54580
H          2.14020        0.42460        0.35030
H          1.11010       -0.58980        1.35880
H         -0.59370        1.24430        1.98150
H         -0.59920        2.52120        0.75240
H          0.94430        1.92630        1.40310
H          1.02780        1.90550       -1.26410
H         -0.06080        0.76500       -2.20290
H         -0.73750        2.10830       -1.25260
H         -1.31670       -0.52390        1.24110
H         -2.16280        0.46600        0.04990
H         -1.35870       -1.31950       -1.73990
H         -2.58320       -1.72640       -0.54730
H         -0.93720       -2.31720       -0.32470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

