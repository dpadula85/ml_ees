%nproc=16
%mem=2GB
%chk=mol_139_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.74540       -1.18090       -0.36430
C          0.72930       -0.27880       -1.02010
C         -0.02610        0.52860        0.01520
C         -1.04020        1.42790       -0.64840
C          0.94450        1.41000        0.75370
C         -0.69440       -0.41160        0.99870
C         -1.66200       -1.32780        0.31480
H          2.27580       -1.71400       -1.20730
H          1.26530       -1.96990        0.22780
H          2.46660       -0.61750        0.25790
H          0.07500       -0.90110       -1.64080
H          1.26270        0.41910       -1.70290
H         -1.50510        2.05070        0.14010
H         -1.85580        0.87010       -1.13500
H         -0.49510        2.07560       -1.36520
H          1.46760        2.05180       -0.01430
H          1.65400        0.87620        1.38500
H          0.32490        2.14870        1.34380
H         -1.24050        0.19700        1.75730
H          0.10820       -0.93530        1.55190
H         -1.34690       -2.39640        0.40510
H         -2.69510       -1.18250        0.72510
H         -1.75810       -1.13960       -0.77820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

