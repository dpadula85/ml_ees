%nproc=16
%mem=2GB
%chk=mol_139_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36630        0.12980        0.71830
C          1.01010       -0.52960        0.92880
C         -0.00240       -0.03660       -0.07940
C         -0.13140        1.45620        0.10210
C          0.43320       -0.43600       -1.44270
C         -1.33050       -0.65410        0.27750
C         -2.41150       -0.21330       -0.68520
H          2.41990        0.63800       -0.26750
H          2.64470        0.78970        1.56190
H          3.12160       -0.70290        0.66170
H          1.07540       -1.64070        0.79350
H          0.60260       -0.29900        1.93190
H         -0.72380        1.93230       -0.70720
H         -0.64980        1.61360        1.07360
H          0.83030        1.97400        0.16820
H          0.57320        0.41620       -2.13670
H          1.40180       -0.97440       -1.41580
H         -0.27800       -1.12830       -1.96100
H         -1.29140       -1.76980        0.21930
H         -1.64740       -0.31750        1.28610
H         -3.22190       -0.96520       -0.76400
H         -2.80570        0.75700       -0.32310
H         -1.98520       -0.03960       -1.70100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

