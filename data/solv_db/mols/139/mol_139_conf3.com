%nproc=16
%mem=2GB
%chk=mol_139_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25300        0.10720        0.91200
C         -1.49210       -0.50330       -0.22240
C         -0.06020       -0.04710       -0.30430
C          0.07840        1.43500       -0.48920
C          0.55300       -0.74190       -1.51910
C          0.75420       -0.48180        0.89240
C          2.17030        0.05210        0.66330
H         -2.53200        1.16620        0.74940
H         -3.19290       -0.51060        1.02500
H         -1.72890       -0.00410        1.88530
H         -1.46880       -1.60490       -0.05460
H         -1.98670       -0.23450       -1.17020
H          0.15460        1.88680        0.52400
H          0.99290        1.62830       -1.08980
H         -0.81910        1.86870       -0.97250
H          1.22560       -0.03570       -2.03970
H          1.18900       -1.60630       -1.21230
H         -0.24370       -1.11370       -2.20890
H          0.33090       -0.13890        1.84030
H          0.87000       -1.60430        0.89670
H          2.20880        1.12650        0.92410
H          2.42900       -0.13530       -0.39370
H          2.82080       -0.50840        1.36430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

