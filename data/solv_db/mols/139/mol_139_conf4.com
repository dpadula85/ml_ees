%nproc=16
%mem=2GB
%chk=mol_139_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88920        0.48190       -1.35250
C         -1.54940       -0.11950       -0.04110
C         -0.07810       -0.24640        0.23880
C          0.50300       -1.12750       -0.84620
C          0.08130       -0.98120        1.54520
C          0.63450        1.06780        0.22060
C          2.09110        0.97190        0.49450
H         -1.04550        0.84320       -1.94780
H         -2.44930       -0.23440       -2.02460
H         -2.59750        1.35820       -1.26150
H         -2.07880       -1.09180        0.03930
H         -2.02960        0.52000        0.75800
H          1.55920       -1.34560       -0.71330
H          0.34850       -0.64320       -1.85440
H         -0.03410       -2.11450       -0.81840
H         -0.85200       -0.90890        2.15020
H          0.89460       -0.61100        2.17060
H          0.25240       -2.08120        1.37610
H          0.50400        1.50340       -0.79550
H          0.21110        1.75980        1.00220
H          2.63930        0.30870       -0.20530
H          2.35230        0.69800        1.52950
H          2.53220        1.99230        0.33550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_139_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

