%nproc=16
%mem=2GB
%chk=mol_512_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51480        0.60300        0.04350
C         -1.98060       -0.78430        0.06860
C         -0.90320       -1.02790       -0.96390
C          0.26230       -0.09400       -0.72560
C          0.87900       -0.28040        0.62700
C          2.02990        0.70760        0.74370
C          3.05270        0.47570       -0.30190
O          4.02150        1.20280       -0.31310
H         -2.21210        1.19250       -0.85060
H         -2.18500        1.21530        0.92950
H         -3.63740        0.63250        0.09460
H         -1.64590       -1.04510        1.07370
H         -2.82910       -1.47490       -0.21140
H         -1.31200       -0.77960       -1.96480
H         -0.54430       -2.07480       -0.93340
H         -0.09950        0.95140       -0.80370
H          1.01860       -0.22210       -1.52220
H          0.12430        0.02830        1.39390
H          1.23930       -1.29290        0.82110
H          2.54970        0.60720        1.73580
H          1.68330        1.75160        0.67610
H          3.00310       -0.29220       -1.05720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

