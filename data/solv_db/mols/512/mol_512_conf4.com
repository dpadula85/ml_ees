%nproc=16
%mem=2GB
%chk=mol_512_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.99150       -1.01330       -0.48400
C         -2.26490        0.24010       -0.03670
C         -0.79530        0.02590       -0.35030
C          0.05330        1.18590        0.03730
C          1.48620        0.89480       -0.32240
C          1.98910       -0.31610        0.38800
C          3.43270       -0.57070        0.01740
O          3.75260       -1.07450       -1.04250
H         -3.98760       -0.76790       -0.91940
H         -2.43960       -1.54870       -1.28240
H         -3.10390       -1.74850        0.34740
H         -2.42380        0.36520        1.04170
H         -2.67070        1.12330       -0.58000
H         -0.66080       -0.18370       -1.41210
H         -0.48460       -0.85140        0.26020
H         -0.22020        2.12940       -0.48730
H         -0.01040        1.43270        1.12590
H          2.12770        1.78710       -0.17360
H          1.58400        0.67190       -1.42610
H          1.94300       -0.22780        1.48460
H          1.45840       -1.25000        0.08290
H          4.22660       -0.30370        0.70330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

