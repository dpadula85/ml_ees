%nproc=16
%mem=2GB
%chk=mol_512_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.23620        0.33550       -0.33910
C         -1.82190       -0.16460       -0.40120
C         -1.16640        0.04880        0.93660
C          0.25310       -0.45300        0.91660
C          1.10370        0.26410       -0.11190
C          2.48560       -0.37070        0.00500
C          3.42750        0.24040       -0.95050
O          4.32880        0.93730       -0.54570
H         -3.71990        0.36470       -1.32190
H         -3.78820       -0.41760        0.29110
H         -3.29750        1.36840        0.09010
H         -1.21650        0.38060       -1.14870
H         -1.86230       -1.23870       -0.63140
H         -1.76180       -0.48580        1.68970
H         -1.20360        1.13950        1.12780
H          0.31020       -1.52310        0.67000
H          0.70320       -0.22960        1.91220
H          0.77110        0.11670       -1.14340
H          1.11380        1.32180        0.11160
H          2.46840       -1.46790       -0.18620
H          2.80370       -0.23630        1.06270
H          3.30520        0.06960       -1.99800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

