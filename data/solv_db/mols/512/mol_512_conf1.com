%nproc=16
%mem=2GB
%chk=mol_512_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.66000        0.64320        0.88640
C         -1.66230        0.34940       -0.21070
C         -0.87460       -0.88410        0.16240
C          0.14090       -1.24740       -0.87450
C          1.16360       -0.18820       -1.13160
C          1.91950        0.10270        0.14500
C          2.95570        1.15610       -0.05710
O          3.09080        1.65460       -1.16070
H         -3.60700        0.06740        0.73030
H         -2.26970        0.34920        1.87120
H         -2.96660        1.70930        0.91140
H         -0.97920        1.21570       -0.36080
H         -2.23120        0.10170       -1.13490
H         -0.45470       -0.79830        1.18310
H         -1.60900       -1.72360        0.20600
H          0.65080       -2.17150       -0.54520
H         -0.41110       -1.50810       -1.80570
H          1.90210       -0.60270       -1.85020
H          0.77470        0.73420       -1.55110
H          1.17500        0.41850        0.90510
H          2.39510       -0.82630        0.54690
H          3.55730        1.44850        0.77820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

