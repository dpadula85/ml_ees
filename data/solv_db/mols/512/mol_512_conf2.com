%nproc=16
%mem=2GB
%chk=mol_512_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99550        0.40990        0.60310
C          2.43280       -0.41040       -0.49760
C          1.01280       -0.80690       -0.40870
C          0.02970        0.31940       -0.34900
C         -1.36020       -0.29850       -0.27210
C         -2.45140        0.74070       -0.20500
C         -3.74710        0.02040       -0.13540
O         -4.64060        0.36680        0.62040
H          4.12030        0.28280        0.55630
H          2.85420        1.50590        0.49260
H          2.72960        0.06440        1.62960
H          3.05180       -1.35250       -0.56490
H          2.67280        0.12240       -1.45860
H          0.79670       -1.47360       -1.27320
H          0.85140       -1.45050        0.49370
H          0.13020        1.05830       -1.14640
H          0.23960        0.86700        0.61730
H         -1.37810       -0.94820        0.62330
H         -1.52290       -0.88060       -1.20210
H         -2.45940        1.36160       -1.10960
H         -2.38110        1.36230        0.72180
H         -3.97660       -0.86080       -0.75430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_512_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

