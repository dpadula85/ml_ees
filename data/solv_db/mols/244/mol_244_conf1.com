%nproc=16
%mem=2GB
%chk=mol_244_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92650       -0.21920       -0.05390
C         -1.32950        1.01710       -0.19510
C          0.04440        1.15300       -0.15390
C          0.88400        0.07190        0.02860
C          0.29000       -1.16190        0.16970
C         -1.08450       -1.29680        0.12830
C          2.33190        0.25020        0.06730
O          2.88090        1.37150       -0.06060
H         -3.00630       -0.30280       -0.08940
H         -1.96880        1.89460       -0.34160
H          0.51510        2.12870       -0.26540
H          0.96020       -2.00420        0.31260
H         -1.54440       -2.28000        0.24130
H          2.95330       -0.62200        0.21220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_244_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_244_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_244_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

