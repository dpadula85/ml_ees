%nproc=16
%mem=2GB
%chk=mol_244_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88260        0.39650       -0.02630
C         -1.43800       -0.84020        0.37360
C         -0.08920       -1.13260        0.39530
C          0.86200       -0.21630        0.02400
C          0.40490        1.03670       -0.38080
C         -0.94160        1.33420       -0.40440
C          2.28700       -0.48980        0.03480
O          3.14650        0.35450       -0.30640
H         -2.93870        0.67290       -0.05970
H         -2.14760       -1.58590        0.67210
H          0.25450       -2.12740        0.71780
H          1.14650        1.74530       -0.66830
H         -1.29480        2.31240       -0.72060
H          2.63110       -1.46040        0.34900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_244_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_244_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_244_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

