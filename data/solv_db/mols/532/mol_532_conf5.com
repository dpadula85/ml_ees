%nproc=16
%mem=2GB
%chk=mol_532_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.73950       -0.18530       -0.03020
C          0.76770       -0.03300       -0.04030
F          1.14410        0.35110        1.22630
F          1.41050       -1.21110       -0.36870
F          1.04670        0.96050       -0.98150
Cl        -1.40080        1.42570        0.41520
H         -1.08380       -0.86310        0.79130
H         -1.14500       -0.44480       -1.01200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

