%nproc=16
%mem=2GB
%chk=mol_532_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.76160       -0.09380        0.13270
C         -0.75230        0.07150       -0.03310
F         -1.09900        1.38160       -0.18510
F         -1.30430       -0.46990        1.13290
F         -1.21890       -0.67520       -1.08040
Cl         1.65260        0.51480       -1.25420
H          0.92050       -1.19370        0.23570
H          1.03990        0.46460        1.05150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

