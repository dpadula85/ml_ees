%nproc=16
%mem=2GB
%chk=mol_532_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.73060       -0.27300       -0.09300
C         -0.71260        0.15900        0.04020
F         -0.90910        1.44690       -0.42360
F         -1.17040        0.06940        1.34140
F         -1.52050       -0.66200       -0.72250
Cl         1.83140        0.75980        0.81050
H          0.96720       -0.16740       -1.17450
H          0.78330       -1.33260        0.22150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

