%nproc=16
%mem=2GB
%chk=mol_532_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.75060       -0.15870       -0.01940
C          0.76590       -0.01420       -0.05300
F          1.34210       -1.03840       -0.73970
F          1.01060        1.22750       -0.63830
F          1.29270       -0.01210        1.22340
Cl        -1.49080        1.18700        0.88130
H         -1.05560       -1.13080        0.40900
H         -1.11430       -0.06030       -1.06330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

