%nproc=16
%mem=2GB
%chk=mol_532_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.75020       -0.18040       -0.07250
C         -0.75460       -0.02920       -0.04750
F         -1.33360       -1.20700       -0.38780
F         -1.08640        0.31780        1.27060
F         -1.18870        1.01250       -0.81730
Cl         1.41110        1.42910        0.39070
H          1.07350       -0.86860        0.73540
H          1.12850       -0.47420       -1.07170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_532_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

