%nproc=16
%mem=2GB
%chk=mol_106_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.81230       -1.28550       -0.02020
C         -0.31170        0.05400        0.39760
C         -0.88670        1.19750       -0.39440
C          1.19460        0.17160        0.37570
O          1.74040       -0.04480       -0.86990
H         -1.26570       -1.29340       -1.04110
H         -1.64750       -1.60170        0.66670
H         -0.01350       -2.05540        0.02880
H         -0.61420        0.23470        1.46880
H         -1.96260        1.04310       -0.60280
H         -0.71430        2.12180        0.21200
H         -0.30500        1.32950       -1.32760
H          1.42170        1.23760        0.66640
H          1.64640       -0.44990        1.17900
H          2.53040       -0.65910       -0.73890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

