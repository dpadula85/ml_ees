%nproc=16
%mem=2GB
%chk=mol_106_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.69300        1.40320       -0.00410
C         -0.02840        0.10230        0.33830
C         -1.05360       -0.99940        0.30870
C          1.12070       -0.15880       -0.61290
O          1.76120       -1.36260       -0.32160
H         -1.48020        1.24030       -0.75000
H         -1.08930        1.93070        0.89170
H          0.06710        2.06790       -0.49790
H          0.39300        0.17390        1.36190
H         -1.24290       -1.37790       -0.70880
H         -0.77360       -1.79400        1.02580
H         -2.02940       -0.58540        0.67750
H          0.81030       -0.10230       -1.67060
H          1.86010        0.65230       -0.44820
H          2.37780       -1.19020        0.41030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

