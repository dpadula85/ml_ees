%nproc=16
%mem=2GB
%chk=mol_106_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.28750        1.50410        0.15000
C         -0.01670        0.11680       -0.41610
C         -1.32490       -0.66410       -0.25100
C          1.08280       -0.48960        0.41040
O          1.44370       -1.75790        0.01700
H          0.54970        1.84160        0.81810
H         -0.37240        2.26110       -0.66600
H         -1.19460        1.50890        0.80610
H          0.25130        0.21680       -1.48930
H         -1.86130       -0.29520        0.67020
H         -2.00680       -0.43420       -1.09270
H         -1.14550       -1.73470       -0.12510
H          1.94900        0.19770        0.35920
H          0.73680       -0.51080        1.45840
H          2.19640       -1.76050       -0.64910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

