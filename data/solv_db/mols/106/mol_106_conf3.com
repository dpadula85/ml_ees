%nproc=16
%mem=2GB
%chk=mol_106_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.36480        1.48670       -0.16810
C         -0.11330        0.03640       -0.40040
C         -1.34020       -0.82880       -0.21720
C          0.94170       -0.52460        0.55970
O          2.13790        0.12430        0.41290
H         -0.01300        1.85380        0.81490
H         -1.43790        1.75540       -0.24970
H          0.15910        2.13370       -0.92880
H          0.32970       -0.07570       -1.42080
H         -2.27610       -0.32720       -0.46570
H         -1.18400       -1.72060       -0.88800
H         -1.38020       -1.27820        0.81680
H          1.09190       -1.59270        0.29470
H          0.55540       -0.52580        1.60340
H          2.89380       -0.51680        0.23630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

