%nproc=16
%mem=2GB
%chk=mol_106_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.13170        1.60770       -0.06320
C          0.06330        0.17680        0.35300
C         -1.26160       -0.56570        0.42450
C          0.93610       -0.58100       -0.64430
O          1.07970       -1.89690       -0.20540
H          0.17820        2.32960        0.74230
H         -1.23420        1.79640       -0.18490
H          0.43650        1.80800       -0.98280
H          0.60000        0.17740        1.31220
H         -1.21690       -1.54640       -0.10530
H         -1.99940        0.06150       -0.15190
H         -1.59090       -0.74280        1.45720
H          1.90030       -0.05330       -0.74050
H          0.41040       -0.65670       -1.64000
H          1.83040       -1.91470        0.42910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_106_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

