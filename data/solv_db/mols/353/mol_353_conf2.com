%nproc=16
%mem=2GB
%chk=mol_353_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.71200        0.02740        0.39540
C         -1.07740       -1.15850        0.08650
C          0.24990       -1.15670       -0.30220
C          0.99290        0.00390       -0.39970
C          0.35650        1.18860       -0.09050
C         -0.97210        1.18890        0.29890
S          2.71210       -0.02330       -0.90620
H         -2.73400        0.06660        0.69880
H         -1.60280       -2.09320        0.14350
H          0.78700       -2.07840       -0.55530
H          0.94070        2.11410       -0.16580
H         -1.45210        2.13050        0.53710
H          3.51160       -0.20990        0.25950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_353_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_353_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_353_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

