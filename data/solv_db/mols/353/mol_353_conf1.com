%nproc=16
%mem=2GB
%chk=mol_353_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69170        0.02190       -0.18120
C         -1.06460       -1.18590       -0.06070
C          0.30030       -1.18950        0.17200
C          1.01610       -0.02250        0.28100
C          0.36030        1.18510        0.15550
C         -1.01090        1.23210       -0.07880
S          2.79590       -0.07810        0.58460
H         -2.76600        0.03960       -0.36420
H         -1.61750       -2.12710       -0.14420
H          0.83570       -2.11050        0.27300
H          0.94110        2.11480        0.24440
H         -1.49880        2.17030       -0.17270
H          3.40000       -0.05030       -0.70870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_353_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_353_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_353_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

