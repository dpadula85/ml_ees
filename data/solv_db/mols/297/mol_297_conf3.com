%nproc=16
%mem=2GB
%chk=mol_297_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.00990        0.61680        0.02280
C         -1.85850       -0.30270        0.29940
C         -0.78650       -0.15030       -0.75880
C          0.38500       -1.06790       -0.50290
C          1.05240       -0.81930        0.80590
C          1.60340        0.56120        0.95940
C          2.64160        0.91190       -0.07410
I          4.31990       -0.36720       -0.03200
H         -3.59440        0.81500        0.95340
H         -2.70320        1.56440       -0.45240
H         -3.73400        0.12940       -0.67960
H         -2.24590       -1.36010        0.32050
H         -1.50000       -0.08610        1.31880
H         -0.43660        0.89870       -0.71780
H         -1.26790       -0.38810       -1.72900
H          1.07270       -1.09120       -1.35710
H         -0.05430       -2.10770       -0.44680
H          1.82130       -1.61450        0.96230
H          0.30750       -0.97460        1.61360
H          2.05870        0.69860        1.96090
H          0.76890        1.28450        0.88620
H          3.00080        1.95250        0.05800
H          2.15890        0.89670       -1.09520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

