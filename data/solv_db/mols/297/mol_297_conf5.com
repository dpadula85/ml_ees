%nproc=16
%mem=2GB
%chk=mol_297_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73550       -0.90160       -0.24300
C          1.95430        0.16400        0.41810
C          0.99080        0.89300       -0.49590
C         -0.02260       -0.00830       -1.10080
C         -0.93070       -0.76000       -0.26250
C         -1.85690       -0.04520        0.65990
C         -2.78680        0.88250       -0.13800
I         -3.94890       -0.22560       -1.53110
H          3.11860       -1.59410        0.56390
H          3.67710       -0.53300       -0.74130
H          2.21490       -1.52730       -0.95850
H          1.38910       -0.24270        1.26440
H          2.68120        0.95200        0.77410
H          0.59700        1.74880        0.06680
H          1.63020        1.33920       -1.29030
H          0.52860       -0.71210       -1.76480
H         -0.61380        0.61670       -1.83540
H         -0.35670       -1.49360        0.38450
H         -1.54770       -1.43850       -0.89330
H         -2.54050       -0.75270        1.17730
H         -1.40450        0.57160        1.42180
H         -3.39220        1.51280        0.50880
H         -2.11590        1.55410       -0.74420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

