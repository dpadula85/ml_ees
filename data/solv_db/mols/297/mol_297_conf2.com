%nproc=16
%mem=2GB
%chk=mol_297_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.79410        0.03810       -0.56050
C         -1.77630        0.37590        0.50150
C         -0.86530       -0.77570        0.79630
C         -0.07490       -1.24490       -0.38590
C          0.81780       -0.19900       -0.97530
C          1.82910        0.28160        0.06970
C          2.69720        1.33730       -0.59690
I          3.69410        0.47160       -2.26770
H         -3.26350        0.96350       -0.95860
H         -3.62720       -0.50420       -0.03490
H         -2.36480       -0.60010       -1.33640
H         -2.32990        0.60770        1.43560
H         -1.22240        1.30640        0.23980
H         -0.18360       -0.56730        1.61590
H         -1.51010       -1.62920        1.11640
H          0.61220       -2.05440        0.00070
H         -0.67410       -1.72580       -1.17040
H          0.30970        0.63990       -1.44520
H          1.43100       -0.69450       -1.76370
H          1.30070        0.69350        0.94500
H          2.45120       -0.55920        0.42500
H          3.45740        1.65020        0.16810
H          2.08570        2.18860       -0.91040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

