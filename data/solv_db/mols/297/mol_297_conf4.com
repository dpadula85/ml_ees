%nproc=16
%mem=2GB
%chk=mol_297_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.11310        0.37260        0.01890
C          2.28900       -0.53520        0.90800
C          0.94650       -0.75380        0.23430
C          0.30160        0.58690        0.08100
C         -1.04780        0.53730       -0.57620
C         -2.02920       -0.31110        0.21390
C         -3.35500       -0.32980       -0.47900
I         -4.16680        1.63350       -0.66550
H          4.20960        0.23550        0.22300
H          2.89210        0.10360       -1.02980
H          2.84550        1.42990        0.21770
H          2.84630       -1.48210        1.04910
H          2.14340        0.00070        1.86550
H          0.33050       -1.48850        0.78100
H          1.13910       -1.16770       -0.79250
H          0.16540        1.01050        1.10090
H          0.96020        1.21660       -0.53360
H         -1.45690        1.57600       -0.55440
H         -1.03210        0.21780       -1.61740
H         -2.15350        0.18760        1.19250
H         -1.66810       -1.35030        0.28640
H         -4.07050       -0.95750        0.09610
H         -3.20230       -0.73250       -1.50800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

