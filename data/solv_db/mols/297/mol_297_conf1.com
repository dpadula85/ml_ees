%nproc=16
%mem=2GB
%chk=mol_297_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.19850        0.70020       -0.25590
C         -2.07790       -0.21920       -0.64490
C         -0.89220       -0.09680        0.30360
C          0.22250       -1.03190       -0.10880
C          1.40980       -0.95160        0.78830
C          2.02400        0.43090        0.82820
C          2.48810        0.87880       -0.53320
I          3.95970       -0.43380       -1.33720
H         -3.34550        0.72820        0.86110
H         -4.13900        0.26790       -0.66430
H         -3.05270        1.73680       -0.61280
H         -2.46820       -1.24780       -0.68520
H         -1.70630        0.07890       -1.64690
H         -0.54140        0.95530        0.23490
H         -1.27240       -0.30180        1.30860
H          0.51030       -0.82440       -1.15050
H         -0.18480       -2.05350       -0.08140
H          2.19610       -1.62110        0.35080
H          1.18620       -1.31860        1.80420
H          2.93320        0.35720        1.46800
H          1.34280        1.17310        1.24310
H          2.96210        1.87690       -0.44090
H          1.64390        0.91620       -1.26780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_297_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

