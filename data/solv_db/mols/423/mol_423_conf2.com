%nproc=16
%mem=2GB
%chk=mol_423_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.29130        3.28550        0.45530
C         -2.79310        2.06360       -0.25410
O         -1.78520        1.46170        0.49540
P         -1.20310        0.07750       -0.28250
O         -1.84460        0.01080       -1.66340
O         -1.62450       -1.25240        0.66060
C         -2.80830       -1.78020        0.10060
C         -3.30520       -2.97710        0.84110
O          0.46180        0.24920       -0.49780
C          1.39810       -0.74460       -0.17030
C          1.08620       -1.99610       -0.36540
Cl         2.09150       -3.31240        0.19350
C          2.72110       -0.28930        0.25060
C          2.82300        0.98180        0.85080
C          3.99120        1.64390        1.06670
C          5.14400        0.99580        0.65330
C          5.11380       -0.21180        0.03940
C          3.90470       -0.85960       -0.19850
Cl         3.81620       -1.75600       -1.75550
Cl         6.68890        1.79120        0.97360
H         -4.38810        3.34790        0.31130
H         -2.81600        4.22690        0.05870
H         -3.12020        3.17920        1.54990
H         -2.31410        2.39250       -1.21610
H         -3.58560        1.33000       -0.47650
H         -2.52220       -2.02840       -0.96300
H         -3.60030       -1.01020        0.06610
H         -3.35530       -3.82370        0.11710
H         -4.35620       -2.83420        1.19420
H         -2.63870       -3.31300        1.65550
H          0.14680       -2.19520       -0.90840
H          1.89760        1.47230        1.17230
H          4.06690        2.61850        1.52910
H          6.00020       -0.74420       -0.30010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_423_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_423_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_423_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

