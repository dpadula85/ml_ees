%nproc=16
%mem=2GB
%chk=mol_448_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93880        0.57970        0.11410
C          1.25240       -0.74660       -0.00280
O         -0.12000       -0.65280       -0.26710
C         -1.04260       -0.02400        0.54820
O         -0.63310        0.50230        1.59980
C         -2.48490        0.05710        0.23980
H          1.53180        1.31860       -0.58680
H          1.95250        0.93420        1.16450
H          3.00710        0.43060       -0.16740
H          1.71350       -1.27550       -0.87450
H          1.50230       -1.36130        0.88080
H         -2.70180        0.99680       -0.30370
H         -2.83280       -0.81140       -0.37270
H         -3.08310        0.05240        1.17190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

