%nproc=16
%mem=2GB
%chk=mol_448_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.82000       -0.61670        0.41280
C         -1.27840        0.73230        0.12520
O          0.12710        0.81430        0.34530
C          0.93320       -0.02770       -0.40170
O          0.47600       -0.82780       -1.23310
C          2.39800        0.04240       -0.18730
H         -1.05400       -1.39450        0.61380
H         -2.45440       -0.57250        1.32240
H         -2.43560       -0.95260       -0.44790
H         -1.46190        1.07470       -0.93110
H         -1.75670        1.49230        0.76440
H          2.72720        1.11860       -0.11200
H          2.97000       -0.40610       -1.04100
H          2.62940       -0.47660        0.77010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

