%nproc=16
%mem=2GB
%chk=mol_448_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.29110       -0.41490        0.40450
C         -1.16800        0.33960       -0.28140
O          0.03360       -0.10650        0.28620
C          1.28580        0.32330       -0.05140
O          1.35860        1.21010       -0.96600
C          2.48600       -0.21490        0.61160
H         -3.21260        0.20150        0.35790
H         -2.39010       -1.38850       -0.11870
H         -1.97990       -0.56240        1.45860
H         -1.20040        0.21340       -1.38490
H         -1.27730        1.42030       -0.02550
H          2.96900        0.61430        1.18940
H          2.17850       -0.96700        1.35580
H          3.20790       -0.66830       -0.11640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

