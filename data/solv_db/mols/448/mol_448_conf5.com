%nproc=16
%mem=2GB
%chk=mol_448_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96780        0.55830       -0.20260
C          1.24000       -0.74350       -0.33270
O         -0.11590       -0.64140       -0.63330
C         -1.07430       -0.00510        0.13870
O         -0.67430        0.52450        1.20520
C         -2.51010        0.07210       -0.23480
H          2.31720        0.86610       -1.23000
H          2.87280        0.35480        0.44070
H          1.37750        1.39670        0.17010
H          1.71750       -1.29910       -1.18860
H          1.43680       -1.41440        0.55290
H         -3.09750       -0.51190        0.48520
H         -2.64270       -0.31620       -1.27570
H         -2.81490        1.15920       -0.23370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

