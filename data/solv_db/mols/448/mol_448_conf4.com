%nproc=16
%mem=2GB
%chk=mol_448_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90760        0.06260        0.65220
C         -1.12230        0.93210       -0.27800
O          0.28510        0.80370       -0.06810
C          0.92260       -0.42070       -0.21690
O          0.19940       -1.39860       -0.54300
C          2.39060       -0.53290        0.00660
H         -1.24390       -0.58920        1.28640
H         -2.56250        0.66130        1.29470
H         -2.50900       -0.63840        0.00790
H         -1.37410        0.76450       -1.35510
H         -1.38250        1.98660       -0.07220
H          2.58490       -0.63520        1.10160
H          2.88060        0.39090       -0.32040
H          2.83870       -1.38660       -0.52540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_448_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

