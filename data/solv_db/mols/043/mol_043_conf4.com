%nproc=16
%mem=2GB
%chk=mol_043_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.04380       -0.10590       -0.00250
O          0.22220       -0.68960        0.09810
C          1.41110        0.01100        0.00960
O          1.36180        1.24670       -0.17200
H         -1.32130        0.40790        0.94380
H         -1.16270        0.59570       -0.83870
H         -1.79250       -0.91620       -0.14100
H          2.32500       -0.54960        0.10270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

