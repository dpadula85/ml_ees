%nproc=16
%mem=2GB
%chk=mol_043_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.04750       -0.06730        0.00080
O          0.20540       -0.70820        0.03270
C          1.42040       -0.04140        0.01000
O          1.40010        1.20220       -0.04190
H         -1.58200       -0.22530        0.94640
H         -1.70290       -0.51560       -0.80170
H         -0.99910        1.00820       -0.18550
H          2.30570       -0.65270        0.03920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

