%nproc=16
%mem=2GB
%chk=mol_043_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.05160       -0.14720        0.27130
O          0.22560       -0.58080       -0.15120
C          1.37580        0.06530        0.28190
O          1.24760        1.04690        1.06060
H         -1.42860       -0.93460        0.97660
H         -1.00280        0.78330        0.88830
H         -1.73870        0.03490       -0.57030
H          2.37280       -0.26770       -0.04350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

