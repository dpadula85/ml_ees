%nproc=16
%mem=2GB
%chk=mol_043_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.04640       -0.14560       -0.00700
O          0.24560       -0.68730       -0.20650
C          1.39980        0.04480       -0.01590
O          1.26710        1.23220        0.34770
H         -1.17030        0.28690        0.99940
H         -1.27180        0.66650       -0.75470
H         -1.76180       -0.98580       -0.18310
H          2.33780       -0.41170       -0.17990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_043_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

