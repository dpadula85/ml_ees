%nproc=16
%mem=2GB
%chk=mol_045_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.44430        0.96470       -0.00560
C          0.87300       -0.45750       -0.37760
C         -0.35500       -0.90500        0.42740
C         -0.98580        0.42490        0.01730
F         -1.53600        0.31400       -1.22630
F         -1.80800        0.99090        0.93230
F         -0.13410       -0.97690        1.76630
F         -0.92340       -1.97810       -0.18350
F          0.74430       -0.70480       -1.72860
F          2.06690       -0.85480        0.12790
F          0.68110        1.89830       -0.97750
F          0.93250        1.28450        1.22790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

