%nproc=16
%mem=2GB
%chk=mol_045_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.89610        0.46110        0.28050
C         -0.35660        0.88190       -0.44820
C         -1.01190       -0.37700        0.11950
C          0.38980       -0.97780        0.10060
F          0.75330       -1.45160       -1.13020
F          0.73010       -1.71530        1.18490
F         -1.44800       -0.26800        1.40880
F         -1.88160       -0.89770       -0.79100
F         -0.18310        0.78180       -1.80480
F         -0.91940        2.05550       -0.07050
F          2.04060        0.67380       -0.43650
F          0.99090        0.83330        1.58680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

