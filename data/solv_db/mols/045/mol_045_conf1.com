%nproc=16
%mem=2GB
%chk=mol_045_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.95390        0.46110       -0.07340
C          0.49740        0.85250       -0.37710
C          0.89210       -0.36310        0.46850
C         -0.42880       -0.97520       -0.02510
F         -0.34480       -1.50670       -1.28970
F         -1.01720       -1.77270        0.90660
F          2.01150       -1.00790        0.03840
F          0.83020       -0.10210        1.81490
F          0.83470        2.03470        0.19230
F          0.81700        0.68900       -1.70700
F         -1.82710        0.71460       -1.09490
F         -1.31110        0.97570        1.14650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

