%nproc=16
%mem=2GB
%chk=mol_045_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.77040       -0.58550        0.44100
C          0.67070        0.69270       -0.40400
C         -0.81260        0.69010       -0.01070
C         -0.67110       -0.83860       -0.00910
F         -0.71610       -1.30730       -1.29390
F         -1.46220       -1.45390        0.90710
F         -0.99210        1.20220        1.25310
F         -1.65070        1.18080       -0.96480
F          1.40210        1.70780        0.12630
F          0.87120        0.46630       -1.75200
F          0.90080       -0.30050        1.77640
F          1.68950       -1.45390       -0.06930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_045_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

