%nproc=16
%mem=2GB
%chk=mol_080_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.64820       -0.11730       -0.00420
N          0.80860        0.05460       -0.00680
O          1.59590       -0.90100       -0.02000
O          1.23250        1.37220        0.00670
H         -1.11190        0.61720       -0.69780
H         -0.89250       -1.13510       -0.31660
H         -0.98430        0.10940        1.03860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

