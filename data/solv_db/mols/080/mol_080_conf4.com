%nproc=16
%mem=2GB
%chk=mol_080_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.63920        0.00470        0.01440
N          0.79300       -0.07720       -0.02470
O          1.44940       -1.13050       -0.01970
O          1.49660        1.10880       -0.07200
H         -1.13930       -0.97080        0.03660
H         -1.03570        0.53650       -0.89120
H         -0.92500        0.52860        0.95660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

