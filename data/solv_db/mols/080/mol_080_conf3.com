%nproc=16
%mem=2GB
%chk=mol_080_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.63650       -0.03450       -0.02920
N          0.78740        0.08640        0.05160
O          1.35220        1.20020        0.07510
O          1.63470       -0.99810        0.10880
H         -0.88540       -1.01650       -0.49760
H         -1.10510        0.75410       -0.67050
H         -1.14740        0.00840        0.96170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

