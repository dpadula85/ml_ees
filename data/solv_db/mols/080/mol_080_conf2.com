%nproc=16
%mem=2GB
%chk=mol_080_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.66500       -0.03070       -0.00350
N          0.78780       -0.05370       -0.01470
O          1.46710       -1.08320       -0.02390
O          1.40940        1.18670       -0.01470
H         -1.02280        0.50700       -0.89100
H         -0.96320        0.54460        0.89140
H         -1.01330       -1.07060        0.05640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_080_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

