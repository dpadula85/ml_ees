%nproc=16
%mem=2GB
%chk=mol_136_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33730        0.55930        0.22540
C          1.58940        0.09410       -0.98440
C          0.39750       -0.73000       -0.71360
C         -0.70480       -0.14360        0.07340
C         -1.78540       -1.24450        0.16420
C         -1.38070        1.02090       -0.61110
C         -0.41790        0.25210        1.47890
H          3.44230        0.58360        0.05170
H          2.05780        1.61450        0.43580
H          2.20240       -0.03900        1.13050
H          1.30880        0.93460       -1.65150
H          2.31800       -0.55170       -1.56100
H          0.76020       -1.69340       -0.24440
H         -0.02160       -1.05580       -1.70370
H         -1.46390       -1.90220        0.98090
H         -1.81660       -1.75980       -0.82740
H         -2.76850       -0.80050        0.33500
H         -1.64630        0.67510       -1.64470
H         -0.77860        1.93280       -0.59990
H         -2.31850        1.22130       -0.02920
H          0.04490        1.22630        1.63250
H         -1.43570        0.34380        1.97530
H          0.07990       -0.53800        2.08720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

