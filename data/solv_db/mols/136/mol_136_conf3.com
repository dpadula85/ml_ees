%nproc=16
%mem=2GB
%chk=mol_136_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.87180       -0.22980       -0.47810
C          1.47280       -0.71030       -0.15800
C          0.57360        0.49260       -0.14400
C         -0.86650        0.15070        0.16650
C         -1.61750        1.47000        0.13820
C         -1.02270       -0.40690        1.56600
C         -1.46390       -0.80420       -0.82650
H          2.94770        0.88900       -0.39990
H          3.15440       -0.45360       -1.54110
H          3.60880       -0.66720        0.22320
H          1.42040       -1.32240        0.73690
H          1.14480       -1.34970       -1.02760
H          0.89690        1.22530        0.62420
H          0.57010        1.00530       -1.12720
H         -2.11810        1.52900       -0.84960
H         -0.94060        2.34520        0.21390
H         -2.32890        1.55640        0.98230
H         -0.56910       -1.39680        1.68810
H         -0.59780        0.31960        2.26810
H         -2.12310       -0.51520        1.73670
H         -1.87670       -1.70720       -0.30880
H         -0.78670       -1.09990       -1.63560
H         -2.34950       -0.32010       -1.32880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

