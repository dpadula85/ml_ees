%nproc=16
%mem=2GB
%chk=mol_136_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.91020       -0.23170       -0.15040
C          1.55640        0.01110        0.48520
C          0.49860       -0.16810       -0.57990
C         -0.88950        0.04840       -0.06600
C         -1.07660        1.43950        0.48710
C         -1.27790       -0.92900        1.01350
C         -1.84730       -0.15450       -1.22280
H          3.42750        0.74160       -0.19030
H          3.50740       -0.98530        0.40390
H          2.79740       -0.63130       -1.18020
H          1.43660       -0.76420        1.26980
H          1.46680        1.03200        0.90760
H          0.59330       -1.17420       -0.99550
H          0.66450        0.54920       -1.41000
H         -0.77440        1.44110        1.55420
H         -2.15020        1.72260        0.38520
H         -0.44900        2.16770       -0.07230
H         -2.29000       -0.59860        1.38420
H         -1.41980       -1.93800        0.61000
H         -0.61640       -0.88990        1.89940
H         -2.82100        0.29760       -1.03260
H         -1.91060       -1.27130       -1.37730
H         -1.33590        0.28530       -2.12280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

