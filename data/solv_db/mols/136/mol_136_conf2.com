%nproc=16
%mem=2GB
%chk=mol_136_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.85050        0.11740       -0.37370
C          1.48620       -0.22320        0.13970
C          0.38520        0.52470       -0.61430
C         -0.92330        0.06990        0.02460
C         -1.00830       -1.43100       -0.16130
C         -2.11730        0.67380       -0.67540
C         -0.88290        0.46790        1.47310
H          3.31310       -0.82520       -0.74340
H          3.52900        0.47230        0.45280
H          2.84880        0.84030       -1.21160
H          1.44220        0.01600        1.22140
H          1.25890       -1.31420        0.05430
H          0.39990        0.24730       -1.68650
H          0.51310        1.60800       -0.55110
H         -0.52290       -1.74860       -1.09290
H         -2.07440       -1.73500       -0.18230
H         -0.56920       -1.89040        0.75250
H         -2.42290        0.01290       -1.49740
H         -2.96190        0.63630        0.07300
H         -1.98570        1.72310       -0.94610
H         -0.11360        1.26590        1.66300
H         -1.84140        0.90550        1.81750
H         -0.60310       -0.41390        2.06400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

