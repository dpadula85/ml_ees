%nproc=16
%mem=2GB
%chk=mol_136_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30340        0.32180        0.80330
C          1.69380       -0.74090       -0.04010
C          0.47950       -0.38790       -0.80770
C         -0.72590        0.04800       -0.07380
C         -0.61970        1.26330        0.76470
C         -1.31310       -1.05850        0.80510
C         -1.81240        0.27410       -1.14420
H          3.41750        0.28230        0.73430
H          2.03420        1.35120        0.59730
H          2.00880        0.10020        1.87170
H          1.59640       -1.68540        0.54630
H          2.47020       -1.01760       -0.82180
H          0.75280        0.46070       -1.50600
H          0.16020       -1.19620       -1.51820
H         -1.64230        1.67410        1.03080
H         -0.08980        1.19400        1.70070
H         -0.17820        2.10370        0.15670
H         -0.75940       -1.14280        1.74790
H         -1.40740       -2.00310        0.24200
H         -2.34940       -0.72910        1.04060
H         -1.36090        0.94280       -1.89840
H         -2.67770        0.68310       -0.62410
H         -1.98040       -0.73790       -1.58500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_136_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

