%nproc=16
%mem=2GB
%chk=mol_446_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90560       -0.36460       -0.31420
O          0.49940       -0.53120       -0.42120
C         -0.40960        0.42980       -0.03620
O          0.05700        1.49500        0.42840
C         -1.87960        0.19180       -0.17520
Cl        -2.23450       -1.38910       -0.85610
H          2.24990       -0.65260        0.68360
H          2.37220       -1.07150       -1.03370
H          2.17530        0.68680       -0.57750
H         -2.39280        0.24150        0.80780
H         -2.34280        0.96400       -0.85230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

