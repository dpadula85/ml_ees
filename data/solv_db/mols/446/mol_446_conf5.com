%nproc=16
%mem=2GB
%chk=mol_446_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90620       -0.05180       -0.26950
O          0.66540       -0.67170        0.01560
C         -0.41560        0.18610        0.21830
O         -0.26190        1.43280        0.14470
C         -1.76530       -0.34510        0.52380
Cl        -2.91820        0.98680        0.72640
H          2.65570       -0.75510       -0.65590
H          2.30380        0.32350        0.71280
H          1.73460        0.82030       -0.95130
H         -2.15740       -1.01540       -0.26450
H         -1.74740       -0.91050        1.49310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

