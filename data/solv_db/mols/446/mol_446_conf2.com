%nproc=16
%mem=2GB
%chk=mol_446_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93740        0.04950       -0.07090
O         -0.61110        0.42840        0.17950
C          0.47560       -0.26750       -0.33130
O          0.21750       -1.27020       -1.04080
C          1.82240        0.23260        0.00500
Cl         3.11140       -0.73760       -0.70380
H         -2.55230        0.36090        0.82130
H         -2.07960       -1.01990       -0.26260
H         -2.34300        0.67920       -0.91500
H          1.93600        0.26000        1.11540
H          1.96050        1.28440       -0.37770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

