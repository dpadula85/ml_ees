%nproc=16
%mem=2GB
%chk=mol_446_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92390        0.37600       -0.05600
O         -0.54240        0.64750        0.03410
C          0.43190       -0.34620        0.05340
O         -0.02220       -1.52890       -0.02420
C          1.87630       -0.05050        0.18460
Cl         2.71300       -0.01420       -1.36270
H         -2.37700        0.15360        0.91150
H         -2.40340        1.25330       -0.57330
H         -2.14410       -0.47560       -0.73630
H          2.32370       -0.87860        0.78080
H          2.06810        0.86360        0.78820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

