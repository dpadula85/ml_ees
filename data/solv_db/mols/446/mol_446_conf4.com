%nproc=16
%mem=2GB
%chk=mol_446_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90010       -0.00370       -0.25570
O          0.51960        0.19600       -0.44430
C         -0.45500       -0.18520        0.43030
O         -0.13150       -0.76270        1.49850
C         -1.88000        0.08670        0.10850
Cl        -2.03170        0.91120       -1.46080
H          2.43230        0.66570       -0.96510
H          2.12680       -1.05160       -0.58570
H          2.26190        0.22920        0.74480
H         -2.47840       -0.84600        0.04580
H         -2.26410        0.76040        0.88380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_446_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

