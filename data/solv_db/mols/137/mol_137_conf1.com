%nproc=16
%mem=2GB
%chk=mol_137_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.46500        0.02440        0.15550
C         -2.72660        1.17910        0.02730
C         -1.34530        1.20860       -0.05290
C         -0.61780        2.38240       -0.18190
C          0.75840        2.40140       -0.26110
C          1.41020        1.18540       -0.20670
C          2.79000        1.19420       -0.28530
C          3.43430       -0.02760       -0.23020
C          2.72260       -1.20550       -0.10170
C          1.32730       -1.21520       -0.02220
C          0.68780        0.00740       -0.07770
C         -0.68650       -0.00080        0.00050
C         -1.39480       -1.17450        0.12850
C         -2.78020       -1.18310        0.20740
C         -0.74990       -2.38900        0.18310
C          0.62460       -2.39170        0.10570
H         -4.55600        0.03420        0.21630
H         -3.23950        2.13480       -0.01530
H         -1.15790        3.31500       -0.22140
H          1.31650        3.33440       -0.36240
H          3.33870        2.13270       -0.38670
H          4.52460       -0.07860       -0.28800
H          3.27660       -2.13370       -0.06340
H         -3.35640       -2.08940        0.30780
H         -1.32080       -3.32090        0.28520
H          1.18510       -3.32400        0.14400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_137_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_137_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_137_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

