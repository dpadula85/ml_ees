%nproc=16
%mem=2GB
%chk=mol_623_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.35290        0.04380        0.28760
C          1.87880       -0.00310        0.39810
C          1.27660       -0.62200        1.40390
C         -0.20260       -0.60500        1.44590
C         -0.86690       -0.85160        0.10530
C          0.10770       -0.67310       -1.03480
C          0.96500        0.53630       -0.65900
O          1.63700        1.05700       -1.73580
C         -2.00040        0.09500       -0.08110
C         -2.13690        0.95920       -1.06580
C         -3.06800        0.01440        0.99280
H          3.72660        0.71010        1.09980
H          3.82630       -0.94670        0.47370
H          3.67690        0.38920       -0.69520
H          1.84570       -1.13930        2.19460
H         -0.49940        0.39440        1.86030
H         -0.55620       -1.33550        2.22610
H         -1.23600       -1.90550        0.16050
H         -0.37190       -0.59270       -2.00740
H          0.79350       -1.57980       -1.00900
H          0.35560        1.32930       -0.18960
H          1.85060        2.00200       -1.53460
H         -3.03510        1.58580       -1.07000
H         -1.38480        1.08550       -1.81550
H         -2.56870       -0.09000        1.98870
H         -3.61630        0.96820        0.98130
H         -3.74980       -0.82600        0.79360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

