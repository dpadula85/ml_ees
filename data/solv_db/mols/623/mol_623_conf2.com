%nproc=16
%mem=2GB
%chk=mol_623_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.39090       -0.82190        0.40730
C          1.98250       -0.47300        0.01880
C          1.10890       -1.43940       -0.18560
C         -0.28440       -1.16200       -0.58140
C         -0.71300        0.25650       -0.36680
C          0.17430        0.97660        0.58800
C          1.57890        0.95430       -0.04660
O          1.50230        1.36750       -1.35900
C         -2.11740        0.28930        0.14100
C         -2.38480        0.88870        1.27760
C         -3.19800       -0.36150       -0.65330
H          3.51620       -0.61440        1.48980
H          3.63220       -1.86480        0.16150
H          4.07670       -0.18050       -0.18810
H          1.39010       -2.50180       -0.06450
H         -0.39240       -1.39890       -1.67310
H         -0.95830       -1.87680       -0.04590
H         -0.64540        0.79930       -1.34150
H         -0.15560        2.02720        0.72600
H          0.26080        0.49780        1.56350
H          2.23840        1.58440        0.57890
H          1.36990        2.34940       -1.36720
H         -1.64750        1.37610        1.90400
H         -3.43580        0.90150        1.62820
H         -4.16980        0.09170       -0.45400
H         -2.94160       -0.21070       -1.72730
H         -3.17800       -1.45470       -0.43030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

