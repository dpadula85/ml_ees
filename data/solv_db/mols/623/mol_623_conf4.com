%nproc=16
%mem=2GB
%chk=mol_623_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.01210       -0.62780        0.77170
C          1.74990       -0.32500       -0.00300
C          0.97440       -1.28570       -0.46600
C         -0.23590       -0.92030       -1.20960
C         -0.95990        0.26420       -0.68080
C         -0.08150        1.36330       -0.18020
C          1.38800        1.06460       -0.25040
O          1.88270        1.41610       -1.52870
C         -1.90960       -0.14240        0.36930
C         -2.00540       -1.34500        0.90510
C         -2.88130        0.88800        0.90780
H          3.30270       -1.69370        0.61090
H          2.73620       -0.52540        1.83990
H          3.81690        0.06500        0.47510
H          1.21410       -2.32920       -0.31410
H          0.09060       -0.69010       -2.27650
H         -0.86300       -1.82070       -1.33880
H         -1.54670        0.68140       -1.55550
H         -0.33410        1.60110        0.87360
H         -0.24110        2.29940       -0.75290
H          1.93770        1.71720        0.45860
H          2.34820        0.66340       -1.95810
H         -2.72980       -1.59660        1.67820
H         -1.31380       -2.11400        0.57990
H         -3.73830        0.87340        0.19080
H         -3.21210        0.63530        1.91320
H         -2.40100        1.88360        0.94030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

