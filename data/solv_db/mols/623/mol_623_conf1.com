%nproc=16
%mem=2GB
%chk=mol_623_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.40230        0.57450       -0.29580
C          1.91410        0.39470       -0.32860
C          1.11650        1.45650       -0.38010
C         -0.37300        1.24720       -0.40840
C         -0.72560        0.20970        0.64520
C          0.03410       -1.06620        0.46040
C          1.28580       -0.93700       -0.32650
O          2.25410       -1.87870        0.02780
C         -2.17700       -0.01710        0.54360
C         -2.95450        0.27280        1.56290
C         -2.76410       -0.57340       -0.69370
H          3.73360        0.59270        0.75090
H          3.93940       -0.18550       -0.84390
H          3.61970        1.56380       -0.74110
H          1.50660        2.46460       -0.40210
H         -0.67080        0.93020       -1.40700
H         -0.90950        2.17550       -0.14210
H         -0.48560        0.63570        1.64500
H         -0.63880       -1.86850        0.04350
H          0.31200       -1.45360        1.47670
H          1.03100       -1.16960       -1.40170
H          2.90500       -1.94310       -0.73170
H         -2.49730        0.67860        2.46780
H         -4.03490        0.12900        1.55280
H         -2.50260       -1.66920       -0.75940
H         -3.88480       -0.49850       -0.69790
H         -2.43550       -0.06490       -1.61650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

