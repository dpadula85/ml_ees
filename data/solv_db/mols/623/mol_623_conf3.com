%nproc=16
%mem=2GB
%chk=mol_623_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.43360        0.79260       -0.48870
C          1.96960        0.49040       -0.27680
C          1.11500        1.31080       -0.80300
C         -0.32490        1.15170       -0.68500
C         -0.76780        0.09950        0.28820
C          0.14720       -1.07770        0.35530
C          1.59840       -0.69270        0.49520
O          1.91260       -0.55540        1.86670
C         -2.15320       -0.36570        0.09720
C         -2.45720       -1.64550        0.01870
C         -3.29560        0.60410       -0.01780
H          3.71870        1.71370        0.07460
H          4.05540       -0.03550       -0.09990
H          3.66070        0.87790       -1.57020
H          1.46470        2.18850       -1.37620
H         -0.75200        0.89850       -1.68940
H         -0.77260        2.15590       -0.43000
H         -0.73220        0.60260        1.30260
H         -0.11340       -1.71050        1.22200
H          0.06600       -1.72670       -0.54800
H          2.20430       -1.55200        0.13920
H          1.95380       -1.48120        2.23270
H         -3.46070       -2.01720       -0.12600
H         -1.69180       -2.39800        0.13480
H         -3.81020        0.41290       -0.98100
H         -4.00720        0.31670        0.81120
H         -2.96110        1.64210        0.05360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_623_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

