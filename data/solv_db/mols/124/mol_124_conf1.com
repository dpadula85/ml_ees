%nproc=16
%mem=2GB
%chk=mol_124_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72830       -0.07090        0.14240
C         -0.97580       -1.22870        0.04970
C          0.39960       -1.15800       -0.06440
C          1.07860        0.04110       -0.09130
C          0.31710        1.19600        0.00200
C         -1.05930        1.13580        0.11650
C          2.50830        0.08720       -0.21060
N          3.66740        0.12270       -0.28830
H         -2.81420       -0.08330        0.23360
H         -1.50970       -2.17120        0.07090
H          0.97590       -2.07880       -0.13630
H          0.80460        2.15740       -0.01460
H         -1.66410        2.05070        0.19050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_124_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_124_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_124_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

