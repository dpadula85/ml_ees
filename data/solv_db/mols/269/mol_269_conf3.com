%nproc=16
%mem=2GB
%chk=mol_269_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.70850       -1.25380        0.01190
C          3.94180        0.05730        0.34680
C          2.90210        0.94450        0.47150
C          1.61760        0.49150        0.25330
C          1.37750       -0.81980       -0.08220
C          2.44220       -1.72190       -0.20830
C          0.01690       -1.28590       -0.31010
O         -0.16700       -2.49440       -0.61620
C         -1.06840       -0.33240       -0.17360
C         -2.34300       -0.80460       -0.39590
C         -3.39590        0.06930       -0.27590
C         -3.23600        1.37550        0.05160
C         -1.94640        1.89480        0.28650
C         -0.87520        1.01180        0.16580
C          0.49180        1.44500        0.38640
O          0.77530        2.63390        0.69620
O         -1.79800        3.19470        0.61150
N         -2.51150       -2.17220       -0.73900
H          4.52040       -1.98660       -0.09520
H          4.97830        0.33310        0.50130
H          3.05930        1.97120        0.73160
H          2.19530       -2.72660       -0.47080
H         -4.39180       -0.31490       -0.45320
H         -4.06420        2.08160        0.15060
H         -1.08450        3.77810        0.81260
H         -2.54620       -2.85010        0.05940
H         -2.59900       -2.51920       -1.71650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

