%nproc=16
%mem=2GB
%chk=mol_269_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.89440        0.65320        0.23020
C          3.96520       -0.72410        0.21600
C          2.78930       -1.43750        0.14050
C          1.60200       -0.75280        0.08280
C          1.52280        0.63610        0.09670
C          2.69800        1.33080        0.17190
C          0.23980        1.34020        0.03380
O          0.15630        2.58010        0.04540
C         -0.97610        0.53970       -0.04530
C         -2.19700        1.17900       -0.10570
C         -3.38660        0.47320       -0.18220
C         -3.29900       -0.91080       -0.19550
C         -2.06590       -1.56390       -0.13450
C         -0.91580       -0.84270       -0.06010
C          0.35640       -1.54200        0.00230
O          0.40370       -2.81450       -0.01180
O         -1.99990       -2.94390       -0.14900
N         -2.22550        2.59200       -0.08870
H          4.82010        1.21240        0.28950
H          4.93890       -1.23520        0.26390
H          2.86950       -2.51710        0.13070
H          2.68660        2.40930        0.18560
H         -4.32860        1.00030       -0.22840
H         -4.21060       -1.48730       -0.25450
H         -2.85570       -3.47810       -0.20430
H         -2.12420        3.17870        0.76440
H         -2.35810        3.12490       -0.99380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

