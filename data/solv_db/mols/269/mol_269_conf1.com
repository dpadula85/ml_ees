%nproc=16
%mem=2GB
%chk=mol_269_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.80720       -1.31220       -0.04250
C          4.00250        0.00360       -0.39190
C          2.97390        0.91250       -0.51920
C          1.67600        0.48340       -0.28430
C          1.45490       -0.83320        0.06790
C          2.51620       -1.73670        0.19060
C          0.07730       -1.27880        0.31500
O         -0.16820       -2.46790        0.63760
C         -1.02900       -0.33770        0.18750
C         -2.33870       -0.76310        0.42300
C         -3.40960        0.09470        0.31280
C         -3.19620        1.39700       -0.03500
C         -1.90910        1.84760       -0.27460
C         -0.83030        0.98190       -0.16320
C          0.53940        1.41060       -0.40540
O          0.75810        2.60560       -0.72680
O         -1.63290        3.15240       -0.62920
N         -2.51960       -2.12770        0.78360
H          4.66580       -1.97440        0.03970
H          5.02910        0.32280       -0.57260
H          3.16660        1.93860       -0.79550
H          2.31490       -2.75120        0.46490
H         -4.41090       -0.26780        0.50210
H         -3.99050        2.13340       -0.14170
H         -2.36080        3.84980       -0.73280
H         -2.38020       -2.48890        1.74930
H         -2.80600       -2.79430        0.04090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

