%nproc=16
%mem=2GB
%chk=mol_269_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.74710        1.20860       -0.03060
C         -3.93710       -0.10850        0.34850
C         -2.88960       -0.97950        0.50310
C         -1.59910       -0.54960        0.27850
C         -1.41150        0.76380       -0.09950
C         -2.46860        1.65650       -0.25920
C         -0.05960        1.24540       -0.34280
O          0.16320        2.44940       -0.69350
C          1.07220        0.35300       -0.18860
C          2.35450        0.82640       -0.42330
C          3.44610       -0.00050       -0.28330
C          3.22620       -1.29300        0.09150
C          1.96080       -1.83850        0.34460
C          0.87450       -0.98100        0.19580
C         -0.46470       -1.48160        0.44330
O         -0.75980       -2.63810        0.78670
O          1.84480       -3.13260        0.71130
N          2.48540        2.18720       -0.81010
H         -4.55810        1.90730       -0.15700
H         -4.98110       -0.40860        0.51710
H         -3.06800       -1.99760        0.79880
H         -2.22730        2.66570       -0.55760
H          4.43530        0.39710       -0.47300
H          4.08270       -1.94860        0.20340
H          1.15730       -3.74490        0.93590
H          2.56880        2.50930       -1.78580
H          2.49980        2.93300       -0.05410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_269_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

