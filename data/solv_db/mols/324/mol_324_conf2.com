%nproc=16
%mem=2GB
%chk=mol_324_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.75870        0.20120        0.43360
C         -0.63100        0.18800       -0.24100
C          0.62250       -0.18840        0.39430
C          1.74550       -0.20260       -0.27360
H         -1.76060       -0.06840        1.46730
H         -2.67660        0.47840       -0.03800
H         -0.58330        0.45560       -1.29690
H          0.61640       -0.45950        1.43710
H          1.73520        0.07160       -1.31700
H          2.69060       -0.47600        0.16580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

