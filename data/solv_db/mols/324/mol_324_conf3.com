%nproc=16
%mem=2GB
%chk=mol_324_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.77220       -0.11700       -0.26820
C          0.61680       -0.22650        0.33930
C         -0.60640        0.21290       -0.31100
C         -1.78550        0.12670        0.24950
H          2.66970       -0.46430        0.26680
H          1.82850        0.29830       -1.25390
H          0.51290       -0.64030        1.33590
H         -0.48410        0.62870       -1.31610
H         -2.65630        0.46340       -0.26930
H         -1.86790       -0.28190        1.22720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

