%nproc=16
%mem=2GB
%chk=mol_324_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.80290       -0.17780        0.19650
C          0.53450       -0.41800        0.42530
C         -0.52160        0.45680       -0.08570
C         -1.78590        0.16480        0.17670
H          2.54220       -0.84290        0.58870
H          2.05770        0.69820       -0.38960
H          0.29050       -1.28800        1.00700
H         -0.30750        1.33700       -0.67230
H         -2.03490       -0.71530        0.76520
H         -2.57800        0.78510       -0.18330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_324_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

