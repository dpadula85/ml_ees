%nproc=16
%mem=2GB
%chk=mol_299_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.74210       -0.68380        0.11780
C          1.38700       -0.07670        0.09340
C          1.28320        1.28720        0.26600
C          0.01970        1.83960        0.24120
C         -1.10430        1.04220        0.04840
C         -1.00020       -0.32000       -0.12390
C          0.26030       -0.86770       -0.09870
C         -2.21550       -1.14990       -0.32830
O         -2.34590        1.67460        0.03590
H          3.05320       -0.98630        1.13290
H          3.50850        0.04260       -0.24180
H          2.75530       -1.61140       -0.51460
H          2.17790        1.89330        0.41590
H         -0.05730        2.90720        0.37670
H          0.33600       -1.94760       -0.23600
H         -2.53030       -1.14630       -1.39640
H         -3.05240       -0.83220        0.34330
H         -2.01520       -2.21970       -0.03240
H         -3.20210        1.15480       -0.09940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

