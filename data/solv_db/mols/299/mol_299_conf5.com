%nproc=16
%mem=2GB
%chk=mol_299_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.64320        1.01010       -0.22030
C          1.34640        0.24960       -0.20480
C          1.33620       -1.12070       -0.23530
C          0.13310       -1.77920       -0.21940
C         -1.02640       -1.05270       -0.17360
C         -1.05550        0.32280       -0.14200
C          0.17050        0.95180       -0.15910
C         -2.33020        1.07220       -0.09190
O         -2.26270       -1.72850       -0.15740
H          3.44830        0.28680        0.01520
H          2.76530        1.51420       -1.18930
H          2.54600        1.74200        0.63410
H          2.29210       -1.68250       -0.27230
H          0.09890       -2.86270       -0.24290
H          0.23090        2.04400       -0.13620
H         -3.14580        0.41450        0.29600
H         -2.65610        1.41240       -1.08810
H         -2.24980        1.95480        0.59150
H         -2.28440       -2.74880       -0.17990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

