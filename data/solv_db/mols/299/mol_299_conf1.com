%nproc=16
%mem=2GB
%chk=mol_299_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.61060       -1.18540       -0.01700
C         -1.37760       -0.34870       -0.02060
C         -1.46430        1.02800       -0.06570
C         -0.32960        1.81810       -0.07030
C          0.93240        1.26620       -0.03040
C          1.02660       -0.10890        0.01480
C         -0.12020       -0.89610        0.01910
C          2.37020       -0.75480        0.05820
O          2.08380        2.06810       -0.03500
H         -2.38060       -2.21790       -0.32150
H         -2.97880       -1.24700        1.04100
H         -3.40850       -0.71220       -0.60220
H         -2.44850        1.50680       -0.09850
H         -0.39110        2.90480       -0.10570
H         -0.05260       -1.97350        0.05470
H          3.02600       -0.30680       -0.69810
H          2.84130       -0.61490        1.06280
H          2.29860       -1.83680       -0.18110
H          2.98350        1.61090       -0.00440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

