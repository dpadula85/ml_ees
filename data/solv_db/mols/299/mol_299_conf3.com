%nproc=16
%mem=2GB
%chk=mol_299_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.76640       -0.71230        0.24790
C          1.42490       -0.09760        0.05500
C          1.27600        1.22520       -0.30730
C          0.02940        1.79390       -0.48600
C         -1.12750        1.06370       -0.31000
C         -0.99020       -0.26020        0.05230
C          0.26120       -0.82410        0.22980
C         -2.21520       -1.07960        0.25120
O         -2.39130        1.62810       -0.48780
H          2.65640       -1.77750        0.46370
H          3.30930       -0.60160       -0.71380
H          3.30900       -0.25530        1.09560
H          2.18270        1.81770       -0.45060
H         -0.08410        2.82990       -0.76960
H          0.37220       -1.87230        0.51670
H         -2.67440       -1.34410       -0.72490
H         -2.97970       -0.57980        0.85830
H         -1.91900       -2.00510        0.82620
H         -3.20610        1.05080       -0.34680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

