%nproc=16
%mem=2GB
%chk=mol_299_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.80730       -0.58800       -0.13040
C          1.42990       -0.02940       -0.08340
C          1.17780        1.32440       -0.16120
C         -0.10320        1.83660       -0.11700
C         -1.20140        1.01300        0.00790
C         -0.95430       -0.34160        0.08600
C          0.33360       -0.85880        0.04180
C         -2.14200       -1.23040        0.22080
O         -2.49290        1.53660        0.05170
H          3.49070        0.16830       -0.55500
H          2.80350       -1.54630       -0.67550
H          3.17080       -0.78310        0.91170
H          2.05650        1.97570       -0.26080
H         -0.27070        2.90890       -0.18070
H          0.50230       -1.91730        0.10430
H         -1.78820       -2.26350        0.39540
H         -2.66350       -1.25170       -0.76770
H         -2.85940       -0.88340        0.96900
H         -3.29680        0.93000        0.14320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_299_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

