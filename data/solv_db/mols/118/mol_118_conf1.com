%nproc=16
%mem=2GB
%chk=mol_118_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.71070        1.76220        0.06310
C          0.19010        0.58580        0.05430
C          1.55570        0.78400        0.16990
C          2.36990       -0.32160        0.15930
N          1.87190       -1.54800        0.04220
C          0.53530       -1.74700       -0.07120
C         -0.32790       -0.67530       -0.06650
C         -1.78800       -0.88740       -0.19010
H         -1.25090        1.74660       -0.92020
H         -0.13840        2.69060        0.20130
H         -1.40730        1.64480        0.90150
H          1.94970        1.76690        0.26320
H          3.42620       -0.15880        0.24920
H          0.21310       -2.76200       -0.16120
H         -2.03820       -1.17490       -1.22880
H         -2.35860        0.01750        0.05620
H         -2.09180       -1.72350        0.47770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

