%nproc=16
%mem=2GB
%chk=mol_118_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07840        1.54250       -0.18210
C          0.07690        0.61800       -0.03720
C          1.38400        1.08160        0.02150
C          2.45670        0.22330        0.15600
N          2.15780       -1.08690        0.22630
C          0.90310       -1.56900        0.17250
C         -0.18740       -0.73750        0.03870
C         -1.55400       -1.25490       -0.02080
H         -0.75400        2.60470       -0.05640
H         -1.88510        1.30800        0.57050
H         -1.53510        1.47870       -1.19720
H          1.57170        2.15340       -0.04000
H          3.48630        0.58870        0.20220
H          0.70410       -2.64880        0.23390
H         -1.81050       -1.71070        0.97890
H         -1.66400       -2.10280       -0.75110
H         -2.27230       -0.48840       -0.31580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

