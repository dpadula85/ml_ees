%nproc=16
%mem=2GB
%chk=mol_118_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.60070        1.81430        0.02360
C          0.18940        0.59530       -0.00260
C          1.56460        0.67760       -0.02870
C          2.34210       -0.48260       -0.05410
N          1.76230       -1.69580       -0.05360
C          0.42300       -1.79490       -0.02830
C         -0.37560       -0.64200       -0.00260
C         -1.82710       -0.80780        0.02410
H         -0.05790        2.56910        0.65970
H         -1.60420        1.72790        0.45750
H         -0.60870        2.28360       -1.00190
H          2.07770        1.63470       -0.03020
H          3.41930       -0.38110       -0.07430
H         -0.00060       -2.78930       -0.02880
H         -2.17150       -1.04400        1.06800
H         -2.41060        0.03310       -0.34210
H         -2.12160       -1.69820       -0.58590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

