%nproc=16
%mem=2GB
%chk=mol_118_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15850       -1.51700        0.23070
C          0.00740       -0.59920        0.08460
C          1.25180       -1.18150        0.02970
C          2.35960       -0.37110       -0.10490
N          2.21740        0.96420       -0.18070
C          1.00640        1.53700       -0.12760
C         -0.12040        0.76430        0.00610
C         -1.45520        1.38280        0.06520
H         -2.07500       -0.89760        0.34670
H         -1.25800       -2.19640       -0.64160
H         -1.00690       -2.16690        1.11450
H          1.37460       -2.27250        0.09100
H          3.35530       -0.84180       -0.14860
H          0.89710        2.61800       -0.18930
H         -1.33610        2.48870        0.20420
H         -2.01250        1.04290        0.97360
H         -2.04710        1.24590       -0.86440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

