%nproc=16
%mem=2GB
%chk=mol_118_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.57270        1.83270        0.16680
C          0.22160        0.56000        0.03310
C          1.59780        0.62250       -0.00240
C          2.38450       -0.51780       -0.12450
N          1.75570       -1.70910       -0.20890
C          0.41250       -1.78190       -0.17530
C         -0.39740       -0.67040       -0.05500
C         -1.88560       -0.76370       -0.01890
H         -0.45030        2.42390       -0.77330
H         -0.11350        2.42100        0.99740
H         -1.60700        1.63990        0.44260
H          2.10920        1.58350        0.06550
H          3.46270       -0.42830       -0.14880
H         -0.12030       -2.74070       -0.24240
H         -2.20040       -1.75680       -0.38920
H         -2.38030       -0.00530       -0.62300
H         -2.21650       -0.70970        1.05630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_118_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

