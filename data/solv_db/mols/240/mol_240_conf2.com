%nproc=16
%mem=2GB
%chk=mol_240_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.66140       -0.55590       -0.23360
C          1.89690       -1.71350       -0.26260
C          0.52570       -1.59520       -0.15520
C         -0.07930       -0.35190       -0.02140
C          0.67090        0.80570        0.00860
C          2.05040        0.67480       -0.10030
C          0.05160        2.03270        0.14260
C         -1.31660        2.16430        0.25120
C         -2.07320        1.01390        0.22230
C         -1.44470       -0.22870        0.08640
N         -2.24590       -1.40660        0.05850
H          3.73590       -0.67810       -0.31940
H          2.38080       -2.68780       -0.36840
H         -0.08850       -2.50270       -0.17700
H          2.65020        1.57920       -0.07770
H          0.65920        2.93630        0.16410
H         -1.81010        3.12110        0.35560
H         -3.16270        1.05710        0.30490
H         -2.75960       -1.69950       -0.81300
H         -2.30230       -1.96500        0.93440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

