%nproc=16
%mem=2GB
%chk=mol_240_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.59030       -0.62320        0.21160
C          1.81450       -1.75830        0.06670
C          0.44950       -1.60250       -0.05600
C         -0.09630       -0.32630       -0.03040
C          0.69930        0.79530        0.11560
C          2.06890        0.65040        0.23930
C          0.15090        2.06260        0.14030
C         -1.21630        2.18900        0.01560
C         -2.00100        1.05470       -0.13000
C         -1.47820       -0.21820       -0.15760
N         -2.27130       -1.37860       -0.30580
H          3.67510       -0.75420        0.30860
H          2.25190       -2.76200        0.04770
H         -0.14850       -2.49240       -0.16880
H          2.66750        1.54460        0.35240
H          0.75850        2.95310        0.25410
H         -1.69780        3.17790        0.02950
H         -3.09490        1.12470       -0.23140
H         -2.12120       -2.18210        0.33120
H         -3.00120       -1.45460       -1.03250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

