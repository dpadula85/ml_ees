%nproc=16
%mem=2GB
%chk=mol_240_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.69560       -0.40710       -0.20620
C          1.98290       -1.58340       -0.13160
C          0.58810       -1.55610       -0.02070
C         -0.05640       -0.32360        0.01220
C          0.63880        0.86780       -0.06120
C          2.00860        0.77960       -0.16920
C         -0.05240        2.06110       -0.02390
C         -1.42820        2.07340        0.08580
C         -2.13200        0.88860        0.15980
C         -1.43740       -0.31850        0.12240
N         -2.13350       -1.55540        0.19660
H          3.76640       -0.49000       -0.29040
H          2.46530       -2.54310       -0.15560
H          0.01140       -2.46480        0.03900
H          2.55780        1.71130       -0.22710
H          0.50420        2.98620       -0.08230
H         -1.99310        3.01250        0.11680
H         -3.22200        0.87960        0.24700
H         -2.22740       -2.09270       -0.69080
H         -2.53670       -1.92530        1.07940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

