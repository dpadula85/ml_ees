%nproc=16
%mem=2GB
%chk=mol_240_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.70150       -0.38200       -0.00660
C          2.01200       -1.57280       -0.11930
C          0.63060       -1.53420       -0.12970
C         -0.08880       -0.36680       -0.03310
C          0.61950        0.82250        0.07960
C          2.00460        0.78430        0.09010
C         -0.07870        2.01060        0.17820
C         -1.45720        2.05770        0.16870
C         -2.16960        0.88400        0.05730
C         -1.47210       -0.32310       -0.04310
N         -2.15720       -1.55240       -0.15940
H          3.78910       -0.36730        0.00550
H          2.56220       -2.52250       -0.19830
H          0.09710       -2.46920       -0.21830
H          2.51410        1.74440        0.18070
H          0.51640        2.93280        0.26590
H         -1.94350        3.01890        0.24920
H         -3.25150        0.87400        0.04550
H         -1.83850       -2.27350       -0.83290
H         -2.98980       -1.76540        0.42000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_240_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

