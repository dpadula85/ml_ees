%nproc=16
%mem=2GB
%chk=mol_120_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.75660       -0.28760        0.32140
C          3.38270       -0.84690        0.01540
O          2.39360        0.18300        0.13730
C          1.07540       -0.17530       -0.11370
O          0.80330       -1.36320       -0.43160
C          0.04930        0.88010        0.00940
C         -1.30990        0.35520       -0.28670
O         -1.52310       -0.85110       -0.60140
O         -2.39600        1.19160       -0.22370
C         -3.72490        0.82730       -0.47780
C         -4.24420       -0.24260        0.42560
H          5.21360        0.20220       -0.56930
H          5.39920       -1.11400        0.68930
H          4.67030        0.43440        1.17600
H          3.40420       -1.22360       -1.04610
H          3.16100       -1.70020        0.68920
H          0.03350        1.26050        1.05320
H          0.34060        1.70300       -0.68790
H         -4.34660        1.74930       -0.28280
H         -3.90170        0.60880       -1.56620
H         -5.06510        0.11800        1.10440
H         -3.45990       -0.63930        1.09720
H         -4.71180       -1.06960       -0.16210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

