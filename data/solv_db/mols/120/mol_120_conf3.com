%nproc=16
%mem=2GB
%chk=mol_120_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.61500       -0.55120        0.90920
C          3.00150       -1.26070       -0.27130
O          1.72870       -0.81850       -0.62860
C          1.35650        0.45430       -0.97880
O          2.26400        1.34000       -0.97120
C         -0.01850        0.83230       -1.34250
C         -0.98030        0.66000       -0.23290
O         -0.69530        0.17470        0.88650
O         -2.27980        1.04970       -0.45990
C         -3.33220        0.94590        0.43160
C         -3.63220       -0.49290        0.73170
H          4.60400       -1.06370        1.08180
H          3.87570        0.49100        0.69670
H          3.02190       -0.68900        1.83630
H          2.87690       -2.34030        0.03650
H          3.74100       -1.28530       -1.10680
H         -0.05830        1.89270       -1.64590
H         -0.42970        0.24750       -2.19750
H         -3.20030        1.54740        1.35770
H         -4.24640        1.39520       -0.05080
H         -4.15020       -0.52500        1.71670
H         -4.33540       -0.90070       -0.04990
H         -2.72650       -1.10330        0.74480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

