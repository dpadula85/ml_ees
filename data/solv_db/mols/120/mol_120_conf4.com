%nproc=16
%mem=2GB
%chk=mol_120_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.88430        0.83090       -0.94470
C          2.87930        1.15350        0.50510
O          1.68310        0.85020        1.17420
C          1.12960       -0.40520        1.26630
O          1.76140       -1.33420        0.69960
C         -0.12240       -0.72710        1.96540
C         -1.27330       -0.83730        1.01210
O         -2.43010       -1.10260        1.42640
O         -1.12180       -0.65280       -0.35440
C         -2.24720       -0.76330       -1.24290
C         -3.23710        0.28440       -0.79990
H          1.92530        0.99270       -1.45430
H          3.63220        1.49910       -1.43980
H          3.26100       -0.21470       -1.13950
H          3.03520        2.26890        0.60290
H          3.71820        0.67270        1.01400
H          0.00840       -1.74560        2.43320
H         -0.35960       -0.05430        2.80960
H         -1.92920       -0.65940       -2.27770
H         -2.68660       -1.78880       -1.11650
H         -2.84270        0.92790        0.01170
H         -3.48370        0.98160       -1.64240
H         -4.18430       -0.17650       -0.41350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

