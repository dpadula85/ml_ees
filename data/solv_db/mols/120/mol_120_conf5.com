%nproc=16
%mem=2GB
%chk=mol_120_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.94440       -0.63160       -0.24150
C         -2.68880       -0.68740       -1.07620
O         -1.74700        0.24060       -0.57700
C         -1.21000        0.21550        0.70030
O         -1.57700       -0.69240        1.48370
C         -0.20820        1.26590        1.10740
C          1.18440        0.76570        0.97290
O          1.68420       -0.00460        1.82470
O          2.00550        1.11700       -0.09180
C          3.31520        0.67870       -0.26820
C          3.49910       -0.79090       -0.38210
H         -4.77220       -0.14130       -0.81660
H         -4.28460       -1.64930        0.09630
H         -3.78290        0.00290        0.66450
H         -2.27660       -1.72120       -1.10060
H         -2.95060       -0.32730       -2.10840
H         -0.37890        2.21510        0.58880
H         -0.39240        1.44630        2.18710
H          3.95710        1.03940        0.58860
H          3.72240        1.21370       -1.16910
H          2.75740       -1.30570       -1.01240
H          3.59360       -1.27640        0.62770
H          4.49450       -0.97270       -0.87560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

