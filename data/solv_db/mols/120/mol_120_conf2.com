%nproc=16
%mem=2GB
%chk=mol_120_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.34940        0.39080       -0.02730
C          3.47670       -0.81950       -0.29150
O          2.22210       -0.60740        0.27910
C          1.35810        0.41890       -0.03730
O          1.69850        1.23810       -0.89890
C          0.06910        0.50860        0.65270
C         -1.05780        0.00270       -0.17030
O         -0.90960       -0.45330       -1.33240
O         -2.34760        0.02090        0.34010
C         -3.45400       -0.45630       -0.41900
C         -4.67650       -0.27030        0.47300
H          5.42850        0.10150       -0.06690
H          4.10710        0.84490        0.95380
H          4.15990        1.12010       -0.82040
H          3.41090       -0.98810       -1.38950
H          3.94830       -1.68050        0.23420
H         -0.16180        1.59420        0.85330
H          0.08720        0.03430        1.65250
H         -3.60320        0.14750       -1.34120
H         -3.32330       -1.50160       -0.73260
H         -4.86300        0.82260        0.50300
H         -4.37450       -0.56860        1.49320
H         -5.54460       -0.84650        0.09260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_120_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

