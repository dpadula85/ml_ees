%nproc=16
%mem=2GB
%chk=mol_009_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.91460        0.47930       -0.40790
C          1.18870       -0.77940        0.03390
O         -0.13550       -0.51930        0.28960
C         -0.83730       -0.03350       -0.78690
C         -2.26970        0.19820       -0.33790
H          3.02000        0.25460       -0.44620
H          1.69860        1.24670        0.36580
H          1.60240        0.78050       -1.42300
H          1.66360       -1.08510        0.99780
H          1.36220       -1.60320       -0.68690
H         -0.41920        0.92710       -1.19350
H         -0.85220       -0.73220       -1.66040
H         -2.54370        1.26420       -0.51880
H         -2.39950        0.00740        0.75050
H         -2.99310       -0.40520       -0.93100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

