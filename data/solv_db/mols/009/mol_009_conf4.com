%nproc=16
%mem=2GB
%chk=mol_009_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.12030        0.42870       -0.92750
C          0.80290        0.26390       -0.20440
O          0.22260       -0.97050       -0.42470
C         -0.97560       -1.00200        0.28770
C         -1.92020        0.08780       -0.16620
H          2.56000       -0.55440       -1.21810
H          2.04870        1.12900       -1.79150
H          2.86270        0.89550       -0.23410
H          0.97060        0.35790        0.89190
H          0.14240        1.09730       -0.55890
H         -1.44920       -2.00450        0.17530
H         -0.82960       -0.85960        1.37840
H         -1.78390        1.03410        0.39860
H         -2.99050       -0.20980       -0.01010
H         -1.78130        0.30660       -1.24040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

