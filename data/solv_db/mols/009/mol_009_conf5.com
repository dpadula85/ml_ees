%nproc=16
%mem=2GB
%chk=mol_009_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.40090        0.15590       -0.27820
C         -1.09120       -0.57840       -0.08700
O         -0.01010        0.26740       -0.29940
C          1.20680       -0.34260       -0.14010
C          2.27960        0.70780       -0.40470
H         -2.98360       -0.32540       -1.09900
H         -2.21560        1.23380       -0.54050
H         -2.98060        0.17050        0.67370
H         -1.01300       -1.39580       -0.85270
H         -1.08390       -1.07150        0.89270
H          1.32980       -0.65750        0.93210
H          1.40010       -1.18060       -0.83490
H          2.05520        1.26190       -1.34410
H          3.29790        0.27200       -0.39270
H          2.20950        1.48260        0.41230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

