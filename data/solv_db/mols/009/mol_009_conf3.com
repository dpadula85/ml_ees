%nproc=16
%mem=2GB
%chk=mol_009_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87560        0.10570        0.28420
C         -1.11740       -0.81840       -0.60840
O          0.18180       -0.98120       -0.12700
C          0.77240        0.27900       -0.11450
C          2.20080        0.22390        0.39560
H         -1.90170        1.15800       -0.07010
H         -2.92970       -0.25470        0.46250
H         -1.42290        0.12960        1.31380
H         -1.00700       -0.40380       -1.64240
H         -1.57470       -1.82110       -0.71870
H          0.81710        0.60240       -1.19340
H          0.21050        1.01930        0.47290
H          2.22830        0.47000        1.47980
H          2.74550        1.05930       -0.11280
H          2.67260       -0.76790        0.17840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

