%nproc=16
%mem=2GB
%chk=mol_009_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91650       -0.22410       -0.09850
C         -1.10650        1.01950        0.20950
O          0.17600        0.81490       -0.26200
C          0.81800       -0.25320        0.29570
C          2.21140       -0.32490       -0.34050
H         -3.00050        0.00500       -0.07020
H         -1.58570       -0.53860       -1.13170
H         -1.63430       -1.05140        0.57520
H         -1.16550        1.21420        1.29140
H         -1.57520        1.85290       -0.33950
H          0.32490       -1.22880        0.03590
H          0.93430       -0.19640        1.39160
H          2.66380        0.67240       -0.34170
H          2.04720       -0.66510       -1.38580
H          2.80850       -1.09630        0.17050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_009_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

