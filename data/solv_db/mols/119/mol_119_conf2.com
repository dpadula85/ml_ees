%nproc=16
%mem=2GB
%chk=mol_119_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.17660        0.35790       -0.61810
C          2.19400       -0.19600        0.34360
C          0.95030        0.57460        0.57950
C          0.13420        0.77820       -0.64440
O          0.70090        1.19950       -1.62530
C         -1.30800        0.47760       -0.74180
C         -1.84040       -0.17490        0.51040
C         -3.30640       -0.48490        0.37200
H          3.40310        1.41620       -0.38230
H          4.14670       -0.21970       -0.42730
H          2.92500        0.27420       -1.67990
H          1.92000       -1.25390        0.15900
H          2.72590       -0.22760        1.34420
H          1.27660        1.60510        0.93340
H          0.32800        0.16930        1.40400
H         -1.85950        1.38750       -0.96170
H         -1.45400       -0.23180       -1.58140
H         -1.73080        0.55450        1.34970
H         -1.28330       -1.08300        0.79810
H         -3.94940        0.18180        0.95470
H         -3.61000       -0.33420       -0.70320
H         -3.53950       -1.54400        0.61690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

