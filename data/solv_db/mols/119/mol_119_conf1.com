%nproc=16
%mem=2GB
%chk=mol_119_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.68620        0.91180       -0.73070
C         -2.42620       -0.36690        0.02170
C         -1.27300       -0.24590        0.99320
C         -0.00750        0.10630        0.29830
O          0.14700        1.26630       -0.02990
C          1.00110       -0.90460        0.02610
C          2.23020       -0.44380       -0.66300
C          3.02690        0.59650        0.04530
H         -2.42540        1.81750       -0.14580
H         -3.76480        0.91880       -0.99140
H         -2.13020        0.88570       -1.68820
H         -3.34610       -0.66460        0.56040
H         -2.24030       -1.16670       -0.74220
H         -1.20890       -1.20480        1.54000
H         -1.55520        0.53640        1.72770
H          0.53450       -1.68720       -0.63920
H          1.25560       -1.43050        0.96640
H          2.90200       -1.33740       -0.77890
H          2.02570       -0.10460       -1.69520
H          2.57530        1.57930        0.14530
H          3.96080        0.74950       -0.57400
H          3.40470        0.18880        1.02140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

