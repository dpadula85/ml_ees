%nproc=16
%mem=2GB
%chk=mol_119_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.25880       -0.01050       -0.73320
C         -2.04990        0.83880       -0.35130
C         -1.01930       -0.13150        0.18310
C          0.22800        0.52790        0.59650
O          0.37550        1.72130        0.49100
C          1.32910       -0.29730        1.14280
C          2.18440       -0.88120        0.05100
C          2.81670        0.16440       -0.81240
H         -3.35230       -0.78630        0.04880
H         -3.10990       -0.44600       -1.73860
H         -4.19610        0.57270       -0.76980
H         -1.70740        1.38970       -1.23550
H         -2.39340        1.48530        0.49410
H         -0.75770       -0.84490       -0.62830
H         -1.51110       -0.72630        0.99380
H          0.83710       -1.11600        1.73820
H          1.97920        0.27070        1.83300
H          1.53020       -1.53800       -0.58130
H          2.96790       -1.48910        0.56530
H          3.87360       -0.13630       -1.05000
H          2.91430        1.16290       -0.30510
H          2.31970        0.26990       -1.79990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

