%nproc=16
%mem=2GB
%chk=mol_119_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.97510       -0.16510        0.66990
C         -2.64880       -0.07110       -0.80120
C         -1.28740        0.55770       -0.92810
C         -0.21890       -0.20870       -0.26360
O         -0.47160       -1.23620        0.31210
C          1.16170        0.30680       -0.31790
C          2.14320       -0.58740        0.40790
C          3.50690        0.05890        0.27120
H         -3.98740        0.19230        0.87060
H         -2.83980       -1.21720        0.99360
H         -2.27520        0.44490        1.27730
H         -2.74560       -1.02350       -1.31540
H         -3.37440        0.65740       -1.24580
H         -1.00950        0.62880       -2.01550
H         -1.25910        1.60020       -0.55720
H          1.47300        0.33150       -1.38300
H          1.18770        1.29330        0.15830
H          2.21390       -1.59760       -0.05400
H          1.93080       -0.65640        1.48820
H          3.53860        0.57200       -0.70390
H          3.62040        0.81850        1.07360
H          4.31660       -0.69910        0.31930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

