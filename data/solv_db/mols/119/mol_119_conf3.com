%nproc=16
%mem=2GB
%chk=mol_119_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.50020       -0.02920        0.43910
C          2.09800       -0.60230        0.60010
C          1.18620        0.21630       -0.27670
C         -0.21140       -0.20190       -0.24240
O         -0.56010       -1.13430        0.44800
C         -1.20140        0.52230       -1.05810
C         -2.60120       -0.05630       -0.89160
C         -3.01770        0.03950        0.55570
H          3.60620        0.28160       -0.62980
H          3.65680        0.85710        1.09050
H          4.28260       -0.81300        0.64590
H          2.14200       -1.64370        0.20160
H          1.82970       -0.57130        1.65990
H          1.24630        1.29340        0.04790
H          1.54870        0.16170       -1.31870
H         -1.19020        1.58580       -0.78080
H         -0.98640        0.47830       -2.14810
H         -2.65110       -1.08850       -1.28840
H         -3.33060        0.54180       -1.47370
H         -2.95860       -0.97520        0.99880
H         -2.32760        0.74210        1.08110
H         -4.06020        0.39600        0.66140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_119_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

