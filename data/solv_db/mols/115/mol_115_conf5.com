%nproc=16
%mem=2GB
%chk=mol_115_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.36190        0.61130       -0.18210
C         -2.20090       -0.32540       -0.08340
C         -0.86010        0.40810       -0.12300
C          0.20050       -0.65140       -0.01560
C          1.56660       -0.04240       -0.04430
O          1.72080        1.20990       -0.14800
O          2.71220       -0.80380        0.03980
C          4.00210       -0.22280        0.01230
H         -4.18760        0.06130       -0.70960
H         -3.06880        1.49810       -0.76760
H         -3.70240        0.89180        0.85180
H         -2.21970       -0.83470        0.92280
H         -2.23410       -1.05800       -0.89440
H         -0.83650        0.96460       -1.06250
H         -0.79200        1.10460        0.74930
H          0.12360       -1.34660       -0.87020
H          0.02200       -1.21560        0.92640
H          3.99080        0.84090        0.31760
H          4.49070       -0.33970       -0.97440
H          4.63460       -0.75030        0.76610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

