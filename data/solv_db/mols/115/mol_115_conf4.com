%nproc=16
%mem=2GB
%chk=mol_115_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.65580        0.94080        0.29420
C         -1.46860        0.38300       -0.41570
C         -1.01040       -0.90730        0.22380
C          0.20220       -1.46260       -0.49180
C          1.33560       -0.51200       -0.46480
O          1.61520        0.13180       -1.51360
O          2.08880       -0.27810        0.67070
C          3.14360        0.68830        0.58510
H         -3.18550        0.21510        0.92560
H         -2.28870        1.78130        0.94820
H         -3.42050        1.38290       -0.38040
H         -1.73540        0.16960       -1.46850
H         -0.59540        1.09700       -0.35490
H         -0.81240       -0.77740        1.28420
H         -1.86550       -1.63560        0.12760
H         -0.12320       -1.66130       -1.55730
H          0.54850       -2.41890       -0.10220
H          2.70280        1.72190        0.51880
H          3.76460        0.62690        1.48960
H          3.76020        0.51470       -0.31850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

