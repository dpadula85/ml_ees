%nproc=16
%mem=2GB
%chk=mol_115_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.06400        0.60690        0.14470
C         -1.66490        0.41180       -0.46030
C         -1.06970       -0.76540        0.22120
C          0.30020       -1.15320       -0.20470
C          1.34310       -0.13980       -0.00510
O          1.07790        0.96230        0.51330
O          2.63890       -0.40650       -0.39560
C          3.70170        0.51160       -0.24000
H         -3.76020        1.03250       -0.61570
H         -3.49770       -0.35500        0.46520
H         -3.00370        1.25360        1.04970
H         -1.84000        0.19490       -1.54670
H         -1.15610        1.36620       -0.35760
H         -1.07270       -0.67840        1.33740
H         -1.76850       -1.63140        0.02360
H          0.24150       -1.39040       -1.30260
H          0.65830       -2.09430        0.29580
H          4.18420        0.45300        0.74040
H          4.46180        0.28770       -1.01620
H          3.28980        1.53400       -0.45460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

