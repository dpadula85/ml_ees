%nproc=16
%mem=2GB
%chk=mol_115_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.93070        1.05520       -0.14420
C         -1.59140        0.50480       -0.68460
C         -1.13050       -0.57950        0.21810
C          0.15060       -1.25210       -0.12510
C          1.34140       -0.39970       -0.17830
O          1.35790        0.64620       -0.86360
O          2.51200       -0.69970        0.54940
C          3.64240        0.18270        0.47900
H         -3.58230        0.15320        0.02160
H         -3.36880        1.75330       -0.85400
H         -2.76340        1.60440        0.80720
H         -1.80250        0.13620       -1.70000
H         -0.90780        1.39290       -0.65710
H         -1.10500       -0.26310        1.29630
H         -1.91910       -1.39090        0.21810
H          0.26480       -2.08220        0.63130
H          0.01130       -1.82370       -1.09150
H          4.55440       -0.46010        0.30370
H          3.75280        0.61980        1.47990
H          3.51380        0.90210       -0.33970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

