%nproc=16
%mem=2GB
%chk=mol_115_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.39010       -0.17160       -0.08030
C         -2.14250        0.65180       -0.32790
C         -0.92530       -0.22170       -0.08410
C          0.28830        0.61370       -0.33480
C          1.55470       -0.14100       -0.12580
O          1.51840       -1.34660        0.22360
O          2.80090        0.43210       -0.30100
C          3.95000       -0.32710       -0.08970
H         -3.43090       -0.46020        0.97880
H         -3.29220       -1.10440       -0.68030
H         -4.28930        0.36530       -0.43790
H         -2.10430        1.01590       -1.37060
H         -2.10380        1.49010        0.41450
H         -0.93480       -0.59220        0.96030
H         -0.93090       -1.05050       -0.80880
H          0.22990        1.01560       -1.35540
H          0.29120        1.50220        0.34980
H          4.54230       -0.30190       -1.05020
H          4.61860        0.03190        0.69220
H          3.74980       -1.40150        0.09170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_115_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

