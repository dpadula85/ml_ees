%nproc=16
%mem=2GB
%chk=mol_112_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.84690        0.21790        0.10480
C          0.42000       -0.55070       -0.02750
O          1.42790        0.29000       -0.53960
H         -1.24110        0.29840        1.12640
H         -0.73040        1.26430       -0.28510
H         -1.68230       -0.21650       -0.51380
H          0.77660       -1.01830        0.91370
H          0.30820       -1.35550       -0.80540
H          1.56810        1.07050        0.02650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

