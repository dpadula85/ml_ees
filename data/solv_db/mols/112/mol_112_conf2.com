%nproc=16
%mem=2GB
%chk=mol_112_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.83680        0.30670       -0.20100
C          0.41160       -0.49770       -0.00530
O          1.55180        0.24510        0.15380
H         -0.86880        0.70950       -1.24500
H         -1.00120        1.08830        0.54510
H         -1.69410       -0.42110       -0.11250
H          0.27160       -1.10230        0.92170
H          0.46060       -1.23810       -0.83640
H          1.70530        0.90950       -0.56240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

