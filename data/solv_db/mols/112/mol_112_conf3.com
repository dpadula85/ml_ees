%nproc=16
%mem=2GB
%chk=mol_112_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.93770       -0.21420        0.00240
C          0.42210        0.46510        0.06070
O          1.39600       -0.35380        0.58610
H         -0.77280       -1.20920       -0.44950
H         -1.44370       -0.23950        0.97470
H         -1.56680        0.37690       -0.70390
H          0.68740        0.68290       -1.00890
H          0.38840        1.40930        0.64720
H          1.82710       -0.91750       -0.10870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

