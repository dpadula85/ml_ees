%nproc=16
%mem=2GB
%chk=mol_112_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.92290        0.03650        0.02110
C         -0.48120       -0.45520       -0.08680
O         -1.29200        0.40310        0.68630
H          1.66790       -0.73400        0.25830
H          1.19660        0.47810       -0.97540
H          1.02010        0.89710        0.73830
H         -0.63130       -1.48120        0.27060
H         -0.90330       -0.37820       -1.12380
H         -1.49970        1.23370        0.21140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

