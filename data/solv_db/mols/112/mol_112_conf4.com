%nproc=16
%mem=2GB
%chk=mol_112_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.92370        0.09540        0.04910
C         -0.49580       -0.34740        0.20800
O         -1.31730        0.75450        0.00040
H          1.48600       -0.55800       -0.66850
H          1.42270        0.10210        1.03420
H          1.01750        1.12660       -0.38160
H         -0.67950       -1.16780       -0.51830
H         -0.73710       -0.74310        1.21030
H         -1.62010        0.73770       -0.93340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_112_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

