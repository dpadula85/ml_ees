%nproc=16
%mem=2GB
%chk=mol_275_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.32970       -2.21640       -0.57960
C         -0.12270       -0.98610       -0.15840
C         -1.28600       -0.11570        0.01380
C         -2.51610       -0.69830        0.21290
C         -3.67680        0.03840        0.14700
C         -3.63730        1.37860       -0.11630
C         -2.40370        1.95200       -0.31330
C         -1.23620        1.22860       -0.25540
C          1.24960       -0.57190        0.09330
C          1.53730        0.50300        0.89920
C          2.78420        1.05020        1.07960
C          3.82920        0.47390        0.40270
C          3.62160       -0.60910       -0.42280
C          2.33060       -1.12780       -0.57580
H          0.50500       -2.88460       -0.69970
H         -1.33560       -2.56670       -0.85850
H         -2.57810       -1.76040        0.48420
H         -4.62560       -0.49890        0.31470
H         -4.57130        1.95190       -0.16300
H         -2.38390        3.03790       -0.52430
H         -0.29570        1.72440       -0.50670
H          0.73710        0.92970        1.49270
H          2.96590        1.90660        1.72750
H          4.82310        0.88310        0.52640
H          4.44990       -1.07310       -0.96280
H          2.16520       -1.94910       -1.25720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

