%nproc=16
%mem=2GB
%chk=mol_275_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.07990        0.21440       -2.25650
C          0.02640        0.12310       -0.94860
C          1.24830        0.16930       -0.17400
C          2.40180        0.73170       -0.73990
C          3.62910        0.55770       -0.13820
C          3.79610       -0.15450        1.02420
C          2.67690       -0.71340        1.59700
C          1.44540       -0.54930        1.00140
C         -1.27220       -0.08620       -0.32330
C         -2.34810       -0.59320       -1.06280
C         -3.60740       -0.55550       -0.52700
C         -3.84450       -0.03410        0.72200
C         -2.79420        0.46640        1.45480
C         -1.51810        0.44310        0.93410
H         -0.81220        0.19850       -2.88760
H          1.04360        0.25610       -2.73510
H          2.28790        1.34760       -1.62030
H          4.49720        1.01570       -0.62460
H          4.77080       -0.27690        1.48080
H          2.80140       -1.27950        2.52220
H          0.60390       -1.06340        1.43200
H         -2.14640       -1.04290       -2.04020
H         -4.43350       -0.95840       -1.13040
H         -4.84590       -0.02220        1.11410
H         -2.99470        0.88070        2.45410
H         -0.69150        0.92520        1.47190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

