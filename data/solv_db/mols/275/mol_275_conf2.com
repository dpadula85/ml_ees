%nproc=16
%mem=2GB
%chk=mol_275_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.07820       -2.23800       -0.24380
C         -0.02980       -0.93110       -0.17380
C          1.25270       -0.27200       -0.35160
C          2.26440       -0.94180       -1.02060
C          3.57170       -0.48960       -1.04930
C          3.84780        0.68220       -0.37820
C          2.85920        1.37520        0.29890
C          1.58670        0.89700        0.30970
C         -1.27440       -0.19640        0.10640
C         -2.37330       -0.83670        0.64400
C         -3.62980       -0.25500        0.71380
C         -3.77830        1.01070        0.22910
C         -2.72930        1.69170       -0.31250
C         -1.48630        1.09780       -0.37850
H          0.81280       -2.80910       -0.41510
H         -1.00630       -2.78050       -0.16910
H          2.06640       -1.84510       -1.62500
H          4.36190       -1.02910       -1.57790
H          4.86410        1.03810       -0.40120
H          3.09490        2.30870        0.82760
H          0.81230        1.36210        0.91700
H         -2.25490       -1.82850        1.07660
H         -4.43540       -0.81680        1.14350
H         -4.76520        1.47510        0.28310
H         -2.86640        2.70770       -0.69590
H         -0.68740        1.62320       -0.88370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

