%nproc=16
%mem=2GB
%chk=mol_275_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.25840        2.14410       -0.85200
C         -0.05940        0.91790       -0.41620
C          1.27170        0.36360       -0.34260
C          1.50650       -0.99490       -0.29910
C          2.76000       -1.51920       -0.08640
C          3.81570       -0.65160        0.08410
C          3.62040        0.70790        0.04390
C          2.36180        1.22940       -0.16600
C         -1.25600        0.12240       -0.10990
C         -2.47080        0.44570       -0.69280
C         -3.65400       -0.08970       -0.28650
C         -3.67950       -0.99910        0.74830
C         -2.49580       -1.34630        1.35660
C         -1.30650       -0.79180        0.93640
H          0.56880        2.75380       -1.17240
H         -1.22880        2.61440       -0.89300
H          0.72300       -1.70010       -0.49660
H          2.88920       -2.60370       -0.05700
H          4.79430       -1.08670        0.24940
H          4.49190        1.34950        0.18330
H          2.21630        2.28100       -0.15860
H         -2.45860        1.14650       -1.53670
H         -4.61570        0.17040       -0.75040
H         -4.62460       -1.41380        1.05330
H         -2.53420       -2.05620        2.16050
H         -0.37730       -0.99330        1.50060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

