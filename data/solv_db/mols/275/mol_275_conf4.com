%nproc=16
%mem=2GB
%chk=mol_275_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.05620       -2.25360       -0.61340
C         -0.06060       -0.96530       -0.37820
C          1.22400       -0.29340       -0.18200
C          2.39760       -0.86450       -0.68930
C          3.63900       -0.41410       -0.30230
C          3.81360        0.61550        0.59490
C          2.66940        1.18670        1.10260
C          1.40210        0.73150        0.71750
C         -1.29410       -0.21580       -0.31170
C         -1.40670        1.14860       -0.47780
C         -2.59040        1.81940       -0.26110
C         -3.71720        1.12740        0.13160
C         -3.65760       -0.23930        0.30920
C         -2.46060       -0.87560        0.08800
H          0.85570       -2.82790       -0.65230
H         -0.96790       -2.78340       -0.83440
H          2.28150       -1.64380       -1.42730
H          4.56080       -0.85880       -0.69450
H          4.78590        0.97410        0.90010
H          2.74660        2.00010        1.81020
H          0.55280        1.16130        1.23240
H         -0.51640        1.66350       -0.84850
H         -2.61020        2.89440       -0.40670
H         -4.65540        1.67780        0.29930
H         -4.54050       -0.81190        0.61960
H         -2.39510       -1.95260        0.27420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_275_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

