%nproc=16
%mem=2GB
%chk=mol_062_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.05420       -1.42690        0.03170
C         -1.19290       -0.83740        0.01670
C         -1.25400        0.54210       -0.01390
C         -0.14900        1.34980       -0.03010
C          1.09340        0.74560       -0.01490
C          1.18160       -0.62670        0.01560
Cl         2.77660       -1.33550        0.03390
Cl         2.51790        1.78440       -0.03560
Cl        -0.26090        3.11350       -0.06940
Cl        -2.66560       -1.81020        0.03570
H          0.12460       -2.51850        0.05590
H         -2.22590        1.01980       -0.02570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_062_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_062_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_062_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

