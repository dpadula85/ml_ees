%nproc=16
%mem=2GB
%chk=mol_363_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.17010       -0.14380        0.00910
N          0.03310        0.71280       -0.28430
C         -1.15680       -0.06840       -0.08700
H          2.01540        0.21280       -0.61260
H          1.39220       -0.19590        1.08020
H          0.91660       -1.18060       -0.32600
H          0.05410        1.59420        0.27200
H         -1.32040       -0.31460        0.99470
H         -1.02670       -1.02860       -0.61250
H         -2.07760        0.41210       -0.43370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

