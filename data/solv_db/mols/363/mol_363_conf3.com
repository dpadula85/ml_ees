%nproc=16
%mem=2GB
%chk=mol_363_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.16410        0.20980        0.13230
N          0.10890       -0.16120        0.71200
C          1.17580       -0.03740       -0.25570
H         -1.79460       -0.68330       -0.03190
H         -0.96670        0.63290       -0.88160
H         -1.68090        0.99420        0.70410
H          0.06120       -1.07720        1.17400
H          1.99330       -0.74480       -0.11330
H          0.69300       -0.12030       -1.26490
H          1.57420        0.98730       -0.17480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

