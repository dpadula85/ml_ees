%nproc=16
%mem=2GB
%chk=mol_363_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.20100        0.05640       -0.14760
N         -0.02470       -0.15540        0.61170
C          1.20500        0.08730       -0.06350
H         -2.03980        0.28650        0.56940
H         -1.08190        0.96090       -0.76070
H         -1.47600       -0.78290       -0.82750
H         -0.03730       -0.93490        1.27130
H          1.23960        1.15580       -0.36880
H          2.03440       -0.09940        0.66040
H          1.38190       -0.57430       -0.94480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_363_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

