%nproc=16
%mem=2GB
%chk=mol_491_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00010        0.00830        0.16360
Br        -1.15900       -1.42560       -0.42870
Br        -0.67840        1.68640       -0.50570
Br         1.79330       -0.32910       -0.50060
H          0.04400        0.06000        1.27130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_491_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_491_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_491_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

