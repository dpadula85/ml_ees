%nproc=16
%mem=2GB
%chk=mol_504_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.16740        0.33520        0.13800
C         -0.12990       -0.44640       -0.69890
C          1.07710       -0.49440        0.12980
O          1.42820       -1.56680        0.63540
O          1.79620        0.65100        0.34610
H         -2.13860        0.33780       -0.34590
H         -1.15180       -0.09380        1.15640
H         -0.72080        1.37970        0.15650
H          0.05060        0.02350       -1.66920
H         -0.54440       -1.47570       -0.88030
H          1.50080        1.35000        1.03210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

