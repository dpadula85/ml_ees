%nproc=16
%mem=2GB
%chk=mol_504_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43990       -0.10550       -0.22250
C         -0.00950       -0.25520        0.15790
C          0.92950        0.47510       -0.68440
O          0.55570        1.17410       -1.65370
O          2.28060        0.40410       -0.41780
H         -1.97740       -1.09770       -0.27290
H         -1.60400        0.36190       -1.21120
H         -2.02860        0.48840        0.51670
H          0.09530        0.12970        1.21350
H          0.28450       -1.34080        0.23900
H          2.91370       -0.23410       -0.90510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

