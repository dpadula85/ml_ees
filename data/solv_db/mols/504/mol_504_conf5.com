%nproc=16
%mem=2GB
%chk=mol_504_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.41900        0.18830        0.24780
C         -0.05460       -0.28640       -0.14960
C          1.02350        0.32970        0.59490
O          0.77780        1.17630        1.47950
O          2.34840        0.02120        0.37180
H         -2.17170       -0.22760       -0.45460
H         -1.48660        1.29900        0.29230
H         -1.71760       -0.18460        1.25880
H          0.07480       -0.01350       -1.23490
H         -0.06670       -1.39220       -0.13150
H          2.69170       -0.91030        0.20300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

