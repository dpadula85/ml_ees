%nproc=16
%mem=2GB
%chk=mol_504_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.33590        0.11490        0.24430
C         -0.09420       -0.46440       -0.38210
C          1.02560        0.51690       -0.23820
O          1.15450        1.52390       -0.96780
O          2.02930        0.26760        0.70320
H         -1.89160       -0.61630        0.87170
H         -2.00890        0.53200       -0.54800
H         -1.09220        0.96650        0.92870
H         -0.29850       -0.69500       -1.45860
H          0.20120       -1.43250        0.09020
H          2.31070       -0.71350        0.75650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

