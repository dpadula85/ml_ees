%nproc=16
%mem=2GB
%chk=mol_504_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.20060        0.34140       -0.55080
C         -0.18860       -0.78540       -0.41110
C          1.19520       -0.25900       -0.48890
O          2.14060       -1.07840       -0.38880
O          1.41200        1.08920       -0.66560
H         -1.23560        0.99470        0.32610
H         -0.95780        0.89330       -1.49080
H         -2.19540       -0.14250       -0.72510
H         -0.35060       -1.45290       -1.28340
H         -0.38780       -1.31630        0.52610
H          1.76860        1.71590        0.05500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_504_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

