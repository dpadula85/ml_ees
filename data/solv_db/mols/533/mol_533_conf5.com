%nproc=16
%mem=2GB
%chk=mol_533_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.88730        0.44080       -0.50640
C          3.42300       -0.00130        0.87040
N          2.02510        0.30870        1.09090
C          1.03970       -0.25110        0.23060
N         -0.27390       -0.00300        0.38150
C         -1.18820       -0.53070       -0.42760
N         -0.80450       -1.33200       -1.42320
C          0.50720       -1.61630       -1.62170
N          1.39790       -1.06180       -0.78050
Cl         0.97550       -2.67020       -2.93690
N         -2.54870       -0.24800       -0.23880
C         -2.93960        0.62430        0.84720
C         -4.42220        0.82930        0.91790
H          4.95070        0.75980       -0.43650
H          3.85090       -0.36640       -1.25120
H          3.29820        1.30190       -0.83810
H          4.05800        0.36800        1.68130
H          3.54030       -1.12500        0.87250
H          1.77490        0.93970        1.88100
H         -3.31950       -0.62720       -0.83470
H         -2.51260        0.17920        1.78250
H         -2.40990        1.61120        0.71740
H         -4.87820        0.17750        0.12860
H         -4.65930        1.88730        0.80600
H         -4.77190        0.40510        1.88330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

