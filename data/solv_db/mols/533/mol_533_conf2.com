%nproc=16
%mem=2GB
%chk=mol_533_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.03730        1.28750        0.25210
C          3.11440        0.10010        0.28470
N          1.72820        0.47680        0.23910
C          0.69670       -0.48510        0.25960
N         -0.60970       -0.16760        0.21820
C         -1.57340       -1.10210        0.23940
N         -1.18560       -2.40050        0.30490
C          0.10160       -2.75330        0.34740
N          1.03910       -1.79890        0.32480
Cl         0.50730       -4.44020        0.43090
N         -2.95130       -0.78080        0.19640
C         -3.40860        0.57800        0.12700
C         -2.92070        1.31300       -1.09450
H          4.90980        1.10690       -0.41460
H          3.49360        2.22360       -0.05700
H          4.47530        1.51190        1.26620
H          3.31270       -0.61560       -0.56380
H          3.33960       -0.50310        1.21130
H          1.48430        1.47780        0.19030
H         -3.66550       -1.53950        0.21500
H         -2.99020        1.13940        1.01120
H         -4.50670        0.67130        0.22520
H         -1.90470        1.76890       -0.92360
H         -2.92580        0.72970       -2.00930
H         -3.59780        2.20200       -1.22670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

