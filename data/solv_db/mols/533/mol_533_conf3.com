%nproc=16
%mem=2GB
%chk=mol_533_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.52140        0.84290        0.53150
C         -3.02680        0.74580        0.37730
N         -2.62630       -0.59720        0.03440
C         -1.26310       -0.95550       -0.16790
N         -0.25380       -0.08320       -0.05710
C          1.02840       -0.41690       -0.24670
N          1.31960       -1.68810       -0.56370
C          0.34770       -2.59430       -0.68560
N         -0.94180       -2.22110       -0.48620
Cl         0.64190       -4.27060       -1.09580
N          2.09130        0.54930       -0.11910
C          3.46020        0.14780       -0.33200
C          4.34400        1.34010       -0.13670
H         -5.00110        1.37810       -0.31660
H         -4.98050       -0.16740        0.55370
H         -4.80250        1.34970        1.48640
H         -2.74790        1.38980       -0.49450
H         -2.48320        1.12730        1.27140
H         -3.37880       -1.31130       -0.06280
H          1.88990        1.53890        0.12530
H          3.51700       -0.26870       -1.35900
H          3.74750       -0.68510        0.34730
H          4.02490        2.11670       -0.85810
H          4.22000        1.70030        0.90130
H          5.39480        1.03270       -0.26790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

