%nproc=16
%mem=2GB
%chk=mol_533_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.74190       -0.43790       -0.37870
C          3.47330        0.34280       -0.28030
N          2.35560       -0.49670        0.00320
C          1.03070       -0.00630        0.14790
N         -0.01860       -0.78410        0.41280
C         -1.27040       -0.33000        0.55120
N         -1.43890        1.01100        0.40530
C         -0.40920        1.84240        0.13660
N          0.83260        1.31630        0.00910
Cl        -0.61160        3.56800       -0.05310
N         -2.37800       -1.16560        0.83370
C         -3.71230       -0.62880        0.97430
C         -4.13840        0.06480       -0.31030
H          4.64550       -1.48600       -0.00110
H          5.56850        0.05000        0.18640
H          5.09010       -0.55510       -1.44320
H          3.59890        1.12780        0.50450
H          3.28320        0.82430       -1.28410
H          2.50250       -1.51670        0.11170
H         -2.22460       -2.18680        0.94180
H         -4.45680       -1.38920        1.23630
H         -3.69350        0.15700        1.76590
H         -3.94620        1.17470       -0.22990
H         -5.21340       -0.12850       -0.46780
H         -3.61080       -0.36730       -1.18570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

