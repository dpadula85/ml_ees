%nproc=16
%mem=2GB
%chk=mol_533_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.99990       -0.19580       -0.61630
C          3.44770       -1.23180        0.36010
N          2.29380       -0.75810        1.04900
C          1.12300       -0.37790        0.33420
N          0.01970        0.07370        0.97350
C         -1.10960        0.44450        0.35220
N         -1.11910        0.35200       -0.98820
C         -0.04650       -0.09200       -1.67850
N          1.05990       -0.45120       -1.01310
Cl        -0.14240       -0.18210       -3.42030
N         -2.20120        0.90180        1.12890
C         -3.42140        1.30310        0.46590
C         -4.00750        0.14130       -0.32970
H          3.64890        0.82530       -0.35530
H          5.09870       -0.24890       -0.69940
H          3.60130       -0.40210       -1.63870
H          3.20640       -2.15540       -0.21840
H          4.27720       -1.50820        1.03780
H          2.27180       -0.67690        2.08030
H         -2.16770        0.96330        2.16320
H         -3.24390        2.12320       -0.27020
H         -4.16220        1.69710        1.18910
H         -5.01600       -0.09390        0.06710
H         -3.40090       -0.78970       -0.15970
H         -4.01010        0.33860       -1.41580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_533_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

