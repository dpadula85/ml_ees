%nproc=16
%mem=2GB
%chk=mol_368_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.85760        0.79500       -0.12670
C         -1.17640       -0.20640        0.76220
C         -0.31330       -1.18420        0.02630
C          0.80300       -0.54170       -0.75340
C          1.69940        0.22670        0.19310
O          2.74980        0.85010       -0.42280
H         -2.39100        1.50950        0.53540
H         -2.65270        0.29260       -0.72550
H         -1.16830        1.38850       -0.74210
H         -0.61390        0.30070        1.57210
H         -1.98430       -0.78660        1.25410
H         -0.93510       -1.85690       -0.59500
H          0.16320       -1.83240        0.79510
H          1.40160       -1.37490       -1.19010
H          0.47800        0.07880       -1.58490
H          2.11280       -0.46670        0.96340
H          1.07830        1.01390        0.68340
H          2.60640        1.79400       -0.64470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

