%nproc=16
%mem=2GB
%chk=mol_368_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.64790       -0.39710        0.46230
C         -1.46150        0.46260        0.15700
C         -0.16630       -0.36170        0.08190
C          0.93620        0.60150       -0.22570
C          2.28640       -0.02980       -0.33730
O          2.64570       -0.66160        0.84910
H         -2.47290       -1.43540        0.09350
H         -2.77930       -0.50780        1.57670
H         -3.57580       -0.04720       -0.02900
H         -1.64080        0.97960       -0.80780
H         -1.27640        1.23020        0.92490
H         -0.33380       -1.08260       -0.75600
H         -0.03590       -0.94020        0.99880
H          0.69570        1.10800       -1.17540
H          0.97920        1.32630        0.62390
H          3.01870        0.77820       -0.54490
H          2.34000       -0.74410       -1.19680
H          3.48850       -0.27890        1.20520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

