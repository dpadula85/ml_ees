%nproc=16
%mem=2GB
%chk=mol_368_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.49580        0.55860        0.28380
C          1.15440       -0.16860        0.41960
C          0.28470        0.08330       -0.78310
C         -1.04260       -0.58830       -0.75230
C         -1.81260       -0.10040        0.44950
O         -3.06380       -0.71870        0.48330
H          2.97700        0.69350        1.26380
H          3.12390        0.08370       -0.47590
H          2.24160        1.57540       -0.09350
H          1.38980       -1.26230        0.53760
H          0.72130        0.16960        1.37520
H          0.89560       -0.21310       -1.67750
H          0.20450        1.19860       -0.87660
H         -1.63610       -0.23580       -1.64550
H         -0.95310       -1.69460       -0.75470
H         -1.22790       -0.30670        1.36940
H         -1.96960        0.98640        0.33510
H         -3.78320       -0.06060        0.54190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

