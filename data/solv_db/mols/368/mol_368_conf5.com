%nproc=16
%mem=2GB
%chk=mol_368_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.01790        0.84050        0.08470
C         -1.62070       -0.46910       -0.52540
C         -0.26770       -0.94650       -0.05470
C          0.78170        0.07250       -0.42560
C          2.15680       -0.31280        0.00020
O          3.03040        0.69860       -0.39490
H         -1.40560        1.68490       -0.24830
H         -3.06110        1.05210       -0.24040
H         -2.06950        0.76280        1.20280
H         -2.41640       -1.20810       -0.35650
H         -1.55240       -0.30840       -1.63170
H         -0.27680       -1.04030        1.04950
H         -0.07690       -1.93610       -0.47610
H          0.53600        1.08490       -0.06850
H          0.78810        0.13970       -1.54620
H          2.50290       -1.23790       -0.51850
H          2.27050       -0.47990        1.07730
H          2.69850        1.60320       -0.16780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

