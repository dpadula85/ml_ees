%nproc=16
%mem=2GB
%chk=mol_368_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19700       -0.20150        0.84090
C          1.55530        0.00950       -0.47820
C          0.10810       -0.33600       -0.55900
C         -0.79370        0.40580        0.34810
C         -2.21010       -0.07860        0.12260
O         -2.58910        0.15130       -1.21820
H          3.28820       -0.03100        0.71560
H          2.10590       -1.26960        1.17070
H          1.86470        0.48430        1.63800
H          2.10360       -0.59410       -1.23200
H          1.72280        1.07610       -0.78080
H         -0.00720       -1.42700       -0.35050
H         -0.21630       -0.20070       -1.61180
H         -0.56000        0.16620        1.42210
H         -0.79280        1.48610        0.25480
H         -2.25070       -1.18220        0.25010
H         -2.94290        0.39400        0.78680
H         -2.58270        1.14760       -1.31910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_368_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

