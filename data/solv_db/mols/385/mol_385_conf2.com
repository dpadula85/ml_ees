%nproc=16
%mem=2GB
%chk=mol_385_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.78220       -0.41640       -0.55470
C          2.12200       -0.72940        0.77850
C          0.90970        0.15110        0.96590
C         -0.10930       -0.08330       -0.15400
C         -1.25720        0.85550        0.16120
C         -2.36380        0.75030       -0.85920
N         -2.86300       -0.59640       -0.86750
H          2.88810        0.68530       -0.65640
H          2.17300       -0.85010       -1.35390
H          3.82610       -0.82740       -0.56320
H          1.85070       -1.79570        0.83220
H          2.85270       -0.43520        1.55890
H          1.23070        1.21280        0.97400
H          0.39700       -0.09020        1.90100
H          0.34020        0.13680       -1.15150
H         -0.42000       -1.14440       -0.11670
H         -1.65800        0.51660        1.15530
H         -0.86060        1.87080        0.23020
H         -1.99310        1.03290       -1.84780
H         -3.16040        1.45580       -0.54160
H         -3.72390       -0.71600       -1.44030
H         -2.96290       -0.98340        0.09820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

