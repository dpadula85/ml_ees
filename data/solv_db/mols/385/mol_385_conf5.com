%nproc=16
%mem=2GB
%chk=mol_385_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.58340       -0.31920        0.01310
C         -1.43520       -0.42620       -0.94020
C         -0.64410        0.85800       -0.90420
C         -0.09540        1.14950        0.47050
C          0.82200        0.04490        0.94310
C          1.98420       -0.11940        0.01650
N          2.89400       -1.16470        0.43100
H         -3.55750       -0.52980       -0.47160
H         -2.47680       -1.06780        0.83440
H         -2.67860        0.69150        0.47440
H         -1.86730       -0.50000       -1.97750
H         -0.81020       -1.32000       -0.81690
H         -1.32650        1.71320       -1.18470
H          0.18410        0.89940       -1.63340
H          0.54200        2.06400        0.38670
H         -0.89440        1.37470        1.19080
H          0.19780       -0.89620        0.90320
H          1.11050        0.15750        1.99890
H          1.59460       -0.40560       -0.99090
H          2.53780        0.82410       -0.09680
H          2.61960       -2.10510        0.11710
H          3.88290       -0.92310        0.32340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

