%nproc=16
%mem=2GB
%chk=mol_385_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.93500       -0.16870        0.20890
C          1.93150        0.91560       -0.06200
C          0.64960        0.38480       -0.64680
C         -0.00220       -0.59990        0.29390
C         -1.28220       -1.12290       -0.30220
C         -2.28580       -0.03560       -0.57780
N         -2.67130        0.68690        0.60360
H          3.25050       -0.68510       -0.72420
H          2.53860       -0.87490        0.94360
H          3.84630        0.33380        0.62120
H          1.73100        1.45320        0.86440
H          2.39670        1.62600       -0.77530
H          0.79900       -0.11500       -1.61160
H         -0.05340        1.25430       -0.72310
H          0.64720       -1.48530        0.48300
H         -0.20430       -0.08150        1.25490
H         -1.06270       -1.64190       -1.24750
H         -1.72870       -1.85210        0.39180
H         -1.95810        0.68620       -1.32940
H         -3.19920       -0.53730       -0.97300
H         -2.96210        1.66910        0.40410
H         -3.31540        0.19030        1.22690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

