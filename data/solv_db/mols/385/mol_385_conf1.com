%nproc=16
%mem=2GB
%chk=mol_385_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.94650        0.36990        0.29150
C         -2.35600       -0.67490       -0.59490
C         -0.92630       -1.01520       -0.26950
C         -0.00520        0.17090       -0.38270
C          1.39910       -0.27650       -0.03710
C          2.31710        0.94210       -0.15830
N          3.68030        0.61530        0.15250
H         -4.06560        0.31000        0.16690
H         -2.77050        0.15710        1.36750
H         -2.66280        1.40300        0.00520
H         -2.94590       -1.61380       -0.44150
H         -2.50550       -0.38800       -1.64210
H         -0.59520       -1.78620       -0.99510
H         -0.83640       -1.42460        0.75600
H         -0.27000        0.98220        0.30620
H          0.03360        0.58450       -1.40880
H          1.47870       -0.76250        0.93860
H          1.74520       -0.98150       -0.84290
H          2.21990        1.31900       -1.19320
H          1.90900        1.70480        0.53820
H          4.32250        0.71040       -0.67920
H          3.78040       -0.34610        0.56480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

