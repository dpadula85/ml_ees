%nproc=16
%mem=2GB
%chk=mol_385_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44950        1.21260       -0.38570
C         -2.35460       -0.27090       -0.13420
C         -1.03810       -0.66460        0.49440
C          0.14050       -0.29360       -0.38940
C          1.38560       -0.73730        0.34800
C          2.64390       -0.43240       -0.41340
N          2.74490        0.97980       -0.63390
H         -3.52310        1.45810       -0.48340
H         -1.93720        1.51730       -1.32610
H         -2.06130        1.72840        0.50780
H         -2.50760       -0.88120       -1.02490
H         -3.14750       -0.52880        0.59100
H         -1.03970       -1.77900        0.55480
H         -0.89690       -0.21370        1.48520
H          0.04510       -0.79450       -1.38290
H          0.16820        0.80450       -0.54170
H          1.41220       -0.23320        1.31490
H          1.32710       -1.85830        0.43070
H          2.73200       -1.02350       -1.34730
H          3.49650       -0.73510        0.22840
H          2.03440        1.51120       -0.11870
H          2.82490        1.23430       -1.63470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_385_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

