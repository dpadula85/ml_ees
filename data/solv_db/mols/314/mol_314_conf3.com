%nproc=16
%mem=2GB
%chk=mol_314_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.56040       -1.02080        0.58650
C         -2.01100        0.19660       -0.07230
C         -2.86890        1.12230       -0.47000
C         -0.55980        0.39570       -0.29000
C          0.20210        0.44950        1.03060
C          1.57710        0.98620        0.65510
C          2.24560       -0.08360       -0.12370
C          1.51420       -0.81090       -0.93140
C          0.06960       -0.65950       -1.13820
C          3.67980       -0.29580        0.02670
O          4.38330        0.39330        0.79450
H         -2.95190       -0.71210        1.58880
H         -3.44330       -1.37380        0.01410
H         -1.81900       -1.81530        0.70440
H         -2.52420        2.03230       -0.95710
H         -3.92510        0.98400       -0.31490
H         -0.39240        1.37920       -0.77220
H         -0.30020        1.04740        1.78640
H          0.36570       -0.59390        1.39550
H          1.45660        1.85400       -0.02450
H          2.15530        1.29440        1.54320
H          2.02000       -1.58720       -1.49100
H         -0.39330       -1.66570       -0.98820
H         -0.09340       -0.43530       -2.22770
H          4.17350       -1.08100       -0.54440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

