%nproc=16
%mem=2GB
%chk=mol_314_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.55000        1.48210        0.56000
C         -1.96650        0.18470        0.11980
C         -2.68650       -0.92380        0.08570
C         -0.53420        0.20640       -0.27810
C          0.32520        0.79370        0.80920
C          1.54380       -0.03540        1.12890
C          2.16630       -0.43220       -0.15480
C          1.38570       -0.98480       -1.05820
C         -0.04460       -1.15750       -0.70660
C          3.58640       -0.19450       -0.34640
O          4.17320       -0.50980       -1.41390
H         -2.19840        2.28070       -0.14400
H         -2.13540        1.78550        1.55910
H         -3.65990        1.43750        0.59200
H         -3.72660       -0.88750        0.38550
H         -2.21780       -1.84390       -0.24180
H         -0.49420        0.84430       -1.20990
H         -0.27210        0.97040        1.73070
H          0.70230        1.80830        0.50270
H          1.21720       -0.95960        1.68910
H          2.24360        0.56850        1.73530
H          1.76500       -1.29860       -2.01100
H         -0.18880       -1.91720        0.09590
H         -0.61370       -1.47820       -1.59590
H          4.18020        0.26110        0.42510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

