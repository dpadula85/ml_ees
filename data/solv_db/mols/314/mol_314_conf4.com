%nproc=16
%mem=2GB
%chk=mol_314_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.96900        0.67380       -0.75070
C          1.89780       -0.03590        0.04720
C          2.24550       -0.45130        1.25660
C          0.56120       -0.29050       -0.55570
C         -0.25650       -1.31290        0.14140
C         -1.37090       -0.73020        0.97350
C         -2.20980        0.18930        0.15460
C         -1.60800        0.97360       -0.70350
C         -0.17150        0.98050       -0.87480
C         -3.65740        0.19250        0.32730
O         -4.18730       -0.57760        1.16230
H          2.90470        0.29390       -1.79730
H          3.98440        0.37170       -0.38200
H          2.85260        1.75880       -0.68560
H          3.25670       -0.29530        1.65540
H          1.58190       -0.93370        1.96740
H          0.80530       -0.74280       -1.56980
H         -0.76370       -2.00910       -0.59390
H          0.36250       -1.96710        0.76200
H         -2.01920       -1.57160        1.29550
H         -1.00930       -0.22850        1.88160
H         -2.21450        1.64100       -1.29350
H          0.05520        1.31640       -1.93060
H          0.29520        1.79250       -0.25400
H         -4.30370        0.83110       -0.23330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

