%nproc=16
%mem=2GB
%chk=mol_314_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.75900        1.17470        0.48470
C         -1.94720       -0.03970        0.16870
C         -2.39550       -1.23240        0.45080
C         -0.60390        0.13200       -0.44410
C          0.25970        1.05160        0.33740
C          1.47660        0.33720        0.87560
C          2.20000       -0.21360       -0.30210
C          1.49590       -1.00590       -1.09530
C          0.07340       -1.20020       -0.64580
C          3.57880        0.07710       -0.59080
O          4.26020        0.81840        0.13970
H         -2.35790        1.70230        1.36930
H         -2.74000        1.82590       -0.39360
H         -3.78490        0.87280        0.71510
H         -3.37750       -1.34460        0.92850
H         -1.86200       -2.13200        0.21960
H         -0.77050        0.56780       -1.46370
H         -0.27710        1.61950        1.12310
H          0.66800        1.84750       -0.35260
H          2.12360        1.02040        1.46570
H          1.15120       -0.49700        1.51710
H          1.89740       -1.47240       -1.98270
H          0.11010       -1.72820        0.31040
H         -0.47040       -1.82940       -1.37400
H          4.05080       -0.35170       -1.46120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

