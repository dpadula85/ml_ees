%nproc=16
%mem=2GB
%chk=mol_314_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.37860       -0.69040        0.89030
C         -1.95650        0.25310       -0.17760
C         -2.92670        0.90590       -0.80960
C         -0.51780        0.46760       -0.53430
C          0.23830        1.12810        0.56200
C          1.42120        0.38430        1.06870
C          2.20490       -0.30130        0.01350
C          1.52760       -0.88540       -0.95230
C          0.04680       -0.85540       -0.97790
C          3.65170       -0.32810        0.04550
O          4.26620       -0.92280       -0.87010
H         -3.28060       -0.27910        1.39280
H         -1.62540       -0.87260        1.65020
H         -2.69550       -1.67570        0.46950
H         -2.63620        1.59720       -1.59050
H         -3.96750        0.78890       -0.59370
H         -0.49450        1.13770       -1.42040
H         -0.44160        1.31140        1.44580
H          0.58470        2.16090        0.27710
H          2.11880        1.06680        1.59750
H          1.14870       -0.40290        1.82150
H          2.10200       -1.38420       -1.71970
H         -0.25520       -1.04610       -2.03040
H         -0.35970       -1.69910       -0.38070
H          4.22500        0.14110        0.82270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_314_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

