%nproc=16
%mem=2GB
%chk=mol_534_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99880        0.20090       -0.15690
C         -0.65910       -0.26680       -0.60390
C          0.45660        0.10480        0.37060
C          1.73370       -0.40700       -0.17030
O          1.83220       -1.64410       -0.44140
O          2.83210        0.41480       -0.41030
H         -2.78130       -0.59880       -0.15580
H         -2.35240        0.94140       -0.93000
H         -1.98300        0.72050        0.82400
H         -0.67670       -1.37200       -0.67250
H         -0.38820        0.09800       -1.62710
H          0.28570       -0.40880        1.35090
H          0.47500        1.20250        0.47220
H          3.22420        1.01460        0.30310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

