%nproc=16
%mem=2GB
%chk=mol_534_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97000       -0.04290        0.12980
C         -0.51110       -0.12430        0.52840
C          0.33960        0.31620       -0.64100
C          1.78130        0.26780       -0.33880
O          2.57470        0.61620       -1.25060
O          2.28390       -0.14140        0.89450
H         -2.65090       -0.27170        0.95150
H         -2.12430        0.97450       -0.29730
H         -2.12520       -0.78170       -0.70560
H         -0.31380        0.43400        1.44170
H         -0.30680       -1.21690        0.70460
H          0.09580        1.35910       -0.92630
H          0.16200       -0.36100       -1.50070
H          2.76470       -1.02770        1.00990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

