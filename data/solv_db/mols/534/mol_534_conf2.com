%nproc=16
%mem=2GB
%chk=mol_534_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90760        0.36290       -0.03940
C         -0.40970        0.42570       -0.14930
C          0.27350       -0.86870        0.18840
C          1.74360       -0.58700        0.19400
O          2.56060       -1.52390        0.42040
O          2.27170        0.60250       -0.31470
H         -2.23610        0.22170        0.99900
H         -2.32840       -0.39000       -0.71860
H         -2.29060        1.35650       -0.37510
H         -0.00830        1.20130        0.52070
H         -0.12410        0.70420       -1.17930
H          0.08910       -1.66420       -0.55630
H          0.00930       -1.24730        1.18990
H          2.35720        1.40610        0.31250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

