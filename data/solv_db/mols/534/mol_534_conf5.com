%nproc=16
%mem=2GB
%chk=mol_534_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.43350       -0.00310       -0.64840
C          0.82780       -0.05880        0.71200
C         -0.55040       -0.64240        0.74780
C         -1.51610        0.12120       -0.07620
O         -2.70380       -0.30140       -0.10310
O         -1.19030        1.25090       -0.80320
H          1.32800        1.03270       -1.07050
H          0.89230       -0.73230       -1.30160
H          2.51330       -0.26620       -0.66170
H          0.74970        0.98370        1.11710
H          1.46500       -0.63810        1.40130
H         -0.59760       -1.70060        0.45900
H         -0.90850       -0.56130        1.80860
H         -1.74270        1.51590       -1.58110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

