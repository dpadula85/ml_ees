%nproc=16
%mem=2GB
%chk=mol_534_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88780       -0.53170       -0.36910
C         -0.79840        0.25240        0.35430
C          0.56780       -0.12300       -0.19420
C          1.59030        0.65650        0.53070
O          1.27290        1.47760        1.44380
O          2.94940        0.54770        0.26850
H         -1.56660       -1.56830       -0.52550
H         -2.79730       -0.45670        0.29540
H         -2.18020       -0.04150       -1.31640
H         -1.00290        1.33800        0.24530
H         -0.89170       -0.01750        1.42220
H          0.71980       -1.19900        0.07580
H          0.61490       -0.03330       -1.28380
H          3.40970       -0.30130        0.02070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_534_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

