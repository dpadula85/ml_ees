%nproc=16
%mem=2GB
%chk=mol_243_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.54480        0.11800        0.21680
O          2.15970       -0.01780        0.53750
C          1.19380        0.15230       -0.43060
O          1.59510        0.42910       -1.59240
C         -0.23000        0.02980       -0.17850
C         -0.72310       -0.27120        1.07340
C         -2.09160       -0.38680        1.30540
C         -2.99770       -0.20280        0.28530
C         -2.50960        0.09940       -0.97240
C         -1.14130        0.21300       -1.19570
H          3.76610        1.15340       -0.13700
H          4.13080       -0.14430        1.11720
H          3.76630       -0.55080       -0.64170
H         -0.00530       -0.41630        1.87790
H         -2.46520       -0.62340        2.29210
H         -4.07480       -0.28680        0.43650
H         -3.18840        0.25620       -1.82240
H         -0.72960        0.44900       -2.17110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

