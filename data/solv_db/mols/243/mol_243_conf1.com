%nproc=16
%mem=2GB
%chk=mol_243_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.51690        0.47060       -0.08930
O          2.11900        0.65230        0.01360
C          1.23320       -0.38820       -0.18840
O          1.71000       -1.51780       -0.47050
C         -0.21310       -0.22480       -0.08770
C         -1.09040       -1.27700       -0.29290
C         -2.45590       -1.12920       -0.19890
C         -3.02510        0.08330        0.10660
C         -2.15380        1.14190        0.31360
C         -0.78530        0.99190        0.21900
H          3.73490       -0.41390       -0.72200
H          3.90270        0.22160        0.92960
H          4.01640        1.39120       -0.44370
H         -0.62880       -2.23090       -0.53390
H         -3.11280       -1.96240       -0.36300
H         -4.09550        0.23820        0.18930
H         -2.55870        2.11520        0.55650
H         -0.11360        1.83780        0.38550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

