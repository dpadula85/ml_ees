%nproc=16
%mem=2GB
%chk=mol_243_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.39810        0.74660        0.04430
O          1.98650        0.79440        0.14260
C          1.25840       -0.26290       -0.36480
O          1.85600       -1.22110       -0.89010
C         -0.19480       -0.25120       -0.28300
C         -0.84340        0.81550        0.30100
C         -2.23350        0.87020        0.40160
C         -2.97970       -0.17560       -0.09910
C         -2.32760       -1.23540       -0.67970
C         -0.94530       -1.29670       -0.78320
H          3.89140        1.36850        0.82370
H          3.73720       -0.32110        0.09130
H          3.75100        1.17640       -0.93560
H         -0.24120        1.64430        0.69740
H         -2.71690        1.71980        0.86510
H         -4.04030       -0.15410       -0.03280
H         -2.89720       -2.06950       -1.08020
H         -0.45860       -2.14790       -1.24770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

