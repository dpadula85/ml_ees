%nproc=16
%mem=2GB
%chk=mol_243_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.46990       -0.53130       -0.17150
O         -2.08760       -0.57300        0.09300
C         -1.18560        0.32600       -0.46070
O         -1.66630        1.19880       -1.22580
C          0.23320        0.24160       -0.15660
C          0.70420       -0.73800        0.69070
C          2.03790       -0.85970        1.00970
C          2.93940        0.01590        0.47410
C          2.51870        1.01430       -0.37940
C          1.15950        1.10820       -0.68090
H         -3.73480       -0.82200       -1.20140
H         -3.88620        0.45650        0.12740
H         -3.96080       -1.27360        0.51110
H         -0.02940       -1.42280        1.10470
H          2.34700       -1.64210        1.67500
H          3.98420       -0.07540        0.72100
H          3.26080        1.68800       -0.78340
H          0.83560        1.88860       -1.34710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

