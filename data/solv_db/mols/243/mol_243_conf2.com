%nproc=16
%mem=2GB
%chk=mol_243_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.42000        0.61420        0.22450
O         -2.01090        0.72350        0.16950
C         -1.26010       -0.41310       -0.04160
O         -1.85760       -1.50110       -0.17670
C          0.19510       -0.28290       -0.09580
C          0.80620        0.95430        0.06140
C          2.17210        1.13130        0.01890
C          2.93850        0.00880       -0.19090
C          2.38500       -1.24400       -0.35330
C          1.01850       -1.36630       -0.30240
H         -3.89720        0.53530       -0.75600
H         -3.68210       -0.22550        0.91870
H         -3.78000        1.57570        0.68160
H          0.14520        1.79840        0.22340
H          2.60690        2.09540        0.14340
H          4.02930        0.06820       -0.23750
H          3.02980       -2.12750       -0.52050
H          0.58110       -2.34480       -0.42900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_243_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

