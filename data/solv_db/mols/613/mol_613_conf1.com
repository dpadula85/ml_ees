%nproc=16
%mem=2GB
%chk=mol_613_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.81180       -0.05190        0.07890
C         -1.33500       -0.04730       -0.02240
C         -0.57840        0.33010        1.07600
C          0.79600        0.32410        0.95020
C          1.44310       -0.03870       -0.21320
N          0.67150       -0.39790       -1.25440
C         -0.67460       -0.40880       -1.18280
C          2.93500       -0.04070       -0.33580
H         -3.12840        0.99470        0.33000
H         -3.11150       -0.77080        0.87700
H         -3.21300       -0.34330       -0.91460
H         -1.07920        0.62170        2.00970
H          1.40830        0.61300        1.78860
H         -1.30140       -0.69730       -2.01810
H          3.38520        0.40370        0.55170
H          3.25040        0.56040       -1.23600
H          3.34400       -1.05100       -0.48460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

