%nproc=16
%mem=2GB
%chk=mol_613_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.80830        0.04010       -0.01120
C          1.30600       -0.01710        0.02830
C          0.54600        0.90440       -0.63470
C         -0.82890        0.83310       -0.58490
C         -1.45370       -0.16890        0.13450
N         -0.66760       -1.05640        0.77320
C          0.67700       -1.01440        0.74430
C         -2.94070       -0.24600        0.18790
H          3.11940        0.80260       -0.74800
H          3.20840        0.19320        0.99860
H          3.16980       -0.94030       -0.43050
H          1.07500        1.67460       -1.18790
H         -1.40640        1.57250       -1.11650
H          1.30070       -1.73330        1.26160
H         -3.20850       -1.25480        0.59570
H         -3.39450       -0.16750       -0.83570
H         -3.31040        0.57820        0.82540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

