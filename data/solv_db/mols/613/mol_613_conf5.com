%nproc=16
%mem=2GB
%chk=mol_613_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.80630       -0.13120       -0.04000
C          1.32380       -0.09390       -0.03750
C          0.62290        0.98180        0.46430
C         -0.77210        0.94930        0.43460
C         -1.47810       -0.11950       -0.07830
N         -0.75600       -1.14890       -0.55800
C          0.60530       -1.15620       -0.54750
C         -2.97350       -0.13510       -0.10140
H          3.21020       -0.13000        1.01260
H          3.17790       -1.05220       -0.50030
H          3.24930        0.75410       -0.57260
H          1.17150        1.82210        0.86750
H         -1.32210        1.78320        0.82370
H          1.17820       -1.98840       -0.93550
H         -3.34560       -0.72520       -0.94010
H         -3.31280        0.93710       -0.14160
H         -3.38520       -0.54710        0.85010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

