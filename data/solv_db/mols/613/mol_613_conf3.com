%nproc=16
%mem=2GB
%chk=mol_613_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.77310        0.21960        0.21800
C         -1.28600        0.15420        0.27080
C         -0.61090       -0.89470       -0.34550
C          0.77160       -0.94410       -0.28920
C          1.42070        0.05700        0.38060
N          0.77910        1.08720        0.98730
C         -0.58040        1.13710        0.93280
C          2.91120       -0.00860        0.43360
H         -3.17100        0.67810        1.12460
H         -3.02830        0.79480       -0.70740
H         -3.23670       -0.77940        0.10010
H         -1.16910       -1.66970       -0.86760
H          1.27800       -1.76910       -0.77560
H         -1.14270        1.92390        1.39170
H          3.32820        0.86190        0.99660
H          3.25520        0.11830       -0.61990
H          3.25410       -0.96670        0.86560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

