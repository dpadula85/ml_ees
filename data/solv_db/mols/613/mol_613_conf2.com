%nproc=16
%mem=2GB
%chk=mol_613_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.79760       -0.14270        0.10650
C          1.29900       -0.11630        0.04850
C          0.62870        1.09430        0.00540
C         -0.75280        1.06740       -0.04690
C         -1.45010       -0.12710       -0.05620
N         -0.74390       -1.26430       -0.01290
C          0.60140       -1.30790        0.03910
C         -2.94730       -0.11270       -0.11380
H          3.15610       -0.99560        0.74690
H          3.19740        0.82330        0.48360
H          3.17050       -0.28660       -0.93050
H          1.15720        2.02960        0.01180
H         -1.31960        1.98400       -0.08160
H          1.16010       -2.24200        0.07380
H         -3.29580        0.83460        0.36200
H         -3.33430       -0.25530       -1.12660
H         -3.32400       -0.98250        0.49100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_613_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

