%nproc=16
%mem=2GB
%chk=mol_032_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.93950       -1.44560       -0.43410
C         -3.37920       -0.30680        0.38930
C         -1.91420       -0.18820        0.24510
C         -1.30260        0.09480       -0.96750
C          0.08160        0.19490       -1.04910
C          0.88170        0.01980        0.05160
C          0.27420       -0.25950        1.25560
C         -1.07320       -0.35530        1.32330
F          1.08580       -0.40320        2.36010
C          2.34910        0.09580       -0.04570
C          2.99000        1.12680       -0.70860
C          4.37790        1.18250       -0.82830
C          5.12270        0.17360       -0.26470
C          4.52460       -0.86690        0.40050
C          3.13130       -0.88440        0.49670
C         -4.05580        0.96980        0.06560
O         -4.88430        0.98470       -0.87250
O         -3.74410        2.09120        0.81300
H         -5.03950       -1.37940       -0.37380
H         -3.66050       -2.40940        0.05960
H         -3.57330       -1.37220       -1.46530
H         -3.61000       -0.53480        1.44380
H         -1.93370        0.23310       -1.83740
H          0.53890        0.41810       -2.01350
H         -1.52340       -0.58100        2.30180
H          2.37410        1.90330       -1.13960
H          4.85670        1.99550       -1.34920
H          6.19630        0.17850       -0.33380
H          5.13430       -1.64260        0.83160
H          2.68400       -1.72860        1.00970
H         -2.96980        2.69560        0.63580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_032_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_032_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_032_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

