%nproc=16
%mem=2GB
%chk=mol_312_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.67050        0.16710        0.13910
C          1.20270        0.22340        0.47000
S          0.16510       -0.38420       -0.88250
C         -1.44440       -0.81680       -0.20630
C         -2.42070        0.33940       -0.21910
H          3.02130       -0.83520       -0.15190
H          2.85170        0.94310       -0.64060
H          3.21450        0.45740        1.07150
H          0.96610        1.30160        0.65960
H          0.97140       -0.37570        1.38690
H         -1.31130       -1.09100        0.87100
H         -1.89560       -1.71470       -0.68410
H         -2.54150        0.79920       -1.20930
H         -3.37980       -0.07570        0.18380
H         -2.07020        1.06200        0.55120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

