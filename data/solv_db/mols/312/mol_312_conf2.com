%nproc=16
%mem=2GB
%chk=mol_312_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.67100       -0.32240        0.20420
C         -1.36510       -0.29580       -0.56390
S          0.07400       -0.41850        0.52480
C          1.35780        0.67530       -0.15570
C          2.69890       -0.00090       -0.23660
H         -3.47110       -0.39050       -0.57660
H         -2.83570        0.57940        0.81400
H         -2.71860       -1.26740        0.76530
H         -1.30170        0.56920       -1.24340
H         -1.36450       -1.22590       -1.18740
H          1.43670        1.52190        0.57260
H          1.08610        1.05710       -1.16440
H          3.35090        0.66250       -0.85920
H          2.63590       -1.01150       -0.69870
H          3.08740       -0.13240        0.78690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

