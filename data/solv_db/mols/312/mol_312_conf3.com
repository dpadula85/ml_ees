%nproc=16
%mem=2GB
%chk=mol_312_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.75040        0.04120       -0.19160
C          1.32530       -0.40000       -0.38360
S          0.11700        0.66730        0.40410
C         -1.45790       -0.25020        0.47430
C         -2.63990        0.70020        0.56390
H          3.37240       -0.85090       -0.46530
H          3.06650        0.83760       -0.88940
H          3.00960        0.25130        0.86490
H          1.16430       -0.52890       -1.46360
H          1.23950       -1.40550        0.10010
H         -1.57550       -0.92490       -0.41210
H         -1.51280       -0.91210        1.36490
H         -3.41890        0.36280       -0.16520
H         -2.32950        1.71200        0.22860
H         -3.11060        0.70020        1.56510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

