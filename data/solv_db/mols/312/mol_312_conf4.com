%nproc=16
%mem=2GB
%chk=mol_312_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.72600        0.33820        0.23990
C          1.37810        0.15040       -0.38620
S          0.02310        0.10110        0.80080
C         -1.40670       -0.56700       -0.11930
C         -2.70410        0.03690        0.32320
H          2.92390        1.39270        0.47950
H          2.90090       -0.27000        1.14080
H          3.48200        0.02370       -0.52440
H          1.19420        0.97420       -1.09070
H          1.38470       -0.81580       -0.94560
H         -1.31240       -0.34630       -1.21140
H         -1.45530       -1.66440       -0.04010
H         -3.44480       -0.72890        0.61700
H         -2.52260        0.69670        1.18460
H         -3.16680        0.67840       -0.46800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

