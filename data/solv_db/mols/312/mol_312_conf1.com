%nproc=16
%mem=2GB
%chk=mol_312_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.77040       -0.40140        0.47910
C         -1.50370       -0.08110       -0.96190
S          0.23660        0.13820       -1.34790
C          1.19210        0.81650        0.02360
C          2.12890       -0.23520        0.60620
H         -1.01920       -1.05170        0.94420
H         -1.95330        0.52730        1.06270
H         -2.73930       -0.96090        0.50490
H         -2.13420        0.80150       -1.22940
H         -1.89460       -0.91150       -1.60290
H          0.52120        1.19330        0.81780
H          1.84820        1.62300       -0.38100
H          2.92140       -0.48810       -0.11790
H          1.54270       -1.14700        0.78410
H          2.62350        0.17700        1.49690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_312_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

