%nproc=16
%mem=2GB
%chk=mol_473_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.40090       -0.75230       -0.13810
C         -1.05190       -0.13270       -0.05960
C         -0.95560        1.25620       -0.05190
C          0.28290        1.86280        0.02010
C          1.44980        1.15750        0.08590
C          1.35010       -0.24100        0.07800
C          0.11510       -0.85490        0.00610
N          2.49180       -1.04790        0.14240
O          3.61740       -0.53330        0.20840
O          2.37120       -2.40360        0.13340
H         -2.32380       -1.76340        0.30410
H         -2.73780       -0.84720       -1.18400
H         -3.17050       -0.15160        0.38940
H         -1.85820        1.85540       -0.10250
H          0.34520        2.95750        0.02500
H          2.43500        1.59680        0.14320
H          0.04020       -1.95800        0.00010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

