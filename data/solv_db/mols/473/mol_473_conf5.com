%nproc=16
%mem=2GB
%chk=mol_473_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.41070       -0.73880       -0.05030
C         -1.06160       -0.13710       -0.05210
C         -0.93150        1.22540       -0.24260
C          0.29680        1.85950       -0.25660
C          1.44650        1.10270       -0.07260
C          1.33350       -0.25860        0.11890
C          0.10190       -0.86360        0.12840
N          2.51730       -1.03070        0.30760
O          3.63730       -0.49470        0.30110
O          2.46640       -2.38940        0.50290
H         -2.78460       -0.84750       -1.09610
H         -2.45240       -1.70750        0.49950
H         -3.11640       -0.05970        0.50100
H         -1.85070        1.80590       -0.38610
H          0.36900        2.92820       -0.40800
H          2.43710        1.52780       -0.07130
H          0.00190       -1.92190        0.27640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

