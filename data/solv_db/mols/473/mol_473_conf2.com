%nproc=16
%mem=2GB
%chk=mol_473_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.37950       -0.87890        0.27500
C          1.08250       -0.16670        0.08110
C          1.02330        1.18860       -0.17450
C         -0.19020        1.82240       -0.34960
C         -1.39150        1.14830       -0.28030
C         -1.33370       -0.20930       -0.02440
C         -0.12130       -0.83820        0.14980
N         -2.56610       -0.91490        0.04910
O         -2.50470       -2.13860        0.28000
O         -3.76960       -0.31010       -0.12010
H          2.46210       -1.31560        1.29040
H          2.42970       -1.64710       -0.52190
H          3.18010       -0.11980        0.19820
H          1.96630        1.73380       -0.23190
H         -0.23550        2.89250       -0.55150
H         -2.34060        1.65100       -0.41870
H         -0.07050       -1.89740        0.34940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

