%nproc=16
%mem=2GB
%chk=mol_473_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.44180       -0.75450        0.07720
C          1.09490       -0.09780        0.00980
C          0.95240        1.26680       -0.15260
C         -0.29470        1.86850       -0.21380
C         -1.44600        1.09490       -0.11090
C         -1.32240       -0.27880        0.05270
C         -0.07900       -0.82830        0.10760
N         -2.50700       -1.06100        0.15610
O         -2.43330       -2.29750        0.30260
O         -3.74160       -0.49880        0.09970
H          2.27910       -1.72980        0.58440
H          3.19060       -0.14320        0.56850
H          2.70130       -1.01200       -0.98300
H          1.86670        1.83720       -0.22920
H         -0.34400        2.94470       -0.34240
H         -2.40170        1.59580       -0.16240
H          0.04280       -1.90630        0.23570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

