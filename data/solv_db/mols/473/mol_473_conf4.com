%nproc=16
%mem=2GB
%chk=mol_473_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.28380       -1.04100        0.01090
C         -1.03840       -0.25580        0.01690
C         -1.12200        1.12970        0.09630
C          0.02480        1.89620        0.10470
C          1.25440        1.25150        0.03240
C          1.36670       -0.12300       -0.04730
C          0.19520       -0.87550       -0.05420
N          2.64590       -0.73670       -0.11960
O          2.72980       -1.97700       -0.19100
O          3.76370        0.02050       -0.11060
H         -2.06520       -2.10230       -0.21750
H         -3.00670       -0.61580       -0.73580
H         -2.82500       -1.02900        0.97740
H         -2.07660        1.60870        0.15120
H         -0.01920        2.98150        0.16630
H          2.18880        1.81950        0.03620
H          0.26750       -1.95140       -0.11610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_473_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

