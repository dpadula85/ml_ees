%nproc=16
%mem=2GB
%chk=mol_316_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.55550        0.59770       -0.21890
C         -2.30090       -0.25130       -0.36340
C         -1.48880       -0.06650        0.93230
S          0.00760       -1.06410        0.84150
C          1.20420       -0.39350       -0.34910
C          2.52700       -0.26500        0.40650
C          3.64370        0.27890       -0.41460
H         -4.23590        0.17380        0.56040
H         -3.34890        1.64840        0.00950
H         -4.11760        0.58750       -1.20250
H         -2.60240       -1.32810       -0.44790
H         -1.70070       -0.01710       -1.24640
H         -2.14310       -0.51050        1.72320
H         -1.34530        1.00250        1.12210
H          1.34150       -1.16400       -1.13280
H          0.89160        0.59950       -0.71050
H          2.37590        0.35930        1.28950
H          2.82680       -1.32960        0.72290
H          3.36730        0.91350       -1.26570
H          4.23250       -0.58050       -0.86780
H          4.42110        0.80900        0.21320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

