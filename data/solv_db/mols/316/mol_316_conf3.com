%nproc=16
%mem=2GB
%chk=mol_316_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.47220        0.04380       -0.89290
C         -2.63500       -0.20760        0.35820
C         -1.28820       -0.63580       -0.15360
S          0.00350       -0.63380        1.09520
C          1.58890       -0.67240        0.23110
C          2.18000        0.72940        0.31840
C          3.51030        0.66550       -0.38120
H         -4.47160        0.43590       -0.66220
H         -2.87390        0.76800       -1.50220
H         -3.53790       -0.87160       -1.50490
H         -2.53310        0.79150        0.84390
H         -3.10880       -0.94950        1.02450
H         -1.00860        0.10860       -0.93930
H         -1.34030       -1.61330       -0.65310
H          1.47680       -0.88160       -0.85990
H          2.28810       -1.42440        0.63410
H          2.25280        1.06810        1.37440
H          1.46180        1.40320       -0.18450
H          4.25330        1.27150        0.15420
H          3.33870        0.97790       -1.43190
H          3.91530       -0.37350       -0.41280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

