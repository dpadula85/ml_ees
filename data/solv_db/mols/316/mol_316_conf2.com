%nproc=16
%mem=2GB
%chk=mol_316_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.42260        0.28120       -0.64240
C          2.59430       -0.78900        0.00730
C          1.12270       -0.65050       -0.28810
S          0.43860        0.91610        0.28040
C         -1.31530        0.98820       -0.11900
C         -2.13250       -0.03200        0.64830
C         -3.57490        0.13370        0.22430
H          3.17690        0.39270       -1.71920
H          3.40740        1.24740       -0.10510
H          4.48130       -0.06850       -0.61320
H          2.83070       -0.81690        1.06700
H          2.91090       -1.76790       -0.42360
H          0.61970       -1.50870        0.24350
H          0.91090       -0.80380       -1.36490
H         -1.70030        1.99610        0.17390
H         -1.45390        0.82030       -1.18650
H         -2.07850        0.19340        1.73630
H         -1.83990       -1.06780        0.40070
H         -4.10800       -0.82880        0.40160
H         -3.62540        0.41540       -0.86250
H         -4.08730        0.94950        0.76460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

