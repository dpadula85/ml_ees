%nproc=16
%mem=2GB
%chk=mol_316_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.87370        0.80060       -0.11100
C         -2.30330       -0.38880       -0.80940
C         -1.65890       -1.38610        0.13050
S         -0.30520       -0.68970        1.07040
C          1.09940       -0.23690        0.01700
C          2.20860        0.34220        0.85520
C          3.40620        0.73690        0.02180
H         -3.99560        0.75080       -0.22920
H         -2.49190        1.73040       -0.57370
H         -2.60330        0.77070        0.96550
H         -1.59350       -0.04340       -1.58690
H         -3.09220       -0.97540       -1.36770
H         -2.38570       -1.86760        0.78770
H         -1.23100       -2.19110       -0.53240
H          1.47770       -1.17560       -0.44270
H          0.78800        0.41540       -0.80390
H          2.54790       -0.42350        1.59020
H          1.88670        1.24790        1.41480
H          3.32820        1.81980       -0.22090
H          3.49160        0.12140       -0.90010
H          4.30000        0.64220        0.67230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

