%nproc=16
%mem=2GB
%chk=mol_316_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.00730        0.07550        0.29840
C         -1.96570       -0.45260       -0.69910
C         -0.92540        0.63060       -0.92950
S         -0.08990        1.05420        0.63440
C          0.90370       -0.36460        1.16970
C          2.05450       -0.61740        0.21440
C          2.97420        0.56480        0.12050
H         -3.45730        1.02130       -0.03110
H         -3.78520       -0.70300        0.40420
H         -2.43550        0.19820        1.26270
H         -1.53840       -1.37840       -0.25550
H         -2.49400       -0.73310       -1.62750
H         -0.20030        0.29300       -1.69740
H         -1.46900        1.53750       -1.25620
H          1.23000       -0.16700        2.20620
H          0.24090       -1.26180        1.20840
H          1.59650       -0.78040       -0.80460
H          2.58820       -1.54660        0.48190
H          2.59970        1.47980        0.63170
H          3.96650        0.29140        0.57260
H          3.21370        0.85850       -0.92290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_316_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

