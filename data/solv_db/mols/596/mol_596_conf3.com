%nproc=16
%mem=2GB
%chk=mol_596_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11020        0.28690       -0.12680
O         -0.75620        0.19250       -0.47800
C          0.16590       -0.45570        0.28990
O         -0.24800       -0.98660        1.35780
C          1.59680       -0.51320       -0.14160
C          2.24790        0.79650        0.01470
N          2.78210        1.82060        0.12840
H         -2.41650       -0.41000        0.67330
H         -2.66180        0.03400       -1.08340
H         -2.45030        1.33620        0.10420
H          2.13840       -1.27090        0.46420
H          1.71180       -0.83020       -1.20270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

