%nproc=16
%mem=2GB
%chk=mol_596_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93790        0.62070       -0.27880
O          0.58530        0.60690        0.15850
C         -0.12060       -0.57320        0.16610
O          0.47710       -1.61110       -0.22350
C         -1.54670       -0.66500        0.61240
C         -2.04580        0.64650        1.02230
N         -2.45430        1.68290        1.33710
H          2.43670       -0.25820        0.20890
H          2.45540        1.55200       -0.05120
H          1.98490        0.41560       -1.36900
H         -1.59230       -1.32780        1.50450
H         -2.11760       -1.08920       -0.24500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

