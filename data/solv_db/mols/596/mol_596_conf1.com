%nproc=16
%mem=2GB
%chk=mol_596_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.08400        0.06610        0.13330
O          0.74960       -0.09390        0.48300
C         -0.23810       -0.30160       -0.46520
O          0.09190       -0.34740       -1.67150
C         -1.65630       -0.42960       -0.02360
C         -2.07160        0.84970        0.52970
N         -2.39220        1.85790        0.99070
H          2.68050        0.09570        1.09070
H          2.26500        0.98710       -0.44910
H          2.49660       -0.79670       -0.43420
H         -2.25450       -0.66590       -0.93320
H         -1.75490       -1.22140        0.74940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

