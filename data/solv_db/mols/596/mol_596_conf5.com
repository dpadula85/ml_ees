%nproc=16
%mem=2GB
%chk=mol_596_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09760        0.41580       -0.43610
O          0.77240        0.15630       -0.86520
C         -0.14020       -0.39460        0.02730
O          0.27990       -0.63000        1.19590
C         -1.54390       -0.65910       -0.37730
C         -2.26080        0.62050       -0.51590
N         -2.82380        1.63240       -0.60050
H          2.74460        0.68200       -1.28070
H          2.41530       -0.51480        0.08010
H          2.14600        1.21670        0.31570
H         -2.03650       -1.23610        0.45620
H         -1.65050       -1.28910       -1.26490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

