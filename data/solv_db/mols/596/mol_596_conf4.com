%nproc=16
%mem=2GB
%chk=mol_596_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.18600        0.12440       -0.31820
O          0.84280       -0.36100       -0.46520
C         -0.13420       -0.08320        0.46960
O          0.10830        0.59820        1.49050
C         -1.51530       -0.57620        0.27600
C         -2.42930        0.53960       -0.05630
N         -3.16710        1.40960       -0.31530
H          2.43850        0.70070       -1.23470
H          2.81510       -0.78880       -0.19730
H          2.28370        0.78800        0.55470
H         -1.86070       -0.98160        1.24810
H         -1.56810       -1.36970       -0.49740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_596_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

