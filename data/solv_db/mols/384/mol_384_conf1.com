%nproc=16
%mem=2GB
%chk=mol_384_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.11650        1.61830       -0.09320
C          1.39600        2.12080        0.07010
C          2.47140        1.27290        0.21180
C          2.26170       -0.10320        0.18930
C          0.97240       -0.56590        0.02440
C         -0.11380        0.24280       -0.11810
C         -1.47450       -0.21840       -0.30300
C         -2.28840        0.31910       -1.26430
C         -3.57150       -0.16460       -1.40440
C         -4.05220       -1.18500       -0.58970
C         -3.24470       -1.72580        0.37220
C         -1.95510       -1.22210        0.49520
F         -1.17590       -1.78190        1.47910
F         -5.33540       -1.63260       -0.76750
C          3.39090       -1.01390        0.33810
O          3.17770       -2.25090        0.31480
O          4.69030       -0.57050        0.50430
O          3.76590        1.74670        0.37730
H         -0.73620        2.29390       -0.20550
H          1.55860        3.19030        0.08730
H          0.81720       -1.65190       -0.00020
H         -1.95930        1.12400       -1.93220
H         -4.26360        0.22830       -2.15550
H         -3.58670       -2.51560        1.01780
H          5.18270       -0.28730       -0.35250
H          3.95600        2.73270        0.39850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

