%nproc=16
%mem=2GB
%chk=mol_384_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.46800       -1.73020       -0.65770
C          1.80750       -1.97370       -0.64010
C          2.74430       -1.02180       -0.21460
C          2.32470        0.21840        0.20850
C          0.94350        0.44270        0.18070
C          0.02980       -0.49630       -0.23820
C         -1.37730       -0.16820       -0.22600
C         -1.79380        1.06610       -0.73020
C         -3.12660        1.42320       -0.74260
C         -4.06320        0.52990       -0.24160
C         -3.64890       -0.67130        0.24820
C         -2.31080       -1.05100        0.27020
F         -1.96510       -2.25950        0.77440
F         -5.35620        0.93270       -0.27870
C          3.20860        1.28090        0.66930
O          4.44020        1.19140        0.73380
O          2.62550        2.49830        1.07110
O          4.05150       -1.36030       -0.23430
H         -0.23920       -2.50450       -1.00200
H          2.20140       -2.92110       -0.95760
H          0.62860        1.43680        0.52140
H         -1.08470        1.78820       -1.13010
H         -3.42520        2.40030       -1.14510
H         -4.42780       -1.34470        0.63400
H          2.45850        3.22160        0.36410
H          4.88660       -0.92770       -0.00800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

