%nproc=16
%mem=2GB
%chk=mol_384_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.18970       -1.14460       -1.28770
C         -1.46960       -1.54590       -1.53880
C         -2.54300       -1.06210       -0.77260
C         -2.26930       -0.16380        0.23150
C         -0.93110        0.21330        0.45190
C          0.12670       -0.25100       -0.28090
C          1.50390        0.17290       -0.04510
C          2.00890        0.18410        1.24070
C          3.31730        0.56330        1.49000
C          4.16030        0.94220        0.46650
C          3.66710        0.93560       -0.82200
C          2.35680        0.55470       -1.05720
F          1.88440        0.57330       -2.32280
F          5.44660        1.31310        0.72370
C         -3.32060        0.54190        0.96010
O         -2.95270        1.36080        1.88340
O         -4.66420        0.45610        0.77950
O         -3.80110       -1.52980       -1.05520
H          0.64840       -1.53070       -1.87350
H         -1.68930       -2.24010       -2.32620
H         -0.70880        0.91080        1.24690
H          1.33100       -0.11800        2.04130
H          3.66810        0.55570        2.51870
H          4.34380        1.23610       -1.61160
H         -5.27930        0.40280        1.57880
H         -4.64430       -1.33060       -0.61930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

