%nproc=16
%mem=2GB
%chk=mol_384_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.02920       -1.57760        0.91770
C         -1.25800       -2.08970        1.20260
C         -2.43740       -1.43080        0.76910
C         -2.33750       -0.26390        0.07400
C         -1.06160        0.25680       -0.22580
C          0.06520       -0.39640        0.19380
C          1.37940        0.14760       -0.13050
C          2.36800       -0.67510       -0.61940
C          3.63240       -0.17230       -0.94530
C          3.91360        1.16870       -0.78100
C          2.91490        1.99630       -0.28740
C          1.67170        1.48590        0.03010
F          0.77420        2.37870        0.51220
F          5.12960        1.69340       -1.08660
C         -3.46390        0.60330       -0.16730
O         -4.64040        0.43790        0.19950
O         -3.28000        1.80370       -0.90800
O         -3.64240       -2.02570        1.04320
H          0.90450       -2.06480        1.23990
H         -1.36290       -3.00340        1.76160
H         -0.96290        1.17440       -0.79590
H          2.24920       -1.73570       -0.78400
H          4.40110       -0.83110       -1.32900
H          3.16880        3.03970       -0.17130
H         -3.56680        1.80940       -1.88130
H         -4.52970       -1.72930        0.78060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

