%nproc=16
%mem=2GB
%chk=mol_384_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.37510       -1.45190        0.67610
C         -1.72570       -1.68220        0.93780
C         -2.62470       -0.69200        0.66570
C         -2.17900        0.50650        0.14120
C         -0.84450        0.73930       -0.11950
C          0.06960       -0.25610        0.15270
C          1.46790       -0.05470       -0.10380
C          2.21670       -1.05450       -0.73420
C          3.55510       -0.86430       -0.98090
C          4.18600        0.28540       -0.62540
C          3.43380        1.27520        0.00140
C          2.08900        1.11970        0.26510
F          1.43420        2.13320        0.87840
F          5.50400        0.43460       -0.88510
C         -3.16780        1.53030       -0.13240
O         -2.81630        2.63480       -0.60820
O         -4.51900        1.34510        0.11200
O         -3.98280       -0.90860        0.92300
H          0.28990       -2.27740        0.91370
H         -2.04480       -2.63790        1.35150
H         -0.59530        1.70610       -0.52850
H          1.75900       -1.98970       -1.03700
H          4.09530       -1.65390       -1.46610
H          3.95040        2.18230        0.27720
H         -4.86780        1.40510        1.05190
H         -4.30800       -1.77450        1.30240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_384_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

