%nproc=16
%mem=2GB
%chk=mol_077_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
S         -0.00360        0.53790       -0.00000
H         -1.14950       -0.27030       -0.00000
H          1.15310       -0.26770        0.00000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_077_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_077_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_077_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

