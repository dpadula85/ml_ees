%nproc=16
%mem=2GB
%chk=mol_354_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.51470        0.09010        0.48040
C          2.94440       -0.39910       -0.66300
C          1.56320       -0.48730       -0.76770
C          0.70350       -0.09120        0.24620
C          1.30660        0.40590        1.40410
C          2.68030        0.48800        1.51810
C         -0.73750       -0.07350        0.11470
C         -1.35930        0.94350       -0.54380
C         -2.74640        1.01580       -0.69180
C         -3.50780        0.00300       -0.14010
C         -2.89200       -1.03750        0.54050
C         -1.51190       -1.07920        0.67280
Cl        -0.66580       -2.33650        1.55730
Cl        -5.24630        0.10350       -0.36440
Cl        -0.39710        2.25050       -1.19820
H          4.58780        0.15990        0.60550
H          3.53240       -0.73930       -1.51460
H          1.14110       -0.88500       -1.67910
H          0.63710        0.72540        2.21390
H          3.11020        0.89050        2.44490
H         -3.16940        1.85110       -1.23680
H         -3.48770       -1.79880        0.98080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

