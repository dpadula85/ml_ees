%nproc=16
%mem=2GB
%chk=mol_354_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.49390        0.09700       -0.21560
C          2.77480        1.13490        0.34650
C          1.41210        1.05440        0.36910
C          0.72850       -0.03390       -0.15260
C          1.47570       -1.05540       -0.70800
C          2.85150       -1.00190       -0.74530
C         -0.70820       -0.04290       -0.07620
C         -1.38330       -0.41290        1.05630
C         -2.77370       -0.42050        1.11490
C         -3.50450       -0.04950        0.01790
C         -2.84680        0.32600       -1.12870
C         -1.46270        0.32520       -1.16370
Cl        -0.63160        0.79630       -2.62060
Cl        -5.27930       -0.05070        0.05230
Cl        -0.48210       -0.89530        2.49670
H          4.56600        0.17860       -0.22350
H          3.28010        2.01560        0.77190
H          0.85200        1.86710        0.80650
H          0.94290       -1.90880       -1.11780
H          3.42350       -1.82350       -1.19000
H         -3.30610       -0.71690        2.01860
H         -3.42280        0.61730       -1.98120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

