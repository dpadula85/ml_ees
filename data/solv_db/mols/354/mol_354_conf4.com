%nproc=16
%mem=2GB
%chk=mol_354_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.48850        0.00970        0.23910
C          2.70330       -0.50820        1.28220
C          1.32370       -0.46700        1.21870
C          0.69480        0.08960        0.12920
C          1.42790        0.61000       -0.91720
C          2.80370        0.54910       -0.83120
C         -0.74980        0.01000        0.00630
C         -1.54220        0.80790        0.79960
C         -2.89990        0.76220        0.59580
C         -3.45460       -0.05740       -0.36730
C         -2.62460       -0.84880       -1.14500
C         -1.26510       -0.82380       -0.96550
Cl        -0.20230       -1.85010       -1.94730
Cl        -5.18330       -0.04170       -0.53280
Cl        -0.84470        1.77200        2.11970
H          4.56660       -0.02600        0.31630
H          3.23540       -0.92940        2.11800
H          0.78800       -0.95390        2.03570
H          0.95520        1.05110       -1.81160
H          3.38880        0.90590       -1.68420
H         -3.51440        1.40600        1.23040
H         -3.09500       -1.46720       -1.88890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

