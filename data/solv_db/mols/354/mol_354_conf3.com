%nproc=16
%mem=2GB
%chk=mol_354_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.52340        0.05270       -0.01960
C         -2.84100       -0.36460       -1.14230
C         -1.43920       -0.32490       -1.15830
C         -0.73350        0.12360       -0.06750
C         -1.43550        0.53780        1.03790
C         -2.81400        0.51170        1.08770
C          0.71070        0.07120       -0.04670
C          1.54860        0.98700       -0.63410
C          2.92910        0.88660       -0.57360
C          3.51810       -0.18390        0.10540
C          2.69490       -1.12930        0.70700
C          1.33010       -0.98900        0.62500
Cl         0.31080       -2.17350        1.41340
Cl         5.25550       -0.32950        0.23880
Cl         0.85170        2.31910       -1.50680
H         -4.60450        0.01220       -0.02290
H         -3.35300       -0.72860       -2.02140
H         -0.91130       -0.65090       -2.05010
H         -0.90370        0.89790        1.88230
H         -3.37330        0.82970        1.94880
H          3.59550        1.59960       -1.01950
H          3.18740       -1.95480        1.21650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

