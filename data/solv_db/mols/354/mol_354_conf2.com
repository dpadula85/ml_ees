%nproc=16
%mem=2GB
%chk=mol_354_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.45900        0.04540        0.00340
C         -2.76550       -0.76160        0.88890
C         -1.39030       -0.77490        0.90180
C         -0.72580        0.03750        0.00790
C         -1.40010        0.84720       -0.87970
C         -2.77860        0.85680       -0.88830
C          0.71310        0.02210        0.01190
C          1.37130       -0.99020       -0.65540
C          2.74740       -1.07150       -0.68830
C          3.44990       -0.08360       -0.01590
C          2.80130        0.93330        0.65440
C          1.42340        1.00040        0.67730
Cl         0.59700        2.31230        1.53830
Cl         5.21230       -0.17970       -0.05190
Cl         0.50010       -2.25980       -1.51850
H         -4.53490        0.06250       -0.01530
H         -3.29560       -1.39710        1.59020
H         -0.85320       -1.42070        1.61630
H         -0.89980        1.50030       -1.59270
H         -3.35780        1.47090       -1.56170
H          3.27260       -1.86080       -1.20950
H          3.37210        1.71120        1.18680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_354_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

