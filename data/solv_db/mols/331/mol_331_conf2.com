%nproc=16
%mem=2GB
%chk=mol_331_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.34660        0.13970        0.46170
C         -0.33860       -0.49410       -0.72440
O         -1.57620        0.18800       -0.92160
N         -2.52500       -0.04960        0.05960
O         -3.09030        0.88850        0.66970
O         -3.16340       -1.26800       -0.01670
O          1.71000       -0.20310        0.55350
N          2.65200        0.43240       -0.20620
O          2.37690        1.42560       -0.92330
O          3.73470       -0.32420       -0.62220
H          0.29760        1.24320        0.30310
H         -0.16990       -0.09910        1.42270
H          0.26700       -0.30940       -1.61920
H         -0.52140       -1.56990       -0.58170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

