%nproc=16
%mem=2GB
%chk=mol_331_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.57670        0.24280       -0.47520
C         -0.54400       -0.77780       -0.34140
O         -1.70440       -0.17840       -0.86770
N         -2.80850       -0.01700       -0.06330
O         -2.93510        1.12890        0.43540
O         -3.95210       -0.72430       -0.38750
O          1.74010       -0.36340        0.04740
N          2.82790        0.46120        0.23200
O          2.63010        1.45130        0.98340
O          4.06400       -0.13790        0.36280
H          0.68900        0.41550       -1.55800
H          0.32370        1.20130        0.00370
H         -0.66760       -1.07530        0.72270
H         -0.23980       -1.62670       -0.98380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

