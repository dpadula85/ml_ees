%nproc=16
%mem=2GB
%chk=mol_331_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.63250        0.45890        0.16570
C         -0.72070       -0.19020        0.22780
O         -1.73640        0.63720       -0.34040
N         -2.99670        0.06670       -0.22630
O         -3.36230       -0.10590        0.96590
O         -3.21160       -1.01730       -1.05030
O          1.61900       -0.56070        0.30920
N          2.90100       -0.14060        0.56220
O          3.74690       -0.95830        0.98480
O          3.25770        1.18500        0.67210
H          0.85010        0.94810       -0.79420
H          0.76060        1.14000        1.03250
H         -0.99260       -0.30110        1.29860
H         -0.74760       -1.16180       -0.29730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

