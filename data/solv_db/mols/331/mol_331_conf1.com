%nproc=16
%mem=2GB
%chk=mol_331_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.70840       -0.56220       -0.29330
C          0.74800       -0.55090        0.07030
O          1.33460        0.71780       -0.19530
N          2.65480        0.86850        0.08850
O          3.01480        1.14120        1.23320
O          3.57590        0.71680       -0.89760
O         -1.40940        0.41600        0.45760
N         -2.74820        0.53220        0.24240
O         -3.52880        0.71630        1.17980
O         -3.23080        0.44350       -1.02760
H         -1.09440       -1.58300       -0.04540
H         -0.80590       -0.35280       -1.37520
H          0.91700       -0.86150        1.12090
H          1.28080       -1.30180       -0.55820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

