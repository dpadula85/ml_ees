%nproc=16
%mem=2GB
%chk=mol_331_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.36500       -0.24500        0.31290
C         -0.57090        0.42960       -0.72790
O         -1.77580       -0.16510       -0.85690
N         -2.66900       -0.17140        0.22520
O         -2.70060       -1.10400        1.02080
O         -3.59080        0.81660        0.28830
O          1.39190        0.64530        0.48070
N          2.69050        0.36680        0.48160
O          3.58780        0.12160        1.21950
O          3.13120        0.53990       -0.91070
H         -0.26460       -0.46250        1.20610
H          0.73220       -1.21050       -0.08900
H         -0.00290       -0.02550       -1.73320
H         -0.32390        1.45460       -0.91750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_331_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

