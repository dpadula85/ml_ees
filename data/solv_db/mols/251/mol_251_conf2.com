%nproc=16
%mem=2GB
%chk=mol_251_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.95640       -0.72460       -0.01120
C         -0.53130       -0.31960       -0.00840
C          0.48030       -1.26540       -0.03560
C          1.80640       -0.87210       -0.03150
C          2.17870        0.45570       -0.00090
C          1.15570        1.39040        0.02600
C         -0.17090        1.01180        0.02240
O         -1.20700        1.94390        0.04920
H         -2.56490       -0.17950        0.72440
H         -2.03870       -1.81070        0.27890
H         -2.43960       -0.61470       -1.00070
H          0.22780       -2.32790       -0.06030
H          2.61520       -1.60180       -0.05250
H          3.21600        0.76620        0.00270
H          1.39460        2.44600        0.05030
H         -2.16600        1.70240        0.04730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

