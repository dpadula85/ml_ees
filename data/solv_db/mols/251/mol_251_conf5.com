%nproc=16
%mem=2GB
%chk=mol_251_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.18530        0.25820       -0.13810
C         -0.72440       -0.06320       -0.04820
C         -0.31210       -1.37900       -0.02980
C          1.04230       -1.66930        0.05360
C          2.01470       -0.69700        0.12040
C          1.59130        0.61590        0.10140
C          0.24130        0.91700        0.01820
O         -0.21850        2.24280       -0.00310
H         -2.59580       -0.06420       -1.10050
H         -2.28900        1.36550       -0.08700
H         -2.71580       -0.13940        0.74530
H         -1.07060       -2.14910       -0.08160
H          1.35290       -2.70810        0.06700
H          3.07280       -0.93250        0.18550
H          2.31510        1.43200        0.15170
H          0.48120        2.97040        0.04510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

