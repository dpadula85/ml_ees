%nproc=16
%mem=2GB
%chk=mol_251_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.16170       -0.04320        0.17280
C          0.69070       -0.20480        0.08010
C          0.07260       -1.43240        0.17850
C         -1.30270       -1.50690        0.08280
C         -2.06270       -0.37750       -0.10870
C         -1.44110        0.84780       -0.20660
C         -0.06750        0.95310       -0.11470
O          0.58510        2.17560       -0.20990
H          2.50430        0.46440       -0.77390
H          2.47000        0.63640        0.99390
H          2.65750       -1.02110        0.30710
H          0.68640       -2.30790        0.32890
H         -1.77480       -2.46930        0.16100
H         -3.15560       -0.45710       -0.18230
H         -2.02500        1.75350       -0.35830
H          0.00110        2.98930       -0.35070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

