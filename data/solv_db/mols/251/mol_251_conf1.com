%nproc=16
%mem=2GB
%chk=mol_251_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.02940        0.40720        0.17860
C          0.55860        0.24290        0.04840
C         -0.28630        1.33200        0.00250
C         -1.65890        1.19330       -0.11870
C         -2.14780       -0.09430       -0.19220
C         -1.30490       -1.18630       -0.14650
C          0.05920       -1.03970       -0.02590
O          0.88500       -2.16690        0.01750
H          2.55690       -0.23250       -0.56090
H          2.27780        0.14200        1.23090
H          2.32330        1.48110        0.04980
H          0.11610        2.33170        0.06150
H         -2.32490        2.05080       -0.15480
H         -3.22290       -0.24750       -0.28810
H         -1.74600       -2.17420       -0.20860
H          1.88530       -2.03960        0.10650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

