%nproc=16
%mem=2GB
%chk=mol_251_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06350        0.24150        0.05870
C          0.56880        0.21070        0.01290
C         -0.17580        1.39520       -0.06330
C         -1.54920        1.31200       -0.10290
C         -2.18920        0.09270       -0.06890
C         -1.41330       -1.06880        0.00730
C         -0.03580       -1.02990        0.04900
O          0.70490       -2.21590        0.12520
H          2.39550        1.29800       -0.06540
H          2.39860       -0.20390        1.01800
H          2.43650       -0.36090       -0.81320
H          0.36240        2.34460       -0.08850
H         -2.12220        2.22210       -0.16160
H         -3.26580       -0.01840       -0.09790
H         -1.89380       -2.02620        0.03460
H          1.71490       -2.19280        0.15590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_251_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

