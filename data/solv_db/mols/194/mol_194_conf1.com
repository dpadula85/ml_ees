%nproc=16
%mem=2GB
%chk=mol_194_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24980        1.69630        0.51360
C         -0.68150        0.40400        0.06430
C          0.81010        0.40860        0.33020
O          1.42000       -0.75250       -0.20530
N          2.77930       -0.88700       -0.03970
O          3.60100       -0.44430       -0.86460
O          3.28470       -1.53390        1.07550
O         -1.21280       -0.72950        0.67110
N         -1.80500       -1.78600        0.10300
O         -1.32220       -2.25240       -0.94650
O         -2.92430       -2.40860        0.59500
H         -2.06370        2.07150       -0.16750
H         -1.60840        1.67560        1.56370
H         -0.45080        2.46840        0.48150
H         -0.81450        0.33050       -1.04600
H          1.27880        1.30800       -0.12540
H          0.95910        0.43130        1.44760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

