%nproc=16
%mem=2GB
%chk=mol_194_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.46870        1.65220       -0.42030
C          0.61220        0.23040        0.05000
C         -0.68320       -0.53240       -0.14970
O         -1.75320        0.11010        0.50440
N         -2.99980       -0.44970        0.42430
O         -3.90460        0.24210       -0.11040
O         -3.28190       -1.71400        0.89980
O          1.66710       -0.42530       -0.60930
N          2.73960       -0.94700        0.04360
O          3.85160       -0.94050       -0.53140
O          2.66000       -1.47730        1.29670
H         -0.40710        1.80440       -1.08240
H          1.41840        1.97610       -0.90540
H          0.31860        2.34220        0.45630
H          0.81470        0.23850        1.14790
H         -0.62800       -1.55520        0.22560
H         -0.89310       -0.55450       -1.23970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

