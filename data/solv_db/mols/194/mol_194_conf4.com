%nproc=16
%mem=2GB
%chk=mol_194_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.49190       -1.56850        0.60810
C          0.59850       -0.20860       -0.03650
C         -0.71040        0.56180        0.16760
O         -1.76400       -0.11740       -0.47910
N         -3.02110        0.43150       -0.40230
O         -3.91820       -0.13240       -1.06060
O         -3.29490        1.53860        0.35130
O          1.63640        0.49670        0.62300
N          2.72960        0.88800       -0.14610
O          2.82220        2.09750       -0.45280
O          3.63320       -0.05200       -0.52240
H         -0.27760       -1.57640        1.42710
H          1.45520       -1.83110        1.10330
H          0.24140       -2.37100       -0.08710
H          0.79700       -0.36210       -1.10960
H         -0.53690        1.57300       -0.25800
H         -0.88240        0.63230        1.26860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

