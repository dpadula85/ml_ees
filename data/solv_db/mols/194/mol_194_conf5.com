%nproc=16
%mem=2GB
%chk=mol_194_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24860       -1.42860        0.29200
C         -0.19360       -0.30710        0.40540
C          1.10810       -0.79730       -0.26430
O          1.66390        0.33320       -0.89730
N          2.60810        1.16020       -0.48130
O          2.81600        2.17810       -1.21300
O          3.00160        1.29420        0.83460
O         -0.73370        0.75740       -0.32080
N         -1.91920        1.36230        0.00630
O         -2.08760        2.24270        0.84360
O         -2.97700        1.19200       -0.91230
H         -2.03490       -1.11260       -0.42590
H         -1.65120       -1.65890        1.28050
H         -0.80270       -2.34050       -0.10850
H         -0.07580       -0.05580        1.45590
H          1.71370       -1.26520        0.50530
H          0.81300       -1.55430       -1.00030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

