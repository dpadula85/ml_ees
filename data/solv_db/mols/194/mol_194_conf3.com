%nproc=16
%mem=2GB
%chk=mol_194_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.02440       -1.47890       -0.01510
C         -0.62770       -0.02650        0.03700
C          0.77590        0.20970       -0.47850
O          1.70450       -0.63530        0.18360
N          3.02650       -0.52200       -0.19240
O          3.77910       -1.50620       -0.05950
O          3.53930        0.63370       -0.70650
O         -1.51820        0.81990       -0.63410
N         -2.21220        1.83550       -0.04350
O         -1.71510        2.92530        0.29660
O         -3.55200        1.65100        0.19590
H         -0.51620       -2.01870       -0.84910
H         -0.83710       -1.99100        0.92850
H         -2.11820       -1.50040       -0.26960
H         -0.60910        0.29720        1.11900
H          1.04510        1.24960       -0.20950
H          0.85970        0.05710       -1.56230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_194_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

