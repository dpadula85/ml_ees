%nproc=16
%mem=2GB
%chk=mol_061_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48110        3.24950        0.66080
C         -2.43940        2.16820        0.39570
O         -3.07890        1.19800       -0.36740
P         -2.31920       -0.30910       -0.26020
S         -1.67120       -0.67680       -1.81190
O         -3.45920       -1.43280        0.25150
C         -3.27210       -2.68980       -0.29310
C         -4.50780       -3.08210       -1.06220
S         -0.68170       -0.18850        1.08510
C          0.81540       -0.90990        0.33360
S          1.65970        0.41670       -0.58790
C          3.43050        0.14890       -0.46530
C          4.01320       -0.06940        0.77400
C          5.27200        0.42320        1.08850
C          6.04110        0.93720        0.07280
C          5.60930        0.77740       -1.23730
C          4.28230        0.51380       -1.49770
Cl         7.33980        2.06340        0.45020
H         -3.16970        3.76280        1.59480
H         -3.47370        4.00640       -0.13920
H         -4.49220        2.79810        0.73340
H         -1.65570        2.65290       -0.22470
H         -2.00530        1.76450        1.31980
H         -3.18190       -3.42030        0.55000
H         -2.34570       -2.76950       -0.88540
H         -4.34190       -3.08710       -2.16180
H         -5.36030       -2.39490       -0.84090
H         -4.87920       -4.10110       -0.78540
H          0.54020       -1.67440       -0.43660
H          1.49250       -1.32830        1.09790
H          3.43390       -0.56240        1.52880
H          5.72410        0.27480        2.05990
H          6.31040        0.94110       -2.03470
H          3.85190        0.59960       -2.47540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_061_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_061_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_061_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

