%nproc=16
%mem=2GB
%chk=mol_192_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97160       -0.60460       -0.09560
C         -0.49400       -0.43680       -0.08520
C          0.30910       -1.52560       -0.29370
C          1.67780       -1.30880       -0.27280
C          2.21380       -0.05560       -0.05240
C          1.39370        1.03540        0.15680
C          0.00880        0.82800        0.13780
N         -0.89010        1.90500        0.34550
H         -2.50740        0.35850       -0.18930
H         -2.32450       -1.32380       -0.83660
H         -2.23650       -1.01230        0.92020
H         -0.12640       -2.50060       -0.46500
H          2.36710       -2.12610       -0.43040
H          3.26860        0.10900       -0.03660
H          1.76430        2.03290        0.33290
H         -0.89480        2.41100        1.25340
H         -1.55800        2.21460       -0.38900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

