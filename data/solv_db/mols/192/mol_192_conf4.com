%nproc=16
%mem=2GB
%chk=mol_192_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.94630       -0.63730        0.27550
C         -0.49670       -0.40780        0.17810
C          0.37280       -1.37680        0.62210
C          1.74750       -1.16240        0.53110
C          2.21050        0.02160       -0.00440
C          1.31880        1.00320       -0.45420
C         -0.03930        0.77480       -0.35670
N         -0.98280        1.72480       -0.79230
H         -2.20410       -0.75790        1.34510
H         -2.56100        0.16270       -0.12630
H         -2.18240       -1.55770       -0.31700
H          0.03390       -2.31640        1.04710
H          2.43120       -1.92300        0.87970
H          3.26190        0.24050       -0.09890
H          1.66780        1.93270       -0.87470
H         -1.79040        1.55950       -1.39840
H         -0.84150        2.71960       -0.45570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

