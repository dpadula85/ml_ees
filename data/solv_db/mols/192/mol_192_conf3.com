%nproc=16
%mem=2GB
%chk=mol_192_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72740        1.18100        0.17140
C         -0.37800        0.57950        0.06980
C          0.78350        1.32910        0.10970
C          2.02050        0.72200        0.01110
C          2.16620       -0.64010       -0.13020
C          1.00630       -1.38670       -0.16990
C         -0.23970       -0.78720       -0.07170
N         -1.44830       -1.56840       -0.11350
H         -2.17320        1.40870       -0.82270
H         -1.63060        2.14760        0.70230
H         -2.40670        0.52980        0.79040
H          0.68650        2.42020        0.22240
H          2.92400        1.31800        0.04340
H          3.14700       -1.11400       -0.20760
H          1.05020       -2.47740       -0.28080
H         -1.71430       -2.24660        0.60990
H         -2.06610       -1.41560       -0.93400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

