%nproc=16
%mem=2GB
%chk=mol_192_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92790       -0.78090       -0.36930
C         -0.47690       -0.47480       -0.13640
C          0.48460       -1.46030       -0.16420
C          1.82050       -1.14050        0.05700
C          2.22290        0.15730        0.30850
C          1.25950        1.15620        0.33820
C         -0.07060        0.81480        0.11430
N         -1.10010        1.81660        0.13630
H         -2.40150       -0.98300        0.63110
H         -2.45220        0.06860       -0.82620
H         -1.98500       -1.69660       -0.96700
H          0.21700       -2.48540       -0.35680
H          2.53770       -1.94990        0.02470
H          3.25990        0.41710        0.48210
H          1.50140        2.19820        0.52980
H         -1.22570        2.33350       -0.78350
H         -1.66370        2.00900        0.98140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

