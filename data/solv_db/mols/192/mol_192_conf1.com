%nproc=16
%mem=2GB
%chk=mol_192_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.00310       -0.45300        0.17780
C         -0.52310       -0.39350        0.08390
C          0.22300       -1.56110        0.20080
C          1.59510       -1.45140        0.10630
C          2.18680       -0.22100       -0.09680
C          1.43730        0.95290       -0.21440
C          0.06940        0.83000       -0.11830
N         -0.76150        1.97570       -0.22720
H         -2.48830       -0.38030       -0.83770
H         -2.39860        0.36630        0.82630
H         -2.32900       -1.46170        0.55740
H         -0.28270       -2.50350        0.35940
H          2.23370       -2.32670        0.18960
H          3.25850       -0.09900       -0.17560
H          1.90550        1.91760       -0.37390
H         -0.90280        2.53200        0.66160
H         -1.22030        2.27680       -1.11920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_192_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

