%nproc=16
%mem=2GB
%chk=mol_015_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.90600        0.28160        0.11720
C         -1.70450       -0.38190       -0.55590
C         -0.47510        0.19560        0.09420
C          0.80360       -0.36590       -0.46800
C          1.93780        0.32580        0.30100
N          3.22410       -0.10730       -0.12440
O          4.03160        0.71530       -0.58540
O          3.63200       -1.42010       -0.04790
H         -3.79590       -0.36580       -0.07300
H         -2.67710        0.35470        1.21130
H         -3.01410        1.27590       -0.33070
H         -1.76440       -1.46760       -0.30540
H         -1.71140       -0.21510       -1.63710
H         -0.45550        1.28820        0.03390
H         -0.50990       -0.11250        1.17180
H          0.96220       -0.12110       -1.52350
H          0.83790       -1.46460       -0.32310
H          1.76430        0.18060        1.39660
H          1.82040        1.40440        0.08130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

