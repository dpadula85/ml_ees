%nproc=16
%mem=2GB
%chk=mol_015_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.76060        0.89620        0.63040
C         -1.74660       -0.31850       -0.22240
C         -0.59860       -1.24130       -0.08970
C          0.75160       -0.72770       -0.42280
C          1.28320        0.42080        0.34850
N          2.64600        0.67180       -0.18160
O          3.64850        0.60480        0.54700
O          2.80680        0.98380       -1.50010
H         -1.56990        1.80260        0.02420
H         -1.07600        0.87750        1.50270
H         -2.81110        1.06210        1.03730
H         -1.78120        0.01850       -1.30170
H         -2.70540       -0.90340       -0.04890
H         -0.78820       -2.07850       -0.83920
H         -0.59990       -1.75550        0.92080
H          1.49130       -1.58650       -0.40110
H          0.71580       -0.41670       -1.50860
H          1.33850        0.30380        1.42580
H          0.75560        1.38610        0.07960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

