%nproc=16
%mem=2GB
%chk=mol_015_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17550        0.73490        0.24570
C         -1.69890       -0.59080       -0.26000
C         -0.22190       -0.60520       -0.59200
C          0.54290       -0.27710        0.64660
C          2.04040       -0.26370        0.42830
N          2.33770        0.73220       -0.56980
O          2.89550        0.38070       -1.61540
O          1.99140        2.02880       -0.32280
H         -1.43690        1.53290        0.22840
H         -3.05640        1.10860       -0.35180
H         -2.53200        0.60760        1.29110
H         -1.94580       -1.43700        0.43530
H         -2.27990       -0.83130       -1.19850
H          0.02280       -1.64090       -0.90280
H         -0.05670        0.13080       -1.40420
H          0.34570       -1.10590        1.38260
H          0.22920        0.69910        1.05940
H          2.43410       -1.25080        0.14380
H          2.56420        0.04700        1.35620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

