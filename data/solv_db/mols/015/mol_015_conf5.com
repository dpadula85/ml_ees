%nproc=16
%mem=2GB
%chk=mol_015_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.60460        0.70430        0.17580
C         -1.60590       -0.45160        0.08370
C         -0.29020        0.13840       -0.36920
C          0.75780       -0.96270       -0.48350
C          2.07400       -0.32600       -0.94370
N          2.53280        0.66360       -0.01250
O          2.68660        1.84120       -0.36740
O          2.81040        0.33370        1.28340
H         -2.16250        1.56420        0.72380
H         -2.83640        0.99770       -0.86790
H         -3.51990        0.38380        0.73250
H         -1.96140       -1.15700       -0.69040
H         -1.51510       -0.99120        1.03900
H          0.00070        0.90340        0.35430
H         -0.43030        0.53160       -1.39700
H          0.45220       -1.63840       -1.31570
H          0.87610       -1.52080        0.45060
H          2.85400       -1.10180       -1.07830
H          1.88180        0.08770       -1.95660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

