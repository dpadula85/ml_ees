%nproc=16
%mem=2GB
%chk=mol_015_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.09670        0.23370        0.55220
C         -1.38710       -0.90540       -0.11710
C         -0.45080       -0.45300       -1.19830
C          0.61750        0.46550       -0.66220
C          1.47030       -0.19400        0.40170
N          2.45610        0.81670        0.81750
O          2.53050        1.22330        1.99310
O          3.29380        1.29620       -0.14300
H         -1.51320        0.68440        1.37210
H         -2.42940        1.02090       -0.14700
H         -3.00770       -0.21060        1.05020
H         -0.81470       -1.45850        0.65440
H         -2.11610       -1.66900       -0.51680
H          0.08770       -1.36560       -1.57600
H         -0.93570        0.02000       -2.04630
H          0.10000        1.34790       -0.23700
H          1.29650        0.76260       -1.48420
H          2.01430       -1.07400        0.01240
H          0.88450       -0.54100        1.27440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_015_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

