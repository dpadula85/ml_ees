%nproc=16
%mem=2GB
%chk=mol_501_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.08790       -0.00070       -0.01880
F         -0.52760       -0.97890       -0.89790
F         -0.52690        1.24670       -0.42210
F         -0.69660       -0.27190        1.19680
Br         1.83910        0.00490        0.14200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_501_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_501_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_501_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

