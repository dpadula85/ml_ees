%nproc=16
%mem=2GB
%chk=mol_501_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.11390       -0.01870        0.01450
F         -0.58500       -0.51520        1.20950
F         -0.49630        1.29620       -0.09740
F         -0.63290       -0.70200       -1.07590
Br         1.82820       -0.06030       -0.05060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_501_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_501_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_501_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

