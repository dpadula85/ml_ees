%nproc=16
%mem=2GB
%chk=mol_565_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.53700        1.23470        0.11980
C         -0.21340        0.54270        0.03890
C          0.97290        1.27060        0.03920
C          2.17150        0.63640       -0.03460
C          2.22460       -0.73110       -0.11030
N          1.08390       -1.43740       -0.11100
C         -0.10540       -0.82700       -0.03880
C         -1.32480       -1.63610       -0.04280
H         -2.10170        0.78020        0.96340
H         -2.10760        1.10600       -0.83310
H         -1.34030        2.31530        0.28520
H          0.87810        2.34820        0.10070
H          3.10670        1.20110       -0.03500
H          3.20280       -1.22100       -0.16910
H         -1.68570       -1.69680       -1.10150
H         -1.09400       -2.65290        0.32100
H         -2.13060       -1.23290        0.60800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

