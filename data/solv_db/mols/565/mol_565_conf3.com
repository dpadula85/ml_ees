%nproc=16
%mem=2GB
%chk=mol_565_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.62000       -1.00570       -0.22440
C          0.21860       -0.51600       -0.09350
C         -0.84470       -1.38180       -0.19030
C         -2.12110       -0.88500       -0.06310
C         -2.30070        0.47710        0.15940
N         -1.23490        1.29290        0.24810
C          0.02910        0.83300        0.12730
C          1.20500        1.76930        0.23070
H          1.95710       -1.31000        0.80630
H          2.31050       -0.22920       -0.55280
H          1.61350       -1.90220       -0.86670
H         -0.63300       -2.44240       -0.36620
H         -2.97060       -1.54820       -0.13600
H         -3.32270        0.82320        0.25300
H          0.79920        2.79700        0.34190
H          1.87310        1.48090        1.05350
H          1.80170        1.74710       -0.72720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

