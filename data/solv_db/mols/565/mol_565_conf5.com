%nproc=16
%mem=2GB
%chk=mol_565_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.57580       -1.00290       -0.40220
C         -0.20020       -0.48470       -0.16890
C          0.89210       -1.31370       -0.39830
C          2.13980       -0.77710       -0.16480
C          2.28850        0.53330        0.27950
N          1.19330        1.27360        0.47930
C         -0.05480        0.81520        0.27170
C         -1.27340        1.63840        0.49310
H         -2.20350       -0.82670        0.50660
H         -2.03660       -0.58470       -1.32180
H         -1.52520       -2.11180       -0.57680
H          0.75650       -2.32450       -0.74210
H          3.05050       -1.36770       -0.32260
H          3.28780        0.91900        0.45150
H         -1.96150        1.60720       -0.39120
H         -1.85890        1.31210        1.37210
H         -0.91870        2.69510        0.63500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

