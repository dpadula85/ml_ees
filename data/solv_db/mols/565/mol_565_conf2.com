%nproc=16
%mem=2GB
%chk=mol_565_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45700        1.24320        0.17260
C         -0.13150        0.55170        0.06220
C          1.05290        1.27390        0.11030
C          2.22910        0.55930        0.00110
C          2.16970       -0.81580       -0.14800
N          0.99510       -1.47760       -0.18970
C         -0.16930       -0.81500       -0.08660
C         -1.46650       -1.54900       -0.13340
H         -1.98630        1.12030       -0.81610
H         -2.05050        0.82170        1.00540
H         -1.26270        2.32860        0.27450
H          1.04160        2.35780        0.22940
H          3.18430        1.07100        0.03210
H          3.11140       -1.33560       -0.22970
H         -2.29800       -0.93380       -0.48610
H         -1.32500       -2.49180       -0.72130
H         -1.63750       -1.90890        0.92330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

