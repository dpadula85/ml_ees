%nproc=16
%mem=2GB
%chk=mol_565_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15600        1.48530       -0.33530
C         -0.03710        0.55180       -0.08950
C          1.24770        1.03860       -0.04750
C          2.25320        0.11120        0.18730
C          1.97060       -1.22280        0.36740
N          0.69290       -1.64100        0.31550
C         -0.31180       -0.77990        0.09100
C         -1.72020       -1.22950        0.03220
H         -0.93530        2.40180        0.28620
H         -2.12950        1.11480       -0.00140
H         -1.20650        1.76280       -1.41860
H          1.46120        2.07350       -0.18750
H          3.30520        0.40860        0.23730
H          2.80260       -1.91940        0.55090
H         -2.25010       -1.05390        0.98950
H         -2.30140       -0.76860       -0.77030
H         -1.68540       -2.33310       -0.20710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_565_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

