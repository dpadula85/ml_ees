%nproc=16
%mem=2GB
%chk=mol_345_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45330        1.25980       -0.84990
C         -1.26250        0.09960        0.09960
C         -1.93540       -1.09720       -0.60380
C         -2.01120        0.36030        1.35580
C          0.14360       -0.26800        0.26400
C          0.75770       -0.94030       -0.80430
C          2.13740       -0.92810       -0.93110
C          2.91550       -0.24570       -0.06120
C          2.35540        0.41470        0.98730
C          0.97420        0.37140        1.14170
O          4.32260       -0.17730       -0.21100
H         -0.87690        2.12720       -0.54110
H         -1.27950        0.97880       -1.90580
H         -2.54420        1.53080       -0.79600
H         -1.80960       -1.04240       -1.67040
H         -3.03640       -0.98960       -0.40300
H         -1.53320       -2.05290       -0.14380
H         -2.92470       -0.28660        1.46630
H         -1.44200        0.25270        2.27850
H         -2.41730        1.41390        1.35980
H          0.21600       -1.42300       -1.58170
H          2.61980       -1.67140       -1.59880
H          2.97270        0.93010        1.73910
H          0.51910        0.76410        2.05070
H          4.59230        0.61910       -0.80460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

