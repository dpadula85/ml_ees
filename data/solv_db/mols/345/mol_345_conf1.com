%nproc=16
%mem=2GB
%chk=mol_345_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.12650       -0.85020        0.75540
C         -1.32160        0.10740       -0.05200
C         -1.99370        1.44280       -0.12680
C         -1.25540       -0.47950       -1.47280
C          0.10810        0.24090        0.34420
C          0.69680       -0.66970        1.23170
C          2.06700       -0.84750        1.28550
C          2.87540       -0.11150        0.48840
C          2.30240        0.83000       -0.34670
C          0.94970        1.00770       -0.43210
O          4.27210       -0.38970        0.40900
H         -3.09620       -0.99090        0.24850
H         -2.33600       -0.54090        1.79020
H         -1.61670       -1.82790        0.82720
H         -2.45270        1.77620        0.83230
H         -2.84300        1.35700       -0.85350
H         -1.35820        2.24050       -0.49340
H         -1.02310       -1.54070       -1.33960
H         -2.14510       -0.21240       -2.00650
H         -0.36180        0.00140       -1.99160
H          0.02620       -1.13870        1.90960
H          2.51340       -1.57720        1.99450
H          3.00720        1.46120       -0.92170
H          0.59580        1.80110       -1.07690
H          4.51600       -1.08950       -0.34600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

