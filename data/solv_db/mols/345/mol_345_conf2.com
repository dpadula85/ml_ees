%nproc=16
%mem=2GB
%chk=mol_345_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.08060        0.69520        1.61510
C         -1.29730        0.09910        0.20810
C         -2.20480        1.03590       -0.53070
C         -1.97880       -1.21910        0.40030
C          0.04020       -0.05530       -0.41900
C          0.83560        1.03410       -0.66500
C          2.20770        0.94460       -0.76810
C          2.83240       -0.27620       -0.62860
C          2.04730       -1.38190       -0.40820
C          0.66000       -1.28060       -0.31220
O          4.23580       -0.36560       -0.69880
H         -1.66440        0.14550        2.36130
H          0.01390        0.63070        1.83690
H         -1.30630        1.78580        1.59690
H         -1.71040        2.02760       -0.68660
H         -2.56070        0.57460       -1.47000
H         -3.11790        1.16030        0.12080
H         -3.01820       -1.00860        0.74800
H         -1.97100       -1.85860       -0.49790
H         -1.47490       -1.70370        1.28540
H          0.32620        1.99380       -0.81320
H          2.80470        1.81770       -0.96040
H          2.52120       -2.34420       -0.24490
H          0.06330       -2.18070       -0.18160
H          4.79690       -0.27030        0.14320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

