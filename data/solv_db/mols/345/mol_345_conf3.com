%nproc=16
%mem=2GB
%chk=mol_345_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.20060       -0.45110       -0.81790
C         -1.28720       -0.14430        0.32750
C         -1.68880        1.21250        0.86930
C         -1.53420       -1.18630        1.41440
C          0.13980       -0.16170        0.00200
C          0.70120       -0.52760       -1.20780
C          2.00300       -0.34890       -1.50590
C          2.86300        0.26060       -0.59240
C          2.30440        0.69660        0.60480
C          1.00010        0.46350        0.89730
O          4.21200        0.46730       -0.86190
H         -2.41550        0.44090       -1.47010
H         -3.20450       -0.68530       -0.37000
H         -1.88590       -1.28740       -1.45140
H         -2.41410        1.09080        1.70970
H         -2.16680        1.86510        0.11460
H         -0.79530        1.67950        1.31380
H         -0.58970       -1.28480        2.01430
H         -2.36350       -0.84970        2.04310
H         -1.68970       -2.17240        0.93610
H          0.05000       -0.94160       -1.95740
H          2.48840       -0.79630       -2.36040
H          2.96270        1.11680        1.35540
H          0.73410        0.59620        1.95490
H          4.77690        0.94750       -0.17690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

