%nproc=16
%mem=2GB
%chk=mol_345_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72920        1.48910       -0.21590
C         -1.31820        0.01750       -0.07420
C         -1.79860       -0.70900       -1.32680
C         -1.98270       -0.53860        1.12070
C          0.17890       -0.05300       -0.09750
C          0.87640        1.01830       -0.57650
C          2.27010        1.01250       -0.46590
C          2.89280       -0.06440        0.06590
C          2.22700       -1.18690        0.53230
C          0.84330       -1.14360        0.44510
O          4.29720       -0.01080        0.16160
H         -1.01290        2.05550        0.44160
H         -2.73620        1.62380        0.24310
H         -1.67480        1.78830       -1.26220
H         -1.77630       -1.81850       -1.19060
H         -1.13080       -0.40160       -2.18080
H         -2.86850       -0.44440       -1.48860
H         -1.40250       -1.30460        1.63840
H         -2.98080       -0.90680        0.80720
H         -2.25080        0.29620        1.84760
H          0.36260        1.83770       -1.12030
H          2.80420        1.88250       -0.81700
H          2.79220       -2.05240        0.83660
H          0.30420       -2.02000        0.83430
H          4.81350       -0.36690       -0.64270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_345_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

