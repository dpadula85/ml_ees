%nproc=16
%mem=2GB
%chk=mol_461_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.38530        0.65410       -0.45680
C         -1.23340       -0.29470       -0.18920
C         -0.41070        0.27750        0.94500
C          0.76440       -0.60080        1.28550
C          1.70180       -0.77870        0.11110
N          2.19520        0.54570       -0.27930
H         -3.35550        0.13240       -0.33410
H         -2.37700        1.01780       -1.51940
H         -2.38350        1.51210        0.24160
H         -0.60030       -0.33670       -1.11120
H         -1.58170       -1.30900        0.10720
H         -0.01940        1.25810        0.64610
H         -1.04710        0.39240        1.84320
H          1.33610       -0.09210        2.08760
H          0.39660       -1.54760        1.67860
H          2.59690       -1.33980        0.44490
H          1.20700       -1.21910       -0.77170
H          3.18230        0.50300       -0.60130
H          2.01350        1.22550        0.47570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

