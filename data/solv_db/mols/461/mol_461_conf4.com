%nproc=16
%mem=2GB
%chk=mol_461_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.92970       -0.18050       -0.26240
C         -1.64200        0.55150       -0.02750
C         -0.48460       -0.44590       -0.16930
C          0.78070        0.35420        0.07870
C          2.01900       -0.50790       -0.02940
N          3.15830        0.36150        0.22570
H         -2.76200       -1.24980       -0.03350
H         -3.20700       -0.08620       -1.33690
H         -3.70640        0.24110        0.40550
H         -1.50760        1.37650       -0.73440
H         -1.60410        0.93330        1.01210
H         -0.45240       -0.80180       -1.21960
H         -0.59160       -1.26060        0.56290
H          0.81660        1.20370       -0.61910
H          0.77890        0.78040        1.11070
H          2.05760       -0.95820       -1.04060
H          2.00890       -1.28000        0.73790
H          4.02660       -0.14330        0.42450
H          3.24080        1.11200       -0.49630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

