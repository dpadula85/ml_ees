%nproc=16
%mem=2GB
%chk=mol_461_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.78820        0.45280        0.30410
C         -1.64760       -0.49570        0.14410
C         -0.34630        0.23410       -0.10280
C          0.78170       -0.76030       -0.25900
C          2.10180       -0.10500       -0.50650
N          2.49790        0.78310        0.57230
H         -3.46470        0.43030       -0.59690
H         -2.38460        1.47430        0.42100
H         -3.46350        0.17740        1.15880
H         -1.77650       -1.23000       -0.67470
H         -1.54120       -1.05290        1.11160
H         -0.13750        0.82740        0.83050
H         -0.46600        0.86270       -0.98330
H          0.86780       -1.38370        0.64730
H          0.54900       -1.41910       -1.11790
H          2.86910       -0.90540       -0.55480
H          2.15050        0.42960       -1.47840
H          3.01250        0.22760        1.29230
H          3.18590        1.45310        0.16010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

