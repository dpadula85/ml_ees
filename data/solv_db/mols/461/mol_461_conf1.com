%nproc=16
%mem=2GB
%chk=mol_461_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.90960       -0.12070        0.15630
C         -1.55910        0.50660        0.16170
C         -0.43360       -0.53060        0.05080
C          0.85490        0.25920        0.07010
C          2.07620       -0.61990       -0.03200
N          3.23610        0.25150       -0.00280
H         -2.89130       -1.18570       -0.10810
H         -3.54200        0.46250       -0.57380
H         -3.40790        0.05630        1.14600
H         -1.38550        1.25730       -0.61180
H         -1.36900        1.01900        1.14780
H         -0.57560       -1.09430       -0.89290
H         -0.47740       -1.24840        0.90070
H          0.86500        0.95950       -0.76720
H          0.88590        0.78910        1.04900
H          2.14010       -1.25180        0.89460
H          2.11550       -1.19250       -0.95670
H          3.38140        0.58120       -0.98830
H          2.99580        1.10160        0.54730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

