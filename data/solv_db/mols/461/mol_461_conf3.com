%nproc=16
%mem=2GB
%chk=mol_461_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.59700        0.70360       -0.37720
C          1.62700       -0.31730        0.17700
C          0.20110        0.04960       -0.18020
C         -0.79050       -0.94050        0.35290
C         -2.17190       -0.49450       -0.05000
N         -2.52000        0.79540        0.45330
H          3.25160        1.13550        0.40770
H          3.26210        0.24440       -1.15220
H          2.01780        1.52270       -0.85310
H          1.91310       -1.29520       -0.20780
H          1.69520       -0.24800        1.29510
H         -0.06080        1.05720        0.15230
H          0.14300        0.03390       -1.28930
H         -0.60800       -1.93820       -0.09960
H         -0.75070       -1.07020        1.43860
H         -2.28980       -0.54620       -1.13850
H         -2.91090       -1.23750        0.37070
H         -2.21750        0.96690        1.41810
H         -2.38770        1.57850       -0.20440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_461_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

