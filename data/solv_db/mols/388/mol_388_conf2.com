%nproc=16
%mem=2GB
%chk=mol_388_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.63690       -0.07410        0.00660
C         -0.66330        0.09150       -0.01450
H          1.35950        0.62030       -0.39980
H          1.04490       -0.96400        0.45870
H         -1.30630       -0.64730        0.41150
H         -1.07170        0.97360       -0.46250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_388_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_388_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_388_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

