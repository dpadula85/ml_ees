%nproc=16
%mem=2GB
%chk=mol_388_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.65030        0.08450       -0.03100
C          0.65770       -0.06620        0.03030
H         -1.29610       -0.78240       -0.00820
H         -1.12180        1.06610       -0.10540
H          1.05950       -1.06270        0.10260
H          1.35100        0.76060        0.01180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_388_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_388_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_388_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

