%nproc=16
%mem=2GB
%chk=mol_548_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24330        0.50770       -0.20590
C         -1.12120       -0.51080       -0.17110
C          0.15330        0.24160        0.11680
C          1.35850       -0.67810        0.17720
C          2.53860        0.18530        0.46530
O          2.46580        1.39350        0.61140
H         -2.19140        1.07390        0.74330
H         -2.11580        1.20980       -1.03100
H         -3.22500       -0.01190       -0.25770
H         -1.00180       -0.96830       -1.19080
H         -1.32110       -1.32250        0.52440
H          0.36180        0.96220       -0.70010
H          0.11480        0.78490        1.07500
H          1.48130       -1.29040       -0.71970
H          1.21780       -1.32850        1.07840
H          3.52770       -0.24840        0.55630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

