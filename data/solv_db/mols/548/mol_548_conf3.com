%nproc=16
%mem=2GB
%chk=mol_548_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92510        0.08830        0.35460
C         -1.07930       -0.37880       -0.78180
C          0.30780        0.20150       -0.78230
C          0.99460       -0.21270        0.52660
C          2.37290        0.32930        0.59070
O          2.81150        1.00010       -0.31920
H         -2.99950        0.24410        0.07180
H         -1.85980       -0.66700        1.16920
H         -1.57060        1.04290        0.80320
H         -1.03730       -1.47580       -0.76790
H         -1.59770       -0.09470       -1.73060
H          0.28600        1.28350       -0.90260
H          0.90920       -0.26290       -1.58950
H          0.35120        0.10720        1.35640
H          1.07690       -1.31700        0.54710
H          2.95920        0.11190        1.45430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

