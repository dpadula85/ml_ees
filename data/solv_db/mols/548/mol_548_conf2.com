%nproc=16
%mem=2GB
%chk=mol_548_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.98510        0.21360        0.31960
C          1.04560       -0.92840        0.06000
C         -0.32660       -0.46090       -0.35160
C         -0.99450        0.40670        0.70390
C         -2.33750        0.75990        0.10980
O         -3.23010       -0.04680        0.08020
H          2.92450       -0.22070        0.73240
H          2.23460        0.70680       -0.65220
H          1.55960        1.01830        0.95230
H          1.01840       -1.59310        0.93890
H          1.45720       -1.50880       -0.78990
H         -0.99510       -1.32760       -0.52010
H         -0.23470        0.12550       -1.30020
H         -1.18390       -0.22520        1.61790
H         -0.45240        1.32300        0.93230
H         -2.47010        1.75770       -0.28810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

