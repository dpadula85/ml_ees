%nproc=16
%mem=2GB
%chk=mol_548_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.06690        0.00020        0.26120
C         -1.02500       -0.54830       -0.66700
C          0.35130       -0.60830       -0.07620
C          1.00260        0.74290        0.07130
C          2.45890        0.52780        0.34820
O          2.94670       -0.59160        0.43790
H         -2.59950        0.88120       -0.17510
H         -1.68820        0.29010        1.25120
H         -2.86740       -0.75880        0.45590
H         -1.30470       -1.59740       -0.93900
H         -1.05400        0.02750       -1.60470
H          0.32570       -1.09480        0.90860
H          0.97810       -1.23140       -0.75770
H          0.87080        1.38030       -0.83870
H          0.51450        1.23020        0.95120
H          3.15700        1.35040        0.23480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

