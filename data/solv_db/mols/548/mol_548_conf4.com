%nproc=16
%mem=2GB
%chk=mol_548_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96420        0.37080       -0.25580
C          1.12900       -0.66950        0.40070
C         -0.28040       -0.27790        0.71040
C         -1.05490        0.09930       -0.54620
C         -2.44800        0.48490       -0.22460
O         -3.30500       -0.34500       -0.01100
H          2.84620       -0.16930       -0.71500
H          1.49250        0.90450       -1.09510
H          2.42290        1.09560        0.44760
H          1.09080       -1.58840       -0.22860
H          1.61430       -0.93340        1.36770
H         -0.78850       -1.19230        1.10250
H         -0.35850        0.49080        1.48790
H         -1.07560       -0.75300       -1.26850
H         -0.53370        0.95590       -0.99230
H         -2.71530        1.52700       -0.17970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_548_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

