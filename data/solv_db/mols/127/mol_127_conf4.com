%nproc=16
%mem=2GB
%chk=mol_127_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08560        0.14500       -0.11660
C         -1.29200        1.27760       -0.01860
C          0.08080        1.18780        0.08280
C          0.71940       -0.03830        0.09060
C         -0.08260       -1.15980       -0.00770
C         -1.44870       -1.08510       -0.10910
C          2.19930       -0.15810        0.19960
Cl         2.91480       -0.15070       -1.40460
H         -3.16960        0.24280       -0.19570
H         -1.81300        2.24780       -0.02600
H          0.67490        2.08630        0.15780
H          0.35840       -2.15090       -0.00720
H         -2.04650       -1.98200       -0.18430
H          2.53440        0.66850        0.85070
H          2.45610       -1.13100        0.68840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

