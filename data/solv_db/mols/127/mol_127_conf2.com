%nproc=16
%mem=2GB
%chk=mol_127_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11630       -0.02470       -0.06700
C         -1.40780        1.14570       -0.24940
C         -0.02890        1.17310       -0.18380
C          0.70550        0.03340        0.06700
C         -0.00610       -1.13320        0.24850
C         -1.38300       -1.16530        0.18380
C          2.19350        0.03760        0.14250
Cl         2.93750       -0.26330       -1.44260
H         -3.19960       -0.03520       -0.12060
H         -1.96690        2.06170       -0.44920
H          0.51770        2.09090       -0.32740
H          0.57330       -2.04010        0.44790
H         -1.90010       -2.10670        0.33300
H          2.56580       -0.76490        0.82470
H          2.51550        0.99090        0.59260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

