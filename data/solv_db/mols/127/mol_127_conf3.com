%nproc=16
%mem=2GB
%chk=mol_127_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.10330        0.12850       -0.05490
C         -1.32360        1.22490       -0.36880
C          0.05230        1.11750       -0.35840
C          0.70210       -0.05880       -0.04250
C         -0.07960       -1.14590        0.26880
C         -1.45590       -1.06710        0.26640
C          2.19970       -0.14830       -0.03880
Cl         2.83710        0.26360        1.58700
H         -3.19690        0.16900       -0.05060
H         -1.79300        2.17370       -0.62470
H          0.69780        1.97170       -0.60330
H          0.39760       -2.09520        0.52420
H         -2.07700       -1.92190        0.51140
H          2.48870       -1.20080       -0.29170
H          2.65400        0.58910       -0.72410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

