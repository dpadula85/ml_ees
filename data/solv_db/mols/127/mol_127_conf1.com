%nproc=16
%mem=2GB
%chk=mol_127_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.05600       -0.02400        0.09080
C         -1.39780        1.17860        0.28580
C         -0.00920        1.17400        0.19360
C          0.69280        0.02120       -0.08100
C          0.03400       -1.17330       -0.27430
C         -1.35940       -1.18700       -0.18540
C          2.16960        0.02440       -0.17750
Cl         2.92990       -0.29500        1.39950
H         -3.14060       -0.02620        0.16180
H         -1.96930        2.06400        0.49870
H          0.49250        2.13380        0.35080
H          0.53820       -2.10470       -0.49210
H         -1.91020       -2.10760       -0.33140
H          2.53200        1.03890       -0.49490
H          2.45340       -0.71720       -0.94430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_127_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

