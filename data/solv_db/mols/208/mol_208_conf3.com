%nproc=16
%mem=2GB
%chk=mol_208_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.75270       -0.05780       -0.10430
C          0.72890        0.08820        0.18350
F          1.36400       -0.93110       -0.52710
F          1.17610        1.30620       -0.26690
H         -1.36080        0.03310        0.82410
H         -1.02030        0.68850       -0.87410
H         -0.99890       -1.07540       -0.51020
H          0.86380       -0.05170        1.27500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

