%nproc=16
%mem=2GB
%chk=mol_208_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.77180       -0.04440       -0.02900
C         -0.75310        0.02670       -0.03770
F         -1.16660       -0.14330        1.26240
F         -1.11640        1.23290       -0.55940
H          1.17100       -0.28150       -1.04340
H          1.01440       -0.87700        0.68620
H          1.18620        0.91860        0.35750
H         -1.10730       -0.83200       -0.63660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

