%nproc=16
%mem=2GB
%chk=mol_208_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.75630        0.11010        0.10700
C         -0.72280       -0.19810       -0.06380
F         -1.36940       -0.39520        1.11390
F         -1.26660        0.95040       -0.64440
H          1.34910       -0.80240        0.32280
H          1.09580        0.48550       -0.89700
H          0.94760        0.90920        0.83110
H         -0.79000       -1.05940       -0.76960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

