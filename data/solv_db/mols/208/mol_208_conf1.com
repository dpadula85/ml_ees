%nproc=16
%mem=2GB
%chk=mol_208_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77210        0.00360       -0.03560
C          0.72590       -0.04160        0.06670
F          1.24220       -0.95800       -0.83620
F          1.31590        1.18500       -0.15720
H         -1.09400        1.07220       -0.16820
H         -1.24290       -0.30640        0.93510
H         -1.17930       -0.55260       -0.89630
H          1.00430       -0.40220        1.09170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_208_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

