%nproc=16
%mem=2GB
%chk=mol_138_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.18480        0.68230        0.61750
C         -3.17780       -0.12910       -0.16600
C         -1.76640        0.38600        0.03450
C         -0.85210       -0.48590       -0.78310
C          0.59560       -0.08140       -0.67480
C          1.12490       -0.14130        0.71210
O          2.47110        0.23940        0.82830
C          3.53300       -0.37030        0.23460
O          3.35940       -1.37620       -0.49730
C          4.93410        0.12480        0.42570
H         -4.52300        1.50520       -0.03470
H         -3.74900        1.01640        1.56700
H         -5.08260        0.07420        0.86270
H         -3.44170       -0.20810       -1.23730
H         -3.19860       -1.17080        0.25120
H         -1.66750        1.45460       -0.17330
H         -1.52020        0.16620        1.11470
H         -1.00220       -1.53300       -0.45860
H         -1.16000       -0.38600       -1.84510
H          0.71270        0.92520       -1.09970
H          1.16180       -0.78920       -1.31000
H          1.07230       -1.19310        1.12490
H          0.49220        0.48800        1.37110
H          4.99490        1.20550        0.13280
H          5.23270        0.08710        1.49950
H          5.64110       -0.49060       -0.12570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

