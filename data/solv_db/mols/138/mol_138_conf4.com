%nproc=16
%mem=2GB
%chk=mol_138_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61510        0.97560       -0.48460
C         -3.48570       -0.50670       -0.33640
C         -2.27250       -0.85270        0.53800
C         -1.07020       -0.27840       -0.17310
C          0.21370       -0.54630        0.58340
C          1.33190        0.06790       -0.21590
O          2.59460       -0.12220        0.40200
C          3.70030        0.40480       -0.25010
O          3.56650        1.02010       -1.33930
C          5.08390        0.27600        0.29000
H         -3.13190        1.35120       -1.40590
H         -3.05000        1.44270        0.37120
H         -4.67980        1.32660       -0.39170
H         -4.40290       -0.99090        0.06290
H         -3.30960       -0.95550       -1.33510
H         -2.16450       -1.94400        0.64030
H         -2.39280       -0.37290        1.51140
H         -0.96930       -0.63150       -1.20110
H         -1.19280        0.82410       -0.20260
H          0.42870       -1.65450        0.62190
H          0.17390       -0.19300        1.62060
H          1.10170        1.12250       -0.39140
H          1.35470       -0.48840       -1.19800
H          5.57750        1.25890        0.14020
H          5.57680       -0.54130       -0.26370
H          5.03280        0.00760        1.37380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

