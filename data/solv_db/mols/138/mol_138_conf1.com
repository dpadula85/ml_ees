%nproc=16
%mem=2GB
%chk=mol_138_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.97600        0.01230        0.38560
C         -2.68710        0.42510       -0.31640
C         -1.67480       -0.69410       -0.26390
C         -0.38300       -0.33410       -0.94780
C          0.22170        0.85310       -0.29030
C          1.52690        1.28780       -0.90880
O          2.53230        0.31460       -0.87830
C          3.06870       -0.26100        0.25080
O          2.63450        0.10380        1.36650
C          4.13450       -1.29190        0.23460
H         -3.88820        0.26570        1.45030
H         -4.11320       -1.06180        0.19090
H         -4.78590        0.62550       -0.05230
H         -2.86030        0.67520       -1.38330
H         -2.35040        1.33600        0.19390
H         -2.11890       -1.61640       -0.65480
H         -1.42940       -0.88680        0.80440
H          0.29560       -1.20130       -0.83670
H         -0.56130       -0.20640       -2.03620
H         -0.44700        1.75760       -0.37400
H          0.32010        0.65010        0.79120
H          1.40570        1.60040       -1.96650
H          1.83530        2.23210       -0.37400
H          4.23300       -1.77210       -0.77410
H          3.95260       -2.03360        1.02570
H          5.11440       -0.77990        0.40640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

