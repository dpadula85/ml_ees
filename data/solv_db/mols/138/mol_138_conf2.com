%nproc=16
%mem=2GB
%chk=mol_138_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.75350        1.07970        0.77340
C         -3.22450       -0.07030       -0.06230
C         -1.75200       -0.20080        0.24610
C         -1.10070       -1.31010       -0.51970
C          0.36790       -1.41160       -0.18530
C          1.14280       -0.17380       -0.49720
O          2.49160       -0.41190       -0.13740
C          3.50780        0.49560       -0.27640
O          3.22550        1.62540       -0.75920
C          4.89890        0.14910        0.13280
H         -4.71800        1.41380        0.33150
H         -3.97160        0.78170        1.81660
H         -3.07210        1.94650        0.78420
H         -3.70500       -1.01590        0.30420
H         -3.43350        0.11820       -1.12230
H         -1.66790       -0.44430        1.32780
H         -1.22310        0.76080        0.06690
H         -1.59560       -2.25690       -0.22090
H         -1.26730       -1.11130       -1.60010
H          0.41080       -1.59210        0.92910
H          0.83440       -2.30100       -0.65630
H          1.02010        0.14720       -1.53200
H          0.80600        0.66510        0.15480
H          5.58770        0.98700       -0.00190
H          4.93500       -0.16560        1.20630
H          5.25650       -0.68770       -0.50280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

