%nproc=16
%mem=2GB
%chk=mol_138_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.32800        0.19350        0.56280
C         -2.17530        0.91910       -0.09000
C         -1.70560        0.14780       -1.30290
C         -1.24300       -1.23370       -0.94680
C         -0.09860       -1.14940        0.02390
C          1.09110       -0.39590       -0.53110
O          2.07570       -0.40630        0.49780
C          3.31720        0.19330        0.35680
O          3.56640        0.76150       -0.72300
C          4.30380        0.14090        1.48630
H         -3.98630        0.89260        1.13600
H         -2.96770       -0.57310        1.28260
H         -3.91430       -0.29030       -0.26560
H         -2.56450        1.91290       -0.39670
H         -1.36390        1.11720        0.62980
H         -2.62310        0.01800       -1.95000
H         -0.92970        0.68750       -1.85800
H         -0.99060       -1.77750       -1.86050
H         -2.07970       -1.73860       -0.39610
H          0.27080       -2.15510        0.32180
H         -0.39850       -0.64460        0.95990
H          1.41360       -0.91280       -1.43540
H          0.82580        0.64610       -0.82310
H          3.72570        0.36150        2.42980
H          4.68690       -0.90160        1.52280
H          5.09170        0.89900        1.36890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_138_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

