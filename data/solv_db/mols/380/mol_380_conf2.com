%nproc=16
%mem=2GB
%chk=mol_380_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.66040       -0.12300       -0.07080
C          0.62070        0.10050        0.06690
Cl         1.79940       -1.21300        0.23790
Cl         1.29450        1.73540        0.09510
Cl        -1.31490       -1.72700       -0.09780
Cl        -1.73930        1.22700       -0.23130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_380_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_380_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_380_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

