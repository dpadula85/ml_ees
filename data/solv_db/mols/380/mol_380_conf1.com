%nproc=16
%mem=2GB
%chk=mol_380_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.64820       -0.12270       -0.00390
C          0.64660        0.11740        0.00400
Cl         1.74780       -1.24140        0.04820
Cl         1.20230        1.77410       -0.02980
Cl        -1.74670        1.24250       -0.04820
Cl        -1.20180       -1.76990        0.02970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_380_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_380_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_380_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

