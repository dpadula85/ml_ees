%nproc=16
%mem=2GB
%chk=mol_055_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76970       -0.15550       -0.12610
C          0.72340       -0.21320        0.15240
Br         1.70510        1.02680       -0.94840
Br        -1.34710        1.65810        0.25960
H         -1.26060       -0.81000        0.61730
H         -1.01160       -0.41590       -1.16460
H          1.11370       -1.22940       -0.00400
H          0.84690        0.13900        1.21380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

