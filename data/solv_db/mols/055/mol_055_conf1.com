%nproc=16
%mem=2GB
%chk=mol_055_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.65670       -0.33810       -0.06890
C         -0.68890        0.34970        0.00710
Br        -2.10380       -0.94970       -0.12280
Br         2.11380        0.92870        0.05720
H          0.79700       -1.00720        0.80570
H          0.74900       -0.95460       -1.00340
H         -0.74840        0.79820        1.02860
H         -0.77550        1.17300       -0.70360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

