%nproc=16
%mem=2GB
%chk=mol_055_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.70300       -0.20820       -0.28940
C         -0.70320       -0.18930        0.28890
Br        -1.54190        1.43860       -0.36950
Br         1.57780        1.40500        0.37090
H          0.61320       -0.18310       -1.39880
H          1.30450       -1.05250        0.08290
H         -0.63260       -0.18270        1.40430
H         -1.32070       -1.02770       -0.08940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

