%nproc=16
%mem=2GB
%chk=mol_055_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.68250       -0.26400       -0.20420
C         -0.72770       -0.04570        0.29390
Br        -1.48840        1.42850       -0.69500
Br         1.77340        1.30660        0.05530
H          0.60470       -0.54760       -1.26980
H          1.12190       -1.08290        0.38900
H         -1.30860       -0.95920        0.05510
H         -0.65780        0.16430        1.37570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_055_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

