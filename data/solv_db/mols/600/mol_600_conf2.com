%nproc=16
%mem=2GB
%chk=mol_600_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.83610        0.82570        0.69230
C         -2.39730       -0.57150        1.05920
C         -0.88340       -0.63860        0.76870
C         -0.74690       -0.35130       -0.69240
C          0.68540       -0.41190       -1.10880
C          1.46650        0.61950       -0.33170
C          2.86660        0.52710       -0.80220
C          3.80180        0.23590        0.04420
H         -3.67560        1.15230        1.34520
H         -2.01410        1.56980        0.77910
H         -3.17010        0.88120       -0.38440
H         -2.85440       -1.27720        0.32360
H         -2.61580       -0.83050        2.10040
H         -0.44430       -1.58310        1.08700
H         -0.48650        0.23140        1.34700
H         -1.22900        0.62250       -0.96480
H         -1.29630       -1.10610       -1.32390
H          0.81530       -0.23770       -2.21160
H          1.12630       -1.43560       -0.94070
H          1.02870        1.61430       -0.50530
H          1.38220        0.36990        0.74890
H          3.07500        0.70280       -1.83060
H          3.57490        0.06140        1.07890
H          4.82700        0.16390       -0.27830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

