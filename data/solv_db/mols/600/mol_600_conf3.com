%nproc=16
%mem=2GB
%chk=mol_600_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.94270        0.92810        0.47520
C         -1.95920       -0.20000        0.32560
C         -0.62980        0.12200        0.99810
C          0.30710       -1.03630        0.81540
C          0.57840       -1.33770       -0.63970
C          1.20540       -0.17910       -1.37120
C          2.50660        0.19210       -0.75790
C          2.74450        1.38190       -0.25720
H         -3.96180        0.51470        0.45770
H         -2.75820        1.46700        1.42350
H         -2.88930        1.64810       -0.38190
H         -2.42000       -1.09950        0.76900
H         -1.75550       -0.36010       -0.74500
H         -0.85970        0.18020        2.09160
H         -0.24060        1.09230        0.69030
H         -0.16810       -1.93060        1.26150
H          1.26640       -0.89650        1.34020
H         -0.30010       -1.73620       -1.17180
H          1.34060       -2.16600       -0.66200
H          1.43410       -0.54030       -2.39880
H          0.52250        0.68800       -1.45460
H          3.28080       -0.55630       -0.72860
H          3.69470        1.60940        0.17550
H          2.00360        2.18540       -0.25490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

