%nproc=16
%mem=2GB
%chk=mol_600_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.97220        0.22670        0.73290
C         -2.70410        0.28390       -0.05650
C         -1.48230       -0.10180        0.78870
C         -0.28710        0.00080       -0.13370
C          0.99880       -0.35760        0.58270
C          2.11180       -0.21090       -0.46270
C          3.43430       -0.53000        0.09340
C          4.39540        0.36970        0.10350
H         -3.84570        0.78380        1.69130
H         -4.26350       -0.81910        0.88290
H         -4.74440        0.79280        0.16370
H         -2.48390        1.29240       -0.46510
H         -2.79090       -0.38860       -0.93390
H         -1.32780        0.63510        1.59610
H         -1.58920       -1.11100        1.21340
H         -0.42600       -0.71110       -0.97270
H         -0.18170        1.02510       -0.50590
H          1.14360        0.33780        1.42690
H          0.97610       -1.37600        1.00530
H          2.02970        0.81630       -0.84370
H          1.84290       -0.88370       -1.30710
H          3.59320       -1.53110        0.49550
H          4.19370        1.35020       -0.30600
H          5.37920        0.10640        0.52620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

