%nproc=16
%mem=2GB
%chk=mol_600_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.62260        0.52380       -0.29750
C         -2.22820        0.20600        0.17050
C         -1.56370       -0.84860       -0.68310
C         -0.18550       -1.11480       -0.16360
C          0.70590        0.08180       -0.16790
C          2.06130       -0.36340        0.38860
C          2.96800        0.82420        0.39150
C          4.10770        0.83800       -0.29800
H         -3.63090        1.42110       -0.94430
H         -4.20070        0.82370        0.62420
H         -4.13700       -0.34830       -0.74770
H         -1.66080        1.16330        0.16610
H         -2.26760       -0.17030        1.22280
H         -2.15670       -1.79250       -0.51820
H         -1.61610       -0.63650       -1.74740
H         -0.25960       -1.59650        0.82740
H          0.27850       -1.87720       -0.84510
H          0.36730        0.91610        0.43860
H          0.80910        0.43380       -1.20830
H          2.48280       -1.14460       -0.25460
H          1.86880       -0.71690        1.43280
H          2.70980        1.69710        0.95850
H          4.76050        1.70130       -0.29200
H          4.40960       -0.02060       -0.88480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

