%nproc=16
%mem=2GB
%chk=mol_600_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.34370        0.12340        0.08480
C         -2.36390       -0.21950        1.16660
C         -0.97060        0.29000        0.76400
C         -0.63220       -0.41230       -0.52860
C          0.71220       -0.02420       -1.06810
C          1.79630       -0.36590       -0.07980
C          3.13810        0.00380       -0.58180
C          3.91240        0.86420        0.05790
H         -3.11520        1.08240       -0.42720
H         -4.38510        0.14080        0.49960
H         -3.34570       -0.67800       -0.69580
H         -2.65010        0.19690        2.16030
H         -2.34130       -1.32160        1.28350
H         -0.29150        0.06170        1.58920
H         -1.02790        1.38880        0.62060
H         -0.62710       -1.51030       -0.37080
H         -1.44630       -0.17380       -1.24330
H          0.74160        1.05220       -1.35310
H          0.90420       -0.61730       -1.98570
H          1.62940        0.11330        0.91400
H          1.78950       -1.47640        0.03980
H          3.48790       -0.44910       -1.50170
H          4.88490        1.12380       -0.31800
H          3.54390        1.30430        0.97380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_600_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

