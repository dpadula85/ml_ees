%nproc=16
%mem=2GB
%chk=mol_256_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.67630        0.19450       -0.04830
C         -2.56660        0.97260       -0.25470
C         -1.26990        0.48790       -0.10080
C         -1.08880       -0.83040        0.27510
C         -2.22820       -1.59990        0.47860
C         -3.49210       -1.12120        0.32680
Cl        -4.92540       -2.11380        0.58970
O          0.19540       -1.35450        0.44000
C          1.28270       -0.51960        0.21730
C          2.58680       -0.99470        0.36870
C          3.66950       -0.17590        0.15050
C          3.50060        1.13530       -0.22330
C          2.23040        1.61840       -0.37650
C          1.14710        0.79890       -0.15810
O         -0.16390        1.28940       -0.31380
Cl         4.90750        2.17350       -0.49940
H         -4.68020        0.59890       -0.17570
H         -2.73440        2.00870       -0.55030
H         -2.08890       -2.62380        0.77040
H          2.68640       -2.02880        0.66300
H          4.67150       -0.56200        0.27280
H          2.03670        2.64650       -0.67050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_256_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_256_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_256_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

