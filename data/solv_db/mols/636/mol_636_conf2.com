%nproc=16
%mem=2GB
%chk=mol_636_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.10690        1.25420       -0.36110
C         -2.62940       -0.13680       -0.15540
C         -1.40700       -0.34730        0.35160
C         -0.50880        0.76020        0.71700
C          0.78410        0.83160       -0.05300
C          1.58850       -0.37410        0.13720
C          2.79330       -0.35130        0.69110
C          3.45280        0.87180        1.17770
O          4.65600        0.97780        0.42480
C          1.19840       -1.64920       -0.55770
C         -3.52830       -1.25870       -0.51870
H         -3.12310        1.73000        0.65200
H         -2.39540        1.83510       -0.99620
H         -4.09310        1.24730       -0.83530
H         -1.14110       -1.35930        0.59520
H         -0.29430        0.72340        1.82340
H         -1.09040        1.71910        0.55950
H          0.46070        0.93950       -1.13720
H          1.26450        1.78110        0.18500
H          3.34860       -1.32350        0.79960
H          3.81400        0.75760        2.23660
H          2.93600        1.81430        1.02550
H          4.95180        0.07900        0.13470
H          2.05960       -1.98780       -1.19440
H          0.39440       -1.41410       -1.26770
H          0.90810       -2.44860        0.11890
H         -4.03540       -1.60310        0.40320
H         -2.96540       -2.12470       -0.94630
H         -4.29230       -0.94330       -1.23360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

