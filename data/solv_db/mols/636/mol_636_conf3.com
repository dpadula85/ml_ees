%nproc=16
%mem=2GB
%chk=mol_636_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.88020       -0.22690        0.93370
C         -2.65970       -0.24600        0.09660
C         -1.60390       -0.91050        0.46420
C         -0.33520       -0.97490       -0.33060
C          0.76490       -0.39920        0.55770
C          2.07390       -0.40310       -0.10590
C          2.74290        0.71030       -0.32020
C          2.32880        2.07570        0.03280
O          3.26100        2.63980        0.90870
C          2.63340       -1.71560       -0.53540
C         -2.67530        0.51460       -1.17430
H         -3.79310       -0.88450        1.82910
H         -4.75940       -0.51080        0.29750
H         -4.05460        0.81780        1.30510
H         -1.60840       -1.46820        1.40690
H         -0.11920       -2.01100       -0.63700
H         -0.48580       -0.37770       -1.23440
H          0.41960        0.56280        0.94570
H          0.75800       -1.08640        1.46190
H          3.71680        0.61060       -0.81810
H          1.29510        2.22470        0.31680
H          2.45590        2.67910       -0.92210
H          3.12410        2.33380        1.84490
H          3.65450       -1.63710       -0.92640
H          2.01790       -2.19590       -1.32720
H          2.67960       -2.35330        0.38550
H         -3.65410        0.99950       -1.36830
H         -1.94240        1.36850       -1.07230
H         -2.35520       -0.13620       -2.01480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

