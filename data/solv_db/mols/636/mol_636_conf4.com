%nproc=16
%mem=2GB
%chk=mol_636_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.63200       -0.83780       -1.35170
C         -2.56860       -0.29920       -0.47070
C         -1.54500        0.34160       -1.01170
C         -0.43870        0.90970       -0.18310
C          0.85710        0.21050       -0.65860
C          1.96650        0.74020        0.13090
C          2.75850        0.06790        0.91060
C          2.76540       -1.35270        1.22000
O          1.84530       -2.18110        0.65980
C          2.15790        2.26000       -0.01630
C         -2.61750       -0.45750        1.00820
H         -3.95280       -0.10250       -2.12910
H         -3.27240       -1.73170       -1.93570
H         -4.48830       -1.21390       -0.75450
H         -1.53180        0.44450       -2.09140
H         -0.60930        0.70870        0.88010
H         -0.31700        1.98970       -0.34970
H          0.71590       -0.85880       -0.64400
H          0.97510        0.46850       -1.76070
H          3.54050        0.73040        1.40940
H          2.73760       -1.53670        2.34660
H          3.77640       -1.78980        0.94390
H          2.20060       -2.75990       -0.05970
H          3.17500        2.50720        0.29420
H          2.08040        2.44320       -1.10520
H          1.35770        2.79970        0.48230
H         -3.58130       -0.95500        1.30630
H         -2.54440        0.53820        1.52330
H         -1.81070       -1.08340        1.40640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

