%nproc=16
%mem=2GB
%chk=mol_636_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.07120       -0.43240        1.27620
C         -2.67410        0.08190       -0.05520
C         -1.42520       -0.16460       -0.46320
C         -0.46600       -0.90310        0.33650
C          0.78670       -0.16600        0.74980
C          1.62260        0.32280       -0.33220
C          2.85700       -0.12050       -0.42620
C          3.51140       -1.06720        0.43150
O          4.63410       -0.40370        0.99650
C          1.11490        1.39980       -1.23480
C         -3.65080        0.84130       -0.88820
H         -4.07580       -0.03360        1.52240
H         -2.35200       -0.00090        2.01390
H         -3.05420       -1.53880        1.30600
H         -1.18790        0.15020       -1.46900
H         -0.15540       -1.84890       -0.19410
H         -0.93570       -1.27160        1.28130
H          0.41020        0.77000        1.28650
H          1.24490       -0.78970        1.51890
H          3.46160        0.28530       -1.26780
H          3.99780       -1.88440       -0.19290
H          3.02650       -1.55830        1.23580
H          5.07300        0.09030        0.25100
H          1.89290        2.20930       -1.36450
H          0.26740        1.90770       -0.73300
H          0.89030        1.05020       -2.24900
H         -4.56160        0.23880       -1.02210
H         -3.94120        1.73480       -0.27660
H         -3.24010        1.10140       -1.86280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

