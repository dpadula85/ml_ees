%nproc=16
%mem=2GB
%chk=mol_636_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.85030       -1.22540        0.55000
C          2.69430       -0.53960       -0.07520
C          1.46980       -0.97710        0.13210
C          0.28580       -0.29560       -0.49190
C         -0.62380        0.18550        0.62500
C         -1.78060        0.85170       -0.00800
C         -3.03680        0.49070        0.12380
C         -3.45260       -0.65050        0.93240
O         -4.12450       -1.62280        0.18260
C         -1.45750        2.04940       -0.87720
C          2.89930        0.64620       -0.94040
H          4.23160       -0.68760        1.44490
H          4.71070       -1.24510       -0.16980
H          3.58690       -2.26960        0.74990
H          1.31340       -1.84370        0.76390
H          0.61930        0.59250       -1.03660
H         -0.23390       -0.98870       -1.15400
H         -0.07650        0.96670        1.21290
H         -0.80880       -0.65160        1.27400
H         -3.75360        1.12160       -0.42730
H         -2.59680       -1.15630        1.38080
H         -4.18600       -0.32770        1.73950
H         -4.43880       -2.30270        0.82880
H         -1.11400        1.74570       -1.87340
H         -0.62120        2.59550       -0.39330
H         -2.31330        2.74200       -0.93050
H          2.44120        0.45440       -1.94120
H          2.53220        1.58680       -0.49450
H          3.98360        0.75520       -1.12730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_636_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

