%nproc=16
%mem=2GB
%chk=mol_126_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.53130        0.05330        0.05080
N          0.90160       -0.06080       -0.09430
O          1.58780        0.89900       -0.48180
O          1.52820       -1.24260        0.19710
Cl        -1.06610        1.69040       -0.39460
Cl        -1.38800       -1.08550       -1.01390
Cl        -1.03220       -0.25390        1.73670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_126_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_126_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_126_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

