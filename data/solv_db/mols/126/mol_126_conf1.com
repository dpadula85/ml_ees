%nproc=16
%mem=2GB
%chk=mol_126_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.52400        0.04750        0.14130
N         -0.85190       -0.12440       -0.27730
O         -1.12980       -1.11010       -0.95140
O         -1.77100        0.80320        0.08880
Cl         1.19050        1.57220       -0.48880
Cl         0.49240        0.07800        1.93930
Cl         1.54580       -1.26650       -0.45200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_126_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_126_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_126_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

