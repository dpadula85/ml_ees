%nproc=16
%mem=2GB
%chk=mol_545_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.31770       -0.18300       -1.14620
C          1.14460       -0.51980       -0.24460
C          1.64450       -0.60230        1.15360
C          0.09440        0.52870       -0.46350
C         -1.13940        0.30920        0.38260
C         -2.10400        1.45400        0.04390
C         -1.85940       -0.96380       -0.01290
H          2.36530        0.92430       -1.22520
H          3.27490       -0.53770       -0.72200
H          2.11200       -0.59370       -2.16090
H          0.76780       -1.50340       -0.57670
H          1.44700        0.29230        1.76850
H          1.20330       -1.47510        1.69950
H          2.75620       -0.77480        1.18670
H         -0.23570        0.48270       -1.52130
H          0.53510        1.52100       -0.25840
H         -0.94440        0.30310        1.45340
H         -1.96810        1.65570       -1.03740
H         -1.95470        2.32420        0.68990
H         -3.16050        1.11240        0.15490
H         -2.47020       -0.79600       -0.93560
H         -1.19900       -1.82250       -0.14210
H         -2.62750       -1.13550        0.79670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

