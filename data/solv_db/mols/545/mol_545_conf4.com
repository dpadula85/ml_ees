%nproc=16
%mem=2GB
%chk=mol_545_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.48540        0.72880        0.30930
C          1.24070        0.16530       -0.44920
C          1.47870       -1.32610       -0.33940
C          0.01150        0.63440        0.23370
C         -1.31230        0.27680       -0.31370
C         -1.58020       -1.17720       -0.35920
C         -2.42900        1.02320        0.44230
H          2.28850        1.74590        0.64030
H          3.35600        0.67870       -0.35050
H          2.69060        0.09710        1.19750
H          1.31020        0.48700       -1.50830
H          2.58820       -1.54280       -0.45130
H          1.27810       -1.65370        0.70440
H          0.89730       -1.84650       -1.08610
H          0.04940        1.77420        0.34410
H          0.11230        0.32090        1.32590
H         -1.41390        0.71540       -1.36460
H         -1.37230       -1.55340       -1.40020
H         -2.66950       -1.41800       -0.16710
H         -1.01610       -1.80720        0.33800
H         -1.96290        1.97410        0.77080
H         -2.74770        0.46850        1.33250
H         -3.28310        1.23450       -0.21490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

