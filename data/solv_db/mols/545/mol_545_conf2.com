%nproc=16
%mem=2GB
%chk=mol_545_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.66940       -0.70140       -1.10230
C         -1.24080       -0.15470        0.22000
C         -2.33750        0.81180        0.68140
C          0.03240        0.61340        0.18790
C          1.23070       -0.13310       -0.27260
C          1.60050       -1.32610        0.53200
C          2.41490        0.82690       -0.24310
H         -1.05370       -1.54000       -1.46580
H         -1.77490        0.08460       -1.87050
H         -2.70620       -1.11850       -0.96230
H         -1.22830       -0.96780        0.96210
H         -2.07960        1.86090        0.36890
H         -2.43760        0.83170        1.77190
H         -3.30000        0.58290        0.21200
H         -0.09580        1.50490       -0.45590
H          0.22320        1.00020        1.21190
H          1.10620       -0.44640       -1.32520
H          0.86420       -2.15150        0.48990
H          2.54460       -1.73570        0.11220
H          1.81260       -1.10350        1.59680
H          3.15200        0.60580       -1.01820
H          2.03630        1.87050       -0.39980
H          2.90620        0.78500        0.76870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

