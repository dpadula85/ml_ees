%nproc=16
%mem=2GB
%chk=mol_545_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.70300        0.53730       -1.04580
C          1.19250       -0.43810       -0.01020
C          2.27040       -0.52240        1.06910
C         -0.03380        0.13990        0.65810
C         -1.14680        0.41170       -0.30190
C         -1.55380       -0.90080       -0.98130
C         -2.37090        0.80070        0.53880
H          1.94230        1.48850       -0.48720
H          2.62840        0.17070       -1.51990
H          0.91360        0.71180       -1.79330
H          1.03960       -1.44950       -0.40380
H          2.04490       -1.47380        1.62810
H          2.13330        0.36780        1.71040
H          3.28570       -0.54320        0.61900
H         -0.41290       -0.52160        1.46320
H          0.27780        1.08430        1.14760
H         -0.92740        1.17810       -1.04820
H         -1.68730       -1.64340       -0.16290
H         -2.53190       -0.77400       -1.51100
H         -0.75580       -1.23830       -1.66470
H         -2.40180        1.89230        0.71480
H         -2.32990        0.19120        1.44350
H         -3.27920        0.53060       -0.06220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

