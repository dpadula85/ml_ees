%nproc=16
%mem=2GB
%chk=mol_545_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.01270        0.98520       -0.39110
C          1.31280        0.05970        0.57680
C          1.61570       -1.35650        0.20160
C         -0.09890        0.39320        0.82910
C         -1.05860        0.41420       -0.28280
C         -1.23120       -0.88990       -1.01840
C         -2.43870        0.72860        0.31860
H          1.65360        0.88370       -1.42180
H          1.97600        2.03410       -0.06950
H          3.08340        0.68700       -0.39870
H          1.85630        0.25140        1.56090
H          0.72890       -1.98410        0.42180
H          2.49990       -1.70490        0.79900
H          1.87570       -1.37550       -0.88130
H         -0.47860       -0.37430        1.57080
H         -0.12940        1.34330        1.43450
H         -0.89230        1.18840       -1.05080
H         -2.15590       -0.74270       -1.65410
H         -1.53680       -1.64980       -0.24010
H         -0.37600       -1.21100       -1.60700
H         -2.37310        1.64420        0.91340
H         -2.71190       -0.16680        0.91580
H         -3.13340        0.84250       -0.52670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_545_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

