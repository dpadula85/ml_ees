%nproc=16
%mem=2GB
%chk=mol_276_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.37660        0.11950       -0.00280
C         -0.61920        1.26170       -0.00210
C          0.75630        1.14820        0.00080
C          1.32850       -0.10180        0.00270
C          0.58070       -1.26390        0.00200
C         -0.79060       -1.12970       -0.00080
F          2.68020       -0.22060        0.00560
H         -2.46830        0.19110       -0.00510
H         -1.08100        2.24840       -0.00380
H          1.34490        2.06080        0.00120
H          1.02740       -2.25690        0.00370
H         -1.38230       -2.05690       -0.00140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_276_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_276_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_276_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

