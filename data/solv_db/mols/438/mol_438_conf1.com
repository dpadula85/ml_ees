%nproc=16
%mem=2GB
%chk=mol_438_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.04260        1.38540        0.00930
C          1.18940        0.62570        0.01520
C          1.17620       -0.75960        0.00610
C         -0.03800       -1.39330       -0.00930
C         -1.19470       -0.65670       -0.01540
C         -1.17530        0.73810       -0.00620
Cl        -2.66760        1.65390       -0.01360
Cl        -2.76560       -1.44770       -0.03620
Cl         2.69170       -1.62390        0.01420
Cl         2.73720        1.47750        0.03600
H          0.07250        2.48320        0.01630
H         -0.06830       -2.48240       -0.01640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_438_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_438_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_438_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

