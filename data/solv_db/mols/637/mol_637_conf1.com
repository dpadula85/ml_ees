%nproc=16
%mem=2GB
%chk=mol_637_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.55140       -0.35750        0.00270
C         -0.57490        0.36920       -0.00270
Cl        -2.09940       -0.46320        0.00170
Cl         2.11600        0.44690       -0.00160
H          0.51770       -1.43170        0.00950
H         -0.51080        1.43620       -0.00960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_637_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_637_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_637_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

