%nproc=16
%mem=2GB
%chk=mol_303_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.70290       -0.04220        0.20530
O         -2.50640        0.64270        0.36310
C         -1.24990        0.10750        0.05030
C         -1.15150       -1.18090       -0.44970
C          0.07230       -1.73700       -0.76780
C          1.20810       -0.97300       -0.57460
C          1.16280        0.31280       -0.08010
C         -0.08790        0.81420        0.21940
O         -0.22690        2.11850        0.72900
O          2.27980        1.09100        0.12020
C          3.59390        0.73530       -0.12920
H         -4.49020        0.53180        0.77590
H         -3.62670       -1.08020        0.58510
H         -4.00050       -0.12060       -0.87120
H         -2.08780       -1.73830       -0.58120
H          0.11770       -2.74990       -1.15800
H          2.17560       -1.39090       -0.81790
H          0.59930        2.67270        0.86890
H          4.23110        1.64090        0.04700
H          3.76240        0.35030       -1.14820
H          3.92770       -0.00490        0.65130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

