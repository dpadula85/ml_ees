%nproc=16
%mem=2GB
%chk=mol_303_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.56150       -0.10820        0.82500
O         -2.24560       -0.11570        1.36900
C         -1.17970       -0.05300        0.47910
C         -1.37830        0.01530       -0.90280
C         -0.32920        0.07730       -1.78400
C          0.97320        0.07500       -1.34360
C          1.17110        0.00730        0.02730
C          0.11760       -0.05550        0.92170
O          0.36100       -0.12240        2.29220
O          2.46090        0.00000        0.55760
C          3.55860        0.06220       -0.31350
H         -3.71500        0.78910        0.15550
H         -4.33020       -0.02430        1.60860
H         -3.67220       -0.98490        0.17720
H         -2.41880        0.01500       -1.21640
H         -0.51670        0.12890       -2.83890
H          1.82070        0.12300       -2.01600
H          1.29670       -0.12590        2.64450
H          3.55450        1.00880       -0.92800
H          4.48730        0.04830        0.30060
H          3.54570       -0.76040       -1.06720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

