%nproc=16
%mem=2GB
%chk=mol_303_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61500       -0.48580       -0.39890
O         -2.33800       -1.03260       -0.66030
C         -1.21980       -0.32950       -0.23320
C         -1.30390        0.87220        0.43100
C         -0.17860        1.55720        0.84810
C          1.04950        1.01130        0.58550
C          1.14500       -0.18960       -0.07780
C          0.01490       -0.87100       -0.49310
O          0.16080       -2.08760       -1.16350
O          2.40340       -0.70800       -0.32410
C          3.55100       -0.03540        0.08690
H         -3.77080       -0.54460        0.69890
H         -4.41430       -1.00100       -0.96380
H         -3.67830        0.58720       -0.67790
H         -2.27480        1.28670        0.62930
H         -0.22550        2.50230        1.37120
H          1.94060        1.55710        0.91760
H          1.07700       -2.47390       -1.34770
H          3.77300        0.89280       -0.43980
H          3.47080        0.23110        1.17840
H          4.43290       -0.73880        0.03320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

