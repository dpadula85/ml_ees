%nproc=16
%mem=2GB
%chk=mol_303_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.49290       -0.08490       -0.84020
O         -2.19860       -0.39760       -1.30110
C         -1.12910       -0.11160       -0.46250
C         -1.27770        0.45750        0.78470
C         -0.17650        0.72410        1.58550
C          1.10060        0.43030        1.16370
C          1.26620       -0.13600       -0.07410
C          0.15070       -0.39950       -0.87080
O          0.38660       -0.97160       -2.10870
O          2.54490       -0.43280       -0.50270
C          3.64800       -0.15150        0.33110
H         -4.28920       -0.49260       -1.49340
H         -3.62520       -0.54030        0.16010
H         -3.62530        1.01460       -0.82220
H         -2.28830        0.68550        1.10860
H         -0.33650        1.17250        2.56040
H          1.95900        0.64710        1.80880
H         -0.37330       -1.17790       -2.71130
H          4.61370       -0.34470       -0.17100
H          3.59730        0.92990        0.62720
H          3.54530       -0.82050        1.22790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

