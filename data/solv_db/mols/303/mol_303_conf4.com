%nproc=16
%mem=2GB
%chk=mol_303_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.56630        0.23410        0.35200
O         -2.36150        0.94950        0.19250
C         -1.14960        0.24900        0.03600
C         -1.11040       -1.12200        0.03460
C          0.06100       -1.81040       -0.11670
C          1.24210       -1.08060       -0.27310
C          1.22410        0.29230       -0.27460
C          0.02490        0.94350       -0.11960
O          0.02320        2.33110       -0.12320
O          2.37670        0.99250       -0.42730
C          3.62440        0.39500       -0.58880
H         -4.36600        0.98050        0.53740
H         -3.48240       -0.50400        1.17390
H         -3.77860       -0.26290       -0.61530
H         -2.05410       -1.65120        0.15950
H          0.08460       -2.88920       -0.11680
H          2.13790       -1.65950       -0.38860
H         -0.83920        2.82810       -0.01170
H          4.30510        1.14040       -1.09290
H          4.02920        0.13790        0.41380
H          3.57480       -0.49450       -1.24860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_303_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

