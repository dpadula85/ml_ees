%nproc=16
%mem=2GB
%chk=mol_525_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.94880       -0.58940       -0.39200
C          3.82860        0.62520        0.24650
C          2.59710        1.14840        0.54730
C          1.43140        0.47720        0.22130
C          1.54590       -0.74010       -0.41850
C          2.79090       -1.25550       -0.71540
C          0.31100       -1.46490       -0.77120
O          0.43650       -2.57580       -1.35550
C         -1.01160       -0.93460       -0.46490
C         -2.17060       -1.60940       -0.79290
C         -3.41900       -1.10210       -0.50000
C         -3.48880        0.13490        0.14920
C         -2.35570        0.83750        0.49230
C         -1.12620        0.29220        0.17990
C          0.10460        0.99580        0.52160
O          0.00110        2.10740        1.10580
N         -2.48210        2.09260        1.15210
H          4.93820       -0.95310       -0.60400
H          4.70500        1.20390        0.53030
H          2.53320        2.09000        1.04160
H          2.85360       -2.21360       -1.21820
H         -2.09970       -2.56440       -1.29450
H         -4.31240       -1.65050       -0.76770
H         -4.48410        0.52370        0.37450
H         -2.24790        2.21130        2.15920
H         -2.82790        2.91320        0.57300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_525_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_525_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_525_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

