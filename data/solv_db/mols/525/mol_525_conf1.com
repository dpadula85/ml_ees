%nproc=16
%mem=2GB
%chk=mol_525_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.95130       -0.37220        0.42460
C          3.74830        0.98600        0.36510
C          2.46500        1.46910        0.21680
C          1.35570        0.64280        0.12370
C          1.56360       -0.73140        0.18410
C          2.86100       -1.21430        0.33380
C          0.41810       -1.62500        0.08910
O          0.57490       -2.88310        0.14090
C         -0.94900       -1.09900       -0.06900
C         -2.02360       -1.97360       -0.15720
C         -3.29650       -1.48400       -0.30460
C         -3.48430       -0.12230       -0.36250
C         -2.41700        0.73950       -0.27470
C         -1.11050        0.27360       -0.12450
C          0.01630        1.19120       -0.03220
O         -0.17080        2.42950       -0.08660
N         -2.57330        2.14870       -0.33080
H          4.96790       -0.71850        0.54100
H          4.57960        1.70360        0.43210
H          2.27220        2.54530        0.16640
H          3.00500       -2.29180        0.37930
H         -1.85020       -3.03940       -0.10900
H         -4.15050       -2.16870       -0.37510
H         -4.48930        0.24320       -0.47820
H         -2.60040        2.67020       -1.24610
H         -2.66330        2.68050        0.55380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_525_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_525_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_525_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

