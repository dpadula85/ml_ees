%nproc=16
%mem=2GB
%chk=mol_057_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.92960       -0.37900       -0.17880
C          0.83330        0.25360        0.64080
C         -0.42090        0.33510       -0.20730
C         -0.81690       -1.05420       -0.61970
C         -1.52820        1.07340        0.48670
H          2.80430        0.27900       -0.15500
H          1.61370       -0.44690       -1.25790
H          2.15320       -1.41520        0.15810
H          1.13290        1.27160        0.98500
H          0.67500       -0.33970        1.56180
H         -0.18070        0.89540       -1.13480
H         -1.61080       -1.46630        0.03940
H         -1.21190       -1.05990       -1.67460
H          0.10350       -1.67210       -0.63200
H         -1.52580        0.88450        1.59670
H         -2.50150        0.67540        0.12140
H         -1.44880        2.16540        0.27020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

