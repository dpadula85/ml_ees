%nproc=16
%mem=2GB
%chk=mol_057_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.74940       -0.26090        0.33370
C         -0.82810       -0.11630       -0.82540
C          0.60300        0.14950       -0.44980
C          0.82330        1.38450        0.33400
C          1.22460       -1.04760        0.23620
H         -1.66130        0.55520        1.08180
H         -2.79170       -0.23450       -0.05030
H         -1.65330       -1.26650        0.83390
H         -0.82650       -1.10700       -1.36870
H         -1.21660        0.60940       -1.56180
H          1.15780        0.26070       -1.42760
H          1.81670        1.81310        0.07360
H          0.87230        1.16390        1.43990
H          0.09450        2.19850        0.12990
H          2.12250       -1.42560       -0.28170
H          1.53320       -0.81220        1.28290
H          0.47890       -1.86420        0.21940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

