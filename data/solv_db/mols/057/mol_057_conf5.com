%nproc=16
%mem=2GB
%chk=mol_057_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.02460        0.02110        0.07860
C          0.73050       -0.48660       -0.48790
C         -0.43710        0.42430       -0.21550
C         -1.66280       -0.21720       -0.84410
C         -0.69700        0.45610        1.28890
H          2.85630       -0.55640       -0.37560
H          2.21700        1.08330       -0.16980
H          2.08020       -0.16980        1.17270
H          0.51700       -1.46060        0.00400
H          0.84720       -0.66620       -1.57160
H         -0.29450        1.44110       -0.61640
H         -1.44640       -1.23790       -1.23200
H         -1.99980        0.36290       -1.75240
H         -2.49330       -0.31410       -0.14000
H         -1.51690        1.15970        1.52050
H         -0.98100       -0.57890        1.57830
H          0.25590        0.73910        1.76240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

