%nproc=16
%mem=2GB
%chk=mol_057_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93640       -0.38650       -0.15680
C          0.79320        0.14050        0.65860
C         -0.42980        0.37840       -0.23440
C         -0.87440       -0.88690       -0.89620
C         -1.48300        1.00570        0.64030
H          2.29670       -1.32760        0.31060
H          1.63650       -0.60020       -1.22330
H          2.79750        0.33760       -0.15630
H          1.07100        1.11300        1.11290
H          0.45720       -0.54630        1.45690
H         -0.09280        1.07710       -1.02360
H         -0.56370       -1.74500       -0.23810
H         -1.97450       -0.98230       -0.99270
H         -0.44310       -1.02650       -1.90500
H         -1.10030        2.03050        0.90750
H         -1.55010        0.37730        1.56250
H         -2.47670        1.04120        0.17690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

