%nproc=16
%mem=2GB
%chk=mol_057_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95880        0.50640        0.23150
C          0.92180       -0.53070       -0.02470
C         -0.47530       -0.07590        0.33110
C         -0.89870        1.13270       -0.44970
C         -1.48980       -1.17360        0.09580
H          2.38400        0.30180        1.25740
H          2.83540        0.35920       -0.46340
H          1.62290        1.53820        0.11620
H          1.13290       -1.47030        0.52650
H          0.94070       -0.77960       -1.10550
H         -0.49050        0.22170        1.41530
H         -0.36820        2.06460       -0.12940
H         -0.78530        0.96600       -1.54640
H         -1.98740        1.27870       -0.26140
H         -2.19700       -0.90480       -0.74210
H         -2.12870       -1.34640        0.98880
H         -0.97570       -2.08790       -0.24000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_057_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

