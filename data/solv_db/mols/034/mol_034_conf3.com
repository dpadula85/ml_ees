%nproc=16
%mem=2GB
%chk=mol_034_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21780        0.13430        0.06130
C          0.77920        0.50890       -0.00330
C          0.38670        1.83860       -0.08370
C         -0.95340        2.11930       -0.14020
C         -1.90540        1.10550       -0.11820
C         -1.50670       -0.20770       -0.03840
C         -0.16980       -0.52220        0.01970
C          0.21970       -1.94810        0.10480
O         -2.44070       -1.22950       -0.01550
H          2.42570       -0.25670        1.08660
H          2.81290        1.05610       -0.15660
H          2.40860       -0.65450       -0.71000
H          1.15920        2.61250       -0.09900
H         -1.23470        3.16560       -0.20280
H         -2.94740        1.38730       -0.16510
H          0.01500       -2.49770       -0.84440
H          1.30290       -2.02610        0.31840
H         -0.35700       -2.39360        0.94530
H         -2.21280       -2.19210        0.04090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

