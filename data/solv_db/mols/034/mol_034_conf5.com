%nproc=16
%mem=2GB
%chk=mol_034_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.13540       -0.70960        0.11170
C          0.68780       -0.64460        0.10490
C         -0.06960       -1.78040        0.28550
C         -1.46560       -1.67280        0.27200
C         -2.09250       -0.44890        0.08100
C         -1.31790        0.67950       -0.09850
C          0.07040        0.56560       -0.08400
C          0.86630        1.76250       -0.27530
O         -1.92710        1.91240       -0.29090
H          2.54140       -1.59760        0.68010
H          2.48110       -0.89710       -0.94890
H          2.66970        0.16950        0.45370
H          0.41680       -2.73400        0.43430
H         -2.09440       -2.55670        0.41230
H         -3.17470       -0.39450        0.07490
H          1.03590        2.22660        0.74070
H          0.30540        2.46140       -0.93080
H          1.85860        1.63080       -0.71590
H         -2.92700        2.02780       -0.30650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

