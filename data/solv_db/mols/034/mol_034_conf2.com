%nproc=16
%mem=2GB
%chk=mol_034_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11840       -0.55270       -0.06470
C          0.86550        0.24780       -0.03390
C          0.89750        1.64000       -0.06010
C         -0.29070        2.34270       -0.02960
C         -1.48250        1.64410        0.02640
C         -1.52840        0.25570        0.05310
C         -0.33630       -0.42790        0.02210
C         -0.34280       -1.89360        0.04800
O         -2.71690       -0.44510        0.10920
H          2.28880       -1.05250       -1.02870
H          2.96950        0.14360        0.19510
H          2.14350       -1.32510        0.74150
H          1.87200        2.14540       -0.10460
H         -0.24920        3.42430       -0.05090
H         -2.40390        2.19800        0.04990
H          0.64280       -2.33660       -0.20000
H         -1.02650       -2.23440       -0.78320
H         -0.74420       -2.32160        0.98450
H         -2.67650       -1.45200        0.12600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

