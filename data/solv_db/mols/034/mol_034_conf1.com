%nproc=16
%mem=2GB
%chk=mol_034_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.02320        0.99150       -0.04740
C          0.55130        0.74920       -0.10360
C         -0.33040        1.79110       -0.36600
C         -1.68670        1.55160       -0.41430
C         -2.17010        0.27710       -0.20220
C         -1.32350       -0.78690        0.06200
C          0.02940       -0.51520        0.10390
C          0.93710       -1.68320        0.39230
O         -1.82520       -2.07770        0.27600
H          2.49550        0.73620       -1.00710
H          2.49570        0.50360        0.82920
H          2.16830        2.09280        0.11700
H          0.04920        2.77990       -0.53030
H         -2.37980        2.36050       -0.61890
H         -3.22880        0.03700       -0.22950
H          0.67930       -2.15100        1.36690
H          1.99650       -1.40310        0.30170
H          0.66770       -2.44780       -0.39330
H         -1.14870       -2.80570        0.46350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

