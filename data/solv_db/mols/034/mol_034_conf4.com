%nproc=16
%mem=2GB
%chk=mol_034_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.13730        0.54280        0.20240
C         -0.87350       -0.23150        0.10600
C         -0.87660       -1.60850        0.17090
C          0.31180       -2.33200        0.07980
C          1.50220       -1.65190       -0.07740
C          1.53290       -0.27520       -0.14520
C          0.33920        0.43020       -0.05270
C          0.32410        1.89200       -0.11980
O          2.72710        0.41700       -0.30340
H         -2.26940        1.03110        1.18240
H         -2.17660        1.35060       -0.58160
H         -2.98770       -0.14380       -0.05070
H         -1.79940       -2.16490        0.29410
H          0.31040       -3.41640        0.13090
H          2.44190       -2.20850       -0.15020
H         -0.60450        2.29130        0.36630
H          0.30040        2.29170       -1.16310
H          1.16420        2.36870        0.46640
H          2.77100        1.41720       -0.35500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_034_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

