%nproc=16
%mem=2GB
%chk=mol_357_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.41900       -1.32920       -0.56370
N         -2.67370       -0.09420        0.10160
C         -4.09080        0.20460        0.42220
C         -1.70100        0.84100        0.44110
O         -2.18050        1.88620        1.06700
C         -0.26130        0.82130        0.23970
C          0.36610       -0.13300       -0.49870
C          1.74000       -0.36370       -0.37190
C          2.47680        0.40240        0.49700
C          1.88940        1.40730        1.25750
C          0.54240        1.59750        1.11660
O          3.84110        0.20380        0.65120
C          4.50570       -0.81260       -0.10060
H         -3.31130       -2.02680       -0.42590
H         -1.56680       -1.88640       -0.12770
H         -2.35550       -1.19430       -1.67160
H         -4.36830       -0.23120        1.39280
H         -4.74700       -0.18680       -0.35820
H         -4.23320        1.31790        0.48370
H         -0.14660       -0.50310       -1.40250
H          2.17440       -1.14350       -0.96830
H          2.47720        2.01300        1.93310
H          0.04280        2.36440        1.67540
H          5.59710       -0.61580        0.04030
H          4.20870       -1.81060        0.29830
H          4.19350       -0.72830       -1.15770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

