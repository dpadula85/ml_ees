%nproc=16
%mem=2GB
%chk=mol_357_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.55000        1.09070        1.14480
N         -2.73260        0.25670        0.00700
C         -4.10130        0.25220       -0.55490
C         -1.77010       -0.49340       -0.63640
O         -2.17880       -1.21100       -1.65820
C         -0.35100       -0.64540       -0.35200
C          0.40140       -1.60180       -1.05870
C          1.77090       -1.67030       -1.01350
C          2.49650       -0.78510       -0.25350
C          1.81260        0.15930        0.44460
C          0.41150        0.22750        0.38850
O          3.87240       -0.89770       -0.23700
C          4.71160       -0.03840        0.51090
H         -2.12820        2.09420        0.94740
H         -1.99810        0.52330        1.92520
H         -3.55300        1.33270        1.63720
H         -4.11730       -0.19950       -1.55800
H         -4.48780        1.28470       -0.57600
H         -4.77550       -0.34710        0.09220
H         -0.14670       -2.34060       -1.64350
H          2.31240       -2.41990       -1.57260
H          2.36510        0.87400        1.05840
H         -0.02060        1.10000        0.84360
H          5.74280       -0.02710        0.14660
H          4.71820       -0.39480        1.55460
H          4.29570        0.98450        0.41330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

