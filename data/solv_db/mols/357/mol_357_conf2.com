%nproc=16
%mem=2GB
%chk=mol_357_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.63900       -0.69500       -1.35780
N         -2.70070        0.22700       -0.25990
C         -4.04880        0.82290        0.00280
C         -1.67900        0.64730        0.56910
O         -2.04690        1.48680        1.50180
C         -0.27060        0.30800        0.61040
C          0.51300        0.78990        1.67150
C          1.87580        0.62360        1.68080
C          2.51950       -0.02460        0.64690
C          1.79510       -0.51160       -0.41830
C          0.40920       -0.32410       -0.40800
O          3.89720       -0.18120        0.68900
C          4.55960       -0.85040       -0.37880
H         -3.67560       -1.05090       -1.65010
H         -2.17530       -0.21670       -2.23780
H         -2.11870       -1.62830       -1.06500
H         -4.79670        0.06510       -0.22840
H         -4.16690        1.69490       -0.68480
H         -4.10740        1.21070        1.02600
H          0.01490        1.28110        2.51910
H          2.48150        1.00930        2.52210
H          2.24330       -1.03170       -1.25100
H         -0.07060       -0.59040       -1.33720
H          4.48130       -0.27740       -1.32040
H          5.62630       -0.93140       -0.06420
H          4.07950       -1.85300       -0.47740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

