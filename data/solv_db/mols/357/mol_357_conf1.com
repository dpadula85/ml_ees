%nproc=16
%mem=2GB
%chk=mol_357_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.87970       -0.40220       -1.23760
N         -2.71360        0.48600       -0.14410
C         -3.90440        1.30480        0.29800
C         -1.56220        0.66800        0.59060
O         -1.58290        1.55480        1.55930
C         -0.27790        0.01730        0.41760
C         -0.03790       -1.03770       -0.42220
C          1.23190       -1.43250       -0.83080
C          2.33580       -0.76040       -0.36390
C          2.14090        0.29070        0.50150
C          0.88280        0.66240        0.87590
O          3.60910       -1.12410       -0.74810
C          4.75170       -0.44600       -0.27900
H         -3.80690       -0.06050       -1.79010
H         -2.03400       -0.31550       -1.93890
H         -3.07760       -1.44250       -0.91240
H         -3.49430        2.31180        0.50390
H         -4.32790        0.82270        1.18660
H         -4.59840        1.38180       -0.54550
H         -0.82950       -1.77750       -0.54230
H          1.28190       -2.26070       -1.51410
H          3.04570        0.79410        0.85840
H          0.75450        1.47960        1.55590
H          4.60680        0.63050       -0.52720
H          5.65650       -0.79550       -0.77510
H          4.82950       -0.54930        0.83420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

