%nproc=16
%mem=2GB
%chk=mol_357_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.13400        0.04030        0.54600
N         -2.75990       -0.00950       -0.00250
C         -2.66060       -0.04680       -1.44320
C         -1.68110        0.36880        0.82160
O         -2.01490        0.77600        2.02020
C         -0.27830        0.37780        0.50470
C          0.60710        0.94530        1.42110
C          1.97480        0.78720        1.35820
C          2.52430        0.03200        0.34530
C          1.67540       -0.52930       -0.54510
C          0.27870       -0.37630       -0.48150
O          3.90100       -0.09120        0.32080
C          4.65220       -0.80280       -0.60800
H         -4.11850       -0.58840        1.46750
H         -4.83190       -0.45300       -0.15580
H         -4.45360        1.06590        0.73780
H         -2.58540       -1.07580       -1.85410
H         -1.86870        0.64200       -1.83100
H         -3.61780        0.34380       -1.91500
H          0.24150        1.55900        2.25440
H          2.64130        1.24940        2.09790
H          2.06770       -1.12840       -1.35220
H         -0.32920       -1.01350       -1.14060
H          5.34440       -1.47940       -0.04510
H          5.32880       -0.06460       -1.12580
H          4.09660       -1.30940       -1.39540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_357_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

