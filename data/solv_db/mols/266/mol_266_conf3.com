%nproc=16
%mem=2GB
%chk=mol_266_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.94900        0.35490       -0.01830
C          0.21850       -0.59270        0.14810
C          1.45320        0.13830       -0.11340
N          2.45770        0.71110       -0.32700
H         -1.84610        0.00490        0.49340
H         -0.60790        1.34190        0.36980
H         -1.08870        0.49480       -1.11040
H          0.13740       -1.39030       -0.60060
H          0.22490       -1.06290        1.15840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

