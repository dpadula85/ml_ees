%nproc=16
%mem=2GB
%chk=mol_266_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.92140        0.44540        0.00020
C          0.18210       -0.61340        0.03130
C          1.51030        0.00630       -0.03630
N          2.56680        0.48950       -0.08310
H         -0.41940        1.41990       -0.22940
H         -1.69850        0.18390       -0.73800
H         -1.36900        0.51440        1.00140
H          0.06380       -1.22990       -0.89860
H          0.08540       -1.21600        0.95260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

