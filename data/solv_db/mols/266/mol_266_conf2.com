%nproc=16
%mem=2GB
%chk=mol_266_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.98100       -0.28510        0.01970
C          0.29140        0.54750       -0.00500
C          1.48960       -0.28560       -0.03850
N          2.44870       -0.93550       -0.06340
H         -1.54910       -0.13650       -0.92770
H         -1.61220        0.01560        0.90030
H         -0.65470       -1.33860        0.14430
H          0.28660        1.17800       -0.90020
H          0.28080        1.24030        0.87060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

