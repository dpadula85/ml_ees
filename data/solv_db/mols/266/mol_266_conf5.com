%nproc=16
%mem=2GB
%chk=mol_266_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97120       -0.22690       -0.10480
C          0.31900        0.52030        0.09020
C          1.48170       -0.35880        0.07620
N          2.41460       -1.06000        0.05540
H         -0.80760       -1.25480       -0.48910
H         -1.45620       -0.35440        0.89210
H         -1.67790        0.34940       -0.76090
H          0.44300        1.24580       -0.76490
H          0.25470        1.13940        1.00580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

