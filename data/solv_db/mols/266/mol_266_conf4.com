%nproc=16
%mem=2GB
%chk=mol_266_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.96140       -0.36290        0.01690
C          0.23040        0.60050        0.07720
C          1.45290       -0.16250       -0.13890
N          2.44000       -0.76670       -0.31660
H         -1.87760        0.18380        0.24210
H         -0.74080       -1.14180        0.77740
H         -0.92720       -0.82650       -0.97080
H          0.22820        1.12500        1.05730
H          0.15550        1.35110       -0.74450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_266_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

