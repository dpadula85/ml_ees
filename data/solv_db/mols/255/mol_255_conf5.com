%nproc=16
%mem=2GB
%chk=mol_255_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73170       -0.05820        0.32850
N         -1.34320        0.12990        0.13820
C         -0.84880       -0.38680       -1.10670
C          0.51490        0.27660       -1.31320
N          1.37740       -0.25220       -0.28050
C          0.91400       -0.05070        1.03900
C         -0.53590       -0.22550        1.27740
C          2.72850        0.28610       -0.38740
H         -3.27060        0.35440       -0.55020
H         -3.06050        0.53840        1.21590
H         -3.00560       -1.12860        0.43880
H         -0.73870       -1.49250       -1.10950
H         -1.56070       -0.13400       -1.89780
H          0.92580        0.06680       -2.29620
H          0.32020        1.37730       -1.15250
H          1.45930       -0.77850        1.70970
H          1.28790        0.95540        1.38670
H         -0.82840        0.49280        2.09980
H         -0.79050       -1.23870        1.68800
H          2.97010        0.40800       -1.45820
H          3.42500       -0.40730        0.09650
H          2.79130        1.26720        0.13370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

