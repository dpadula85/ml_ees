%nproc=16
%mem=2GB
%chk=mol_255_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.66340        0.05950        0.43440
N         -1.47510       -0.03290       -0.37600
C         -0.79430       -1.24880       -0.34180
C          0.69760       -1.18990       -0.37860
N          1.24420       -0.06450        0.31050
C          0.69650        1.13600       -0.23510
C         -0.75990        1.17240       -0.40630
C          2.68620       -0.04250        0.05390
H         -3.49800       -0.48850       -0.01020
H         -2.42080       -0.20810        1.46970
H         -2.94420        1.15170        0.45090
H         -1.11020       -1.87480       -1.22780
H         -1.13550       -1.85000        0.54430
H          1.07950       -1.15090       -1.43350
H          1.07750       -2.16610        0.02030
H          1.22130        1.32090       -1.21540
H          1.05810        1.98360        0.41990
H         -1.19300        1.86390        0.37680
H         -0.99490        1.70280       -1.37650
H          3.24230       -0.16880        1.00150
H          2.97600       -0.82570       -0.69040
H          3.01010        0.92080       -0.38860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

