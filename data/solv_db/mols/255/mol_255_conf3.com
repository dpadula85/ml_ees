%nproc=16
%mem=2GB
%chk=mol_255_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.62090       -0.10950       -0.20890
N         -1.33340       -0.19250        0.28350
C         -0.55180       -1.33500        0.06210
C          0.63460       -0.96650       -0.78670
N          1.22630        0.26500       -0.52280
C          0.57830        1.23640        0.23680
C         -0.71120        0.90020        0.87950
C          2.65890        0.19560       -0.22000
H         -3.11840       -1.10150       -0.27840
H         -3.24370        0.56530        0.44220
H         -2.62950        0.39030       -1.21100
H         -1.11840       -2.16350       -0.38150
H         -0.13750       -1.68240        1.05350
H          1.38650       -1.78550       -0.65440
H          0.29510       -1.04180       -1.85360
H          1.28100        1.59880        1.04520
H          0.42180        2.15720       -0.39970
H         -0.55480        0.68170        1.97810
H         -1.41360        1.78190        0.88960
H          2.95280        1.22050        0.11090
H          3.22070       -0.14030       -1.10610
H          2.77760       -0.47430        0.64200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

