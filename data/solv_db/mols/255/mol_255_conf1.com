%nproc=16
%mem=2GB
%chk=mol_255_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.66340        0.17380       -0.25470
N          1.27400        0.39380       -0.53810
C          0.69620        1.11550        0.56410
C         -0.79600        1.09600        0.40710
N         -1.25650       -0.24840        0.64320
C         -0.45490       -1.25850        0.03350
C          0.55060       -0.75250       -0.94960
C         -2.65480       -0.30480        0.27360
H          3.22280       -0.17890       -1.13410
H          2.82850       -0.46580        0.65190
H          3.09610        1.17090        0.02900
H          1.03180        0.75300        1.54630
H          1.06220        2.17290        0.48290
H         -1.32120        1.75650        1.11580
H         -1.02880        1.34380       -0.64930
H          0.08740       -1.81830        0.81960
H         -1.07800       -2.02740       -0.50420
H          1.21390       -1.59370       -1.23290
H         -0.01520       -0.49710       -1.89160
H         -3.08840        0.73010        0.21650
H         -3.20590       -0.80750        1.08980
H         -2.82710       -0.75350       -0.71870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

