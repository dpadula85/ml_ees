%nproc=16
%mem=2GB
%chk=mol_255_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.75010       -0.03970        0.14170
N         -1.32370       -0.00250        0.31590
C         -0.80500        0.98510       -0.63400
C          0.68360        1.13680       -0.39710
N          1.37570       -0.11670       -0.44500
C          0.79520       -1.07890        0.45140
C         -0.66860       -1.24820        0.02150
C          2.77300        0.10830       -0.22530
H         -3.18000        0.76780        0.79700
H         -3.22070       -0.98340        0.42590
H         -3.04070        0.23550       -0.90590
H         -1.03790        0.65650       -1.65240
H         -1.30400        1.94250       -0.36370
H          1.07110        1.76320       -1.23750
H          0.79560        1.65350        0.57780
H          0.77330       -0.69610        1.50130
H          1.31260       -2.03800        0.41680
H         -0.70400       -1.47500       -1.05660
H         -1.06400       -2.09600        0.60030
H          3.22540       -0.55180        0.53330
H          2.98220        1.19050       -0.02220
H          3.31090       -0.11320       -1.18160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_255_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

