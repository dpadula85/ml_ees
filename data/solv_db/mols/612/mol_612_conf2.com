%nproc=16
%mem=2GB
%chk=mol_612_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.73540        1.18680        0.15360
C         -0.33860        0.65690        0.04700
C          0.74670        1.50850        0.00970
C          2.04650        1.03850       -0.08970
C          2.24980       -0.34200       -0.15330
C          1.19010       -1.22270       -0.11860
C         -0.08390       -0.69840       -0.01880
C         -1.24930       -1.62450        0.02000
H         -2.41940        0.64960       -0.55200
H         -2.14390        1.09080        1.17170
H         -1.69570        2.23810       -0.20340
H          0.59450        2.59280        0.05910
H          2.90320        1.66330       -0.12090
H          3.26820       -0.73370       -0.23270
H          1.36010       -2.30740       -0.16930
H         -1.98350       -1.32860        0.81890
H         -1.77700       -1.70120       -0.93450
H         -0.93220       -2.66670        0.31320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

