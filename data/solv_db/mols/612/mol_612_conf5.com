%nproc=16
%mem=2GB
%chk=mol_612_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.28230       -1.58330        0.13330
C         -0.13140       -0.70790        0.01600
C          1.13390       -1.26250       -0.10230
C          2.21670       -0.38490       -0.21230
C          2.03360        0.98350       -0.20350
C          0.74790        1.52100       -0.08310
C         -0.31700        0.65280        0.02510
C         -1.66280        1.17910        0.15160
H         -1.09300       -2.55190       -0.39220
H         -2.23130       -1.20160       -0.22820
H         -1.39180       -1.87640        1.21890
H          1.30760       -2.33030       -0.11240
H          3.21010       -0.83160       -0.30510
H          2.91000        1.61810       -0.29180
H          0.61380        2.59130       -0.07720
H         -1.63960        2.20490        0.62600
H         -2.05100        1.38400       -0.88950
H         -2.37350        0.59560        0.72680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

