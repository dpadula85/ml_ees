%nproc=16
%mem=2GB
%chk=mol_612_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.18260       -1.72380        0.01260
C         -0.09410       -0.71850       -0.01250
C          1.21540       -1.15370       -0.06320
C          2.24920       -0.22940       -0.08740
C          2.01040        1.12920       -0.06220
C          0.69470        1.53850       -0.01170
C         -0.35630        0.62100        0.01320
C         -1.76510        1.12900        0.06820
H         -1.81590       -1.63060       -0.91140
H         -0.77180       -2.74400        0.09370
H         -1.86980       -1.58910        0.88250
H          1.43780       -2.22830       -0.08420
H          3.28310       -0.59940       -0.12770
H          2.85210        1.80840       -0.08260
H          0.49870        2.60430        0.00830
H         -2.40180        0.43030        0.68480
H         -2.18130        1.25620       -0.95250
H         -1.80290        2.09980        0.63210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

