%nproc=16
%mem=2GB
%chk=mol_612_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12900        1.76870        0.01650
C         -0.07340        0.71510       -0.01500
C          1.25200        1.09720       -0.09040
C          2.25290        0.15340       -0.12170
C          1.92310       -1.18430       -0.07710
C          0.61060       -1.60260       -0.00150
C         -0.37090       -0.62320        0.02800
C         -1.79460       -1.01210        0.10820
H         -2.07420        1.39800        0.41160
H         -0.75400        2.53780        0.75190
H         -1.20820        2.29810       -0.95520
H          1.49210        2.15750       -0.12430
H          3.30730        0.41420       -0.18120
H          2.71270       -1.94660       -0.10150
H          0.32690       -2.64860        0.03500
H         -1.88050       -2.04780       -0.31710
H         -2.15500       -1.09640        1.15700
H         -2.43790       -0.37850       -0.52320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

