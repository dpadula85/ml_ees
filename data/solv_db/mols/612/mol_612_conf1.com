%nproc=16
%mem=2GB
%chk=mol_612_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.34600       -1.61110        0.01340
C         -0.15970       -0.74010        0.00010
C          1.13700       -1.24840       -0.01490
C          2.21790       -0.38960       -0.02610
C          2.06410        0.98200       -0.02310
C          0.78440        1.49710       -0.00830
C         -0.30570        0.62830        0.00310
C         -1.65910        1.22200        0.01970
H         -2.15070       -1.26250        0.67750
H         -1.02540       -2.60910        0.43210
H         -1.69930       -1.80440       -1.02930
H          1.25840       -2.33910       -0.01740
H          3.22920       -0.80120       -0.03750
H          2.93260        1.63760       -0.03190
H          0.64800        2.57360       -0.00570
H         -1.82080        1.67530        1.03440
H         -1.66980        2.07060       -0.71180
H         -2.43520        0.51900       -0.27430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_612_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

