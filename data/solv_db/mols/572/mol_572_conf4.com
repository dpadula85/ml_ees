%nproc=16
%mem=2GB
%chk=mol_572_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.95020       -0.50180       -0.98370
O         -1.27700       -0.12080        0.14450
C          0.09270        0.02190       -0.02520
C          0.73840       -1.27790       -0.44480
C          2.18920       -1.00100       -0.58400
N          3.31390       -0.79520       -0.67590
O          0.37800        0.98930       -0.99080
C         -0.11830        2.23500       -0.66340
O          0.71160        0.43920        1.13980
C          0.56040       -0.40740        2.20670
H         -3.03840       -0.57090       -0.69010
H         -1.88880        0.26570       -1.75820
H         -1.69600       -1.51410       -1.36190
H          0.56740       -2.10220        0.26970
H          0.33350       -1.56520       -1.44840
H          0.16260        2.92100       -1.49280
H         -1.20900        2.19110       -0.50440
H          0.34420        2.66070        0.24830
H          1.10510        0.08240        3.07300
H         -0.47040       -0.60040        2.51580
H          1.15100       -1.34950        2.02580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

