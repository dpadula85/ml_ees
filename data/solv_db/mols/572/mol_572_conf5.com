%nproc=16
%mem=2GB
%chk=mol_572_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.20200       -0.75170        0.23640
O          0.95780       -1.19940       -0.16360
C          0.01390       -0.17200       -0.26520
C          0.00080        0.62120        0.98970
C         -0.28970       -0.13220        2.20310
N         -0.49850       -0.72060        3.17330
O          0.60520        0.68820       -1.24870
C         -0.13860        1.78920       -1.53780
O         -1.16630       -0.53500       -0.81360
C         -1.92120       -1.49860       -0.24270
H          2.24210       -0.27290        1.22450
H          2.60480        0.02780       -0.47540
H          2.96460       -1.56060        0.23570
H         -0.83470        1.38000        0.88420
H          0.91150        1.19440        1.09790
H          0.39950        2.38450       -2.34250
H         -1.14760        1.59810       -1.95500
H         -0.28330        2.46350       -0.64870
H         -2.24160       -1.23990        0.78240
H         -1.49030       -2.51980       -0.30830
H         -2.89030       -1.54400       -0.82550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

