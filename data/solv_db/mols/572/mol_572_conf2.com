%nproc=16
%mem=2GB
%chk=mol_572_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45190       -0.93540       -1.36850
O         -0.19540       -0.47760       -1.12030
C          0.02070        0.08260        0.10240
C         -0.36400       -0.89520        1.18590
C         -0.11230       -0.24590        2.47560
N          0.07350        0.28270        3.49990
O          1.33420        0.50700        0.22890
C          2.25300       -0.50640        0.09390
O         -0.77470        1.23110        0.31520
C         -0.46870        2.17440       -0.66010
H         -1.44410       -1.35890       -2.41680
H         -2.18340       -0.13210       -1.26730
H         -1.78840       -1.77800       -0.73600
H          0.18660       -1.85110        1.08780
H         -1.47560       -1.03540        1.13350
H          2.17720       -1.00070       -0.91320
H          3.26240       -0.06180        0.21100
H          2.16680       -1.29830        0.86880
H          0.59090        2.47570       -0.63390
H         -0.73050        1.74900       -1.63150
H         -1.07630        3.07460       -0.45490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

