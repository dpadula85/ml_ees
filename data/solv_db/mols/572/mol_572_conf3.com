%nproc=16
%mem=2GB
%chk=mol_572_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.77550       -1.17070        1.95230
O          1.21490       -0.68130        0.73060
C          0.13950       -0.28010       -0.08080
C          0.72010        0.04470       -1.41430
C          1.74930        1.06950       -1.42810
N          2.59300        1.87010       -1.44810
O         -0.68490       -1.43240       -0.29280
C         -1.76850       -1.14150       -1.07580
O         -0.64390        0.65500        0.43620
C         -0.24100        1.88360        0.79670
H          1.67140       -1.46190        2.52620
H          0.28630       -0.35430        2.53160
H          0.12990       -2.05550        1.87360
H          1.13490       -0.87510       -1.90770
H         -0.06530        0.43040       -2.13510
H         -2.35600       -0.33870       -0.54470
H         -1.58160       -0.89730       -2.11920
H         -2.50110       -2.00130       -1.10580
H         -1.17280        2.38150        1.25850
H          0.57040        1.84230        1.54810
H          0.03010        2.51300       -0.10140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

