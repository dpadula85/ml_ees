%nproc=16
%mem=2GB
%chk=mol_572_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.15310        2.02700        1.06260
O         -0.69310        1.15990        0.41150
C         -0.09280       -0.05150        0.08540
C          0.28410       -0.76460        1.32490
C         -0.93420       -1.00360        2.11970
N         -1.88560       -1.19310        2.76460
O         -1.08210       -0.80440       -0.56530
C         -1.53300       -0.16180       -1.71130
O          0.92020        0.20710       -0.79680
C          1.62760       -0.88460       -1.22940
H          1.06450        2.28240        0.44810
H         -0.35690        3.01910        1.17140
H          0.45560        1.74460        2.07230
H          1.00990       -0.19130        1.92670
H          0.74170       -1.76270        1.16320
H         -2.30710       -0.82250       -2.15890
H         -0.74580       -0.03580       -2.47910
H         -2.01890        0.79230       -1.45600
H          2.12960       -1.45160       -0.42650
H          0.91980       -1.58700       -1.71800
H          2.34350       -0.51770       -2.00910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_572_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

