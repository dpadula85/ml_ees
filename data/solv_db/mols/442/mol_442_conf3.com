%nproc=16
%mem=2GB
%chk=mol_442_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.45720       -1.00090        1.08790
N         -2.49060       -0.40850        0.20770
C         -2.89700        0.28900       -0.99560
C         -1.06850       -0.47030        0.46310
O         -0.73820       -1.07660        1.50560
N         -0.15330        0.12240       -0.42260
C          1.25160        0.15720       -0.33650
C          1.97950       -0.57440        0.56410
C          3.35830       -0.43050        0.75160
C          4.01870        0.49560       -0.02050
C          3.29940        1.23580       -0.93830
C          1.95030        1.08960       -1.11150
H         -3.09910       -0.98200        2.15110
H         -3.74310       -2.02890        0.79610
H         -4.39340       -0.38990        1.03320
H         -2.49860       -0.21620       -1.90110
H         -2.48500        1.33540       -0.98140
H         -4.00700        0.35360       -1.01740
H         -0.57130        0.58600       -1.29720
H          1.54130       -1.40220        1.12760
H          3.89480       -1.03540        1.48900
H          5.08330        0.66890        0.06190
H          3.81520        1.98480       -1.57120
H          1.40960        1.69730       -1.85250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

