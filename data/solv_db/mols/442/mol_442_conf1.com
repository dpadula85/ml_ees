%nproc=16
%mem=2GB
%chk=mol_442_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.79700       -0.69210       -1.06910
N         -2.51600        0.19460        0.04270
C         -3.60120        0.81960        0.77010
C         -1.15940        0.42810        0.39180
O         -0.94790        1.19110        1.34710
N         -0.11760       -0.18900       -0.32770
C          1.26090       -0.06260       -0.10170
C          2.15870       -0.49440       -1.09810
C          3.51790       -0.52490       -0.92490
C          4.05590       -0.12110        0.26360
C          3.20730        0.30230        1.25450
C          1.82490        0.32960        1.07900
H         -2.18910       -0.38120       -1.94590
H         -3.88450       -0.58700       -1.34430
H         -2.55620       -1.74630       -0.83260
H         -4.17250        1.49790        0.09560
H         -4.31450        0.05680        1.18240
H         -3.19000        1.44400        1.58780
H         -0.38840       -0.87110       -1.12180
H          1.72280       -0.79850       -2.05300
H          4.15400       -0.87480       -1.74650
H          5.12520       -0.13980        0.40610
H          3.59200        0.63380        2.21830
H          1.21450        0.58510        1.92660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

