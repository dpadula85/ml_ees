%nproc=16
%mem=2GB
%chk=mol_442_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.82770       -0.21570       -1.39100
N         -2.52950       -0.05010        0.01100
C         -3.58650       -0.01790        0.98660
C         -1.17470        0.08010        0.40740
O         -0.94610        0.22170        1.62380
N         -0.11630        0.05230       -0.52400
C          1.25770        0.17240       -0.27810
C          1.82890        0.54590        0.88950
C          3.22080        0.45180        1.07580
C          4.04250       -0.01410        0.07230
C          3.47160       -0.38610       -1.11860
C          2.11180       -0.28960       -1.27480
H         -3.90310       -0.25130       -1.60350
H         -2.32480       -1.11830       -1.80570
H         -2.33100        0.64990       -1.91630
H         -3.86700       -1.07080        1.27840
H         -3.19780        0.44090        1.92000
H         -4.49140        0.50390        0.63450
H         -0.45160       -0.05240       -1.54500
H          1.22920        1.03800        1.65180
H          3.62790        0.75960        2.03970
H          5.11830       -0.09190        0.20370
H          4.11530       -0.75370       -1.92290
H          1.72340       -0.60460       -2.25370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

