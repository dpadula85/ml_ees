%nproc=16
%mem=2GB
%chk=mol_442_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.86650        0.92840       -0.58140
N          2.51150       -0.28710        0.11420
C          3.57150       -1.13990        0.60250
C          1.13070       -0.57790        0.28060
O          0.83510       -1.62930        0.88230
N          0.13360        0.27670       -0.20890
C         -1.26690        0.11610       -0.11220
C         -2.04300        0.98280       -0.89720
C         -3.41190        1.03290       -0.84530
C         -4.05960        0.18890        0.02150
C         -3.33550       -0.67360        0.80960
C         -1.93470       -0.69760        0.73430
H          2.40810        1.78170       -0.05310
H          3.97110        1.03360       -0.67770
H          2.44300        0.81660       -1.62380
H          4.48320       -0.58900        0.86910
H          3.26060       -1.67820        1.54630
H          3.85270       -1.91440       -0.16740
H          0.52310        1.16990       -0.67800
H         -1.55820        1.65720       -1.59830
H         -4.01550        1.69690       -1.44550
H         -5.14390        0.18850        0.09830
H         -3.82140       -1.35310        1.50480
H         -1.40010       -1.33010        1.42520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

