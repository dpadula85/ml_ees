%nproc=16
%mem=2GB
%chk=mol_442_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.90590        0.93840       -0.99260
N          2.51730       -0.06770       -0.03420
C          3.55100       -0.78180        0.67070
C          1.14550       -0.39790        0.25720
O          0.88530       -1.28980        1.10610
N          0.08400        0.25910       -0.38940
C         -1.29290        0.03900       -0.21240
C         -1.83520       -1.05770        0.41590
C         -3.19300       -1.12910        0.73740
C         -4.05030       -0.10040        0.42400
C         -3.52970        0.99950       -0.21550
C         -2.20450        1.07170       -0.52400
H          3.97540        0.78150       -1.32130
H          2.90350        1.94360       -0.50330
H          2.23310        0.99440       -1.87190
H          4.48050       -0.84840        0.05570
H          3.16110       -1.76860        0.96560
H          3.83500       -0.17090        1.56240
H          0.35570        0.99590       -1.09330
H         -1.24490       -1.97940        0.53550
H         -3.59450       -2.01280        1.24750
H         -5.09730       -0.19050        0.68880
H         -4.21000        1.82230       -0.47320
H         -1.78130        1.94960       -1.03570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_442_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

