%nproc=16
%mem=2GB
%chk=mol_490_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.68860        0.46740        0.29480
C         -1.36150       -0.26720        0.04560
C         -0.35920        0.81100       -0.27990
O          0.91550        0.33330       -0.54280
C          1.66540       -0.35540        0.37640
O          1.24120       -0.59650        1.52770
C          3.01900       -0.80080       -0.07840
H         -2.74160        1.24340       -0.50130
H         -2.55170        0.95900        1.29350
H         -3.53860       -0.23250        0.27610
H         -1.47290       -0.89070       -0.88860
H         -1.07360       -0.87100        0.89990
H         -0.74870        1.30960       -1.20810
H         -0.38600        1.53410        0.58330
H          3.69650       -0.68820        0.81770
H          3.41440       -0.10530       -0.84650
H          2.97040       -1.85040       -0.41500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

