%nproc=16
%mem=2GB
%chk=mol_490_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.23030        0.73890       -0.48550
C         -1.86520       -0.19730        0.66220
C         -0.54430       -0.87540        0.38220
O          0.50430        0.06470        0.23420
C          1.79090       -0.36200       -0.02670
O          1.97490       -1.60360       -0.12320
C          2.93910        0.56130       -0.19410
H         -2.57840        0.16800       -1.35860
H         -3.06470        1.38490       -0.11740
H         -1.33850        1.33180       -0.77700
H         -2.64670       -0.96980        0.78800
H         -1.78860        0.37700        1.60700
H         -0.29900       -1.50780        1.25930
H         -0.58520       -1.51910       -0.51280
H          2.99350        0.86090       -1.26840
H          2.84840        1.46780        0.44020
H          3.88980        0.07970        0.05980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

