%nproc=16
%mem=2GB
%chk=mol_490_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.75510        0.05350        0.39240
C         -1.29350        0.07010        0.78650
C         -0.48530       -0.63630       -0.27250
O          0.87560       -0.68750       -0.02400
C          1.73370        0.35960        0.13110
O          1.25960        1.52560        0.04250
C          3.19300        0.13250        0.39830
H         -2.92450       -0.54570       -0.52450
H         -3.14120        1.10450        0.26070
H         -3.39860       -0.39700        1.17660
H         -1.21980       -0.52010        1.74210
H         -0.97430        1.09730        0.98010
H         -0.73240       -0.15870       -1.26460
H         -0.86300       -1.67630       -0.36350
H          3.59680       -0.67800       -0.23920
H          3.78160        1.05230        0.15200
H          3.34740       -0.09560        1.48470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

