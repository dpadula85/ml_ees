%nproc=16
%mem=2GB
%chk=mol_490_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99410        1.20330       -0.41850
C         -1.72160        0.06030        0.53780
C         -0.67130       -0.87820       -0.04820
O          0.50460       -0.11740       -0.24900
C          1.65080       -0.70070       -0.77770
O          1.57110       -1.93240       -1.06060
C          2.91010        0.07120       -1.00340
H         -2.60560        1.95860        0.13190
H         -1.06380        1.69010       -0.71030
H         -2.62680        0.87710       -1.27260
H         -2.65340       -0.47630        0.78070
H         -1.35660        0.52120        1.48350
H         -0.51840       -1.76160        0.59140
H         -1.07940       -1.24790       -1.01170
H          3.66070       -0.14700       -0.22500
H          3.30500       -0.26640       -2.00170
H          2.68870        1.14620       -1.12110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

