%nproc=16
%mem=2GB
%chk=mol_490_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.89570       -0.25550        0.35680
C         -1.58230        0.17110        1.00160
C         -0.47320       -0.16270        0.03230
O          0.79250        0.20170        0.55210
C          1.93040       -0.02750       -0.19550
O          1.78610       -0.56030       -1.32640
C          3.30390        0.32560        0.27630
H         -2.72710       -1.19950       -0.18180
H         -3.10880        0.52530       -0.42500
H         -3.70670       -0.36720        1.08240
H         -1.41600       -0.40080        1.93020
H         -1.54280        1.27550        1.15540
H         -0.54520       -1.23170       -0.22340
H         -0.63870        0.43000       -0.90430
H          3.72430        1.02100       -0.47770
H          3.89660       -0.61690        0.36630
H          3.20280        0.87200        1.21890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_490_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

