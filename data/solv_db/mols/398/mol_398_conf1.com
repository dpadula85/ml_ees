%nproc=16
%mem=2GB
%chk=mol_398_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.86950       -1.00740        0.01980
O         -1.71030       -1.94360        0.03820
N         -1.30130        0.27570       -0.00280
C         -0.44950        1.29350       -0.02280
O         -0.78600        2.50570       -0.04420
N          0.86810        1.01370       -0.01990
C          1.30570       -0.27370        0.00280
O          2.53160       -0.55090        0.00580
N          0.44330       -1.30580        0.02300
H         -2.33160        0.49620       -0.00510
H          1.57630        1.80220       -0.03540
H          0.72290       -2.30550        0.04060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_398_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_398_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_398_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

