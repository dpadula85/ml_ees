%nproc=16
%mem=2GB
%chk=mol_226_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.69290        0.23390       -0.22650
C         -2.46600       -1.00020        0.33040
C         -1.17060       -1.39020        0.58610
C         -0.11810       -0.55040        0.28570
C          1.16810       -0.96910        0.55350
C          2.21070       -0.12350        0.25000
C          1.97560        1.11140       -0.30780
C          0.70550        1.54280       -0.58010
C         -0.34920        0.70200       -0.27950
C         -1.64110        1.07260       -0.52640
O          3.50960       -0.55050        0.52210
H         -3.70320        0.59280       -0.45010
H         -3.32490       -1.62510        0.54890
H         -1.02500       -2.36670        1.02460
H          1.35230       -1.96200        1.00170
H          2.82770        1.77050       -0.54170
H          0.50900        2.52420       -1.02410
H         -1.79710        2.05190       -0.96660
H          4.02980       -1.06430       -0.20050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

