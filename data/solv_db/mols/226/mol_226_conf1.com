%nproc=16
%mem=2GB
%chk=mol_226_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.72600        0.19270        0.14760
C         -2.44210       -1.10640       -0.21660
C         -1.12570       -1.53600       -0.40290
C         -0.09430       -0.61510       -0.21110
C          1.21370       -1.00120       -0.38520
C          2.22770       -0.08700       -0.19420
C          1.94480        1.21030        0.16940
C          0.63960        1.60990        0.34710
C         -0.35990        0.68900        0.15350
C         -1.68180        1.07320        0.32790
O          3.55140       -0.45920       -0.36550
H         -3.73530        0.54690        0.29710
H         -3.24640       -1.81220       -0.36280
H         -0.88420       -2.54820       -0.68770
H          1.40730       -2.02920       -0.67180
H          2.73750        1.95010        0.32550
H          0.41780        2.63890        0.63540
H         -1.86220        2.10200        0.61400
H          4.01820       -0.81850        0.48030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

