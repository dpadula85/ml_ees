%nproc=16
%mem=2GB
%chk=mol_226_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.71760       -0.29490       -0.08390
C         -2.40380        1.04280       -0.21090
C         -1.09830        1.44460       -0.14520
C         -0.07730        0.54540        0.04580
C          1.22410        1.00930        0.10400
C          2.26720        0.12310        0.29550
C          1.98200       -1.20280        0.42360
C          0.67000       -1.67630        0.36550
C         -0.37880       -0.79920        0.17460
C         -1.68760       -1.20410        0.10900
O          3.57790        0.60680        0.35220
H         -3.76850       -0.61620       -0.13710
H         -3.21590        1.73990       -0.36080
H         -0.87100        2.51680       -0.24920
H          1.38240        2.07060       -0.00450
H          2.79390       -1.89580        0.57290
H          0.43160       -2.72530        0.46580
H         -1.90930       -2.25260        0.21060
H          3.79900        1.56810        0.26060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_226_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

