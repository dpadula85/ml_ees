%nproc=16
%mem=2GB
%chk=mol_492_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.34590        0.63800        0.04260
C         -1.20570       -0.73640        0.01340
C          0.05220       -1.33640       -0.02650
C          1.19460       -0.58110       -0.03810
C          1.06150        0.79130       -0.00910
C         -0.19250        1.39770        0.03090
Br         2.94710       -1.35610       -0.09230
H         -2.31560        1.11450        0.07350
H         -2.08130       -1.38410        0.02100
H          0.15530       -2.41690       -0.04950
H          1.97250        1.37480       -0.01860
H         -0.24220        2.49480        0.05290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_492_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_492_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_492_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

