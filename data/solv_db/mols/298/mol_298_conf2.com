%nproc=16
%mem=2GB
%chk=mol_298_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.30840        0.00850        0.10900
C         -1.93960        0.64930        0.10320
C         -0.87500       -0.43040       -0.01600
C          0.51820        0.20500       -0.02300
C          1.51310       -0.89970       -0.14160
C          2.90980       -0.45800       -0.16290
C          3.29320        0.81430       -0.08490
H         -3.20360       -1.08700       -0.08460
H         -3.71340        0.12940        1.13330
H         -3.98430        0.47100       -0.63910
H         -1.89580        1.30620       -0.78890
H         -1.76940        1.30340        0.98490
H         -1.05340       -0.95210       -0.97830
H         -0.96950       -1.17590        0.78670
H          0.64570        0.67090        0.98390
H          0.60800        0.98350       -0.77730
H          1.32370       -1.65240        0.63830
H          1.31320       -1.42250       -1.11890
H          3.68860       -1.20660       -0.24720
H          4.33360        1.11630       -0.10270
H          2.56540        1.62680        0.00230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

