%nproc=16
%mem=2GB
%chk=mol_298_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.62140        0.58150       -1.06050
C         -1.56880       -0.30320       -0.41060
C         -0.83150        0.43000        0.67970
C          0.18330       -0.47100        1.29950
C          1.20750       -0.97040        0.31610
C          1.90830        0.20520       -0.25650
C          3.20330        0.40140       -0.13220
H         -2.15410        1.47390       -1.49500
H         -3.07750       -0.03030       -1.88140
H         -3.40980        0.86900       -0.35300
H         -2.03850       -1.23110       -0.07120
H         -0.82760       -0.50850       -1.23380
H         -0.33540        1.31630        0.23730
H         -1.55230        0.84310        1.42450
H          0.75320        0.06160        2.11630
H         -0.30450       -1.37190        1.77080
H          0.72140       -1.54310       -0.48210
H          1.94590       -1.63880        0.83430
H          1.32060        0.90660       -0.83220
H          3.64920        1.27270       -0.57010
H          3.82860       -0.29310        0.39980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

