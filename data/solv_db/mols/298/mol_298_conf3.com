%nproc=16
%mem=2GB
%chk=mol_298_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.39450        0.64820        0.67600
C         -1.70190       -0.66490        1.02570
C         -0.99440       -1.24930       -0.16460
C          0.05140       -0.28930       -0.68030
C          1.09260        0.02260        0.35580
C          2.12720        0.96730       -0.16180
C          3.39790        0.58800       -0.19540
H         -1.67070        1.44910        0.49180
H         -3.11100        0.52570       -0.16100
H         -3.03480        0.88460        1.57630
H         -2.54150       -1.33310        1.36680
H         -1.01050       -0.44540        1.85410
H         -0.44400       -2.15400        0.22740
H         -1.69610       -1.52380       -0.96240
H         -0.42890        0.69830       -0.94190
H          0.55690       -0.67950       -1.59400
H          1.54150       -0.87040        0.81300
H          0.59530        0.58120        1.18610
H          1.80590        1.94020       -0.49670
H          3.71840       -0.37810        0.13660
H          4.14120        1.28250       -0.57360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

