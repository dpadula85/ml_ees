%nproc=16
%mem=2GB
%chk=mol_298_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.38220       -0.66320       -1.03710
C          1.91450       -0.75300        0.38120
C          0.47580       -0.41140        0.56550
C          0.15210        1.00090        0.12210
C         -1.32610        1.29910        0.33060
C         -2.17620        0.36790       -0.44700
C         -3.05840       -0.43480        0.13150
H          2.16620        0.30350       -1.52400
H          2.04800       -1.51700       -1.65860
H          3.50870       -0.74100       -1.00880
H          2.06310       -1.80340        0.71410
H          2.60150       -0.14060        0.99970
H         -0.12910       -1.16350        0.02230
H          0.22270       -0.47700        1.64330
H          0.75410        1.74880        0.66640
H          0.37510        1.09330       -0.94650
H         -1.53710        2.35440        0.09550
H         -1.52350        1.11550        1.40690
H         -2.08230        0.33490       -1.51220
H         -3.67960       -1.11350       -0.42200
H         -3.15150       -0.40010        1.20200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

