%nproc=16
%mem=2GB
%chk=mol_298_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.87850        0.51690       -0.85630
C          1.93490       -0.51210       -0.32150
C          0.52980        0.05700       -0.14730
C         -0.37590       -1.03590        0.39700
C         -1.77430       -0.47740        0.57260
C         -2.31530       -0.00040       -0.72280
C         -2.65380        1.26330       -0.84870
H          2.37120        1.14750       -1.62450
H          3.70720        0.00480       -1.38530
H          3.34080        1.13390       -0.06480
H          1.83030       -1.36790       -1.01610
H          2.29540       -0.85870        0.66460
H          0.61210        0.86830        0.61010
H          0.19050        0.42110       -1.12960
H         -0.02260       -1.31910        1.40620
H         -0.34460       -1.91240       -0.26960
H         -1.69710        0.35640        1.29880
H         -2.47460       -1.21440        0.97600
H         -2.43510       -0.66920       -1.55810
H         -2.54760        1.96250       -0.03230
H         -3.04980        1.63570       -1.78120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_298_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

