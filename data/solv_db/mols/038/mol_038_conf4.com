%nproc=16
%mem=2GB
%chk=mol_038_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.38600       -0.45520        0.35930
C         -0.03260        0.06030        0.24620
C         -0.09350        1.41250       -0.42640
C         -0.90010       -0.90290       -0.51770
O         -0.49000        0.24860        1.56280
H          1.73180       -0.54680        1.40000
H          1.40630       -1.49060       -0.06710
H          2.02970        0.21540       -0.22780
H         -0.00920        2.20790        0.32850
H         -1.08340        1.55150       -0.92240
H          0.67480        1.53170       -1.22030
H         -1.54840       -0.36330       -1.24440
H         -0.26420       -1.62310       -1.10440
H         -1.53420       -1.53550        0.12830
H         -1.27320       -0.31060        1.70520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

