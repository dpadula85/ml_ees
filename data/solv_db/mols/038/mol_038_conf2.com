%nproc=16
%mem=2GB
%chk=mol_038_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15870       -0.91850       -0.08280
C         -0.03760        0.01130        0.30540
C          1.31740       -0.55260       -0.10030
C         -0.24880        1.30400       -0.48320
O         -0.08570        0.22080        1.66970
H         -1.79540       -1.15860        0.78990
H         -0.70320       -1.82260       -0.54630
H         -1.82940       -0.49070       -0.85800
H          1.88530        0.16390       -0.74050
H          1.94760       -0.78390        0.79520
H          1.15910       -1.46130       -0.69580
H          0.50260        2.06250       -0.26040
H         -0.26280        0.98770       -1.55780
H         -1.29520        1.61940       -0.25070
H          0.60490        0.81850        2.01570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

