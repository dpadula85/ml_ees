%nproc=16
%mem=2GB
%chk=mol_038_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.70050       -1.19380       -0.50270
C         -0.00940       -0.06540        0.26290
C          1.45090       -0.01700       -0.14930
C         -0.74200        1.20430       -0.08290
O         -0.10090       -0.39180        1.61040
H         -1.77220       -0.95180       -0.62250
H         -0.21930       -1.23670       -1.48580
H         -0.62560       -2.12930        0.08220
H          2.12100        0.04390        0.74840
H          1.70540       -0.96750       -0.65010
H          1.66210        0.83830       -0.82800
H         -1.68810        1.21730        0.51350
H         -0.13810        2.11320        0.03920
H         -1.07960        1.19070       -1.15050
H          0.13630        0.34550        2.21530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

