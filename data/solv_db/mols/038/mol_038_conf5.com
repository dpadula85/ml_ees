%nproc=16
%mem=2GB
%chk=mol_038_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.25060       -0.69460        1.14220
C          0.26490        0.10790       -0.00940
C          0.30100       -0.76250       -1.25620
C         -0.66380        1.30030       -0.20510
O          1.53570        0.63300        0.21210
H         -1.17310       -0.24740        1.58820
H         -0.56470       -1.71250        0.79830
H          0.49160       -0.85510        1.93410
H          1.34590       -0.84360       -1.62630
H         -0.04820       -1.79840       -1.00270
H         -0.36220       -0.36860       -2.04970
H         -0.10600        2.22010       -0.42230
H         -1.43610        1.06220       -0.96850
H         -1.21750        1.49070        0.75570
H          1.88320        0.46860        1.10950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

