%nproc=16
%mem=2GB
%chk=mol_038_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.16990       -0.81800       -0.12090
C          0.00780        0.04910        0.23700
C         -1.32820       -0.57400       -0.16350
C          0.07590        1.38990       -0.47240
O         -0.08330        0.31990        1.59950
H          1.45660       -1.41500        0.77490
H          0.84470       -1.55030       -0.88740
H          2.06400       -0.23730       -0.47440
H         -1.27440       -0.95190       -1.20640
H         -2.06600        0.27690       -0.09700
H         -1.64000       -1.36370        0.53260
H         -0.93440        1.84950       -0.35050
H          0.87140        1.97020        0.03510
H          0.29270        1.28780       -1.54400
H          0.54330       -0.23320        2.13750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_038_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

