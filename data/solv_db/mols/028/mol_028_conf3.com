%nproc=16
%mem=2GB
%chk=mol_028_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.76230        0.52600        0.27190
C          2.29640        0.34910        0.09660
C          1.49420       -0.02900        1.14740
C          0.13170       -0.19160        0.97680
C         -0.48050        0.01500       -0.24030
C          0.32900        0.39410       -1.29090
C          1.70190        0.56080       -1.13020
C         -1.95890       -0.16630       -0.40570
C         -2.30000       -1.61110       -0.10020
C         -2.73160        0.72580        0.52720
H          4.20980        0.71770       -0.70310
H          4.21690       -0.36610        0.77890
H          3.91370        1.38660        0.94240
H          1.96730       -0.19390        2.10950
H         -0.49570       -0.49090        1.81310
H         -0.13010        0.56130       -2.25350
H          2.33420        0.86280       -1.97490
H         -2.27740        0.03890       -1.43670
H         -2.48770       -1.68810        0.99250
H         -1.41490       -2.27480       -0.30690
H         -3.18880       -1.89150       -0.67180
H         -3.79940        0.67200        0.21340
H         -2.70210        0.31960        1.57580
H         -2.39030        1.77360        0.49960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

