%nproc=16
%mem=2GB
%chk=mol_028_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.75250       -0.31560       -0.30450
C          2.29550       -0.05910       -0.19570
C          1.76200        1.13750        0.23520
C          0.38790        1.32220        0.31760
C         -0.48710        0.31870       -0.02740
C          0.06840       -0.87590       -0.45790
C          1.43560       -1.07300       -0.54470
C         -1.96050        0.50080        0.05540
C         -2.49700       -0.47690        1.08190
C         -2.57920        0.26210       -1.30490
H          4.00230       -1.11470        0.41890
H          4.35130        0.60190       -0.13110
H          3.92350       -0.74260       -1.31490
H          2.44380        1.95680        0.51700
H         -0.02770        2.27610        0.66110
H         -0.56860       -1.70000       -0.74220
H          1.85680       -2.00810       -0.88160
H         -2.15160        1.53020        0.42300
H         -2.61900       -1.46760        0.56360
H         -1.73420       -0.54530        1.88520
H         -3.47940       -0.18010        1.48080
H         -3.61770       -0.11170       -1.12550
H         -1.94410       -0.47680       -1.82820
H         -2.61360        1.24100       -1.84620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

