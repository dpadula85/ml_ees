%nproc=16
%mem=2GB
%chk=mol_028_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.75690        0.32920       -0.02010
C          2.29020        0.20610       -0.04230
C          1.62560        0.43710       -1.22280
C          0.25210        0.31530       -1.21570
C         -0.48020       -0.02420       -0.09160
C          0.21750       -0.25010        1.07980
C          1.58650       -0.13400        1.09470
C         -1.93190       -0.12220       -0.20880
C         -2.54430        0.95790        0.65790
C         -2.48940       -1.47730        0.12300
H          4.21870       -0.17410        0.85280
H          4.13620       -0.18310       -0.94660
H          4.03560        1.39580       -0.02700
H          2.17590        0.70940       -2.13750
H         -0.27620        0.50100       -2.16270
H         -0.34640       -0.51380        1.95470
H          2.09340       -0.32220        2.04420
H         -2.23610        0.10590       -1.24770
H         -2.34180        0.83560        1.72320
H         -2.11710        1.92840        0.28890
H         -3.65070        0.92400        0.44780
H         -3.59560       -1.39690        0.07300
H         -2.17590       -1.86010        1.09880
H         -2.20290       -2.18770       -0.68610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

