%nproc=16
%mem=2GB
%chk=mol_028_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.78260       -0.24370       -0.16810
C          2.30510       -0.08590       -0.16320
C          1.68290        0.91000       -0.90380
C          0.31110        1.06080       -0.90290
C         -0.50510        0.22820       -0.16540
C          0.12550       -0.76260        0.57040
C          1.49260       -0.91520        0.57130
C         -1.99160        0.38330       -0.15750
C         -2.67390       -0.89390       -0.61260
C         -2.39140        0.73850        1.26070
H          4.19820       -0.36690        0.84860
H          4.06770       -1.10460       -0.80660
H          4.23750        0.66390       -0.61200
H          2.33190        1.56560       -1.48490
H         -0.16680        1.84830       -1.48980
H         -0.52890       -1.42000        1.15350
H          1.95130       -1.69410        1.15310
H         -2.27600        1.22760       -0.80700
H         -1.92590       -1.65820       -0.89370
H         -3.33990       -1.27450        0.19850
H         -3.36500       -0.63770       -1.45630
H         -1.57670        1.38470        1.65880
H         -2.36930       -0.18910        1.90240
H         -3.37600        1.23530        1.30680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

