%nproc=16
%mem=2GB
%chk=mol_028_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.81020       -0.23270       -0.12660
C          2.32340       -0.09130       -0.13900
C          1.69720        0.77230       -1.01160
C          0.32480        0.92040       -1.04170
C         -0.49280        0.20400       -0.19160
C          0.13680       -0.65950        0.68060
C          1.50930       -0.80800        0.71120
C         -1.97620        0.33810       -0.19700
C         -2.65850       -0.96770       -0.52160
C         -2.45600        0.86840        1.14110
H          4.31000        0.70010       -0.46310
H          4.15160       -1.07500       -0.74310
H          4.14800       -0.38760        0.92640
H          2.32420        1.34690       -1.69100
H         -0.16600        1.60410       -1.73280
H         -0.48250       -1.23140        1.35620
H          1.97340       -1.49560        1.40940
H         -2.30790        1.07310       -0.95950
H         -1.94720       -1.68300       -0.96420
H         -3.04740       -1.38210        0.43300
H         -3.51220       -0.83630       -1.23490
H         -3.40540        1.39420        0.98460
H         -1.65140        1.56810        1.49500
H         -2.60520        0.06080        1.88040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_028_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

