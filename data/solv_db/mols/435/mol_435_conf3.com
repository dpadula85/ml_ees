%nproc=16
%mem=2GB
%chk=mol_435_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08430       -0.82430        0.39730
C         -0.65360       -0.58160        0.14770
C          0.23630       -1.63250        0.17880
C          1.58530       -1.36940       -0.06140
C          2.03030       -0.08260       -0.32640
C          1.11950        0.95330       -0.35210
C         -0.23260        0.68920       -0.11140
C         -1.22260        1.78140       -0.13440
O          3.38370        0.12630       -0.55880
H         -2.53860       -1.18370       -0.56720
H         -2.21920       -1.67160        1.13070
H         -2.64660        0.06970        0.70960
H         -0.12300       -2.63560        0.38730
H          2.27320       -2.22090       -0.03140
H          1.48580        1.93680       -0.55850
H         -2.16130        1.50600       -0.65510
H         -1.42890        2.19300        0.88930
H         -0.77990        2.61310       -0.72160
H          3.97650        0.33340        0.23750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

