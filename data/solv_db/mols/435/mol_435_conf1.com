%nproc=16
%mem=2GB
%chk=mol_435_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.76220        1.53440        0.17180
C         -0.47410        0.80450       -0.00610
C          0.72300        1.46220       -0.23360
C          1.92190        0.78480       -0.39920
C          1.94530       -0.60290       -0.33840
C          0.75500       -1.26280       -0.11200
C         -0.44530       -0.57380        0.05330
C         -1.67440       -1.38640        0.29330
O          3.15680       -1.26170       -0.50690
H         -1.57630        2.61900       -0.05420
H         -2.55400        1.16480       -0.50230
H         -2.15270        1.43160        1.20990
H          0.68590        2.54280       -0.27770
H          2.83940        1.33440       -0.57570
H          0.78330       -2.32780       -0.06710
H         -1.72100       -2.24820       -0.42510
H         -1.61600       -1.75760        1.33750
H         -2.58150       -0.79420        0.12220
H          3.74700       -1.46310        0.31020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

