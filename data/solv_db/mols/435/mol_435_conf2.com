%nproc=16
%mem=2GB
%chk=mol_435_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07370        1.04260        0.12920
C         -0.63750        0.65030       -0.04530
C          0.33630        1.57810       -0.36030
C          1.65120        1.17520       -0.51200
C          2.01200       -0.15330       -0.35230
C          1.05310       -1.09450       -0.03760
C         -0.24630       -0.65940        0.10620
C         -1.29170       -1.66870        0.44740
O          3.33780       -0.50120       -0.51490
H         -2.73700        0.53430       -0.58500
H         -2.12450        2.16210        0.04370
H         -2.42710        0.82000        1.17010
H          0.03540        2.61630       -0.48200
H          2.40770        1.92530       -0.76180
H          1.34150       -2.13780        0.08690
H         -1.37650       -1.81360        1.53640
H         -2.26280       -1.36000        0.00490
H         -1.00500       -2.60640       -0.09490
H          4.00710       -0.50940        0.22140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

