%nproc=16
%mem=2GB
%chk=mol_435_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.70130       -1.55130       -0.23090
C         -0.42430       -0.78310       -0.20200
C          0.79320       -1.34690       -0.52550
C          1.94810       -0.57850       -0.47970
C          1.94580        0.75220       -0.11930
C          0.72520        1.30710        0.20220
C         -0.42280        0.54780        0.15790
C         -1.74490        1.15620        0.50810
O          3.12750        1.49520       -0.08410
H         -2.55280       -0.95430       -0.59020
H         -1.52280       -2.47290       -0.83920
H         -1.96730       -1.93390        0.79700
H          0.79010       -2.39110       -0.80760
H          2.88890       -1.04480       -0.73800
H          0.68660        2.35300        0.49050
H         -2.32700        0.47710        1.14010
H         -1.56810        2.13800        0.96300
H         -2.32670        1.31790       -0.44230
H          3.65260        1.51240        0.80010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

