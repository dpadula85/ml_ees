%nproc=16
%mem=2GB
%chk=mol_435_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88560       -1.31540       -0.19270
C         -0.52540       -0.75570       -0.01050
C          0.59880       -1.53560        0.16990
C          1.84020       -0.95030        0.33410
C          2.02070        0.41630        0.32690
C          0.89620        1.19480        0.14640
C         -0.36230        0.61020       -0.02030
C         -1.56100        1.46980       -0.21330
O          3.28630        0.97520        0.49520
H         -1.87440       -2.42430       -0.17060
H         -2.31380       -1.04630       -1.19410
H         -2.59290       -0.98820        0.58000
H          0.45280       -2.60550        0.17500
H          2.69870       -1.59830        0.47310
H          1.00630        2.27400        0.13590
H         -1.35950        2.29890       -0.93090
H         -1.76140        1.95810        0.76330
H         -2.41710        0.87580       -0.51830
H          3.85370        1.14650       -0.34910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_435_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

