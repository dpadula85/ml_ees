%nproc=16
%mem=2GB
%chk=mol_432_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.33970        0.31830        0.65320
C         -0.01290        0.11980       -0.02650
O          1.07730        0.52920        0.79220
N          2.31350        0.01970        0.45240
O          3.27660        0.75130        0.12710
O          2.36450       -1.26540       -0.06290
O         -2.31840       -0.26750       -0.15680
H         -1.59970        1.38130        0.75070
H         -1.31960       -0.12950        1.66350
H         -0.04070        0.65910       -0.98690
H          0.12560       -0.96890       -0.18980
H         -2.52650       -1.14750        0.24370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

