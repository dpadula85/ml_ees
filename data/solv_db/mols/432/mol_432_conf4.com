%nproc=16
%mem=2GB
%chk=mol_432_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.41620        0.19350       -0.20510
C         -0.07230       -0.54220       -0.29630
O          0.92390        0.42350       -0.18290
N          2.25060        0.25040       -0.20670
O          3.00230        1.16170       -0.57400
O          2.80190       -0.93760        0.17480
O         -1.46930        0.84210        1.03470
H         -1.40730        0.99320       -0.96920
H         -2.26580       -0.48840       -0.34950
H         -0.08090       -1.07240       -1.27960
H         -0.04590       -1.27240        0.54960
H         -2.22100        0.44860        1.55710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

