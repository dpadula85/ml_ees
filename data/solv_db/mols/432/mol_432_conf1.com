%nproc=16
%mem=2GB
%chk=mol_432_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38980       -0.51620       -0.12990
C         -0.01530       -0.12120        0.35350
O          0.90890       -0.03370       -0.71570
N          2.19290        0.31850       -0.39570
O          3.10280       -0.52490       -0.42400
O          2.43210        1.62110       -0.04730
O         -1.91930        0.37630       -1.03080
H         -2.05170       -0.75350        0.72660
H         -1.24760       -1.48740       -0.68890
H         -0.00040        0.85090        0.86910
H          0.35400       -0.88900        1.05150
H         -2.36650        1.15920       -0.61820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

