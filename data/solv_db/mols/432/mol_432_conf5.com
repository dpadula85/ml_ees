%nproc=16
%mem=2GB
%chk=mol_432_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.36760        0.05810        0.09390
C         -0.12110       -0.80650       -0.16480
O          0.76570       -0.43000        0.85820
N          2.00120        0.03040        0.54980
O          1.95780        1.36920        0.62470
O          2.90240       -0.50960       -0.22320
O         -1.08240        1.37850       -0.30210
H         -2.30850       -0.33010       -0.16060
H         -1.37850        0.18160        1.23120
H          0.30830       -0.59730       -1.15160
H         -0.39440       -1.86620       -0.10480
H         -1.28300        1.52200       -1.25060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

