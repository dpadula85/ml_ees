%nproc=16
%mem=2GB
%chk=mol_432_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17320       -0.07060       -0.36940
C         -0.12840        0.94550        0.07390
O          0.89410        0.32570        0.81460
N          1.90180       -0.36850        0.17110
O          2.24400       -1.44770        0.71830
O          2.94490        0.34790       -0.38970
O         -1.58410       -0.87400        0.70690
H         -2.07610        0.43200       -0.73760
H         -0.68970       -0.73830       -1.10180
H          0.28650        1.40610       -0.85720
H         -0.61700        1.71110        0.70550
H         -2.00280       -1.66910        0.26540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_432_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

