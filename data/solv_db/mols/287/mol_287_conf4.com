%nproc=16
%mem=2GB
%chk=mol_287_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.10920       -0.36980       -0.51430
O          1.66840       -0.64710        0.76930
C          0.76260        0.32460        1.15480
C         -0.45420        0.43560        0.29960
O         -1.20230       -0.73390        0.25430
C         -2.30620       -0.47580       -0.58270
H          1.29600       -0.36430       -1.22970
H          2.95940       -1.03840       -0.81360
H          2.55520        0.65560       -0.49110
H          0.50200        0.12290        2.22800
H          1.23680        1.34910        1.17020
H         -0.20640        0.86290       -0.66970
H         -1.11670        1.19380        0.81910
H         -1.91970       -0.20640       -1.56680
H         -2.96590        0.28830       -0.12180
H         -2.91810       -1.39700       -0.70570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

