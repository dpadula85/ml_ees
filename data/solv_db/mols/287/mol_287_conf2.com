%nproc=16
%mem=2GB
%chk=mol_287_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.05070       -0.04250        0.99540
O         -0.79270        0.26660        0.52670
C         -0.60050       -0.03540       -0.78830
C          0.77440        0.30580       -1.28890
O          1.79750       -0.31440       -0.68270
C          2.04930       -0.17820        0.62870
H         -2.06550        0.25830        2.08450
H         -2.24960       -1.15120        0.91770
H         -2.85910        0.54830        0.52700
H         -0.73890       -1.12290       -0.99210
H         -1.39640        0.45760       -1.42260
H          0.83720        1.42440       -1.28980
H          0.73180        0.05970       -2.40120
H          1.27010       -0.52220        1.33210
H          2.95340       -0.80930        0.90480
H          2.33980        0.85540        0.94860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

