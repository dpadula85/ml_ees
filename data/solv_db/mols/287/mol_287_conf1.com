%nproc=16
%mem=2GB
%chk=mol_287_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.28400       -0.11990       -0.19550
O          1.70140        0.29830        0.97210
C          0.34850        0.49600        0.94390
C         -0.43120       -0.74500        0.59180
O         -1.78800       -0.59280        0.55640
C         -2.25650        0.33310       -0.35930
H          2.13830        0.58730       -1.03700
H          3.39530       -0.17320        0.01810
H          2.00050       -1.13350       -0.48060
H          0.03800        1.35590        0.31100
H          0.03930        0.76960        1.98850
H         -0.01300       -1.21130       -0.32810
H         -0.22080       -1.49580        1.40630
H         -1.91550        0.00350       -1.37110
H         -1.93250        1.33410       -0.06560
H         -3.38780        0.29380       -0.40650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

