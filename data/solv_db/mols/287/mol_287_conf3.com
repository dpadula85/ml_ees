%nproc=16
%mem=2GB
%chk=mol_287_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.89680       -0.00320       -0.18280
O         -1.65620        0.42170        0.27570
C         -0.65870       -0.33160       -0.38250
C          0.66250        0.17330        0.15490
O          1.74440       -0.48730       -0.41230
C          2.89590        0.07880        0.17430
H         -3.66740        0.59590        0.34170
H         -2.92810        0.16710       -1.26690
H         -3.00940       -1.09310       -0.01680
H         -0.85380       -1.38800       -0.11300
H         -0.71080       -0.22290       -1.46250
H          0.74230        1.23780       -0.16540
H          0.72720        0.11120        1.24020
H          2.94050        1.17690       -0.04270
H          2.85050       -0.00730        1.28030
H          3.81800       -0.42930       -0.15450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

