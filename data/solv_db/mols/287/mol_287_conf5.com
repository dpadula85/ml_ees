%nproc=16
%mem=2GB
%chk=mol_287_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.62110        0.21160        0.27710
O          1.57190       -0.64370        0.01920
C          0.38490        0.04200       -0.25610
C         -0.71390       -0.94750       -0.52710
O         -1.92630       -0.38390       -0.81440
C         -2.40750        0.39910        0.20740
H          2.45020        0.86950        1.13720
H          2.85420        0.85740       -0.59590
H          3.50930       -0.41290        0.50280
H          0.16610        0.66230        0.65080
H          0.47410        0.74050       -1.09790
H         -0.86680       -1.64240        0.34300
H         -0.39980       -1.63560       -1.35850
H         -1.73340        1.23690        0.43230
H         -2.58880       -0.20360        1.13940
H         -3.39530        0.85030       -0.08200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_287_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

