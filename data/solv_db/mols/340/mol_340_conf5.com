%nproc=16
%mem=2GB
%chk=mol_340_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.12880        0.72160       -0.18440
C         -1.69450       -0.70510       -0.37950
C         -0.43870       -1.01970        0.43710
C          0.66790       -0.12430        0.00770
O          0.54090        0.74300       -0.89340
O          1.89470       -0.26810        0.64540
C          3.02550        0.53010        0.32130
H         -1.71590        1.13780        0.77500
H         -1.83670        1.39430       -0.99840
H         -3.24880        0.78870       -0.05480
H         -2.52020       -1.35080       -0.01260
H         -1.54200       -0.98090       -1.44270
H         -0.65160       -0.95310        1.51630
H         -0.17510       -2.08430        0.23990
H          2.99810        1.38960        1.03480
H          3.92660       -0.10410        0.45860
H          2.89860        0.88520       -0.70950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

