%nproc=16
%mem=2GB
%chk=mol_340_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.12080        0.06800       -0.94190
C          1.80040       -0.39950        0.44770
C          0.54300        0.23000        0.99950
C         -0.64450       -0.08180        0.16650
O         -0.60160       -0.78160       -0.86580
O         -1.87710        0.42610        0.54410
C         -3.06700        0.18790       -0.17980
H          1.42490        0.87790       -1.26500
H          3.15220        0.48120       -1.02770
H          2.09780       -0.79780       -1.66460
H          1.64690       -1.50920        0.47930
H          2.66700       -0.15360        1.09480
H          0.36010       -0.26340        1.99140
H          0.63230        1.31570        1.12320
H         -3.13790        0.81880       -1.09450
H         -3.17930       -0.85640       -0.49230
H         -3.93800        0.43760        0.46160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

