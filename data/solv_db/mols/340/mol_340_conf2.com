%nproc=16
%mem=2GB
%chk=mol_340_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.55290        1.08180       -0.03620
C         -1.71290       -0.35470        0.42990
C         -0.73460       -1.26770       -0.24680
C          0.67710       -0.87690        0.01800
O          1.48050       -1.59600        0.70470
O          1.21170        0.33170       -0.46210
C          2.54580        0.70380       -0.19240
H         -0.78220        1.63000        0.52140
H         -1.26330        1.13680       -1.10540
H         -2.51610        1.62230        0.04690
H         -2.72390       -0.70220        0.20140
H         -1.54180       -0.41660        1.52780
H         -0.89800       -1.43930       -1.30650
H         -0.86950       -2.28040        0.23970
H          3.12090       -0.14880        0.27060
H          3.05910        1.03620       -1.13530
H          2.50020        1.54010        0.52440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

