%nproc=16
%mem=2GB
%chk=mol_340_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.65040        0.24890       -0.62170
C         -1.42680       -0.96240        0.25690
C         -0.60620       -0.53500        1.45060
C          0.71630        0.01820        1.03600
O          1.47080        0.40200        1.97440
O          1.10390        0.11000       -0.29440
C          2.40600        0.66500       -0.52470
H         -2.74200        0.53010       -0.67150
H         -1.38490        0.00400       -1.68200
H         -1.12960        1.15180       -0.25150
H         -2.41850       -1.27430        0.64410
H         -0.97990       -1.78010       -0.30420
H         -0.39870       -1.36460        2.14440
H         -1.12240        0.26040        2.02450
H          3.15940        0.14250        0.08180
H          2.34780        1.70610       -0.10780
H          2.65510        0.67740       -1.60600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

