%nproc=16
%mem=2GB
%chk=mol_340_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.75180       -0.40240       -0.31540
C         -1.34360       -0.42150        0.29170
C         -0.53180        0.52170       -0.55860
C          0.86190        0.64900       -0.09760
O          1.28920        1.68900        0.52120
O          1.84560       -0.33920       -0.29140
C          3.18020       -0.16870        0.14920
H         -3.46880       -0.77390        0.45910
H         -3.02010        0.65990       -0.58940
H         -2.80470       -0.98720       -1.24770
H         -0.95230       -1.46420        0.15400
H         -1.43090       -0.15720        1.33720
H         -0.98630        1.52350       -0.62000
H         -0.57910        0.13040       -1.62360
H          3.58970        0.80840       -0.19160
H          3.78200       -0.95850       -0.36180
H          3.32090       -0.30930        1.21540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_340_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

