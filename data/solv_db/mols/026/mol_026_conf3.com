%nproc=16
%mem=2GB
%chk=mol_026_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97400       -1.41220        0.21190
C         -2.93210       -0.39710        0.71930
C         -2.73060        0.88680        0.59180
C         -1.55570        1.50860       -0.06640
C         -0.65230        0.46170       -0.57290
C         -0.62580       -0.77810        0.33800
C          0.46460       -1.50590       -0.33900
O          0.52900       -2.71660       -0.63560
N          1.45750       -0.47250       -0.59830
C          0.80290        0.78840       -0.54940
O          1.36970        1.88780       -0.49450
S          3.13740       -0.85830       -0.86980
C          4.30400        0.15320        0.02660
Cl         5.95440       -0.54160       -0.19280
Cl         3.91090        0.36850        1.72190
Cl         4.40520        1.78560       -0.73030
H         -2.05610       -2.37680        0.72050
H         -2.19030       -1.56350       -0.85720
H         -3.83110       -0.74810        1.19750
H         -3.47330        1.58240        1.03040
H         -1.97860        2.04940       -0.96510
H         -1.07910        2.30250        0.53630
H         -0.88360        0.09040       -1.59410
H         -0.37320       -0.49460        1.37130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

