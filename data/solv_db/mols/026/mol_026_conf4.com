%nproc=16
%mem=2GB
%chk=mol_026_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08860       -1.20280       -0.04320
C         -2.89230       -0.03690       -0.51400
C         -2.61900        1.22880       -0.26590
C         -1.40870        1.56240        0.53450
C         -0.39660        0.54580        0.10550
C         -0.87580       -0.80240        0.68170
C          0.32390       -1.60570        0.41910
O          0.48750       -2.80550        0.09860
N          1.44200       -0.70450        0.61680
C          0.91450        0.61950        0.77880
O          1.49940        1.54480        1.37990
S          3.13070       -1.20500        0.56780
C          4.23840       -0.02120       -0.18660
Cl         4.49010        1.45150        0.75880
Cl         3.69590        0.45550       -1.82060
Cl         5.81520       -0.81060       -0.40310
H         -2.76270       -1.74010        0.69630
H         -1.92660       -1.91580       -0.88180
H         -3.75750       -0.29230       -1.15610
H         -3.25840        2.00200       -0.61820
H         -1.06550        2.59750        0.42900
H         -1.67520        1.31610        1.59590
H         -0.33260        0.49870       -0.98390
H         -0.97820       -0.67990        1.77540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

