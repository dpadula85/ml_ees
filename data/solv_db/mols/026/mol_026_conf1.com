%nproc=16
%mem=2GB
%chk=mol_026_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31650       -0.27560       -0.88880
C         -3.05170       -0.28130        0.39540
C         -2.50730        0.07860        1.54140
C         -1.08260        0.53580        1.66470
C         -0.40010        0.51480        0.35890
C         -0.91140       -0.62300       -0.54040
C          0.05620       -0.54420       -1.62010
O         -0.03110       -0.67400       -2.86180
N          1.31910       -0.25080       -0.95610
C          1.03970        0.13280        0.38090
O          1.75730        0.17250        1.40030
S          2.89660       -0.29910       -1.72830
C          4.22850        0.07390       -0.60760
Cl         5.79560        0.05940       -1.48920
Cl         4.42140       -1.13680        0.69790
Cl         4.03740        1.71560        0.02640
H         -2.37370        0.75380       -1.28430
H         -2.69030       -0.96390       -1.63770
H         -4.07710       -0.59540        0.39480
H         -3.08400        0.05430        2.45010
H         -0.58050        0.01860        2.49800
H         -1.15920        1.61550        1.99410
H         -0.45660        1.48330       -0.19240
H         -0.82980       -1.56490        0.00410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

