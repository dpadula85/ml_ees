%nproc=16
%mem=2GB
%chk=mol_026_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31310        0.02130       -0.93590
C         -3.10750       -0.11700        0.31260
C         -2.55140        0.04560        1.50150
C         -1.11410        0.37180        1.72770
C         -0.41790        0.59360        0.43820
C         -0.93350       -0.45360       -0.55870
C          0.08510       -0.32460       -1.60210
O         -0.01870       -0.32610       -2.85240
N          1.33370       -0.18460       -0.89380
C          1.03060        0.26390        0.42840
O          1.83400        0.35440        1.36900
S          2.87310       -0.50280       -1.67170
C          4.29550       -0.31450       -0.61950
Cl         4.50090        1.39420       -0.11230
Cl         4.30200       -1.41980        0.76810
Cl         5.78060       -0.70900       -1.56480
H         -2.72800       -0.52790       -1.78480
H         -2.21830        1.12060       -1.13940
H         -4.14990       -0.37520        0.25260
H         -3.15430       -0.04270        2.43080
H         -0.60820       -0.37220        2.36870
H         -1.13170        1.36540        2.27030
H         -0.61720        1.59150       -0.01240
H         -0.97160       -1.45210       -0.12000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

