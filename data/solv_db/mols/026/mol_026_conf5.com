%nproc=16
%mem=2GB
%chk=mol_026_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.60150       -0.13540       -1.32820
C         -2.91180       -0.20790       -0.62300
C         -3.03770       -0.14060        0.69590
C         -1.87330        0.01160        1.61990
C         -0.68100        0.41970        0.81360
C         -0.59890       -0.59890       -0.33800
C          0.79930       -0.53930       -0.69680
O          1.30520       -0.66760       -1.82360
N          1.52810       -0.29910        0.50000
C          0.62050        0.24440        1.44830
O          0.89180        0.52710        2.63250
S          3.21350       -0.62480        0.87030
C          4.36480        0.44790        0.03870
Cl         4.32040        0.15740       -1.73350
Cl         4.14740        2.15900        0.35770
Cl         6.02880       -0.03780        0.53200
H         -1.41670        0.96080       -1.50630
H         -1.60150       -0.69690       -2.28060
H         -3.80060       -0.33520       -1.21720
H         -4.01260       -0.21920        1.17760
H         -1.71270       -0.98620        2.10860
H         -2.17300        0.71270        2.40820
H         -0.86350        1.43770        0.42460
H         -0.93490       -1.58950        0.04880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_026_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

