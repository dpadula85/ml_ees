%nproc=16
%mem=2GB
%chk=mol_370_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.94410       -0.17120        0.34520
O          0.64310       -0.65690        0.16300
C         -0.19260        0.35390       -0.30540
C         -1.57830       -0.25340       -0.47590
O         -1.97950       -0.71580        0.77440
H          2.62290       -0.96520        0.71840
H          1.95260        0.70830        1.05220
H          2.27660        0.24080       -0.63220
H          0.17780        0.71400       -1.29970
H         -0.21320        1.24090        0.34960
H         -1.55390       -1.05820       -1.23750
H         -2.29900        0.51630       -0.86530
H         -1.80060        0.04650        1.41320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

