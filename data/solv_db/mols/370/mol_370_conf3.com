%nproc=16
%mem=2GB
%chk=mol_370_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.07130       -0.01780        0.26520
O          0.84860       -0.66160        0.46630
C         -0.16380        0.05330       -0.15670
C         -1.51350       -0.56090        0.00420
O         -2.43130        0.25940       -0.66950
H          2.00140        1.01340        0.70590
H          2.26650        0.13380       -0.83100
H          2.90250       -0.53330        0.75220
H         -0.20530        1.05250        0.35890
H          0.04980        0.27380       -1.22080
H         -1.60450       -1.57280       -0.42270
H         -1.80000       -0.62750        1.06350
H         -2.42150        1.18760       -0.31560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

