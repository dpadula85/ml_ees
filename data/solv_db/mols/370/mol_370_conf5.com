%nproc=16
%mem=2GB
%chk=mol_370_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.70430        0.39950       -0.05070
O         -0.90420       -0.07840       -1.05970
C          0.23280       -0.71670       -0.63680
C          1.14750        0.17210        0.18890
O          2.24590       -0.63350        0.53200
H         -1.15470        1.12970        0.56770
H         -2.59140        0.88240       -0.52780
H         -2.07320       -0.39060        0.64100
H         -0.01150       -1.60420        0.00050
H          0.81690       -1.01020       -1.53380
H          0.65860        0.41970        1.17480
H          1.47320        1.06430       -0.35080
H          1.86440       -1.38520        1.05480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

