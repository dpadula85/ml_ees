%nproc=16
%mem=2GB
%chk=mol_370_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.36740       -0.53010       -0.76890
O          0.63580        0.62900       -0.78540
C         -0.05450        0.90280        0.36290
C         -1.06300       -0.14100        0.75020
O         -2.00740       -0.27490       -0.26700
H          2.13910       -0.49540        0.05680
H          0.76110       -1.43980       -0.51180
H          1.88150       -0.63360       -1.73700
H         -0.58170        1.86970        0.22410
H          0.67810        1.05810        1.18070
H         -1.58560        0.21280        1.65750
H         -0.63670       -1.12390        0.94220
H         -1.53410       -0.03380       -1.10460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

