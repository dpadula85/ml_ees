%nproc=16
%mem=2GB
%chk=mol_370_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95090       -0.07100        0.39640
O          0.62470       -0.41590        0.44310
C         -0.15280        0.27950       -0.47680
C         -1.61140       -0.12320       -0.40080
O         -2.15100        0.11960        0.84430
H          2.43670       -0.25360       -0.58340
H          2.04960        0.98570        0.71540
H          2.49860       -0.67880        1.13570
H          0.26790        0.05890       -1.47850
H         -0.09000        1.35140       -0.22570
H         -1.77260       -1.16990       -0.72520
H         -2.15060        0.51160       -1.14570
H         -1.90010       -0.59450        1.50110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_370_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

