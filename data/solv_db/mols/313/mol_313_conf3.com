%nproc=16
%mem=2GB
%chk=mol_313_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.13970        0.07110       -0.36250
C         -3.92120       -1.27710       -0.17330
C         -2.60950       -1.72540       -0.01730
C         -1.55530       -0.84280       -0.05140
C         -1.81920        0.49380       -0.24260
C         -3.11820        0.97060       -0.40120
C         -0.68460        1.40120       -0.27390
O         -0.84720        2.64300       -0.44540
C          0.70150        0.94850       -0.11200
C          1.74240        1.85640       -0.15030
C          3.03440        1.39760        0.00550
C          3.28220        0.05100        0.19680
C          2.21640       -0.82490        0.22910
C          0.90150       -0.40430        0.07650
C         -0.21160       -1.36470        0.11620
O          0.01760       -2.58300        0.28970
N          4.61380       -0.41250        0.35610
H         -5.14260        0.42200       -0.48290
H         -4.71730       -2.00120       -0.14010
H         -2.46870       -2.79880        0.13090
H         -3.28170        2.03440       -0.54980
H          1.53590        2.90720       -0.30060
H          3.87400        2.10620       -0.02240
H          2.34830       -1.89450        0.37620
H          4.85820       -1.42040        0.27030
H          5.39070        0.24650        0.56480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

