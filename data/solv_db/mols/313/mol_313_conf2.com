%nproc=16
%mem=2GB
%chk=mol_313_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.15140       -0.27720        0.09310
C          4.01440        1.07690        0.17720
C          2.71000        1.58220        0.13150
C          1.62340        0.74510        0.00750
C          1.77260       -0.61830       -0.07650
C          3.06180       -1.11700       -0.03130
C          0.61260       -1.48200       -0.20670
O          0.77970       -2.74830       -0.28300
C         -0.74090       -0.93880       -0.25270
C         -1.84540       -1.75560       -0.37630
C         -3.12190       -1.23640       -0.41910
C         -3.29930        0.12930       -0.33670
C         -2.18240        0.93660       -0.21300
C         -0.89770        0.43740       -0.16830
C          0.27740        1.29630       -0.03750
O          0.09070        2.54380        0.03630
N         -4.59670        0.71020       -0.37680
H          5.17370       -0.67090        0.12900
H          4.86590        1.74460        0.27520
H          2.53250        2.64030        0.19300
H          3.19000       -2.18140       -0.09620
H         -1.73670       -2.83700       -0.44350
H         -3.97260       -1.89640       -0.51630
H         -2.28630        2.01520       -0.14570
H         -5.35080        0.37860       -1.01190
H         -4.82530        1.52280        0.26570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

