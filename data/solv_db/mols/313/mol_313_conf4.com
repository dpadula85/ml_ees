%nproc=16
%mem=2GB
%chk=mol_313_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.11710        0.55300       -0.25680
C         -4.06630       -0.80660       -0.02990
C         -2.82820       -1.40640        0.13500
C         -1.66510       -0.63830        0.07030
C         -1.72250        0.72100       -0.15690
C         -2.96170        1.30770       -0.31970
C         -0.51240        1.52840       -0.22550
O         -0.55180        2.78160       -0.43430
C          0.78550        0.85510       -0.04520
C          1.95890        1.58160       -0.10240
C          3.18900        0.97870        0.06260
C          3.29770       -0.37770        0.29210
C          2.11490       -1.08630        0.34580
C          0.87600       -0.49510        0.18230
C         -0.36150       -1.27090        0.24420
O         -0.26820       -2.50730        0.45300
N          4.55360       -0.98880        0.46000
H         -5.06600        1.07520       -0.39340
H         -4.98050       -1.38260        0.01620
H         -2.75580       -2.45830        0.31220
H         -3.02540        2.38460       -0.50070
H          1.84990        2.64920       -0.28420
H          4.08490        1.61300        0.00580
H          2.11990       -2.17380        0.52550
H          5.15610       -1.29690       -0.33850
H          4.89630       -1.13990        1.43400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

