%nproc=16
%mem=2GB
%chk=mol_313_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.14300       -0.52840       -0.32870
C         -4.08350        0.54240        0.51110
C         -2.84800        1.06440        0.89540
C         -1.66370        0.51080        0.43570
C         -1.76160       -0.57470       -0.41490
C         -2.99730       -1.09750       -0.79980
C         -0.51880       -1.15900       -0.89970
O         -0.57180       -2.14560       -1.67360
C          0.80020       -0.65170       -0.52850
C          1.96650       -1.22320       -1.00180
C          3.21730       -0.75320       -0.65860
C          3.28680        0.32970        0.19070
C          2.12630        0.92120        0.67960
C          0.88400        0.43610        0.32430
C         -0.35610        1.03110        0.81740
O         -0.28130        2.01880        1.59170
N          4.57820        0.80920        0.54030
H         -5.09790       -0.92920       -0.62360
H         -4.97590        1.02280        0.90710
H         -2.79910        1.91650        1.56350
H         -3.05150       -1.95350       -1.47110
H          1.88590       -2.07420       -1.66880
H          4.13080       -1.22810       -1.05090
H          2.20600        1.75860        1.33590
H          5.09010        1.39260       -0.15860
H          4.97710        0.56400        1.48570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_313_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

