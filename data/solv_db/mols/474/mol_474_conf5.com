%nproc=16
%mem=2GB
%chk=mol_474_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07280       -2.84930        1.00310
C         -1.63440       -1.89820       -0.08570
O         -1.33240       -0.63240        0.41190
P         -0.65670        0.30160       -0.81100
S          0.09490       -0.80960       -1.91840
O         -1.77800        1.17030       -1.68790
C         -2.73680        1.81630       -0.94540
C         -2.18190        2.75520        0.09110
O          0.61250        1.28920       -0.19480
C          1.86220        0.78690        0.11870
C          2.81670        0.64130       -0.88140
C          3.91190       -0.14440       -0.64430
C          4.17110       -0.52960        0.64890
C          3.17370       -0.50040        1.59190
N          1.98900        0.05110        1.24680
Cl         3.54220       -0.75950        3.28190
Cl         5.74190       -1.13880        1.15140
Cl         2.69180        1.60990       -2.34130
H         -1.51160       -2.69760        1.94540
H         -1.86920       -3.90840        0.70350
H         -3.17560       -2.81360        1.17780
H         -0.79390       -2.37580       -0.60680
H         -2.48930       -1.80720       -0.78110
H         -3.50750        1.13450       -0.51090
H         -3.30510        2.45460       -1.67020
H         -1.76200        3.64600       -0.41920
H         -3.05820        3.12610        0.67960
H         -1.48160        2.27350        0.78850
H          4.73910       -0.19150       -1.34200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

