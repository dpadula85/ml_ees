%nproc=16
%mem=2GB
%chk=mol_474_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.03030        2.18300       -0.06200
C         -2.21090        2.13640        1.24790
O         -0.94720        1.66870        0.83830
P         -0.70160        0.11700        1.09120
S         -0.15170       -0.37770        2.69480
O         -2.02020       -0.96180        0.78190
C         -1.90320       -1.43510       -0.53660
C         -1.83510       -2.95900       -0.42320
O          0.53080       -0.52390        0.07290
C          1.81540       -0.12400       -0.02730
C          2.60170       -0.97120       -0.83300
C          3.88100       -0.66610       -1.20020
C          4.38730        0.53570       -0.70700
C          3.65410        1.31530        0.15720
N          2.36730        0.99760        0.45330
Cl         4.52220        2.50250        1.12990
Cl         5.72200        1.30360       -1.56400
Cl         2.17400       -2.68340       -0.69490
H         -3.36970        3.13210       -0.37160
H         -2.30890        1.74810       -0.81960
H         -3.84650        1.44040        0.13000
H         -2.03420        3.22970        1.49390
H         -2.67660        1.69010        2.07910
H         -1.13810       -0.97300       -1.08810
H         -2.91210       -1.24410       -1.06370
H         -1.54500       -3.36500       -1.36340
H         -2.65590       -3.35190        0.12740
H         -0.86850       -3.04130        0.25870
H          4.50010       -1.32240       -1.80190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

