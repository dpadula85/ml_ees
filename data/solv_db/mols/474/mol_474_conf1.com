%nproc=16
%mem=2GB
%chk=mol_474_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.10030       -3.02010        0.21990
C         -2.65740       -1.69650       -0.37770
O         -1.27330       -1.63850       -0.25090
P         -0.64030       -0.20530       -0.88080
S         -1.54410        0.24670       -2.30660
O         -0.76600        0.99080        0.30340
C         -1.50410        2.07440       -0.17710
C         -1.64630        3.16460        0.83350
O          0.96090       -0.46520       -1.36900
C          2.06790       -0.29130       -0.58350
C          3.30640       -0.55550       -1.16190
C          4.41470       -0.37690       -0.36090
C          4.27640        0.04460        0.94890
C          3.03540        0.29890        1.49540
N          1.96330        0.11780        0.69430
Cl         2.92770        0.82560        3.14600
Cl         5.71220        0.25910        1.93270
Cl         3.40990       -1.08690       -2.82810
H         -4.06140       -3.36780       -0.23960
H         -2.31020       -3.78660       -0.01330
H         -3.16120       -2.98010        1.32790
H         -3.09400       -0.85950        0.21160
H         -3.00220       -1.63820       -1.41180
H         -0.98150        2.41790       -1.10450
H         -2.51460        1.68230       -0.42790
H         -0.85700        3.17620        1.59660
H         -2.65920        3.11340        1.32580
H         -1.67670        4.13670        0.26320
H          5.37510       -0.58050       -0.80550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

