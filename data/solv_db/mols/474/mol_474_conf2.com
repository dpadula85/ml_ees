%nproc=16
%mem=2GB
%chk=mol_474_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.95020       -2.94610       -0.06130
C         -1.89390       -1.91790        0.14360
O         -2.12630       -0.72430       -0.59530
P         -0.70600        0.15550       -0.35860
S         -0.37760       -0.00110        1.37880
O         -0.79340        1.77100       -0.74940
C         -2.04330        2.31890       -0.50740
C         -1.88290        3.07950        0.80390
O          0.54720       -0.58870       -1.24160
C          1.81090       -0.50360       -0.65370
C          2.86050       -0.06950       -1.45630
C          4.12940       -0.12190       -0.95750
C          4.34020       -0.37890        0.37750
C          3.26610       -0.64770        1.18840
N          2.05550       -0.76610        0.63400
Cl         3.37550       -0.32990        2.91040
Cl         5.94490       -0.41680        1.05410
Cl         2.46190        1.02300       -2.80890
H         -2.68200       -3.64650       -0.88690
H         -3.03630       -3.54760        0.87200
H         -3.90000       -2.43430       -0.29650
H         -1.83280       -1.59330        1.22950
H         -0.86870       -2.23470       -0.12770
H         -2.23730        3.13170       -1.28020
H         -2.88290        1.67700       -0.47510
H         -2.66550        3.81820        0.94300
H         -1.98650        2.32200        1.65030
H         -0.86780        3.48890        0.91050
H          4.94140        0.08320       -1.63950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

