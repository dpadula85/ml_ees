%nproc=16
%mem=2GB
%chk=mol_474_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.89020       -3.66240        0.23700
C         -1.98940       -2.59550       -0.32690
O         -1.79000       -1.64970        0.67350
P         -0.77120       -0.39180        0.11670
S         -0.63020       -0.44890       -1.60090
O         -1.45530        1.04550        0.61160
C         -1.53930        2.00320       -0.37260
C         -2.17860        3.22920        0.24640
O          0.72290       -0.61430        0.89050
C          1.93220       -0.18860        0.42900
C          2.43140        1.01530        0.87360
C          3.66270        1.43450        0.39510
C          4.34730        0.66270       -0.49150
C          3.83130       -0.52750       -0.91770
N          2.64600       -0.91480       -0.44710
Cl         4.67220       -1.55820       -2.05830
Cl         5.90390        1.15890       -1.11620
Cl         1.56450        2.03050        2.02140
H         -2.71390       -3.78160        1.33060
H         -3.93350       -3.30220        0.13770
H         -2.77930       -4.63810       -0.26290
H         -1.02510       -3.03670       -0.58140
H         -2.41730       -2.14270       -1.25130
H         -2.11710        1.65350       -1.27340
H         -0.51960        2.33640       -0.72320
H         -1.85720        4.16560       -0.24840
H         -1.89080        3.26040        1.31420
H         -3.28210        3.07680        0.21600
H          4.06590        2.38080        0.73800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_474_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

