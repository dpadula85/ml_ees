%nproc=16
%mem=2GB
%chk=mol_100_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.04760        0.01840       -0.12030
O          1.13180       -0.11580       -0.47320
H         -0.73430       -0.70120       -0.55450
H         -0.35000        0.79850        0.56820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_100_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_100_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_100_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

