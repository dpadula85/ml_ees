%nproc=16
%mem=2GB
%chk=mol_151_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.36910       -0.20740       -0.28890
C          1.95210        0.31550       -0.11280
O          1.00190       -0.70480       -0.34500
C         -0.33990       -0.38570       -0.22320
C         -0.75510        0.88550        0.11410
C         -2.09960        1.17050        0.22720
C         -3.08780        0.23310        0.01720
C         -2.66430       -1.03430       -0.31940
C         -1.32490       -1.33830       -0.43720
H          3.72960       -0.02810       -1.34450
H          3.44890       -1.26530       -0.00860
H          4.07720        0.36390        0.34720
H          1.76880        1.20520       -0.74460
H          1.86630        0.60630        0.96320
H          0.00590        1.65840        0.28900
H         -2.38070        2.19820        0.49830
H         -4.13940        0.49170        0.11480
H         -3.40500       -1.80640       -0.49480
H         -1.02290       -2.35810       -0.70670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

