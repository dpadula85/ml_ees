%nproc=16
%mem=2GB
%chk=mol_151_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.31970       -0.18140       -0.72650
C          2.06320        0.11510        0.09710
O          0.96490       -0.20560       -0.70970
C         -0.34950       -0.09090       -0.36280
C         -1.31090       -0.45360       -1.28490
C         -2.65240       -0.35530       -0.98000
C         -3.07510        0.10410        0.24470
C         -2.10470        0.46200        1.15410
C         -0.75520        0.37490        0.87820
H          3.10200       -0.88640       -1.55090
H          3.70080        0.76200       -1.16960
H          4.07250       -0.61630       -0.00710
H          2.03580        1.22970        0.27710
H          2.04090       -0.45090        1.03180
H         -1.04330       -0.82680       -2.27800
H         -3.39600       -0.64950       -1.72910
H         -4.14660        0.17240        0.46160
H         -2.43760        0.82700        2.12690
H         -0.02840        0.66960        1.62920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

