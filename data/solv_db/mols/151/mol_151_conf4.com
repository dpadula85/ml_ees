%nproc=16
%mem=2GB
%chk=mol_151_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.54330       -0.75420        0.55330
C         -2.24090        0.57940       -0.04650
O         -1.01530        1.13490        0.29170
C          0.23700        0.65370        0.07750
C          1.34930        1.35180        0.49060
C          2.61300        0.83940        0.25780
C          2.80290       -0.36060       -0.38110
C          1.70340       -1.07040       -0.80060
C          0.43990       -0.54470       -0.56110
H         -3.53340       -0.77330        1.09180
H         -1.76310       -1.09980        1.27240
H         -2.62610       -1.57470       -0.20210
H         -2.41230        0.54990       -1.16240
H         -3.04180        1.28710        0.31970
H          1.28130        2.31210        1.00490
H          3.47170        1.41680        0.59740
H          3.81340       -0.76310       -0.56310
H          1.84310       -2.02000       -1.30650
H         -0.37880       -1.16420       -0.94020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

