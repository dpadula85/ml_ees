%nproc=16
%mem=2GB
%chk=mol_151_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.36140       -0.11400        0.06200
C         -1.97420        0.51710        0.02850
O         -1.04800       -0.53070        0.02070
C          0.32750       -0.34750       -0.00820
C          0.87670        0.91460       -0.03140
C          2.24250        1.11990       -0.06030
C          3.08710        0.03000       -0.06610
C          2.53480       -1.23690       -0.04270
C          1.15610       -1.44270       -0.01360
H         -4.12910        0.69600        0.19920
H         -3.36520       -0.79270        0.94820
H         -3.51740       -0.68530       -0.85910
H         -1.91450        1.15990       -0.87250
H         -1.90620        1.15420        0.93250
H          0.21970        1.77170       -0.02690
H          2.62430        2.14740       -0.07790
H          4.17030        0.19080       -0.08900
H          3.19250       -2.09110       -0.04730
H          0.78440       -2.46080        0.00380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

