%nproc=16
%mem=2GB
%chk=mol_151_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.33240        0.16970       -0.66660
C         -1.90180        0.48530       -0.34540
O         -1.05120       -0.62840       -0.52810
C          0.28950       -0.43050       -0.25530
C          1.20150       -1.46590       -0.40560
C          2.54930       -1.30790       -0.14320
C          3.00830       -0.07530        0.28440
C          2.13680        0.97200        0.44460
C          0.77740        0.78340        0.17170
H         -3.89770        0.00830        0.26020
H         -3.42260       -0.73380       -1.30150
H         -3.75410        1.07450       -1.18540
H         -1.53000        1.35350       -0.94540
H         -1.84820        0.82740        0.71570
H          0.87840       -2.44010       -0.73730
H          3.22080       -2.16290       -0.28000
H          4.05740        0.01940        0.48040
H          2.51490        1.92270        0.77800
H          0.10380        1.62870        0.30640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_151_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

