%nproc=16
%mem=2GB
%chk=mol_036_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.82280       -1.48800        0.17040
C          1.16640       -0.14500       -0.14060
C          2.06930        0.97150        0.34160
O         -0.04400       -0.00710        0.50530
C         -1.11860        0.17850       -0.33510
C         -1.88020        1.45150       -0.04290
C         -2.06590       -0.97390       -0.05810
H          2.91840       -1.38500       -0.02250
H          1.45880       -2.26450       -0.55130
H          1.65190       -1.79390        1.21770
H          1.08380       -0.08450       -1.24080
H          2.99290        0.57180        0.84030
H          1.57640        1.59930        1.12730
H          2.34430        1.66840       -0.48860
H         -0.85840        0.19020       -1.39980
H         -2.88020        1.35490       -0.49810
H         -2.04730        1.58690        1.05700
H         -1.28930        2.29570       -0.47370
H         -2.76410       -1.07710       -0.91110
H         -2.70770       -0.77680        0.83630
H         -1.42930       -1.87300        0.06660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

