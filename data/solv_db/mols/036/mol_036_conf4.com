%nproc=16
%mem=2GB
%chk=mol_036_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72680        0.02520        1.60870
C         -1.11800       -0.11900        0.22500
C         -2.17390        0.34640       -0.75930
O          0.05440        0.59800        0.09300
C          1.13300       -0.17890       -0.28500
C          1.75040        0.22230       -1.58320
C          2.16000       -0.18250        0.80070
H         -2.23650        1.00510        1.71130
H         -1.00880       -0.09650        2.41730
H         -2.56220       -0.73550        1.63920
H         -0.90440       -1.17040       -0.01020
H         -2.83190       -0.52080       -0.98280
H         -2.85010        1.09720       -0.25430
H         -1.73130        0.74590       -1.68790
H          0.75180       -1.22700       -0.38760
H          2.85120       -0.02680       -1.52540
H          1.69650        1.28860       -1.81680
H          1.34290       -0.40200       -2.40330
H          2.88000       -1.01970        0.56690
H          2.77260        0.73520        0.83220
H          1.75130       -0.38480        1.80170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

