%nproc=16
%mem=2GB
%chk=mol_036_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.15040        0.94720        0.25730
C         -1.15170       -0.07690       -0.20730
C         -1.77950       -1.44430       -0.02070
O          0.03350       -0.05330        0.49460
C          1.14450        0.20840       -0.27730
C          1.85710        1.39940        0.32880
C          2.09500       -0.96690       -0.17410
H         -2.08470        1.89960       -0.33690
H         -3.16970        0.53820        0.03410
H         -2.12640        1.13710        1.33530
H         -0.94850        0.12680       -1.27860
H         -2.48140       -1.40950        0.85500
H         -1.00960       -2.20430        0.25530
H         -2.29140       -1.74000       -0.95460
H          0.91190        0.36040       -1.34950
H          1.32020        1.79120        1.23110
H          1.86950        2.19320       -0.45100
H          2.88150        1.15520        0.67880
H          3.11980       -0.65590        0.13520
H          2.21970       -1.48860       -1.14390
H          1.74040       -1.71710        0.58820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

