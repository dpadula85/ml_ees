%nproc=16
%mem=2GB
%chk=mol_036_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04660       -0.79940        0.82970
C          1.13110       -0.15100       -0.15210
C          1.70160       -0.32090       -1.53410
O         -0.18920       -0.53350       -0.04560
C         -1.04970        0.50680        0.27270
C         -1.83960        0.14470        1.51840
C         -2.08540        0.58750       -0.83660
H          3.04370       -1.01280        0.36650
H          1.65050       -1.72750        1.27550
H          2.27220       -0.11690        1.69380
H          1.15100        0.95920        0.05930
H          1.77640        0.65970       -2.08650
H          2.77280       -0.67710       -1.48080
H          1.17570       -1.07630       -2.13550
H         -0.49730        1.45420        0.38000
H         -2.68000       -0.50120        1.24300
H         -2.16920        1.02710        2.08760
H         -1.14700       -0.42250        2.17060
H         -1.72520        1.29230       -1.64300
H         -2.35410       -0.38820       -1.23780
H         -2.98500        1.09590       -0.41220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

