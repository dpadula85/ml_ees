%nproc=16
%mem=2GB
%chk=mol_036_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05030       -0.05630        1.27760
C          1.16350       -0.24980        0.09250
C          1.92680        0.30410       -1.11180
O         -0.01760        0.41170        0.10260
C         -1.16300       -0.28980       -0.10570
C         -1.99360        0.25020       -1.26100
C         -2.00610       -0.11700        1.14560
H          3.05590       -0.46900        1.01420
H          1.73550       -0.60690        2.18110
H          2.20600        1.00340        1.54520
H          1.07640       -1.34220       -0.09210
H          2.20900        1.35190       -0.81930
H          1.21890        0.32380       -1.95190
H          2.85050       -0.26110       -1.31190
H         -1.05190       -1.36550       -0.27120
H         -3.02680        0.49270       -0.96200
H         -1.54690        1.19600       -1.66530
H         -2.07710       -0.50920       -2.08810
H         -1.33210       -0.07250        2.00990
H         -2.48740        0.89910        1.03900
H         -2.79030       -0.89350        1.23260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_036_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

