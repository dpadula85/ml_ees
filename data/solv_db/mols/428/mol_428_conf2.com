%nproc=16
%mem=2GB
%chk=mol_428_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00090       -0.23510       -0.00230
I         -1.73100        1.01420       -0.01370
I          1.75010        0.99320       -0.03030
H         -0.02660       -0.92720       -0.87550
H          0.00650       -0.84510        0.92190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_428_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_428_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_428_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

