%nproc=16
%mem=2GB
%chk=mol_428_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00170       -0.22510       -0.00600
I         -1.74280        0.98330       -0.00770
I          1.74690        0.97910       -0.00210
H         -0.00830       -0.84990        0.89830
H          0.00250       -0.88730       -0.88240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_428_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_428_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_428_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

