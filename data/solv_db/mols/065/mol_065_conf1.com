%nproc=16
%mem=2GB
%chk=mol_065_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.58740       -0.17460        0.35460
C         -0.14470       -0.48840        0.05130
C          0.73880        0.73520        0.08820
S          2.43600        0.15920       -0.30120
H         -2.00060        0.46620       -0.45770
H         -2.14210       -1.12280        0.33130
H         -1.62690        0.32570        1.34220
H          0.22760       -1.21580        0.80660
H         -0.03290       -0.94760       -0.95570
H          0.39920        1.50430       -0.61190
H          0.70880        1.19770        1.10790
H          3.02430       -0.43900        0.80420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

