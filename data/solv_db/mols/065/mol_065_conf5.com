%nproc=16
%mem=2GB
%chk=mol_065_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.13910        0.62990       -0.37560
C         -0.48190       -0.60800        0.24070
C          0.88770       -0.16940        0.70420
S          1.90870        0.45090       -0.64010
H         -1.64870        1.24070        0.40110
H         -0.33030        1.20800       -0.88260
H         -1.85700        0.33140       -1.15890
H         -1.04510       -0.88880        1.15790
H         -0.45050       -1.44570       -0.47390
H          1.33390       -0.97710        1.28010
H          0.71200        0.69390        1.40410
H          2.11040       -0.46590       -1.65690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

