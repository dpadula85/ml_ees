%nproc=16
%mem=2GB
%chk=mol_065_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07140       -0.58590        0.31080
C         -0.25380        0.00330       -0.81720
C          0.85970        0.84730       -0.24100
S          1.95100       -0.14880        0.81290
H         -2.10520       -0.14210        0.23140
H         -1.22620       -1.67850        0.12070
H         -0.68580       -0.36560        1.30240
H         -0.86360        0.65910       -1.47120
H          0.23210       -0.79320       -1.42170
H          0.45400        1.67590        0.38060
H          1.46630        1.29630       -1.03900
H          1.24280       -0.76770        1.83130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

