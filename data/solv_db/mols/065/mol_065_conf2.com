%nproc=16
%mem=2GB
%chk=mol_065_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.55930       -0.35680        0.28950
C         -0.22580        0.28810        0.54250
C          0.74690        0.04340       -0.59230
S          2.31660        0.85500       -0.19750
H         -1.74230       -1.20540        1.00080
H         -2.38010        0.36470        0.48320
H         -1.69110       -0.71620       -0.74380
H          0.21940       -0.10620        1.45600
H         -0.37080        1.40300        0.64800
H          0.94700       -1.04670       -0.65440
H          0.33620        0.44870       -1.55880
H          3.40330        0.02830       -0.59900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

