%nproc=16
%mem=2GB
%chk=mol_065_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.41310        0.24600       -0.27880
C         -0.42090       -0.51490        0.55170
C          0.97190        0.03890        0.40250
S          1.58770       -0.01200       -1.27710
H         -1.78470        1.12520        0.32210
H         -2.33350       -0.38520       -0.42480
H         -1.02640        0.57020       -1.25750
H         -0.46450       -1.60430        0.39020
H         -0.70030       -0.36150        1.62790
H          0.92960        1.11390        0.74140
H          1.66690       -0.46850        1.07390
H          2.98720        0.25220       -1.22680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_065_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

