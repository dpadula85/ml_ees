%nproc=16
%mem=2GB
%chk=mol_338_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.60510        0.50940        0.26340
O          0.75720       -0.26180        0.94350
C         -0.35250       -0.80660        0.43690
C         -1.42940        0.08130       -0.11050
Cl        -0.92340        1.06360       -1.45950
Cl        -2.06570        1.02750        1.24340
F         -0.99720       -1.57140        1.44560
F         -0.05550       -1.76110       -0.56780
H          1.19310        1.46160       -0.11280
H          2.09030        0.01940       -0.62640
H          2.43910        0.83800        0.96480
H         -2.26110       -0.59980       -0.47470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

