%nproc=16
%mem=2GB
%chk=mol_338_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.76930        0.48760       -0.19450
O         -0.40940        0.67940       -0.00390
C          0.28710       -0.52660        0.05730
C          1.75260       -0.14450        0.26740
Cl         2.23550        0.85350       -1.13370
Cl         1.89390        0.85340        1.71080
F         -0.17760       -1.35740        1.03630
F          0.23680       -1.14600       -1.19630
H         -2.23410       -0.10370        0.62290
H         -2.23340        1.48640       -0.29570
H         -1.97970       -0.04680       -1.15100
H          2.39760       -1.03510        0.28020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

