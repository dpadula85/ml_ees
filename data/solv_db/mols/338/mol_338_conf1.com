%nproc=16
%mem=2GB
%chk=mol_338_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.78390        0.25600       -0.43190
O         -0.43300        0.51270       -0.37930
C          0.30160       -0.42110        0.30520
C          1.76160        0.04280        0.24820
Cl         1.91780        1.64270        1.00550
Cl         2.28590        0.16340       -1.43450
F          0.20930       -1.61840       -0.38460
F         -0.09420       -0.61310        1.59950
H         -2.24250        1.10080       -1.01910
H         -2.28530        0.30780        0.56640
H         -2.04900       -0.70850       -0.88440
H          2.41160       -0.66520        0.80890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

