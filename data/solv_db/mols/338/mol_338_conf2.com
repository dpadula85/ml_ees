%nproc=16
%mem=2GB
%chk=mol_338_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.77150        0.44850        0.27290
O         -0.45600        0.60420       -0.07720
C          0.32930       -0.51220        0.09370
C          1.73250       -0.11920       -0.35110
Cl         2.30610        1.22930        0.65340
Cl         1.57020        0.47080       -2.03580
F         -0.06820       -1.56880       -0.71700
F          0.42770       -0.93510        1.39380
H         -1.93780        0.17330        1.33340
H         -2.27330        1.42830        0.12430
H         -2.30270       -0.27250       -0.39450
H          2.44360       -0.94660       -0.29600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

