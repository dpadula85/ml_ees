%nproc=16
%mem=2GB
%chk=mol_338_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.85040       -0.05720       -0.20300
O         -0.66140        0.58010        0.14820
C          0.41200       -0.30730        0.19300
C          1.63240        0.50530        0.58310
Cl         3.06090       -0.55050        0.66610
Cl         1.39770        1.22940        2.17530
F          0.15980       -1.15970        1.27000
F          0.58800       -0.99840       -0.96540
H         -2.14030       -0.85220        0.49750
H         -2.62130        0.75800       -0.26250
H         -1.80240       -0.48020       -1.24240
H          1.82500        1.33250       -0.14450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_338_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

