%nproc=16
%mem=2GB
%chk=mol_220_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.50120       -0.40180        0.94780
C         -2.15650       -0.32510       -0.51240
C         -0.90890        0.48340       -0.77290
C          0.29840       -0.11010       -0.07020
C          1.51410        0.74260       -0.36890
C          2.75120        0.22430        0.28890
O          3.08720       -1.05840       -0.10290
H         -3.42360       -1.01300        1.03470
H         -2.79360        0.62410        1.28830
H         -1.67730       -0.76440        1.57700
H         -2.98450        0.13090       -1.05370
H         -1.96360       -1.34550       -0.91710
H         -1.02510        1.55300       -0.55520
H         -0.71280        0.40920       -1.87130
H          0.17490       -0.10680        1.03130
H          0.49200       -1.12890       -0.44840
H          1.30270        1.74790        0.05740
H          1.64270        0.89190       -1.46220
H          2.59410        0.29580        1.39660
H          3.57920        0.91490        0.05260
H          2.71050       -1.76410        0.46050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

