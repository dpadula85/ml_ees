%nproc=16
%mem=2GB
%chk=mol_220_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.20260        1.13570       -0.64670
C          1.97660       -0.34160       -0.65520
C          0.55500       -0.73020       -0.40000
C          0.05950       -0.24290        0.94940
C         -1.37040       -0.63850        1.19640
C         -2.32060       -0.08480        0.17320
O         -2.32710        1.29120        0.11690
H          1.69520        1.57560       -1.54780
H          3.29300        1.30800       -0.82040
H          1.87210        1.63310        0.28090
H          2.26060       -0.72020       -1.65910
H          2.68310       -0.80030        0.07430
H         -0.06540       -0.27590       -1.20210
H          0.45040       -1.83380       -0.43310
H          0.70290       -0.72910        1.70930
H          0.12060        0.86730        0.99510
H         -1.68340       -0.33260        2.21200
H         -1.43250       -1.74530        1.15310
H         -3.34030       -0.45110        0.41040
H         -2.08620       -0.46710       -0.84070
H         -3.24560        1.58240       -0.11370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

