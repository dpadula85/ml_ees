%nproc=16
%mem=2GB
%chk=mol_220_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.09490        0.36970        0.35560
C          1.63020        0.47000       -0.09820
C          0.92750       -0.68880        0.53450
C         -0.52690       -0.77170        0.21250
C         -1.32240        0.41560        0.63840
C         -2.77050        0.19140        0.24870
O         -2.79310        0.03830       -1.13610
H          3.62770        1.31270        0.21490
H          3.14350        0.05440        1.43070
H          3.59710       -0.41750       -0.25130
H          1.27910        1.44990        0.25040
H          1.62020        0.46010       -1.18780
H          1.41050       -1.61760        0.16100
H          1.07680       -0.59080        1.64620
H         -0.93460       -1.67610        0.70480
H         -0.63540       -0.90620       -0.87380
H         -1.24850        0.60600        1.72680
H         -0.99820        1.32960        0.06180
H         -3.40740        1.03920        0.50790
H         -3.08840       -0.76910        0.74210
H         -3.68210       -0.29910       -1.45530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

