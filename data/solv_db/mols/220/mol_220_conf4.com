%nproc=16
%mem=2GB
%chk=mol_220_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.33460        0.19900       -0.06820
C         -1.94900        0.00690       -0.59470
C         -0.88960        0.10090        0.50350
C          0.44240       -0.10970       -0.16450
C          1.59790       -0.04510        0.79730
C          2.85500       -0.27910       -0.05280
O          4.00280       -0.24830        0.69460
H         -3.55650        1.29570       -0.11380
H         -4.04530       -0.29490       -0.78810
H         -3.50530       -0.20190        0.93550
H         -1.69930        0.78390       -1.33840
H         -1.81180       -0.97210       -1.09520
H         -0.95530        1.04520        1.04400
H         -1.01570       -0.73200        1.23550
H          0.44730       -1.08330       -0.66580
H          0.55890        0.67340       -0.93840
H          1.68400        0.90800        1.32320
H          1.56690       -0.89600        1.50720
H          2.80980        0.47960       -0.86070
H          2.70910       -1.26210       -0.52930
H          4.08820        0.63180        1.13630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

