%nproc=16
%mem=2GB
%chk=mol_220_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.54190        1.12760       -0.76060
C          1.54700        0.21400       -0.07650
C          0.66930       -0.45670       -1.08880
C         -0.34430       -1.37840       -0.50090
C         -1.30190       -0.69520        0.45030
C         -2.03070        0.38700       -0.33390
O         -2.94370        1.06130        0.50690
H          3.36150        0.54240       -1.24140
H          2.99210        1.83890       -0.02900
H          2.01370        1.70110       -1.55160
H          0.98330        0.77380        0.69300
H          2.15420       -0.54550        0.49740
H          0.24370        0.27910       -1.76500
H          1.35070       -1.10110       -1.71770
H          0.13260       -2.25800       -0.01490
H         -0.96770       -1.79620       -1.32190
H         -2.00760       -1.47890        0.80160
H         -0.75640       -0.27640        1.31270
H         -2.51020       -0.05350       -1.20900
H         -1.27080        1.11060       -0.68770
H         -3.85660        1.00420        0.10580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_220_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

