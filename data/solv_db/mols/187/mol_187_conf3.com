%nproc=16
%mem=2GB
%chk=mol_187_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.88110        0.46760        0.50630
C          1.46090        0.14880        0.18110
C          1.11620       -0.90670       -0.63710
C         -0.20290       -1.19570       -0.93380
C         -1.23510       -0.42250       -0.41060
C         -0.89950        0.63110        0.40570
C          0.42250        0.91010        0.69490
O         -1.89880        1.42770        0.94850
C         -2.66030       -0.69630       -0.70220
H          3.52270        0.51230       -0.38140
H          2.89420        1.42110        1.07140
H          3.29030       -0.29990        1.22420
H          1.91390       -1.52110       -1.05420
H         -0.49670       -2.01440       -1.57010
H          0.71120        1.73990        1.33930
H         -1.74390        2.20390        1.54440
H         -3.12750       -1.16990        0.19500
H         -2.70440       -1.45130       -1.50740
H         -3.24370        0.21540       -0.91400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

