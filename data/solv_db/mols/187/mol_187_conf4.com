%nproc=16
%mem=2GB
%chk=mol_187_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.94460        0.38320       -0.03610
C          1.47990        0.10510       -0.06420
C          0.95320       -1.05730       -0.59410
C         -0.40350       -1.30970       -0.61710
C         -1.29500       -0.39100       -0.10130
C         -0.77820        0.77300        0.43020
C          0.59370        1.02280        0.45070
O         -1.69110        1.68370        0.94390
C         -2.74880       -0.68430       -0.13730
H          3.17950        1.36000       -0.51760
H          3.33830        0.34320        1.00040
H          3.48540       -0.38630       -0.61000
H          1.62960       -1.78860       -1.00230
H         -0.81320       -2.23280       -1.03840
H          0.93610        1.94790        0.87800
H         -1.32980        2.55700        1.34470
H         -2.95830       -1.78000       -0.00590
H         -3.24240       -0.34370       -1.05890
H         -3.27980       -0.20210        0.73540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

