%nproc=16
%mem=2GB
%chk=mol_187_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.97340       -0.19410       -0.12090
C          1.47270       -0.18050       -0.05960
C          0.75000       -1.35440       -0.03050
C         -0.62850       -1.38440        0.02600
C         -1.33730       -0.19330        0.05560
C         -0.63460        0.99840        0.02740
C          0.75690        1.00030       -0.02970
O         -1.35240        2.16390        0.05730
C         -2.82770       -0.16580        0.11650
H          3.30460       -1.00750       -0.79740
H          3.35140        0.75000       -0.51870
H          3.31730       -0.36820        0.94090
H          1.30890       -2.28590       -0.05420
H         -1.17450       -2.32550        0.04820
H          1.29030        1.94080       -0.05090
H         -0.99130        3.09480        0.04280
H         -3.18620        0.68790        0.74740
H         -3.20420       -1.10430        0.53150
H         -3.18880       -0.07230       -0.93170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

