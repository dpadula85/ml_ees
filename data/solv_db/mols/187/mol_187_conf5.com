%nproc=16
%mem=2GB
%chk=mol_187_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.94310        0.14730        0.53160
C         -1.46670       -0.02800        0.30660
C         -0.89520       -1.27090        0.14340
C          0.45220       -1.47060       -0.06450
C          1.25670       -0.34580       -0.10810
C          0.74370        0.92950        0.04870
C         -0.60730        1.05190        0.25220
O          1.60670        2.02210       -0.00530
C          2.73170       -0.47510       -0.32970
H         -3.09560        0.29400        1.63750
H         -3.28990        1.05250        0.01480
H         -3.42830       -0.77850        0.17650
H         -1.51750       -2.14600        0.17670
H          0.89930       -2.44800       -0.19260
H         -1.03490        2.03340        0.37760
H          1.27090        2.95860        0.10390
H          3.09190        0.37380       -0.95400
H          2.96700       -1.43770       -0.81200
H          3.25850       -0.46230        0.66050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

