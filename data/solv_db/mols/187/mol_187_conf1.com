%nproc=16
%mem=2GB
%chk=mol_187_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.96850        0.01610        0.20810
C         -1.47020       -0.10040        0.11190
C         -0.84100       -1.32380        0.21940
C          0.55110       -1.40010        0.12560
C          1.32250       -0.27370       -0.07370
C          0.69720        0.95120       -0.18170
C         -0.68010        1.01120       -0.08690
O          1.47320        2.09260       -0.38320
C          2.78790       -0.36920       -0.17000
H         -3.21330        0.31350        1.25960
H         -3.36820       -0.98000       -0.00680
H         -3.28900        0.80290       -0.47800
H         -1.45460       -2.19050        0.37430
H          1.03700       -2.34760        0.20890
H         -1.18640        1.95770       -0.16870
H          0.98720        2.98090       -0.45920
H          3.13660       -1.38600        0.11590
H          3.30260        0.32180        0.55790
H          3.17590       -0.07640       -1.17340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_187_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

