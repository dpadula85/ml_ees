%nproc=16
%mem=2GB
%chk=mol_625_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.37580        0.15850        0.51970
C         -0.49360        0.28730       -0.66430
C          0.76630       -0.52960       -0.59600
N          1.51750       -0.10220        0.55560
O          1.79670       -0.88790        1.46700
O          1.93130        1.19230        0.64700
H         -1.14690        0.93950        1.30160
H         -1.31280       -0.81460        1.04570
H         -2.42510        0.39300        0.19310
H         -0.23360        1.35700       -0.80330
H         -1.01060       -0.01120       -1.62020
H          0.57160       -1.61950       -0.56510
H          1.41500       -0.36240       -1.48060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

