%nproc=16
%mem=2GB
%chk=mol_625_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.75620        0.09030       -0.20070
C         -0.30560       -0.00410       -0.53790
C          0.59210       -0.00380        0.68200
N          1.94970       -0.09870        0.20680
O          2.63500       -1.06740        0.56860
O          2.54820        0.81300       -0.62280
H         -2.28440       -0.74870       -0.74010
H         -1.95830        0.04160        0.90120
H         -2.15540        1.06610       -0.56460
H         -0.05630       -0.94260       -1.10180
H         -0.00220        0.83680       -1.19270
H          0.43520        0.89870        1.30850
H          0.35810       -0.88120        1.29340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

