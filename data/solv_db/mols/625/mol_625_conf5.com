%nproc=16
%mem=2GB
%chk=mol_625_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.71970        0.12210       -0.54700
C         -0.34580        0.57200       -0.11850
C          0.64490       -0.57320       -0.10340
N          1.95990       -0.12550        0.31020
O          2.23320        1.05270        0.61320
O          2.98340       -1.03880        0.37960
H         -2.26600       -0.37360        0.28920
H         -2.31340        0.97870       -0.92240
H         -1.65640       -0.62590       -1.38580
H          0.00750        1.41860       -0.73450
H         -0.44250        0.95350        0.92890
H          0.66800       -1.00330       -1.13960
H          0.24690       -1.35730        0.56880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

