%nproc=16
%mem=2GB
%chk=mol_625_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.37070        0.32540        0.48660
C         -0.53210       -0.65290       -0.29680
C          0.76800       -0.04640       -0.75750
N          1.56500        0.39590        0.36330
O          1.92910        1.57210        0.54170
O          1.93900       -0.55390        1.28530
H         -1.05280        0.40500        1.53990
H         -2.41930       -0.08780        0.48280
H         -1.37680        1.31530       -0.00830
H         -0.30500       -1.50040        0.34240
H         -1.08410       -1.02710       -1.18250
H          1.32930       -0.87280       -1.27560
H          0.61040        0.72770       -1.52130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

