%nproc=16
%mem=2GB
%chk=mol_625_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.37280        0.61300        0.13350
C         -0.58810       -0.66790        0.17740
C          0.76250       -0.55830       -0.46340
N          1.61920        0.41720        0.13630
O          1.27020        1.10470        1.09590
O          2.86420        0.57060       -0.39580
H         -2.46680        0.35310        0.18220
H         -1.19010        1.24170        1.03060
H         -1.15780        1.16820       -0.81930
H         -1.14850       -1.43810       -0.41280
H         -0.46460       -0.99290        1.23570
H          0.61620       -0.26380       -1.53440
H          1.25650       -1.54740       -0.36600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_625_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

