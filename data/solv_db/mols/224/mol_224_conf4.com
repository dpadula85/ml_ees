%nproc=16
%mem=2GB
%chk=mol_224_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.05880        0.68090       -0.71910
C         -2.66120        1.09500       -0.30620
C         -1.81920       -0.13150       -0.26120
C         -0.37530        0.16180        0.14690
C          0.31250       -1.16180        0.13930
C          1.72370       -1.25690        0.51110
C          2.78340       -0.61050       -0.27640
C          2.81220        0.85380       -0.46890
O          4.02860        1.07650       -1.21830
H         -4.64400        1.51500       -1.12020
H         -4.55500        0.15600        0.12850
H         -3.92140       -0.04930       -1.56910
H         -2.29830        1.78400       -1.11070
H         -2.67430        1.68220        0.61680
H         -1.77300       -0.57480       -1.27620
H         -2.21300       -0.84570        0.49040
H          0.04440        0.95260       -0.45790
H         -0.37750        0.55790        1.20310
H          0.12130       -1.67930       -0.86590
H         -0.25520       -1.87000        0.84070
H          2.04420       -2.36590        0.55820
H          1.89870       -0.96820        1.60270
H          3.76780       -0.85910        0.26320
H          2.92620       -1.14440       -1.27040
H          2.06110        1.27610       -1.17320
H          2.92510        1.40030        0.47240
H          4.17700        0.32530       -1.82850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

