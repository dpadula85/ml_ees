%nproc=16
%mem=2GB
%chk=mol_224_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.41950        0.35390       -0.57340
C          3.34250       -0.15480        0.36280
C          2.01370        0.14850       -0.27850
C          0.84630       -0.30920        0.55580
C         -0.41120        0.07270       -0.23440
C         -1.66000       -0.34040        0.51050
C         -2.84040        0.07630       -0.34220
C         -4.13760       -0.29860        0.32700
O         -5.17840        0.12030       -0.52410
H          4.01460        1.26010       -1.06910
H          5.35370        0.59820       -0.03040
H          4.59330       -0.40810       -1.38150
H          3.39720        0.42240        1.31320
H          3.47940       -1.23750        0.55310
H          1.95890       -0.24320       -1.30250
H          1.93930        1.26450       -0.34550
H          0.87510       -1.40840        0.73920
H          0.83190        0.18930        1.52620
H         -0.32700       -0.47560       -1.19380
H         -0.35760        1.16650       -0.39830
H         -1.69400        0.05810        1.53260
H         -1.63570       -1.46390        0.55030
H         -2.74810       -0.43110       -1.31040
H         -2.78350        1.18750       -0.48240
H         -4.21270        0.20490        1.31490
H         -4.15090       -1.39970        0.42360
H         -4.92820        1.04720       -0.79900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

