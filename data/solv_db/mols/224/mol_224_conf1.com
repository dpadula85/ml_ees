%nproc=16
%mem=2GB
%chk=mol_224_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.23760        0.30930       -0.98770
C         -2.31630        0.07890        0.18860
C         -1.09020        0.95960        0.11910
C         -0.18740        0.70770        1.31130
C          0.29640       -0.66930        1.41690
C          1.04490       -1.35430        0.38080
C          2.36110       -0.97650       -0.12900
C          2.63770        0.28420       -0.81680
O          2.49070        1.47330       -0.16290
H         -3.13850        1.33060       -1.39380
H         -4.27510        0.13350       -0.63700
H         -2.96940       -0.39530       -1.81360
H         -2.05230       -1.00460        0.16710
H         -2.86760        0.28500        1.12880
H         -0.50770        0.74980       -0.78890
H         -1.43220        2.00830        0.07550
H          0.45550        1.54990        1.47250
H         -0.96600        0.80200        2.19490
H          0.81750       -0.82170        2.44230
H         -0.65020       -1.30910        1.60920
H          0.36130       -1.55260       -0.52770
H          1.16120       -2.46320        0.73100
H          2.65910       -1.80460       -0.88500
H          3.17830       -1.10030        0.65930
H          3.73220        0.27450       -1.15920
H          2.08410        0.31230       -1.81610
H          2.41050        2.19260       -0.83580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

