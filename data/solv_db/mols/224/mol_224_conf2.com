%nproc=16
%mem=2GB
%chk=mol_224_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.59620        0.45510       -0.64360
C         -3.30680       -0.34570        0.61100
C         -2.01720       -1.12970        0.47970
C         -0.90780       -0.13210        0.24640
C          0.44040       -0.82030        0.09980
C          1.45410        0.28270       -0.12730
C          2.84970       -0.27160       -0.29040
C          3.78260        0.90530       -0.51120
O          3.72420        1.77430        0.60310
H         -3.63110       -0.20460       -1.54030
H         -4.60790        0.92390       -0.46930
H         -2.90590        1.31440       -0.76850
H         -4.16970       -1.04300        0.74290
H         -3.27670        0.36710        1.45910
H         -1.87760       -1.72980        1.37910
H         -2.12230       -1.76640       -0.42720
H         -0.86410        0.54360        1.09690
H         -1.10000        0.36690       -0.73030
H          0.73650       -1.36510        1.01770
H          0.44340       -1.51950       -0.76890
H          1.40330        0.96860        0.73780
H          1.21550        0.82540       -1.07570
H          3.18660       -0.84180        0.58230
H          2.89080       -0.89360       -1.18780
H          3.48770        1.50910       -1.37610
H          4.83060        0.56980       -0.57350
H          3.93810        1.25690        1.43450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

