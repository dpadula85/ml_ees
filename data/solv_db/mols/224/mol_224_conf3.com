%nproc=16
%mem=2GB
%chk=mol_224_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.76770        0.59380       -0.66760
C         -3.42410       -0.26960        0.51490
C         -2.06120       -0.89660        0.40200
C         -0.95500        0.10620        0.29820
C          0.35120       -0.66900        0.19140
C          1.45560        0.35660        0.08750
C          2.81870       -0.31620       -0.02510
C          3.83330        0.82630       -0.12220
O          5.09050        0.27950       -0.22660
H         -3.67520        0.07520       -1.63450
H         -4.85560        0.85490       -0.56530
H         -3.23160        1.55790       -0.68230
H         -3.55130        0.32430        1.42630
H         -4.16190       -1.09710        0.54590
H         -1.89290       -1.48520        1.32860
H         -2.02280       -1.56380       -0.45490
H         -1.02370        0.69910       -0.65280
H         -0.87880        0.78800        1.15600
H          0.37740       -1.33660       -0.67830
H          0.49920       -1.26440        1.10650
H          1.44770        1.02160        0.96970
H          1.27810        0.98060       -0.80280
H          2.86020       -0.97240       -0.89920
H          2.99890       -0.90520        0.89350
H          3.59730        1.41770       -1.01470
H          3.70170        1.43920        0.79640
H          5.19190       -0.54500        0.32420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_224_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

