%nproc=16
%mem=2GB
%chk=mol_521_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.34130        1.10320        0.55290
C          1.03260        1.00390        0.49090
C          1.63980       -0.07300       -0.12050
C          0.83830       -1.04710       -0.66780
C         -0.53570       -0.94260       -0.60280
C         -1.15490        0.12750        0.00480
C         -2.60670        0.22080        0.06370
O         -3.28410       -0.70000       -0.45400
O          3.04290       -0.16850       -0.18050
H         -0.79410        1.97370        1.04700
H          1.67820        1.76830        0.92030
H          1.31540       -1.89840       -1.15070
H         -1.14370       -1.73640       -1.04900
H         -3.14150        1.02910        0.52290
H          3.45470       -0.66040        0.62280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

