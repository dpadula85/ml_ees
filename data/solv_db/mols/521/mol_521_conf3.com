%nproc=16
%mem=2GB
%chk=mol_521_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.28610       -1.25120       -0.19120
C         -1.09320       -1.07880       -0.04440
C         -1.60400        0.18150        0.15260
C         -0.74810        1.28510        0.20590
C          0.62660        1.10450        0.05850
C          1.12780       -0.16220       -0.13840
C          2.55750       -0.33790       -0.28960
O          3.37480        0.61560       -0.25070
O         -2.96820        0.35780        0.29840
H          0.65730       -2.25820       -0.34530
H         -1.73730       -1.94110       -0.08830
H         -1.15160        2.28200        0.36170
H          1.22930        2.00050        0.11010
H          3.00010       -1.32590       -0.44740
H         -3.55700        0.52820       -0.52570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

