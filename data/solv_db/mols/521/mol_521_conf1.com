%nproc=16
%mem=2GB
%chk=mol_521_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.38650        1.01980        0.61200
C         -0.98320        1.04860        0.45280
C         -1.62990        0.05110       -0.24320
C         -0.86590       -0.96910       -0.77100
C          0.50070       -0.97940       -0.60080
C          1.16970        0.00910        0.09260
C          2.61850       -0.01700        0.26370
O          3.21420        0.88990        0.89750
O         -3.00520        0.03300       -0.43190
H          0.90690        1.81370        1.16680
H         -1.59330        1.84490        0.86300
H         -1.37410       -1.76300       -1.32410
H          1.10130       -1.77820       -1.01380
H          3.21700       -0.80540       -0.14320
H         -3.66330       -0.39790        0.17940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_521_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

