%nproc=16
%mem=2GB
%chk=mol_358_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.40950        0.51640        0.37410
C          1.90540        0.58170        0.55440
C          1.46880       -0.79500        1.03590
C         -0.02870       -0.71660        1.25270
C         -0.65570       -0.80430       -0.12200
C          0.13450       -0.03530       -1.08900
O         -0.11750       -0.18290       -2.27430
C          1.21660        0.90830       -0.74080
C         -2.11720       -0.45350       -0.10320
C         -2.66050       -0.56680       -1.50970
C         -2.37530        0.91550        0.47710
H          3.68230        0.95190       -0.62730
H          3.78360       -0.52560        0.34550
H          3.96420        1.07490        1.13030
H          1.68060        1.32520        1.35060
H          1.66760       -1.48550        0.17080
H          2.02030       -1.11150        1.91850
H         -0.39150       -1.51920        1.91680
H         -0.25820        0.28590        1.71900
H         -0.59080       -1.88280       -0.42050
H          1.98500        0.91600       -1.54070
H          0.80010        1.93680       -0.64100
H         -2.63940       -1.22040        0.50640
H         -2.20250       -1.46580       -1.99160
H         -2.52110        0.33510       -2.10910
H         -3.77290       -0.75170       -1.48480
H         -1.45000        1.50660        0.48980
H         -2.75190        0.85500        1.52500
H         -3.18530        1.40770       -0.11280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

