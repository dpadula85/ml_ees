%nproc=16
%mem=2GB
%chk=mol_358_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.52840        0.06110        0.25090
C          2.03640        0.05750        0.54660
C          1.43220       -0.87700       -0.49570
C         -0.03670       -1.02370       -0.12130
C         -0.68480        0.30280       -0.47940
C          0.01970        1.44760        0.13410
O         -0.62860        2.44940        0.36270
C          1.44330        1.43740        0.47710
C         -2.15960        0.30300       -0.23050
C         -2.75170       -0.80880       -1.10510
C         -2.53820       -0.00030        1.18130
H          3.80570       -1.00140        0.06660
H          3.70560        0.59310       -0.70900
H          4.13770        0.42570        1.06360
H          1.90900       -0.35030        1.55860
H          1.97590       -1.81940       -0.48730
H          1.50760       -0.34290       -1.46570
H         -0.46230       -1.78970       -0.77780
H         -0.10370       -1.25390        0.94620
H         -0.54710        0.40680       -1.59300
H          1.56510        1.90530        1.48210
H          2.05260        2.08440       -0.21510
H         -2.57710        1.24700       -0.58880
H         -2.17780       -0.78770       -2.05510
H         -2.56890       -1.75090       -0.54340
H         -3.81870       -0.62290       -1.21270
H         -3.64270        0.09360        1.26250
H         -2.10900        0.68390        1.92460
H         -2.31240       -1.06970        1.39220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

