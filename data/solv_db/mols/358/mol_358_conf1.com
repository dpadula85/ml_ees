%nproc=16
%mem=2GB
%chk=mol_358_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.39860        0.22270        0.11020
C         -2.12650       -0.57120       -0.09710
C         -1.46060       -0.73320        1.22670
C          0.01480       -0.90310        1.08360
C          0.57120        0.43720        0.58060
C         -0.11370        0.78920       -0.65470
O          0.24710        1.75550       -1.29600
C         -1.25720       -0.00270       -1.16960
C          2.05410        0.27340        0.47380
C          2.80210        1.45640        0.00150
C          2.43710       -0.91390       -0.38490
H         -4.28820       -0.28000       -0.34470
H         -3.35970        1.23300       -0.30410
H         -3.65170        0.30770        1.20520
H         -2.45690       -1.59270       -0.42910
H         -1.85020       -1.64430        1.75900
H         -1.70250        0.13110        1.89740
H          0.22750       -1.71480        0.37250
H          0.44640       -1.06160        2.09050
H          0.30940        1.16730        1.37420
H         -0.89770       -0.82020       -1.86190
H         -1.92490        0.62300       -1.83090
H          2.42170        0.02430        1.50970
H          2.25010        2.40430       -0.00530
H          3.72510        1.64470        0.62320
H          3.20770        1.31440       -1.04180
H          2.31340       -1.87400        0.13460
H          1.93180       -0.86920       -1.36320
H          3.52880       -0.80310       -0.59640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

