%nproc=16
%mem=2GB
%chk=mol_358_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.50570        0.55790       -0.14490
C          2.11810        0.08390        0.17840
C          1.54790       -0.80410       -0.90830
C          0.19970       -1.28370       -0.41570
C         -0.78240       -0.14940       -0.56700
C         -0.01920        1.12680       -0.52380
O         -0.32440        2.08220       -1.19790
C          1.15490        1.23590        0.37350
C         -1.79070       -0.12550        0.56120
C         -2.75150        1.02700        0.36520
C         -2.60790       -1.39600        0.60970
H          4.14060       -0.34410       -0.27080
H          3.46720        1.19250       -1.05170
H          3.87640        1.11980        0.73500
H          2.16560       -0.52570        1.10270
H          2.20610       -1.65190       -1.15850
H          1.42660       -0.21320       -1.84620
H          0.26700       -1.50190        0.68580
H         -0.13200       -2.17340       -0.98470
H         -1.35540       -0.21760       -1.50860
H          0.76590        1.21480        1.42560
H          1.72360        2.16290        0.17760
H         -1.24210        0.01590        1.50390
H         -2.26560        2.00380        0.31900
H         -3.48590        0.95910        1.22030
H         -3.33190        0.83520       -0.57480
H         -3.26930       -1.32880        1.48390
H         -3.21520       -1.46970       -0.32340
H         -1.99160       -2.30240        0.73450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

