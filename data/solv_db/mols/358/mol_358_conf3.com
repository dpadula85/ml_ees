%nproc=16
%mem=2GB
%chk=mol_358_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.06060       -0.37330        0.50440
C          2.14050       -0.59520       -0.70250
C          1.61310        0.69770       -1.19820
C          0.19370        1.00730       -0.89350
C         -0.43210        0.34950        0.25770
C          0.23420       -0.92010        0.69160
O          0.04990       -1.33120        1.80560
C          1.09600       -1.59610       -0.31120
C         -1.89620        0.00790        0.07330
C         -2.74750        1.22710       -0.19100
C         -2.47220       -0.64530        1.31400
H          3.43730       -1.36000        0.86070
H          3.94940        0.20410        0.16030
H          2.55210        0.20090        1.29520
H          2.79830       -1.11360       -1.46180
H          2.28200        1.50880       -0.80200
H          1.80270        0.72460       -2.30920
H         -0.40030        0.82390       -1.83340
H          0.11260        2.12350       -0.76130
H         -0.38210        1.00790        1.15690
H          0.50620       -1.83860       -1.21730
H          1.59960       -2.46960        0.13430
H         -2.06780       -0.68790       -0.77480
H         -3.79220        0.86650       -0.28670
H         -2.73150        1.90850        0.68280
H         -2.50160        1.73490       -1.13890
H         -3.58840       -0.47750        1.36730
H         -2.33690       -1.73100        1.33910
H         -2.07950       -0.17120        2.23850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_358_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

