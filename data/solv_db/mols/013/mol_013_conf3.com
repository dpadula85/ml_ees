%nproc=16
%mem=2GB
%chk=mol_013_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23280        0.11240        0.04360
C          0.04620       -0.58730       -0.36560
C          1.22930        0.07540        0.34660
I          1.26230        2.11130       -0.24960
H         -2.06960       -0.49130       -0.41160
H         -1.33710        0.21400        1.13630
H         -1.25680        1.12060       -0.44390
H          0.16050       -0.39160       -1.46170
H          0.01920       -1.66400       -0.17000
H          2.16750       -0.45230        0.13940
H          1.01130       -0.04710        1.43640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

