%nproc=16
%mem=2GB
%chk=mol_013_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.34550        0.17080        0.09570
C         -0.05500       -0.53480       -0.16000
C          1.15050        0.36440        0.07200
I          2.92450       -0.75260       -0.32460
H         -1.99190       -0.41400        0.77550
H         -1.88080        0.36910       -0.87740
H         -1.20210        1.16890        0.55680
H          0.03090       -0.90420       -1.20310
H          0.04220       -1.36750        0.56850
H          1.12100        1.21100       -0.63600
H          1.20630        0.68880        1.13260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

