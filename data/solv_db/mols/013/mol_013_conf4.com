%nproc=16
%mem=2GB
%chk=mol_013_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15950        0.34650        0.14640
C         -0.13540       -0.64510       -0.34770
C          1.22920       -0.13040        0.12530
I          1.62080        1.81110       -0.67820
H         -1.91560        0.61120       -0.60470
H         -0.69930        1.30010        0.49540
H         -1.67470       -0.07070        1.05280
H         -0.15380       -0.77550       -1.44070
H         -0.25210       -1.64280        0.12240
H          1.10160        0.02000        1.23470
H          2.03890       -0.82440       -0.10560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

