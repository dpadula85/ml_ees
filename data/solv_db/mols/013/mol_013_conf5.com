%nproc=16
%mem=2GB
%chk=mol_013_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.05210       -0.59250       -0.24360
C         -0.16990        0.64600       -0.26950
C          1.06120        0.34790        0.57840
I          1.97600       -1.35070       -0.38570
H         -2.06530       -0.31980        0.09060
H         -0.66520       -1.34850        0.49680
H         -1.02280       -1.05250       -1.24680
H          0.18110        0.88310       -1.29210
H         -0.69350        1.54070        0.10470
H          1.72750        1.21470        0.58020
H          0.72290        0.03140        1.58680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

