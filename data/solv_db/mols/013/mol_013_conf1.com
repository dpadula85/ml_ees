%nproc=16
%mem=2GB
%chk=mol_013_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.21730        0.39270        0.01360
C          0.09420       -0.54610       -0.37440
C         -1.17130       -0.22650        0.40350
I         -1.81030        1.76960        0.01350
H          1.42680        0.37870        1.10030
H          2.11090        0.06740       -0.55700
H          0.91970        1.40720       -0.35350
H         -0.13020       -0.32170       -1.45140
H          0.38090       -1.59520       -0.29080
H         -1.97080       -0.89960        0.02050
H         -1.06730       -0.42640        1.47590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_013_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

