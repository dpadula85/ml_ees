%nproc=16
%mem=2GB
%chk=mol_522_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.17920       -1.22820       -0.22000
C          1.18700       -0.22780        0.36600
C          0.62980        1.05380       -0.26880
C         -0.83250        0.90310       -0.03100
C         -1.10400       -0.57210        0.21720
H          0.33180       -2.23100        0.15340
H          0.26300       -1.11060       -1.31960
H          2.19960       -0.41520        0.01410
H          1.07260       -0.13540        1.45690
H          1.08960        1.94490        0.16060
H          0.80640        0.93870       -1.36800
H         -1.43570        1.24150       -0.90570
H         -1.18880        1.49250        0.83180
H         -1.94030       -0.91260       -0.40000
H         -1.25790       -0.74180        1.31310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

