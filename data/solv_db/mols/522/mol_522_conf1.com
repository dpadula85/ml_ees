%nproc=16
%mem=2GB
%chk=mol_522_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.45450        1.16910        0.07780
C          1.25870       -0.09490        0.00090
C          0.25430       -1.18170        0.28710
C         -0.96020       -0.66800       -0.49720
C         -1.00880        0.76870        0.03460
H          0.65890        1.80370       -0.81890
H          0.63720        1.69710        1.02190
H          1.76520       -0.21520       -0.97480
H          2.04810       -0.14120        0.78930
H          0.56090       -2.17640       -0.01710
H         -0.01890       -1.08900        1.36550
H         -1.84480       -1.24480       -0.20700
H         -0.79490       -0.69900       -1.57840
H         -1.38490        0.65010        1.09080
H         -1.62520        1.42150       -0.57450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

