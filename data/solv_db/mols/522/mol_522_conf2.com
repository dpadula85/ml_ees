%nproc=16
%mem=2GB
%chk=mol_522_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.20360        0.21910       -0.31190
C          0.55370       -1.13180        0.00800
C         -0.91500       -0.83540       -0.04100
C         -1.05140        0.67790       -0.18390
C          0.24930        1.12120        0.48640
H          2.22730        0.28630        0.02950
H          1.01310        0.41270       -1.36250
H          0.85790       -1.33400        1.07760
H          0.88330       -1.92440       -0.66030
H         -1.36020       -1.36720       -0.89230
H         -1.43140       -1.09100        0.91320
H         -1.93590        1.07840        0.29550
H         -0.93630        0.94560       -1.25180
H          0.46050        2.17280        0.35880
H          0.18150        0.76980        1.53460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

