%nproc=16
%mem=2GB
%chk=mol_522_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.03330       -0.57810       -0.41620
C         -1.01050        0.74540        0.27340
C          0.43490        1.11990        0.30720
C          1.24570       -0.13750        0.27700
C          0.33370       -1.18080       -0.33390
H         -1.27380       -0.39690       -1.48970
H         -1.81990       -1.20000        0.05650
H         -1.46150        0.74670        1.27610
H         -1.55280        1.52280       -0.32280
H          0.60660        1.74840        1.19680
H          0.69160        1.70310       -0.60710
H          1.64230       -0.44580        1.26040
H          2.13810       -0.03310       -0.39640
H          0.36180       -2.12570        0.27070
H          0.69710       -1.48820       -1.35210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

