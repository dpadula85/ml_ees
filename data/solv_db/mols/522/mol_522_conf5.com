%nproc=16
%mem=2GB
%chk=mol_522_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.58980        1.07990        0.03130
C          1.21400       -0.29400       -0.10420
C          0.09410       -1.18080        0.42000
C         -1.04220       -0.56680       -0.41030
C         -0.90250        0.87710        0.08210
H          0.84610        1.69110       -0.85530
H          0.98340        1.62660        0.91470
H          1.28640       -0.52840       -1.20040
H          2.14930       -0.41230        0.43240
H         -0.07800       -0.89790        1.46630
H          0.23890       -2.23330        0.22200
H         -0.70910       -0.59070       -1.44730
H         -2.00600       -1.00010       -0.19250
H         -1.47580        1.58240       -0.51480
H         -1.18860        0.84710        1.15600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_522_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

