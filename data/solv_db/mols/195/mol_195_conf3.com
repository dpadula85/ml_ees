%nproc=16
%mem=2GB
%chk=mol_195_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.15130       -1.28500        0.19700
C         -1.18930       -0.30730       -0.36050
C         -1.03230        0.95670        0.49280
C          0.33830        1.45500        0.18660
C          1.30740        0.64790       -0.17810
C          1.18780       -0.82160       -0.33380
H         -0.11110       -1.18460        1.29800
H         -0.41250       -2.29850       -0.14180
H         -0.88410        0.02060       -1.39130
H         -2.19660       -0.70350       -0.33540
H         -1.81490        1.69820        0.25290
H         -1.06590        0.67670        1.57200
H          0.51730        2.52120        0.27450
H          2.28560        1.11900       -0.38150
H          1.25640       -1.12980       -1.41120
H          1.96500       -1.36490        0.25970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

