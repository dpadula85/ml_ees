%nproc=16
%mem=2GB
%chk=mol_195_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23410        0.27190        0.34770
C         -0.74680       -1.06800       -0.15110
C          0.73500       -1.22810       -0.08520
C          1.45090       -0.01500        0.34940
C          0.96330        1.21390        0.22920
C         -0.39550        1.32610       -0.38920
H         -2.28310        0.37590        0.02720
H         -1.08490        0.41950        1.42630
H         -1.02990       -1.10480       -1.24440
H         -1.28530       -1.89340        0.37690
H          0.97650       -2.05120        0.61680
H          1.18070       -1.52480       -1.07460
H          2.43230       -0.14620        0.79150
H          1.45810        2.11080        0.53480
H         -0.81990        2.32770       -0.29970
H         -0.31750        0.98570       -1.45550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

