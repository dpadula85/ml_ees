%nproc=16
%mem=2GB
%chk=mol_195_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.67890       -1.11270        0.16010
C         -0.75340       -0.91070        0.57340
C         -1.35780        0.08320       -0.43010
C         -0.70320        1.37180       -0.13100
C          0.60590        1.38460       -0.02650
C          1.39560        0.13320       -0.20930
H          1.21150       -1.59810        1.02980
H          0.65330       -1.84810       -0.69680
H         -1.33370       -1.83060        0.59630
H         -0.77390       -0.42350        1.58870
H         -1.04440       -0.24160       -1.44580
H         -2.45640        0.10520       -0.31490
H         -1.30220        2.24460       -0.00980
H          1.10430        2.33810        0.20170
H          2.32750        0.26920        0.38920
H          1.74800        0.03550       -1.27520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

