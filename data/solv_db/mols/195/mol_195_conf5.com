%nproc=16
%mem=2GB
%chk=mol_195_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.04380       -0.93130        0.88070
C         -1.14040       -0.65770       -0.05410
C         -1.16430        0.83660       -0.27890
C          0.19090        1.40940       -0.41040
C          1.32010        0.72100       -0.29900
C          1.27180       -0.74130       -0.02100
H          0.00830       -1.92520        1.32010
H          0.08610       -0.16880        1.68780
H         -2.05620       -1.05520        0.37160
H         -0.89410       -1.11180       -1.05500
H         -1.78230        1.04510       -1.20110
H         -1.66350        1.29350        0.62540
H          0.25300        2.48360       -0.61530
H          2.28370        1.17400       -0.40140
H          1.06530       -1.24840       -1.00400
H          2.17790       -1.12340        0.45480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

