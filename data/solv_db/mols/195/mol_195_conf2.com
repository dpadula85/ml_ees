%nproc=16
%mem=2GB
%chk=mol_195_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.37320       -1.19130        0.42400
C         -1.03630       -0.65770        0.45280
C         -1.22280        0.57690       -0.39600
C         -0.13380        1.52830       -0.02700
C          1.10090        1.04300       -0.08160
C          1.26030       -0.38000       -0.49960
H          0.41770       -2.27370        0.19490
H          0.80950       -1.03880        1.45410
H         -1.34290       -0.36790        1.49240
H         -1.73220       -1.46220        0.13960
H         -2.21090        1.01360       -0.23170
H         -1.04420        0.27210       -1.44590
H         -0.37610        2.54480        0.26240
H          1.93680        1.65930        0.16680
H          2.29650       -0.73720       -0.36400
H          0.90430       -0.52920       -1.54120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_195_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

