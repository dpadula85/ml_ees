%nproc=16
%mem=2GB
%chk=mol_549_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.02700       -0.01850       -0.34660
C         -1.86170        0.90490       -0.19490
C         -0.93350        0.44190        0.92170
C         -0.39960       -0.92850        0.64000
C          0.38620       -1.00330       -0.63620
O          1.48710       -0.13040       -0.59770
C          2.52330       -0.19330        0.30590
O          2.50680       -1.09560        1.17350
C          3.61600        0.81390        0.21060
H         -3.10850       -0.60710        0.60800
H         -2.93900       -0.66690       -1.22570
H         -3.98690        0.56080       -0.47980
H         -1.29050        0.95640       -1.14210
H         -2.27090        1.91360        0.08720
H         -1.59580        0.34800        1.82840
H         -0.17650        1.18500        1.16450
H         -1.30960       -1.58850        0.48650
H          0.11560       -1.34050        1.51490
H          0.70880       -2.06770       -0.82620
H         -0.21880       -0.73490       -1.52160
H          4.45940        0.54840        0.89970
H          3.26370        1.82880        0.55820
H          4.05160        0.87350       -0.80200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

