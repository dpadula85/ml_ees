%nproc=16
%mem=2GB
%chk=mol_549_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.25760        1.03990        0.45970
C         -2.54010       -0.21560        0.02850
C         -1.10320        0.09460       -0.33850
C         -0.42640       -1.20410       -0.76540
C          1.01860       -0.88010       -1.13240
O          1.60420       -0.33470        0.04840
C          2.91630        0.07680        0.09170
O          3.60300       -0.03930       -0.94990
C          3.53890        0.65080        1.32840
H         -3.91330        0.84890        1.31480
H         -2.52780        1.81880        0.77530
H         -3.90730        1.39650       -0.37560
H         -3.06680       -0.72570       -0.79800
H         -2.54580       -0.93370        0.88860
H         -0.61450        0.46390        0.58590
H         -1.10540        0.82110       -1.16350
H         -0.41290       -1.88570        0.11420
H         -0.96690       -1.71340       -1.56130
H          1.51660       -1.80940       -1.45620
H          1.05270       -0.17540       -1.98080
H          4.13930        1.55500        1.06150
H          4.20300       -0.14000        1.74400
H          2.79550        0.91230        2.08050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

