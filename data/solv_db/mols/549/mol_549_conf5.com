%nproc=16
%mem=2GB
%chk=mol_549_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.68170        0.50600       -0.30340
C         -2.21470        0.22570       -0.67470
C         -1.67500       -0.70720        0.36760
C         -0.24720       -1.03130        0.06190
C          0.60740        0.21240        0.05400
O          1.93670       -0.25020       -0.24070
C          3.01540        0.58190       -0.33760
O          2.85740        1.81450       -0.16370
C          4.38090        0.04240       -0.64800
H         -4.12370        1.00380       -1.17710
H         -4.14810       -0.50340       -0.10590
H         -3.74470        1.06120        0.63480
H         -1.69030        1.17980       -0.72630
H         -2.25180       -0.22430       -1.67880
H         -1.75280       -0.24700        1.37570
H         -2.27320       -1.64480        0.41750
H         -0.13850       -1.56140       -0.90890
H          0.19040       -1.69850        0.85090
H          0.30090        0.96060       -0.70330
H          0.56000        0.64840        1.07200
H          4.29040       -1.05440       -0.83260
H          4.98540        0.18960        0.27470
H          4.81670        0.49630       -1.54140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

