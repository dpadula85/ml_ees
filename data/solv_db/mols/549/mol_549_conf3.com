%nproc=16
%mem=2GB
%chk=mol_549_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.74870       -1.13720        0.69470
C         -2.62120       -0.15750       -0.45100
C         -1.62450        0.91540       -0.02130
C         -0.31030        0.16720        0.25780
C          0.67400        1.20710        0.67960
O          1.92640        0.64580        0.97090
C          2.65690       -0.02000        0.02580
O          2.27420       -0.17690       -1.15600
C          3.99780       -0.60860        0.39200
H         -3.75500       -1.60030        0.66850
H         -1.97830       -1.92150        0.55510
H         -2.56430       -0.58260        1.63050
H         -2.24980       -0.71310       -1.32750
H         -3.60440        0.27210       -0.71520
H         -1.91150        1.36380        0.94440
H         -1.49740        1.70070       -0.76910
H         -0.55370       -0.50170        1.12430
H         -0.08330       -0.38280       -0.64950
H          0.73290        1.95720       -0.14460
H          0.25930        1.70290        1.60570
H          4.64680       -0.46860       -0.47500
H          3.91150       -1.65420        0.70810
H          4.42240       -0.00730        1.22530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

