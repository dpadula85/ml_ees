%nproc=16
%mem=2GB
%chk=mol_549_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.87620        0.02070        0.24880
C         -2.51440        0.63540        0.19040
C         -1.46500       -0.40010       -0.16140
C         -0.08080        0.22140       -0.22150
C          0.88040       -0.87080       -0.57290
O          2.21440       -0.48980       -0.67750
C          2.95630        0.05230        0.35470
O          2.39450        0.21430        1.46950
C          4.39540        0.44350        0.16930
H         -3.91170       -0.98240       -0.26930
H         -4.65100        0.63330       -0.27960
H         -4.16200       -0.19920        1.30030
H         -2.24370        1.07790        1.18180
H         -2.52430        1.44190       -0.57030
H         -1.42620       -1.18790        0.61730
H         -1.68690       -0.82220       -1.15130
H         -0.05840        0.99960       -1.01540
H          0.12750        0.72430        0.72950
H          0.60790       -1.35040       -1.55480
H          0.72090       -1.69090        0.17380
H          4.89900        0.35060        1.15150
H          4.91690       -0.27840       -0.51430
H          4.48740        1.45670       -0.26080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_549_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

