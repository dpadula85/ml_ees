%nproc=16
%mem=2GB
%chk=mol_513_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.81220       -0.18090        1.43340
C          2.76210       -0.15040        0.35090
C          1.63230        0.76060        0.78010
C          0.56000        0.83230       -0.28680
N         -0.01400       -0.43210       -0.53740
C         -0.93020       -0.56340       -1.58250
C         -2.25400        0.07820       -1.52030
C         -3.19120       -0.28660       -0.45000
C         -2.71610        0.00100        0.93260
H          3.68320       -0.98350        2.17390
H          4.83700       -0.25800        1.00410
H          3.83280        0.78430        2.01800
H          3.23050        0.17800       -0.59670
H          2.35650       -1.18330        0.28870
H          1.24490        0.40730        1.74410
H          2.05040        1.77970        0.89790
H         -0.16950        1.60060        0.07860
H          1.03230        1.21530       -1.20950
H          0.26190       -1.23230        0.02140
H         -1.12170       -1.67730       -1.71900
H         -0.41720       -0.24810       -2.53560
H         -2.78910       -0.15830       -2.50210
H         -2.17750        1.21950       -1.58980
H         -3.43750       -1.37670       -0.51390
H         -4.19120        0.23820       -0.56410
H         -2.48660        1.10280        1.01030
H         -1.84760       -0.60110        1.19780
H         -3.55290       -0.21430        1.67600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

