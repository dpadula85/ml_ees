%nproc=16
%mem=2GB
%chk=mol_513_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.44290        1.54820        0.26890
C          2.36000        0.82280       -0.51190
C          2.25690       -0.58960        0.07480
C          1.24000       -1.40180       -0.63200
N         -0.12030       -0.95110       -0.60000
C         -0.72560       -1.07260        0.72230
C         -2.20450       -0.74150        0.62580
C         -2.35880        0.67170        0.14670
C         -3.84150        1.01720        0.00930
H          3.02010        1.94350        1.21670
H          4.20340        0.78370        0.54890
H          3.97670        2.29370       -0.34480
H          1.40690        1.34800       -0.44310
H          2.66610        0.78310       -1.57770
H          3.24360       -1.08260       -0.02380
H          2.05550       -0.42250        1.15420
H          1.57170       -1.48780       -1.69020
H          1.27850       -2.43500       -0.18300
H         -0.70800       -1.55130       -1.22370
H         -0.64970       -2.15190        1.03980
H         -0.22420       -0.42910        1.45180
H         -2.71580       -0.91460        1.59820
H         -2.71150       -1.46400       -0.09050
H         -1.86300        0.82020       -0.80880
H         -1.94990        1.38430        0.89230
H         -4.33960        1.12960        0.98540
H         -4.38870        0.20740       -0.53090
H         -3.92140        1.94180       -0.58710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

