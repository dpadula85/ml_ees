%nproc=16
%mem=2GB
%chk=mol_513_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.12060        1.22650        0.48500
C         -3.10020       -0.28550        0.68730
C         -2.64840       -0.96010       -0.61330
C         -1.27160       -0.45560       -0.99670
N         -0.24610       -0.80590       -0.04960
C          0.93420       -0.00870       -0.28430
C          2.08160       -0.59890        0.48370
C          3.32030        0.25360        0.34910
C          3.80200        0.47660       -1.04870
H         -3.85710        1.53590       -0.27450
H         -2.10080        1.60480        0.24690
H         -3.40110        1.67760        1.44830
H         -4.14770       -0.60560        0.85490
H         -2.50680       -0.55110        1.55600
H         -3.39250       -0.81330       -1.37770
H         -2.53620       -2.02890       -0.39350
H         -1.03190       -1.05400       -1.93930
H         -1.23510        0.59940       -1.29530
H         -0.57940       -0.62240        0.91090
H          0.74390        1.00660        0.16690
H          1.12800        0.06200       -1.35410
H          2.31730       -1.65100        0.20130
H          1.85140       -0.59730        1.59760
H          3.07160        1.21710        0.89320
H          4.12160       -0.24740        0.95690
H          3.02850        0.28230       -1.80880
H          4.10970        1.54570       -1.20180
H          4.66550       -0.20270       -1.32490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

