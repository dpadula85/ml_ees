%nproc=16
%mem=2GB
%chk=mol_513_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.56730        0.11240        0.13680
C         -3.12200       -0.24850        0.42420
C         -2.26090        0.19700       -0.74120
C         -0.81950       -0.14080       -0.56200
N         -0.16170        0.42930        0.56390
C          1.17940       -0.13270        0.61780
C          2.08740        0.30580       -0.48500
C          3.42820       -0.37510       -0.31120
C          4.05040       -0.00810        1.00790
H         -4.58120        1.08620       -0.40450
H         -5.20270        0.11110        1.04720
H         -4.91650       -0.66280       -0.60000
H         -2.84450        0.40010        1.30540
H         -2.97680       -1.30970        0.68690
H         -2.41220        1.25540       -0.95530
H         -2.59690       -0.37420       -1.65500
H         -0.63410       -1.23170       -0.59920
H         -0.29540        0.26820       -1.47910
H         -0.09900        1.47390        0.51340
H          1.67730        0.11380        1.58490
H          1.13110       -1.25830        0.56760
H          1.65290        0.04370       -1.48270
H          2.25340        1.40860       -0.50780
H          3.29910       -1.49570       -0.34300
H          4.11570       -0.13760       -1.14800
H          3.54080       -0.62310        1.80120
H          3.93950        1.07830        1.18810
H          5.13550       -0.28560        1.01370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

