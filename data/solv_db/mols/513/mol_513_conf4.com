%nproc=16
%mem=2GB
%chk=mol_513_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.88420        0.37460       -0.13290
C          2.85640       -0.44500       -0.85310
C          1.66520        0.43090       -1.30420
C          1.04470        1.07560       -0.08480
N          0.57320        0.15340        0.86740
C         -0.57030       -0.62550        0.55170
C         -1.78510        0.29170        0.57810
C         -3.05740       -0.45990        0.24860
C         -4.17840        0.57470        0.32950
H          3.73270        1.45730       -0.21030
H          4.90510        0.15510       -0.51180
H          3.86130        0.07930        0.95300
H          3.23980       -1.00060       -1.70510
H          2.38890       -1.18530       -0.14160
H          2.11340        1.19540       -1.96820
H          0.99150       -0.19520       -1.88740
H          0.22720        1.73550       -0.42320
H          1.79810        1.74030        0.37200
H          0.71070        0.39220        1.85940
H         -0.47480       -0.99920       -0.50620
H         -0.76110       -1.49850        1.18260
H         -1.89070        0.67640        1.61290
H         -1.60500        1.13460       -0.07920
H         -3.02450       -0.92960       -0.76210
H         -3.29270       -1.22660        1.01240
H         -5.18170        0.09470        0.38180
H         -4.03790        1.20290        1.20770
H         -4.13270        1.20080       -0.58690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_513_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

