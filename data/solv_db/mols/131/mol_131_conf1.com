%nproc=16
%mem=2GB
%chk=mol_131_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.77530       -0.02510        0.04710
C         -0.54200        0.01650       -0.03280
Cl        -1.51200       -1.42640        0.10880
Cl        -1.35570        1.54060       -0.28590
H          1.36200        0.87800       -0.04270
H          1.27240       -0.98360        0.20540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_131_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_131_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_131_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

