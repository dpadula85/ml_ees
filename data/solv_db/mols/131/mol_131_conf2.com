%nproc=16
%mem=2GB
%chk=mol_131_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.75260       -0.06700       -0.00890
C          0.55540        0.05490        0.00500
Cl         1.52420       -1.32850        0.47040
Cl         1.25640        1.59180       -0.44230
H         -1.21050       -1.02380        0.26850
H         -1.37290        0.77260       -0.29260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_131_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_131_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_131_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

