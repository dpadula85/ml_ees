%nproc=16
%mem=2GB
%chk=mol_083_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.17450       -0.15100        0.08770
C          0.75220       -0.40590        0.43890
O          0.52370       -1.26700        1.26980
C         -0.29420        0.38260       -0.22450
C         -1.60000        0.64440        0.43380
C         -1.55040       -0.43190       -0.66400
H          2.31770        0.95980        0.18060
H          2.88570       -0.69280        0.72800
H          2.29570       -0.40380       -0.98870
H          0.02290        1.14210       -0.96630
H         -2.16570        1.55280        0.13810
H         -1.78070        0.31730        1.47180
H         -1.96480       -0.18530       -1.64730
H         -1.61660       -1.46130       -0.25810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

