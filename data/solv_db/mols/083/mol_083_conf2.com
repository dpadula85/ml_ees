%nproc=16
%mem=2GB
%chk=mol_083_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.64860        0.54950        0.42650
C          0.99430       -0.25550       -0.63850
O          1.70290       -0.58710       -1.57140
C         -0.41390       -0.65770       -0.62420
C         -1.39360        0.48590       -0.23380
C         -1.21170       -0.71120        0.61940
H          1.17470        0.38750        1.41670
H          1.54170        1.62040        0.13190
H          2.72890        0.31070        0.48720
H         -0.76530       -1.25050       -1.47420
H         -2.29430        0.60920       -0.86820
H         -0.93900        1.45850        0.04130
H         -0.74680       -0.49820        1.62010
H         -2.02640       -1.46140        0.66720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

