%nproc=16
%mem=2GB
%chk=mol_083_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.19530       -0.13440       -0.12630
C         -0.76060       -0.46600       -0.18360
O         -0.44830       -1.53850       -0.67630
C          0.33350        0.39360        0.30690
C          1.56240        0.42160       -0.59980
C          1.62230       -0.17970        0.76130
H         -2.64690       -0.32330        0.86600
H         -2.76140       -0.69670       -0.86620
H         -2.31700        0.94770       -0.32520
H          0.02250        1.36120        0.74930
H          1.59470       -0.28490       -1.45690
H          2.03680        1.38900       -0.82240
H          1.79020       -1.27100        0.82460
H          2.16730        0.38120        1.54840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

