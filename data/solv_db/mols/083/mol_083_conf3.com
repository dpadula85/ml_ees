%nproc=16
%mem=2GB
%chk=mol_083_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.17750        0.16620       -0.22630
C          0.82970       -0.12290        0.31950
O          0.72180       -0.71680        1.37990
C         -0.43570        0.27100       -0.35300
C         -1.60460       -0.72820       -0.21890
C         -1.61390        0.53500        0.54090
H          2.74780       -0.79820       -0.21090
H          2.74580        0.89610        0.37200
H          2.12610        0.47180       -1.29170
H         -0.36990        0.81770       -1.32790
H         -2.21190       -0.89580       -1.12130
H         -1.38060       -1.67320        0.33770
H         -2.30090        1.31800        0.16550
H         -1.43110        0.45950        1.63470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

