%nproc=16
%mem=2GB
%chk=mol_083_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.15830        0.10030        0.38970
C          0.85710       -0.17770       -0.27770
O          0.88660       -0.81870       -1.31410
C         -0.45580        0.29040        0.26470
C         -1.58220       -0.69920        0.08190
C         -1.57480        0.58640       -0.66060
H          2.53790       -0.77950        0.95640
H          2.07240        0.92540        1.12450
H          2.92190        0.29960       -0.39670
H         -0.45570        0.87020        1.20690
H         -1.39860       -1.63030       -0.50350
H         -2.29200       -0.86710        0.93590
H         -1.38090        0.52330       -1.76120
H         -2.29410        1.37700       -0.39940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_083_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

