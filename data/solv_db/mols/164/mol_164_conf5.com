%nproc=16
%mem=2GB
%chk=mol_164_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.40710       -0.46630        0.01840
C          0.97130       -0.54100       -0.43590
C          0.21620        0.59840        0.18910
C         -1.23910        0.62050       -0.19970
C         -1.86690       -0.67920        0.25670
O         -1.82360        1.70700        0.46160
H          3.11440       -0.86340       -0.73900
H          2.47670       -1.04060        0.98670
H          2.63560        0.59320        0.25270
H          0.55450       -1.53450       -0.13720
H          0.95850       -0.49480       -1.54660
H          0.34480        0.50830        1.29220
H          0.74050        1.53430       -0.09770
H         -1.31600        0.71350       -1.28920
H         -2.76180       -0.42960        0.86340
H         -2.11150       -1.34680       -0.60580
H         -1.17320       -1.26940        0.91460
H         -2.12780        2.39030       -0.18420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

