%nproc=16
%mem=2GB
%chk=mol_164_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.55040        0.06910        0.03620
C          1.16570        0.59400       -0.14740
C          0.10690       -0.45040        0.18250
C         -1.27610        0.14090       -0.02270
C         -2.33980       -0.87580        0.29810
O         -1.43110        1.22240        0.83200
H          3.01160        0.43510        0.99640
H          3.22170        0.45000       -0.76560
H          2.60930       -1.03450        0.09000
H          1.02190        1.49360        0.48280
H          0.98330        0.91590       -1.19200
H          0.25630       -1.34220       -0.44340
H          0.24090       -0.72380        1.23930
H         -1.38760        0.47070       -1.07050
H         -2.33500       -1.70750       -0.42800
H         -3.32960       -0.39230        0.30310
H         -2.11810       -1.27940        1.32290
H         -0.95070        2.01410        0.48890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

