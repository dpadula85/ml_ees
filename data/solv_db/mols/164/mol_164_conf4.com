%nproc=16
%mem=2GB
%chk=mol_164_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.43350       -0.12130        0.08180
C         -0.99020       -0.45330       -0.30950
C         -0.12800        0.62740        0.24990
C          1.33110        0.46090       -0.04080
C          1.91900       -0.79890        0.49760
O          1.51620        0.54620       -1.43040
H         -2.66170        0.84240       -0.43390
H         -2.47500        0.01560        1.18420
H         -3.13390       -0.86730       -0.29960
H         -0.74920       -1.42400        0.18840
H         -0.91810       -0.58250       -1.38640
H         -0.31910        0.69680        1.34210
H         -0.44110        1.60670       -0.18890
H          1.85730        1.33510        0.41740
H          2.05580       -1.51100       -0.34080
H          2.96390       -0.57930        0.86370
H          1.33510       -1.28770        1.27530
H          1.27140        1.49430       -1.67020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

