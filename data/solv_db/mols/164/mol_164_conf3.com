%nproc=16
%mem=2GB
%chk=mol_164_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.51000        0.19380       -0.29010
C          1.30380       -0.56300        0.20320
C          0.09340        0.31940       -0.03760
C         -1.18800       -0.34390        0.42360
C         -2.31310        0.63470        0.12860
O         -1.36260       -1.56200       -0.18410
H          2.25730        1.16490       -0.74210
H          3.18290        0.40680        0.57060
H          3.08690       -0.39320       -1.01220
H          1.37420       -0.84420        1.27330
H          1.14550       -1.47690       -0.42660
H          0.22510        1.22040        0.60080
H         -0.01690        0.57140       -1.09600
H         -1.16030       -0.52820        1.53430
H         -3.04090        0.61760        0.96700
H         -2.78450        0.41200       -0.84720
H         -1.91020        1.67250        0.09940
H         -1.40260       -1.50200       -1.16490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

