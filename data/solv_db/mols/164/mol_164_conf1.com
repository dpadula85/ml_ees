%nproc=16
%mem=2GB
%chk=mol_164_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.12630       -0.19370       -0.29990
C          1.31780        0.70240        0.59730
C         -0.10930        0.82280        0.11180
C         -0.82440       -0.49320        0.06170
C         -2.23120       -0.22960       -0.43720
O         -0.23580       -1.42800       -0.75650
H          2.10050       -1.25780        0.01070
H          1.78690       -0.15210       -1.35310
H          3.16810        0.16920       -0.28080
H          1.73400        1.73120        0.63200
H          1.30320        0.32510        1.64630
H         -0.12370        1.26250       -0.90890
H         -0.68570        1.50110        0.76300
H         -0.86560       -0.87410        1.12300
H         -2.38170       -0.70750       -1.44530
H         -2.95580       -0.61700        0.31030
H         -2.44160        0.84110       -0.57230
H         -0.68210       -1.40240       -1.64750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_164_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

