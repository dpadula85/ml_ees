%nproc=16
%mem=2GB
%chk=mol_622_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.43750       -0.46200       -0.24400
C         -1.09570        0.26860       -0.39370
O         -0.34980       -0.02000        0.70510
C          0.88480        0.53340        0.76980
C          1.81810        0.16410       -0.34650
O          2.04660       -1.20970       -0.40810
H         -2.34460       -1.53490       -0.46340
H         -2.76380       -0.33100        0.81280
H         -3.21310        0.00940       -0.88240
H         -1.35500        1.32890       -0.52580
H         -0.60170       -0.17400       -1.28610
H          0.81340        1.63160        0.91090
H          1.37210        0.15880        1.71930
H          1.50310        0.47900       -1.33690
H          2.81090        0.62830       -0.08570
H          2.91230       -1.47070       -0.01450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

