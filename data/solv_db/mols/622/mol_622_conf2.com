%nproc=16
%mem=2GB
%chk=mol_622_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08240        0.39200       -0.21020
C         -1.42580       -0.75740        0.49450
O         -0.11440       -0.96030        0.15440
C          0.72380        0.08820        0.40670
C          2.12950       -0.32860       -0.03720
O          3.00570        0.70760        0.20270
H         -3.08410        0.53490        0.29310
H         -1.55090        1.34580       -0.12050
H         -2.33810        0.11890       -1.26000
H         -1.99120       -1.67990        0.19720
H         -1.60830       -0.63970        1.58140
H          0.44460        0.94240       -0.25200
H          0.72290        0.45000        1.44610
H          2.41770       -1.20420        0.58180
H          2.04390       -0.58890       -1.11140
H          2.70720        1.57910       -0.10080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

