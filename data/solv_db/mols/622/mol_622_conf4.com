%nproc=16
%mem=2GB
%chk=mol_622_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51460        0.14980        0.17070
C         -1.09460       -0.12230       -0.32720
O         -0.20570       -0.22050        0.71030
C          1.06890       -0.49430        0.31350
C          1.61390        0.56860       -0.57480
O          2.93500        0.25890       -0.95910
H         -3.04480       -0.83160        0.28230
H         -3.05490        0.78110       -0.53820
H         -2.52120        0.62720        1.15150
H         -0.86430        0.68790       -1.06260
H         -1.07640       -1.08050       -0.88640
H          1.14910       -1.51450       -0.13060
H          1.70820       -0.54980        1.24360
H          1.60490        1.54290       -0.08730
H          1.03800        0.62500       -1.52320
H          3.25850       -0.42780       -0.32790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

