%nproc=16
%mem=2GB
%chk=mol_622_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.96590        0.92610       -0.02050
C         -1.52630       -0.41640       -0.49920
O         -0.15690       -0.51980       -0.70280
C          0.55740       -0.27880        0.46290
C          2.03660       -0.41420        0.14110
O          2.44280        0.47560       -0.82090
H         -1.16280        1.66520       -0.17690
H         -2.27390        0.87950        1.06260
H         -2.86260        1.32440       -0.54930
H         -2.01400       -0.73070       -1.44430
H         -1.79690       -1.18010        0.25590
H          0.29900       -0.94590        1.29270
H          0.40850        0.77940        0.77810
H          2.64640       -0.25930        1.05860
H          2.24980       -1.43860       -0.21380
H          3.11880        0.13360       -1.44990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

