%nproc=16
%mem=2GB
%chk=mol_622_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.45400       -0.20000        0.01360
C          1.01500        0.17090        0.29890
O          0.27490        0.38960       -0.82630
C         -1.01280        0.73690       -0.47400
C         -1.76150       -0.28060        0.32000
O         -1.86490       -1.48190       -0.41180
H          3.03850        0.05960        0.92060
H          2.85860        0.37840       -0.82860
H          2.57940       -1.30730       -0.11520
H          1.03280        1.08940        0.91870
H          0.55160       -0.66390        0.88010
H         -1.62460        1.04680       -1.36130
H         -0.95410        1.66210        0.17420
H         -1.38190       -0.41530        1.33720
H         -2.84630        0.04260        0.40180
H         -2.35870       -1.22720       -1.24790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_622_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

