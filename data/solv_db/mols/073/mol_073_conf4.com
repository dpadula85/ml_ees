%nproc=16
%mem=2GB
%chk=mol_073_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93150       -0.30930       -0.09580
C          0.54470       -0.26800       -0.67440
C         -0.43090       -0.02410        0.45180
C         -1.84820        0.03290       -0.03320
O         -0.13010        1.12840        1.15020
H          2.04550       -1.29100        0.41700
H          2.09160        0.46990        0.67790
H          2.66550       -0.19160       -0.90390
H          0.47820        0.51340       -1.44840
H          0.29770       -1.25400       -1.12260
H         -0.35160       -0.87000        1.17050
H         -2.52950       -0.08550        0.82650
H         -2.01900        1.03390       -0.47800
H         -2.03070       -0.75620       -0.80980
H         -0.71460        1.87110        0.87230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

