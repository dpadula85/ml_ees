%nproc=16
%mem=2GB
%chk=mol_073_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.61530        0.44370        0.22490
C          0.65930       -0.74640        0.24510
C         -0.55100       -0.36200       -0.56550
C         -1.18890        0.83970        0.05060
O         -1.39460       -1.43430       -0.73590
H          2.41820        0.16030       -0.50190
H          2.01090        0.65270        1.23650
H          1.12910        1.34290       -0.21200
H          1.12690       -1.66060       -0.14810
H          0.40420       -0.93500        1.30160
H         -0.16700       -0.08780       -1.58710
H         -1.27530        1.66170       -0.69680
H         -0.62370        1.18730        0.96490
H         -2.19130        0.57850        0.39910
H         -1.97220       -1.64060        0.02480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

