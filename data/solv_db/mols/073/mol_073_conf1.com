%nproc=16
%mem=2GB
%chk=mol_073_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.89670        0.13580        0.13930
C          0.47650        0.62700        0.03610
C         -0.47450       -0.50510       -0.28740
C         -1.86930        0.10090       -0.36720
O         -0.49890       -1.48460        0.66410
H          2.33890        0.33950        1.13030
H          1.89510       -0.95500       -0.09080
H          2.54950        0.60970       -0.63080
H          0.43990        1.39730       -0.75960
H          0.19230        1.14430        0.98400
H         -0.15750       -0.90780       -1.27860
H         -2.59110       -0.64600       -0.73830
H         -1.79710        0.99800       -1.03450
H         -2.15460        0.36740        0.66980
H         -0.24590       -1.22130        1.56350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

