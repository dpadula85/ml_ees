%nproc=16
%mem=2GB
%chk=mol_073_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97440       -0.10350       -0.01560
C         -0.57860       -0.57640       -0.30570
C          0.43140        0.46600        0.17050
C          1.83520        0.02440       -0.10440
O          0.19470        0.65300        1.54530
H         -1.94080        0.59460        0.82110
H         -2.61410       -0.96430        0.20880
H         -2.39030        0.42870       -0.88610
H         -0.41990       -0.84350       -1.36420
H         -0.42660       -1.51010        0.30530
H          0.24960        1.40890       -0.38730
H          2.33460        0.61880       -0.89990
H          2.44170        0.10890        0.82660
H          1.80690       -1.05170       -0.39170
H          1.05060        0.74620        2.03090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

