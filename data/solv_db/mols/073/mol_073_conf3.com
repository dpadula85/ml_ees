%nproc=16
%mem=2GB
%chk=mol_073_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.63430       -0.53970       -0.08750
C          0.75650        0.57280        0.29550
C         -0.67810        0.47070       -0.08540
C         -1.43850       -0.67010        0.44560
O         -0.76620        0.54220       -1.48760
H          2.24200       -0.33820       -1.01170
H          1.14810       -1.50960       -0.25730
H          2.39740       -0.70520        0.71550
H          1.15130        1.51290       -0.15660
H          0.80090        0.76450        1.40830
H         -1.17660        1.40710        0.28540
H         -2.05290       -0.41840        1.35440
H         -2.20130       -0.98420       -0.32290
H         -0.87170       -1.56940        0.70550
H         -0.94520        1.46450       -1.80120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_073_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

