%nproc=16
%mem=2GB
%chk=mol_156_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.56260        0.99490       -0.96830
C         -1.48860        0.01440        0.17980
C         -0.63670       -1.17750       -0.13140
C          0.77140       -0.90570       -0.41090
C          1.37240        0.26070       -0.39450
C          2.80540        0.38150       -0.72280
O          3.43980       -0.65480       -1.01890
H         -2.58880        1.09130       -1.39080
H         -0.86720        0.69380       -1.78250
H         -1.31760        1.99570       -0.56960
H         -2.55850       -0.29430        0.37570
H         -1.17520        0.53060        1.11430
H         -1.12530       -1.80780       -0.90960
H         -0.67010       -1.81740        0.80190
H          1.42150       -1.75940       -0.66680
H          0.85260        1.13070       -0.04330
H          3.32740        1.32330       -0.71840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

