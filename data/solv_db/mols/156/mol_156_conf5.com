%nproc=16
%mem=2GB
%chk=mol_156_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.67400       -0.17510       -0.27830
C         -1.32160        0.50930       -0.26520
C         -0.39340       -0.45290        0.49080
C          0.93540        0.19370        0.51720
C          1.99520       -0.38050       -0.02520
C          3.28020        0.30350        0.03070
O          4.32180       -0.19060       -0.46600
H         -2.85180       -0.54620        0.74630
H         -2.57440       -1.06440       -0.92620
H         -3.44800        0.53210       -0.63150
H         -1.38740        1.48200        0.25880
H         -0.94790        0.62110       -1.30480
H         -0.40170       -1.39130       -0.07490
H         -0.81490       -0.52890        1.50810
H          1.02330        1.16090        0.99670
H          1.88200       -1.33610       -0.49310
H          3.37720        1.26340        0.50440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

