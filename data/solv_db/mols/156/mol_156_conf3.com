%nproc=16
%mem=2GB
%chk=mol_156_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99700        0.85040        0.46270
C         -1.57900       -0.17540       -0.53720
C         -0.44310       -1.05590        0.00080
C          0.71030       -0.18630        0.31060
C          1.86120       -0.39610       -0.31510
C          3.00970        0.43880       -0.03350
O          2.92080        1.34670        0.78840
H         -1.47610        1.81250        0.20250
H         -3.09970        1.05530        0.36440
H         -1.82760        0.56040        1.51050
H         -2.44850       -0.82600       -0.78300
H         -1.22000        0.29210       -1.48960
H         -0.80450       -1.60390        0.89940
H         -0.14300       -1.76270       -0.79500
H          0.65010        0.61140        1.02540
H          1.96070       -1.20630       -1.05660
H          3.92560        0.24530       -0.55460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

