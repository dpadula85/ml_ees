%nproc=16
%mem=2GB
%chk=mol_156_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70340        0.20820        0.14960
C         -1.25760        0.33400        0.62040
C         -0.41500       -0.50880       -0.30720
C          0.99290       -0.41270        0.11660
C          1.94810        0.05320       -0.66820
C          3.30610        0.10320       -0.14530
O          4.19660        0.55150       -0.91430
H         -3.40640        0.32960        0.99360
H         -2.87370        0.94300       -0.67060
H         -2.86780       -0.81250       -0.27810
H         -0.95640        1.39160        0.47320
H         -1.15870        0.06100        1.67750
H         -0.76470       -1.56340       -0.33520
H         -0.56250       -0.10540       -1.32890
H          1.27230       -0.73070        1.10310
H          1.71060        0.38750       -1.67820
H          3.53950       -0.22930        0.85770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

