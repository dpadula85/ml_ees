%nproc=16
%mem=2GB
%chk=mol_156_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70190        0.15870        0.06770
C         -1.40350       -0.16380        0.81990
C         -0.29560        0.51780        0.02830
C          1.02900        0.29690        0.63390
C          1.95360       -0.31430       -0.08760
C          3.29180       -0.57000        0.44260
O          4.12870       -1.14400       -0.26250
H         -2.70100       -0.35950       -0.91000
H         -3.57650       -0.07100        0.69640
H         -2.62040        1.25890       -0.16370
H         -1.28540       -1.26080        0.76490
H         -1.48710        0.21970        1.84600
H         -0.50610        1.60270       -0.07400
H         -0.34450        0.09650       -0.98990
H          1.27210        0.61780        1.65140
H          1.69730       -0.62500       -1.09000
H          3.54970       -0.26060        1.44270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_156_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

