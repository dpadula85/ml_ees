%nproc=16
%mem=2GB
%chk=mol_351_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.69220        0.47750        0.01500
C          3.62160       -0.89190       -0.10480
C          2.38810       -1.50240       -0.11740
C          1.26150       -0.74780       -0.01200
C          1.28160        0.62720        0.11000
C          2.53060        1.21320        0.12000
O          0.13280        1.40040        0.21780
C         -1.10490        0.77010        0.20350
C         -1.17670       -0.60160        0.08360
C         -2.43080       -1.21340        0.07140
C         -3.60210       -0.50410        0.17440
C         -3.51860        0.86820        0.29400
C         -2.28610        1.48660        0.30740
Cl        -2.25710        3.22350        0.46140
Cl        -5.01010        1.78710        0.42640
Cl        -5.17920       -1.29090        0.15770
Cl        -2.50920       -2.94690       -0.08060
O         -0.01140       -1.34910       -0.02260
H          4.67870        0.96580        0.02520
H          4.54940       -1.47620       -0.18820
H          2.32170       -2.57930       -0.21120
H          2.62800        2.28410        0.21220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_351_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_351_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_351_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

