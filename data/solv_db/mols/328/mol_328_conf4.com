%nproc=16
%mem=2GB
%chk=mol_328_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.28790       -0.10160        0.80780
C         -1.46450        0.34730       -0.35610
C         -0.00970        0.13050       -0.12380
C          0.91900        0.48020       -1.08190
N          2.25220        0.28330       -0.87380
C          2.70470       -0.25820        0.27220
C          1.77710       -0.60850        1.23150
N          0.45810       -0.40830        1.01330
H         -3.30310        0.33730        0.68920
H         -1.79300        0.28060        1.73450
H         -2.32170       -1.20550        0.91290
H         -1.65860        1.42430       -0.54170
H         -1.75390       -0.17290       -1.30160
H          0.58430        0.91450       -2.00610
H          3.77070       -0.39840        0.39920
H          2.12620       -1.04470        2.15750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

