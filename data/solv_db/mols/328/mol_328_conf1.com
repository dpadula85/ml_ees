%nproc=16
%mem=2GB
%chk=mol_328_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25760        0.55430        0.24940
C         -1.38290       -0.68630        0.46000
C          0.03920       -0.36120        0.16600
C          0.56890       -0.50740       -1.09720
N          1.85180       -0.19340       -1.27110
C          2.64720        0.25720       -0.27400
C          2.12170        0.40570        0.99350
N          0.81910        0.08820        1.17620
H         -3.19260        0.22780       -0.24950
H         -2.54090        1.01290        1.22780
H         -1.73650        1.30520       -0.39010
H         -1.52220       -0.98080        1.52340
H         -1.70680       -1.52820       -0.17170
H         -0.09560       -0.86900       -1.86620
H          3.67840        0.51070       -0.41030
H          2.70890        0.76440        1.84150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

