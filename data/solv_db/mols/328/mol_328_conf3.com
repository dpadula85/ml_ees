%nproc=16
%mem=2GB
%chk=mol_328_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.32920       -0.40430       -0.14560
C          1.28090       -0.89870        0.78780
C         -0.07400       -0.50240        0.29580
C         -0.25150        0.21910       -0.86750
N         -1.49750        0.56830       -1.29430
C         -2.59210        0.22230       -0.59800
C         -2.42830       -0.49930        0.56700
N         -1.18960       -0.83460        0.97190
H          3.33540       -0.57290        0.33800
H          2.28970       -0.86720       -1.15040
H          2.25980        0.70660       -0.26100
H          1.49260       -0.49130        1.79620
H          1.34630       -2.01790        0.83320
H          0.60630        0.50540       -1.43830
H         -3.57830        0.50800       -0.95120
H         -3.32890       -0.76960        1.11630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

