%nproc=16
%mem=2GB
%chk=mol_328_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.28260       -0.46750        0.56730
C         -1.34410       -0.33590       -0.57950
C          0.05130       -0.01570       -0.17120
C          0.47390        1.29220       -0.04380
N          1.73730        1.60790        0.32590
C          2.63060        0.63700        0.58360
C          2.22020       -0.67210        0.45980
N          0.95550       -0.98460        0.08970
H         -2.95820       -1.34760        0.42040
H         -2.96910        0.41750        0.61840
H         -1.79700       -0.62370        1.54950
H         -1.73730        0.39410       -1.33000
H         -1.30500       -1.31540       -1.12380
H         -0.27060        2.05510       -0.25830
H          3.65140        0.83440        0.88190
H          2.94360       -1.47570        0.66860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

