%nproc=16
%mem=2GB
%chk=mol_328_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.20980        0.17600       -0.82950
C         -1.51230       -0.04050        0.47350
C         -0.03210       -0.04840        0.30950
C          0.77700       -0.23480        1.40080
N          2.12330       -0.25350        1.32660
C          2.72980       -0.08380        0.13740
C          1.92470        0.10430       -0.96590
N          0.57570        0.12110       -0.87860
H         -2.52400       -0.77440       -1.31950
H         -3.14860        0.74230       -0.64780
H         -1.60870        0.82070       -1.52330
H         -1.80680       -1.05640        0.86820
H         -1.83070        0.75090        1.18120
H          0.29750       -0.37440        2.37590
H          3.81170       -0.08930        0.01510
H          2.43320        0.24050       -1.92360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_328_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

