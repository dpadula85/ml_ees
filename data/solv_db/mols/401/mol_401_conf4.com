%nproc=16
%mem=2GB
%chk=mol_401_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.75950        1.53760       -0.15990
C         -0.53280        1.09800       -0.04390
C         -0.89020       -0.23010        0.06910
C          0.14040       -1.14740        0.06150
C          1.46780       -0.77860       -0.05210
C          1.75390        0.57400       -0.16190
Cl         2.72380       -2.00150       -0.05480
N         -2.26470       -0.62270        0.18710
H          1.04360        2.58130       -0.24920
H         -1.32100        1.82560       -0.03960
H         -0.10880       -2.20400        0.14920
H          2.79670        0.88510       -0.25220
H         -2.82210       -1.02110       -0.57310
H         -2.74620       -0.49620        1.11990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

