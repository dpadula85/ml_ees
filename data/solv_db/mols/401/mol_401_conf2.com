%nproc=16
%mem=2GB
%chk=mol_401_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.69830        1.56160       -0.08040
C         -0.60360        1.08020       -0.08140
C         -0.88340       -0.26760       -0.00750
C          0.18420       -1.13920        0.06860
C          1.49670       -0.65120        0.06950
C          1.76390        0.70090       -0.00490
Cl         2.85510       -1.77420        0.16680
N         -2.21770       -0.77530       -0.00760
H          0.86500        2.62630       -0.14000
H         -1.46830        1.74630       -0.14070
H          0.00510       -2.21430        0.12860
H          2.79290        1.01440        0.00010
H         -2.93660       -0.38320        0.66500
H         -2.55160       -1.52460       -0.63610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

