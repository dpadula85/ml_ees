%nproc=16
%mem=2GB
%chk=mol_401_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.75630        1.51100        0.08470
C          0.56230        1.11200        0.02040
C          0.90470       -0.22310       -0.04270
C         -0.13290       -1.13840       -0.03840
C         -1.45100       -0.73730        0.02590
C         -1.79090        0.59720        0.08890
Cl        -2.75410       -1.91380        0.03020
N          2.26550       -0.64900       -0.10960
H         -1.03050        2.55630        0.13420
H          1.37880        1.82090        0.01660
H          0.10670       -2.19730       -0.08710
H         -2.83680        0.89290        0.13920
H          3.00290       -0.01570       -0.43720
H          2.53150       -1.61560        0.17510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

