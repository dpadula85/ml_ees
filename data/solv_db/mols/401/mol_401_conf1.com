%nproc=16
%mem=2GB
%chk=mol_401_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12920       -1.23230        0.07950
C          0.23700       -1.22520        0.06710
C          0.93950       -0.02880       -0.00840
C          0.16660        1.12680       -0.06840
C         -1.20710        1.10730       -0.05520
C         -1.89430       -0.08550        0.01990
Cl        -2.13140        2.57880       -0.13270
N          2.34510        0.02790       -0.02400
H         -1.64510       -2.18170        0.13900
H          0.84410       -2.14060        0.11470
H          0.70990        2.08040       -0.12830
H         -2.97350       -0.15810        0.03370
H          2.92220       -0.66400       -0.52630
H          2.81620        0.79500        0.48940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_401_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

