%nproc=16
%mem=2GB
%chk=mol_444_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29720        0.15710        0.60990
C         -0.33890       -0.89300        0.08210
O          0.95670       -0.70110        0.47980
C          1.49500        0.51140        0.07190
H         -1.42210        1.00730       -0.08050
H         -0.97630        0.55170        1.58640
H         -2.29950       -0.34580        0.66990
H         -0.66390       -1.88470        0.47630
H         -0.46140       -0.96530       -1.02860
H          0.91520        1.37810        0.47900
H          2.53790        0.58110        0.44500
H          1.55450        0.60320       -1.02850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

