%nproc=16
%mem=2GB
%chk=mol_444_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.35570        0.20400       -0.50480
C         -0.32040       -0.85760       -0.76110
O          0.78180       -0.75270        0.04320
C          1.47960        0.42180       -0.06180
H         -1.44820        0.88670       -1.39040
H         -1.05280        0.75830        0.40180
H         -2.36150       -0.24010       -0.25870
H         -0.08470       -0.86580       -1.84270
H         -0.79920       -1.84000       -0.54650
H          0.89590        1.32730        0.18570
H          2.31330        0.38910        0.67090
H          1.95170        0.56910       -1.06030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

