%nproc=16
%mem=2GB
%chk=mol_444_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.30410        0.09520        0.34090
C         -0.33780       -0.82060       -0.41790
O          0.70450       -0.11050       -0.96510
C          1.46050        0.56240       -0.00920
H         -1.57110        0.90690       -0.39090
H         -2.25020       -0.43330        0.54780
H         -0.87950        0.47980        1.26530
H         -0.93430       -1.23620       -1.26230
H         -0.01100       -1.63420        0.27240
H          2.27370        1.10260       -0.55450
H          1.98150       -0.19410        0.61820
H          0.86780        1.28220        0.55530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

