%nproc=16
%mem=2GB
%chk=mol_444_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32150        0.23590       -0.34170
C         -0.48640       -0.23770        0.77250
O          0.77980        0.27570        0.85860
C          1.59640        0.04560       -0.21690
H         -0.92210        1.06930       -0.93000
H         -2.32370        0.55060        0.02990
H         -1.54170       -0.59690       -1.07050
H         -0.98920       -0.05090        1.74690
H         -0.39520       -1.36240        0.71340
H          2.57770        0.51910        0.00210
H          1.24190        0.57840       -1.14800
H          1.78390       -1.02680       -0.41630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

