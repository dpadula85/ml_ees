%nproc=16
%mem=2GB
%chk=mol_444_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32630       -0.05540        0.29020
C         -0.38050        0.54910       -0.72310
O          0.75200       -0.20920       -0.89480
C          1.49340       -0.37170        0.25690
H         -0.99180       -1.11280        0.43420
H         -1.35570        0.50760        1.23800
H         -2.35680       -0.14490       -0.15960
H         -0.17320        1.59720       -0.44080
H         -0.89900        0.59150       -1.71580
H          2.38090       -0.99910       -0.00330
H          0.95470       -0.92410        1.05150
H          1.90240        0.57180        0.66660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_444_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

