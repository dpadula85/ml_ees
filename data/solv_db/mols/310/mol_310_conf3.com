%nproc=16
%mem=2GB
%chk=mol_310_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.46580        0.02710       -0.91090
C          3.25690       -0.02860       -0.02210
C          3.41100       -0.01200        1.34660
C          2.31420       -0.05570        2.15110
C          1.06020       -0.11630        1.57650
C          0.83530       -0.14610        0.21600
C          2.00010       -0.09400       -0.56160
C          1.81370       -0.11260       -2.04210
N         -0.42780        0.14230       -0.34250
C         -1.67730       -0.04580        0.29480
C         -1.83960       -1.14900        1.10430
C         -3.08100       -1.72380        1.39700
C         -4.19980       -1.17060        0.84050
C         -4.05820       -0.06900        0.02210
C         -2.83710        0.51760       -0.26600
C         -2.76000        1.80890       -0.93010
O         -1.67440        2.41400       -0.99470
O         -3.92140        2.32300       -1.47500
H          4.73630       -0.98130       -1.27030
H          5.32390        0.38470       -0.29310
H          4.31230        0.75220       -1.73940
H          4.41490        0.03610        1.77530
H          2.44700       -0.04230        3.23760
H          0.24050        0.01260        2.30160
H          2.76140       -0.34560       -2.53500
H          1.39910        0.88680       -2.30730
H          1.05830       -0.90140       -2.29070
H         -0.41590        0.41800       -1.37210
H         -0.98830       -1.73810        1.41410
H         -3.18530       -2.57800        2.03870
H         -5.19120       -1.56920        1.02180
H         -4.93470        0.40320       -0.44970
H         -4.65860        2.75290       -0.93560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_310_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_310_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_310_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

