%nproc=16
%mem=2GB
%chk=mol_550_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.82980        1.14010        0.80800
C         -1.82120        0.01840        0.83360
C         -0.80210        0.12890       -0.26950
C          0.13850       -1.05500       -0.12060
C          1.20750       -1.03820       -1.18770
C          2.04830        0.22170       -1.12040
C          2.69190        0.29170        0.18150
C          3.20790        0.35830        1.26060
H         -3.79920        0.70700        1.13990
H         -2.97370        1.56170       -0.21120
H         -2.59060        1.96150        1.48580
H         -2.34670       -0.96130        0.68440
H         -1.26830        0.01800        1.80480
H         -0.19780        1.04640       -0.12530
H         -1.23780        0.09650       -1.26910
H          0.55420       -1.14070        0.88680
H         -0.47990       -1.96710       -0.31550
H          0.71300       -1.15790       -2.16360
H          1.86400       -1.90440       -1.03440
H          1.44850        1.12780       -1.33740
H          2.81890        0.13660       -1.91510
H          3.65450        0.41050        2.22660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

