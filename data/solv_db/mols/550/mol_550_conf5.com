%nproc=16
%mem=2GB
%chk=mol_550_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.71710        0.80360       -1.09290
C         -2.56300        0.27850        0.30440
C         -1.35910       -0.60450        0.46830
C         -0.13970        0.19280        0.12860
C          1.13940       -0.62510        0.26670
C          2.27610        0.33530       -0.11490
C          3.57030       -0.35990       -0.01480
C          4.62710       -0.93750        0.06990
H         -1.90900        1.50880       -1.32910
H         -3.66460        1.39950       -1.11170
H         -2.81460       -0.03030       -1.81830
H         -3.48140       -0.27060        0.59820
H         -2.45860        1.13050        1.00710
H         -1.31060       -0.88060        1.55380
H         -1.39560       -1.51850       -0.13760
H         -0.17650        0.50300       -0.94440
H         -0.02710        1.06460        0.78290
H          1.28650       -0.99010        1.28620
H          1.14430       -1.50240       -0.41070
H          2.12180        0.75640       -1.11220
H          2.29050        1.18280        0.60240
H          5.56100       -1.43640        0.14180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

