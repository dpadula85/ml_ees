%nproc=16
%mem=2GB
%chk=mol_550_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.50960        1.32490       -0.64590
C         -1.64800        0.50770        0.25770
C         -1.08250       -0.67950       -0.48530
C         -0.20630       -1.51980        0.41520
C          0.96490       -0.74230        0.96590
C          1.85000       -0.21650       -0.17160
C          2.94990        0.52010        0.49600
C          3.83300        1.12790        1.04930
H         -3.09080        0.71520       -1.37270
H         -1.95840        2.15760       -1.14200
H         -3.28050        1.82650       -0.00540
H         -0.84690        1.15930        0.65790
H         -2.28690        0.13070        1.09940
H         -0.59500       -0.38050       -1.41730
H         -1.94920       -1.31970       -0.76630
H         -0.80790       -1.90170        1.24720
H          0.21250       -2.35040       -0.19090
H          1.59390       -1.46620        1.52640
H          0.67340        0.06660        1.64250
H          1.29670        0.42970       -0.86130
H          2.28130       -1.05170       -0.75260
H          4.60630        1.66210        1.55250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

