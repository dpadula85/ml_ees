%nproc=16
%mem=2GB
%chk=mol_550_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.80440       -0.40070        0.37820
C         -1.91220        0.76540        0.64220
C         -0.42670        0.31140        0.63930
C         -0.18840       -0.24690       -0.70230
C          1.10820       -0.81900       -1.06110
C          2.33820       -0.03080       -1.01090
C          2.68490        0.42370        0.32770
C          3.01530        0.79560        1.45280
H         -2.33610       -1.29540        0.86870
H         -2.95150       -0.59360       -0.70880
H         -3.80340       -0.22950        0.84240
H         -2.13660        1.13880        1.66560
H         -2.05030        1.61220       -0.04460
H         -0.34300       -0.40290        1.47670
H          0.09780        1.27100        0.84710
H         -0.97120       -1.06140       -0.85490
H         -0.48820        0.55500       -1.45440
H          1.30910       -1.77580       -0.46830
H          1.03380       -1.24190       -2.12150
H          2.36910        0.81310       -1.75090
H          3.16890       -0.71340       -1.38440
H          3.28670        1.12490        2.42160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

