%nproc=16
%mem=2GB
%chk=mol_550_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.23540        0.18870        0.06260
C         -1.74760        0.23060        0.25920
C         -1.03810       -0.88500       -0.47290
C          0.45360       -0.82780       -0.26310
C          1.03480        0.47980       -0.76360
C          2.53950        0.51960       -0.54560
C          2.87840        0.39440        0.86460
C          3.15720        0.27180        2.03620
H         -3.74290       -0.50140        0.77760
H         -3.52680       -0.08900       -0.96870
H         -3.62570        1.20420        0.29890
H         -1.53600        0.18290        1.33270
H         -1.40760        1.21060       -0.18860
H         -1.40320       -1.84850       -0.06630
H         -1.28580       -0.87690       -1.55190
H          0.97070       -1.68860       -0.72080
H          0.64700       -0.87310        0.83190
H          0.62480        1.31720       -0.16020
H          0.83470        0.63670       -1.84240
H          2.93140        1.48560       -0.89520
H          3.06500       -0.27260       -1.08700
H          3.41210        0.16220        3.06250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_550_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

