%nproc=16
%mem=2GB
%chk=mol_595_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.92420        0.21950        0.40230
C          0.82830        0.51480       -0.62660
C          0.08060       -0.74760       -0.81330
C         -1.20090       -0.87010       -0.56790
C         -1.98420        0.26150       -0.08360
H          1.39620       -0.23740        1.25800
H          2.58100       -0.57870       -0.02580
H          2.48660        1.13110        0.65300
H          1.34790        0.78710       -1.57130
H          0.24470        1.33890       -0.23700
H          0.61150       -1.62360       -1.17180
H         -1.69500       -1.83280       -0.72670
H         -3.08480        0.03260       -0.12100
H         -1.77090        0.37580        1.01840
H         -1.76520        1.22890       -0.56780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

