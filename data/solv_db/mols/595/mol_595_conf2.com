%nproc=16
%mem=2GB
%chk=mol_595_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.86920       -0.04650        0.74160
C         -0.85060        0.25840       -0.31940
C          0.04330       -0.86280       -0.62730
C          1.35050       -0.71670       -0.45200
C          1.85020        0.58800        0.05250
H         -2.64610        0.76070        0.71240
H         -2.39130       -0.99560        0.54230
H         -1.45320       -0.07030        1.75920
H         -1.43640        0.50130       -1.25420
H         -0.31750        1.19270       -0.06550
H         -0.34990       -1.80320       -0.99570
H          2.06820       -1.52840       -0.66820
H          1.63400        1.42510       -0.63860
H          2.96720        0.56630        0.15220
H          1.40090        0.73080        1.06050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

