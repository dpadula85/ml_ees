%nproc=16
%mem=2GB
%chk=mol_595_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.30080        0.86730        0.12970
C          1.18060       -0.59270        0.06750
C         -0.07330       -1.28230       -0.02610
C         -1.25520       -0.74230       -0.07080
C         -1.60230        0.67750       -0.03970
H          1.04900        1.21520        1.17430
H          0.81410        1.46190       -0.62840
H          2.40280        1.19470        0.03760
H          1.78650       -0.90100       -0.86940
H          1.86670       -1.08880        0.86460
H          0.00060       -2.42810       -0.06320
H         -2.09310       -1.48350       -0.16100
H         -1.42610        1.13260       -1.06070
H         -2.74590        0.69770        0.06250
H         -1.20530        1.27180        0.77910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

