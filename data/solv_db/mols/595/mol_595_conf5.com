%nproc=16
%mem=2GB
%chk=mol_595_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.01270       -0.30900        0.14540
C         -0.81050        0.33070       -0.53740
C          0.13570        0.80710        0.50340
C          1.37250        0.39410        0.62020
C          1.90100       -0.60970       -0.32320
H         -2.70320       -0.73710       -0.60400
H         -2.56070        0.53370        0.64480
H         -1.72010       -1.05300        0.90400
H         -1.20510        1.22710       -1.06640
H         -0.36410       -0.31840       -1.28790
H         -0.23260        1.54370        1.20550
H          1.99070        0.80300        1.41730
H          1.51630       -1.59320       -0.02100
H          1.67460       -0.36230       -1.35880
H          3.01820       -0.65660       -0.24180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

