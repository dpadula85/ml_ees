%nproc=16
%mem=2GB
%chk=mol_595_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92930        0.46110        0.32070
C         -0.76120       -0.51700        0.31340
C         -0.01120       -0.41740       -0.97150
C          1.25450       -0.08370       -0.92450
C          1.88260        0.17820        0.38750
H         -2.76490        0.10040        0.95710
H         -2.29300        0.50740       -0.73530
H         -1.64570        1.45010        0.69870
H         -0.13270       -0.26720        1.17550
H         -1.21200       -1.53670        0.38820
H         -0.50770       -0.61770       -1.92440
H          1.80050       -0.01140       -1.86440
H          1.91880       -0.71280        1.04470
H          2.95940        0.45670        0.19040
H          1.44190        1.00990        0.94400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_595_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

