%nproc=16
%mem=2GB
%chk=mol_564_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.33840       -1.29920       -0.29030
C         -0.47680       -0.20350        0.29750
C         -0.89440        1.16120       -0.16590
C          0.91330       -0.45160       -0.11410
O          1.27800       -1.38690       -0.80920
C          1.96090        0.49890        0.34290
H         -1.15330       -2.21250        0.32000
H         -0.99620       -1.54100       -1.32250
H         -2.42130       -1.03840       -0.26150
H         -0.62730       -0.24920        1.40250
H         -1.51510        1.68930        0.61440
H          0.00910        1.79620       -0.29300
H         -1.45290        1.14080       -1.12120
H          2.50740        0.88540       -0.54440
H          1.54350        1.31390        0.96410
H          2.66370       -0.10330        0.98070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

