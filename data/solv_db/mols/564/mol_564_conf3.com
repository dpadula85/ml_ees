%nproc=16
%mem=2GB
%chk=mol_564_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77800       -1.07890       -0.75340
C         -0.44880        0.27350       -0.18110
C         -1.52540        0.61640        0.82080
C          0.86060        0.35280        0.45310
O          0.92560        0.64660        1.63110
C          2.16300        0.10220       -0.21900
H          0.05420       -1.46560       -1.37740
H         -1.08330       -1.77000        0.05130
H         -1.68540       -0.92820       -1.40920
H         -0.46680        0.99220       -1.03030
H         -1.28110        0.14630        1.78290
H         -2.50580        0.19110        0.46310
H         -1.69280        1.71110        0.89520
H          2.60760        1.11830       -0.42780
H          2.04840       -0.44130       -1.18300
H          2.80790       -0.46660        0.48380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

