%nproc=16
%mem=2GB
%chk=mol_564_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.78030       -1.26010       -0.23240
C         -0.39290        0.12530        0.23320
C         -1.54340        1.04420       -0.09130
C          0.85390        0.61180       -0.35170
O          0.83560        1.63020       -1.01660
C          2.13290       -0.09440       -0.16020
H         -0.16030       -1.62870       -1.06060
H         -1.84920       -1.31390       -0.54900
H         -0.68800       -1.95750        0.63270
H         -0.26960        0.13820        1.34800
H         -2.40120        0.73430        0.54410
H         -1.86160        0.85170       -1.13990
H         -1.25130        2.09490        0.05290
H          2.00500       -1.12990       -0.59130
H          2.95120        0.38920       -0.73310
H          2.41900       -0.23530        0.89580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

