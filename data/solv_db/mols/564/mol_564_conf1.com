%nproc=16
%mem=2GB
%chk=mol_564_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.70250        1.30010        0.02010
C         -0.42690       -0.15750       -0.31850
C         -1.58710       -1.04310        0.04940
C          0.83240       -0.58590        0.31980
O          0.81310       -1.49700        1.10940
C          2.10600        0.08540        0.00310
H         -0.39740        1.97650       -0.81240
H         -0.05580        1.58960        0.88430
H         -1.75470        1.45460        0.26420
H         -0.35830       -0.18730       -1.44410
H         -1.60230       -1.30560        1.12950
H         -2.51660       -0.52500       -0.27180
H         -1.53330       -1.97070       -0.58710
H          2.90450       -0.10770        0.73220
H          1.89230        1.18920       -0.04380
H          2.38660       -0.21560       -1.03430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

