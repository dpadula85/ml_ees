%nproc=16
%mem=2GB
%chk=mol_564_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32810        1.28040       -0.25260
C         -0.41620        0.08290       -0.39400
C         -1.00940       -1.00540        0.49240
C          0.97590        0.40760       -0.03240
O          1.24930        1.53880        0.32480
C          2.08720       -0.57740       -0.07730
H         -2.28150        1.08580       -0.74850
H         -0.81570        2.20160       -0.59960
H         -1.54150        1.39380        0.83570
H         -0.47120       -0.29380       -1.44480
H         -2.09980       -1.04690        0.20920
H         -0.58650       -1.99130        0.29890
H         -0.99100       -0.69630        1.55490
H          2.84460       -0.29280       -0.85600
H          1.74050       -1.61920       -0.20630
H          2.64330       -0.46780        0.89560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_564_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

