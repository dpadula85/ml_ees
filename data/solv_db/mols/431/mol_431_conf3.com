%nproc=16
%mem=2GB
%chk=mol_431_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.55980       -0.03340        0.00480
N          0.83320       -0.02090        0.05860
H         -0.92250        0.92230       -0.46750
H         -1.03130       -0.84300       -0.59360
H         -1.00360       -0.01790        1.03380
H          1.32650        0.86900       -0.01690
H          1.35750       -0.87600       -0.01910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

