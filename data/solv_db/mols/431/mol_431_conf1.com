%nproc=16
%mem=2GB
%chk=mol_431_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.55880       -0.05240        0.01280
N          0.84220        0.05480        0.05250
H         -0.94610       -0.87190       -0.63860
H         -1.00550       -0.16010        1.03990
H         -1.00540        0.88210       -0.39650
H          1.39740       -0.77820        0.14660
H          1.27610        0.92560       -0.21670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

