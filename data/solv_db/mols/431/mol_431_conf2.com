%nproc=16
%mem=2GB
%chk=mol_431_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.59210       -0.00290       -0.00370
N          0.84780        0.05380        0.25610
H         -0.75640       -0.64790       -0.87760
H         -1.03750       -0.42170        0.93900
H         -0.99660        1.01070       -0.17860
H          1.23230       -0.86500        0.01020
H          1.30260        0.87300       -0.14530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

