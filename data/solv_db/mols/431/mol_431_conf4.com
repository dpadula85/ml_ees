%nproc=16
%mem=2GB
%chk=mol_431_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.57320        0.05720        0.01880
N          0.81310       -0.16750        0.37050
H         -0.63420        1.05020       -0.46250
H         -1.23610       -0.03190        0.90060
H         -0.95030       -0.67440       -0.72930
H          1.16530       -0.91120       -0.29230
H          1.41540        0.67760        0.19410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_431_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

