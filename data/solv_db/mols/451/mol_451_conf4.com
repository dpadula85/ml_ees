%nproc=16
%mem=2GB
%chk=mol_451_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44380       -0.06650        0.39320
C         -1.50420       -0.59250       -0.65420
C         -0.09530       -0.30810       -0.26800
C          0.46740        0.87740       -0.68070
C          1.76950        1.12760       -0.31720
C          2.52050        0.24270        0.43680
C          1.88600       -0.93540        0.81820
N          0.61480       -1.17680        0.46170
H         -2.96640        0.84690        0.01650
H         -1.89420        0.25120        1.29540
H         -3.17260       -0.87210        0.65200
H         -1.77620       -0.14930       -1.62940
H         -1.56050       -1.71570       -0.74450
H         -0.10710        1.59850       -1.28070
H          2.24600        2.06470       -0.63140
H          3.54510        0.45150        0.71650
H          2.47090       -1.64400        1.41570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

