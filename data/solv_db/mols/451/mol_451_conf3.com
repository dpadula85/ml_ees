%nproc=16
%mem=2GB
%chk=mol_451_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.39770        0.43260        0.28070
C         -1.59780       -0.37000       -0.75130
C         -0.16890       -0.11570       -0.43150
C          0.47160       -0.96390        0.46140
C          1.79720       -0.78480        0.80200
C          2.51560        0.25470        0.25230
C          1.85740        1.08370       -0.63200
N          0.55580        0.87490       -0.94080
H         -3.47060        0.25170        0.20950
H         -1.95850        0.12950        1.27210
H         -2.11420        1.50490        0.08190
H         -1.87150        0.04090       -1.73720
H         -1.81810       -1.45040       -0.68720
H         -0.10720       -1.78700        0.89310
H          2.31190       -1.43540        1.49520
H          3.57020        0.43540        0.49330
H          2.42480        1.89890       -1.06150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

