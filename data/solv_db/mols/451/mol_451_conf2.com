%nproc=16
%mem=2GB
%chk=mol_451_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.40740        0.34610        0.26470
C          1.51800        0.14320       -0.95650
C          0.10930       -0.02720       -0.55110
C         -0.30190        0.00080        0.78160
C         -1.62630       -0.16090        1.12460
C         -2.56900       -0.35460        0.13580
C         -2.13700       -0.37850       -1.17730
N         -0.84050       -0.21840       -1.48400
H          2.03210        1.25470        0.80050
H          2.40240       -0.54520        0.92190
H          3.45410        0.55500       -0.03190
H          1.59540        1.07560       -1.56300
H          1.89900       -0.69540       -1.56490
H          0.47890        0.15650        1.53530
H         -1.89670       -0.13200        2.16360
H         -3.62500       -0.48580        0.37270
H         -2.90010       -0.53380       -1.95160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

