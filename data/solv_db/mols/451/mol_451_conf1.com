%nproc=16
%mem=2GB
%chk=mol_451_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.48480       -0.72870        0.30090
C          1.61390        0.45180        0.04050
C          0.16560        0.14260        0.10100
C         -0.71920        1.17210       -0.12600
C         -2.08770        1.01740       -0.09740
C         -2.57990       -0.23000        0.17180
C         -1.67500       -1.26530        0.39990
N         -0.33750       -1.06330        0.36110
H          3.37090       -0.38990        0.90770
H          2.93810       -1.08550       -0.66390
H          2.01820       -1.53700        0.86960
H          1.82350        0.85360       -0.97910
H          1.79760        1.22320        0.83420
H         -0.33030        2.17460       -0.34260
H         -2.71600        1.87590       -0.28620
H         -3.65240       -0.38140        0.20200
H         -2.11480       -2.23030        0.60710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

