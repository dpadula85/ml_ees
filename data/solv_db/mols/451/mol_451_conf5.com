%nproc=16
%mem=2GB
%chk=mol_451_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.34670        0.27830        0.64510
C          1.65570       -0.26600       -0.58720
C          0.18870       -0.04900       -0.40850
C         -0.58550       -1.01800        0.19970
C         -1.94080       -0.87280        0.39130
C         -2.51360        0.30320       -0.05630
C         -1.74250        1.28370       -0.66940
N         -0.41480        1.07870       -0.82720
H          2.83020       -0.52060        1.24960
H          3.09720        1.04090        0.36410
H          1.62510        0.79610        1.32980
H          2.05550        0.22200       -1.48150
H          1.87960       -1.34610       -0.63860
H         -0.13720       -1.94490        0.55330
H         -2.52930       -1.64390        0.87030
H         -3.57350        0.47280        0.06690
H         -2.24150        2.18580       -1.00140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_451_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

