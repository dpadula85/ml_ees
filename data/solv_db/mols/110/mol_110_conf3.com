%nproc=16
%mem=2GB
%chk=mol_110_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.62240        0.21290        0.01920
C         -0.25130       -0.46370       -0.08960
O          0.70160        0.58190        0.10330
C          2.06350        0.33410        0.07040
O          2.43370       -0.84470       -0.13530
H         -2.44240       -0.53550       -0.00190
H         -1.70110        0.94700       -0.82800
H         -1.58980        0.78640        0.97660
H         -0.21560       -1.20520        0.73930
H         -0.15610       -0.94620       -1.07160
H          2.77980        1.13300        0.21770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

