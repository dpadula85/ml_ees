%nproc=16
%mem=2GB
%chk=mol_110_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.58730        0.31770        0.06480
C         -0.29170       -0.41370       -0.15750
O          0.73740        0.46120        0.25840
C          2.07100        0.15450        0.21110
O          2.42160       -0.96290       -0.22330
H         -1.55160        1.23970       -0.56390
H         -1.72150        0.63700        1.11910
H         -2.43910       -0.32790       -0.28470
H         -0.20160       -0.55970       -1.27060
H         -0.24450       -1.40250        0.30690
H          2.80740        0.85660        0.53970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

