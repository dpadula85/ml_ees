%nproc=16
%mem=2GB
%chk=mol_110_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.59670        0.31900       -0.00570
C         -0.25930       -0.37900       -0.14030
O          0.74750        0.55350        0.20490
C          2.10490        0.18710        0.17310
O          2.33740       -0.99570       -0.17530
H         -2.20870       -0.11820        0.83120
H         -1.46550        1.41320        0.13400
H         -2.20450        0.15720       -0.93420
H         -0.09390       -0.80730       -1.14140
H         -0.24140       -1.20390        0.62430
H          2.88020        0.87410        0.42950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

