%nproc=16
%mem=2GB
%chk=mol_110_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23110        0.20170        0.38100
C         -0.33770       -0.68380       -0.42040
O          0.99600       -0.71560       -0.02010
C          1.86410        0.33590        0.03590
O          1.42660        1.46200       -0.30830
H         -0.98130        0.09090        1.45670
H         -2.26880       -0.18080        0.26740
H         -1.25190        1.25580        0.04720
H         -0.73670       -1.73140       -0.31030
H         -0.37660       -0.33910       -1.47660
H          2.89740        0.30430        0.34740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

