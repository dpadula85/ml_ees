%nproc=16
%mem=2GB
%chk=mol_110_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21830        0.35240       -0.04060
C         -0.28840       -0.79340       -0.24930
O          1.01130       -0.42970       -0.62390
C          1.82230        0.36740        0.16520
O          1.36240        0.76420        1.24060
H         -1.24340        0.67400        1.02090
H         -2.24750        0.04170       -0.32520
H         -0.98270        1.24400       -0.63980
H         -0.32650       -1.43250        0.66140
H         -0.71120       -1.43110       -1.07890
H          2.82200        0.64300       -0.13040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_110_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

