%nproc=16
%mem=2GB
%chk=mol_535_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.27100        1.34440        0.08200
C          0.66290        0.11050       -0.05210
C          1.41660       -1.02460       -0.26990
C          2.78950       -0.92540       -0.35430
C          3.39420        0.32170       -0.21790
C          2.64370        1.46100        0.00020
Cl         3.46010        3.00230        0.16320
Cl         5.14870        0.38410       -0.33520
Cl         3.73960       -2.36890       -0.63060
Cl         0.68660       -2.63240       -0.44940
C         -0.76510       -0.02210        0.03160
C         -1.50150        0.09010       -1.12870
C         -2.87110       -0.03790       -1.04250
C         -3.49350       -0.27360        0.18060
C         -2.70720       -0.37980        1.32890
C         -1.33690       -0.25500        1.26370
Cl        -0.35230       -0.39210        2.73090
Cl        -3.49550       -0.67600        2.86310
Cl        -5.22370       -0.42800        0.24220
Cl        -0.74360        0.38980       -2.69990
H          0.71310        2.25970        0.25410
H         -3.43560        0.05230       -1.95990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

