%nproc=16
%mem=2GB
%chk=mol_535_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31410       -1.18220       -0.58860
C         -0.67510       -0.08250       -0.07660
C         -1.40130        0.99290        0.38790
C         -2.78030        1.00690        0.35720
C         -3.43900       -0.09200       -0.15500
C         -2.68870       -1.17900       -0.62410
Cl        -3.51040       -2.58640       -1.28000
Cl        -5.18950       -0.15750       -0.23120
Cl        -3.71340        2.36060        0.94160
Cl        -0.62340        2.44610        1.06290
C          0.75650       -0.01360       -0.00710
C          1.35730       -0.48940        1.14810
C          2.72940       -0.46220        1.30110
C          3.48430        0.05690        0.26070
C          2.87780        0.52770       -0.88440
C          1.52480        0.49860       -1.03030
Cl         0.67910        1.07480       -2.44780
Cl         3.84030        1.17680       -2.18160
Cl         5.23690        0.14210        0.34300
Cl         0.41190       -1.15170        2.47930
H         -0.76010       -2.05770       -0.96770
H          3.19700       -0.82920        2.19260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

