%nproc=16
%mem=2GB
%chk=mol_535_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.41670        0.98310       -0.62630
C          0.70300       -0.02740       -0.02330
C          1.42060       -1.11280        0.44480
C          2.81090       -1.15450        0.29800
C          3.46750       -0.12080       -0.30980
C          2.77790        0.96640       -0.78230
Cl         3.64310        2.28590       -1.56160
Cl         5.21770       -0.14010       -0.51130
Cl         3.74400       -2.52430        0.88520
Cl         0.60480       -2.43450        1.21910
C         -0.75280        0.06430        0.10590
C         -1.35580        0.63830        1.20350
C         -2.74600        0.72070        1.31650
C         -3.54860        0.22660        0.32650
C         -2.95450       -0.34590       -0.76830
C         -1.57060       -0.43180       -0.88840
Cl        -0.89910       -1.17920       -2.32050
Cl        -3.97640       -0.99030       -2.05900
Cl        -5.31410        0.29990        0.40880
Cl        -0.33660        1.26870        2.46530
H          0.87430        1.83770       -0.99760
H         -3.22610        1.16980        2.17480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_535_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

