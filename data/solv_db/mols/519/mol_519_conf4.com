%nproc=16
%mem=2GB
%chk=mol_519_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47420        0.56520        0.33390
C         -1.99520       -0.67920       -0.34270
C         -0.59830       -1.06240        0.07810
C          0.41380        0.00740       -0.24470
C          1.77740       -0.49860        0.22490
C          2.78850        0.55410       -0.09010
O          0.10930        1.18200        0.43100
H         -3.59240        0.52720        0.42900
H         -2.10810        0.68730        1.36580
H         -2.22830        1.44710       -0.27810
H         -2.70830       -1.49810       -0.13180
H         -2.03830       -0.53070       -1.44880
H         -0.62610       -1.21430        1.19460
H         -0.32970       -2.03990       -0.34110
H          0.50070        0.19900       -1.31560
H          1.98280       -1.46660       -0.29100
H          1.72730       -0.71220        1.31190
H          3.08020        1.16220        0.81130
H          3.74310        0.12800       -0.49420
H          2.39260        1.29830       -0.82610
H          0.18350        1.94430       -0.19540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

