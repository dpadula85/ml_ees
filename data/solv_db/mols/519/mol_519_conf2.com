%nproc=16
%mem=2GB
%chk=mol_519_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47270        0.62540        0.31430
C         -1.99990       -0.53520       -0.48920
C         -0.64150       -1.04060       -0.12190
C          0.42010        0.02280       -0.29140
C          1.79310       -0.49540        0.08190
C          2.75350        0.67020       -0.13510
O          0.14170        1.07780        0.58310
H         -2.17930        1.60930       -0.05370
H         -2.11850        0.48510        1.36770
H         -3.58460        0.59190        0.35370
H         -2.72210       -1.37090       -0.34580
H         -1.99980       -0.30730       -1.56880
H         -0.61470       -1.40470        0.93640
H         -0.38760       -1.87210       -0.79400
H          0.45130        0.37280       -1.32720
H          1.83140       -0.83660        1.14020
H          2.09620       -1.31000       -0.61030
H          2.36910        1.27120       -1.00430
H          2.67010        1.32220        0.74580
H          3.77120        0.30530       -0.27780
H          0.42300        0.81870        1.49640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

