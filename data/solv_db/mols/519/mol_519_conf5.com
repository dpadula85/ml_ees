%nproc=16
%mem=2GB
%chk=mol_519_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.93310       -0.29350        0.46580
C          1.48720       -0.54850        0.09150
C          0.82560        0.78210       -0.09840
C         -0.62380        0.66550       -0.47560
C         -1.39780       -0.06620        0.59870
C         -2.85370       -0.17010        0.19490
O         -0.71230        0.05110       -1.70350
H          3.02480       -0.15850        1.55420
H          3.61500       -1.08580        0.14230
H          3.24130        0.64110       -0.01480
H          0.99650       -1.11980        0.90250
H          1.49410       -1.15550       -0.82880
H          0.91370        1.35280        0.84910
H          1.38070        1.32420       -0.88410
H         -1.04030        1.68740       -0.56300
H         -1.26560        0.47490        1.55700
H         -1.03630       -1.09950        0.74330
H         -3.24380       -1.20620        0.29620
H         -3.49690        0.51140        0.81050
H         -2.95970        0.16380       -0.85400
H         -1.28150       -0.75080       -1.60030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

