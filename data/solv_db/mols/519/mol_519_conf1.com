%nproc=16
%mem=2GB
%chk=mol_519_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.37690       -0.02940       -0.96220
C          2.06260        0.39660        0.44180
C          0.77900       -0.20080        0.93430
C         -0.40910        0.18550        0.11290
C         -1.66730       -0.45410        0.67260
C         -2.86270       -0.05550       -0.16740
O         -0.29500       -0.12890       -1.21810
H          1.96170        0.72810       -1.67070
H          1.99570       -1.01300       -1.22470
H          3.48800       -0.08370       -1.10770
H          2.06330        1.50390        0.50280
H          2.92120        0.06280        1.07450
H          0.86280       -1.32340        0.94310
H          0.64070        0.07480        2.00100
H         -0.56120        1.29200        0.19650
H         -1.85070       -0.15180        1.72030
H         -1.57500       -1.56530        0.57500
H         -3.72550       -0.66280        0.21140
H         -3.06910        1.02720        0.00470
H         -2.67580       -0.25830       -1.23700
H         -0.46050        0.65600       -1.80320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

