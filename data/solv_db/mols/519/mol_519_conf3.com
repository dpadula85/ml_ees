%nproc=16
%mem=2GB
%chk=mol_519_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.84130        0.05860       -0.82350
C         -1.78450       -0.11010        0.23730
C         -0.42560        0.33890       -0.31270
C          0.56740        0.13810        0.80570
C          1.96640        0.54230        0.40680
C          2.49380       -0.23040       -0.75570
O          0.56630       -1.19970        1.15430
H         -2.95760       -0.85970       -1.43860
H         -3.81300        0.28220       -0.30760
H         -2.63010        0.89040       -1.51950
H         -2.06190        0.52050        1.11340
H         -1.74930       -1.16130        0.52980
H         -0.22380       -0.31890       -1.17320
H         -0.53920        1.38170       -0.61770
H          0.27880        0.72610        1.69750
H          1.99080        1.62220        0.15210
H          2.63210        0.37300        1.27630
H          2.14580       -1.27450       -0.77200
H          2.28550        0.26500       -1.72580
H          3.61680       -0.25400       -0.66630
H          0.48260       -1.73030        0.32040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_519_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

