%nproc=16
%mem=2GB
%chk=mol_145_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.33820        0.53690        0.10820
C          0.17620        1.37110       -0.40370
C         -1.08870        0.89170        0.28870
C         -1.32540       -0.54510       -0.12120
C         -0.15700       -1.34900        0.46150
C          1.07160       -0.89530       -0.33330
H          2.30600        0.88080       -0.26040
H          1.27490        0.53840        1.21430
H          0.41160        2.41830       -0.17130
H          0.01700        1.18400       -1.49220
H         -0.92260        0.91960        1.38790
H         -1.96220        1.49480       -0.00510
H         -1.19020       -0.60140       -1.23260
H         -2.29130       -0.94030        0.20970
H         -0.03720       -1.00000        1.51630
H         -0.31970       -2.42440        0.38220
H          0.76470       -0.93490       -1.40530
H          1.93400       -1.54520       -0.14370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

