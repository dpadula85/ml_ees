%nproc=16
%mem=2GB
%chk=mol_145_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.15150       -1.31510       -0.51180
C         -1.28800       -0.64840        0.25970
C         -1.16990        0.84390       -0.03950
C          0.12970        1.29150        0.63370
C          1.23890        0.70820       -0.24410
C          1.17280       -0.80290       -0.03570
H         -0.17480       -2.41000       -0.37000
H         -0.29530       -1.02140       -1.57030
H         -2.25570       -1.06330       -0.00880
H         -1.03750       -0.77310        1.34360
H         -1.02870        0.94910       -1.14790
H         -2.03400        1.41520        0.28840
H          0.19900        2.37120        0.74080
H          0.20140        0.74450        1.60930
H          2.21660        1.12770       -0.01570
H          0.94790        0.88060       -1.30710
H          1.35180       -1.06030        1.01860
H          1.97720       -1.23740       -0.64300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

