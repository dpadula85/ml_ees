%nproc=16
%mem=2GB
%chk=mol_145_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.41890       -0.24940        0.16790
C          0.89040        1.13620       -0.00370
C         -0.55960        1.31630        0.13440
C         -1.43860        0.14740        0.06150
C         -0.80300       -1.19710        0.19860
C          0.51480       -1.22310       -0.54600
H          1.57130       -0.53700        1.23980
H          2.42050       -0.33910       -0.30390
H          1.25890        1.50050       -1.00930
H          1.46710        1.78120        0.72360
H         -0.89760        2.06600       -0.64520
H         -0.75680        1.86490        1.10500
H         -2.02650        0.12950       -0.90440
H         -2.24670        0.19150        0.85270
H         -1.44360       -1.97630       -0.26620
H         -0.67220       -1.51690        1.26630
H          0.34210       -0.86810       -1.55770
H          0.96090       -2.22640       -0.51340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

