%nproc=16
%mem=2GB
%chk=mol_145_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.35820        1.35620        0.24340
C          1.04950        0.96300        0.08650
C          1.35310       -0.48640        0.00620
C          0.26670       -1.27650       -0.66210
C         -0.94530       -1.07760        0.25930
C         -1.39430        0.33090        0.02130
H         -0.54530        1.82080        1.25770
H         -0.56360        2.21240       -0.46770
H          1.53160        1.45460       -0.81050
H          1.64380        1.37720        0.95570
H          1.47260       -0.89800        1.04230
H          2.33630       -0.64250       -0.48910
H          0.52740       -2.32260       -0.77850
H          0.03570       -0.76860       -1.63440
H         -0.61620       -1.13470        1.33230
H         -1.71120       -1.79870        0.01230
H         -1.77470        0.38290       -1.02990
H         -2.30770        0.50770        0.65520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

