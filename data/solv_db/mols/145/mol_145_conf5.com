%nproc=16
%mem=2GB
%chk=mol_145_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.40320        0.34250       -0.17230
C          0.98120       -1.01780        0.28080
C         -0.43000       -1.37150       -0.04340
C         -1.42450       -0.30930        0.32970
C         -1.00350        0.97350       -0.34290
C          0.37780        1.41060        0.06620
H          1.73650        0.37840       -1.22290
H          2.30670        0.62310        0.43700
H          1.17870       -1.13980        1.35840
H          1.63880       -1.75840       -0.25640
H         -0.51340       -1.53220       -1.13590
H         -0.67630       -2.31560        0.48700
H         -1.61050       -0.21710        1.39660
H         -2.39870       -0.60520       -0.14800
H         -0.93970        0.72930       -1.44170
H         -1.69540        1.80530       -0.18890
H          0.63140        2.30220       -0.53390
H          0.43790        1.70190        1.13040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_145_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

