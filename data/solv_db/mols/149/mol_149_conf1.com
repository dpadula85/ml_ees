%nproc=16
%mem=2GB
%chk=mol_149_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.78100       -1.07040       -0.00910
C          1.27730        0.21440        0.00410
C          0.36970        1.24940        0.01270
O          0.84260        2.41780        0.02470
N         -0.95960        0.99940        0.00810
C         -1.42750       -0.25930       -0.00480
O         -2.65500       -0.49040       -0.00890
N         -0.55990       -1.28190       -0.01320
I          3.36760        0.56660        0.01060
H          1.50300       -1.87220       -0.01570
H         -1.63020        1.78930        0.01470
H         -0.90900       -2.26270       -0.02320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_149_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_149_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_149_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

