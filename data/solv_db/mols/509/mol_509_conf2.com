%nproc=16
%mem=2GB
%chk=mol_509_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.03470       -1.14680       -0.20230
C         -0.68640        0.04250        0.04350
C          0.07920        1.16650        0.20400
O         -0.52690        2.24790        0.42820
N          1.42730        1.10480        0.12300
C          2.01950       -0.08870       -0.12040
O          3.26850       -0.11050       -0.18910
N          1.31150       -1.22250       -0.28570
C         -2.16640        0.04030        0.12020
F         -2.61590        0.47880        1.35830
F         -2.62160        0.94000       -0.83030
F         -2.65290       -1.22790       -0.08760
H         -0.56450       -2.08780       -0.34260
H          1.94990        1.99020        0.25370
H          1.81340       -2.12680       -0.47280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

