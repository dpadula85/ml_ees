%nproc=16
%mem=2GB
%chk=mol_509_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01970       -1.15670       -0.27250
C         -0.70640       -0.02440        0.03650
C          0.01910        1.12710        0.25420
O         -0.63220        2.17670        0.53920
N          1.36180        1.15240        0.17140
C          2.05930        0.04240       -0.13070
O          3.32510        0.04830       -0.21290
N          1.35720       -1.08490       -0.34430
C         -2.19830       -0.04890        0.12940
F         -2.76040        0.81980       -0.77900
F         -2.63980       -1.32130       -0.13370
F         -2.51750        0.26040        1.43340
H         -0.51880       -2.08480       -0.45070
H          1.90350        2.04080        0.34050
H          1.92760       -1.94700       -0.58080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

