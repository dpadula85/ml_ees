%nproc=16
%mem=2GB
%chk=mol_509_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.06610        1.18410       -0.30310
C         -0.67970        0.06370       -0.00140
C         -0.05220       -1.15410        0.29380
O         -0.76670       -2.14490        0.56170
N          1.28770       -1.16720        0.26670
C          2.03590       -0.08540       -0.02520
O          3.29450       -0.19500       -0.02590
N          1.39810        1.06720       -0.30360
C         -2.14750        0.11680        0.01890
F         -2.55660       -0.22720        1.30040
F         -2.65740       -0.79730       -0.89420
F         -2.53400        1.39850       -0.31760
H         -0.44860        2.11950       -0.52910
H          1.77130       -2.08270        0.48840
H          1.98910        1.90410       -0.52970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

