%nproc=16
%mem=2GB
%chk=mol_509_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.08660       -1.13210       -0.43630
C         -0.66870       -0.12010        0.08610
C         -0.06350        1.08280        0.40380
O         -0.75530        2.00560        0.88070
N          1.26040        1.24580        0.19640
C          1.99260        0.23160       -0.32210
O          3.22300        0.43270       -0.49660
N          1.40030       -0.93800       -0.63020
C         -2.10970       -0.26350        0.32440
F         -2.49450       -1.52900       -0.06080
F         -2.77300        0.72970       -0.36800
F         -2.32920       -0.11590        1.68500
H         -0.42560       -2.06160       -0.67290
H          1.70440        2.15960        0.44080
H          1.95220       -1.72760       -1.03020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_509_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

