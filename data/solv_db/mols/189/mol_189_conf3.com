%nproc=16
%mem=2GB
%chk=mol_189_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.65460       -0.13310        0.76040
C          3.01560        0.38100       -0.48880
C          1.60470       -0.10650       -0.69260
C          0.68990        0.30700        0.43830
C         -0.68570       -0.23730        0.11980
C         -1.67760        0.13390        1.21240
C         -3.04710       -0.41560        0.88280
C         -3.58910        0.10750       -0.40470
H          3.97340        0.76240        1.36760
H          3.02020       -0.74670        1.39440
H          4.58880       -0.69660        0.56510
H          3.03960        1.48620       -0.55320
H          3.61190        0.00970       -1.35020
H          1.61930       -1.19160       -0.80370
H          1.24060        0.33130       -1.64480
H          1.05800       -0.18890        1.36650
H          0.69710        1.38330        0.61610
H         -1.00160        0.15550       -0.86370
H         -0.62400       -1.33060        0.02650
H         -1.78070        1.24670        1.21520
H         -1.34270       -0.17360        2.20130
H         -3.75310       -0.22500        1.69690
H         -2.97950       -1.53900        0.79550
H         -4.61720        0.52910       -0.25130
H         -3.74410       -0.71610       -1.16040
H         -2.97120        0.86700       -0.88750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

