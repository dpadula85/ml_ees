%nproc=16
%mem=2GB
%chk=mol_189_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.80930        0.36780        0.52770
C          2.85420       -0.27850       -0.46410
C          1.46110       -0.18660        0.17660
C          0.41830       -0.79030       -0.70110
C         -0.92960       -0.69140       -0.03310
C         -1.32120        0.72960        0.23060
C         -2.70570        0.67620        0.89470
C         -3.62680        0.00310       -0.07960
H          3.41850        1.37020        0.84580
H          3.86860       -0.26680        1.43420
H          4.80030        0.55880        0.08170
H          3.08340       -1.36250       -0.51120
H          2.89030        0.19360       -1.44680
H          1.26790        0.87810        0.39710
H          1.55620       -0.70420        1.15300
H          0.65280       -1.82500       -1.00140
H          0.40660       -0.20540       -1.66600
H         -1.70110       -1.24840       -0.60900
H         -0.90870       -1.20620        0.97230
H         -1.36770        1.33020       -0.70500
H         -0.62350        1.25510        0.91000
H         -2.60270        0.08830        1.82170
H         -3.00170        1.71420        1.14330
H         -3.22420        0.08020       -1.12780
H         -4.60940        0.55740       -0.04450
H         -3.86510       -1.03760        0.16690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

