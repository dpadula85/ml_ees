%nproc=16
%mem=2GB
%chk=mol_189_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.87020        0.10860        0.25570
C          2.79840        0.60120       -0.68640
C          1.48150       -0.00050       -0.20670
C          0.33400        0.42990       -1.08360
C         -0.95130       -0.18590       -0.58550
C         -1.30270        0.20130        0.81260
C         -2.62380       -0.49270        1.16910
C         -3.69140       -0.03670        0.20740
H          3.56010       -0.87180        0.67460
H          3.97500        0.85310        1.07670
H          4.83460       -0.06870       -0.26080
H          2.75740        1.69190       -0.75200
H          2.98570        0.19030       -1.70830
H          1.61420       -1.09910       -0.22540
H          1.35940        0.29310        0.85600
H          0.56820        0.10070       -2.11160
H          0.27410        1.53620       -1.12670
H         -0.89660       -1.29540       -0.62730
H         -1.76840        0.07720       -1.31850
H         -0.50470       -0.16550        1.48790
H         -1.41430        1.27560        0.95680
H         -2.90220       -0.27230        2.22570
H         -2.51130       -1.59410        1.10700
H         -4.66260        0.03080        0.72520
H         -3.36560        0.95650       -0.21840
H         -3.81800       -0.74090       -0.64350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

