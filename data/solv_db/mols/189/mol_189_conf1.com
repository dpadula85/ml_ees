%nproc=16
%mem=2GB
%chk=mol_189_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.09320        0.54680       -0.95570
C         -2.72620        0.30370        0.48080
C         -1.82630       -0.88740        0.65310
C         -0.51980       -0.76030       -0.09540
C          0.18110        0.44260        0.42890
C          1.52080        0.70060       -0.22380
C          2.47540       -0.45140       -0.02780
C          3.77590       -0.08590       -0.71830
H         -3.37680       -0.36660       -1.48460
H         -3.91560        1.32000       -0.95770
H         -2.25040        1.03390       -1.50770
H         -2.30640        1.22170        0.91940
H         -3.68130        0.13300        1.05370
H         -2.35690       -1.75640        0.20410
H         -1.67370       -1.12470        1.71820
H          0.03950       -1.69850       -0.00400
H         -0.79560       -0.67270       -1.17890
H         -0.44770        1.33760        0.14170
H          0.31970        0.34230        1.50510
H          1.95460        1.57400        0.31390
H          1.44990        0.90420       -1.29650
H          2.69760       -0.65590        1.03720
H          2.09490       -1.38610       -0.49190
H          3.72660        0.85580       -1.26660
H          4.60170        0.03190        0.03790
H          4.13230       -0.90220       -1.39300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

