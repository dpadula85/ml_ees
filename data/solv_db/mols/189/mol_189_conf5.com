%nproc=16
%mem=2GB
%chk=mol_189_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.54590        0.64540       -1.01530
C         -3.26330       -0.41240        0.00220
C         -1.96520       -0.23780        0.72930
C         -0.75900       -0.24740       -0.16610
C          0.46240       -0.06200        0.70940
C          1.73740       -0.06040       -0.12910
C          2.93670        0.12690        0.77410
C          4.19130        0.12800       -0.05480
H         -3.18790        1.62870       -0.66670
H         -4.65420        0.71390       -1.12860
H         -3.17460        0.39310       -2.03800
H         -3.33600       -1.41300       -0.47150
H         -4.07390       -0.37510        0.76000
H         -1.87230       -1.08100        1.44540
H         -1.99880        0.69100        1.33460
H         -0.63970       -1.23180       -0.66370
H         -0.77580        0.51170       -0.94780
H          0.40520        0.81430        1.36460
H          0.52430       -0.97840        1.36170
H          1.75530        0.73820       -0.87300
H          1.84860       -1.01990       -0.67000
H          2.88210        1.11690        1.28160
H          2.95510       -0.63330        1.58970
H          4.91080       -0.59950        0.40180
H          4.67690        1.12300       -0.08530
H          3.96070       -0.27920       -1.05930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_189_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

