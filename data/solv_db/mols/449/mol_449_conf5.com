%nproc=16
%mem=2GB
%chk=mol_449_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.57050       -0.66810        0.02700
O          2.50130        0.07960       -0.51980
C          1.33750        0.21450        0.22340
O          1.30270       -0.34160        1.34970
C          0.15640        0.95760       -0.21950
C         -0.44270        0.41700       -1.49190
C         -1.26160       -0.84990       -1.21470
C         -2.35640       -0.41810       -0.25780
C         -1.68310       -0.15700        1.06530
C         -0.82750        1.09810        0.89010
H          4.01430       -0.15130        0.90580
H          3.16670       -1.63350        0.42350
H          4.37580       -0.84450       -0.69860
H          0.51060        2.00010       -0.47120
H         -1.19390        1.17040       -1.83720
H          0.32770        0.26430       -2.23990
H         -1.63400       -1.20130       -2.17580
H         -0.61190       -1.56440       -0.68350
H         -2.80040        0.55590       -0.58620
H         -3.17060       -1.16190       -0.18840
H         -2.40820        0.04710        1.87470
H         -1.06870       -1.03390        1.30290
H         -0.27190        1.31090        1.82310
H         -1.53280        1.91000        0.62790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

