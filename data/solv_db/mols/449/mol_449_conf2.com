%nproc=16
%mem=2GB
%chk=mol_449_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.89640        0.15510       -0.04420
O          2.52650        0.44900        0.14240
C          1.54490       -0.51960       -0.09440
O          1.90680       -1.64430       -0.47270
C          0.13060       -0.12070        0.12840
C         -0.17620        1.03830       -0.79860
C         -1.58060        0.88560       -1.34100
C         -2.55160        0.46050       -0.27100
C         -2.18280       -0.81840        0.40450
C         -0.81580       -1.24760       -0.13420
H          4.34720       -0.37990        0.81060
H          4.06590       -0.50230       -0.93610
H          4.40450        1.13860       -0.21600
H          0.02290        0.22370        1.17950
H          0.52530        1.00200       -1.66080
H         -0.13500        2.01300       -0.27440
H         -1.93540        1.87410       -1.71960
H         -1.56560        0.20980       -2.21270
H         -3.56350        0.44970       -0.73130
H         -2.57410        1.28460        0.47890
H         -2.92800       -1.61710        0.31140
H         -2.02180       -0.68320        1.51400
H         -0.87410       -1.48630       -1.21270
H         -0.46640       -2.16440        0.39460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

