%nproc=16
%mem=2GB
%chk=mol_449_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.92940        0.06030       -0.53800
O         -2.64620       -0.17290       -0.02380
C         -1.52370        0.54330       -0.41900
O         -1.67450        1.44560       -1.28290
C         -0.17740        0.26760        0.14890
C          0.19320       -1.15950       -0.15440
C          1.56560       -1.38730        0.49180
C          2.52800       -0.56050       -0.34650
C          2.22560        0.88450       -0.29600
C          0.80170        1.27230       -0.33380
H         -3.90620        0.32830       -1.62160
H         -4.50000       -0.90190       -0.43050
H         -4.50910        0.80480        0.04560
H         -0.28410        0.33980        1.26010
H         -0.56950       -1.81570        0.30560
H          0.28310       -1.35600       -1.22770
H          1.53090       -0.93700        1.50930
H          1.84560       -2.43840        0.50730
H          2.51200       -1.00420       -1.37530
H          3.57170       -0.71750        0.02210
H          2.75430        1.33220        0.59650
H          2.73990        1.37460       -1.17510
H          0.67500        2.19690        0.29380
H          0.49350        1.60070       -1.36800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

