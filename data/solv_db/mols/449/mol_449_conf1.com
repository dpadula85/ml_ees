%nproc=16
%mem=2GB
%chk=mol_449_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.87060        0.45880        0.52180
O          2.55540        0.70830        0.01210
C          1.57350       -0.27050        0.00800
O          1.88000       -1.39420        0.47120
C          0.20130       -0.06070       -0.50750
C         -0.57330       -1.31440       -0.35490
C         -2.05050       -1.17290       -0.58340
C         -2.53580        0.23860       -0.57860
C         -1.83150        0.95800        0.56300
C         -0.38400        1.11130        0.24610
H          4.50790       -0.04400       -0.23590
H          3.80200       -0.12100        1.46280
H          4.35600        1.42880        0.76200
H          0.22570        0.21390       -1.60320
H         -0.34720       -1.77820        0.64860
H         -0.19680       -2.05070       -1.09760
H         -2.56940       -1.71070        0.24930
H         -2.39160       -1.68760       -1.51470
H         -3.61590        0.27370       -0.37520
H         -2.27770        0.69460       -1.55430
H         -2.28570        1.93000        0.78810
H         -1.91710        0.30390        1.46820
H          0.15640        1.24580        1.22510
H         -0.15210        2.03920       -0.33470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

