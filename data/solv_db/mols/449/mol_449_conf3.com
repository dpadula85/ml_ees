%nproc=16
%mem=2GB
%chk=mol_449_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.86770        0.67700        0.53450
O          2.48020        0.78580        0.35030
C          1.67540       -0.20060       -0.14590
O          2.18310       -1.30160       -0.47210
C          0.21920        0.02500       -0.29780
C         -0.44350        0.34590        1.04260
C         -1.79060        0.92410        0.67330
C         -2.60770        0.04750       -0.19010
C         -1.90270       -0.86590       -1.10690
C         -0.49980       -1.20580       -0.78520
H          4.14740        0.33470        1.55200
H          4.36930        0.07870       -0.26680
H          4.29170        1.71130        0.44270
H         -0.00810        0.84380       -1.02640
H          0.12520        1.05340        1.63470
H         -0.65390       -0.61520        1.57970
H         -2.33700        1.19420        1.59640
H         -1.57670        1.90110        0.14730
H         -3.33680       -0.50170        0.47660
H         -3.27760        0.71070       -0.81440
H         -2.48300       -1.83520       -1.14300
H         -1.93020       -0.50170       -2.17590
H         -0.48630       -1.93720        0.06120
H         -0.02530       -1.66830       -1.66680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_449_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

