%nproc=16
%mem=2GB
%chk=mol_183_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.56220        1.03560        0.47480
C          2.07730        0.04510       -0.53360
C          3.03170       -1.14720       -0.51310
C          0.70430       -0.50140       -0.20450
C         -0.32610        0.59690       -0.14530
C         -1.69690        0.11420        0.17550
C         -2.10880       -0.87410       -0.91910
C         -2.62670        1.31580        0.16850
C         -1.81950       -0.53760        1.52190
H          3.55650        0.73470        0.91780
H          1.88440        1.16690        1.34220
H          2.76230        2.03650        0.00290
H          2.10830        0.52070       -1.53590
H          4.05650       -0.79430       -0.74750
H          3.06640       -1.48850        0.55660
H          2.68450       -1.93350       -1.19940
H          0.79030       -0.95680        0.81730
H          0.41180       -1.31000       -0.87870
H         -0.36310        1.09680       -1.13520
H          0.00070        1.36820        0.58080
H         -3.20650       -0.72400       -1.12370
H         -1.87550       -1.90490       -0.61760
H         -1.60060       -0.58170       -1.87100
H         -2.49000        1.80680        1.17560
H         -3.67810        1.03180        0.05100
H         -2.28340        2.03080       -0.61630
H         -1.28270        0.02450        2.28440
H         -1.43680       -1.58170        1.52140
H         -2.90240       -0.58950        1.76470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

