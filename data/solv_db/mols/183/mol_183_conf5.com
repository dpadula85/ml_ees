%nproc=16
%mem=2GB
%chk=mol_183_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.26680       -0.20760        0.81480
C          1.84120        0.42570        0.54070
C          1.92880        0.65070       -0.97550
C          0.83210       -0.58650        0.95430
C         -0.58690       -0.37170        0.94370
C         -1.47210       -0.03980       -0.14910
C         -1.32820        1.23310       -0.89930
C         -2.90940        0.00650        0.46320
C         -1.60370       -1.14380       -1.20370
H          3.28400       -1.10400        0.16330
H          3.96510        0.56670        0.49480
H          3.33130       -0.49430        1.86270
H          1.78160        1.39220        1.06060
H          3.01570        0.59830       -1.30100
H          1.34930       -0.14240       -1.44550
H          1.63380        1.68560       -1.22440
H          1.15570       -0.84490        2.04150
H          1.14880       -1.56990        0.45290
H         -1.05920       -1.29130        1.49860
H         -0.81140        0.41210        1.78890
H         -0.96340        1.12110       -1.94650
H         -0.78310        2.01200       -0.31880
H         -2.35740        1.69340       -1.05310
H         -2.99060       -0.80540        1.21630
H         -3.61170       -0.08150       -0.36510
H         -2.95160        0.99860        0.95770
H         -1.79080       -2.08400       -0.68330
H         -0.78110       -1.14190       -1.91700
H         -2.53370       -0.88720       -1.77170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

