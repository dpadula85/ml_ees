%nproc=16
%mem=2GB
%chk=mol_183_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.31540       -1.38440        0.41460
C          2.09810       -0.14850       -0.42570
C          3.26900        0.77340       -0.22570
C          0.79260        0.55210       -0.05030
C         -0.31980       -0.42350       -0.28720
C         -1.67960        0.08480        0.02540
C         -1.89140        0.50600        1.43600
C         -2.64860       -1.07520       -0.21700
C         -2.00760        1.23420       -0.92020
H          1.70870       -1.40330        1.32170
H          3.37620       -1.49510        0.72270
H          2.06590       -2.28960       -0.20050
H          2.01800       -0.38810       -1.50510
H          2.97630        1.53530        0.55300
H          3.46210        1.30890       -1.16420
H          4.13100        0.18630        0.11130
H          0.84240        0.95420        0.95610
H          0.69150        1.40720       -0.75470
H         -0.29880       -0.72240       -1.35590
H         -0.14990       -1.35280        0.30240
H         -1.31850        1.36780        1.76600
H         -1.71970       -0.37820        2.10420
H         -2.97150        0.76510        1.55420
H         -3.69710       -0.76480       -0.03970
H         -2.35210       -1.88400        0.48670
H         -2.48530       -1.47690       -1.23970
H         -1.88540        0.91590       -1.96730
H         -3.04920        1.55980       -0.68450
H         -1.27250        2.03580       -0.71670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

