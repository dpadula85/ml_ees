%nproc=16
%mem=2GB
%chk=mol_183_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.48150        1.17420       -0.31550
C          2.12870       -0.30060       -0.37640
C          3.14520       -1.03460        0.49730
C          0.76050       -0.56690        0.21750
C         -0.27170        0.19170       -0.53560
C         -1.66760        0.04570       -0.08720
C         -1.81400        0.51070        1.36770
C         -2.27150       -1.30070       -0.19000
C         -2.52670        1.01070       -0.90300
H          1.91440        1.78390       -1.01820
H          2.41100        1.49770        0.73960
H          3.56630        1.24370       -0.60470
H          2.20200       -0.64670       -1.42930
H          3.42240       -0.35880        1.32910
H          4.04050       -1.32860       -0.07830
H          2.64730       -1.92530        0.94730
H          0.81660       -0.19510        1.27620
H          0.62680       -1.64160        0.27070
H          0.00130        1.26790       -0.50920
H         -0.20710       -0.10460       -1.61020
H         -1.69840       -0.33600        2.05910
H         -1.11160        1.33690        1.59440
H         -2.84530        0.91680        1.53240
H         -2.17930       -1.68000       -1.24610
H         -3.36880       -1.20780       -0.01310
H         -1.90860       -2.04230        0.54270
H         -1.89760        1.67830       -1.53670
H         -3.16190        0.44030       -1.64160
H         -3.23440        1.57120       -0.27920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

