%nproc=16
%mem=2GB
%chk=mol_183_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.90790       -1.13740        0.19240
C          1.68690       -0.20110        0.42300
C          2.31110        1.13180        0.69650
C          0.88650       -0.33270       -0.78450
C         -0.36970        0.36270       -1.01160
C         -1.52840        0.09170       -0.10240
C         -2.67540        0.97260       -0.64230
C         -1.36240        0.48520        1.32770
C         -2.01340       -1.34140       -0.23030
H          2.56420       -2.04710       -0.35120
H          3.52990       -0.57020       -0.54180
H          3.41070       -1.36180        1.12760
H          1.23290       -0.58180        1.34710
H          3.24950        1.25680        0.11920
H          2.61990        1.15920        1.78020
H          1.67700        1.99050        0.52900
H          0.65590       -1.44660       -0.89460
H          1.56920       -0.17050       -1.68370
H         -0.30270        1.46710       -1.14480
H         -0.73990        0.02960       -2.03820
H         -2.52760        2.02570       -0.33370
H         -3.64880        0.55430       -0.35950
H         -2.59930        0.94560       -1.75050
H         -2.36390        0.82960        1.70750
H         -1.03240       -0.41990        1.88890
H         -0.67250        1.31650        1.44950
H         -1.82580       -1.68260       -1.24880
H         -1.53690       -1.97920        0.55050
H         -3.10240       -1.34680       -0.02140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_183_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

