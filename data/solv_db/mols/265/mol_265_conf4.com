%nproc=16
%mem=2GB
%chk=mol_265_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90050        0.21300       -0.38310
C          0.49380        0.35100        0.13680
C         -0.42450       -0.35090       -0.82560
C         -1.83030       -0.28050       -0.41720
C         -2.99000       -0.20670       -0.09170
H          2.43520       -0.46980        0.33360
H          1.93910       -0.31180       -1.36360
H          2.38690        1.20220       -0.43540
H          0.35450       -0.10160        1.12490
H          0.15630        1.41080        0.19010
H         -0.12830       -1.43220       -0.88200
H         -0.28170        0.13130       -1.83020
H         -4.01130       -0.15470        0.18220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

