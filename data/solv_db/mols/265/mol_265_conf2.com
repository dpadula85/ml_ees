%nproc=16
%mem=2GB
%chk=mol_265_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91200        0.13880       -0.04460
C         -0.46850        0.19110       -0.44860
C          0.45770       -0.27950        0.67760
C          1.83170       -0.18120        0.15970
C          2.95540       -0.08210       -0.27010
H         -2.36770        1.17000       -0.10180
H         -2.06880       -0.21030        0.98840
H         -2.52240       -0.53660       -0.69340
H         -0.12720        1.22210       -0.69170
H         -0.31390       -0.44200       -1.36120
H          0.28260        0.37440        1.54360
H          0.29150       -1.35790        0.88850
H          3.96150       -0.00670       -0.64610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

