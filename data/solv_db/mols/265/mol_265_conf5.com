%nproc=16
%mem=2GB
%chk=mol_265_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31030       -0.77600       -0.18830
C         -0.81340        0.52420        0.37780
C          0.48560        0.97140       -0.22570
C          1.53920        0.00560       -0.00980
C          2.43420       -0.79130        0.18310
H         -0.56310       -1.57310       -0.18520
H         -1.63630       -0.63500       -1.24310
H         -2.20600       -1.06690        0.42730
H         -0.66060        0.40410        1.47900
H         -1.56770        1.31330        0.15100
H          0.71660        1.97390        0.19440
H          0.34830        1.12980       -1.31950
H          3.23360       -1.47990        0.35910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

