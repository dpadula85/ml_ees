%nproc=16
%mem=2GB
%chk=mol_265_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17780        0.87040        0.29340
C         -0.84140       -0.31600       -0.58760
C          0.28270       -1.13680        0.03810
C          1.44870       -0.25680        0.16630
C          2.40800        0.49180        0.27300
H         -0.22790        1.42700        0.43900
H         -1.85500        1.53710       -0.27950
H         -1.65850        0.58260        1.22550
H         -1.68310       -0.99000       -0.75630
H         -0.44190        0.11040       -1.54000
H          0.02850       -1.49160        1.04040
H          0.47730       -1.96770       -0.69320
H          3.24040        1.13970        0.38090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

