%nproc=16
%mem=2GB
%chk=mol_265_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38820       -0.52880       -0.56660
C         -0.70380       -0.42640        0.77080
C          0.61040        0.30490        0.67990
C          1.47570       -0.44480       -0.24980
C          2.17560       -1.05560       -1.02310
H         -2.47480       -0.68750       -0.37050
H         -0.94110       -1.42150       -1.07850
H         -1.29900        0.38870       -1.17360
H         -0.51240       -1.45640        1.13420
H         -1.31980        0.07780        1.53770
H          1.09860        0.30330        1.67470
H          0.49490        1.36170        0.38350
H          2.78380       -1.60210       -1.71880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_265_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

