%nproc=16
%mem=2GB
%chk=mol_212_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.46510       -0.08640       -0.04230
C         -0.85820        1.16430       -0.02280
C          0.52840        1.20200        0.01710
C          1.29500        0.05530        0.03730
C          0.66420       -1.17030        0.01720
C         -0.71950       -1.23190       -0.02260
I          3.41430        0.21080        0.09810
H         -2.56040       -0.11730       -0.07390
H         -1.43220        2.07920       -0.03770
H          1.04200        2.16830        0.03330
H          1.28040       -2.05960        0.03370
H         -1.18900       -2.21440       -0.03750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_212_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_212_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_212_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

