%nproc=16
%mem=2GB
%chk=mol_097_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.74250       -1.10590       -0.18440
C         -0.63380       -1.22340       -0.16000
C         -1.46300       -0.12820       -0.03560
C         -0.85560        1.10560        0.06490
C          0.51810        1.22250        0.04050
C          1.34970        0.12900       -0.08380
C          2.79070        0.27210       -0.10740
N          3.93990        0.39660       -0.13960
O         -2.84820       -0.26800       -0.01290
H          1.40030       -1.96440       -0.28210
H         -1.04810       -2.21900       -0.24320
H         -1.49820        1.96920        0.16250
H          0.96540        2.21700        0.12280
H         -3.35970       -0.40310        0.85830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_097_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_097_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_097_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

