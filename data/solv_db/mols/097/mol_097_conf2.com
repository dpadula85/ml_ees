%nproc=16
%mem=2GB
%chk=mol_097_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.59310       -1.20520       -0.11670
C         -0.78160       -1.21360       -0.10850
C         -1.42310       -0.00220       -0.17780
C         -0.71040        1.17700       -0.25270
C          0.66950        1.19540       -0.26150
C          1.30990       -0.02670       -0.19160
C          2.75840       -0.03880       -0.19910
N          3.92180       -0.05640       -0.20690
O         -2.81090        0.08010       -0.17500
H          1.11320       -2.14630       -0.06290
H         -1.38780       -2.10230       -0.05160
H         -1.20890        2.15310       -0.30870
H          1.23260        2.13220       -0.32100
H         -3.27570        0.05370       -1.06490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_097_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_097_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_097_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

