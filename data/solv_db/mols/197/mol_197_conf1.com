%nproc=16
%mem=2GB
%chk=mol_197_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.05350       -0.54950       -0.22280
C         -0.02890        0.49580       -0.08030
C         -1.25290       -0.17350        0.54560
O         -1.60880       -1.25340       -0.25450
O          0.34770        1.57150        0.69220
O          2.20060       -0.02900       -0.81620
H          0.64520       -1.32080       -0.93180
H          1.29830       -1.04220        0.72130
H         -0.28750        0.87210       -1.09080
H         -2.06700        0.56080        0.67310
H         -0.98900       -0.58990        1.53950
H         -2.19210       -0.95580       -0.98300
H         -0.09200        2.41750        0.41790
H          2.97300       -0.00370       -0.21040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

