%nproc=16
%mem=2GB
%chk=mol_197_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.90570       -0.79740       -0.50600
C          0.03730        0.36880       -0.59680
C          0.97550        0.46100        0.57330
O          1.70210       -0.69620        0.76910
O         -0.63770        1.57650       -0.70180
O         -1.75080       -0.75240        0.57050
H         -1.54090       -0.76360       -1.42910
H         -0.35630       -1.75840       -0.50290
H          0.63700        0.24960       -1.52210
H          0.37480        0.76090        1.45130
H          1.69120        1.29430        0.34020
H          2.39990       -0.73420        0.06190
H         -1.29080        1.59750        0.04790
H         -1.33560       -0.80650        1.44470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

