%nproc=16
%mem=2GB
%chk=mol_197_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21430        0.09510       -0.07760
C          0.08990       -0.44550       -0.64510
C          1.23530       -0.09560        0.27210
O          1.50320        1.24680        0.34280
O         -0.04280       -1.85210       -0.59600
O         -1.24590        1.47370       -0.05600
H         -2.09220       -0.33340       -0.58360
H         -1.25730       -0.30390        0.97830
H          0.25250       -0.17110       -1.69020
H          2.15720       -0.62050       -0.04850
H          0.97490       -0.51840        1.27700
H          1.17240        1.77870       -0.42500
H         -0.19110       -2.04230        0.38490
H         -1.34170        1.78860        0.86670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

