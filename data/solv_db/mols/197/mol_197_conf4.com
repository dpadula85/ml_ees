%nproc=16
%mem=2GB
%chk=mol_197_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31650        0.25630        0.27190
C          0.01450        0.33280       -0.47410
C          1.04870       -0.36060        0.36970
O          2.28590       -0.40480       -0.25040
O          0.31220        1.63000       -0.78060
O         -1.63570       -1.05420        0.59180
H         -2.13530        0.72250       -0.28580
H         -1.22930        0.80760        1.24660
H         -0.09250       -0.24260       -1.41110
H          1.11650        0.22620        1.32970
H          0.64240       -1.36990        0.60480
H          2.22410       -1.14500       -0.93560
H          0.64700        2.18060       -0.05120
H         -1.88210       -1.57890       -0.22570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

