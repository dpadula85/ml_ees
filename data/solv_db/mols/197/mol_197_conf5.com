%nproc=16
%mem=2GB
%chk=mol_197_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.91020        0.20910       -0.87640
C         -0.03370       -0.64330       -0.08400
C         -1.00530        0.14540        0.74300
O         -1.78980        0.97980       -0.05550
O          0.72070       -1.45500        0.75340
O          1.69340        1.00620       -0.06490
H          1.58140       -0.47360       -1.43800
H          0.38430        0.88950       -1.58790
H         -0.60750       -1.30140       -0.75020
H         -0.52710        0.72650        1.55380
H         -1.68460       -0.58390        1.23380
H         -2.49180        0.37400       -0.45200
H          0.39240       -2.39060        0.77910
H          2.45740        0.42460        0.24570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_197_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

