%nproc=16
%mem=2GB
%chk=mol_007_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79650        0.05330        0.03960
C          0.66640       -0.35380       -0.00870
I          1.78980        1.47930        0.02440
H         -1.47850       -0.74230       -0.26240
H         -1.09210        0.44770        1.02730
H         -0.89290        0.91030       -0.67450
H          0.83150       -0.83050       -0.99350
H          0.97230       -0.96410        0.84780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

