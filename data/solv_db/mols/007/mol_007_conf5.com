%nproc=16
%mem=2GB
%chk=mol_007_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.78250       -0.22120       -0.01260
C          0.59160        0.41020        0.04630
I          2.12130       -1.04200       -0.29580
H         -1.37880       -0.03600        0.90280
H         -1.29890        0.16220       -0.93550
H         -0.64070       -1.31990       -0.07150
H          0.68290        1.22260       -0.71440
H          0.70510        0.82410        1.08070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

