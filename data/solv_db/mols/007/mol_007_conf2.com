%nproc=16
%mem=2GB
%chk=mol_007_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79090        0.12390       -0.03110
C          0.63350       -0.34630        0.01060
I          1.97850        1.31040       -0.07120
H         -1.35430       -0.11740        0.91040
H         -1.34510       -0.25710       -0.92360
H         -0.79970        1.24710       -0.07430
H          0.80090       -0.88230        0.96970
H          0.87720       -1.07830       -0.79060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

