%nproc=16
%mem=2GB
%chk=mol_007_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.80070        0.05970       -0.03850
C          0.66870       -0.35400        0.00790
I          1.78300        1.46840       -0.02750
H         -1.46950       -0.74160        0.27010
H         -1.08500        0.45210       -1.03690
H         -0.88520        0.91710        0.68400
H          0.82940       -0.83220        0.99130
H          0.95930       -0.96950       -0.85030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

