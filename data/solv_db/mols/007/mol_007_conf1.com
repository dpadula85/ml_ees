%nproc=16
%mem=2GB
%chk=mol_007_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.78570        0.19360       -0.07300
C          0.58860       -0.35990        0.13440
I          2.09680        1.14890       -0.10370
H         -0.73220        1.29720       -0.10260
H         -1.16130       -0.21950       -1.04580
H         -1.51290       -0.10090        0.70420
H          0.63610       -0.83090        1.12490
H          0.87070       -1.12840       -0.63840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_007_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

