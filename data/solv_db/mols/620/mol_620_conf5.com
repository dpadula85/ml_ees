%nproc=16
%mem=2GB
%chk=mol_620_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.75430        0.66590       -0.88540
C         -3.23220        0.47560       -0.93780
C         -2.92300       -0.72460       -0.09970
C         -1.47030       -1.07880       -0.03700
C         -0.60130        0.01340        0.53720
C          0.85810       -0.43230        0.56770
C          1.76200        0.63280        1.13200
C          3.19510        0.18570        1.17490
C          3.77370       -0.15620       -0.13740
O          3.49250       -1.21170       -0.66000
C          4.70630        0.74620       -0.86050
H         -5.01620        1.61310       -1.35750
H         -5.19340       -0.20130       -1.45980
H         -5.09180        0.61220        0.15960
H         -2.79180        1.42570       -0.53140
H         -2.95340        0.33750       -1.99360
H         -3.28810       -0.55640        0.93550
H         -3.49690       -1.57470       -0.56130
H         -1.07170       -1.40050       -1.03810
H         -1.38910       -1.96210        0.63330
H         -0.73660        0.94050       -0.04550
H         -0.92710        0.21800        1.57420
H          0.92180       -1.32850        1.21420
H          1.12360       -0.75790       -0.45190
H          1.46060        0.82870        2.18700
H          1.64470        1.58870        0.55610
H          3.82010        0.92820        1.72910
H          3.21850       -0.75210        1.80510
H          4.24550        1.28130       -1.69920
H          5.20830        1.39720       -0.13430
H          5.50660        0.09910       -1.31560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_620_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_620_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_620_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

