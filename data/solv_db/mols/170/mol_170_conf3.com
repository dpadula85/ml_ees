%nproc=16
%mem=2GB
%chk=mol_170_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48050       -0.32390        0.05890
C         -2.06080       -0.02300       -0.38900
C         -1.28860        0.47910        0.79060
N          0.06160        0.82510        0.56770
C          0.98770       -0.22280        0.30150
C          2.35570        0.44410        0.10800
C          3.44290       -0.56670       -0.17080
H         -4.17630       -0.39750       -0.80130
H         -3.53170       -1.27160        0.62470
H         -3.84770        0.56240        0.65930
H         -1.59220       -0.93940       -0.75110
H         -2.09230        0.78140       -1.14310
H         -1.42960       -0.26120        1.62570
H         -1.81520        1.40490        1.16730
H          0.42310        1.44070        1.30660
H          1.11740       -0.94330        1.14460
H          0.76900       -0.79770       -0.59910
H          2.61680        1.03470        1.03600
H          2.31870        1.14680       -0.77340
H          3.85790       -0.97990        0.78330
H          4.26400       -0.02610       -0.68240
H          3.10000       -1.36600       -0.82540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

