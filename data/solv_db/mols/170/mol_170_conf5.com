%nproc=16
%mem=2GB
%chk=mol_170_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.33940        0.29430        0.03350
C         -2.00860       -0.46140        0.22870
C         -0.99430        0.58430        0.55120
N          0.33230        0.13000        0.78140
C          0.89570       -0.37610       -0.42810
C          2.35470       -0.76940       -0.26260
C          3.15390        0.47090        0.16960
H         -3.53630        0.78660        1.00860
H         -4.12030       -0.36820       -0.33100
H         -3.14030        1.11500       -0.70260
H         -2.15640       -1.23290        0.97360
H         -1.81420       -0.92810       -0.78250
H         -1.39210        1.21730        1.39160
H         -0.99440        1.29600       -0.31740
H          0.35910       -0.54690        1.58480
H          0.83920        0.38320       -1.20930
H          0.32000       -1.25870       -0.74280
H          2.50890       -1.62560        0.40870
H          2.73950       -1.05320       -1.25970
H          4.21910        0.39120       -0.11560
H          3.04510        0.62610        1.26160
H          2.72880        1.32570       -0.42170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

