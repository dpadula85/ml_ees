%nproc=16
%mem=2GB
%chk=mol_170_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.15310       -0.15320       -0.36360
C         -1.91130       -0.56590        0.40680
C         -1.07020        0.67820        0.72390
N          0.09810        0.36620        1.46750
C          1.13760       -0.34430        0.81780
C          1.87300        0.52020       -0.20850
C          2.98390       -0.31120       -0.85360
H         -3.32260        0.95360       -0.25020
H         -3.10040       -0.45880       -1.42800
H         -4.07240       -0.66810        0.05510
H         -1.31400       -1.35770       -0.10310
H         -2.18990       -1.01250        1.40920
H         -1.68610        1.37490        1.32950
H         -0.72200        1.15160       -0.24710
H          0.36420        1.01510        2.19880
H          0.77920       -1.27950        0.29940
H          1.91930       -0.68760        1.53890
H          2.39890        1.35430        0.33750
H          1.19320        0.92820       -0.96810
H          3.25120        0.15730       -1.85040
H          2.69280       -1.34530       -1.03930
H          3.85060       -0.31540       -0.16210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

