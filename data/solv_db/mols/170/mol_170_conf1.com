%nproc=16
%mem=2GB
%chk=mol_170_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.94360        0.38020        0.12860
C          1.95410        0.64570       -1.01900
C          0.50980        0.63600       -0.47680
N          0.26000       -0.63200        0.07170
C         -0.91890       -0.90290        0.77560
C         -2.20060       -0.90570       -0.00830
C         -2.57320        0.38980       -0.64420
H          3.91870        0.16230       -0.38980
H          3.04890        1.30460        0.74090
H          2.68370       -0.51560        0.70210
H          2.12220       -0.16600       -1.73880
H          2.22820        1.64140       -1.41270
H         -0.10710        0.73580       -1.42750
H          0.39830        1.51320        0.17950
H          0.97870       -1.34980        0.03570
H         -1.02220       -0.31180        1.73170
H         -0.82980       -1.96770        1.17070
H         -3.06320       -1.22630        0.64220
H         -2.15260       -1.69640       -0.79870
H         -2.05550        1.24630       -0.14490
H         -2.44890        0.39550       -1.74620
H         -3.67430        0.62320       -0.48960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

