%nproc=16
%mem=2GB
%chk=mol_170_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.41900        1.12490       -0.59230
C          2.30050        0.06080        0.48710
C          1.24110       -0.97320        0.16290
N         -0.01930       -0.27400        0.07960
C         -1.12140       -1.13010       -0.27210
C         -2.39180       -0.32110       -0.14100
C         -2.37760        0.88780       -1.02000
H          3.20610        0.80880       -1.33090
H          1.46820        1.38960       -1.05790
H          2.81460        2.04450       -0.08680
H          3.30210       -0.38050        0.63030
H          2.03550        0.60280        1.44150
H          1.14800       -1.63600        1.07290
H          1.49660       -1.58690       -0.70180
H         -0.21440        0.11780        1.02230
H         -1.18330       -2.02060        0.38990
H         -1.06940       -1.50740       -1.31710
H         -2.50900        0.01460        0.93480
H         -3.29260       -0.95100       -0.40580
H         -2.28940        1.83890       -0.41740
H         -1.59400        0.89110       -1.78780
H         -3.36970        0.99890       -1.54300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_170_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

