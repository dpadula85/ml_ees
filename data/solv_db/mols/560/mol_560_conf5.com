%nproc=16
%mem=2GB
%chk=mol_560_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33320       -0.50580       -0.21640
C          0.90050       -0.06870       -0.08430
C          0.58560        1.20400        0.34510
C         -0.73510        1.61190        0.46860
C         -1.77430        0.75560        0.16510
C         -1.47190       -0.51900       -0.26510
C         -0.15100       -0.90870       -0.38230
O         -2.50870       -1.39240       -0.57430
H          2.82440       -0.03650       -1.07100
H          2.81460       -0.21530        0.74800
H          2.38880       -1.60060       -0.27760
H          1.37070        1.90170        0.59170
H         -1.00940        2.60250        0.80250
H         -2.81490        1.03690        0.24860
H          0.08770       -1.90040       -0.71710
H         -2.84020       -1.96520        0.21860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

