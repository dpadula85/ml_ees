%nproc=16
%mem=2GB
%chk=mol_560_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30040       -0.39750        0.10810
C          0.86790        0.01430        0.04130
C          0.48800        1.34050        0.02850
C         -0.84380        1.67680       -0.03370
C         -1.85420        0.74010       -0.08570
C         -1.48180       -0.59210       -0.07320
C         -0.13980       -0.92650       -0.01050
O         -2.49200       -1.55190       -0.12520
H          2.34550       -1.44250       -0.27430
H          2.95030        0.22480       -0.52260
H          2.61960       -0.44830        1.16970
H          1.28870        2.08090        0.06980
H         -1.12170        2.73130       -0.04280
H         -2.88860        1.05250       -0.13380
H          0.15470       -1.98380       -0.00090
H         -2.19320       -2.51850       -0.11490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

