%nproc=16
%mem=2GB
%chk=mol_560_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24910       -0.54940        0.03850
C         -0.85780       -0.06820        0.15140
C         -0.61430        1.27920        0.06840
C          0.70490        1.68520        0.17960
C          1.73630        0.81060        0.36350
C          1.43670       -0.52870        0.43980
C          0.15140       -0.97570        0.33570
O          2.43780       -1.49510        0.62790
H         -2.51590       -0.50240       -1.05730
H         -2.32710       -1.62500        0.35760
H         -2.97120        0.03760        0.63010
H         -1.41800        2.00080       -0.07830
H          0.89850        2.73790        0.11500
H          2.78330        1.14120        0.45110
H         -0.09460       -2.02880        0.39450
H          2.89900       -1.91920       -0.17530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

