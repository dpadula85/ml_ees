%nproc=16
%mem=2GB
%chk=mol_560_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.35090       -0.12900       -0.16830
C          0.88580        0.07500       -0.05820
C          0.39200        1.31120        0.29950
C         -0.97020        1.51370        0.40530
C         -1.83020        0.45000        0.14480
C         -1.36360       -0.79660       -0.21470
C          0.00190       -0.96200       -0.31050
O         -2.27800       -1.81130       -0.45970
H          2.68600       -1.11220        0.19100
H          2.60500       -0.04670       -1.26010
H          2.89400        0.69020        0.32870
H          1.11060        2.10500        0.49130
H         -1.32570        2.49430        0.68790
H         -2.91470        0.59700        0.22530
H          0.35440       -1.94540       -0.59370
H         -2.59820       -2.43330        0.29130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

