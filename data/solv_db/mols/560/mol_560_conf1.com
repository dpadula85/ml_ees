%nproc=16
%mem=2GB
%chk=mol_560_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.35660       -0.32530        0.04920
C          0.89370        0.02160       -0.00670
C          0.46660        1.32740        0.08200
C         -0.88360        1.60640        0.02690
C         -1.84780        0.62930       -0.11500
C         -1.41630       -0.67800       -0.20340
C         -0.06020       -0.97360       -0.14940
O         -2.32050       -1.71890       -0.34730
H          2.78590        0.07320        1.01230
H          2.90470        0.03570       -0.82440
H          2.44630       -1.42550        0.12280
H          1.22300        2.11150        0.19460
H         -1.22270        2.64710        0.09780
H         -2.90980        0.87210       -0.15620
H          0.31780       -1.99210       -0.21580
H         -2.73370       -2.21080        0.43270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_560_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

