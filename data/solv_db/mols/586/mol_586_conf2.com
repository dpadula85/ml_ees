%nproc=16
%mem=2GB
%chk=mol_586_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.68790        0.65730       -0.79510
C         -0.72150        0.02540        0.15280
C         -1.14400       -0.48390        1.28490
C          0.69860       -0.04030       -0.16180
C          1.19470        0.45390       -1.28670
C          1.63610       -0.67560        0.79440
H         -1.25040        1.60780       -1.21700
H         -1.91630        0.00370       -1.63860
H         -2.61260        0.88330       -0.24150
H         -2.20470       -0.43590        1.51470
H         -0.42220       -0.93000        1.94850
H          0.53620        0.91330       -1.98280
H          2.24610        0.39140       -1.49230
H          1.73060        0.01850        1.67850
H          2.63720       -0.71260        0.32630
H          1.28010       -1.67630        1.11590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

