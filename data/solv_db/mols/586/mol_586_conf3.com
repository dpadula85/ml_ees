%nproc=16
%mem=2GB
%chk=mol_586_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.57430        0.87190       -0.78970
C         -0.56450        0.34830        0.17910
C         -0.78950        0.54890        1.45880
C          0.62270       -0.35250       -0.24090
C          0.85060       -0.55540       -1.52770
C          1.56510       -0.83030        0.81080
H         -2.26850        1.58030       -0.29030
H         -2.16840       -0.00750       -1.11650
H         -1.09660        1.32590       -1.68540
H         -0.05070        0.16550        2.16160
H         -1.67750        1.07530        1.77810
H          0.14030       -0.19160       -2.25030
H          1.73890       -1.07690       -1.79730
H          0.97370       -1.53590        1.45900
H          1.87700       -0.00860        1.47330
H          2.42180       -1.35750        0.37740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

