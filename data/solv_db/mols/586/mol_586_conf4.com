%nproc=16
%mem=2GB
%chk=mol_586_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.73470       -0.69380       -0.59020
C          0.68240        0.09130        0.13650
C          1.05340        0.97010        1.04660
C         -0.71590       -0.10300       -0.14680
C         -1.06440       -0.98220       -1.05590
C         -1.72260        0.68170        0.58250
H          2.73160       -0.39710       -0.23100
H          1.61330       -0.50060       -1.69020
H          1.59530       -1.76610       -0.36170
H          0.32260        1.53820        1.57450
H          2.10140        1.12220        1.26530
H         -2.10830       -1.14870       -1.28910
H         -0.34010       -1.56880       -1.60280
H         -1.70360        0.37420        1.65190
H         -1.44050        1.76120        0.55190
H         -2.73920        0.62150        0.15840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

