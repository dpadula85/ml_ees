%nproc=16
%mem=2GB
%chk=mol_586_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.74010       -0.69720        0.48730
C          0.69300        0.25590        0.09000
C          0.93230        1.53050       -0.12040
C         -0.66560       -0.24160       -0.07880
C         -0.95470       -1.50980        0.12060
C         -1.74900        0.69200       -0.48240
H          1.71030       -1.59970       -0.17380
H          1.53540       -1.05660        1.52690
H          2.75960       -0.27160        0.45130
H          1.94610        1.90360        0.00260
H          0.18810        2.23110       -0.40530
H         -1.96830       -1.84680       -0.01210
H         -0.18330       -2.20830        0.41470
H         -1.98590        1.38590        0.34070
H         -1.38280        1.32830       -1.33730
H         -2.61510        0.10410       -0.82410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

