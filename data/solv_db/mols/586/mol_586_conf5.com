%nproc=16
%mem=2GB
%chk=mol_586_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.51790       -0.63090        0.27380
C         -0.28100        0.72800        0.47450
C         -1.01290        1.25800        1.23420
C          0.61110        0.08180       -0.45150
C          1.15220        0.77970       -1.44820
C          1.28700       -1.24110       -0.19240
H         -2.06230       -0.03210       -0.42110
H         -0.88540       -1.35710       -0.09520
H         -1.82400       -0.61720        1.27120
H         -1.98510        1.05310        1.64340
H         -0.66140        2.26740        1.68960
H          0.87320        1.82590       -1.51530
H          1.86460        0.35400       -2.15450
H          1.19030       -1.90280       -1.05110
H          0.88430       -1.62680        0.75940
H          2.36740       -0.93990       -0.01690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_586_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

