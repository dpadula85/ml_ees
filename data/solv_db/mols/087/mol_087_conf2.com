%nproc=16
%mem=2GB
%chk=mol_087_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.49960       -0.07210        0.08120
C         -2.73770       -1.21730       -0.15240
C         -1.37500       -1.18440       -0.18530
C         -0.73180        0.01320        0.01780
C         -1.49110        1.13590        0.24730
C         -2.86420        1.13190        0.28580
C          0.72060        0.02360       -0.02220
C          1.41570        0.92060        0.75070
C          2.79290        0.94910        0.73090
C          3.49460        0.06590       -0.07540
C          2.80610       -0.84170       -0.85810
C          1.43020       -0.84650       -0.81740
H         -4.58020       -0.19110        0.09040
H         -3.24360       -2.16580       -0.31330
H         -0.83850       -2.10830       -0.37110
H         -1.00190        2.09420        0.41130
H         -3.46670        2.01890        0.46690
H          0.91140        1.62980        1.39490
H          3.34760        1.66740        1.34990
H          4.58590        0.08910       -0.09030
H          3.38450       -1.52900       -1.48690
H          0.94080       -1.58340       -1.45480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

