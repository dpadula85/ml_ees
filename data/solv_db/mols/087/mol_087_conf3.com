%nproc=16
%mem=2GB
%chk=mol_087_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.37840       -0.62950        0.68370
C          3.01100        0.12980       -0.42260
C          1.65620        0.34900       -0.63890
C          0.68520       -0.15750        0.19960
C          1.11910       -0.91020        1.29070
C          2.43740       -1.15060        1.54160
C         -0.72700        0.09130       -0.05780
C         -1.68840       -0.80350        0.28760
C         -3.01010       -0.52580        0.02480
C         -3.36440        0.65640       -0.58750
C         -2.41670        1.59020       -0.95340
C         -1.10290        1.26830       -0.66980
H          4.44400       -0.79380        0.84130
H          3.77360        0.52770       -1.08100
H          1.38070        0.93890       -1.49970
H          0.34440       -1.29380        1.92920
H          2.74000       -1.73810        2.39600
H         -1.43740       -1.74790        0.77380
H         -3.80170       -1.23960        0.29770
H         -4.40190        0.91990       -0.81620
H         -2.69040        2.53700       -1.44310
H         -0.32920        1.98180       -0.94490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

