%nproc=16
%mem=2GB
%chk=mol_087_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.54250       -0.01480       -0.01030
C          2.84550        0.83440        0.82000
C          1.45340        0.83880        0.82880
C          0.72340        0.00450        0.01600
C          1.44180       -0.84130       -0.81000
C          2.82980       -0.86140       -0.83400
C         -0.73570        0.01570        0.03620
C         -1.47860        0.28760        1.16230
C         -2.87130        0.27660        1.12890
C         -3.55980       -0.00740       -0.03170
C         -2.81600       -0.27760       -1.15650
C         -1.43890       -0.26740       -1.12140
H          4.62930       -0.01240       -0.01990
H          3.42690        1.49410        1.45490
H          0.94880        1.52800        1.50440
H          0.87810       -1.50410       -1.45930
H          3.39350       -1.52260       -1.48720
H         -0.92490        0.51200        2.07050
H         -3.42100        0.50000        2.03410
H         -4.65470       -0.00200       -0.03520
H         -3.34490       -0.49810       -2.07050
H         -0.86740       -0.48270       -2.02010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_087_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

