%nproc=16
%mem=2GB
%chk=mol_103_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.81250        0.97890        0.38910
C         -3.30740        0.78450        0.25960
C         -2.97170       -0.67830        0.32230
C         -1.48600       -0.89750        0.19940
C         -0.96470       -0.36370       -1.10280
C          0.50860       -0.57410       -1.25610
C          1.33670        0.09480       -0.18520
C          2.79800       -0.23090       -0.51750
C          3.64010        0.43980        0.55730
C          5.09160        0.11070        0.21910
H         -5.26040        0.60620       -0.55230
H         -5.22330        0.53030        1.29680
H         -4.97990        2.08410        0.38430
H         -3.06130        1.23690       -0.74570
H         -2.75740        1.29830        1.06840
H         -3.46550       -1.26660       -0.47400
H         -3.28440       -1.05210        1.30140
H         -1.22440       -1.97120        0.29720
H         -0.92560       -0.38520        1.02380
H         -1.48340       -0.83030       -1.93720
H         -1.11710        0.75190       -1.07300
H          0.76760       -1.67200       -1.19270
H          0.89110       -0.20190       -2.22660
H          1.18530        1.17560       -0.11660
H          1.08900       -0.35240        0.81990
H          2.91300       -1.31290       -0.52520
H          2.98850        0.21830       -1.50730
H          3.46870        1.52520        0.56380
H          3.41260       -0.05680        1.50750
H          5.14310       -0.99480        0.12080
H          5.37140        0.52430       -0.77260
H          5.71980        0.48110        1.03430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_103_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_103_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_103_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

