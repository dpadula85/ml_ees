%nproc=16
%mem=2GB
%chk=mol_204_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.81380        0.98790       -0.87660
C          3.47610       -0.14820        0.06520
C          2.08990        0.02850        0.65140
C          1.10560        0.04680       -0.48610
C         -0.29570        0.22460        0.09010
O         -0.56590        1.36770        0.33880
C         -1.05550       -0.98520        0.24310
C         -2.47610       -1.01320        0.58200
C         -3.48120       -0.47130       -0.36620
C         -3.41490        0.96060       -0.72330
H          3.53890        1.93030       -0.32290
H          3.27600        0.88900       -1.83200
H          4.92340        0.94540       -1.03880
H          4.26000       -0.17980        0.85640
H          3.52890       -1.10270       -0.52300
H          1.90810       -0.81570        1.34710
H          2.05560        0.97960        1.20490
H          1.20060       -0.89740       -1.03300
H          1.35730        0.90130       -1.13900
H         -0.91920       -1.58700       -0.72290
H         -0.48470       -1.62040        1.00650
H         -2.73550       -2.12100        0.74070
H         -2.68060       -0.61180        1.63330
H         -3.50680       -1.08650       -1.32940
H         -4.53460       -0.67810        0.03750
H         -3.32270        1.65510        0.11290
H         -4.40170        1.23520       -1.22770
H         -2.65900        1.16630       -1.49580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

