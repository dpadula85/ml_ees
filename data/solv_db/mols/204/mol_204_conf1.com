%nproc=16
%mem=2GB
%chk=mol_204_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.66080       -0.14860       -0.00090
C          3.31670        0.30870        0.54830
C          2.25140       -0.37640       -0.26050
C          0.85680       -0.02140        0.16730
C         -0.11310       -0.72140       -0.65970
O          0.28870       -1.46880       -1.54910
C         -1.58850       -0.63440       -0.54960
C         -2.06590        0.27440        0.53190
C         -3.57890        0.26900        0.53300
C         -4.05000        0.76160       -0.83220
H          4.84870       -1.22440        0.20220
H          4.69790       -0.03690       -1.12150
H          5.50310        0.41660        0.41770
H          3.26540       -0.05250        1.59360
H          3.25340        1.39220        0.44220
H          2.36110       -0.16150       -1.33980
H          2.38020       -1.47400       -0.12400
H          0.72840       -0.36670        1.23340
H          0.74220        1.07780        0.20390
H         -1.96580       -0.30380       -1.53940
H         -1.96200       -1.66930       -0.40380
H         -1.77190        1.32280        0.22840
H         -1.62660        0.10840        1.50860
H         -3.90950       -0.79140        0.62910
H         -4.00060        0.91300        1.33020
H         -4.02210       -0.10080       -1.52910
H         -3.39500        1.55490       -1.23610
H         -5.10460        1.15310       -0.75780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

