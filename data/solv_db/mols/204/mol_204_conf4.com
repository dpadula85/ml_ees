%nproc=16
%mem=2GB
%chk=mol_204_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.17060        0.19580       -0.88400
C         -3.64880        0.23070        0.51440
C         -2.42040       -0.57840        0.74300
C         -1.25210       -0.13610       -0.11200
C         -0.04380       -0.96670        0.14530
O         -0.13400       -1.85950        0.97160
C          1.21600       -0.72180       -0.56740
C          2.21910        0.05570        0.26320
C          3.47340        0.24860       -0.56640
C          4.52910        1.01840        0.19060
H         -3.55060        0.75040       -1.60810
H         -5.17250        0.70810       -0.87380
H         -4.35930       -0.84190       -1.18320
H         -4.45220       -0.21890        1.16810
H         -3.56220        1.28790        0.88900
H         -2.65880       -1.63420        0.53050
H         -2.12380       -0.45780        1.80780
H         -1.03090        0.93310        0.05530
H         -1.54450       -0.27670       -1.17130
H          1.71060       -1.70580       -0.80880
H          1.07460       -0.22710       -1.54610
H          1.82090        1.05100        0.54200
H          2.52430       -0.52450        1.15640
H          3.92100       -0.74240       -0.81420
H          3.25290        0.79400       -1.49860
H          4.09630        1.82720        0.81950
H          5.06260        0.33780        0.86640
H          5.22370        1.45300       -0.55840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

