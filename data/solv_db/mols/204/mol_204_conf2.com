%nproc=16
%mem=2GB
%chk=mol_204_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.00890        1.03700        0.67120
C         -2.82330        0.79540       -0.27470
C         -2.25490       -0.53560        0.11240
C         -1.08780       -0.98470       -0.68350
C          0.10810       -0.12220       -0.60910
O          0.13920        0.85960        0.09720
C          1.29960       -0.46420       -1.41350
C          2.41120       -1.10500       -0.65530
C          2.97630       -0.25270        0.45690
C          3.54690        1.04300       -0.07440
H         -3.76600        0.57300        1.67590
H         -4.21100        2.10510        0.75300
H         -4.84340        0.42210        0.27380
H         -3.18840        0.72250       -1.32700
H         -2.09990        1.61980       -0.16780
H         -3.05950       -1.30060        0.07760
H         -1.98090       -0.44680        1.20000
H         -0.83220       -2.02370       -0.34190
H         -1.41050       -1.09340       -1.75820
H          1.01050       -1.12100       -2.26630
H          1.73970        0.45410       -1.90250
H          2.13380       -2.10800       -0.23800
H          3.23080       -1.32270       -1.38050
H          2.19480       -0.02850        1.22410
H          3.80460       -0.76850        0.98990
H          3.56730        0.94820       -1.19780
H          4.56790        1.24360        0.32310
H          2.83620        1.85430        0.15790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

