%nproc=16
%mem=2GB
%chk=mol_204_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.40870        1.15060        0.53020
C          3.48980       -0.21820        0.00840
C          2.43980       -1.21550        0.25540
C          1.06510       -1.01190       -0.23300
C          0.26580        0.08250        0.30520
O          0.50090        0.83410        1.22720
C         -1.08830        0.35610       -0.34100
C         -2.10690       -0.53460        0.35620
C         -3.45490       -0.29080       -0.26080
C         -3.92130        1.14670       -0.12250
H          2.78960        1.85170       -0.08810
H          3.24900        1.26590        1.61710
H          4.46210        1.59570        0.38760
H          4.51450       -0.65230        0.30300
H          3.58890       -0.10890       -1.13200
H          2.80360       -2.17190       -0.28090
H          2.43720       -1.58260        1.33070
H          0.42760       -1.95580       -0.03320
H          1.05800       -1.01100       -1.37140
H         -0.98550        0.08090       -1.38830
H         -1.38120        1.40730       -0.24920
H         -1.81930       -1.58260        0.03200
H         -2.06050       -0.42530        1.43830
H         -3.35470       -0.51110       -1.35590
H         -4.17770       -0.99370        0.22180
H         -3.27510        1.66040        0.58560
H         -3.88860        1.69830       -1.08580
H         -4.98670        1.13600        0.24680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_204_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

