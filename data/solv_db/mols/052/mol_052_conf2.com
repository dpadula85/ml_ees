%nproc=16
%mem=2GB
%chk=mol_052_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.17960       -0.10970       -0.00460
C          0.69620       -0.06040       -0.00720
C         -0.05330       -1.20860       -0.13620
C         -1.43940       -1.16870       -0.13920
C         -2.12640        0.02060       -0.01320
C         -1.37890        1.17280        0.11630
C          0.00710        1.12880        0.11890
Cl         0.97290        2.58660        0.28360
H          2.57390       -1.07740        0.32860
H          2.62480        0.12080       -0.98700
H          2.60340        0.65160        0.71570
H          0.46300       -2.14960       -0.23620
H         -2.02660       -2.06670       -0.24000
H         -3.21180        0.02910       -0.01780
H         -1.88440        2.13100        0.21820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

