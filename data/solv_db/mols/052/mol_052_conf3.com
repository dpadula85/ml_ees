%nproc=16
%mem=2GB
%chk=mol_052_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19260       -0.08880       -0.11550
C          0.70370       -0.05890       -0.04120
C         -0.04610       -1.21450        0.03090
C         -1.42530       -1.17840        0.09950
C         -2.11370        0.01750        0.09890
C         -1.36780        1.17490        0.02700
C          0.02060        1.14010       -0.04220
Cl         0.91830        2.65210       -0.13230
H          2.55870       -0.56090       -1.04690
H          2.59350        0.91430        0.03420
H          2.60130       -0.72690        0.72070
H          0.49480       -2.14340        0.03090
H         -2.02190       -2.08080        0.15630
H         -3.19640        0.03310        0.15320
H         -1.91230        2.12070        0.02650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

