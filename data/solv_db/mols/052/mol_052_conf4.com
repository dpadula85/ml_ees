%nproc=16
%mem=2GB
%chk=mol_052_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.14780       -0.25310       -0.18310
C          0.66350       -0.12280       -0.14360
C         -0.10760       -1.25120       -0.22610
C         -1.48180       -1.07890       -0.18530
C         -2.07170        0.16770       -0.06690
C         -1.27390        1.28680        0.01430
C          0.09340        1.14450       -0.02380
Cl         1.17660        2.53310        0.07420
H          2.37850       -1.15200       -0.78440
H          2.56530        0.64200       -0.69720
H          2.57830       -0.28900        0.83930
H          0.32390       -2.24920       -0.31990
H         -2.11510       -1.94750       -0.24830
H         -3.16170        0.28110       -0.03660
H         -1.71540        2.28840        0.10910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

