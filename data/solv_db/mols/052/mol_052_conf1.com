%nproc=16
%mem=2GB
%chk=mol_052_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.17600       -0.06680       -0.03370
C          0.69690       -0.05590       -0.01200
C         -0.03540       -1.22130        0.05740
C         -1.42040       -1.20380        0.07730
C         -2.12110       -0.01640        0.02840
C         -1.38170        1.14650       -0.04090
C          0.00590        1.13320       -0.06120
Cl         0.93070        2.62840       -0.14990
H          2.59990       -0.24980       -1.03490
H          2.61620       -0.78570        0.68820
H          2.56830        0.93940        0.28600
H          0.49240       -2.17220        0.09710
H         -2.01040       -2.12660        0.13240
H         -3.20380       -0.03050        0.04550
H         -1.91330        2.08150       -0.07980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_052_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

