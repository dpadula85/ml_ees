%nproc=16
%mem=2GB
%chk=mol_344_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.70840        0.03730        0.05980
C          1.36630       -0.54230        1.12260
C          2.75290       -0.45870        1.29130
C          3.49550        0.23050        0.35900
C          2.85630        0.81610       -0.70920
C          1.46960        0.72190       -0.86110
Cl         0.67970        1.47320       -2.22580
Cl         3.79210        1.69660       -1.90260
Cl         5.22990        0.38190        0.49460
Cl         3.52680       -1.21880        2.66590
Cl         0.38250       -1.41710        2.29740
C         -0.75490       -0.06910       -0.08920
C         -1.31970       -1.11830       -0.78950
C         -2.68940       -1.18240       -0.90650
C         -3.47860       -0.20980       -0.33040
C         -2.89040        0.84000        0.37160
C         -1.52530        0.90770        0.49070
Cl        -0.79800        2.22420        1.37070
Cl        -3.94550        2.04170        1.07880
Cl        -5.22370       -0.30850       -0.49040
Cl        -3.36810       -2.52160       -1.79790
Cl        -0.26660       -2.32440       -1.49990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_344_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_344_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_344_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

