%nproc=16
%mem=2GB
%chk=mol_344_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.70460       -0.01990       -0.25450
C         -1.38380       -0.67230        0.75320
C         -2.77770       -0.69980        0.79420
C         -3.50770       -0.06460       -0.18800
C         -2.80400        0.59200       -1.20220
C         -1.43980        0.61920       -1.24300
Cl        -0.63300        1.46330       -2.54780
Cl        -3.70100        1.40520       -2.45980
Cl        -5.26700       -0.06530       -0.18860
Cl        -3.62160       -1.53730        2.08640
Cl        -0.50910       -1.48730        2.01320
C          0.75380        0.00980       -0.29870
C          1.39970       -1.00010       -0.98010
C          2.78500       -0.97990       -1.02730
C          3.47500        0.05200       -0.38970
C          2.80750        1.05910        0.29080
C          1.42690        1.03340        0.33430
Cl         0.58140        2.30500        1.19380
Cl         3.65800        2.35850        1.09510
Cl         5.22570        0.13110       -0.41470
Cl         3.71170       -2.21050       -1.86360
Cl         0.52470       -2.29150       -1.77720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_344_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_344_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_344_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

