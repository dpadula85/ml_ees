%nproc=16
%mem=2GB
%chk=mol_508_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.98070       -0.11730       -0.09090
C          0.67970       -0.73140        0.20120
C         -0.48860       -0.11570        0.07800
C         -0.45390        1.30070       -0.39210
C         -1.76890       -0.76710        0.38090
H          1.92400        0.71180       -0.82630
H          2.49430        0.22750        0.85020
H          2.63250       -0.90020       -0.54040
H          0.66840       -1.76490        0.54270
H          0.15280        1.93680        0.29030
H         -1.47790        1.73350       -0.45210
H         -0.05990        1.29870       -1.42280
H         -2.58860       -0.04960        0.42560
H         -1.70580       -1.27030        1.38240
H         -1.98880       -1.49260       -0.42660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

