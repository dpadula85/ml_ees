%nproc=16
%mem=2GB
%chk=mol_508_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.80960       -0.75760        0.01150
C          0.90300        0.30090       -0.46460
C         -0.35850        0.28790       -0.10580
C         -0.85590       -0.81620        0.79100
C         -1.30190        1.34540       -0.57090
H          1.64790       -0.94130        1.10420
H          2.84470       -0.42200       -0.11320
H          1.59580       -1.69560       -0.55860
H          1.25390        1.08540       -1.10110
H         -0.79300       -0.52570        1.85170
H         -1.92140       -0.96590        0.51310
H         -0.27560       -1.76300        0.58560
H         -2.19370        0.88890       -1.05660
H         -0.76260        2.04060       -1.22510
H         -1.59240        1.93820        0.33880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

