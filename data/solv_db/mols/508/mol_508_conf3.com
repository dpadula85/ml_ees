%nproc=16
%mem=2GB
%chk=mol_508_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.89360       -0.60500        0.12470
C          0.88180        0.44730       -0.13760
C         -0.42780        0.27190       -0.05340
C         -1.34770        1.40760       -0.34120
C         -0.92490       -1.07120        0.33050
H          2.10090       -1.10600       -0.84750
H          1.51590       -1.39720        0.80740
H          2.84530       -0.16440        0.48220
H          1.25950        1.41920       -0.41560
H         -2.14160        1.44220        0.43530
H         -1.76340        1.33740       -1.36090
H         -0.81160        2.37160       -0.27650
H         -0.47230       -1.40970        1.28460
H         -2.01080       -1.13630        0.41000
H         -0.59670       -1.80740       -0.44200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

