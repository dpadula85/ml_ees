%nproc=16
%mem=2GB
%chk=mol_508_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.89590       -0.33280        0.35600
C          0.72490        0.58680        0.32850
C         -0.47820        0.18710       -0.06060
C         -1.56690        1.20060       -0.04350
C         -0.65060       -1.22490       -0.48010
H          1.72300       -1.25350       -0.25230
H          2.74530        0.21670       -0.08770
H          2.13800       -0.55910        1.40740
H          0.92970        1.60850        0.65070
H         -2.55720        0.77310       -0.26410
H         -1.26150        2.00450       -0.74290
H         -1.60080        1.63930        0.97760
H         -0.04210       -1.41440       -1.39190
H         -1.69560       -1.48520       -0.69600
H         -0.30380       -1.94690        0.29890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

