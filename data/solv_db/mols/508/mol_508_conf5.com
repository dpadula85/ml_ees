%nproc=16
%mem=2GB
%chk=mol_508_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93670        0.38020       -0.10410
C          0.81670       -0.59490        0.03470
C         -0.45900       -0.19120        0.04140
C         -0.77400        1.24260       -0.09020
C         -1.51110       -1.23920        0.18490
H          2.25800        0.33680       -1.16950
H          2.83220        0.04540        0.48350
H          1.67190        1.39080        0.22430
H          1.09560       -1.63700        0.12850
H         -1.86390        1.36510        0.10390
H         -0.26010        1.88450        0.65420
H         -0.55470        1.65880       -1.09500
H         -1.25890       -2.05630       -0.55220
H         -1.41250       -1.72180        1.18110
H         -2.51680       -0.86370       -0.02550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_508_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

