%nproc=16
%mem=2GB
%chk=mol_025_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76950       -0.53900        0.04980
C          0.46750       -1.12720        0.03720
C          1.61870       -0.35890       -0.03550
C          1.57800        1.01740       -0.09800
C          0.33630        1.61190       -0.08560
C         -0.81380        0.85450       -0.01340
Cl        -2.35870        1.66420       -0.00130
Cl         0.29110        3.36820       -0.16540
Cl         3.06380        1.94310       -0.18840
Cl         3.17570       -1.14660       -0.04920
Cl         0.53630       -2.87260        0.11600
N         -1.97890       -1.26440        0.12230
O         -3.20170       -0.64360        0.13280
O         -1.94500       -2.50720        0.17860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

