%nproc=16
%mem=2GB
%chk=mol_025_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.91460       -0.18090       -0.01370
C          0.44600        1.11270        0.08140
C         -0.91470        1.39880        0.05420
C         -1.84850        0.38940       -0.07050
C         -1.39000       -0.90520       -0.16850
C         -0.02910       -1.18080       -0.13970
Cl         0.56440       -2.82520       -0.25130
Cl        -2.56350       -2.19480       -0.32460
Cl        -3.57840        0.75360       -0.07270
Cl        -1.42130        3.04370        0.19730
Cl         1.65410        2.38370        0.25760
N          2.30970       -0.48700        0.07620
O          2.78590       -1.02380        1.27440
O          3.07080       -0.28420       -0.90020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

