%nproc=16
%mem=2GB
%chk=mol_025_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.86830       -0.24700        0.01990
C          0.10550       -1.22310        0.06560
C          1.41920       -0.80210        0.03700
C          1.76840        0.52840       -0.03340
C          0.79590        1.50230       -0.07720
C         -0.51770        1.09010       -0.04950
Cl        -1.75930        2.32010       -0.09980
Cl         1.24180        3.20860       -0.15430
Cl         3.44540        1.03080       -0.07330
Cl         2.65800       -2.02760        0.10040
Cl        -0.30150       -2.93850        0.17020
N         -2.22390       -0.67810        0.04560
O         -3.04740       -0.64880       -1.05470
O         -2.71600       -1.11510        1.10350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

