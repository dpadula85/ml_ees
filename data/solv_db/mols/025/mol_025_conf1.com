%nproc=16
%mem=2GB
%chk=mol_025_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.87090       -0.20180       -0.01160
C          0.06360       -1.21700        0.04370
C          1.38530       -0.88790        0.06060
C          1.77670        0.43070        0.02310
C          0.86940        1.47410       -0.03270
C         -0.46530        1.12920       -0.04930
Cl        -1.66400        2.41200       -0.11970
Cl         1.40370        3.14420       -0.07940
Cl         3.46640        0.81350        0.04600
Cl         2.55300       -2.20520        0.13140
Cl        -0.48050       -2.90090        0.09070
N         -2.24960       -0.52280       -0.03000
O         -2.66390       -1.82840        0.00670
O         -3.12390        0.36040       -0.07950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_025_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

