%nproc=16
%mem=2GB
%chk=mol_536_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06740       -0.42800        0.62820
C         -0.43690        0.73850       -0.06040
C          0.75190        0.20160       -0.85350
C          1.65310       -0.41640        0.13220
N          2.35470       -0.89440        0.92100
H         -0.29130       -1.18980        0.82830
H         -1.53770       -0.17060        1.59620
H         -1.79840       -0.87970       -0.09240
H         -0.02910        1.49610        0.63430
H         -1.15180        1.17820       -0.78760
H          1.22730        0.98260       -1.44580
H          0.32550       -0.61790       -1.50050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

