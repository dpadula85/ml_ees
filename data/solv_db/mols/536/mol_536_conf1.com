%nproc=16
%mem=2GB
%chk=mol_536_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24540        0.55600       -0.01230
C         -0.37300       -0.60240       -0.47000
C          0.91440       -0.56390        0.36060
C          1.59850        0.69970        0.14630
N          2.15100        1.70000       -0.03970
H         -2.24900        0.49680       -0.47930
H         -1.37050        0.42480        1.09520
H         -0.74120        1.52420       -0.16940
H         -0.12060       -0.51180       -1.54710
H         -0.88770       -1.54370       -0.23710
H          0.72980       -0.81530        1.40170
H          1.59360       -1.36450       -0.04910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

