%nproc=16
%mem=2GB
%chk=mol_536_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.25410        0.54250        0.25790
C         -0.43750       -0.53190       -0.34370
C          0.98740       -0.59510        0.14320
C          1.72190        0.64140       -0.13180
N          2.30150        1.63630       -0.33430
H         -1.90980        0.13410        1.06370
H         -0.69420        1.41300        0.64170
H         -1.96930        0.92330       -0.52310
H         -0.88200       -1.53210       -0.18440
H         -0.37860       -0.37340       -1.45680
H          1.45670       -1.47220       -0.36200
H          1.05810       -0.78590        1.22960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

