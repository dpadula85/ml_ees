%nproc=16
%mem=2GB
%chk=mol_536_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23180       -0.45090       -0.31690
C         -0.37360        0.15030        0.74950
C          0.88180        0.70930        0.08300
C          1.61980       -0.35110       -0.60200
N          2.22670       -1.20140       -1.14300
H         -0.81310       -1.44550       -0.61460
H         -2.26710       -0.54260        0.06590
H         -1.23760        0.20250       -1.19180
H         -0.05310       -0.66300        1.43800
H         -0.85610        0.91870        1.35260
H          1.50710        1.20330        0.84800
H          0.59700        1.47030       -0.66880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

