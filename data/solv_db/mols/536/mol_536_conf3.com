%nproc=16
%mem=2GB
%chk=mol_536_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31460        0.24800        0.27150
C         -0.28010       -0.54320       -0.51760
C          1.08400       -0.45650        0.09950
C          1.49080        0.93870        0.13130
N          1.80110        2.05920        0.15590
H         -1.41430       -0.09610        1.30180
H         -1.07100        1.31270        0.24890
H         -2.29190        0.09810       -0.24100
H         -0.64570       -1.58280       -0.61980
H         -0.21760       -0.04890       -1.51550
H          1.10020       -0.82550        1.15300
H          1.75920       -1.10370       -0.46800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_536_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

