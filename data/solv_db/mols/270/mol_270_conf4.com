%nproc=16
%mem=2GB
%chk=mol_270_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.35450        0.57320        0.16750
C         -2.87290        0.37610        0.39900
C         -2.43080       -1.01730        0.02600
C         -0.97040       -1.17780        0.26540
C         -0.12300       -0.22130       -0.52960
C          1.34940       -0.44960       -0.22910
C          2.12070        0.55250       -1.07030
C          3.59600        0.34440       -0.78760
C          3.82720        0.57810        0.68380
H         -4.92050        0.14920        1.03590
H         -4.52560        1.68650        0.24180
H         -4.70310        0.19770       -0.79460
H         -2.70630        0.48390        1.49150
H         -2.34490        1.12930       -0.20540
H         -2.99710       -1.74700        0.63380
H         -2.69540       -1.16990       -1.03530
H         -0.66800       -2.20950       -0.02840
H         -0.72960       -1.07590        1.33300
H         -0.32900        0.83470       -0.30710
H         -0.34340       -0.37740       -1.60390
H          1.65490       -1.47980       -0.44540
H          1.58870       -0.21820        0.82540
H          1.95120        0.41660       -2.15350
H          1.83140        1.59270       -0.82250
H          4.21720        1.02210       -1.41760
H          3.90170       -0.68570       -1.05740
H          3.18450        1.43810        0.98480
H          3.58840       -0.35340        1.22410
H          4.90310        0.80780        0.81310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

