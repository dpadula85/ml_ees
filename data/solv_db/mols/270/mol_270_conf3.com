%nproc=16
%mem=2GB
%chk=mol_270_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.34560        0.62360       -0.14520
C          2.82730        0.52180       -0.10210
C          2.37930       -0.72750        0.59370
C          0.86820       -0.76460        0.61720
C          0.27640       -0.76120       -0.75170
C         -1.20960       -0.82350       -0.69430
C         -1.87030        0.32290        0.03220
C         -3.36310        0.05550       -0.04790
C         -4.17830        1.10090        0.62620
H          4.76070        0.12610       -1.02950
H          4.78930        0.19450        0.78930
H          4.59070        1.70160       -0.19500
H          2.53220        0.43120       -1.18840
H          2.41330        1.40680        0.36980
H          2.71880       -1.64490        0.06660
H          2.74930       -0.81080        1.62710
H          0.49700       -1.64950        1.20670
H          0.46530        0.11390        1.19750
H          0.55320        0.17960       -1.31470
H          0.64350       -1.64580       -1.31720
H         -1.65870       -0.88560       -1.72790
H         -1.58140       -1.75800       -0.18140
H         -1.57920        1.25120       -0.45510
H         -1.51800        0.30510        1.09070
H         -3.61430       -0.03600       -1.12130
H         -3.56380       -0.93400        0.41150
H         -4.76050        1.67840       -0.12850
H         -4.93300        0.62650        1.28680
H         -3.58000        1.80170        1.24530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

