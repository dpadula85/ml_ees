%nproc=16
%mem=2GB
%chk=mol_270_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.14590        1.21410        0.21620
C          3.15050       -0.26880        0.36730
C          2.53740       -0.96730       -0.83520
C          1.11040       -0.53430       -1.02490
C          0.23910       -0.85270        0.17260
C         -1.13570       -0.35760       -0.18610
C         -2.15700       -0.58720        0.89410
C         -3.47850       -0.02420        0.35270
C         -3.38910        1.42520        0.04230
H          2.45210        1.73130        0.93130
H          2.83440        1.52970       -0.80110
H          4.19980        1.58260        0.35780
H          2.64820       -0.55460        1.29350
H          4.19640       -0.67020        0.44140
H          2.66190       -2.04710       -0.78250
H          3.10410       -0.60980       -1.71960
H          0.67480       -1.06880       -1.88570
H          1.07580        0.56030       -1.16040
H          0.26320       -1.95130        0.34050
H          0.62690       -0.37070        1.09650
H         -1.05910        0.72370       -0.38900
H         -1.47010       -0.92720       -1.08240
H         -2.28400       -1.64240        1.15830
H         -1.89460        0.05490        1.76480
H         -3.76860       -0.56200       -0.57960
H         -4.26340       -0.22590        1.09330
H         -4.31020        1.94010        0.41520
H         -2.48500        1.83210        0.55980
H         -3.22560        1.62820       -1.05120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

