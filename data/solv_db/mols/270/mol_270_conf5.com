%nproc=16
%mem=2GB
%chk=mol_270_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.33610        0.94200       -1.29990
C         -2.65050       -0.09390       -0.45900
C         -1.92360        0.62040        0.66050
C         -1.19640       -0.33590        1.57030
C         -0.15890       -1.16610        0.86640
C          0.86360       -0.23740        0.25870
C          1.93980       -1.02290       -0.46130
C          2.91870       -0.00960       -1.03750
C          3.48340        0.78170        0.13660
H         -4.43250        0.98620       -1.13340
H         -3.16400        0.66640       -2.36860
H         -2.92010        1.95540       -1.16770
H         -3.33110       -0.82490       -0.00810
H         -1.91070       -0.67140       -1.05600
H         -2.69650        1.11400        1.32350
H         -1.23310        1.39620        0.33500
H         -1.94400       -1.01380        2.07210
H         -0.75720        0.26620        2.38920
H          0.36790       -1.85090        1.54180
H         -0.58370       -1.78330        0.05550
H          0.36960        0.45140       -0.45570
H          1.33730        0.31840        1.09400
H          1.43140       -1.53480       -1.31770
H          2.42380       -1.72730        0.21080
H          3.73240       -0.58400       -1.51820
H          2.39850        0.63340       -1.75160
H          2.86450        1.64560        0.37800
H          4.54390        1.03610       -0.10630
H          3.56340        0.04280        0.98650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

