%nproc=16
%mem=2GB
%chk=mol_270_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.59710       -0.12470        0.22980
C          3.25690        0.33470       -0.26160
C          2.18810       -0.62920        0.25390
C          0.86430       -0.10880       -0.27700
C         -0.21300       -1.04400        0.21770
C         -1.53430       -0.58730       -0.29350
C         -1.83300        0.79350        0.19890
C         -3.22220        1.15400       -0.37190
C         -4.20650        0.12510        0.16380
H          4.51390       -1.01020        0.88880
H          5.30880       -0.33820       -0.58720
H          5.04410        0.68380        0.84700
H          3.25840        0.37830       -1.36270
H          2.97140        1.33500        0.12010
H          2.31700       -1.64730       -0.16770
H          2.16070       -0.64670        1.35090
H          0.89240       -0.12850       -1.38020
H          0.74740        0.93120        0.13570
H         -0.18000       -1.15280        1.32010
H          0.07120       -2.06200       -0.17930
H         -2.33710       -1.31700        0.02130
H         -1.59380       -0.63520       -1.42100
H         -1.10830        1.54840       -0.12140
H         -1.88910        0.74970        1.30540
H         -3.20050        1.09760       -1.47330
H         -3.47740        2.18340       -0.06360
H         -4.36520       -0.70350       -0.56600
H         -3.84300       -0.33450        1.09450
H         -5.18820        0.63360        0.37860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_270_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

