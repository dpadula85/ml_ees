%nproc=16
%mem=2GB
%chk=mol_207_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.90070       -0.13230       -1.00600
C         -2.32030        1.23950       -0.70900
C         -1.64630        1.24770        0.63390
C         -0.54630        0.25540        0.65850
C         -0.82870       -1.02590        1.06740
C          0.17110       -1.98840        1.09890
C          1.45390       -1.63390        0.71220
C          1.76400       -0.35650        0.29840
C          0.75970        0.57310        0.27660
O          3.06190       -0.08500       -0.06920
C          3.55410        1.13870       -0.50980
O          2.46080       -2.58910        0.74000
H         -3.48280       -0.49970       -0.14000
H         -3.58960       -0.08510       -1.88980
H         -2.10710       -0.83120       -1.27500
H         -1.57890        1.54200       -1.46310
H         -3.19820        1.94160       -0.67130
H         -2.37050        1.02500        1.45350
H         -1.21020        2.23310        0.87960
H         -1.83460       -1.33520        1.38500
H         -0.04630       -3.01020        1.42310
H          0.95380        1.57020       -0.03540
H          3.49360        1.94870        0.23370
H          4.64450        0.99370       -0.74180
H          3.06050        1.40570       -1.46660
H          2.28240       -3.54180        1.03650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

