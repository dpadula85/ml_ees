%nproc=16
%mem=2GB
%chk=mol_207_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.61690        1.30600        0.64880
C          2.29140        0.59490        0.82680
C          1.70260        0.31420       -0.54380
C          0.41090       -0.37660       -0.45510
C          0.38330       -1.77340       -0.43990
C         -0.80430       -2.47000       -0.35690
C         -1.97620       -1.74520       -0.28860
C         -1.99560       -0.35380       -0.30070
C         -0.79330        0.30130       -0.38430
O         -3.21980        0.30900       -0.22840
C         -3.20250        1.71290       -0.24300
O         -3.17780       -2.40290       -0.20500
H          3.99170        1.07750       -0.36370
H          4.31000        0.88230        1.43330
H          3.49440        2.40690        0.80130
H          1.62690        1.29060        1.39820
H          2.43680       -0.37170        1.31860
H          1.64890        1.27270       -1.10140
H          2.41800       -0.36610       -1.06850
H          1.29870       -2.36310       -0.49330
H         -0.84050       -3.55200       -0.34400
H         -0.77370        1.38090       -0.39620
H         -4.24410        2.12650       -0.32520
H         -2.79790        2.15370        0.70310
H         -2.57090        2.05280       -1.09120
H         -3.23380       -3.40760       -0.19160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

