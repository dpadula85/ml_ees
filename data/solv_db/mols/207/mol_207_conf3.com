%nproc=16
%mem=2GB
%chk=mol_207_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.76190        1.15540        0.02540
C          2.44770        0.54530       -0.41140
C          1.66670        0.18670        0.82480
C          0.35890       -0.42010        0.53690
C          0.23520       -1.78840        0.40230
C         -0.96760       -2.39950        0.13350
C         -2.10560       -1.65370       -0.01230
C         -2.02520       -0.28110        0.11480
C         -0.79510        0.32270        0.38790
O         -3.15600        0.47300       -0.02900
C         -3.17130        1.88090        0.08350
O         -3.34260       -2.26500       -0.28730
H          3.65580        2.20390        0.34490
H          4.46150        1.05510       -0.83130
H          4.16610        0.50430        0.83330
H          2.70490       -0.35430       -1.02310
H          1.87560        1.21080       -1.09170
H          2.29640       -0.50170        1.42410
H          1.54800        1.11190        1.42010
H          1.09490       -2.41700        0.50710
H         -1.06910       -3.48460        0.02620
H         -0.75770        1.38760        0.48230
H         -4.11080        2.30400       -0.32740
H         -3.10020        2.13670        1.16000
H         -2.30670        2.35870       -0.42170
H         -3.36570       -3.27170       -0.37400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

