%nproc=16
%mem=2GB
%chk=mol_207_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.67260        1.23160        0.61950
C         -2.37080        0.48930        0.69310
C         -1.73240        0.35730       -0.67130
C         -0.45180       -0.37260       -0.60070
C         -0.40650       -1.74390       -0.73560
C          0.79640       -2.40890       -0.66730
C          1.97570       -1.68840       -0.46030
C          1.96040       -0.31460       -0.32170
C          0.73550        0.33490       -0.39410
O          3.15350        0.32880       -0.12090
C          3.34330        1.69740        0.04030
O          3.20010       -2.36470       -0.39080
H         -3.54040        2.33580        0.62510
H         -4.20470        0.92550       -0.32650
H         -4.31880        0.88330        1.45220
H         -2.56600       -0.50200        1.15230
H         -1.68440        1.09260        1.33030
H         -1.50050        1.38160       -1.03710
H         -2.43730       -0.07840       -1.41240
H         -1.30900       -2.31710       -0.89620
H          0.84210       -3.49600       -0.77320
H          0.73970        1.41180       -0.28370
H          2.45710        2.18810        0.49750
H          3.54500        2.17430       -0.93960
H          4.23510        1.83010        0.71490
H          3.21140       -3.37600       -0.49270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

