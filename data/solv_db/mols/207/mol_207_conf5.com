%nproc=16
%mem=2GB
%chk=mol_207_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.58920        1.31880       -0.83400
C         -2.34630        0.47100       -0.71650
C         -1.52560        0.84720        0.51960
C         -0.33500       -0.04410        0.53440
C         -0.39890       -1.24840        1.18640
C          0.68080       -2.11000        1.22750
C          1.83230       -1.73800        0.59850
C          1.93190       -0.53180       -0.06790
C          0.85100        0.30140       -0.09390
O          3.11360       -0.18970       -0.69270
C          3.24510        1.03890       -1.38270
O          2.92960       -2.55530        0.61000
H         -4.45560        0.78780       -0.39110
H         -3.42440        2.21690       -0.20110
H         -3.83130        1.57630       -1.88180
H         -1.66090        0.60900       -1.57790
H         -2.67800       -0.58130       -0.61250
H         -2.15960        0.70740        1.41120
H         -1.24610        1.91620        0.42470
H         -1.30000       -1.59160        1.70580
H          0.60460       -3.06510        1.75420
H          0.92450        1.25280       -0.61780
H          4.28390        1.06630       -1.80830
H          2.56820        1.07970       -2.27230
H          3.09500        1.90930       -0.71400
H          2.89030       -3.44350        1.08790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_207_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

