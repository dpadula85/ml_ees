%nproc=16
%mem=2GB
%chk=mol_229_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.98860        0.50570        0.47490
C          2.56810        0.63410        0.03350
C          1.84310       -0.71370       -0.06320
C          0.46770       -0.40250       -0.48670
O          0.09880       -0.62770       -1.65270
O         -0.40100        0.15010        0.41790
C         -1.71830        0.52110        0.22520
C         -2.59380       -0.67220       -0.13050
C         -4.03600       -0.21530       -0.32650
H          4.17050       -0.39460        1.11130
H          4.67350        0.58200       -0.39150
H          4.21400        1.41450        1.10810
H          2.49560        1.13540       -0.94380
H          1.95490        1.21970        0.77670
H          1.88050       -1.21670        0.90580
H          2.39800       -1.34910       -0.80750
H         -2.14280        0.98850        1.15160
H         -1.84980        1.29780       -0.58580
H         -2.22630       -1.10660       -1.05710
H         -2.56660       -1.38920        0.73650
H         -4.03710        0.67890       -0.97040
H         -4.54990       -0.02530        0.65140
H         -4.63170       -1.01480       -0.81150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

