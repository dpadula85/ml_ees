%nproc=16
%mem=2GB
%chk=mol_229_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.30320        0.19800        0.26280
C         -2.91530        0.16790        0.83110
C         -1.85510        0.05960       -0.24000
C         -0.50670        0.03400        0.37670
O         -0.42600        0.10110        1.62860
O          0.64860       -0.06100       -0.38690
C          1.87960       -0.07740        0.29900
C          3.05960       -0.18240       -0.62330
C          4.30940       -0.18840        0.26120
H         -4.39140       -0.42540       -0.66090
H         -4.64110        1.24840        0.04660
H         -5.05820       -0.20780        0.96870
H         -2.85200       -0.73730        1.50320
H         -2.78690        1.03610        1.47590
H         -2.00290       -0.83280       -0.86670
H         -1.96440        0.94300       -0.90740
H          1.95310        0.89770        0.83710
H          1.89760       -0.94460        0.98400
H          3.05390       -1.15610       -1.15120
H          3.13480        0.64540       -1.34000
H          4.48860        0.86700        0.56220
H          5.17020       -0.64480       -0.24340
H          4.10780       -0.74010        1.20980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

