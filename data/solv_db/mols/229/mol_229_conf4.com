%nproc=16
%mem=2GB
%chk=mol_229_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.11140       -0.21110        0.59190
C         -2.47170        1.14300        0.37530
C         -1.47920        1.06690       -0.76930
C         -0.45560        0.06380       -0.39000
O         -0.48570       -0.56240        0.70480
O          0.62060       -0.27370       -1.20100
C          1.60570       -1.20380       -0.89530
C          2.40520       -0.91930        0.33570
C          3.13360        0.40390        0.23830
H         -2.86940       -0.91040       -0.23730
H         -2.79690       -0.65800        1.56450
H         -4.22570       -0.10870        0.67090
H         -3.27530        1.84160        0.05250
H         -1.99620        1.49940        1.31230
H         -1.07680        2.04400       -1.02740
H         -2.07560        0.72330       -1.66260
H          1.24580       -2.25150       -0.86510
H          2.33800       -1.17900       -1.75660
H          1.87080       -0.99650        1.27630
H          3.21670       -1.70440        0.37170
H          2.51620        1.18170        0.73170
H          4.12010        0.35130        0.73380
H          3.24690        0.66010       -0.83190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

