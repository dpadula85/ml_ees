%nproc=16
%mem=2GB
%chk=mol_229_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.60970       -0.75550        0.24100
C          2.08170       -0.66880        0.27120
C          1.69760        0.78250        0.45230
C          0.25580        1.03360        0.51460
O         -0.31030        1.38870        1.59710
O         -0.61810        0.89410       -0.56850
C         -1.98760        1.11080       -0.53950
C         -2.77490        0.29640        0.43770
C         -2.65650       -1.19810        0.21530
H          3.99060       -1.19090       -0.69720
H          3.98150       -1.36500        1.08020
H          4.06880        0.27440        0.30450
H          1.62530       -1.35960        0.99240
H          1.75060       -1.02270       -0.74960
H          2.24840        1.37380       -0.29830
H          2.13870        1.10440        1.43340
H         -2.28120        2.17190       -0.44520
H         -2.37630        0.81620       -1.56320
H         -3.87830        0.52740        0.29620
H         -2.60290        0.54320        1.50270
H         -2.70480       -1.77730        1.16250
H         -1.72230       -1.38690       -0.32810
H         -3.53520       -1.59240       -0.37860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

