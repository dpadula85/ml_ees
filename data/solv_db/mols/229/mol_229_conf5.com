%nproc=16
%mem=2GB
%chk=mol_229_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.21070       -0.19320        0.16860
C         -2.79010       -0.67020       -0.03640
C         -1.84030        0.39390        0.44070
C         -0.42550        0.00440        0.27460
O         -0.10980       -1.09890       -0.21390
O          0.65830        0.75270        0.61210
C          1.98310        0.49180        0.50260
C          2.62410        0.22330       -0.78670
C          4.10540       -0.02790       -0.55680
H         -4.38440        0.70040       -0.45530
H         -4.92230       -1.00630       -0.14060
H         -4.34850        0.08060        1.23220
H         -2.64110       -1.60980        0.53360
H         -2.64320       -0.93040       -1.10400
H         -2.10870        0.64880        1.47350
H         -1.99710        1.31230       -0.17470
H          2.21290       -0.38150        1.17810
H          2.53780        1.34280        1.00510
H          2.54650        0.95260       -1.57490
H          2.21530       -0.74740       -1.19750
H          4.38200        0.13550        0.52500
H          4.74120        0.67170       -1.12630
H          4.41490       -1.04520       -0.80190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_229_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

