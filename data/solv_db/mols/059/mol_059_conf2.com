%nproc=16
%mem=2GB
%chk=mol_059_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.56760        0.90290        0.42910
C         -1.71520       -0.28750        0.03550
C         -0.28020        0.16230       -0.09790
C          0.66300       -0.90680       -0.48010
C          2.06790       -0.40190       -0.60180
C          2.66670        0.15210        0.61140
O          2.21190        0.23230        1.71750
H         -1.92340        1.76270        0.63040
H         -3.31620        1.09750       -0.36050
H         -3.14320        0.70310        1.35900
H         -1.86520       -1.08710        0.75230
H         -2.05570       -0.66890       -0.97190
H         -0.26510        0.97190       -0.88500
H          0.03000        0.66130        0.83250
H          0.36350       -1.48190       -1.38010
H          0.68570       -1.68800        0.33760
H          2.74580       -1.14190       -1.08500
H          2.00860        0.46160       -1.33420
H          3.68840        0.55620        0.49120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

