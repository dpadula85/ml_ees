%nproc=16
%mem=2GB
%chk=mol_059_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.69710        0.54150       -0.54380
C         -1.60850       -0.13870        0.26280
C         -0.24880        0.02230       -0.38500
C          0.75450       -0.67550        0.47470
C          2.16160       -0.59290       -0.05820
C          2.64560        0.79300       -0.18460
O          3.75440        1.00970       -0.59830
H         -2.42840        1.58250       -0.82050
H         -3.03020       -0.07310       -1.39240
H         -3.58190        0.61510        0.14460
H         -1.59320        0.21650        1.31170
H         -1.89620       -1.22510        0.30120
H         -0.05350        1.11180       -0.44690
H         -0.33980       -0.43870       -1.40320
H          0.48730       -1.76690        0.48340
H          0.65960       -0.32640        1.50080
H          2.80580       -1.19920        0.63510
H          2.20690       -1.05500       -1.06440
H          2.00180        1.59920        0.09480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

