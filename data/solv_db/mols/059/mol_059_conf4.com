%nproc=16
%mem=2GB
%chk=mol_059_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.34950        0.74190       -0.28430
C         -1.81150       -0.64300       -0.56470
C         -0.46290       -0.85860        0.07640
C          0.58240        0.09830       -0.40260
C          1.88030       -0.21280        0.32840
C          2.95590        0.72850       -0.15310
O          3.55230        0.56290       -1.19670
H         -3.37420        0.78200       -0.75950
H         -2.52010        0.92470        0.79800
H         -1.77120        1.53940       -0.77840
H         -1.78120       -0.77970       -1.66100
H         -2.51570       -1.36820       -0.08570
H         -0.52670       -0.84190        1.18540
H         -0.15060       -1.89430       -0.20650
H          0.80170       -0.08120       -1.49150
H          0.30980        1.14040       -0.18000
H          1.74370       -0.17530        1.40940
H          2.23010       -1.25540        0.05810
H          3.20760        1.59230        0.46320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

