%nproc=16
%mem=2GB
%chk=mol_059_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.84110        0.38790       -0.53920
C         -1.63200       -0.50800       -0.36720
C         -0.46230        0.37720       -0.02940
C          0.76630       -0.51170        0.14750
C          1.90510        0.42680        0.48400
C          3.17780       -0.28320        0.68770
O          3.20610       -1.49510        0.57490
H         -3.27430        0.16500       -1.52490
H         -2.56900        1.47320       -0.49450
H         -3.51270        0.20190        0.31040
H         -1.76300       -1.22900        0.47730
H         -1.38160       -1.06260       -1.28250
H         -0.22690        1.04220       -0.88790
H         -0.60860        0.96080        0.90010
H          0.93540       -1.06830       -0.79830
H          0.61540       -1.26460        0.94780
H          1.58160        1.02510        1.35030
H          1.99080        1.13270       -0.39100
H          4.09300        0.22970        0.93460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

