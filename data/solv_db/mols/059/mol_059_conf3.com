%nproc=16
%mem=2GB
%chk=mol_059_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35180       -0.91980        0.29000
C         -1.27780        0.14450        0.24750
C         -0.32540       -0.19310       -0.86030
C          0.79680        0.80840       -0.99290
C          1.62800        0.90730        0.25790
C          2.25780       -0.39410        0.60960
O          3.24970       -0.82310        0.03230
H         -2.33150       -1.47630       -0.65130
H         -3.34980       -0.46330        0.50410
H         -2.15630       -1.58940        1.16740
H         -0.75670        0.14280        1.23490
H         -1.77140        1.13900        0.09520
H         -0.89220       -0.23560       -1.81370
H          0.05540       -1.21650       -0.68540
H          1.46900        0.45390       -1.80740
H          0.41660        1.80410       -1.27550
H          2.43760        1.62900        0.08010
H          1.04960        1.28720        1.12660
H          1.85250       -1.00490        1.39470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_059_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

