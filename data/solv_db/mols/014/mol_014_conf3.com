%nproc=16
%mem=2GB
%chk=mol_014_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24980       -0.23800        0.15770
C          0.00210        0.54610       -0.24050
C          1.25530       -0.27010        0.03970
Cl         0.06960        2.12240        0.55140
H         -2.11660        0.38380       -0.12100
H         -1.25080       -1.13680       -0.50940
H         -1.22360       -0.55840        1.19900
H         -0.09940        0.68610       -1.35070
H          0.92100       -1.32580        0.20670
H          1.79270        0.10130        0.93430
H          1.89940       -0.31070       -0.86710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

