%nproc=16
%mem=2GB
%chk=mol_014_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23640       -0.29130       -0.06550
C          0.00660        0.58180        0.15050
C          1.25140       -0.23870       -0.01120
Cl        -0.07400        1.97230       -0.91570
H         -2.12700        0.34430       -0.05750
H         -1.23840       -0.99490        0.81130
H         -1.16760       -0.84370       -1.02200
H         -0.11160        0.95690        1.20370
H          0.96150       -1.27070       -0.33490
H          1.96970        0.16570       -0.73320
H          1.76570       -0.38160        0.97450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

