%nproc=16
%mem=2GB
%chk=mol_014_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.20030       -0.26840        0.16400
C          0.00550        0.62030       -0.00240
C         -1.24290       -0.21190       -0.08740
Cl         0.23450        1.68260       -1.38900
H          1.43530       -0.39720        1.24950
H          1.00860       -1.30820       -0.20980
H          2.06010        0.10340       -0.41970
H         -0.08890        1.25360        0.91580
H         -1.52450       -0.48450       -1.12320
H         -2.06950        0.21680        0.50170
H         -1.01850       -1.20650        0.40040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

