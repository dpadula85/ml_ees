%nproc=16
%mem=2GB
%chk=mol_014_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.17210       -0.41000        0.31160
C          0.11200        0.45130       -0.33410
C         -1.26960       -0.05650       -0.03810
Cl         0.31180        2.16530        0.08260
H          1.37540       -1.26280       -0.35530
H          2.09670        0.15800        0.49230
H          0.79390       -0.80790        1.26140
H          0.26510        0.37790       -1.44290
H         -1.98720        0.78340       -0.12300
H         -1.53250       -0.82790       -0.81000
H         -1.33780       -0.57080        0.95550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

