%nproc=16
%mem=2GB
%chk=mol_014_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24550        0.02260       -0.33510
C         -0.02710        0.13630        0.57240
C          1.25000       -0.18530       -0.17440
Cl         0.05610        1.71220        1.37140
H         -1.41970        0.95170       -0.91550
H         -2.11070       -0.30240        0.25930
H         -1.00550       -0.81250       -1.04460
H         -0.17670       -0.63520        1.36920
H          0.97420       -0.57210       -1.18830
H          1.89140        0.71160       -0.21940
H          1.81360       -1.02690        0.30490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_014_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

