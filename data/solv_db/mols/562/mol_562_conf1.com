%nproc=16
%mem=2GB
%chk=mol_562_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.25050        2.09870        0.58660
C         -3.11610        1.15980        0.12650
C         -1.87750        1.59850        0.81510
N         -0.66970        0.83680        0.49700
C         -0.09200       -0.10990        1.37520
C         -0.30600       -1.55320        1.18760
C         -1.72910       -2.04500        1.29230
C          0.03530        1.28220       -0.64000
O         -0.58440        2.28570       -1.24870
S          1.55480        0.83200       -1.35650
C          2.64910       -0.28000       -0.57980
C          3.89290       -0.42590       -1.48780
C          4.86040       -1.38460       -0.84040
H         -5.04990        1.97850       -0.17540
H         -3.86160        3.12120        0.65630
H         -4.55910        1.71130        1.58970
H         -2.97400        1.25840       -0.97430
H         -3.49890        0.17210        0.30680
H         -1.72200        2.68410        0.53460
H         -2.01610        1.64120        1.93310
H         -0.41750        0.10130        2.46430
H          0.99800        0.08410        1.43560
H          0.22250       -2.05730        2.05800
H          0.20390       -2.01340        0.30970
H         -2.21850       -1.39880        2.07660
H         -2.28460       -1.96720        0.32880
H         -1.80650       -3.08500        1.62460
H          2.27840       -1.31290       -0.47810
H          3.06960        0.03090        0.41070
H          4.38850        0.55100       -1.62450
H          3.59320       -0.81500       -2.48230
H          4.51340       -2.43170       -1.02240
H          4.90020       -1.23630        0.26990
H          5.87390       -1.31130       -1.23160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_562_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_562_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_562_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

