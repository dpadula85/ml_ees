%nproc=16
%mem=2GB
%chk=mol_562_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.79530        2.03510       -0.77810
C         -2.21360        1.84400        0.59010
C         -1.89560        0.40250        0.92960
N         -0.94190       -0.20050        0.03930
C         -1.39780       -0.93370       -1.11210
C         -1.64170       -2.39020       -0.86880
C         -2.67110       -2.69770        0.16640
C          0.43970       -0.10650        0.30850
O          0.80550        0.51130        1.33610
S          1.69090       -0.82250       -0.74270
C          3.32570       -0.50400       -0.08240
C          3.67950        0.97050       -0.10950
C          5.06570        1.18380        0.45830
H         -2.04130        1.90540       -1.56760
H         -3.72410        1.43680       -0.93600
H         -3.14050        3.10730       -0.81320
H         -1.29610        2.47920        0.63860
H         -2.91550        2.29200        1.31410
H         -2.80600       -0.19380        1.08990
H         -1.40450        0.45100        1.94830
H         -0.58470       -0.85820       -1.88410
H         -2.32940       -0.47340       -1.48220
H         -0.71230       -2.93950       -0.59850
H         -1.98420       -2.83450       -1.82650
H         -2.28470       -2.52540        1.19750
H         -3.62620       -2.17410        0.03880
H         -2.91370       -3.80150        0.15490
H          4.11640       -1.05420       -0.62310
H          3.33220       -0.80760        0.99790
H          3.70530        1.36200       -1.14740
H          2.96720        1.56900        0.51360
H          5.72890        0.44050       -0.03340
H          5.09890        1.10850        1.55840
H          5.36440        2.21830        0.15630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_562_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_562_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_562_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

