%nproc=16
%mem=2GB
%chk=mol_234_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24930       -0.21890       -1.00020
C          1.43170        0.23540        0.17140
C          2.02350        0.89800        1.13250
C          0.02440        0.00870        0.15050
C         -0.54430       -0.80610       -0.83060
C         -1.89900       -0.99710       -0.91930
C         -2.71490       -0.36340       -0.01040
C         -2.19810        0.43960        0.95950
C         -0.81670        0.62190        1.03150
H          2.49580       -1.28820       -0.93790
H          1.63990       -0.05760       -1.93660
H          3.13080        0.44630       -1.07240
H          3.10570        1.13080        1.07970
H          1.45130        1.15810        2.00640
H          0.08430       -1.31790       -1.55710
H         -2.34330       -1.63910       -1.69320
H         -3.79180       -0.51830       -0.08310
H         -2.84660        0.94960        1.68960
H         -0.48210        1.31820        1.81970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

