%nproc=16
%mem=2GB
%chk=mol_234_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.30520        0.74500        0.67660
C         -1.45570       -0.19850       -0.09070
C         -2.00810       -1.08940       -0.85830
C         -0.01330       -0.11000        0.03130
C          0.56040        0.86400        0.86490
C          1.91800        0.99220        1.01940
C          2.76610        0.15320        0.34820
C          2.26400       -0.81520       -0.47820
C          0.88240       -0.92640       -0.61930
H         -3.33780        0.38760        0.68040
H         -1.98690        0.81660        1.74480
H         -2.27220        1.75930        0.21050
H         -1.45080       -1.79890       -1.43800
H         -3.10090       -1.18710       -0.97520
H         -0.10700        1.52320        1.39120
H          2.35140        1.76430        1.67950
H          3.84260        0.29530        0.50440
H          2.94900       -1.46360       -0.99580
H          0.50400       -1.71170       -1.28750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

