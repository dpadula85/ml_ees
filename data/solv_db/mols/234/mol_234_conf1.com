%nproc=16
%mem=2GB
%chk=mol_234_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.09330        0.08320       -1.38090
C         -1.42500       -0.39910       -0.13470
C         -2.10990       -1.11260        0.71430
C         -0.03290       -0.07440        0.12210
C          0.68800        0.69670       -0.80800
C          2.00830        1.03850       -0.62390
C          2.69660        0.63390        0.49980
C          2.01680       -0.12130        1.42650
C          0.67360       -0.46590        1.23370
H         -1.41920       -0.16650       -2.24750
H         -2.22870        1.17940       -1.38880
H         -3.05580       -0.44710       -1.52500
H         -3.16790       -1.37290        0.54380
H         -1.71110       -1.49810        1.62870
H          0.14780        1.01560       -1.69180
H          2.55090        1.62870       -1.34110
H          3.73360        0.88920        0.66730
H          2.54980       -0.44450        2.31390
H          0.17850       -1.06280        1.99160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

