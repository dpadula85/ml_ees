%nproc=16
%mem=2GB
%chk=mol_234_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25670        1.03960        0.32890
C         -1.47250       -0.21550        0.22590
C         -2.11420       -1.34980        0.15910
C         -0.02910       -0.14160        0.20350
C          0.82160       -1.21980        0.11240
C          2.19730       -1.12080        0.09290
C          2.78990        0.12270        0.16780
C          1.97700        1.21370        0.25910
C          0.60570        1.10280        0.27770
H         -1.92400        1.78620       -0.41200
H         -3.34690        0.85350        0.30120
H         -2.03510        1.45800        1.34820
H         -1.55290       -2.26050        0.08450
H         -3.20730       -1.40600        0.17620
H          0.42530       -2.22220        0.05040
H          2.81660       -2.02070        0.01850
H          3.88170        0.19540        0.15200
H          2.42930        2.19260        0.31840
H         -0.00570        1.99230        0.35120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

