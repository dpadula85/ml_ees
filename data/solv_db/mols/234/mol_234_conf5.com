%nproc=16
%mem=2GB
%chk=mol_234_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.03750       -0.49840       -1.32630
C         -1.43780       -0.35280        0.04340
C         -2.19430       -0.54530        1.08460
C         -0.02600        0.00000        0.12750
C          0.72960        0.19300       -1.01090
C          2.05860        0.52500       -0.96880
C          2.71460        0.68430        0.24370
C          1.98090        0.49680        1.39310
C          0.62580        0.15840        1.32890
H         -1.37770       -1.13700       -1.96790
H         -2.23210        0.46500       -1.79790
H         -2.99440       -1.07020       -1.24610
H         -1.85300       -0.46320        2.10450
H         -3.23820       -0.80600        0.97660
H          0.25980        0.07910       -1.99950
H          2.66370        0.67950       -1.85620
H          3.76840        0.94730        0.28930
H          2.49390        0.62160        2.33450
H          0.09570        0.02270        2.24760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_234_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

