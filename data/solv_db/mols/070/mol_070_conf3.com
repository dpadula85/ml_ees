%nproc=16
%mem=2GB
%chk=mol_070_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07420        0.84240        0.46980
C         -1.66700       -0.00210       -0.69660
C         -0.78740       -1.16410       -0.30320
C          0.48650       -0.73720        0.35230
C          1.33800        0.14070       -0.53430
C          2.59330        0.50980        0.22690
Cl         2.22160        1.38750        1.71730
H         -2.18240        0.18370        1.36330
H         -1.33330        1.63550        0.72690
H         -3.03980        1.36320        0.30180
H         -1.23590        0.58590       -1.53170
H         -2.61190       -0.45220       -1.11030
H         -0.56920       -1.83390       -1.15720
H         -1.35100       -1.77740        0.43200
H          0.27760       -0.26760        1.33320
H          1.07910       -1.65190        0.55930
H          0.81790        1.03410       -0.88810
H          1.65990       -0.46900       -1.40700
H          3.27200        1.11260       -0.39020
H          3.10600       -0.44020        0.53570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

