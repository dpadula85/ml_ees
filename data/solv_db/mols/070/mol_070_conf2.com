%nproc=16
%mem=2GB
%chk=mol_070_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.09200       -0.29700       -0.08940
C          1.88390        0.60660        0.04190
C          0.66080       -0.26350       -0.00720
C         -0.62440        0.52120        0.11440
C         -1.75800       -0.48090        0.04890
C         -3.10630        0.19160        0.16110
Cl        -4.38790       -1.02070        0.07900
H          3.75440       -0.21730        0.80030
H          3.69310       -0.06390       -0.99990
H          2.80690       -1.37180       -0.15480
H          1.94870        1.19520        0.96690
H          1.92110        1.32420       -0.81430
H          0.72730       -0.96680        0.85440
H          0.66830       -0.80960       -0.96160
H         -0.70130        1.09900        1.03400
H         -0.73270        1.15930       -0.80830
H         -1.73000       -1.04870       -0.89630
H         -1.66690       -1.23900        0.87060
H         -3.24530        0.87570       -0.70860
H         -3.20370        0.80650        1.06410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

