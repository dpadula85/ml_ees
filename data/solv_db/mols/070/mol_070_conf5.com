%nproc=16
%mem=2GB
%chk=mol_070_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.91170       -0.00840        0.07080
C         -1.44870       -0.01260        0.53520
C         -0.66290        0.66950       -0.54860
C          0.80930        0.74920       -0.24470
C          1.36710       -0.64100       -0.08180
C          2.84810       -0.62900        0.22620
Cl         3.80300        0.14290       -1.03990
H         -3.21760        1.00160       -0.27770
H         -3.58870       -0.31080        0.88970
H         -3.04010       -0.73510       -0.74600
H         -1.17420       -1.05020        0.71620
H         -1.37290        0.60360        1.46340
H         -1.11090        1.67320       -0.70010
H         -0.82900        0.07390       -1.48500
H          0.99380        1.40190        0.60570
H          1.29940        1.20430       -1.13630
H          0.86390       -1.10100        0.78640
H          1.15800       -1.21470       -1.00360
H          3.15030       -1.69580        0.38110
H          3.06390       -0.12170        1.18790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

