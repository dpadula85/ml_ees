%nproc=16
%mem=2GB
%chk=mol_070_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.11440       -0.06540       -0.33730
C         -1.84810       -0.12930        0.46800
C         -0.61800        0.09110       -0.38770
C          0.61860        0.01420        0.48290
C          1.87150        0.22410       -0.30370
C          3.05420        0.13180        0.63730
Cl         3.15670       -1.43520        1.44010
H         -3.95110       -0.34110        0.34180
H         -3.09720       -0.74860       -1.20090
H         -3.34220        0.96420       -0.69670
H         -1.89810        0.54380        1.32000
H         -1.77440       -1.16780        0.87120
H         -0.52780       -0.72830       -1.13070
H         -0.67930        1.06840       -0.92080
H          0.59860        0.75220        1.30610
H          0.65080       -1.01080        0.90320
H          1.99300       -0.59850       -1.04520
H          1.92770        1.22410       -0.78100
H          4.02210        0.28830        0.09840
H          2.95750        0.92270        1.41010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

