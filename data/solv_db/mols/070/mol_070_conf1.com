%nproc=16
%mem=2GB
%chk=mol_070_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.79940        1.13640        0.44020
C         -1.86570       -0.36090        0.44650
C         -0.95160       -1.07030       -0.48400
C          0.50260       -0.93540       -0.31470
C          1.12320        0.40690       -0.41430
C          2.65580        0.29900       -0.25340
Cl         3.40910        1.88040       -0.36860
H         -1.27970        1.55130       -0.45470
H         -2.85940        1.54830        0.43100
H         -1.28030        1.49990        1.35940
H         -2.94720       -0.64980        0.25270
H         -1.71640       -0.67630        1.51150
H         -1.26160       -0.89370       -1.56520
H         -1.18960       -2.18030       -0.35350
H          1.02730       -1.60060       -1.07990
H          0.81780       -1.41600        0.65270
H          0.88370        0.98550       -1.31370
H          0.82960        1.05020        0.46980
H          2.90070       -0.25640        0.67030
H          3.00110       -0.31840       -1.10800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_070_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

