%nproc=16
%mem=2GB
%chk=mol_543_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.46310        0.00850       -0.37260
C          1.13160       -0.51420        0.12140
N          0.02750        0.26250       -0.32990
C         -1.22520       -0.28930        0.01720
C         -2.40400        0.59220       -0.26510
H          2.62750        1.07870       -0.14750
H          2.59240       -0.19750       -1.47450
H          3.27020       -0.57840        0.13390
H          1.07940       -1.57240       -0.18370
H          1.20220       -0.52490        1.24630
H          0.13420        1.26310       -0.40720
H         -1.41650       -1.26000       -0.54030
H         -1.22730       -0.62030        1.08910
H         -2.70230        0.52330       -1.32290
H         -3.27660        0.18770        0.33550
H         -2.27610        1.64110        0.05750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

