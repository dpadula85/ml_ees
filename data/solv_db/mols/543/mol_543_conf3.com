%nproc=16
%mem=2GB
%chk=mol_543_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.07810        0.23500        0.48160
C          1.19940       -0.64370       -0.32850
N         -0.10970       -0.08300       -0.58650
C         -0.91550        0.01010        0.61430
C         -2.28190        0.51100        0.16270
H          2.62340        0.97660       -0.14640
H          1.60850        0.78330        1.28760
H          2.89640       -0.37410        0.95570
H          1.67670       -0.69300       -1.35810
H          1.19260       -1.68010        0.03150
H         -0.59720       -0.80990       -1.17500
H         -1.01280       -0.91460        1.17870
H         -0.55830        0.79790        1.31760
H         -2.57040       -0.03650       -0.73880
H         -3.07320        0.33890        0.91640
H         -2.15590        1.58180       -0.08610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

