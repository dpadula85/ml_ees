%nproc=16
%mem=2GB
%chk=mol_543_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30380        0.42430        0.19460
C          0.89940        0.38580       -0.38880
N          0.17150       -0.62120        0.25470
C         -1.16790       -0.85900       -0.08130
C         -2.13800        0.22830        0.30930
H          3.01720        0.89560       -0.48890
H          2.65410       -0.61880        0.43430
H          2.33050        0.98530        1.16460
H          0.45430        1.39510       -0.18650
H          0.93240        0.25520       -1.48380
H          0.57250       -1.06430        1.08050
H         -1.51260       -1.79740        0.43590
H         -1.25650       -1.10940       -1.17660
H         -1.61540        1.16480        0.55100
H         -2.78130       -0.06370        1.16860
H         -2.86390        0.39950       -0.53540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

