%nproc=16
%mem=2GB
%chk=mol_543_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26950        0.26730       -0.28620
C         -1.17500       -0.75550       -0.02510
N          0.06630       -0.37440       -0.63080
C          1.11790       -0.13620        0.30800
C          2.32560        0.45930       -0.38400
H         -2.68820        0.08630       -1.30780
H         -3.05660        0.18960        0.48500
H         -1.85000        1.28680       -0.28120
H         -1.52760       -1.69020       -0.52990
H         -1.12200       -0.89420        1.07210
H         -0.10490        0.37150       -1.31820
H          1.45310       -1.13080        0.69570
H          0.86080        0.54080        1.13150
H          2.96110       -0.34400       -0.83560
H          2.94780        0.95220        0.40550
H          2.06130        1.17150       -1.18240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

