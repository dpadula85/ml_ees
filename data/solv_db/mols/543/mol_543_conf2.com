%nproc=16
%mem=2GB
%chk=mol_543_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.10940        0.50650       -0.40690
C         -1.31560       -0.60990        0.19360
N          0.05010       -0.70630       -0.13660
C          0.90610        0.30810        0.34050
C          2.38310        0.06730        0.09510
H         -1.42060        1.32460       -0.67610
H         -2.73440        0.19140       -1.26230
H         -2.86730        0.83540        0.36210
H         -1.39750       -0.58630        1.32320
H         -1.79190       -1.57090       -0.14600
H          0.39300       -1.65090        0.03020
H          0.79110        0.56520        1.42900
H          0.66250        1.25460       -0.24200
H          3.01610        0.53030        0.91410
H          2.76600        0.53470       -0.84860
H          2.66870       -0.99390        0.06670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_543_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

