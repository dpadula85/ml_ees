%nproc=16
%mem=2GB
%chk=mol_098_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.39860       -0.16040        0.40940
C         -1.49330        0.31090       -0.71330
C         -0.07790        0.02770       -0.39060
C          0.60790       -1.02170       -1.01410
C          1.95130       -1.25010       -0.70280
C          2.58040       -0.43060        0.21950
C          1.92670        0.60140        0.84090
C          0.58160        0.80570        0.50990
O         -0.05170        1.84210        1.14670
H         -3.01020       -0.98220        0.04000
H         -1.76370       -0.59340        1.23420
H         -2.97440        0.66020        0.86990
H         -1.64070        1.40300       -0.83390
H         -1.79380       -0.20010       -1.67450
H          0.07210       -1.62820       -1.72180
H          2.47990       -2.04550       -1.17030
H          3.62070       -0.63550        0.43260
H          2.39930        1.25450        1.56090
H         -1.01530        2.04240        0.95740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

