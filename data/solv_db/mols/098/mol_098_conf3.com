%nproc=16
%mem=2GB
%chk=mol_098_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.40680        0.09280       -0.41970
C          1.45920        0.29600        0.73150
C          0.05430       -0.01670        0.37910
C         -0.53070       -1.19690        0.79500
C         -1.83870       -1.53030        0.47600
C         -2.55350       -0.62660       -0.28730
C         -1.97100        0.54630       -0.69940
C         -0.67530        0.87790       -0.38260
O         -0.13040        2.10030       -0.83060
H          2.99980       -0.82720       -0.23920
H          3.14890        0.91500       -0.52100
H          1.86080       -0.01190       -1.39080
H          1.77990       -0.34310        1.58890
H          1.57310        1.35780        1.05200
H          0.00970       -1.93210        1.40320
H         -2.29350       -2.44930        0.80280
H         -3.58780       -0.86350       -0.55840
H         -2.53480        1.25630       -1.29900
H          0.82320        2.35520       -0.60050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

