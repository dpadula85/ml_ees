%nproc=16
%mem=2GB
%chk=mol_098_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35300       -0.34760        0.62680
C         -1.58050        0.21210       -0.49920
C         -0.10830        0.05730       -0.38680
C          0.57280       -0.93370       -1.05490
C          1.92410       -1.08800       -0.88120
C          2.67770       -0.28770       -0.05010
C          1.99690        0.70400        0.61590
C          0.61400        0.87880        0.44950
O          0.01020        1.92250        1.15930
H         -2.55830        0.34380        1.46720
H         -3.37920       -0.69350        0.29790
H         -1.89990       -1.25470        1.08980
H         -1.91060       -0.30620       -1.44430
H         -1.78800        1.28690       -0.64190
H         -0.00760       -1.57860       -1.70850
H          2.44530       -1.88410       -1.41700
H          3.75550       -0.43490        0.07250
H          2.56550        1.34710        1.27150
H         -0.97650        2.05660        1.03360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

