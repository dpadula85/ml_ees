%nproc=16
%mem=2GB
%chk=mol_098_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43040        0.10870        0.56050
C          1.58380       -0.63260       -0.46090
C          0.13810       -0.40510       -0.21490
C         -0.65930       -1.34280        0.41160
C         -1.98640       -1.07900        0.66150
C         -2.56020        0.12250        0.29810
C         -1.74540        1.04210       -0.32740
C         -0.41530        0.80910       -0.59290
O          0.39070        1.74520       -1.22580
H          2.97600        0.90200        0.02380
H          1.74430        0.54970        1.34610
H          3.11720       -0.56570        1.11130
H          1.76610       -1.72180       -0.34160
H          1.87800       -0.38510       -1.50170
H         -0.20710       -2.29100        0.69870
H         -2.61070       -1.79980        1.15370
H         -3.61880        0.31520        0.51490
H         -2.17450        2.00330       -0.62710
H         -0.04650        2.62520       -1.48780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

