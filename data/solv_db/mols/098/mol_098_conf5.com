%nproc=16
%mem=2GB
%chk=mol_098_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.38290        0.09040       -0.16400
C         -1.39240        0.06470        0.98940
C         -0.03030       -0.08410        0.40960
C          0.57830       -1.32620        0.41830
C          1.81740       -1.48990       -0.16100
C          2.48760       -0.44610       -0.76000
C          1.86460        0.77900       -0.75600
C          0.61880        0.99070       -0.18450
O          0.04290        2.26060       -0.20940
H         -2.80390        1.09560       -0.32840
H         -3.25170       -0.56850        0.11720
H         -1.91000       -0.28600       -1.09420
H         -1.57660       -0.84220        1.61790
H         -1.40070        0.97820        1.59460
H          0.05420       -2.15340        0.88810
H          2.29880       -2.46190       -0.15820
H          3.47880       -0.58420       -1.22280
H          2.36870        1.61420       -1.21860
H         -0.86130        2.36900        0.22190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_098_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

