%nproc=16
%mem=2GB
%chk=mol_632_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.26080       -0.82590       -1.03990
C         -0.21200       -0.17060       -0.14190
C         -0.23520       -0.78430        1.21270
C         -0.69170        1.28040        0.01540
O          0.95790       -0.15790       -0.80130
C          2.05200        0.37740       -0.20750
H         -0.78960       -1.62860       -1.64270
H         -2.05640       -1.29870       -0.40050
H         -1.68720       -0.09720       -1.75630
H          0.73510       -1.16270        1.57190
H         -0.91380       -1.68300        1.19620
H         -0.66120       -0.11830        1.99260
H         -0.73000        1.74850       -0.99780
H         -0.04180        1.84960        0.69850
H         -1.69650        1.21660        0.47810
H          2.91100        0.29310       -0.90840
H          2.33440       -0.27680        0.66700
H          1.98580        1.43820        0.06400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

