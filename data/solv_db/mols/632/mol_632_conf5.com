%nproc=16
%mem=2GB
%chk=mol_632_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.68730        0.01510        0.66110
C          0.26420       -0.01450        0.08480
C          0.25920       -1.04430       -0.99320
C          0.06740        1.37480       -0.52280
O         -0.58210       -0.24870        1.13260
C         -1.91160       -0.31180        0.83150
H          1.71740        0.47470        1.64810
H          2.12140       -1.01610        0.66280
H          2.31930        0.59550       -0.06180
H          1.11320       -1.77210       -0.86570
H         -0.65860       -1.62210       -1.04990
H          0.40130       -0.53680       -1.98370
H         -0.90660        1.49850       -0.99170
H          0.26790        2.09360        0.30700
H          0.85700        1.51280       -1.26530
H         -2.45390       -0.50820        1.80230
H         -2.21920       -1.11700        0.15790
H         -2.34340        0.62680        0.44600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

