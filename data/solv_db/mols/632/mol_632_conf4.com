%nproc=16
%mem=2GB
%chk=mol_632_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.58110       -0.58290        1.13430
C         -0.21300       -0.05980       -0.23980
C         -1.23700       -0.57380       -1.22570
C         -0.32370        1.43620       -0.20110
O          1.01320       -0.54940       -0.57390
C          2.04690       -0.21820        0.25720
H         -0.16130       -1.62900        1.18880
H         -1.67860       -0.67350        1.26730
H         -0.17510        0.02710        1.94660
H         -0.77890       -1.31190       -1.93660
H         -2.07750       -1.08710       -0.75220
H         -1.65490        0.24930       -1.84660
H         -1.40780        1.68180       -0.18410
H          0.08220        1.91860       -1.10870
H          0.14740        1.88890        0.68830
H          2.24960        0.85080        0.36470
H          1.79840       -0.60050        1.28570
H          2.95110       -0.76670       -0.06410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

