%nproc=16
%mem=2GB
%chk=mol_632_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.52160        1.30140       -0.35440
C         -0.23870       -0.09790        0.13200
C         -0.13790       -0.99640       -1.08180
C         -1.51150       -0.51710        0.87040
O          0.79460       -0.21250        1.00010
C          2.02800        0.18680        0.56580
H         -0.87950        1.92620        0.49060
H         -1.40390        1.21260       -1.04860
H          0.28300        1.74810       -0.92890
H         -1.15690       -1.39450       -1.36250
H          0.59440       -1.80650       -0.95480
H          0.18120       -0.38560       -1.96170
H         -1.39710       -0.12500        1.90320
H         -2.40910       -0.09990        0.37410
H         -1.49720       -1.60590        0.92070
H          2.75040        0.02500        1.39650
H          2.09730        1.26790        0.32500
H          2.42460       -0.42670       -0.28580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

