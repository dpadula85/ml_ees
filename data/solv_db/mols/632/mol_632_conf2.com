%nproc=16
%mem=2GB
%chk=mol_632_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.45500        1.36760        0.18840
C         -0.21630       -0.09440       -0.15180
C         -1.37920       -0.49310       -1.05730
C         -0.34480       -0.94430        1.09290
O          0.92190       -0.31370       -0.86450
C          2.09390        0.07300       -0.25820
H         -1.36980        1.38380        0.83690
H          0.38910        1.84680        0.67400
H         -0.75140        1.88690       -0.74540
H         -2.30840       -0.16680       -0.53320
H         -1.40290       -1.60190       -1.13310
H         -1.33810        0.00400       -2.03570
H         -0.90340       -0.33550        1.86100
H         -0.96330       -1.84830        0.93170
H          0.60820       -1.21350        1.53730
H          2.92480       -0.17380       -0.96860
H          2.32290       -0.51560        0.66900
H          2.17160        1.13880       -0.04320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_632_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

