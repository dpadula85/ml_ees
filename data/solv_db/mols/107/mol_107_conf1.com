%nproc=16
%mem=2GB
%chk=mol_107_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.00420       -1.76180        0.08860
C          2.32110       -0.42470        0.16360
C          1.28940        0.49100        0.11540
C          1.53030        1.84750        0.18590
C          0.42450        2.66870        0.13300
C         -0.86100        2.18240        0.01880
C         -1.11900        0.80900       -0.05260
C         -0.00240        0.00760       -0.00590
C         -0.32520       -1.30590       -0.08640
C          0.72330       -2.23240       -0.03390
C         -1.77570       -1.41510       -0.30890
C         -2.30880       -0.03760       -0.08480
H          2.79880       -2.51280        0.12650
H          3.35350       -0.11880        0.25720
H          2.54090        2.21750        0.27730
H          0.61480        3.73700        0.18640
H         -1.71990        2.83730       -0.02010
H          0.47300       -3.29210       -0.09300
H         -1.94340       -1.77050       -1.36460
H         -2.17670       -2.16450        0.40060
H         -2.78420       -0.03320        0.93970
H         -3.05760        0.27150       -0.84300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

