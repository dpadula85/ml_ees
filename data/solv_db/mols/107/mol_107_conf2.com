%nproc=16
%mem=2GB
%chk=mol_107_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.56510       -2.20920       -0.17200
C         -2.16080       -0.94000       -0.14890
C         -1.37310        0.18640       -0.06320
C         -1.87290        1.46680       -0.03490
C         -0.97590        2.54200        0.05430
C          0.38620        2.33090        0.11340
C          0.88350        1.03990        0.08440
C         -0.01230        0.01840       -0.00270
C          0.56220       -1.20290       -0.02500
C         -0.19810       -2.38470       -0.11160
C          2.02510       -1.02770        0.05090
C          2.23510        0.45300        0.12990
H         -2.19340       -3.06200       -0.23820
H         -3.21990       -0.80410       -0.19580
H         -2.92480        1.61660       -0.08110
H         -1.35900        3.55470        0.07720
H          1.02650        3.19670        0.18120
H          0.26170       -3.36150       -0.12930
H          2.46900       -1.43000       -0.89920
H          2.45130       -1.53870        0.93850
H          2.90170        0.84340       -0.66480
H          2.65280        0.71220        1.13710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

