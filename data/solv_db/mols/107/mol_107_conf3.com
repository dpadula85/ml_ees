%nproc=16
%mem=2GB
%chk=mol_107_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.14700        2.70770        0.34380
C         -1.32340        1.95130        0.27630
C         -1.25830        0.59050        0.10050
C         -2.36220       -0.22090        0.02400
C         -2.20550       -1.60240       -0.15680
C         -0.93800       -2.14650       -0.25790
C          0.17250       -1.33450       -0.18150
C         -0.01500        0.01630       -0.00380
C          1.13710        0.72170        0.05780
C          1.08670        2.11330        0.23710
C          2.26430       -0.22530       -0.09060
C          1.63620       -1.56080       -0.24700
H         -0.20840        3.76300        0.48050
H         -2.29170        2.42130        0.36050
H         -3.35300        0.20940        0.10370
H         -3.03440       -2.26940       -0.22160
H         -0.81760       -3.22510       -0.39900
H          2.03540        2.62610        0.27900
H          2.91940        0.06560       -0.93930
H          2.86560       -0.25100        0.86670
H          1.91430       -2.27110        0.56360
H          1.92290       -2.07930       -1.19610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_107_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

