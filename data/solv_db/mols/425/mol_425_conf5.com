%nproc=16
%mem=2GB
%chk=mol_425_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.06700       -0.07100        0.25000
C         -2.83920       -0.86430       -0.15250
C         -1.63330       -0.02960        0.11950
C         -0.33930       -0.69730       -0.23510
C          0.78180        0.22410        0.07810
O          0.54680        1.35910        0.56220
O          2.10740       -0.10070       -0.13690
C          3.14710        0.83430        0.18690
C          4.49900        0.23900       -0.14180
H         -4.61160        0.21830       -0.67590
H         -4.71530       -0.62090        0.95120
H         -3.79880        0.88340        0.75260
H         -2.91430       -1.20630       -1.20000
H         -2.84040       -1.78760        0.49750
H         -1.63000        0.20720        1.21140
H         -1.74260        0.92290       -0.42600
H         -0.38700       -0.90670       -1.34350
H         -0.19130       -1.66710        0.24190
H          3.04700        1.07780        1.26640
H          2.97340        1.71670       -0.46600
H          5.24700        0.43880        0.66910
H          4.43350       -0.85320       -0.30470
H          4.92680        0.68330       -1.08150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

