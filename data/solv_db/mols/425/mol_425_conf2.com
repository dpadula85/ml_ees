%nproc=16
%mem=2GB
%chk=mol_425_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.07700        0.07430        0.15540
C         -2.82230        0.75030        0.67020
C         -1.64060       -0.01320        0.14510
C         -0.32880        0.54910        0.57910
C          0.76840       -0.26570        0.01510
O          0.48250       -1.25240       -0.70290
O          2.09830        0.04080        0.26510
C          3.15280       -0.72470       -0.26420
C          4.46800       -0.15020        0.17910
H         -4.07420       -0.96110        0.58550
H         -3.95820       -0.05170       -0.93080
H         -4.99360        0.63070        0.42830
H         -2.88820        0.73680        1.77460
H         -2.79840        1.81310        0.36720
H         -1.70690       -1.05510        0.49930
H         -1.72550        0.01550       -0.98070
H         -0.21090        1.63080        0.29410
H         -0.30790        0.51580        1.69840
H          3.10340       -0.60110       -1.37950
H          3.05430       -1.80170       -0.07570
H          4.38500        0.96000        0.16880
H          4.74600       -0.44100        1.21310
H          5.27380       -0.39950       -0.54920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

