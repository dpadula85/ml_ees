%nproc=16
%mem=2GB
%chk=mol_425_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.07230        1.00940       -0.40360
C          2.08650       -0.14730       -0.35510
C          1.48290       -0.24840        1.02150
C          0.48420       -1.37530        1.08600
C         -0.61320       -1.17420        0.09460
O         -0.61680       -1.89870       -0.92580
O         -1.55790       -0.18500        0.31680
C         -2.56300        0.00790       -0.64740
C         -3.48710        1.15280       -0.27830
H          3.14500        1.53100        0.60090
H          4.07360        0.64540       -0.67930
H          2.67210        1.79280       -1.09220
H          2.56330       -1.10570       -0.64170
H          1.26350        0.06110       -1.07820
H          2.30080       -0.49820        1.75770
H          1.01630        0.71320        1.32360
H          0.99100       -2.32210        0.84460
H          0.04890       -1.47930        2.10330
H         -2.11670        0.21140       -1.62610
H         -3.13220       -0.96890       -0.71670
H         -4.52980        0.76020       -0.36140
H         -3.28460        2.00550       -0.95310
H         -3.29900        1.51240        0.75670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

