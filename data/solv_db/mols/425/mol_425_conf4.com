%nproc=16
%mem=2GB
%chk=mol_425_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.34730        0.89360       -0.76000
C         -3.08330       -0.11330        0.33210
C         -1.70940       -0.71770        0.23780
C         -0.61170        0.28600        0.33850
C          0.68600       -0.45080        0.22980
O          0.73740       -1.69670        0.08150
O          1.89710        0.22370        0.28940
C          3.12420       -0.51240        0.18190
C          4.29270        0.43560        0.27440
H         -2.67440        1.77090       -0.70460
H         -4.38530        1.28950       -0.56530
H         -3.38480        0.43280       -1.75440
H         -3.81890       -0.93330        0.21080
H         -3.24880        0.34070        1.32020
H         -1.60720       -1.30530       -0.69310
H         -1.59850       -1.43140        1.07940
H         -0.65010        1.05810       -0.45800
H         -0.70730        0.78510        1.34590
H          3.14250       -1.17530        1.07750
H          3.14520       -1.14080       -0.71130
H          4.13290        1.13710        1.10350
H          4.45100        0.96910       -0.69430
H          5.21780       -0.14530        0.51000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

