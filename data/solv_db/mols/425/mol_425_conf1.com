%nproc=16
%mem=2GB
%chk=mol_425_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.65820        0.20080       -0.54000
C          2.21910        0.68880       -0.24550
C          1.69320       -0.24740        0.80010
C          0.32000        0.00550        1.28920
C         -0.76050       -0.03290        0.29660
O         -0.70670        0.58660       -0.77580
O         -1.87260       -0.80190        0.55560
C         -2.98450       -0.98270       -0.26040
C         -3.74450        0.32420       -0.54010
H          4.00640        0.70620       -1.46900
H          4.28430        0.45380        0.33570
H          3.63660       -0.90580       -0.63450
H          1.63940        0.61970       -1.19610
H          2.25520        1.71880        0.16180
H          1.87410       -1.30690        0.49160
H          2.38640       -0.11150        1.68660
H          0.33360        1.00490        1.81320
H          0.11620       -0.71500        2.13420
H         -2.76720       -1.49880       -1.23010
H         -3.69880       -1.63300        0.30360
H         -3.53090        1.04970        0.29500
H         -3.49820        0.72940       -1.53960
H         -4.85860        0.14770       -0.47770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_425_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

