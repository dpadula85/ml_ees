%nproc=16
%mem=2GB
%chk=mol_458_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.53300       -1.06900        0.21790
C          0.85190       -1.16320        0.10480
C          1.64140       -0.06100       -0.14200
C          0.99730        1.15110       -0.27370
C         -0.37710        1.24430       -0.16140
C         -1.17240        0.14540        0.08540
O         -2.54260        0.24470        0.19640
Cl         3.38760       -0.17840       -0.28500
H         -1.12450       -1.95940        0.41310
H          1.36740       -2.11580        0.20730
H          1.58740        2.04830       -0.46990
H         -0.90140        2.19440       -0.26290
H         -3.18200       -0.48130        0.37000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

