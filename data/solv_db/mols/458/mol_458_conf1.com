%nproc=16
%mem=2GB
%chk=mol_458_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.58460       -1.09870       -0.23920
C         -0.79300       -1.23270       -0.18640
C         -1.62730       -0.15060       -0.00460
C         -1.03230        1.09190        0.12510
C          0.34070        1.22820        0.07270
C          1.17410        0.14150       -0.10950
O          2.54380        0.23230       -0.16750
Cl        -3.37520       -0.34530        0.05920
H          1.21910       -1.95760       -0.38210
H         -1.25500       -2.21490       -0.28930
H         -1.68400        1.94140        0.26760
H          0.78400        2.20870        0.17640
H          3.12050        0.15580        0.67770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

