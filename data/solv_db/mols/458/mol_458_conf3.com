%nproc=16
%mem=2GB
%chk=mol_458_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.32530        1.23430       -0.04340
C         -1.03650        1.07350        0.11180
C         -1.61800       -0.17890        0.11980
C         -0.78260       -1.27020       -0.03410
C          0.56770       -1.10500       -0.18780
C          1.16350        0.14910       -0.19730
O          2.52350        0.27280       -0.35410
Cl        -3.35220       -0.38840        0.31720
H          0.76510        2.21500       -0.04790
H         -1.67060        1.94250        0.23070
H         -1.24750       -2.24130       -0.02600
H          1.16870       -1.98690       -0.30340
H          3.19370        0.28360        0.41450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_458_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

