%nproc=16
%mem=2GB
%chk=mol_190_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.30550       -1.11980       -0.27220
C         -1.91800        0.11360       -0.13050
C         -1.12640        1.21590        0.09830
C          0.26080        1.09230        0.18530
C          0.87260       -0.13480        0.04460
C          0.07190       -1.22890       -0.18370
N          2.28670       -0.19420        0.14350
Cl         1.24260        2.50680        0.47640
H         -1.94250       -1.99320       -0.45420
H         -2.99430        0.19480       -0.20040
H         -1.61530        2.19200        0.21010
H          0.52970       -2.20550       -0.29800
H          2.75250       -0.46510        1.04240
H          2.88530        0.02630       -0.66150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

