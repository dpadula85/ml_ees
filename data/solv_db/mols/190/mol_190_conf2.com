%nproc=16
%mem=2GB
%chk=mol_190_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29660        1.16370        0.04410
C         -1.91910       -0.08060        0.05340
C         -1.14140       -1.22810        0.02210
C          0.24890       -1.10640       -0.01790
C          0.86150        0.13070       -0.02700
C          0.08060        1.26720        0.00430
N          2.28470        0.16230       -0.06860
Cl         1.20620       -2.57050       -0.05660
H         -1.89340        2.06660        0.06840
H         -2.99650       -0.17960        0.08460
H         -1.61260       -2.20340        0.02880
H          0.60040        2.21490       -0.00420
H          2.79210        0.56420        0.76930
H          2.78520       -0.20100       -0.90080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

