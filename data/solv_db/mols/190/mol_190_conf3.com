%nproc=16
%mem=2GB
%chk=mol_190_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32370       -1.14090       -0.10800
C         -1.93190        0.10760        0.01690
C         -1.13700        1.22170        0.11810
C          0.25930        1.11230        0.09670
C          0.85750       -0.12300       -0.02680
C          0.05250       -1.25160       -0.12920
N          2.26980       -0.18800       -0.04390
Cl         1.27740        2.52980        0.22510
H         -1.94010       -2.01840       -0.18770
H         -2.99460        0.17160        0.03130
H         -1.58350        2.21850        0.21770
H          0.57270       -2.19100       -0.22360
H          2.87370       -0.05510       -0.86160
H          2.74780       -0.39340        0.87500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

