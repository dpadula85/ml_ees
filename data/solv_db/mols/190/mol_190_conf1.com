%nproc=16
%mem=2GB
%chk=mol_190_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29990       -1.19830       -0.02830
C         -1.88600        0.04330        0.07230
C         -1.16610        1.22140        0.11760
C          0.22380        1.14080        0.05790
C          0.84390       -0.07780       -0.04260
C          0.08500       -1.22480       -0.08440
N          2.26360       -0.14210       -0.10250
Cl         1.22580        2.57780        0.10820
H         -1.89020       -2.11460       -0.06230
H         -2.96190        0.14050        0.12050
H         -1.62210        2.19180        0.19610
H          0.59340       -2.18070       -0.16420
H          2.74500       -0.71030        0.62830
H          2.84560        0.33300       -0.81650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_190_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

