%nproc=16
%mem=2GB
%chk=mol_294_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51680        1.31670       -0.20410
C         -2.27020        0.32110        0.87850
C         -1.89660       -1.04490        0.36240
C         -0.65240       -1.04170       -0.46360
C          0.55140       -0.53460        0.34250
C          1.73050       -0.58510       -0.61080
C          3.00600       -0.10550        0.05850
C          2.87430        1.28810        0.52830
O          3.80610        1.81640        1.08340
H         -1.72090        1.38500       -0.95540
H         -2.57990        2.32460        0.27670
H         -3.49630        1.16880       -0.71380
H         -3.21810        0.21000        1.45570
H         -1.51940        0.71040        1.56260
H         -1.69050       -1.68060        1.26570
H         -2.69540       -1.53520       -0.19130
H         -0.74260       -0.49590       -1.40990
H         -0.41140       -2.09630       -0.71380
H          0.67550       -1.26360        1.16580
H          0.37960        0.45830        0.76240
H          1.55680        0.07150       -1.48700
H          1.82290       -1.61330       -0.98980
H          3.79430       -0.13690       -0.73020
H          3.25980       -0.75230        0.92360
H          1.95330        1.81500        0.37130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

