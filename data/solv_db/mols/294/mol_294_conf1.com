%nproc=16
%mem=2GB
%chk=mol_294_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.91730        0.04090        0.92490
C         -2.89960       -0.32350       -0.12460
C         -1.53100        0.08140        0.39420
C         -0.44400       -0.22510       -0.58870
C          0.87940        0.20150        0.00910
C          1.97290       -0.08440       -0.97960
C          3.34230        0.29290       -0.53900
C          3.85170       -0.36670        0.66250
O          4.68390       -1.28680        0.56980
H         -3.52090        0.91700        1.53060
H         -4.04150       -0.84170        1.58960
H         -4.91190        0.26360        0.47530
H         -3.12290        0.18180       -1.09500
H         -2.97370       -1.43180       -0.32180
H         -1.57510        1.15350        0.63420
H         -1.29520       -0.44610        1.36230
H         -0.61980        0.26880       -1.58140
H         -0.40880       -1.32180       -0.84390
H          0.97740       -0.32290        0.97120
H          0.89650        1.30030        0.24330
H          1.90770       -1.13990       -1.27660
H          1.73140        0.55360       -1.88660
H          3.41450        1.41540       -0.43490
H          4.03280        0.02200       -1.39080
H          3.57100       -0.14350        1.69590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

