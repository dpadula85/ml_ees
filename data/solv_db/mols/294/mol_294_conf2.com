%nproc=16
%mem=2GB
%chk=mol_294_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61530        0.87070        0.43720
C         -2.72500        0.02290       -0.40750
C         -1.30750       -0.08040        0.11880
C         -0.53680       -0.96640       -0.83780
C          0.89950       -1.15690       -0.42880
C          1.63440        0.15380       -0.36730
C          3.06690       -0.15080        0.04980
C          3.78260        1.14710        0.10660
O          3.71140        1.89050        1.06460
H         -4.36610        0.18650        0.93050
H         -3.07340        1.42360        1.21320
H         -4.24740        1.54210       -0.19930
H         -2.70490        0.39580       -1.45280
H         -3.09110       -1.03770       -0.48640
H         -1.29280       -0.59850        1.10250
H         -0.84200        0.91020        0.26390
H         -1.06600       -1.94590       -0.84560
H         -0.54380       -0.52000       -1.85090
H          1.00170       -1.73300        0.51120
H          1.37930       -1.77220       -1.22200
H          1.68980        0.58690       -1.37230
H          1.19440        0.87500        0.33780
H          3.08590       -0.67530        1.03050
H          3.58100       -0.80150       -0.66190
H          4.38510        1.43350       -0.74000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

