%nproc=16
%mem=2GB
%chk=mol_294_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.46630        0.69510        0.22030
C          3.01100       -0.71570        0.39380
C          1.69610       -1.02100       -0.23990
C          0.56350       -0.18660        0.29650
C         -0.75230       -0.50240       -0.34770
C         -1.80220        0.37440        0.25160
C         -3.17650        0.13050       -0.33630
C         -4.17540        1.03250        0.30090
O         -5.32420        0.97750       -0.04350
H          3.09270        1.29850        1.07760
H          3.19840        1.13540       -0.75790
H          4.57930        0.71260        0.30300
H          3.79650       -1.38430        0.00220
H          2.98680       -0.91380        1.50540
H          1.45140       -2.08350       -0.01320
H          1.81020       -0.95330       -1.33850
H          0.74250        0.90100        0.17970
H          0.50870       -0.37570        1.38780
H         -1.00640       -1.57870       -0.32140
H         -0.69900       -0.23930       -1.44350
H         -1.85470        0.13950        1.34280
H         -1.55630        1.42930        0.08030
H         -3.17510        0.29840       -1.42850
H         -3.49840       -0.91320       -0.13690
H         -3.88310        1.74290        1.07530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

