%nproc=16
%mem=2GB
%chk=mol_294_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.70830        0.48280       -0.59090
C         -2.34970        0.43850        0.13560
C         -1.74860       -0.89340       -0.24100
C         -0.41440       -1.16150        0.36960
C          0.56560       -0.11170       -0.06810
C          1.94890       -0.35140        0.52070
C          2.82860        0.77340        0.00020
C          4.21660        0.65060        0.50850
O          5.01300        1.50350        0.15310
H         -3.99170        1.53840       -0.71060
H         -4.48190       -0.02990       -0.00090
H         -3.62310       -0.00400       -1.57270
H         -2.50120        0.52250        1.24070
H         -1.72510        1.28270       -0.21590
H         -1.70250       -0.88930       -1.37070
H         -2.43390       -1.73430       -0.00160
H         -0.01480       -2.16210        0.13120
H         -0.53020       -1.17210        1.49270
H          0.60310       -0.11710       -1.17030
H          0.17210        0.88000        0.24040
H          2.36890       -1.28770        0.10190
H          1.90990       -0.39100        1.60720
H          2.34570        1.73260        0.29080
H          2.80660        0.67090       -1.12350
H          4.44620       -0.17060        1.15360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_294_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

