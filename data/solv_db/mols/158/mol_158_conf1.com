%nproc=16
%mem=2GB
%chk=mol_158_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.44170        0.60120        0.14670
C          2.39720       -0.75710       -0.07650
C          1.16610       -1.36850       -0.20100
C         -0.03080       -0.68910       -0.11350
C         -1.25590       -1.31890       -0.24080
C         -2.43960       -0.59880       -0.14630
C         -2.39840        0.76320        0.07750
C         -1.16660        1.36680        0.20070
C          0.02700        0.67220        0.11060
C          1.26080        1.31780        0.24070
H          3.37680        1.15650        0.25610
H          3.31740       -1.32180       -0.15080
H          1.13390       -2.42280       -0.37430
H         -1.23270       -2.38560       -0.41500
H         -3.38520       -1.14720       -0.25470
H         -3.32060        1.32480        0.15130
H         -1.13590        2.43330        0.37600
H          1.24480        2.37390        0.41330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_158_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_158_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_158_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

