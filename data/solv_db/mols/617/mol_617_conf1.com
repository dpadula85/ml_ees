%nproc=16
%mem=2GB
%chk=mol_617_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.03650       -0.85630       -0.01010
C          1.22160        0.50330        0.03370
C          0.12230        1.32090        0.04160
O          0.30270        2.56320        0.08180
N         -1.12560        0.77330        0.00620
C         -1.30270       -0.55500       -0.03650
O         -2.44400       -1.06430       -0.06910
N         -0.23230       -1.36420       -0.04460
Cl         2.84970        1.19870        0.07930
H          1.90190       -1.50000       -0.01640
H         -1.96030        1.38030        0.01190
H         -0.36970       -2.39970       -0.07790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_617_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_617_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_617_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

