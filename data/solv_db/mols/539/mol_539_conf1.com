%nproc=16
%mem=2GB
%chk=mol_539_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.08090        0.00250        0.05180
F         -0.47650        1.21840       -0.46720
F         -0.74780       -1.03220       -0.57800
Cl         1.68240       -0.14400       -0.13000
H         -0.37720       -0.04470        1.12350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_539_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_539_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_539_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

