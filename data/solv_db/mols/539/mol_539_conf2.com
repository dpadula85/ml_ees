%nproc=16
%mem=2GB
%chk=mol_539_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.08360       -0.00900        0.06610
F         -0.68870       -1.06010       -0.57200
F         -0.54990        1.17030       -0.47360
Cl         1.67060       -0.05270       -0.15200
H         -0.34830       -0.04840        1.13140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_539_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_539_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_539_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

