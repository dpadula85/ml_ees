%nproc=16
%mem=2GB
%chk=mol_011_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.06880       -1.15420       -0.03330
C          1.69120        0.06490       -0.07290
C          0.93530        1.21900       -0.05300
C         -0.44710        1.18140        0.00650
C         -1.08450       -0.05410        0.04700
C         -0.30880       -1.19880        0.02610
O         -2.46210       -0.06130        0.10590
Cl        -1.38630        2.68260        0.02980
H          1.69010       -2.03910       -0.05030
H          2.76260        0.19350       -0.12000
H          1.39810        2.21350       -0.08320
H         -0.82500       -2.16330        0.05840
H         -3.03230       -0.88410        0.13910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_011_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_011_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_011_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

