%nproc=16
%mem=2GB
%chk=mol_011_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.46370       -0.69320       -0.00630
C         -1.58270        0.67700        0.09420
C         -0.42120        1.42170        0.11290
C          0.80630        0.77490        0.03130
C          0.91900       -0.59650       -0.06910
C         -0.24850       -1.33210       -0.08700
O          2.16110       -1.23690       -0.15090
Cl         2.32260        1.67970        0.05130
H         -2.35870       -1.29230       -0.02250
H         -2.52300        1.16410        0.15620
H         -0.43370        2.50950        0.19030
H         -0.17120       -2.41600       -0.16600
H          2.99380       -0.65990       -0.13440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_011_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_011_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_011_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

