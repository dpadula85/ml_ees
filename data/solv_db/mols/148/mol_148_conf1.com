%nproc=16
%mem=2GB
%chk=mol_148_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65870       -0.50340       -0.20450
C          1.64470        0.55030       -0.39730
O          1.97650        1.68000       -0.88440
N          0.28300        0.37670       -0.06340
C         -0.80480        1.31730       -0.19930
C         -1.99930        0.41130       -0.37110
C         -1.71400       -0.66990        0.67580
C         -0.24240       -0.87210        0.50620
H          3.67120       -0.06620       -0.10860
H          2.67690       -1.21610       -1.07220
H          2.40680       -1.09270        0.70380
H         -0.90720        1.99440        0.66640
H         -0.68260        1.94740       -1.09260
H         -1.95430       -0.07760       -1.34820
H         -2.95020        0.87570       -0.11730
H         -2.31600       -1.54100        0.39360
H         -1.98580       -0.28890        1.69340
H         -0.04150       -1.68980       -0.23010
H          0.28040       -1.13540        1.44970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

