%nproc=16
%mem=2GB
%chk=mol_148_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.71630        0.42070       -0.23140
C          1.66340       -0.63410       -0.12520
O          1.99480       -1.85810       -0.03630
N          0.29040       -0.32620       -0.11920
C         -0.78090       -1.27630       -0.02100
C         -2.02680       -0.51710       -0.49030
C         -1.75910        0.83090        0.11350
C         -0.26920        1.00260       -0.21170
H          3.41650        0.24120        0.61630
H          3.28130        0.36690       -1.16960
H          2.24550        1.40110       -0.04460
H         -0.95290       -1.46870        1.06780
H         -0.61600       -2.19500       -0.59190
H         -2.89060       -0.98450        0.00750
H         -2.10080       -0.48710       -1.58180
H         -1.82410        0.73870        1.22460
H         -2.35670        1.64330       -0.29120
H          0.12940        1.69790        0.54750
H         -0.16050        1.40410       -1.25000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

