%nproc=16
%mem=2GB
%chk=mol_148_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73430        0.10650        0.00260
C          1.54950       -0.02780       -0.87870
O          1.76690       -0.14510       -2.11210
N          0.21650       -0.03010       -0.40650
C         -0.10510        0.10240        1.01490
C         -1.58980        0.12400        1.12730
C         -2.12350       -0.15790       -0.24490
C         -0.97480       -0.15460       -1.18910
H          3.39130       -0.78190       -0.03230
H          3.27990        1.04740       -0.25980
H          2.35260        0.19430        1.05900
H          0.36830       -0.75720        1.52820
H          0.38140        1.02180        1.43670
H         -1.89750       -0.65410        1.88130
H         -1.89000        1.12810        1.50220
H         -2.64240       -1.15560       -0.19120
H         -2.87140        0.62610       -0.48040
H         -1.06100        0.63190       -1.98640
H         -0.88530       -1.11800       -1.77070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

