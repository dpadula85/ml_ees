%nproc=16
%mem=2GB
%chk=mol_148_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.66830        0.46390        0.19080
C          1.67920       -0.57080       -0.20690
O          2.13510       -1.68920       -0.55090
N          0.28830       -0.28480       -0.19080
C         -0.31430        0.98030        0.19350
C         -1.78620        0.78600       -0.02060
C         -1.92750       -0.69070        0.27120
C         -0.75340       -1.22940       -0.56210
H          2.54580        0.59550        1.30420
H          3.68580        0.21000       -0.08720
H          2.31180        1.42590       -0.25720
H          0.10410        1.82600       -0.38550
H         -0.15360        1.13850        1.28630
H         -2.34070        1.41900        0.69570
H         -2.03380        0.98800       -1.07690
H         -1.67910       -0.85840        1.33230
H         -2.88090       -1.10880       -0.04350
H         -0.54740       -2.25390       -0.24190
H         -1.00170       -1.14720       -1.65020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

