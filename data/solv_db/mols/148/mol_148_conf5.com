%nproc=16
%mem=2GB
%chk=mol_148_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73040       -0.18830       -0.17160
C          1.60370        0.62260        0.40960
O          1.95040        1.67060        1.01400
N          0.26810        0.21350        0.27130
C         -0.87810        0.92120        0.79180
C         -2.06640        0.52600       -0.01590
C         -1.68280       -0.80770       -0.61050
C         -0.20030       -0.96850       -0.40410
H          3.31410       -0.55940        0.69950
H          2.38040       -1.04920       -0.73650
H          3.32360        0.54100       -0.78140
H         -0.66400        2.01890        0.63870
H         -0.99950        0.76950        1.87790
H         -3.01010        0.45850        0.57220
H         -2.25130        1.30400       -0.79560
H         -1.98550       -0.90280       -1.67130
H         -2.20750       -1.58820        0.00290
H          0.34780       -1.13200       -1.35190
H          0.02690       -1.84950        0.26110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_148_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

