%nproc=16
%mem=2GB
%chk=mol_460_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.18850       -0.16200       -0.51210
C          1.54930        0.44080        0.71850
C          0.11700        0.75890        0.60860
C         -0.86140       -0.28940        0.32430
C         -2.26000        0.36220        0.27940
C         -0.73520       -1.04270       -0.94900
H          2.27860       -1.26780       -0.29110
H          1.65820        0.04750       -1.44160
H          3.26540        0.18020       -0.56570
H          2.09040        1.42680        0.86680
H          1.84060       -0.14710        1.61250
H          0.01670        1.60030       -0.14310
H         -0.18460        1.25930        1.57590
H         -0.94400       -1.02100        1.15720
H         -3.04260       -0.35530        0.54540
H         -2.23730        1.21560        0.99120
H         -2.45390        0.74360       -0.75260
H          0.06560       -1.76390       -1.03470
H         -0.66230       -0.36570       -1.83750
H         -1.68880       -1.62030       -1.15230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

