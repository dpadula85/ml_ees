%nproc=16
%mem=2GB
%chk=mol_460_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.10360        0.75490        0.07610
C          1.55700       -0.47010       -0.59820
C          0.23420       -0.95170       -0.13710
C         -0.93080       -0.05450       -0.29000
C         -2.13730       -0.85450        0.26000
C         -0.88240        1.25900        0.41460
H          2.08100        0.69090        1.18930
H          1.64760        1.66020       -0.33130
H          3.19540        0.80080       -0.24190
H          2.35680       -1.26300       -0.46360
H          1.55470       -0.21960       -1.69770
H          0.25560       -1.28950        0.94510
H          0.04440       -1.92700       -0.68610
H         -1.19010        0.15840       -1.36400
H         -3.08540       -0.38970       -0.07190
H         -2.02130       -1.86840       -0.18430
H         -2.01860       -0.92830        1.34000
H         -0.65810        2.12550       -0.26150
H         -0.20940        1.26680        1.26770
H         -1.89690        1.49980        0.83500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

