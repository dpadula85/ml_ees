%nproc=16
%mem=2GB
%chk=mol_460_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.70900        0.21290       -0.23910
C          1.29730        0.26860       -0.76670
C          0.39590       -0.34270        0.26960
C         -1.04900       -0.34820       -0.13790
C         -1.83670       -0.98980        0.99580
C         -1.61140        1.02140       -0.36160
H          2.81020       -0.53620        0.57730
H          3.03750        1.18250        0.18610
H          3.35750       -0.10600       -1.06440
H          0.99640        1.33240       -0.90610
H          1.16640       -0.23780       -1.72500
H          0.68440       -1.40950        0.44530
H          0.53530        0.15980        1.25440
H         -1.13630       -0.91750       -1.07740
H         -1.79990       -0.24350        1.83800
H         -1.23770       -1.86700        1.32350
H         -2.85910       -1.25310        0.71230
H         -1.48010        1.70680        0.48780
H         -2.71990        0.90000       -0.48910
H         -1.25980        1.46680       -1.32280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

