%nproc=16
%mem=2GB
%chk=mol_460_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11450        0.77000       -0.10700
C          1.61960       -0.51510        0.45270
C          0.18260       -0.60460        0.80020
C         -0.80690       -0.41790       -0.28960
C         -0.78280        0.89840       -0.99740
C         -2.22920       -0.52330        0.29340
H          2.24290        0.67310       -1.22160
H          3.14810        1.00670        0.29370
H          1.51650        1.65610        0.07520
H          1.87420       -1.37810       -0.21880
H          2.25070       -0.73170        1.37080
H         -0.05050        0.04850        1.69720
H          0.03480       -1.64150        1.23640
H         -0.75130       -1.23400       -1.06770
H         -1.77950        0.97000       -1.53670
H         -0.83840        1.76130       -0.27190
H         -0.02980        1.02480       -1.76860
H         -2.47420       -1.60820        0.37930
H         -2.28870       -0.09570        1.29820
H         -2.95260       -0.05870       -0.41790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

