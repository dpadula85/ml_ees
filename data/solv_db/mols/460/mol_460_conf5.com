%nproc=16
%mem=2GB
%chk=mol_460_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65450       -0.22700       -0.38330
C          1.36580        0.54500       -0.18840
C          0.31260       -0.47410        0.24770
C         -1.00500        0.23030        0.46120
C         -2.02170       -0.80170        0.89030
C         -1.37160        0.91320       -0.81900
H          3.19520        0.16580       -1.26350
H          2.45920       -1.31070       -0.55490
H          3.22890       -0.12830        0.55580
H          0.99280        0.98550       -1.12950
H          1.44970        1.28510        0.62810
H          0.19130       -1.26370       -0.52860
H          0.60800       -0.93820        1.19390
H         -0.89830        0.97260        1.27910
H         -2.89710       -0.85370        0.24120
H         -1.57900       -1.82320        0.92350
H         -2.38830       -0.61370        1.93620
H         -0.81950        1.88030       -0.84680
H         -1.01910        0.35060       -1.72790
H         -2.45860        1.10600       -0.91500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_460_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

