%nproc=16
%mem=2GB
%chk=mol_088_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.77410        0.01120       -0.04490
C         -0.75210       -0.01540       -0.02360
H          1.08930       -0.66570        0.78650
H          1.19220       -0.25190       -1.01350
H          1.06750        1.05960        0.19270
H         -1.19560        0.64510       -0.76930
H         -1.03050        0.26230        1.03220
H         -1.14490       -1.04500       -0.16000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

