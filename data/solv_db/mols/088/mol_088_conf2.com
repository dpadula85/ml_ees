%nproc=16
%mem=2GB
%chk=mol_088_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.76150        0.02310        0.00060
C         -0.73310       -0.03010       -0.04280
H          1.25790       -0.72760       -0.64290
H          1.07470       -0.18850        1.05040
H          1.13300        1.04620       -0.24370
H         -1.12990       -0.95410       -0.52620
H         -1.19280        0.82990       -0.58180
H         -1.17130        0.00100        0.98650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

