%nproc=16
%mem=2GB
%chk=mol_088_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.76250        0.01390       -0.03050
C         -0.74440        0.01190        0.01480
H          1.04300       -0.83670       -0.71020
H          1.20920       -0.13230        0.98550
H          1.18140        0.97190       -0.41970
H         -1.05850       -0.61200        0.88520
H         -1.20690       -0.42100       -0.89280
H         -1.18630        1.00440        0.16760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

