%nproc=16
%mem=2GB
%chk=mol_088_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.76240        0.01420       -0.02150
C         -0.75760        0.03170       -0.02720
H          1.17120       -0.76190       -0.70810
H          1.20400        1.00440       -0.16900
H          1.05840       -0.32420        1.00530
H         -1.17180       -0.59680        0.81390
H         -1.10990        1.09070        0.05930
H         -1.15660       -0.45820       -0.95270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

