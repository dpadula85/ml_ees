%nproc=16
%mem=2GB
%chk=mol_088_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.73610       -0.05360        0.05410
C          0.74930        0.03730       -0.02580
H         -1.02510       -0.10880        1.12330
H         -1.16070       -0.92800       -0.50720
H         -1.23390        0.84070       -0.40780
H          1.01630        0.99520       -0.50300
H          1.26900       -0.00420        0.93970
H          1.12110       -0.77860       -0.67330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_088_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

