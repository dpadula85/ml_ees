%nproc=16
%mem=2GB
%chk=mol_390_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.46050        2.09500        0.08630
O         -0.69460        0.81410        0.55930
P          0.10230       -0.37190       -0.33220
O          0.63380        0.29950       -1.58830
O          1.34030       -1.10530        0.53370
C          2.60100       -0.65400        0.17310
O         -1.00990       -1.55480       -0.79420
C         -2.26750       -1.34730       -0.25980
H         -0.11540        2.74730        0.91770
H          0.30800        2.12170       -0.72420
H         -1.38900        2.58190       -0.30530
H          2.59290        0.09650       -0.62970
H          3.07330       -0.25420        1.09690
H          3.23760       -1.50780       -0.14400
H         -2.35120       -1.65400        0.80630
H         -2.61990       -0.30730       -0.33980
H         -2.98120       -1.99920       -0.81580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

