%nproc=16
%mem=2GB
%chk=mol_390_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.47830        2.06450        0.44040
O         -0.10870        1.82580        0.58610
P          0.23480        0.23080        0.06900
O          0.67890        0.22850       -1.37750
O         -1.15530       -0.66370        0.27350
C         -1.36220       -1.61970       -0.70650
O          1.44590       -0.42620        1.07030
C          2.65450       -0.54400        0.41780
H         -2.08960        1.16170        0.66910
H         -1.74000        2.39250       -0.59070
H         -1.84410        2.85350        1.13570
H         -2.34400       -2.10320       -0.47310
H         -0.59290       -2.42320       -0.58640
H         -1.35770       -1.20650       -1.73550
H          2.56680       -0.89850       -0.62190
H          3.27300       -1.29320        0.98350
H          3.21880        0.42090        0.44640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

