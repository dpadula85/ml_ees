%nproc=16
%mem=2GB
%chk=mol_390_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.33550        2.49140        0.26100
O         -0.59810        1.56530       -0.16630
P         -0.01100       -0.01230        0.05580
O         -0.34570       -0.49600        1.46870
O          1.62440       -0.11450       -0.26900
C          1.98780       -1.45590       -0.28950
O         -0.93190       -1.00790       -0.98810
C         -2.25280       -0.92070       -0.52030
H         -0.20230        3.27750        0.81570
H          0.87040        2.88750       -0.62330
H          1.07510        1.99480        0.92410
H          1.60510       -1.91790       -1.22040
H          1.57210       -2.01470        0.58000
H          3.10480       -1.50760       -0.25090
H         -2.61170       -1.97830       -0.44460
H         -2.89680       -0.33470       -1.20490
H         -2.32520       -0.45620        0.46880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

