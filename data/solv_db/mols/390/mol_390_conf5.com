%nproc=16
%mem=2GB
%chk=mol_390_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.49190        1.93050       -0.35470
O         -1.39330        0.56440       -0.30660
P          0.12950       -0.02800       -0.02340
O          0.50900        0.27440        1.42010
O          1.30090        0.58470       -1.06780
C          2.55180        0.23720       -0.52470
O          0.11000       -1.71120       -0.23010
C         -1.17000       -2.21280       -0.06880
H         -0.64580        2.47790        0.07630
H         -2.40490        2.28330        0.19570
H         -1.66690        2.21940       -1.42240
H          3.28470       -0.02260       -1.32580
H          2.99770        1.11100        0.01300
H          2.43890       -0.63070        0.17580
H         -1.77040       -1.61460        0.65870
H         -1.70420       -2.24000       -1.04710
H         -1.07510       -3.22300        0.39020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

