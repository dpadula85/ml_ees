%nproc=16
%mem=2GB
%chk=mol_390_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69340       -1.46170       -0.62360
O         -0.52320       -0.71610       -0.86440
P         -0.00490       -0.10120        0.59330
O         -0.49880       -1.01020        1.71430
O         -0.71580        1.40690        0.84260
C         -0.63840        2.22610       -0.27880
O          1.64450        0.08290        0.72070
C          2.37430       -0.56960       -0.24380
H         -1.34640       -2.44520       -0.22490
H         -2.28390       -0.97890        0.19480
H         -2.27660       -1.63690       -1.53540
H          0.33450        2.71580       -0.40380
H         -1.42740        3.01270       -0.13680
H         -0.83870        1.65880       -1.20980
H          3.39250       -0.08800       -0.26410
H          1.97470       -0.46470       -1.26730
H          2.52690       -1.63070        0.00250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_390_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

