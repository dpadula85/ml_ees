%nproc=16
%mem=2GB
%chk=mol_068_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.42860        0.05190        0.09740
C          1.11010       -0.65970        0.20110
C         -0.00070        0.33500       -0.06200
C         -1.32740       -0.32640        0.03040
O         -1.37320       -1.51500        0.28990
C         -2.60280        0.38440       -0.17690
H          2.63400        0.71020        0.96820
H          2.41300        0.71860       -0.79010
H          3.27830       -0.65930       -0.08810
H          0.96690       -1.08100        1.23180
H          1.12590       -1.47440       -0.57070
H          0.08310        1.12920        0.69100
H          0.13900        0.75580       -1.09090
H         -2.48230        1.40950       -0.56640
H         -3.18090       -0.21230       -0.93620
H         -3.21160        0.43340        0.77140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

