%nproc=16
%mem=2GB
%chk=mol_068_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36910        0.32630        0.20710
C          1.19470       -0.43650       -0.41820
C         -0.05970        0.18160        0.17020
C         -1.28710       -0.45780       -0.34500
O         -1.23980       -1.35670       -1.13490
C         -2.59510        0.03970        0.13950
H          2.79950       -0.26510        1.03230
H          3.15350        0.54400       -0.55750
H          2.02720        1.34480        0.55640
H          1.19890       -1.49630       -0.13620
H          1.19470       -0.30360       -1.51290
H         -0.03450        1.26270       -0.08620
H          0.04020        0.08540        1.27450
H         -2.55330        1.16160        0.23890
H         -2.78260       -0.43090        1.12630
H         -3.42560       -0.19930       -0.55420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

