%nproc=16
%mem=2GB
%chk=mol_068_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.89020       -0.46180        0.76130
C          1.37900        0.51450       -0.27530
C         -0.09960        0.80470       -0.08680
C         -0.92380       -0.40650       -0.19730
O         -0.39770       -1.46090       -0.41380
C         -2.38150       -0.31680       -0.04210
H          1.09210       -0.82100        1.44270
H          2.41230       -1.30890        0.26660
H          2.64920        0.02430        1.43850
H          1.90810        1.47480       -0.17600
H          1.47620        0.05420       -1.28020
H         -0.29350        1.36270        0.84870
H         -0.38310        1.46100       -0.96070
H         -2.67620        0.42830        0.72090
H         -2.82460       -1.29840        0.24600
H         -2.82700       -0.05020       -1.02050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

