%nproc=16
%mem=2GB
%chk=mol_068_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.37650        0.21010        0.48540
C         -1.11400        0.22890       -0.32540
C          0.03980       -0.45860        0.38300
C          1.27940       -0.41510       -0.43940
O          1.24370       -0.83490       -1.58000
C          2.56170        0.13800        0.06660
H         -3.18360       -0.34520       -0.05200
H         -2.19300       -0.35550        1.41500
H         -2.69860        1.22310        0.80830
H         -0.82200        1.31390       -0.47510
H         -1.33060       -0.15490       -1.32460
H         -0.29260       -1.51960        0.58760
H          0.16550        0.01920        1.37560
H          3.30330        0.14240       -0.76660
H          2.43640        1.22770        0.32610
H          2.98100       -0.41940        0.91200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

