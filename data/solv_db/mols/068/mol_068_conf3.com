%nproc=16
%mem=2GB
%chk=mol_068_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.43430        0.25740        0.01280
C         -1.15900       -0.45590        0.35580
C          0.07300        0.37470        0.07160
C          1.25190       -0.45260        0.45660
O          1.15660       -1.58320        0.91040
C          2.63190        0.08570        0.29730
H         -3.08780       -0.49220       -0.52270
H         -2.27860        1.09560       -0.69990
H         -3.00420        0.55330        0.91140
H         -1.17060       -0.66140        1.43860
H         -1.04460       -1.39790       -0.22330
H          0.07220        1.34080        0.61570
H          0.08330        0.59280       -1.02660
H          3.23430       -0.04830        1.20740
H          3.11000       -0.38110       -0.60980
H          2.56590        1.17240        0.07280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_068_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

