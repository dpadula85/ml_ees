%nproc=16
%mem=2GB
%chk=mol_609_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.16500       -1.48310       -0.03980
C         -1.36080       -0.22060        0.08790
C         -1.94830        0.99680        0.34240
C         -1.20300        2.15890        0.46030
C          0.16410        2.09290        0.31860
C          0.80460        0.89200        0.06170
C          0.01240       -0.23760       -0.04640
C          0.62300       -1.45890       -0.30390
C          1.99310       -1.49270       -0.44070
C          2.78100       -0.36550       -0.33250
C          2.17720        0.84570       -0.07740
H         -3.19380       -1.28660        0.33600
H         -1.75470       -2.26880        0.65500
H         -2.14750       -1.86190       -1.07590
H         -3.02600        1.05720        0.45550
H         -1.65520        3.12930        0.66170
H          0.80860        2.96700        0.39920
H         -0.00280       -2.33650       -0.38720
H          2.45770       -2.46290       -0.64330
H          3.87540       -0.41610       -0.44550
H          2.75960        1.75160        0.01440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

