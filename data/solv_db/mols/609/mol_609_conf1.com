%nproc=16
%mem=2GB
%chk=mol_609_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92400       -1.78580        0.05490
C         -1.30300       -0.43620        0.08130
C         -2.08610        0.68170        0.21830
C         -1.54560        1.96420        0.25070
C         -0.19470        2.11750        0.14320
C          0.62820        1.00860        0.00320
C          0.06100       -0.27110       -0.02690
C          0.90640       -1.33330       -0.16690
C          2.28210       -1.21460       -0.27970
C          2.82410        0.05390       -0.24790
C          1.99710        1.14990       -0.10750
H         -1.34470       -2.52010        0.68810
H         -2.06640       -2.16890       -0.96990
H         -2.94490       -1.76140        0.52960
H         -3.14920        0.58500        0.30440
H         -2.14710        2.84770        0.35750
H          0.26730        3.11250        0.16470
H          0.48330       -2.32530       -0.19300
H          2.94340       -2.06470       -0.39040
H          3.90310        0.19110       -0.33350
H          2.40980        2.16940       -0.08000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

