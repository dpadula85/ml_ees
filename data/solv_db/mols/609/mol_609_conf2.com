%nproc=16
%mem=2GB
%chk=mol_609_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26010       -1.29610        0.06400
C         -1.36080       -0.11020        0.02560
C         -1.85620        1.18380        0.01300
C         -1.02570        2.28990       -0.02270
C          0.34660        2.10490       -0.04690
C          0.87990        0.82700       -0.03540
C          0.01230       -0.25310        0.00060
C          0.52690       -1.54620        0.01220
C          1.89220       -1.70040       -0.01300
C          2.75220       -0.61810       -0.04890
C          2.26020        0.66660       -0.06080
H         -2.35950       -1.80440       -0.89340
H         -1.99050       -2.00720        0.86990
H         -3.28030       -0.90760        0.32890
H         -2.93920        1.28370        0.03270
H         -1.45550        3.28630       -0.03160
H          0.99740        2.97490       -0.07470
H         -0.16460       -2.38110        0.04040
H          2.27790       -2.70870       -0.00390
H          3.82340       -0.81050       -0.06740
H          2.92340        1.52670       -0.08870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

