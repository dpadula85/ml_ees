%nproc=16
%mem=2GB
%chk=mol_609_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26640       -1.30200        0.08060
C         -1.36210       -0.13780        0.05350
C         -1.86610        1.14550        0.08100
C         -1.06250        2.26010        0.05750
C          0.30840        2.11100        0.00400
C          0.84620        0.84250       -0.02470
C          0.03050       -0.26960       -0.00070
C          0.58040       -1.54650       -0.03000
C          1.95340       -1.70130       -0.08360
C          2.75980       -0.58010       -0.10720
C          2.22830        0.69640       -0.07860
H         -2.47490       -1.67640       -0.96250
H         -3.25860       -0.95800        0.45520
H         -1.94550       -2.11550        0.73080
H         -2.93930        1.26040        0.12290
H         -1.47160        3.27160        0.07960
H          0.92070        2.99830       -0.01370
H         -0.06520       -2.39090       -0.01080
H          2.36000       -2.70340       -0.10560
H          3.82810       -0.75100       -0.14920
H          2.89640        1.54670       -0.09860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

