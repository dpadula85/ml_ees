%nproc=16
%mem=2GB
%chk=mol_609_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69870       -1.97860       -0.00890
C         -1.25750       -0.55870       -0.02880
C         -2.16730        0.45930       -0.07950
C         -1.74910        1.77240       -0.09730
C         -0.40910        2.09050       -0.06440
C          0.53800        1.08180       -0.01280
C          0.08280       -0.23640        0.00390
C          1.01750       -1.23180        0.05460
C          2.36140       -0.93470        0.08780
C          2.79240        0.37030        0.07070
C          1.87640        1.39000        0.02000
H         -2.79200       -2.02820        0.19240
H         -1.46700       -2.45340       -0.96750
H         -1.22730       -2.53960        0.84750
H         -3.23090        0.19400       -0.10550
H         -2.45360        2.59170       -0.13720
H         -0.03200        3.10340       -0.07640
H          0.65500       -2.26130        0.06760
H          3.06780       -1.77750        0.12810
H          3.86830        0.54020        0.09880
H          2.22520        2.40670        0.00690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_609_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

