#!/bin/tcsh

foreach i ( `seq -w 0 641` )

  mkdir $i
  mv mol_${i}.sdf $i
  cd $i
  babel -imol mol_$i.sdf -oxyz mol_$i.xyz
  set n = `head -n 1 mol_$i.xyz`
  set n = `echo "$n+2" | bc`
  split --lines=$n mol_$i.xyz

  set k = 1
  foreach j ( `\ls x* | sort` )
    mv $j mol_${i}_conf${k}.xyz

    # Gaussian Input
    # GP
    echo "%nproc=16" > mol_${i}_conf${k}.com
    echo "%mem=2GB" >> mol_${i}_conf${k}.com
    echo "%chk=mol_${i}_conf${k}.chk" >> mol_${i}_conf${k}.com
    echo "#p B3LYP 6-31G(2df,p) nosymm" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "Title" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "0 1" >> mol_${i}_conf${k}.com
    sed '1,2d' mol_${i}_conf${k}.xyz >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com

    # Water
    echo "--Link1--" >> mol_${i}_conf${k}.com
    echo "%nproc=16" >> mol_${i}_conf${k}.com
    echo "%mem=2GB" >> mol_${i}_conf${k}.com
    echo "%chk=mol_${i}_conf${k}.chk" >> mol_${i}_conf${k}.com
    echo "#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "Title" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "0 1" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com

    # DMSO
    echo "--Link1--" >> mol_${i}_conf${k}.com
    echo "%nproc=16" >> mol_${i}_conf${k}.com
    echo "%mem=2GB" >> mol_${i}_conf${k}.com
    echo "%chk=mol_${i}_conf${k}.chk" >> mol_${i}_conf${k}.com
    echo "#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "Title" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "0 1" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com

    # Cyclohexane
    echo "--Link1--" >> mol_${i}_conf${k}.com
    echo "%nproc=16" >> mol_${i}_conf${k}.com
    echo "%mem=2GB" >> mol_${i}_conf${k}.com
    echo "%chk=mol_${i}_conf${k}.chk" >> mol_${i}_conf${k}.com
    echo "#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "Title" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com
    echo "0 1" >> mol_${i}_conf${k}.com
    echo "" >> mol_${i}_conf${k}.com

    rm mol_${i}_conf${k}.xyz
    @ k ++
  end
  cd ..
end
