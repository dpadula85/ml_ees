%nproc=16
%mem=2GB
%chk=mol_264_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.41610        1.13010        0.01730
C         -1.30690        0.07300        0.00510
C         -0.90660       -1.23730       -0.01470
N          0.41450       -1.48920       -0.02240
C          1.33710       -0.52550       -0.01160
C          0.92690        0.79810        0.00840
Cl         3.05890       -0.82430       -0.02120
H         -0.74150        2.16490        0.03250
H         -2.37980        0.31350        0.01230
H         -1.65800       -2.00570       -0.02340
H          1.67150        1.60240        0.01770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_264_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_264_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_264_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

