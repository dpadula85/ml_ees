%nproc=16
%mem=2GB
%chk=mol_330_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.54300       -0.91980        1.01670
C         -2.12890       -0.43500       -0.33750
C         -0.96080        0.44780       -0.34970
C         -1.03870        1.82840       -0.49330
C          0.05380        2.63980       -0.48200
C          1.30460        2.08420       -0.32430
C          1.43880        0.72030       -0.17810
C          0.30140       -0.08040       -0.19270
C          0.44940       -1.44900       -0.04970
C          1.69490       -2.01140        0.10510
C          2.81810       -1.20730        0.11800
C          2.71100        0.15870       -0.02120
H         -1.70870       -0.92420        1.76280
H         -2.97360       -1.95550        0.91740
H         -3.36390       -0.30270        1.42300
H         -2.01490       -1.31750       -1.03590
H         -3.01420        0.11640       -0.76490
H         -2.02390        2.23770       -0.61340
H         -0.00100        3.71540       -0.58260
H          2.17060        2.71630       -0.32160
H         -0.40660       -2.12280       -0.05060
H          1.82120       -3.09020        0.21980
H          3.82580       -1.63510        0.23540
H          3.58870        0.78580       -0.00060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

