%nproc=16
%mem=2GB
%chk=mol_330_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.72420       -0.45470        0.35420
C         -2.03100        0.07390       -0.89050
C         -0.74260        0.64530       -0.48830
C         -0.64950        2.01070       -0.32150
C          0.49700        2.61260        0.14540
C          1.55840        1.78980        0.44260
C          1.47550        0.41420        0.27570
C          0.32460       -0.20250       -0.19620
C          0.24710       -1.56590       -0.38640
C          1.34910       -2.33210       -0.09050
C          2.50730       -1.76900        0.38230
C          2.55900       -0.40230        0.56070
H         -3.83850       -0.29300        0.25870
H         -2.61690       -1.55000        0.46810
H         -2.32470        0.00630        1.28130
H         -1.98180       -0.76050       -1.60910
H         -2.68130        0.90000       -1.25700
H         -1.48050        2.67080       -0.55310
H          0.57740        3.68890        0.27500
H          2.47580        2.19940        0.81060
H         -0.66380       -2.02240       -0.78880
H          1.31640       -3.39450       -0.22940
H          3.38450       -2.35620        0.62020
H          3.46260        0.09110        0.93610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

