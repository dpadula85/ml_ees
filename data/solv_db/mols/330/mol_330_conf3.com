%nproc=16
%mem=2GB
%chk=mol_330_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.66970        0.82450       -0.37250
C          2.08820        0.06510        0.82810
C          0.85140       -0.55970        0.38740
C          0.89440       -1.90820        0.05460
C         -0.21330       -2.56890       -0.43360
C         -1.40860       -1.90870       -0.60950
C         -1.47760       -0.58170       -0.28910
C         -0.34700        0.07480        0.20550
C         -0.43720        1.40530        0.53450
C         -1.62160        2.13790        0.38990
C         -2.72980        1.48620       -0.10000
C         -2.64390        0.14790       -0.42940
H          1.85990        1.33500       -0.92620
H          3.26770        0.14570       -1.01510
H          3.36980        1.60970        0.01020
H          2.85180       -0.69040        1.09590
H          2.03390        0.79140        1.63020
H          1.79320       -2.50020        0.16000
H         -0.18360       -3.60550       -0.69240
H         -2.29030       -2.40460       -0.99350
H          0.45220        1.87000        0.93230
H         -1.60690        3.18450        0.67230
H         -3.66740        2.04150       -0.22290
H         -3.50510       -0.39170       -0.81680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

