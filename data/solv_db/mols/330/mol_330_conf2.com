%nproc=16
%mem=2GB
%chk=mol_330_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.29530       -0.36040        0.19880
C          2.03460        0.43440        0.20190
C          0.81190       -0.33510        0.01660
C          0.77980       -1.70090       -0.15420
C         -0.40370       -2.39550       -0.32770
C         -1.58570       -1.73220       -0.33360
C         -1.59790       -0.36860       -0.16640
C         -0.39520        0.32690        0.00860
C         -0.51550        1.68720        0.16740
C         -1.70470        2.35510        0.16150
C         -2.88140        1.63300       -0.01480
C         -2.80430        0.27230       -0.17680
H          3.66340       -0.42030       -0.84630
H          4.07230        0.26160        0.73660
H          3.25940       -1.31260        0.71840
H          1.97130        0.94750        1.20520
H          2.17470        1.24860       -0.55740
H          1.68660       -2.27210       -0.15740
H         -0.37640       -3.48050       -0.45940
H         -2.49800       -2.32320       -0.47410
H          0.40200        2.24290        0.30400
H         -1.81950        3.42110        0.28440
H         -3.85190        2.14710       -0.02330
H         -3.71710       -0.27640       -0.31200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_330_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

