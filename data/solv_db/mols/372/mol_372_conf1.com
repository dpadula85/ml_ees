%nproc=16
%mem=2GB
%chk=mol_372_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.28880       -0.76150        0.04200
C         -2.39220        0.61280       -0.01130
C         -1.25140        1.39940       -0.04830
C          0.02140        0.84370       -0.03330
C          0.10010       -0.53380        0.02030
C         -1.02350       -1.34200        0.05810
C          1.38500       -1.07670        0.03490
C          2.51830       -0.30370       -0.00140
C          2.34930        1.06530       -0.05420
N          1.12210        1.63050       -0.07000
H         -3.16550       -1.41940        0.07250
H         -3.38330        1.04560       -0.02300
H         -1.34770        2.47140       -0.08980
H         -0.89810       -2.39880        0.09880
H          1.48510       -2.13900        0.07600
H          3.51140       -0.75370        0.01110
H          3.25790        1.66000       -0.08240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_372_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_372_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_372_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

