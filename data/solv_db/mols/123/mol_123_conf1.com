%nproc=16
%mem=2GB
%chk=mol_123_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.04810        0.37730       -0.35780
C          3.93670       -0.99560       -0.14740
C          2.71830       -1.61270        0.04080
C          1.57940       -0.81850        0.01540
C          1.65500        0.55000       -0.19150
C          2.89260        1.13030       -0.37580
C          0.43740        1.37190       -0.21440
O          0.56340        2.60870       -0.40610
C         -0.85770        0.73270       -0.01670
C         -1.98550        1.52740       -0.04270
C         -3.25030        0.95720        0.14260
C         -3.30200       -0.38410        0.34350
C         -2.20610       -1.21660        0.37760
C         -0.97160       -0.62540        0.19170
C          0.27520       -1.41520        0.20760
O          0.21270       -2.65530        0.39460
N         -2.31950       -2.60350        0.59010
N         -1.92600        2.92690       -0.25280
H          5.03390        0.80580       -0.49970
H          4.81780       -1.62200       -0.12600
H          2.62340       -2.67630        0.20450
H          2.95310        2.19480       -0.53690
H         -4.12130        1.60810        0.11700
H         -4.28440       -0.84000        0.48940
H         -1.97790       -3.02330        1.46320
H         -2.74710       -3.19130       -0.13650
H         -2.06400        3.62250        0.50270
H         -1.73400        3.26610       -1.21430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

