%nproc=16
%mem=2GB
%chk=mol_123_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.01360       -0.92140        0.15930
C          4.09340        0.43350       -0.12970
C          2.92180        1.15030       -0.27020
C          1.67220        0.56540       -0.13300
C          1.59190       -0.78470        0.15500
C          2.76290       -1.49450        0.29400
C          0.27210       -1.39630        0.29840
O          0.17460       -2.62490        0.56070
C         -0.95990       -0.64290        0.15060
C         -2.21200       -1.18560        0.27860
C         -3.36980       -0.44750        0.13330
C         -3.30370        0.89610       -0.15300
C         -2.04640        1.45260       -0.28400
C         -0.89020        0.71110       -0.13800
C          0.43820        1.32760       -0.28260
O          0.51240        2.55340       -0.54410
N         -1.95060        2.83570       -0.57900
N         -2.31350       -2.57280        0.57430
H          4.91850       -1.50640        0.27470
H          5.07140        0.86600       -0.23170
H          2.96240        2.19820       -0.49350
H          2.70990       -2.53690        0.51630
H         -4.35570       -0.90710        0.24100
H         -4.16790        1.52390       -0.27790
H         -1.66850        3.54770        0.12270
H         -2.17150        3.16040       -1.54030
H         -2.38120       -3.23060       -0.23290
H         -2.32450       -2.97060        1.53100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

