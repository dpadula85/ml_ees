%nproc=16
%mem=2GB
%chk=mol_123_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.99510        0.85530        0.23500
C          4.08180       -0.50130        0.00940
C          2.90190       -1.19900       -0.13520
C          1.66490       -0.59260       -0.06230
C          1.57320        0.77220        0.16450
C          2.75380        1.45410        0.30650
C          0.27770        1.42580        0.24390
O          0.22800        2.67320        0.45210
C         -0.96430        0.64850        0.08460
C         -2.20070        1.28410        0.16250
C         -3.34860        0.50940        0.00580
C         -3.26140       -0.84640       -0.21970
C         -2.02910       -1.47610       -0.29670
C         -0.89180       -0.71040       -0.14170
C          0.41770       -1.33180       -0.21550
O          0.50770       -2.57280       -0.42150
N         -1.98080       -2.87840       -0.53120
N         -2.27550        2.67600        0.39470
H          4.89300        1.44780        0.35540
H          5.04060       -0.99660       -0.05130
H          2.92220       -2.27760       -0.31550
H          2.67700        2.52640        0.48450
H         -4.31860        0.98400        0.06300
H         -4.13920       -1.46120       -0.34330
H         -2.19830       -3.29540       -1.47710
H         -1.72010       -3.48610        0.27280
H         -2.29350        3.33050       -0.38990
H         -2.31250        3.03830        1.36620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

