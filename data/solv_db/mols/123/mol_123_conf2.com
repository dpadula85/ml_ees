%nproc=16
%mem=2GB
%chk=mol_123_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.08620        0.24780       -0.04280
C          3.96510       -1.03190       -0.54420
C          2.69310       -1.54310       -0.70230
C          1.56450       -0.80460       -0.37160
C          1.68520        0.47470        0.12960
C          2.95720        0.98710        0.28820
C          0.49520        1.24720        0.47580
O          0.59420        2.42030        0.93590
C         -0.85510        0.71590        0.31240
C         -1.96210        1.47600        0.65090
C         -3.23500        0.96920        0.49460
C         -3.36290       -0.29790       -0.00160
C         -2.25830       -1.06610       -0.34330
C         -0.97080       -0.56450       -0.18950
C          0.21680       -1.33780       -0.53590
O          0.07930       -2.49830       -0.98970
N         -2.40060       -2.37830       -0.85710
N         -1.83660        2.79090        1.16600
H          5.06990        0.69560        0.10020
H          4.85810       -1.59090       -0.79590
H          2.56850       -2.54920       -1.09530
H          3.01820        1.99500        0.68450
H         -4.12390        1.54670        0.75340
H         -4.36000       -0.69110       -0.12250
H         -2.64620       -2.48310       -1.86790
H         -2.27740       -3.24710       -0.29730
H         -1.52360        2.89480        2.15110
H         -2.03910        3.62270        0.61420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_123_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

