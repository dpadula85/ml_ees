%nproc=16
%mem=2GB
%chk=mol_542_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93630        1.01250        0.83730
C         -0.71770        0.11480        0.84340
C         -1.07070       -1.08340       -0.02090
C          0.47850        0.85800        0.28390
C          1.69440       -0.00390        0.27790
O          2.29180       -0.19800        1.32170
C          2.19980       -0.63260       -0.95870
H         -2.07870        1.35490       -0.22940
H         -2.83250        0.44470        1.09070
H         -1.71350        1.86210        1.49260
H         -0.53260       -0.22800        1.86200
H         -0.32130       -1.87970        0.01680
H         -2.04690       -1.45870        0.37740
H         -1.29940       -0.74950       -1.07040
H          0.24820        1.24810       -0.72430
H          0.65290        1.73340        0.94090
H          1.38900       -1.18420       -1.51640
H          2.59740        0.13850       -1.67650
H          2.99760       -1.34910       -0.68740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

