%nproc=16
%mem=2GB
%chk=mol_542_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.77060       -1.40110        0.26870
C         -0.98330       -0.34970       -0.52880
C         -1.53630        1.01010       -0.16990
C          0.44740       -0.40630       -0.05670
C          1.31910        0.55950       -0.74760
O          0.92840        1.30810       -1.59720
C          2.76590        0.59650       -0.35770
H         -1.69560       -1.08390        1.33040
H         -1.26760       -2.37360        0.19230
H         -2.82510       -1.42570       -0.05000
H         -1.08760       -0.52560       -1.60530
H         -1.43660        1.69230       -1.03940
H         -0.97480        1.37630        0.70930
H         -2.60220        0.87590        0.13040
H          0.54840       -0.25570        1.02510
H          0.81850       -1.45640       -0.25220
H          3.35420        0.06720       -1.11580
H          2.92640        0.12740        0.63260
H          3.07130        1.66480       -0.24590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

