%nproc=16
%mem=2GB
%chk=mol_542_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.48990        0.48770        1.04160
C         -0.96300        0.28050       -0.35520
C         -1.88110       -0.72070       -1.04500
C          0.41680       -0.28190       -0.37650
C          1.42430        0.55410        0.28880
O          1.20060        1.62530        0.82740
C          2.83270        0.07050        0.31670
H         -2.27480       -0.26780        1.29930
H         -1.97510        1.48610        1.14560
H         -0.70040        0.39110        1.79640
H         -1.04530        1.24350       -0.89700
H         -1.87850       -0.50200       -2.12960
H         -2.89200       -0.71390       -0.60900
H         -1.47860       -1.75390       -0.91110
H          0.72260       -0.39990       -1.43730
H          0.36940       -1.29790        0.10210
H          2.97990       -0.83710       -0.29820
H          3.49150        0.84970       -0.11660
H          3.14120       -0.21350        1.35760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

