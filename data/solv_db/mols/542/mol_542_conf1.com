%nproc=16
%mem=2GB
%chk=mol_542_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.79760       -1.30420        0.12860
C         -0.84470       -0.21710       -0.30120
C         -1.49160        1.11320       -0.12010
C          0.43680       -0.39280        0.50460
C          1.43470        0.64040        0.15650
O          1.14070        1.81910        0.24330
C          2.78620        0.26660       -0.30350
H         -1.31220       -2.07970        0.74110
H         -2.28580       -1.74500       -0.76970
H         -2.62850       -0.89390        0.76150
H         -0.62920       -0.33300       -1.40020
H         -1.46080        1.53490        0.87860
H         -1.11690        1.85700       -0.85280
H         -2.59030        1.00360       -0.37140
H          0.77400       -1.43110        0.33670
H          0.14200       -0.34830        1.57720
H          3.25430       -0.33990        0.51710
H          3.41230        1.18660       -0.48650
H          2.77670       -0.33650       -1.23990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

