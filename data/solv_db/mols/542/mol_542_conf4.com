%nproc=16
%mem=2GB
%chk=mol_542_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.22280        0.90430       -0.80130
C         -0.99990        0.22650        0.53030
C         -2.06130       -0.84220        0.68360
C          0.33750       -0.45730        0.55750
C          1.46530        0.49790        0.34670
O          1.32430        1.68450        0.17370
C          2.83930       -0.09070        0.35730
H         -0.38430        0.77050       -1.50220
H         -1.44920        1.98430       -0.61620
H         -2.13410        0.51210       -1.32560
H         -1.02590        0.93810        1.37670
H         -3.04340       -0.32430        0.55930
H         -1.94930       -1.51570       -0.20340
H         -1.99560       -1.37630        1.64150
H          0.38810       -1.17810       -0.29670
H          0.46140       -0.96790        1.53920
H          3.63290        0.64990        0.38600
H          2.93100       -0.67180       -0.58460
H          2.88630       -0.74370        1.25780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_542_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

