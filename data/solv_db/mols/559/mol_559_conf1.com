%nproc=16
%mem=2GB
%chk=mol_559_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.70150        0.14390        0.27790
C         -0.74820       -0.02500       -0.12190
F         -1.26910       -1.20180        0.38400
F         -1.48700        1.04500        0.32860
F         -0.87560       -0.04130       -1.51270
F          1.17240        1.32070       -0.24510
Br         1.73600       -1.30870       -0.47680
H          0.77010        0.06730        1.36610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

