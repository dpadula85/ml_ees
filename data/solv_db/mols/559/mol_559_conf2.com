%nproc=16
%mem=2GB
%chk=mol_559_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.72780       -0.20800        0.10510
C          0.75160        0.04380       -0.07350
F          1.48000       -1.14120       -0.08370
F          1.16990        0.71980        1.07870
F          1.04680        0.74740       -1.19560
F         -1.21810       -0.98940       -0.91900
Br        -1.60790        1.52160        0.01340
H         -0.89450       -0.69400        1.07460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

