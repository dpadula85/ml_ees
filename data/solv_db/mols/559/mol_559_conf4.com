%nproc=16
%mem=2GB
%chk=mol_559_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.69220       -0.21320       -0.22310
C         -0.74630        0.14620        0.07810
F         -1.63910       -0.63010       -0.62260
F         -1.01160        1.46520       -0.21880
F         -0.96080       -0.13040        1.42740
F          0.90920       -1.52050        0.11440
Br         1.84580        0.98560        0.74330
H          0.91070       -0.10290       -1.29880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

