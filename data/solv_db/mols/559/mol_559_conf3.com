%nproc=16
%mem=2GB
%chk=mol_559_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.72360       -0.16560       -0.12140
C         -0.76800        0.05830       -0.01300
F         -1.19200        0.17410        1.27670
F         -1.12010        1.19250       -0.71290
F         -1.38970       -1.05950       -0.55570
F          1.11360       -1.30520        0.51770
Br         1.59760        1.32410        0.77670
H          1.03500       -0.21880       -1.16810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_559_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

