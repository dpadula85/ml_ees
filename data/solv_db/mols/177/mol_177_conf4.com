%nproc=16
%mem=2GB
%chk=mol_177_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.56220       -0.44300        0.04590
C          0.87750        0.04860        0.16960
F          1.49890       -0.14210       -1.04540
F          1.52330       -0.63350        1.16080
F          0.92790        1.39650        0.44230
O         -1.16990        0.32480       -0.97060
H         -0.58560       -1.49050       -0.23850
H         -1.13660       -0.25370        0.97090
H         -1.37310        1.19300       -0.53510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

