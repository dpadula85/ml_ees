%nproc=16
%mem=2GB
%chk=mol_177_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.57280       -0.46160        0.22580
C          0.81900        0.10380        0.03640
F          0.98290        1.28800        0.72000
F          1.13860        0.28660       -1.27920
F          1.70850       -0.80200        0.56520
O         -1.46420        0.50100       -0.26560
H         -0.82490       -0.64080        1.26720
H         -0.74360       -1.37410       -0.34370
H         -1.04350        1.09910       -0.92610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

