%nproc=16
%mem=2GB
%chk=mol_177_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.52180        0.33970        0.01480
C         -0.94200       -0.03060       -0.00850
F         -1.11200       -1.11240       -0.83060
F         -1.39080       -0.37900        1.25520
F         -1.69660        1.03320       -0.46000
O          1.32850       -0.66940        0.47470
H          0.61090        1.19560        0.73880
H          0.79580        0.73230       -0.98170
H          1.88440       -1.10940       -0.20270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

