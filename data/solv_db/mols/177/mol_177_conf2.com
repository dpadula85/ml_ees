%nproc=16
%mem=2GB
%chk=mol_177_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.47520       -0.50800       -0.12390
C          0.80970        0.29340       -0.21450
F          0.58200        1.63120        0.03810
F          1.24930        0.18160       -1.53460
F          1.75740       -0.14780        0.66130
O         -0.94950       -0.33010        1.18810
H         -1.24610       -0.11350       -0.81620
H         -0.33410       -1.56320       -0.36450
H         -1.39360        0.55630        1.16610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_177_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

