%nproc=16
%mem=2GB
%chk=mol_485_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.93630        0.72560       -0.29800
C         -2.59850       -0.58330       -0.57380
C         -1.34630       -1.05990       -0.24560
C         -0.39800       -0.23800        0.36790
C         -0.78330        1.06010        0.62240
C         -2.02560        1.56760        0.30750
C          0.85780       -0.83610        0.85560
C          1.96500       -0.92820       -0.12230
C          2.44090        0.37670       -0.69610
O          1.49680        1.08050       -1.39050
H         -3.93650        1.06090       -0.57420
H         -3.32420       -1.25440       -1.05640
H         -1.06310       -2.08980       -0.45590
H         -0.03890        1.70990        1.10540
H         -2.28630        2.60770        0.52960
H          0.66780       -1.88260        1.24270
H          1.17750       -0.25850        1.76090
H          1.66570       -1.62350       -0.95010
H          2.88740       -1.41210        0.31300
H          3.36150        0.16220       -1.28110
H          2.80820        0.99200        0.17530
H          1.40850        0.82320       -2.33810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

