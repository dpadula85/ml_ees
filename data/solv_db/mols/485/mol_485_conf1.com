%nproc=16
%mem=2GB
%chk=mol_485_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.04340        0.41120        0.19000
C         -2.11180        1.35790        0.53940
C         -0.83040        0.98640        0.86480
C         -0.40630       -0.32650        0.86210
C         -1.35650       -1.27460        0.50730
C         -2.64530       -0.90920        0.17930
C          0.98610       -0.70850        1.21780
C          1.89940       -0.73550        0.01200
C          1.97820        0.61110       -0.66560
O          2.83560        0.48750       -1.75350
H         -4.06550        0.67660       -0.07250
H         -2.43510        2.39590        0.54490
H         -0.12350        1.75340        1.13440
H         -1.05120       -2.29980        0.49800
H         -3.40400       -1.63410       -0.10100
H          0.95320       -1.71820        1.66650
H          1.37920       -0.03280        2.00110
H          1.63040       -1.52110       -0.69650
H          2.92540       -0.96090        0.38040
H          2.29340        1.35010        0.07200
H          1.00390        0.94640       -1.07920
H          3.58830        1.14480       -1.67940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

