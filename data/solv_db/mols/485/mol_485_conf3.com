%nproc=16
%mem=2GB
%chk=mol_485_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.38440        0.10930        0.25320
C          2.64370       -0.09930        1.39140
C          1.28910       -0.39670        1.34870
C          0.65120       -0.48870        0.11690
C          1.41200       -0.27590       -1.01430
C          2.75840        0.01940       -0.96470
C         -0.79910       -0.80510        0.03720
C         -1.64920        0.44810        0.06110
C         -3.10420       -0.02890       -0.02630
O         -3.26320       -0.73710       -1.21560
H          4.44970        0.34360        0.27830
H          3.17160       -0.02050        2.34270
H          0.73850       -0.55450        2.26050
H          0.91330       -0.34700       -1.99390
H          3.29250        0.17500       -1.91070
H         -1.03660       -1.33740       -0.91730
H         -1.07390       -1.50040        0.85210
H         -1.54020        1.01920        0.99420
H         -1.48570        1.04900       -0.85360
H         -3.80470        0.81350        0.06190
H         -3.22360       -0.77470        0.81240
H         -3.72410       -0.21160       -1.91450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

