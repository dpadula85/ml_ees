%nproc=16
%mem=2GB
%chk=mol_485_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.37010       -0.37970        0.12210
C          2.50670       -1.42340       -0.17460
C          1.17840       -1.19940       -0.48710
C          0.66400        0.06960       -0.51510
C          1.49580        1.11730       -0.22600
C          2.81780        0.88670        0.08440
C         -0.76970        0.24510       -0.85770
C         -1.53930        0.18510        0.45470
C         -3.03390        0.35620        0.21250
O         -3.64960        0.28730        1.46430
H          4.41210       -0.55480        0.36750
H          2.85750       -2.43530       -0.16700
H          0.51170       -2.03990       -0.71900
H          1.09880        2.12130       -0.24580
H          3.49530        1.71400        0.31730
H         -0.97270        1.23060       -1.31290
H         -1.15610       -0.56510       -1.50560
H         -1.16180        0.91620        1.18980
H         -1.43120       -0.82300        0.93010
H         -3.24390        1.34200       -0.28530
H         -3.37080       -0.45050       -0.45790
H         -4.07920       -0.60030        1.52180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

