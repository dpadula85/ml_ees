%nproc=16
%mem=2GB
%chk=mol_485_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.30160        0.26800        0.50490
C         -2.38560        0.80000        1.39200
C         -1.04290        0.84370        1.04540
C         -0.62260        0.36630       -0.16160
C         -1.54900       -0.16180       -1.03630
C         -2.89400       -0.21830       -0.71780
C          0.81460        0.42740       -0.50050
C          1.56460       -0.80670       -0.07100
C          3.03240       -0.71820       -0.42950
O          3.64110        0.37390        0.18480
H         -4.34510        0.22520        0.75440
H         -2.69420        1.18530        2.35610
H         -0.30590        1.25370        1.71820
H         -1.24860       -0.55510       -2.01510
H         -3.59210       -0.64520       -1.44150
H          1.23060        1.33870       -0.01180
H          0.88560        0.55740       -1.59810
H          1.13630       -1.67490       -0.61170
H          1.49370       -1.00420        1.00270
H          3.20160       -0.67010       -1.51550
H          3.52630       -1.62630        0.01960
H          3.45480        0.44130        1.13250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_485_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

