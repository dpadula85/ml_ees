%nproc=16
%mem=2GB
%chk=mol_152_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.15440       -0.18960       -0.31320
N          0.80300       -0.08150        0.00850
C          0.30780        1.10240        0.56090
C         -1.08420        1.34130       -0.04090
N         -1.88620        0.18540       -0.07590
C         -1.29400       -1.01420       -0.52410
C          0.03880       -1.23970        0.20410
H          2.86020        0.06830        0.50700
H          2.38500        0.50460       -1.17380
H          2.36790       -1.20550       -0.70630
H          0.94480        1.96540        0.37400
H          0.18400        0.97910        1.66250
H         -1.57610        2.09660        0.63070
H         -0.89470        1.81240       -1.03230
H         -2.63890        0.10450        0.61080
H         -1.07040       -1.07200       -1.60380
H         -1.95050       -1.86370       -0.28490
H         -0.18570       -1.34250        1.30430
H          0.53510       -2.15140       -0.10740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

