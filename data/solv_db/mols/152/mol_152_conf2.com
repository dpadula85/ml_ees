%nproc=16
%mem=2GB
%chk=mol_152_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17280       -0.03870        0.25560
N         -0.79850        0.00930        0.09510
C         -0.17070        1.22110       -0.24820
C          1.21750        1.17220        0.40630
N          1.91150       -0.03520        0.13040
C          1.16910       -1.23750        0.31830
C         -0.12820       -1.09570       -0.49330
H         -2.62900       -0.73310       -0.49380
H         -2.64730        0.94850        0.23140
H         -2.42150       -0.51140        1.25210
H          0.01980        1.25040       -1.36100
H         -0.66570        2.14050        0.06470
H          1.80680        2.02250        0.04450
H          1.02730        1.26630        1.49420
H          2.28110       -0.04480       -0.85950
H          1.74150       -2.06940       -0.15680
H          0.92840       -1.47280        1.36660
H          0.22570       -0.76730       -1.51520
H         -0.69490       -2.02470       -0.53140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

