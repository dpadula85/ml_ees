%nproc=16
%mem=2GB
%chk=mol_152_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17540        0.31550        0.18100
N         -0.87350        0.20040       -0.49720
C         -0.04770        1.26580       -0.11320
C          1.36860        0.96000        0.19850
N          1.62920       -0.38380        0.61220
C          1.04090       -1.34090       -0.28090
C         -0.42260       -1.11410       -0.35120
H         -1.91430        0.62740        1.22890
H         -2.83440        1.03580       -0.31800
H         -2.67080       -0.66620        0.22640
H         -0.04730        2.03420       -0.94310
H         -0.49340        1.81500        0.76720
H          2.04150        1.23030       -0.66740
H          1.73850        1.63080        1.02940
H          2.65040       -0.54130        0.64310
H          1.20310       -2.35640        0.18820
H          1.52590       -1.39710       -1.26810
H         -0.83550       -1.73310       -1.20080
H         -0.88310       -1.58240        0.56500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

