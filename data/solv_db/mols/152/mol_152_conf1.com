%nproc=16
%mem=2GB
%chk=mol_152_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24030       -0.10340        0.03190
N          0.87050        0.00620       -0.45710
C          0.26860        1.19840        0.02810
C         -1.20700        1.26720       -0.06480
N         -1.92880        0.07040       -0.02040
C         -1.29650       -1.14650       -0.27700
C          0.18410       -1.19450       -0.06900
H          2.25030       -0.75980        0.92100
H          2.86300       -0.55160       -0.75920
H          2.57250        0.91140        0.31310
H          0.69920        2.05610       -0.56640
H          0.61120        1.41210        1.08210
H         -1.56840        1.92910        0.77900
H         -1.51340        1.84010       -0.98950
H         -2.67710        0.00220        0.71110
H         -1.74000       -1.93440        0.40320
H         -1.59140       -1.50220       -1.30870
H          0.39220       -1.48930        0.99060
H          0.57070       -2.01150       -0.74790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

