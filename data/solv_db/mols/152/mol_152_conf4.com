%nproc=16
%mem=2GB
%chk=mol_152_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.88150        0.78100        0.25840
N          0.89900        0.17700       -0.60040
C          0.61580       -1.18170       -0.33620
C         -0.60800       -1.39370        0.52750
N         -1.73540       -0.67320       -0.01340
C         -1.41370        0.74870        0.01280
C         -0.21120        1.02570       -0.83270
H          2.29420        1.65660       -0.32000
H          2.73480        0.12060        0.44790
H          1.38970        1.14510        1.19480
H          0.45810       -1.80080       -1.25340
H          1.46780       -1.65290        0.21490
H         -0.80370       -2.48980        0.54040
H         -0.33170       -1.01790        1.54780
H         -2.57300       -0.82650        0.57140
H         -1.34780        1.13170        1.04220
H         -2.29370        1.25760       -0.46670
H          0.10030        2.07950       -0.63070
H         -0.52310        0.91310       -1.90450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_152_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

