%nproc=16
%mem=2GB
%chk=mol_295_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15510       -0.29100        0.41390
C         -0.01980        0.62270        0.10280
C          1.12020       -0.16320       -0.52260
Cl         1.66700       -1.41860        0.62260
Cl        -0.51180        1.89200       -1.03370
H         -1.00270       -1.25810       -0.12140
H         -1.14560       -0.47270        1.51890
H         -2.14860        0.12590        0.18430
H          0.38140        1.13570        1.00760
H          1.99830        0.48300       -0.71810
H          0.81660       -0.65580       -1.45430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

