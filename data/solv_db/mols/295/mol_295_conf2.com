%nproc=16
%mem=2GB
%chk=mol_295_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.16970       -0.47420       -0.01500
C         -0.14310        0.60930       -0.05960
C          1.22910        0.05840        0.29610
Cl         1.66490       -1.20210       -0.86640
Cl        -0.07310        1.41350       -1.63870
H         -1.52390       -0.70040       -1.01840
H         -0.72290       -1.37240        0.41430
H         -2.01390       -0.17200        0.60960
H         -0.37360        1.37430        0.71810
H          1.96120        0.87390        0.26390
H          1.16500       -0.40820        1.29610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

