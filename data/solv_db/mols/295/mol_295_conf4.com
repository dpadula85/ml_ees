%nproc=16
%mem=2GB
%chk=mol_295_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.04830       -0.29660       -0.58960
C         -0.18210        0.54140        0.31440
C          1.11000       -0.21850        0.60550
Cl         1.89580       -0.49330       -0.96830
Cl         0.28220        2.05630       -0.52770
H         -1.97890       -0.57860       -0.04500
H         -1.36020        0.28870       -1.47630
H         -0.54020       -1.24300       -0.83770
H         -0.71370        0.83360        1.22780
H          0.79580       -1.22480        0.98640
H          1.73960        0.33490        1.31060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

