%nproc=16
%mem=2GB
%chk=mol_295_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.14500       -0.37900       -0.34320
C          0.14120        0.34280        0.51160
C         -1.24360        0.11710       -0.06950
Cl        -1.25900        0.75830       -1.72300
Cl         0.24940       -0.35320        2.15410
H          2.15770       -0.03370       -0.06830
H          0.94680       -0.12800       -1.39150
H          1.01440       -1.46760       -0.14850
H          0.39110        1.40490        0.57810
H         -1.53350       -0.94820       -0.01640
H         -2.00970        0.68670        0.51650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

