%nproc=16
%mem=2GB
%chk=mol_295_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.21050       -0.19330        0.31640
C          0.07900        0.53020       -0.39490
C         -1.19930       -0.29700       -0.24480
Cl        -1.61120       -0.49590        1.47150
Cl        -0.14330        2.11870        0.33820
H          0.95820       -0.31750        1.37680
H          1.26850       -1.20850       -0.14390
H          2.14560        0.38080        0.17760
H          0.34310        0.57070       -1.45680
H         -2.03840        0.21390       -0.73400
H         -1.01270       -1.30200       -0.70610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_295_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

