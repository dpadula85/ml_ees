%nproc=16
%mem=2GB
%chk=mol_081_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.92720        0.07040       -0.03490
C          0.51950        0.39330        0.25210
O          0.20060        1.40010        0.95890
N         -0.53080       -0.40480       -0.24790
C         -1.92290       -0.12910        0.00590
H          2.09590       -0.16280       -1.10510
H          2.56030        0.94470        0.29120
H          2.30060       -0.81100        0.53170
H         -0.31010       -1.23780       -0.83050
H         -2.02240        0.96960        0.10630
H         -2.51920       -0.45010       -0.85530
H         -2.29890       -0.58260        0.92760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

