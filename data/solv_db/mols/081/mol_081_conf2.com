%nproc=16
%mem=2GB
%chk=mol_081_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92360       -0.19540        0.02800
C         -0.54080        0.04560       -0.45090
O         -0.40800        0.30480       -1.67430
N          0.54480       -0.02190        0.45330
C          1.92690        0.20740        0.03300
H         -1.98960       -0.11420        1.13670
H         -2.58600        0.52000       -0.49590
H         -2.23950       -1.22720       -0.29840
H          0.34340       -0.24320        1.44990
H          2.11540       -0.33830       -0.91250
H          2.60540       -0.22030        0.80720
H          2.15160        1.28270       -0.07600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

