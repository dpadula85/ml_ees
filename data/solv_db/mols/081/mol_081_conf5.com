%nproc=16
%mem=2GB
%chk=mol_081_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90970        0.14680        0.06300
C         -0.48970        0.50410       -0.06230
O         -0.15570        1.70280       -0.07050
N          0.48870       -0.49470       -0.17250
C          1.90820       -0.17090       -0.29850
H         -2.02620       -0.40610        1.03030
H         -2.22310       -0.55130       -0.76220
H         -2.58920        1.01200        0.02380
H          0.19930       -1.49350       -0.16460
H          2.48170       -0.62950        0.55160
H          2.04180        0.93170       -0.26410
H          2.27380       -0.55160       -1.28500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

