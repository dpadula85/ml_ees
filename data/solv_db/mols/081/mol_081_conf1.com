%nproc=16
%mem=2GB
%chk=mol_081_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91390       -0.22230       -0.33320
C         -0.62650        0.32920        0.13830
O         -0.59950        1.38310        0.81470
N          0.60970       -0.30380       -0.15250
C          1.87340        0.22070        0.29950
H         -1.75770       -1.26150       -0.71120
H         -2.59330       -0.30310        0.54260
H         -2.35900        0.34580       -1.16360
H          0.58330       -1.17980       -0.71450
H          2.54910       -0.60670        0.66960
H          1.77720        0.91470        1.15110
H          2.45710        0.68380       -0.54090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

