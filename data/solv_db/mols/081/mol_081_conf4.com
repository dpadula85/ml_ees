%nproc=16
%mem=2GB
%chk=mol_081_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.40100        1.36160        0.13660
C          0.67710        0.07300        0.12620
O          1.32240       -0.96850        0.37400
N         -0.70700       -0.04600       -0.14900
C         -1.37430       -1.30950       -0.15040
H          2.04840        1.50870       -0.73380
H          0.64160        2.17640        0.12190
H          1.99290        1.39720        1.07640
H         -1.20820        0.84130       -0.35010
H         -1.46110       -1.76610       -1.16680
H         -2.43270       -1.20080        0.22550
H         -0.90020       -2.06730        0.48960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_081_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

