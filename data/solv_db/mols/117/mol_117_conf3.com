%nproc=16
%mem=2GB
%chk=mol_117_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.08400       -0.21780       -0.94090
C         -2.11400        0.15060        0.16670
C         -0.69150       -0.29380       -0.17810
C          0.17560        0.11940        0.98020
O          1.52120       -0.20560        0.85090
C          2.27880        0.30360       -0.18440
O          1.83860        1.06690       -1.05230
C          3.72500       -0.09290       -0.26190
H         -2.50530       -0.75130       -1.71680
H         -3.50780        0.74630       -1.32010
H         -3.93750       -0.82770       -0.55950
H         -2.46190       -0.35250        1.09200
H         -2.07580        1.23850        0.35520
H         -0.64000       -1.40040       -0.25320
H         -0.41070        0.14030       -1.14870
H          0.06460        1.23930        1.06340
H         -0.29160       -0.30010        1.89500
H          3.73450       -1.19660       -0.22590
H          4.20080        0.33490       -1.14210
H          4.18120        0.29890        0.68820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

