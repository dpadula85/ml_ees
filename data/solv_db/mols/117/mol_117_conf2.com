%nproc=16
%mem=2GB
%chk=mol_117_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.91060       -0.21300        0.61220
C         -2.19690        0.41150       -0.55660
C         -0.75490       -0.09600       -0.64670
C          0.00320        0.24890        0.61270
O          1.32780       -0.17230        0.63490
C          2.33840        0.19450       -0.22440
O          2.08700        0.99150       -1.13800
C          3.73440       -0.35120       -0.07540
H         -3.08030        0.53660        1.43210
H         -3.93380       -0.51910        0.28660
H         -2.35440       -1.07380        1.04830
H         -2.16410        1.50370       -0.38930
H         -2.69110        0.13920       -1.50790
H         -0.23250        0.36570       -1.50410
H         -0.73790       -1.18800       -0.82850
H         -0.51570       -0.29180        1.45980
H         -0.08740        1.32920        0.86310
H          3.93420       -0.52640        1.00740
H          3.77320       -1.32370       -0.60310
H          4.46120        0.36750       -0.48310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

