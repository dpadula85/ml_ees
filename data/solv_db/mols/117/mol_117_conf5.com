%nproc=16
%mem=2GB
%chk=mol_117_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26280        0.81930        0.42740
C         -1.70280       -0.45980        1.05360
C         -1.19730       -1.35170       -0.06820
C         -0.11290       -0.62780       -0.83570
O          0.98080       -0.29360       -0.00250
C          2.03580        0.37340       -0.59710
O          1.98830        0.65180       -1.82140
C          3.25180        0.79140        0.14070
H         -3.01450        1.28270        1.09810
H         -2.69580        0.54640       -0.55390
H         -1.39780        1.47600        0.24500
H         -0.91060       -0.15220        1.73370
H         -2.51120       -0.94200        1.61060
H         -2.01380       -1.55170       -0.79180
H         -0.87120       -2.30860        0.35620
H         -0.54560        0.32060       -1.20480
H          0.23970       -1.28480       -1.64640
H          4.12310        0.32830       -0.40170
H          3.35670        1.88790       -0.00490
H          3.26000        0.49450        1.18650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

