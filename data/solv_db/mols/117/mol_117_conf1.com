%nproc=16
%mem=2GB
%chk=mol_117_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.66770       -0.38060        0.77430
C         -1.47940        0.48130        0.42600
C         -0.97350        0.02540       -0.95060
C          0.21690        0.84260       -1.36850
O          1.30400        0.76220       -0.50560
C          1.99610       -0.38260       -0.17140
O          1.60860       -1.45600       -0.69580
C          3.16100       -0.38070        0.77080
H         -3.58140        0.02830        0.28840
H         -2.46100       -1.39780        0.37530
H         -2.86100       -0.43870        1.85580
H         -1.75800        1.54370        0.29470
H         -0.68100        0.34360        1.18480
H         -0.80940       -1.05200       -0.94540
H         -1.78120        0.22950       -1.68790
H         -0.12450        1.91700       -1.36590
H          0.44900        0.61320       -2.41970
H          3.92990       -1.04190        0.27270
H          3.59690        0.61020        0.89130
H          2.91580       -0.86670        1.72690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

