%nproc=16
%mem=2GB
%chk=mol_117_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.65070       -0.35510       -0.59350
C         -2.14050        0.86720        0.12970
C         -0.68820        0.70770        0.56010
C          0.11000        0.49130       -0.67590
O          1.46470        0.31840       -0.55040
C          2.11610       -0.67120        0.15210
O          1.35370       -1.47680        0.72100
C          3.60480       -0.77220        0.22690
H         -3.77080       -0.24460       -0.62760
H         -2.47260       -1.28740       -0.00020
H         -2.31810       -0.43090       -1.63060
H         -2.24980        1.72370       -0.57140
H         -2.81090        1.00940        1.01780
H         -0.45240        1.66210        1.07920
H         -0.66250       -0.15890        1.24680
H         -0.07570        1.36280       -1.35640
H         -0.31980       -0.38910       -1.20780
H          4.06430        0.23380        0.33100
H          4.01890       -1.26310       -0.67620
H          3.87940       -1.32730        1.14920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_117_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

