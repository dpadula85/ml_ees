%nproc=16
%mem=2GB
%chk=mol_242_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.47730        1.02290        0.31240
C         -0.91230        0.75750       -0.20850
C         -1.05050       -0.72650        0.12630
C          0.21920       -1.20160       -0.45350
O          0.37640       -2.12800       -1.19220
C          1.24410       -0.28670        0.07640
H          0.50790        1.33440        1.36020
H          0.92860        1.82240       -0.34190
H         -1.68730        1.35970        0.28070
H         -0.88590        0.82650       -1.30110
H         -1.94760       -1.16520       -0.34310
H         -1.00230       -0.88700        1.22350
H          2.08080       -0.09950       -0.60470
H          1.65160       -0.62900        1.06550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

