%nproc=16
%mem=2GB
%chk=mol_242_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.34650        1.13940       -0.01510
C          1.16100       -0.13890        0.12770
C          0.24190       -1.14110       -0.53560
C         -1.04740       -0.81390        0.10790
O         -1.91590       -1.60270        0.36440
C         -1.04270        0.63760        0.36070
H          0.70890        1.91470        0.67530
H          0.38190        1.42550       -1.07990
H          2.13200       -0.06960       -0.39040
H          1.21900       -0.37280        1.18950
H          0.23570       -0.89260       -1.61840
H          0.57410       -2.18050       -0.34220
H         -1.19480        0.90920        1.41890
H         -1.80020        1.18570       -0.26300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

