%nproc=16
%mem=2GB
%chk=mol_242_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.14240       -0.25390        0.03740
C         -0.01580       -1.18020        0.23080
C         -1.15690       -0.36470       -0.37300
C         -0.85350        0.93040        0.23460
O         -1.60920        1.63750        0.80590
C          0.56920        1.16270       -0.01210
H          1.60720       -0.47870       -0.94760
H          1.85710       -0.34860        0.87880
H          0.09190       -2.15510       -0.26720
H         -0.21730       -1.25510        1.31310
H         -2.14100       -0.78250       -0.10880
H         -0.99590       -0.30150       -1.46820
H          0.66420        1.56840       -1.05350
H          1.05760        1.82140        0.72980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

