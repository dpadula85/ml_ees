%nproc=16
%mem=2GB
%chk=mol_242_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.03330       -0.57070        0.05560
C          0.33090       -0.98120        0.57200
C          1.21390       -0.19690       -0.39840
C          0.58370        1.14050       -0.32960
O          1.12630        2.19210       -0.52200
C         -0.84270        0.95670        0.02020
H         -1.84690       -0.84990        0.73700
H         -1.17880       -0.92200       -0.97920
H          0.51300       -2.04920        0.54030
H          0.48890       -0.51090        1.56860
H          2.26350       -0.22000       -0.13280
H          0.99420       -0.60160       -1.41800
H         -1.53820        1.31770       -0.75130
H         -1.07460        1.29520        1.03760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

