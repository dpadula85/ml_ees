%nproc=16
%mem=2GB
%chk=mol_242_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.68000       -0.94520        0.30090
C         -0.71000       -0.86580       -0.25020
C         -1.22750        0.50980        0.05950
C          0.01410        1.32450       -0.08750
O          0.05180        2.50390       -0.29190
C          1.21660        0.43640        0.06050
H          1.25820       -1.71750       -0.28660
H          0.73260       -1.26980        1.34960
H         -1.37590       -1.67490        0.07090
H         -0.61500       -0.91990       -1.37460
H         -1.66730        0.58120        1.05770
H         -1.96660        0.77840       -0.74640
H          1.73850        0.77480        0.96470
H          1.87070        0.48400       -0.82670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_242_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

