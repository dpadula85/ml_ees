%nproc=16
%mem=2GB
%chk=mol_024_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.55320       -0.45820       -0.18740
C         -1.20700        0.16880        0.00320
C         -0.06820       -0.60960       -0.17260
C          1.19260       -0.07390       -0.00720
C          1.26030        1.26100        0.33830
N          0.14290        1.99420        0.50290
C         -1.09610        1.49010        0.34610
C          2.43790       -0.87100       -0.18550
H         -3.23820        0.23170       -0.70200
H         -2.94440       -0.63970        0.84830
H         -2.46200       -1.43740       -0.66400
H         -0.11980       -1.65030       -0.44190
H          2.23900        1.69360        0.47110
H         -1.99190        2.10460        0.48500
H          2.26710       -1.94600       -0.11530
H          3.18660       -0.62240        0.62130
H          2.95440       -0.63540       -1.14030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

