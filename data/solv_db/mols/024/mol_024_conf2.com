%nproc=16
%mem=2GB
%chk=mol_024_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.38850       -0.76520        0.38840
C          1.18590        0.00020        0.02100
C         -0.05120       -0.58360        0.28050
C         -1.16320        0.15570       -0.07380
C         -1.09700        1.40540       -0.65400
N          0.12720        1.91490       -0.87920
C          1.24870        1.24750       -0.55820
C         -2.48840       -0.48070        0.20940
H          3.16740       -0.64990       -0.39010
H          2.77320       -0.40350        1.38470
H          2.16280       -1.85490        0.43430
H         -0.09400       -1.55850        0.73320
H         -2.02280        1.90670       -0.89580
H          2.22330        1.67160       -0.74600
H         -3.27850        0.27590        0.17640
H         -2.42350       -1.03480        1.16510
H         -2.65850       -1.24690       -0.59590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

