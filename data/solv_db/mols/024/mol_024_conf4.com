%nproc=16
%mem=2GB
%chk=mol_024_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.52780       -0.55380        0.26500
C          1.19690        0.07480       -0.04460
C          0.01880       -0.56900        0.27630
C         -1.20050        0.04410       -0.02560
C         -1.23150        1.28030       -0.63810
N         -0.08240        1.89400       -0.94400
C          1.10020        1.30920       -0.65610
C         -2.43800       -0.68300        0.33720
H          2.78880       -1.33560       -0.47730
H          3.31980        0.19900        0.37720
H          2.46720       -1.08660        1.25700
H          0.07660       -1.53340        0.75390
H         -2.21030        1.71820       -0.85350
H          2.05170        1.80340       -0.90250
H         -2.62880       -1.56550       -0.30860
H         -3.29110        0.02860        0.21140
H         -2.46530       -1.02480        1.37210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

