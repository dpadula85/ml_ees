%nproc=16
%mem=2GB
%chk=mol_024_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44740       -0.75660        0.21820
C         -1.21220        0.06800        0.11070
C         -0.00940       -0.61530       -0.00260
C          1.17610        0.08520       -0.10670
C          1.12500        1.46380       -0.09440
N         -0.05650        2.08130        0.01660
C         -1.23500        1.42840        0.12030
C          2.50060       -0.62130       -0.23090
H         -3.35700       -0.17630       -0.03780
H         -2.48100       -1.17250        1.24250
H         -2.34660       -1.61710       -0.48710
H          0.05050       -1.68580       -0.01390
H          2.07160        1.99460       -0.17770
H         -2.18950        1.96390        0.21010
H          2.31000       -1.67800       -0.56430
H          2.95350       -0.63280        0.78410
H          3.14730       -0.12960       -0.98720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

