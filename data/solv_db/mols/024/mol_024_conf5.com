%nproc=16
%mem=2GB
%chk=mol_024_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43800       -0.91140       -0.12440
C          1.22810       -0.02910       -0.01400
C         -0.02930       -0.59090       -0.09880
C         -1.19440        0.14530       -0.00790
C         -1.05340        1.50420        0.17630
N          0.18890        2.06360        0.26080
C          1.31030        1.32590        0.16940
C         -2.54810       -0.45890       -0.09910
H          3.34870       -0.34100       -0.29280
H          2.27370       -1.70450       -0.89510
H          2.51200       -1.44580        0.85090
H         -0.15900       -1.66220       -0.24430
H         -1.93110        2.13460        0.25490
H          2.30380        1.79320        0.23950
H         -3.27310        0.21390       -0.59810
H         -2.51550       -1.46240       -0.54210
H         -2.89960       -0.57450        0.96470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_024_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

