%nproc=16
%mem=2GB
%chk=mol_063_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.73730       -0.12720       -0.13320
C         -0.74480       -0.10050        0.14410
Cl        -1.52770        1.33040       -0.53850
Cl         1.59130        1.25470        0.53050
H          0.91440       -0.14450       -1.23090
H          1.13040       -1.05920        0.34010
H         -0.93210       -0.13080        1.21490
H         -1.16890       -1.02290       -0.32700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_063_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_063_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_063_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

