%nproc=16
%mem=2GB
%chk=mol_063_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.70220       -0.33010        0.03330
C         -0.69600        0.12840       -0.29600
Cl        -1.47490        0.63050        1.22660
Cl         1.53820        1.07390        0.77530
H          1.28040       -0.64440       -0.84030
H          0.64680       -1.16970        0.75190
H         -1.29660       -0.70960       -0.69420
H         -0.70000        1.02100       -0.95660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_063_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_063_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_063_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

