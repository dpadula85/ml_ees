%nproc=16
%mem=2GB
%chk=mol_379_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04970        0.56940        0.09340
C          1.93570       -0.77370       -0.19270
C          0.70580       -1.38630       -0.31120
C         -0.46830       -0.65270       -0.14220
C         -0.37110        0.69700        0.14570
C          0.86420        1.27560        0.25680
N         -1.57570        1.43380        0.31830
N         -1.75040       -1.26550       -0.26410
O         -1.94580       -2.27470       -0.97770
O         -2.78630       -0.68100        0.45610
H          3.01340        1.07700        0.19150
H          2.84890       -1.33870       -0.32220
H          0.59840       -2.44500       -0.53570
H          0.92980        2.34580        0.48420
H         -1.88440        1.70700        1.27660
H         -2.16400        1.71190       -0.47670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

