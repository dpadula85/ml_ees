%nproc=16
%mem=2GB
%chk=mol_379_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.03990       -0.21560        0.22580
C         -1.51980       -1.40580       -0.26340
C         -0.16470       -1.47310       -0.53520
C          0.69830       -0.41490       -0.30610
C          0.11540        0.82630       -0.10070
C         -1.23100        0.91520        0.20970
N          1.00590        1.93830        0.00520
N          2.06970       -0.64490        0.04190
O          2.46620       -1.82870        0.15550
O          2.74900        0.28500        0.80930
H         -2.85620       -0.19910        0.93080
H         -2.06000       -2.34050       -0.10150
H          0.25830       -2.31620       -1.08420
H         -1.68330        1.90450        0.19600
H          1.36490        2.29790       -0.91740
H          0.82710        2.67150        0.73430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

