%nproc=16
%mem=2GB
%chk=mol_379_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96650       -0.70570       -0.13330
C          1.94240        0.66840       -0.25570
C          0.75680        1.37210       -0.19590
C         -0.43090        0.68070       -0.00870
C         -0.39670       -0.69400        0.11190
C          0.78930       -1.38970        0.05160
N         -1.63670       -1.39510        0.28970
N         -1.67990        1.36210        0.06000
O         -2.57020        0.86750        0.79760
O         -1.91090        2.52500       -0.64470
H          2.90580       -1.25980       -0.18050
H          2.85560        1.22200       -0.39650
H          0.74460        2.44690       -0.29600
H          0.77740       -2.46360        0.15210
H         -1.75080       -2.00390        1.11140
H         -2.36240       -1.23290       -0.46310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

