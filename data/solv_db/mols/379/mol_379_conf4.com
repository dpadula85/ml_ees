%nproc=16
%mem=2GB
%chk=mol_379_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.99390       -0.38680       -0.32820
C          1.91570        0.90480        0.15370
C          0.65630        1.38280        0.44150
C         -0.47050        0.60080        0.25380
C         -0.38310       -0.69040       -0.22870
C          0.88370       -1.17940       -0.52080
N         -1.51080       -1.53390       -0.43740
N         -1.76280        1.09270        0.57370
O         -1.83410        1.95520        1.47050
O         -2.85760        0.60430       -0.09680
H          2.97110       -0.76480       -0.55770
H          2.80820        1.52000        0.30070
H          0.55090        2.40650        0.82330
H          0.96320       -2.19880       -0.90170
H         -2.16620       -1.71290        0.37590
H         -1.75770       -2.00010       -1.32190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

