%nproc=16
%mem=2GB
%chk=mol_379_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04850        0.52150        0.26380
C         -1.90350       -0.79250       -0.14060
C         -0.67650       -1.35620       -0.37220
C          0.49270       -0.62220       -0.20840
C          0.35780        0.69140        0.19560
C         -0.90120        1.26080        0.43030
N          1.54000        1.46650        0.37130
N          1.74730       -1.23210       -0.45350
O          1.86110       -2.17760       -1.25160
O          2.88450       -0.80700        0.17890
H         -3.01930        0.95890        0.44410
H         -2.78280       -1.40800       -0.28120
H         -0.54800       -2.39420       -0.69320
H         -0.94560        2.30260        0.74720
H          2.22360        1.36960        1.12240
H          1.71840        2.21850       -0.35290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_379_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

