%nproc=16
%mem=2GB
%chk=mol_144_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.06800        0.84760       -0.01360
C          1.26130       -0.51870       -0.00370
C          0.19700       -1.39840        0.01030
C         -1.06890       -0.85010        0.01380
C         -1.26620        0.51450        0.00400
C         -0.20090        1.39240       -0.01000
Cl        -0.44530        3.13460       -0.02290
Cl        -2.45090       -1.94060        0.03140
Cl         2.89030       -1.19200       -0.00910
H          1.92580        1.53170       -0.02470
H          0.35810       -2.47510        0.01800
H         -2.26830        0.95410        0.00660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_144_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_144_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_144_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

