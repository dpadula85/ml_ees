%nproc=16
%mem=2GB
%chk=mol_146_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.67430       -1.07370        0.20890
C         -1.48630       -0.10510        0.33420
C         -1.71020        0.92630       -0.77890
C         -0.27060       -0.90780        0.05980
C          1.01350       -0.22720        0.12010
C          1.15690        1.10040        0.43970
N          2.37300        1.68150        0.44010
C          3.47160        0.96100        0.12620
C          3.33410       -0.37840       -0.19520
N          2.12110       -0.91820       -0.18640
H         -3.50270       -0.74540        0.85660
H         -2.33210       -2.07580        0.55160
H         -2.94200       -1.15510       -0.85390
H         -1.49540        0.40730        1.31450
H         -0.88040        0.89830       -1.51400
H         -1.83260        1.94230       -0.31650
H         -2.63860        0.74240       -1.35050
H         -0.24800       -1.75800        0.80380
H         -0.41460       -1.41350       -0.92030
H          0.31620        1.73180        0.73070
H          4.46500        1.37430        0.11100
H          4.17640       -1.00720       -0.45450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

