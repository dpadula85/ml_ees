%nproc=16
%mem=2GB
%chk=mol_146_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.77920       -0.04120       -0.67430
C         -1.40790       -0.44750       -0.12660
C         -1.46660       -0.53570        1.38180
C         -0.43860        0.62090       -0.57970
C          0.94090        0.38220       -0.13440
C          1.36730        0.88780        1.06610
N          2.61300        0.69360        1.51000
C          3.46500       -0.02900        0.73240
C          3.06260       -0.54950       -0.48030
N          1.80030       -0.32910       -0.88660
H         -3.42040       -0.93420       -0.62630
H         -2.64790        0.28670       -1.72450
H         -3.12700        0.78890       -0.04440
H         -1.19110       -1.43260       -0.55130
H         -2.36500       -1.07790        1.71340
H         -1.49340        0.45760        1.85580
H         -0.57260       -1.07970        1.75200
H         -0.81880        1.60200       -0.24390
H         -0.43480        0.57550       -1.70330
H          0.68070        1.45340        1.66000
H          4.45770       -0.16630        1.11810
H          3.77580       -1.12570       -1.07880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

