%nproc=16
%mem=2GB
%chk=mol_146_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.79690       -0.96180       -0.65660
C         -1.64150        0.48610       -0.21690
C         -1.76340        0.59030        1.26940
C         -0.39180        1.08830       -0.77650
C          0.84330        0.40200       -0.36740
C          1.35440       -0.63610       -1.11550
N          2.50000       -1.28410       -0.74950
C          3.15960       -0.91500        0.36430
C          2.65760        0.11700        1.11410
N          1.52240        0.74840        0.73850
H         -1.13600       -1.58290       -0.00720
H         -2.87370       -1.21450       -0.49650
H         -1.58130       -1.05740       -1.72500
H         -2.49170        1.03550       -0.72160
H         -2.56290        1.34630        1.51130
H         -2.15880       -0.37820        1.68790
H         -0.84350        0.85000        1.79440
H         -0.51680        1.13160       -1.88830
H         -0.37370        2.16210       -0.45250
H          0.81520       -0.91900       -2.00300
H          4.07630       -1.41790        0.67630
H          3.20340        0.40950        2.02030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

