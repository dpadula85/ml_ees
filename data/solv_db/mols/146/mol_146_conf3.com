%nproc=16
%mem=2GB
%chk=mol_146_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.46250       -1.20620       -0.13230
C         -1.45930        0.22500        0.36560
C         -2.78580        0.86440       -0.00730
C         -0.38250        0.96070       -0.39040
C          0.96210        0.37640       -0.18460
C          1.78270        0.79910        0.84010
N          3.01310        0.26230        1.02560
C          3.47860       -0.70470        0.21400
C          2.65640       -1.12680       -0.81110
N          1.42970       -0.59060       -0.99480
H         -1.32890       -1.23810       -1.21800
H         -0.62650       -1.78350        0.33440
H         -2.39410       -1.71100        0.20120
H         -1.28410        0.23590        1.46380
H         -3.49260        0.09310       -0.39880
H         -3.22200        1.40230        0.86630
H         -2.67110        1.62780       -0.80900
H         -0.43140        2.03690       -0.10660
H         -0.68690        0.93450       -1.47100
H          1.42040        1.57900        1.50330
H          4.46580       -1.13220        0.36640
H          3.01890       -1.90430       -1.47120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

