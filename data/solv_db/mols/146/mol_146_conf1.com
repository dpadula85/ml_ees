%nproc=16
%mem=2GB
%chk=mol_146_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.78140       -0.17160       -0.79030
C         -1.50190       -0.20480        0.02300
C         -1.57600        0.88340        1.02510
C         -0.35440       -0.16740       -0.91440
C          0.97560       -0.19230       -0.30830
C          2.06170       -0.15830       -1.16290
N          3.32720       -0.17760       -0.68830
C          3.54960       -0.23120        0.64770
C          2.48640       -0.26550        1.50780
N          1.24930       -0.24460        0.99260
H         -3.02850        0.90040       -1.03410
H         -2.61260       -0.69810       -1.75820
H         -3.62550       -0.64370       -0.22290
H         -1.50530       -1.18280        0.56970
H         -1.23260        1.87690        0.63340
H         -1.05510        0.62710        1.98000
H         -2.64170        1.03500        1.30210
H         -0.45860        0.73010       -1.57700
H         -0.43230       -1.04470       -1.59120
H          1.94230       -0.11460       -2.24910
H          4.55750       -0.24710        1.03940
H          2.65610       -0.30830        2.57580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_146_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

