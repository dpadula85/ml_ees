%nproc=16
%mem=2GB
%chk=mol_306_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.39820        0.81850        0.21500
C         -0.99070       -0.61700        0.01190
C          0.34420       -0.83740       -0.58250
C          1.52960       -0.33260        0.13200
C          1.55540        1.10930        0.32450
N          1.60330        2.27250        0.47730
H         -1.09930        1.48380       -0.62010
H         -1.11970        1.20640        1.21110
H         -2.53410        0.81260        0.20150
H         -1.72910       -1.04990       -0.73300
H         -1.13860       -1.10820        1.00210
H          0.48150       -1.95100       -0.70660
H          0.30650       -0.39820       -1.62360
H          2.48160       -0.56400       -0.43330
H          1.70740       -0.84490        1.12370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

