%nproc=16
%mem=2GB
%chk=mol_306_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.42990        0.68000       -0.46200
C          0.97590       -0.60660        0.12010
C         -0.45950       -0.97990       -0.07540
C         -1.47910       -0.06660        0.50670
C         -1.46400        1.26000       -0.03580
N         -1.48670        2.35470       -0.47660
H          0.95400        0.92400       -1.42930
H          1.29980        1.54450        0.22580
H          2.54500        0.58750       -0.66660
H          1.67100       -1.42850       -0.16930
H          1.10980       -0.52890        1.24610
H         -0.68650       -1.14710       -1.16000
H         -0.56870       -2.02440        0.36100
H         -1.31900       -0.07010        1.63330
H         -2.52180       -0.49870        0.38200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

