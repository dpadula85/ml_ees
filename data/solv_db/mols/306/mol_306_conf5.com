%nproc=16
%mem=2GB
%chk=mol_306_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43860        0.77110       -0.17060
C         -0.81460       -0.48020       -0.65060
C          0.33550       -0.97610        0.13340
C          1.53890       -0.14950        0.25550
C          1.32420        1.10420        0.90690
N          1.18240        2.12500        1.44910
H         -0.91700        1.63150       -0.61460
H         -2.51920        0.76340       -0.53230
H         -1.47630        0.86040        0.91930
H         -0.48760       -0.35910       -1.71930
H         -1.63260       -1.26160       -0.73550
H          0.66030       -1.94970       -0.36300
H         -0.04070       -1.30410        1.13490
H          2.32690       -0.78310        0.77000
H          1.95840        0.00790       -0.78320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

