%nproc=16
%mem=2GB
%chk=mol_306_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.61790        0.60840       -0.02550
C         -0.75170       -0.49150       -0.56610
C          0.35280       -0.89980        0.35470
C          1.30660        0.19720        0.69030
C          1.91520        0.67120       -0.55670
N          2.40890        1.03080       -1.55140
H         -2.40960        0.79730       -0.79020
H         -1.04700        1.54950        0.05790
H         -2.13210        0.33440        0.91870
H         -0.40500       -0.18330       -1.56760
H         -1.40130       -1.38120       -0.72300
H         -0.10640       -1.25830        1.29780
H          0.89730       -1.73520       -0.11150
H          0.86210        1.00330        1.27520
H          2.12810       -0.24270        1.29750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

