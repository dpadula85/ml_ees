%nproc=16
%mem=2GB
%chk=mol_306_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.03290        0.45990       -0.19500
C          0.56450        0.37510       -0.54000
C         -0.14210       -0.66060        0.31630
C         -1.59740       -0.68980       -0.08200
C         -2.23370        0.60780        0.11750
N         -2.74630        1.62120        0.28520
H          2.45690        1.27460       -0.83090
H          2.21050        0.75000        0.85530
H          2.49180       -0.50700       -0.46600
H          0.52110       -0.03080       -1.59050
H          0.05600        1.34120       -0.56300
H         -0.05920       -0.46480        1.38970
H          0.30450       -1.64250        0.04020
H         -1.72490       -0.97260       -1.14350
H         -2.13460       -1.46180        0.52580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_306_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

