%nproc=16
%mem=2GB
%chk=mol_624_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17790        0.41190       -0.58870
C         -0.03010        0.14190        0.36730
C          0.39160       -1.28680        0.36090
N          1.09310        1.02840        0.05590
O          2.21700        0.58710       -0.27010
O          0.96080        2.39450        0.10880
H         -0.88480        0.21180       -1.64210
H         -1.98880       -0.28000       -0.32310
H         -1.53960        1.43890       -0.45870
H         -0.40900        0.41070        1.39030
H         -0.18210       -1.81450       -0.44610
H          1.46200       -1.45160        0.14050
H          0.08780       -1.79240        1.30510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

