%nproc=16
%mem=2GB
%chk=mol_624_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.46030       -1.28180        0.29890
C         -0.07260        0.08910        0.79390
C          1.33650        0.46120        0.42380
N         -1.05200        1.02040        0.29810
O         -1.69820        1.67870        1.13140
O         -1.31540        1.23380       -1.03100
H         -0.75860       -1.25650       -0.76430
H          0.38730       -1.98490        0.37450
H         -1.31080       -1.61620        0.94470
H         -0.12160        0.05320        1.89550
H          1.48840        0.56260       -0.66360
H          1.58490        1.38160        0.99360
H          1.99240       -0.34120        0.85600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

