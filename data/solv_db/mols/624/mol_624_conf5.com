%nproc=16
%mem=2GB
%chk=mol_624_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.17350        0.39610       -0.61760
C          0.01280        0.14500        0.28590
C         -0.36240       -1.31640        0.34220
N         -1.12260        0.99930        0.03260
O         -2.25040        0.56800       -0.29020
O         -1.01250        2.36490        0.13750
H          1.09550       -0.07800       -1.60410
H          1.30310        1.48680       -0.72440
H          2.09330        0.00970       -0.11280
H          0.36110        0.40620        1.32950
H          0.58270       -1.87420        0.55050
H         -1.01900       -1.43230        1.24630
H         -0.85520       -1.67510       -0.57550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

