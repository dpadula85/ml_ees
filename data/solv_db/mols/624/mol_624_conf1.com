%nproc=16
%mem=2GB
%chk=mol_624_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.81410       -1.06100        0.37370
C          0.10970       -0.10690       -0.37770
C         -0.33510        1.33000       -0.09460
N          1.48280       -0.32040       -0.07170
O          2.27530       -0.62710       -0.98230
O          1.97870       -0.20160        1.19900
H         -1.83020       -0.60090        0.30460
H         -0.57290       -1.14220        1.43000
H         -0.88490       -2.02520       -0.16190
H         -0.09150       -0.27000       -1.45610
H          0.38170        1.79380        0.59540
H         -0.32640        1.93190       -1.02780
H         -1.37310        1.29960        0.26930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

