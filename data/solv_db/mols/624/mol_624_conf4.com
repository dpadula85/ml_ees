%nproc=16
%mem=2GB
%chk=mol_624_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.85280       -1.08280        0.14980
C          0.03760        0.12390        0.34580
C          1.38100       -0.02150       -0.29610
N         -0.66590        1.31280       -0.07600
O         -0.25010        2.05070       -0.98430
O         -1.83860        1.63080        0.55820
H         -0.47850       -1.69020       -0.71670
H         -0.94510       -1.70000        1.06450
H         -1.88580       -0.77300       -0.13630
H          0.18550        0.23380        1.45730
H          1.61910       -1.05500       -0.57800
H          1.50980        0.63260       -1.20010
H          2.18380        0.33780        0.41180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_624_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

