%nproc=16
%mem=2GB
%chk=mol_333_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.47720       -0.69530       -0.54500
C          0.59700       -0.67830        0.50500
C          1.36760        0.61820        0.43370
Cl         2.65710        0.68250        1.65200
C         -1.44730        0.43170       -0.35940
Cl        -2.72130        0.36820       -1.63180
H          0.00810       -0.65090       -1.55020
H         -1.01100       -1.66810       -0.51920
H          0.14450       -0.85060        1.49770
H          1.27770       -1.54270        0.30720
H          0.69290        1.46810        0.65620
H          1.78870        0.75870       -0.55810
H         -1.93020        0.35190        0.62490
H         -0.94660        1.40670       -0.51300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

