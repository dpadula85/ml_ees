%nproc=16
%mem=2GB
%chk=mol_333_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.67420       -0.23000       -0.70900
C         -0.42150       -0.91750        0.06880
C         -1.51110        0.08880        0.36790
Cl        -2.84400       -0.65340        1.28170
C          1.30410        0.91030        0.01130
Cl         2.01550        0.28600        1.53890
H          0.32350        0.13670       -1.69220
H          1.49050       -0.98190       -0.89810
H         -0.85590       -1.76420       -0.46340
H         -0.05340       -1.27860        1.04760
H         -1.89550        0.45480       -0.60690
H         -1.02730        0.91280        0.96380
H          0.65050        1.76830        0.21870
H          2.15040        1.26800       -0.64870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

