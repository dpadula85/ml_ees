%nproc=16
%mem=2GB
%chk=mol_333_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.61610        0.42940        0.00980
C         -0.60760       -0.38850        0.37650
C         -1.87360        0.42600        0.22920
Cl        -3.29830       -0.54580        0.66260
C          1.86490       -0.42290        0.17080
Cl         3.30410        0.52330       -0.25550
H          0.57970        0.71670       -1.06040
H          0.66180        1.32410        0.62340
H         -0.66090       -1.22780       -0.34710
H         -0.51430       -0.79710        1.40300
H         -2.00390        0.71390       -0.83420
H         -1.88290        1.31850        0.87090
H          1.83460       -1.29320       -0.49810
H          1.98030       -0.77640        1.21500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

