%nproc=16
%mem=2GB
%chk=mol_333_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.50620        0.24910       -0.38990
C         -0.56790       -0.38090        0.47800
C         -1.94670       -0.05830       -0.02360
Cl        -2.24520        1.69520       -0.04010
C          1.88700       -0.06460        0.09930
Cl         3.04040        0.73280       -1.00540
H          0.37490        1.35240       -0.34750
H          0.43300       -0.11820       -1.43490
H         -0.47130       -0.07940        1.51680
H         -0.43990       -1.48160        0.40650
H         -2.04380       -0.47080       -1.06300
H         -2.71130       -0.50430        0.63980
H          2.08770        0.28970        1.12390
H          2.09710       -1.16090        0.04010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

