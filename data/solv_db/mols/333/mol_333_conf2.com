%nproc=16
%mem=2GB
%chk=mol_333_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.53800       -0.50290       -0.44000
C         -0.51250        0.41910        0.15680
C         -1.89950        0.04020       -0.26250
Cl        -3.02520        1.19390        0.50220
C          1.89940       -0.01170        0.06190
Cl         3.20760       -1.00960       -0.56780
H          0.37760       -1.55160       -0.13430
H          0.56360       -0.38420       -1.54230
H         -0.47090        0.35880        1.26400
H         -0.32510        1.45000       -0.20190
H         -2.02810        0.01620       -1.36560
H         -2.19470       -0.97250        0.10310
H          2.01820        1.06400       -0.13320
H          1.85150       -0.10960        1.18540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_333_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

