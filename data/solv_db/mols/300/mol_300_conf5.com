%nproc=16
%mem=2GB
%chk=mol_300_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47240       -0.01320        0.33970
C         -1.08060        0.40840       -0.11600
C         -0.12820       -0.72740       -0.15350
C          1.23560       -0.33740       -0.63130
C          1.91150        0.67630        0.17720
O          1.51280        1.85480        0.28530
O          3.09470        0.39470        0.90150
H         -2.47330       -0.07740        1.44980
H         -3.19280        0.80120        0.05510
H         -2.77380       -0.98150       -0.11240
H         -1.18770        0.95400       -1.05180
H         -0.71730        1.12580        0.67900
H         -0.53570       -1.55270       -0.77640
H          0.01730       -1.16810        0.87780
H          1.09630        0.05220       -1.67310
H          1.84940       -1.27960       -0.72230
H          3.84440       -0.13000        0.47130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

