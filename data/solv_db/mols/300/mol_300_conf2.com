%nproc=16
%mem=2GB
%chk=mol_300_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47380       -0.05790       -0.39040
C         -0.99910        0.10580       -0.78520
C         -0.19570       -0.49860        0.31020
C          1.28450       -0.45690        0.14250
C          1.87760        0.87200        0.03340
O          1.20820        1.93040        0.08470
O          3.24670        1.00100       -0.13610
H         -2.78950       -1.11410       -0.52020
H         -3.04410        0.61310       -1.05980
H         -2.55950        0.30290        0.64530
H         -0.87530       -0.53020       -1.69060
H         -0.85950        1.16430       -0.98080
H         -0.47700       -1.59250        0.34630
H         -0.52510       -0.10870        1.31000
H          1.71260       -0.96080        1.06100
H          1.56100       -1.13770       -0.68960
H          3.90820        0.46810        0.43200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

