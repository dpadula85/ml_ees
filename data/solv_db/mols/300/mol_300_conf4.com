%nproc=16
%mem=2GB
%chk=mol_300_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24270        0.09760        0.05260
C          0.83890       -0.07610        0.63320
C         -0.01330       -0.54770       -0.52130
C         -1.45990       -0.78170       -0.15100
C         -1.99840        0.49370        0.37190
O         -2.29110        0.63290        1.58180
O         -2.14130        1.56590       -0.50260
H          2.19690        0.41620       -1.03160
H          2.77190        0.87370        0.63230
H          2.75100       -0.88070        0.01960
H          0.47870        0.93010        1.00550
H          0.83220       -0.83500        1.42740
H          0.02890        0.18730       -1.36650
H          0.48960       -1.47780       -0.91370
H         -1.97520       -1.10930       -1.09250
H         -1.47220       -1.56880        0.63460
H         -1.27940        2.07990       -0.77990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

