%nproc=16
%mem=2GB
%chk=mol_300_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.42900        0.02930       -0.51970
C          0.90920        0.03330       -0.73240
C          0.28380        0.34820        0.57730
C         -1.19850        0.40220        0.59100
C         -1.88020       -0.83350        0.20530
O         -1.33360       -1.91290       -0.12590
O         -3.27190       -0.83050        0.19920
H          2.58120       -0.58020        0.41370
H          2.76680        1.05300       -0.33740
H          2.92630       -0.47110       -1.36640
H          0.66380       -0.93020       -1.17160
H          0.71000        0.81130       -1.49340
H          0.65820       -0.36470        1.34700
H          0.66060        1.34830        0.89980
H         -1.57520        1.24730       -0.01250
H         -1.51070        0.63280        1.64720
H         -3.81900       -0.05420       -0.12100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

