%nproc=16
%mem=2GB
%chk=mol_300_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.26440        0.45580       -0.44430
C          0.85290        0.50880        0.10990
C          0.28550       -0.89600        0.20580
C         -1.11970       -0.80360        0.75980
C         -2.00670        0.01430       -0.10740
O         -2.50690       -0.46430       -1.15980
O         -2.33170        1.33420        0.18650
H          2.30860        0.85090       -1.46820
H          2.58740       -0.61050       -0.49550
H          2.99190        0.95280        0.24050
H          0.20040        1.19570       -0.44010
H          0.88430        0.89000        1.17110
H          0.89220       -1.41550        0.98840
H          0.33880       -1.37870       -0.76580
H         -1.55230       -1.81510        0.90310
H         -1.06880       -0.33320        1.75880
H         -3.02010        1.51430        0.90500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_300_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

