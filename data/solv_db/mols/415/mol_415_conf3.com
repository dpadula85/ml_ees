%nproc=16
%mem=2GB
%chk=mol_415_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.32110       -0.24740       -0.22900
N          0.93510        0.11180       -0.42980
C          0.43410        0.83260        0.70320
C         -0.94010        1.37940        0.46860
C         -1.85010        0.39100       -0.21480
C         -1.26480       -0.98520       -0.23670
C          0.15820       -1.07190       -0.61070
H          3.00000        0.16500       -0.99050
H          2.46560       -1.35110       -0.23040
H          2.70130        0.10320        0.75930
H          0.46140        0.18840        1.61160
H          1.12550        1.68380        0.88860
H         -1.35820        1.71580        1.45180
H         -0.89790        2.30750       -0.15390
H         -2.16110        0.74200       -1.23550
H         -2.79700        0.32690        0.39900
H         -1.86020       -1.57730       -0.99050
H         -1.41110       -1.45770        0.76100
H          0.27540       -1.36650       -1.69420
H          0.66270       -1.89020       -0.02680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

