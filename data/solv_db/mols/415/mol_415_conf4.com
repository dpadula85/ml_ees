%nproc=16
%mem=2GB
%chk=mol_415_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06480        0.15890        0.54900
N          1.06550        0.34090       -0.47020
C          0.44100       -0.89560       -0.87720
C         -0.57340       -1.36160        0.14220
C         -1.64390       -0.32030        0.34600
C         -1.32500        0.92790       -0.46940
C          0.09660        1.32330       -0.09570
H          1.80310       -0.59240        1.30830
H          3.03040       -0.06710        0.07790
H          2.20540        1.12180        1.10260
H         -0.04750       -0.69630       -1.86030
H          1.18100       -1.69060       -1.03150
H         -0.02570       -1.47420        1.10370
H         -0.99100       -2.32400       -0.21680
H         -1.68430        0.03290        1.41350
H         -2.65890       -0.70600        0.13030
H         -1.97260        1.76900       -0.10090
H         -1.44890        0.75220       -1.54150
H          0.09740        1.41120        1.02460
H          0.38620        2.28990       -0.53450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

