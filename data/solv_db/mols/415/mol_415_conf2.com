%nproc=16
%mem=2GB
%chk=mol_415_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30520       -0.24210        0.05790
N          0.88670       -0.14320        0.43550
C          0.13520       -1.26350        0.09670
C         -1.28060       -1.06200       -0.29470
C         -1.78520        0.19100        0.43160
C         -0.98660        1.32340       -0.22910
C          0.43340        1.14570        0.14150
H          2.42950       -1.01350       -0.71970
H          2.93780       -0.38510        0.94670
H          2.65410        0.71730       -0.41130
H          0.14500       -1.99260        0.95990
H          0.63770       -1.83970       -0.73570
H         -1.93810       -1.92480       -0.09560
H         -1.33140       -0.83290       -1.38880
H         -2.85160        0.32240        0.25380
H         -1.55260        0.13190        1.51490
H         -1.13380        1.15270       -1.33600
H         -1.40870        2.29960        0.02340
H          0.64100        1.81460        1.02820
H          1.06290        1.60090       -0.67920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

