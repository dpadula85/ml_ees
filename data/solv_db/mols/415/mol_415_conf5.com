%nproc=16
%mem=2GB
%chk=mol_415_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24620       -0.08930        0.37070
N          0.99090       -0.04790       -0.30860
C          0.24700       -1.23210       -0.30920
C         -1.22610       -1.09540       -0.46770
C         -1.66010       -0.04150        0.55950
C         -1.07810        1.25810        0.04240
C          0.34090        1.19030       -0.33250
H          2.31520       -1.03530        0.93910
H          3.08300       -0.14060       -0.38460
H          2.38240        0.79980        1.02670
H          0.60180       -1.97350       -1.08350
H          0.42520       -1.77130        0.66790
H         -1.73600       -2.04590       -0.33240
H         -1.41380       -0.66300       -1.49040
H         -1.15240       -0.29190        1.52280
H         -2.74260        0.02540        0.64720
H         -1.28640        2.06990        0.79850
H         -1.72050        1.54130       -0.83620
H          0.46930        1.63890       -1.36180
H          0.91420        1.90400        0.33200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

