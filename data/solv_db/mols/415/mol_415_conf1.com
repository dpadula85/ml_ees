%nproc=16
%mem=2GB
%chk=mol_415_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32410       -0.30040       -0.06730
N         -0.93760       -0.06830       -0.35700
C         -0.45940        1.20360       -0.10290
C          0.97390        1.38380        0.20210
C          1.85230        0.21910       -0.12600
C          1.22680       -1.09970        0.24040
C         -0.18660       -1.21790       -0.19270
H         -2.62370       -1.22880       -0.63690
H         -2.97630        0.51450       -0.49080
H         -2.47080       -0.43230        1.00960
H         -0.70770        1.87110       -0.98280
H         -1.04900        1.66840        0.74310
H          1.14030        1.62230        1.29070
H          1.41350        2.27410       -0.33420
H          2.08840        0.18470       -1.20650
H          2.79000        0.31130        0.48860
H          1.31060       -1.29030        1.35100
H          1.85000       -1.89370       -0.22520
H         -0.20490       -1.82190       -1.14960
H         -0.70570       -1.89980        0.54640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_415_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

