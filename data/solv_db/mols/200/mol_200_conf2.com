%nproc=16
%mem=2GB
%chk=mol_200_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.19500       -1.78000       -0.98830
N         -1.01450       -0.56420       -0.17990
C         -2.05270        0.44090       -0.19650
C         -2.05040        1.40450        0.93500
F         -0.92380        2.15080        1.09120
F         -2.39540        0.74880        2.13950
F         -3.10950        2.32700        0.79130
C          0.35600       -0.21490        0.02770
C          0.99030        0.75790       -0.72920
C          2.34290        0.99980       -0.64320
C          3.11660        0.24450        0.21070
C          2.48720       -0.71550        0.95590
C          1.13090       -0.95230        0.88290
H         -1.45500       -2.64210       -0.30850
H         -0.32210       -2.03080       -1.60040
H         -2.08410       -1.70620       -1.65670
H         -1.95240        1.06090       -1.15330
H         -3.05890       -0.01030       -0.30270
H          0.41800        1.36050       -1.42930
H          2.76750        1.80040       -1.23890
H          4.19340        0.45280        0.30530
H          3.10740       -1.32980        1.62950
H          0.70350       -1.80280        1.45800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

