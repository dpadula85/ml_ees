%nproc=16
%mem=2GB
%chk=mol_200_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.26470       -0.91710       -1.54700
N         -0.77400        0.11780       -0.63100
C         -1.67970        1.06920       -0.07820
C         -2.64730        0.44160        0.91090
F         -3.41630       -0.53390        0.34140
F         -3.43800        1.45170        1.41700
F         -1.91030       -0.07220        1.97910
C          0.57640        0.03830       -0.21130
C          1.12820        0.99480        0.61570
C          2.43730        0.85900        1.08660
C          3.15310       -0.24810        0.70090
C          2.61960       -1.21290       -0.12750
C          1.33890       -1.07870       -0.58730
H         -1.28800       -1.90630       -1.04570
H         -2.28780       -0.66880       -1.86530
H         -0.59070       -1.01650       -2.42450
H         -1.26030        1.96010        0.38610
H         -2.33560        1.44250       -0.93630
H          0.57920        1.90690        0.90300
H          2.82040        1.64450        1.72720
H          4.16950       -0.39060        1.04080
H          3.21350       -2.09360       -0.42090
H          0.85670       -1.78770       -1.23370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

