%nproc=16
%mem=2GB
%chk=mol_200_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21340        1.71430        0.57250
N         -0.83470        0.49270       -0.11120
C         -1.88510       -0.32150       -0.65940
C         -2.73620       -1.01360        0.38000
F         -3.68430       -1.75100       -0.32820
F         -3.30260       -0.19210        1.30070
F         -1.97280       -1.96410        1.07300
C          0.54280        0.14280       -0.12090
C          1.02210       -1.01320       -0.69640
C          2.37600       -1.33830       -0.73100
C          3.28610       -0.49030       -0.17820
C          2.84580        0.67310        0.41060
C          1.50560        0.98360        0.44010
H         -0.74110        2.55430       -0.02290
H         -2.29770        1.91990        0.54500
H         -0.75450        1.80510        1.57880
H         -1.53100       -1.02640       -1.41020
H         -2.63350        0.33010       -1.23080
H          0.29250       -1.72310       -1.09730
H          2.62840       -2.26290       -1.23130
H          4.33590       -0.76460       -0.22740
H          3.58730        1.33250        0.84530
H          1.16430        1.91280        0.89920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

