%nproc=16
%mem=2GB
%chk=mol_200_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97340       -0.49230       -1.89440
N         -0.74900        0.23870       -0.63560
C         -1.81490        0.90860        0.02460
C         -2.84580       -0.03890        0.60700
F         -3.81360        0.73770        1.21430
F         -3.49480       -0.75890       -0.38010
F         -2.25360       -0.83020        1.54390
C          0.53390        0.09280       -0.03980
C          1.47780       -0.77540       -0.61340
C          2.75270       -0.92870       -0.13260
C          3.14290       -0.19410        0.97690
C          2.25390        0.66620        1.56740
C          0.97150        0.80520        1.06220
H         -1.89030       -0.17650       -2.39200
H         -0.13300       -0.27710       -2.59050
H         -0.90550       -1.57270       -1.65670
H         -2.36300        1.61840       -0.66400
H         -1.46040        1.54580        0.83520
H          1.13560       -1.35540       -1.49020
H          3.41850       -1.61850       -0.63900
H          4.15390       -0.35180        1.32620
H          2.56040        1.22730        2.42680
H          0.29610        1.52980        1.54350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

