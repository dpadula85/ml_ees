%nproc=16
%mem=2GB
%chk=mol_200_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.84090       -1.34480        1.85750
N          0.85760       -0.49010        0.66860
C          2.14310       -0.08710        0.19270
C          2.14920        0.78260       -1.01410
F          1.56410        0.18120       -2.08600
F          1.63110        2.03860       -0.75010
F          3.49630        0.96770       -1.36140
C         -0.39480       -0.13820        0.12760
C         -1.01020        1.01780        0.56680
C         -2.25860        1.38470        0.08270
C         -2.92630        0.59650       -0.86040
C         -2.29720       -0.56190       -1.29360
C         -1.05840       -0.90920       -0.80150
H          1.70160       -2.00860        1.79760
H         -0.07750       -1.96350        1.84400
H          0.79060       -0.73470        2.78520
H          2.76310        0.38480        0.99340
H          2.70750       -1.02760       -0.07470
H         -0.54750        1.67770        1.29830
H         -2.75780        2.28730        0.41460
H         -3.89880        0.90800       -1.21760
H         -2.83680       -1.14890       -2.02200
H         -0.58090       -1.81220       -1.14750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_200_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

