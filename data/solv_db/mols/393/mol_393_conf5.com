%nproc=16
%mem=2GB
%chk=mol_393_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.00710       -1.49560       -0.02510
O         -2.33580       -0.67900        0.91040
C         -1.15010       -0.03000        0.58620
C          0.09210       -0.63230        0.79440
C          1.26590        0.01730        0.47040
C          1.14860        1.28310       -0.06580
C         -0.07030        1.90200       -0.28210
C         -1.22980        1.23570        0.04850
Cl        -2.75270        2.05630       -0.24170
C         -0.10900        3.23620       -0.85060
O          0.94820        3.83240       -1.14710
Cl         2.66230        2.09190       -0.47170
O          2.46340       -0.61460        0.69300
C          3.13970       -1.43700       -0.21210
O          0.12080       -1.88980        1.33060
H         -4.06010       -1.21300       -0.15920
H         -3.01130       -2.55210        0.36190
H         -2.50030       -1.52730       -1.01070
H         -1.03850        3.74540       -1.03140
H          3.67660       -0.78010       -0.93460
H          2.49860       -2.18490       -0.71230
H          3.94790       -1.96090        0.36630
H         -0.69890       -2.40370        1.58260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

