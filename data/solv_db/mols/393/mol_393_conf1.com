%nproc=16
%mem=2GB
%chk=mol_393_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.10280       -1.66280       -0.45790
O         -2.18520       -1.29490        0.54270
C         -1.12800       -0.40190        0.29530
C          0.10150       -0.89290       -0.13700
C          1.14080       -0.03190       -0.38170
C          0.97890        1.30400       -0.20440
C         -0.20420        1.81850        0.21550
C         -1.26950        0.95970        0.46860
Cl        -2.80990        1.61530        1.01380
C         -0.43130        3.24030        0.42080
O         -0.18680        3.71360        1.57450
Cl         2.31130        2.41280       -0.51690
O          2.36090       -0.55990       -0.81460
C          3.30250       -0.91830        0.17900
O          0.25530       -2.25680       -0.31430
H         -2.84510       -1.24180       -1.46030
H         -3.08670       -2.78600       -0.51780
H         -4.14310       -1.32280       -0.24240
H         -0.79410        3.88020       -0.37080
H          3.67350        0.04940        0.62390
H          2.77960       -1.49680        0.97770
H          4.13980       -1.50470       -0.26640
H          1.14260       -2.62240       -0.62730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

