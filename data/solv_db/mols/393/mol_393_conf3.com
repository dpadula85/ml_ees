%nproc=16
%mem=2GB
%chk=mol_393_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.36940       -0.88670        0.00040
O         -2.27430       -0.73580       -0.91150
C         -1.08810       -0.17720       -0.45880
C         -0.05740       -0.92070        0.07560
C          1.09540       -0.30060        0.50660
C          1.24580        1.06240        0.41570
C          0.24060        1.81290       -0.10830
C         -0.91710        1.20440       -0.54280
Cl        -2.22400        2.15360       -1.22080
C          0.37560        3.25640       -0.21450
O          0.87070        3.72510       -1.26790
Cl         2.75500        1.76320        0.99260
O          2.12360       -1.09120        1.04530
C          3.15120       -1.64150        0.24480
O         -0.21340       -2.29860        0.16570
H         -3.12650       -1.62430        0.79010
H         -4.24800       -1.17990       -0.58160
H         -3.57510        0.10030        0.49820
H          0.05690        3.89780        0.59310
H          3.99900       -0.91560        0.27260
H          2.75910       -1.80790       -0.76960
H          3.47840       -2.64140        0.62450
H         -1.05780       -2.75460       -0.14980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

