%nproc=16
%mem=2GB
%chk=mol_393_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.19480        2.51870       -0.64870
O         -1.85880        1.73750        0.45460
C         -1.06610        0.60380        0.42090
C          0.29300        0.73750        0.58730
C          1.15810       -0.34310        0.56740
C          0.62760       -1.60180        0.37230
C         -0.74220       -1.76290        0.20180
C         -1.59300       -0.65110        0.22660
Cl        -3.33020       -0.86840        0.00920
C         -1.32020       -3.07270       -0.00400
O         -1.68640       -3.74670        0.97810
Cl         1.69970       -3.00610        0.34040
O          2.52650       -0.22470        0.73320
C          3.44920       -0.03470       -0.29220
O          0.86460        1.99130        0.78640
H         -1.61970        3.49090       -0.62730
H         -1.99650        2.05480       -1.62070
H         -3.26650        2.84120       -0.53040
H         -1.43570       -3.46680       -0.99120
H          4.20860       -0.86680       -0.31470
H          4.03430        0.91000       -0.20690
H          2.89080       -0.08810       -1.26450
H          0.35770        2.84810        0.82220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

