%nproc=16
%mem=2GB
%chk=mol_393_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.35480       -1.07500        0.02530
O         -2.23640       -1.01430        0.88580
C         -1.12640       -0.28390        0.50340
C         -0.13570       -0.91530       -0.20980
C          0.98800       -0.19350       -0.60200
C          1.08430        1.14640       -0.26510
C          0.08220        1.77750        0.45410
C         -1.03540        1.04620        0.84030
Cl        -2.28330        1.87460        1.74830
C          0.21110        3.18820        0.79400
O          0.77410        3.52890        1.88900
Cl         2.49920        2.07020       -0.75450
O          2.01030       -0.78600       -1.32080
C          3.08630       -1.38820       -0.62360
O         -0.19350       -2.24620       -0.56470
H         -3.13330       -0.65520       -0.98140
H         -3.59180       -2.15770       -0.10300
H         -4.20530       -0.54880        0.47360
H         -0.13940        3.99510        0.18040
H          3.24310       -2.41100       -0.98610
H          4.03710       -0.84100       -0.76010
H          2.87030       -1.43600        0.46320
H          0.54920       -2.67500       -1.08650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_393_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

