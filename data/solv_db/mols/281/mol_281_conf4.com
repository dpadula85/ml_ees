%nproc=16
%mem=2GB
%chk=mol_281_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.48480       -0.89860        0.56060
C         -1.19680       -0.18920        0.34180
C         -1.13620        1.18820        0.17920
C          0.04740        1.85830       -0.02330
C          1.21540        1.11420       -0.06380
C          1.15960       -0.25320        0.09720
N         -0.03260       -0.84880        0.29210
C          2.41840       -1.07330        0.05560
H         -3.28760       -0.17340        0.74710
H         -2.70010       -1.52670       -0.32790
H         -2.39380       -1.61390        1.42620
H         -2.05810        1.77910        0.21060
H          0.14290        2.91700       -0.15350
H          2.19440        1.57230       -0.22020
H          2.29270       -1.93470       -0.62830
H          2.56340       -1.51050        1.07220
H          3.25570       -0.40700       -0.22410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

