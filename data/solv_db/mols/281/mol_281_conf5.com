%nproc=16
%mem=2GB
%chk=mol_281_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.40830       -1.11980       -0.00570
C          1.17520       -0.30130        0.02710
C          1.25660        1.06730        0.20570
C          0.09490        1.80890        0.23270
C         -1.15480        1.23200        0.08700
C         -1.17620       -0.14000       -0.08850
N         -0.04920       -0.87380       -0.11650
C         -2.48720       -0.83620       -0.25290
H          3.28300       -0.45200       -0.10230
H          2.42910       -1.82170       -0.87260
H          2.44120       -1.68810        0.94630
H          2.23840        1.53930        0.32250
H          0.15530        2.86910        0.37070
H         -2.07390        1.82730        0.10940
H         -2.66450       -1.14420       -1.30830
H         -3.33550       -0.20680        0.06770
H         -2.54080       -1.76020        0.37770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

