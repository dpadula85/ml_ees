%nproc=16
%mem=2GB
%chk=mol_281_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.52000       -0.77910        0.29790
C         -1.16700       -0.15210        0.09690
C         -1.13370        1.20630       -0.16480
C          0.07730        1.80960       -0.35300
C          1.21510        1.01950       -0.27050
C          1.15440       -0.33580       -0.00770
N         -0.04960       -0.88850        0.17040
C          2.41320       -1.15080        0.07220
H         -3.31180       -0.11600       -0.05320
H         -2.51510       -1.71740       -0.30270
H         -2.66590       -1.02480        1.34920
H         -2.02780        1.78000       -0.22070
H          0.14720        2.86620       -0.55930
H          2.21380        1.43250       -0.40970
H          2.31210       -2.01720       -0.58060
H          3.26130       -0.50690       -0.21440
H          2.59640       -1.42530        1.14990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

