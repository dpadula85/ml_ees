%nproc=16
%mem=2GB
%chk=mol_281_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.20860       -1.39210       -0.17270
C         -1.11410       -0.39110       -0.05960
C         -1.42210        0.94820        0.05580
C         -0.36620        1.82830        0.15720
C          0.94240        1.33430        0.13890
C          1.20460       -0.00900        0.02220
N          0.16330       -0.85240       -0.07550
C          2.58920       -0.54910        0.00120
H         -3.08280       -1.07420        0.43150
H         -2.43620       -1.52640       -1.24180
H         -1.87050       -2.37360        0.20830
H         -2.45860        1.27150        0.06420
H         -0.57710        2.88360        0.24880
H          1.78760        2.01500        0.21790
H          3.17310       -0.16130        0.87640
H          2.56500       -1.64160        0.08930
H          3.11080       -0.31000       -0.96220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

