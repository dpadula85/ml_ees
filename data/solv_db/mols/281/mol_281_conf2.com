%nproc=16
%mem=2GB
%chk=mol_281_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.46100       -1.00710        0.04590
C         -1.17850       -0.24530        0.04040
C         -1.13220        1.13380        0.13970
C          0.04570        1.85090        0.13580
C          1.23020        1.15770        0.02770
C          1.16320       -0.21250       -0.06990
N         -0.00140       -0.88740       -0.06350
C          2.44410       -0.97090       -0.18780
H         -2.80920       -1.19860       -0.98660
H         -2.33500       -2.00620        0.53870
H         -3.27130       -0.45550        0.56350
H         -2.11310        1.62220        0.22290
H          0.03450        2.93370        0.21600
H          2.18000        1.70160        0.02210
H          2.23350       -2.07280       -0.18670
H          3.07510       -0.67810        0.69220
H          2.89530       -0.66550       -1.15030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_281_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

