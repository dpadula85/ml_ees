%nproc=16
%mem=2GB
%chk=mol_272_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.14040       -0.00230       -0.00330
Cl         1.63980        0.02670       -0.03180
H         -0.51520       -0.60980       -0.84550
H         -0.45120       -0.44360        0.95160
H         -0.53300        1.02900       -0.07090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_272_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_272_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_272_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

