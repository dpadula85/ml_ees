%nproc=16
%mem=2GB
%chk=mol_058_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.59870       -0.77690       -0.22870
C         -1.43040        0.16720       -0.17750
C         -1.44140        1.54220       -0.28960
N         -0.18420        1.98130       -0.18850
C          0.66910        0.94290       -0.01190
C         -0.11070       -0.19500       -0.00480
C          0.54970       -1.39920        0.16400
C          1.91180       -1.48240        0.31870
C          2.68790       -0.34330        0.31110
C          2.03860        0.86230        0.14340
H         -2.83400       -1.00910        0.83760
H         -3.47400       -0.30330       -0.67440
H         -2.25610       -1.69750       -0.71340
H         -2.34780        2.11210       -0.43400
H          0.13660        2.96690       -0.23330
H         -0.09270       -2.28410        0.16600
H          2.37690       -2.45780        0.44770
H          3.78120       -0.39410        0.43430
H          2.61830        1.76790        0.13340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

