%nproc=16
%mem=2GB
%chk=mol_058_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.52490        0.90310       -0.21260
C         -1.41130       -0.07040       -0.04750
C         -1.53020       -1.43510        0.12340
N         -0.28850       -1.98350        0.23910
C          0.64220       -0.99800        0.14650
C         -0.05520        0.18320       -0.03060
C          0.61510        1.38640       -0.16090
C          1.99660        1.37720       -0.10960
C          2.67610        0.19050        0.06750
C          2.03130       -1.02310        0.20020
H         -2.92420        0.90600       -1.24600
H         -2.21610        1.93010        0.08880
H         -3.38010        0.65310        0.48250
H         -2.48870       -1.93110        0.15190
H         -0.08700       -3.00180        0.37730
H          0.09380        2.33000       -0.30100
H          2.51110        2.33820       -0.21460
H          3.77390        0.20780        0.10500
H          2.56630       -1.96260        0.34040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

