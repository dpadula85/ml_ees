%nproc=16
%mem=2GB
%chk=mol_058_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.48920       -0.95310        0.18110
C         -1.39060        0.03980        0.08380
C         -1.60540        1.40330        0.04950
N         -0.38570        1.96650       -0.04020
C          0.57240        1.00260       -0.06250
C         -0.03910       -0.23940        0.01540
C          0.71210       -1.40080        0.01170
C          2.08890       -1.37380       -0.06880
C          2.70270       -0.13520       -0.14680
C          1.94840        1.03580       -0.14330
H         -3.11490       -0.79500        1.08490
H         -3.19390       -0.82190       -0.68730
H         -2.13130       -1.99090        0.13670
H         -2.56630        1.93100        0.08690
H         -0.24600        3.00060       -0.08370
H          0.21630       -2.35020        0.07290
H          2.70700       -2.25660       -0.07420
H          3.78900       -0.08010       -0.21150
H          2.42550        2.01740       -0.20460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

