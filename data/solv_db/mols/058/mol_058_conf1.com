%nproc=16
%mem=2GB
%chk=mol_058_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.62920       -0.72750        0.03290
C         -1.43400        0.17850       -0.08670
C         -1.42030        1.53760       -0.32350
N         -0.14260        1.95860       -0.35560
C          0.71110        0.92770       -0.14800
C         -0.10410       -0.17540        0.01820
C          0.49480       -1.39910        0.25120
C          1.87060       -1.48190        0.31030
C          2.70260       -0.39080        0.14680
C          2.08610        0.81940       -0.08450
H         -3.52940       -0.10080       -0.05400
H         -2.64440       -1.15560        1.07700
H         -2.56350       -1.55520       -0.67110
H         -2.30520        2.13700       -0.45690
H          0.21210        2.92710       -0.51300
H         -0.15380       -2.25350        0.37940
H          2.34030       -2.43420        0.49160
H          3.78850       -0.49520        0.20040
H          2.72040        1.68320       -0.21470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

