%nproc=16
%mem=2GB
%chk=mol_058_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.57460        0.88470        0.13810
C          1.43840       -0.08720        0.09250
C          1.50790       -1.47410        0.14240
N          0.26120       -1.97790        0.07390
C         -0.64780       -0.98060       -0.02050
C          0.08810        0.18650       -0.00840
C         -0.61680        1.37980       -0.09530
C         -1.99170        1.40620       -0.18970
C         -2.71220        0.23260       -0.20060
C         -2.03720       -0.96500       -0.11560
H          3.11810        0.93900       -0.81270
H          3.29610        0.59410        0.95220
H          2.16740        1.87980        0.46730
H          2.43370       -2.01130        0.22290
H         -0.01950       -2.99360        0.08790
H         -0.00760        2.28630       -0.08330
H         -2.49420        2.35000       -0.25460
H         -3.80290        0.25260       -0.27570
H         -2.55590       -1.90200       -0.12070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_058_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

