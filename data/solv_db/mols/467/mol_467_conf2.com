%nproc=16
%mem=2GB
%chk=mol_467_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.02770       -1.08510       -0.12950
C         -1.63930        0.16310       -0.07040
C         -0.89700        1.31600        0.04960
C          0.47480        1.20760        0.11060
C          1.08040       -0.03850        0.05140
C          0.34490       -1.19240       -0.06840
O          2.44780       -0.09700        0.11570
F          1.19600        2.35770        0.22940
H         -1.63320       -1.99540       -0.22540
H         -2.70420        0.23130       -0.11870
H         -1.35640        2.30040        0.09740
H          0.81680       -2.16890       -0.11490
H          2.89720       -0.99890        0.07340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_467_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_467_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_467_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

