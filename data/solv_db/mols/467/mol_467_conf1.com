%nproc=16
%mem=2GB
%chk=mol_467_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.16260       -1.06390       -0.27280
C         -1.68070        0.18530        0.00440
C         -0.79860        1.20020        0.25950
C          0.56330        1.00550        0.24560
C          1.08560       -0.24240       -0.03120
C          0.20240       -1.26960       -0.28920
O          2.45030       -0.47450       -0.05350
F          1.41210        2.02160        0.49970
H         -1.81580       -1.90440       -0.48170
H         -2.76900        0.31550        0.00880
H         -1.22200        2.18150        0.47720
H          0.64500       -2.24880       -0.50600
H          3.09000        0.29400        0.13910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_467_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_467_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_467_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

