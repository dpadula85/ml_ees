%nproc=16
%mem=2GB
%chk=mol_373_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.41840        0.78520        0.92150
C         -2.54350       -0.02360       -0.34890
C         -1.29450        0.04830       -1.19390
C         -0.08450       -0.49790       -0.40750
O          0.97290       -0.38250       -1.26540
C          2.19070       -0.81810       -0.85550
C          2.63390       -0.05790        0.36830
O          2.70900        1.29550        0.07950
H         -3.45320        0.85940        1.35440
H         -2.03540        1.77720        0.65370
H         -1.79110        0.23980        1.65180
H         -3.35840        0.42230       -0.93810
H         -2.81280       -1.04690       -0.08810
H         -1.05860        1.08140       -1.44260
H         -1.41780       -0.58730       -2.06950
H          0.04890        0.16180        0.50100
H         -0.23630       -1.53800       -0.10880
H          2.93630       -0.69240       -1.69580
H          2.14610       -1.91030       -0.62760
H          3.61500       -0.44930        0.74080
H          1.86990       -0.20190        1.15020
H          3.38180        1.53500       -0.58120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

