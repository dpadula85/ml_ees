%nproc=16
%mem=2GB
%chk=mol_373_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47720        1.12050       -0.00750
C         -2.50470       -0.31370        0.41770
C         -1.16650       -0.74580        0.96800
C         -0.07130       -0.58440       -0.09820
O          1.09640       -0.99410        0.47850
C          2.22650       -0.94690       -0.28150
C          2.52380        0.44520       -0.74300
O          2.70200        1.26900        0.37710
H         -2.60300        1.13910       -1.12800
H         -3.33170        1.70930        0.40070
H         -1.56670        1.66400        0.25120
H         -2.81000       -0.99930       -0.39080
H         -3.25020       -0.41860        1.22960
H         -1.22600       -1.82020        1.20490
H         -0.87310       -0.10270        1.79970
H         -0.07940        0.45960       -0.45480
H         -0.39940       -1.23510       -0.94590
H          2.26030       -1.68920       -1.10530
H          3.07970       -1.24310        0.39840
H          1.70390        0.83590       -1.34680
H          3.49890        0.42630       -1.27310
H          3.26790        2.02440        0.08660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

