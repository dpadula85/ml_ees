%nproc=16
%mem=2GB
%chk=mol_373_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.05820       -0.17040        0.27480
C          2.51690        0.35900       -1.02240
C          1.08030        0.78120       -0.93170
C          0.15100       -0.32450       -0.53900
O         -1.13380        0.18340       -0.48950
C         -2.07740       -0.76900       -0.13200
C         -3.44110       -0.11060       -0.10830
O         -3.45810        0.94500        0.82740
H          2.99940        0.59550        1.08310
H          4.16140       -0.33180        0.11470
H          2.62750       -1.13060        0.58430
H          2.64360       -0.40600       -1.80310
H          3.10880        1.27040       -1.27290
H          0.78290        1.14870       -1.94120
H          0.95480        1.60650       -0.20620
H          0.20990       -1.20540       -1.20290
H          0.41990       -0.64760        0.48540
H         -1.91240       -1.17990        0.89160
H         -2.14510       -1.59130       -0.88360
H         -3.70260        0.35590       -1.08000
H         -4.24030       -0.82340        0.17940
H         -2.60360        1.44480        0.74070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

