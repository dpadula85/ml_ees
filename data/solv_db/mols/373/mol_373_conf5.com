%nproc=16
%mem=2GB
%chk=mol_373_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.97690        0.29460       -0.70460
C          1.87910       -0.20770        0.17990
C          0.67920       -0.63580       -0.62730
C         -0.37820       -1.12770        0.34490
O         -0.74600       -0.13790        1.22880
C         -1.27610        0.98990        0.64220
C         -2.52650        0.70690       -0.16600
O         -3.53510        0.16350        0.61530
H          2.90770       -0.25220       -1.67140
H          2.87590        1.38350       -0.90170
H          3.93990        0.10930       -0.18500
H          2.22090       -1.11780        0.73720
H          1.52510        0.54820        0.90920
H          1.03840       -1.48590       -1.27320
H          0.27280        0.13880       -1.28470
H         -1.22410       -1.50600       -0.26860
H         -0.02080       -1.99120        0.93790
H         -0.55220        1.50000       -0.02210
H         -1.53720        1.68280        1.49440
H         -2.88280        1.67550       -0.56960
H         -2.26470        0.07600       -1.03240
H         -3.37200       -0.80690        0.81010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

