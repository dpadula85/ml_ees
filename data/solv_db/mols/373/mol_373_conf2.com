%nproc=16
%mem=2GB
%chk=mol_373_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.15960        0.94780        0.66820
C         -2.19850       -0.48400        0.24900
C         -1.46440       -0.76320       -1.03510
C          0.00860       -0.39840       -0.93590
O          0.65960       -1.08190        0.02920
C          1.98640       -0.90980        0.22340
C          2.50080        0.43050        0.58810
O          2.26470        1.43240       -0.32690
H         -1.59480        1.60580       -0.02660
H         -1.75730        1.05210        1.70250
H         -3.18890        1.41030        0.72180
H         -3.25840       -0.76740        0.08160
H         -1.82460       -1.13570        1.08050
H         -1.53790       -1.83490       -1.24380
H         -1.90400       -0.18180       -1.84220
H          0.44970       -0.60090       -1.93280
H          0.07640        0.70490       -0.77300
H          2.50320       -1.22730       -0.73690
H          2.38020       -1.61260        1.02950
H          2.07010        0.80500        1.56260
H          3.60610        0.31340        0.79990
H          2.38270        2.29580        0.11710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_373_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

