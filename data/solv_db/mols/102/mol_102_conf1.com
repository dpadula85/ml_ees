%nproc=16
%mem=2GB
%chk=mol_102_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.01170        1.18270        0.82670
C         -1.93750       -0.31520        0.53560
C         -0.99320       -0.54430       -0.62320
C         -0.94200       -2.03200       -0.88540
C          0.37900       -0.04210       -0.31250
C          0.93810        0.93890       -1.09930
C          2.21980        1.42670       -0.83110
C          2.94610        0.92850        0.23220
C          2.39780       -0.04820        1.02090
C          1.11320       -0.52970        0.74340
H         -2.09680        1.73640       -0.14550
H         -1.08220        1.48740        1.34770
H         -2.88020        1.38960        1.47810
H         -1.59390       -0.80330        1.46100
H         -2.94230       -0.62480        0.22000
H         -1.41150       -0.02500       -1.50880
H         -0.99950       -2.55300        0.09340
H          0.00580       -2.31640       -1.35650
H         -1.79640       -2.36610       -1.50450
H          0.41160        1.36000       -1.94030
H          2.67600        2.18830       -1.42740
H          3.93950        1.29320        0.45570
H          2.96420       -0.44040        1.85440
H          0.69620       -1.29130        1.36530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

