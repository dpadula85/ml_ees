%nproc=16
%mem=2GB
%chk=mol_102_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.08770        0.14920        0.49860
C         -1.61240        0.31340        0.74090
C         -0.81380       -0.02670       -0.52190
C         -1.06870       -1.44510       -0.93600
C          0.63250        0.20810       -0.26580
C          1.27920       -0.54370        0.71040
C          2.61700       -0.38140        1.00230
C          3.31210        0.56160        0.29040
C          2.70210        1.31770       -0.67760
C          1.37720        1.13440       -0.94400
H         -3.27850        0.21130       -0.59990
H         -3.44910       -0.84580        0.85440
H         -3.66230        0.98590        0.97260
H         -1.30400       -0.37030        1.55920
H         -1.40630        1.35640        1.04580
H         -1.15250        0.65810       -1.32480
H         -1.09810       -1.57350       -2.02120
H         -2.03370       -1.77660       -0.45420
H         -0.30280       -2.15780       -0.53560
H          0.74690       -1.29420        1.28420
H          3.11760       -0.96910        1.76380
H          4.35550        0.71260        0.49360
H          3.25110        2.05210       -1.22940
H          0.87850        1.72320       -1.70600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

