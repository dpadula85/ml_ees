%nproc=16
%mem=2GB
%chk=mol_102_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.95130       -0.00790        0.96570
C         -1.45070       -0.11320        0.99960
C         -0.82780        0.29440       -0.34320
C         -1.38110       -0.63980       -1.37820
C          0.62560        0.23010       -0.20650
C          1.29080       -0.91830        0.11460
C          2.67590       -0.97990        0.24520
C          3.41150        0.17270        0.04080
C          2.76510        1.34380       -0.28390
C          1.37510        1.36950       -0.40660
H         -3.39490       -1.02470        1.08110
H         -3.31940        0.47050        0.05430
H         -3.32880        0.59460        1.82190
H         -1.09600       -1.10750        1.27200
H         -1.07280        0.62060        1.75120
H         -1.16330        1.31780       -0.60780
H         -1.47840       -1.65650       -0.94970
H         -2.40140       -0.27760       -1.64160
H         -0.80290       -0.62870       -2.33470
H          0.69770       -1.81510        0.27180
H          3.16100       -1.91310        0.50150
H          4.48150        0.11290        0.14450
H          3.32030        2.24650       -0.44570
H          0.86460        2.30890       -0.66650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

