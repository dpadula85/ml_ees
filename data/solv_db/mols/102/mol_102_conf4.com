%nproc=16
%mem=2GB
%chk=mol_102_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.27920        1.10730       -0.57060
C         -2.00360       -0.10780        0.23320
C         -0.80340       -0.89180       -0.19320
C         -0.70760       -2.09060        0.76260
C          0.46270       -0.18750       -0.12670
C          0.90200        0.42580        1.01750
C          2.12860        1.08940        1.02560
C          2.93360        1.15430       -0.09430
C          2.47780        0.53290       -1.23610
C          1.26510       -0.12060       -1.23640
H         -1.49370        1.39570       -1.28260
H         -3.22050        0.99880       -1.18160
H         -2.51300        1.96120        0.12730
H         -1.92960        0.13610        1.32780
H         -2.88520       -0.79670        0.16030
H         -0.96380       -1.31190       -1.20670
H         -1.63450       -2.67210        0.63110
H         -0.74300       -1.63460        1.78790
H          0.21100       -2.65680        0.57860
H          0.30630        0.40740        1.93600
H          2.50730        1.58640        1.91570
H          3.89580        1.68300       -0.06320
H          3.10750        0.58060       -2.12170
H          0.97940       -0.58840       -2.19060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

