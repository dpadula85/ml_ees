%nproc=16
%mem=2GB
%chk=mol_102_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.89270       -1.52770        0.57980
C         -1.98350       -0.25250       -0.20900
C         -0.95800        0.75990        0.20250
C         -1.16490        1.99590       -0.67460
C          0.41740        0.26330        0.11820
C          0.93220       -0.19900       -1.07480
C          2.24090       -0.66160       -1.11370
C          3.04500       -0.67570       -0.00240
C          2.52240       -0.21140        1.18710
C          1.21720        0.25360        1.24150
H         -1.17860       -1.39060        1.41530
H         -2.88390       -1.86220        0.94940
H         -1.50650       -2.37160       -0.04300
H         -2.98170        0.20360       -0.06980
H         -1.89110       -0.49450       -1.28620
H         -1.19290        1.08660        1.23460
H         -1.68940        1.71200       -1.61400
H         -1.84040        2.73140       -0.17040
H         -0.18680        2.48710       -0.90880
H          0.34590       -0.21650       -2.00100
H          2.63430       -1.02210       -2.05550
H          4.06670       -1.02600        0.01240
H          3.10520       -0.19700        2.09580
H          0.82330        0.61500        2.18660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_102_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

