%nproc=16
%mem=2GB
%chk=mol_404_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06730       -0.83690        0.14130
C         -0.19980        0.39210        0.02900
C          1.26340        0.00180        0.09050
F          1.62140       -0.85360       -0.92060
F          2.03180        1.15730       -0.03300
F          1.55370       -0.56660        1.30450
O         -0.49120        1.08240       -1.13730
H         -1.95750       -0.62310        0.78550
H         -0.53330       -1.67090        0.64190
H         -1.45930       -1.16360       -0.85330
H         -0.39550        1.02480        0.92870
H         -0.36650        2.05630       -0.97720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

