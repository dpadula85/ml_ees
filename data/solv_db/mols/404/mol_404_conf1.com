%nproc=16
%mem=2GB
%chk=mol_404_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21670       -0.54090        0.06690
C         -0.14960        0.48180        0.26280
C          1.21920       -0.19000        0.12440
F          1.36730       -1.18380        1.06760
F          1.29740       -0.82520       -1.12100
F          2.16480        0.79590        0.25030
O         -0.23800        1.57550       -0.56690
H         -1.94320       -0.16910       -0.68150
H         -1.79260       -0.74620        0.99620
H         -0.83600       -1.51300       -0.34120
H         -0.19730        0.85320        1.31310
H          0.32460        1.46190       -1.37070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

