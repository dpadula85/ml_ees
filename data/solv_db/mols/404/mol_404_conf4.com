%nproc=16
%mem=2GB
%chk=mol_404_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.13350       -0.53240        0.45700
C         -0.05540        0.39800       -0.02250
C          1.29740       -0.27910       -0.10220
F          1.24700       -1.34190       -0.97440
F          1.72680       -0.69560        1.14230
F          2.19770        0.67440       -0.57770
O         -0.41130        0.96290       -1.23420
H         -1.72810       -0.10910        1.29220
H         -0.70300       -1.47590        0.82270
H         -1.86520       -0.77300       -0.34630
H          0.00560        1.23080        0.71720
H         -0.57800        1.94080       -1.17420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

