%nproc=16
%mem=2GB
%chk=mol_404_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15830       -0.56990        0.18690
C         -0.03420        0.43110        0.16050
C          1.30380       -0.25100       -0.06020
F          1.28080       -0.92050       -1.26430
F          2.30270        0.70140       -0.02540
F          1.57550       -1.17380        0.91820
O         -0.26250        1.25990       -0.95500
H         -1.92360       -0.38550       -0.58040
H         -1.57120       -0.59270        1.21520
H         -0.74170       -1.58980       -0.01540
H         -0.01230        1.03770        1.08570
H         -0.75900        2.05300       -0.66580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

