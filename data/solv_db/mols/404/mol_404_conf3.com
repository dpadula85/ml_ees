%nproc=16
%mem=2GB
%chk=mol_404_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.32210       -0.34550        0.25660
C          0.09020        0.17040       -0.48390
C         -1.18410       -0.29960        0.21360
F         -1.18880        0.18700        1.50520
F         -2.25220        0.10700       -0.52910
F         -1.07790       -1.68410        0.29920
O          0.11590        1.57520       -0.46220
H          2.17020        0.25840       -0.10220
H          1.17270       -0.22910        1.33610
H          1.50980       -1.39290       -0.01690
H          0.14740       -0.23170       -1.50420
H         -0.82530        1.88480       -0.51210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_404_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

