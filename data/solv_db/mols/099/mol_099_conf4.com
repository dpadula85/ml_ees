%nproc=16
%mem=2GB
%chk=mol_099_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.93320        1.30280        0.05070
C         -0.25990       -0.05150        0.26730
C         -1.09370       -1.07930       -0.47180
C          1.14170       -0.03380       -0.23720
C          2.17690       -0.28550        0.51990
H         -1.97660        1.17510        0.44570
H         -0.43990        2.08350        0.68910
H         -0.93760        1.58360       -1.01450
H         -0.30250       -0.21650        1.36350
H         -1.98750       -1.30470        0.14310
H         -1.40050       -0.61510       -1.45250
H         -0.48500       -1.98270       -0.67310
H          1.30250        0.19910       -1.27990
H          3.15500       -0.25540        0.09260
H          2.04040       -0.51960        1.55730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

