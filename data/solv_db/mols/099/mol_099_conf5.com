%nproc=16
%mem=2GB
%chk=mol_099_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.01900        1.01060        0.77900
C         -0.24560       -0.26820        0.53260
C         -0.78680       -0.89190       -0.73200
C          1.19020        0.10120        0.39460
C          1.88900       -0.09630       -0.69790
H         -0.37010        1.84330        1.09100
H         -1.52580        1.35260       -0.16990
H         -1.85060        0.80670        1.48520
H         -0.36940       -0.94220        1.39730
H         -0.58360       -0.24290       -1.58860
H         -0.43120       -1.92670       -0.83360
H         -1.89770       -0.94700       -0.63150
H          1.64750        0.55490        1.26380
H          2.91980        0.19240       -0.73330
H          1.43320       -0.54640       -1.55690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

