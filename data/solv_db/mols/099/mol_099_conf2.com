%nproc=16
%mem=2GB
%chk=mol_099_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.28940       -1.07090       -0.08730
C         -0.30970        0.01320       -0.47760
C         -0.56320        1.28920        0.23940
C          1.06110       -0.52550       -0.29030
C          1.99630        0.02900        0.42650
H         -2.29320       -0.60890       -0.03230
H         -0.96850       -1.54250        0.84740
H         -1.30710       -1.81020       -0.91890
H         -0.45200        0.20980       -1.57760
H         -0.23520        2.18390       -0.36570
H         -1.68240        1.41560        0.31320
H         -0.09010        1.35740        1.24360
H          1.27380       -1.45610       -0.80210
H          1.89350        0.95970        0.98130
H          2.96610       -0.44360        0.50040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

