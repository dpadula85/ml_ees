%nproc=16
%mem=2GB
%chk=mol_099_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17880       -1.00060        0.64700
C         -0.56030       -0.03050       -0.33500
C         -0.38680        1.33950        0.26100
C          0.77720       -0.57280       -0.69440
C          1.90450        0.08210       -0.50140
H         -0.38990       -1.58880        1.19560
H         -1.74090       -1.78230        0.06210
H         -1.80270       -0.51100        1.39970
H         -1.14990        0.00270       -1.27540
H          0.28940        1.25710        1.13580
H          0.06450        1.98850       -0.51280
H         -1.36280        1.72250        0.62020
H          0.77570       -1.57360       -1.14070
H          1.94050        1.06380       -0.06670
H          2.82010       -0.39640       -0.79500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

