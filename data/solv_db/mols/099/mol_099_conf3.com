%nproc=16
%mem=2GB
%chk=mol_099_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38800       -0.85270        0.07100
C         -0.27310        0.09350       -0.34730
C         -0.55800        1.48680        0.22830
C          0.99390       -0.36230        0.29220
C          2.07280       -0.65550       -0.41230
H         -0.94810       -1.79950        0.45320
H         -2.04850       -1.11830       -0.77590
H         -2.02270       -0.41370        0.87200
H         -0.19600        0.18190       -1.43480
H         -1.39450        1.97820       -0.28480
H         -0.68080        1.40670        1.32410
H          0.38590        2.06170        0.04200
H          1.00000       -0.44630        1.37160
H          2.08760       -0.57880       -1.48480
H          2.96960       -0.98160        0.08570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_099_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

