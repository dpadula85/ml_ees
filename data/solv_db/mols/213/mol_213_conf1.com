%nproc=16
%mem=2GB
%chk=mol_213_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.28560        0.31180        0.18390
C          1.44050       -0.17500       -0.97060
C          0.17970       -0.84590       -0.52030
C         -0.71680        0.04060        0.28750
C         -1.94680       -0.79790        0.66080
C         -1.19180        1.24860       -0.45240
O         -0.09320        0.47870        1.46260
H          3.33570        0.40840       -0.22080
H          2.23890       -0.34250        1.05740
H          1.97810        1.35300        0.45240
H          1.25670        0.63010       -1.68700
H          2.05040       -0.94350       -1.50190
H          0.48380       -1.69450        0.15000
H         -0.36320       -1.20860       -1.39540
H         -2.65730       -0.22740        1.26130
H         -1.53800       -1.68150        1.21320
H         -2.36870       -1.18440       -0.29660
H         -1.20930        1.12140       -1.54800
H         -2.21670        1.48460       -0.09770
H         -0.57820        2.16660       -0.21820
H         -0.36930       -0.14280        2.17990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

