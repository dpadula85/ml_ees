%nproc=16
%mem=2GB
%chk=mol_213_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73450        0.06970       -0.08350
C          1.28160        0.36630       -0.38110
C          0.42800       -0.48240        0.54730
C         -1.03880       -0.25350        0.32310
C         -1.40690        1.19730        0.56720
C         -1.37240       -0.69760       -1.07490
O         -1.73180       -1.07500        1.21040
H          3.38470        0.96630       -0.19160
H          2.86340       -0.30460        0.94320
H          3.03720       -0.74380       -0.78900
H          1.07570        1.45630       -0.22610
H          1.01790        0.16390       -1.43780
H          0.63530       -1.54810        0.25560
H          0.68310       -0.30730        1.60670
H         -2.49940        1.31280        0.72150
H         -0.83170        1.53300        1.46270
H         -1.12250        1.84680       -0.27140
H         -2.40220       -1.15860       -1.07810
H         -1.35240        0.12790       -1.80990
H         -0.67820       -1.50120       -1.36980
H         -2.70510       -0.96820        1.07550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

