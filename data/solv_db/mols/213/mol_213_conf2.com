%nproc=16
%mem=2GB
%chk=mol_213_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21410       -0.23680       -0.56660
C          1.59040       -0.23590        0.81310
C          0.36440        0.62940        0.88460
C         -0.71810        0.19650       -0.05890
C         -1.18970       -1.21480        0.21030
C         -1.92540        1.10400        0.21490
O         -0.33220        0.29210       -1.39180
H          2.24770        0.77510       -1.00250
H          3.28960       -0.53590       -0.41220
H          1.76970       -0.98810       -1.23570
H          2.32510        0.15740        1.53890
H          1.34460       -1.30530        1.04400
H          0.66720        1.65420        0.59580
H         -0.01860        0.61880        1.90620
H         -2.29150       -1.28480        0.02890
H         -0.95370       -1.51120        1.23780
H         -0.69970       -1.94720       -0.46740
H         -1.60540        2.13470        0.45490
H         -2.59540        1.02180       -0.64150
H         -2.38010        0.69630        1.16320
H         -1.10300       -0.02030       -1.93870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

