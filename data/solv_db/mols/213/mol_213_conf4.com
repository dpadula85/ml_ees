%nproc=16
%mem=2GB
%chk=mol_213_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.81020        0.06000       -0.00850
C          1.42470        0.32090        0.60230
C          0.44090       -0.40130       -0.30320
C         -0.97910       -0.23750        0.17140
C         -1.28400        1.25780        0.16730
C         -1.91210       -0.96430       -0.77030
O         -1.18660       -0.69770        1.44430
H          3.60730        0.49610        0.59600
H          2.87870       -1.06220       -0.07400
H          2.74740        0.46590       -1.04400
H          1.29620        1.42120        0.54220
H          1.39500       -0.05600        1.63560
H          0.56420        0.02880       -1.31760
H          0.68830       -1.47660       -0.39860
H         -2.36780        1.45760        0.10070
H         -0.77330        1.72960       -0.71320
H         -0.80790        1.69300        1.06940
H         -1.31200       -1.66290       -1.39900
H         -2.64100       -1.59530       -0.22720
H         -2.45840       -0.26260       -1.44240
H         -2.13050       -0.51450        1.68130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

