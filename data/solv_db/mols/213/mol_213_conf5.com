%nproc=16
%mem=2GB
%chk=mol_213_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.31200        0.47250       -0.09950
C          1.51740       -0.37010        0.85520
C          0.28500       -0.97240        0.22840
C         -0.66980        0.04440       -0.29280
C         -1.85600       -0.72410       -0.89170
C         -1.26200        0.91410        0.79560
O         -0.07090        0.81550       -1.29950
H          1.98750        1.52610       -0.14250
H          3.39300        0.46420        0.21820
H          2.29910        0.07170       -1.14630
H          2.16400       -1.16990        1.26020
H          1.22860        0.26290        1.71740
H         -0.20900       -1.58710        1.00090
H          0.54920       -1.65430       -0.60590
H         -1.56890       -1.32420       -1.75280
H         -2.63010        0.04190       -1.09240
H         -2.26650       -1.35850       -0.06600
H         -2.06170        0.34220        1.32730
H         -0.54640        1.25040        1.53830
H         -1.78950        1.75590        0.29350
H         -0.80500        1.19870       -1.84580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_213_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

