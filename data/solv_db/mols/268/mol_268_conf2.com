%nproc=16
%mem=2GB
%chk=mol_268_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.19170        0.00350       -0.02090
I          1.94440       -0.00840        0.03050
H         -0.55420        0.93700       -0.50120
H         -0.62580       -0.01630        1.00950
H         -0.57270       -0.91590       -0.51790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_268_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_268_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_268_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

