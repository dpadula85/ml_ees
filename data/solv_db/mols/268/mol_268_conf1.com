%nproc=16
%mem=2GB
%chk=mol_268_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.20540        0.00160       -0.01000
I          1.93000        0.00120        0.01660
H         -0.60230       -0.05600        1.02280
H         -0.55820        0.94620       -0.45790
H         -0.56400       -0.89290       -0.57150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_268_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_268_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_268_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

