%nproc=16
%mem=2GB
%chk=mol_285_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.11130        0.72130        0.19870
C         -2.68190       -0.13160       -0.79700
C         -1.46720       -0.76370       -0.72650
C         -0.62110       -0.57240        0.34650
C         -1.02290        0.27400        1.35630
C         -2.24330        0.89790        1.26750
C          0.68910       -1.25840        0.41580
N          1.69970       -0.40220       -0.22370
C          2.43190        0.52820        0.39940
C          3.27130        1.12720       -0.52390
N          2.99450        0.51120       -1.68490
C          2.04480       -0.41720       -1.51820
H         -4.07010        1.22040        0.14330
H         -3.31460       -0.31910       -1.67150
H         -1.13880       -1.43770       -1.52300
H         -0.40610        0.46590        2.22350
H         -2.60610        1.57860        2.04400
H          0.60550       -2.23360       -0.06760
H          0.97900       -1.39850        1.47360
H          2.35710        0.75460        1.46140
H          3.97570        1.91550       -0.29860
H          1.63500       -1.06030       -2.29530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

