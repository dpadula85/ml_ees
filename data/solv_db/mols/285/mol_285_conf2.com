%nproc=16
%mem=2GB
%chk=mol_285_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.01980        1.04630       -0.50910
C          2.53420        0.90570        0.77490
C          1.41920        0.13580        1.03710
C          0.74280       -0.52510        0.03080
C          1.23410       -0.38080       -1.25630
C          2.34780        0.38800       -1.51930
C         -0.46330       -1.36020        0.30200
N         -1.67700       -0.55400        0.21360
C         -2.29830        0.13260        1.18530
C         -3.41030        0.75040        0.65490
N         -3.40360        0.39270       -0.65720
C         -2.36310       -0.39240       -0.92940
H          3.90260        1.65880       -0.69880
H          3.07520        1.43140        1.56350
H          1.06020        0.04450        2.06140
H          0.68480       -0.91020       -2.02890
H          2.69750        0.47540       -2.52830
H         -0.45960       -2.16970       -0.45540
H         -0.38550       -1.83520        1.29750
H         -1.99260        0.20200        2.22050
H         -4.14500        1.38240        1.13580
H         -2.12020       -0.81840       -1.89470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

