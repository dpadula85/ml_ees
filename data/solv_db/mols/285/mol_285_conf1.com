%nproc=16
%mem=2GB
%chk=mol_285_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.98700       -0.65570       -1.27460
C         -2.07780        0.06690       -2.02010
C         -0.95860        0.65160       -1.42020
C         -0.73180        0.52230       -0.06480
C         -1.65540       -0.20470        0.65760
C         -2.76670       -0.78860        0.07860
C          0.46660        1.15450        0.54600
N          1.61420        0.30110        0.55960
C          2.54410        0.20660       -0.40060
C          3.49870       -0.71770       -0.04100
N          3.12170       -1.17800        1.15940
C          1.97410       -0.55490        1.52090
H         -3.86750       -1.12120       -1.71960
H         -2.24880        0.17320       -3.08640
H         -0.26710        1.21520       -2.05790
H         -1.47160       -0.30230        1.71880
H         -3.49590       -1.36410        0.65750
H          0.74310        2.08450       -0.02680
H          0.17460        1.51110        1.55470
H          2.58510        0.75580       -1.34290
H          4.38750       -1.04350       -0.57540
H          1.41870       -0.71220        2.44240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_285_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

