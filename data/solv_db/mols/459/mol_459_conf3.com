%nproc=16
%mem=2GB
%chk=mol_459_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.19450       -0.49370       -0.37670
C         -0.40440        0.12390        0.48860
C          1.04380        0.37010        0.20290
Cl         1.49230       -0.26670       -1.39230
H         -0.81180       -0.84190       -1.32560
H         -2.23950       -0.67180       -0.17050
H         -0.80680        0.46650        1.43580
H          1.66070       -0.15060        0.95430
H          1.26020        1.46410        0.18340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

