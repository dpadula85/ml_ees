%nproc=16
%mem=2GB
%chk=mol_459_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.31440       -0.28590       -0.34980
C          0.34320        0.08360        0.44970
C         -1.07940       -0.01980        0.02120
Cl        -1.74540        1.64820        0.07900
H          1.08200       -0.66660       -1.32270
H          2.33670       -0.20010       -0.01300
H          0.53120        0.46870        1.42830
H         -1.18610       -0.34690       -1.03220
H         -1.59660       -0.68100        0.73950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

