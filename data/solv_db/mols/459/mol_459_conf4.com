%nproc=16
%mem=2GB
%chk=mol_459_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.30590       -0.14750       -0.47360
C         -0.26180        0.53640       -0.02250
C          0.95830       -0.17840        0.45480
Cl         2.39900        0.27100       -0.48300
H         -2.18360        0.38540       -0.81630
H         -1.32600       -1.22820       -0.51770
H         -0.26970        1.60040        0.00960
H          1.18350        0.03760        1.51830
H          0.80620       -1.27670        0.33040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

