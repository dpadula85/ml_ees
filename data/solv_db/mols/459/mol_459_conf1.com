%nproc=16
%mem=2GB
%chk=mol_459_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.31170        0.01430        0.35460
C          0.28040       -0.38130       -0.36680
C         -1.07160        0.06450        0.05980
Cl        -1.74340        1.00060       -1.30980
H          1.17250        0.63790        1.22050
H          2.32930       -0.28980        0.07790
H          0.44650       -1.00910       -1.23780
H         -1.71260       -0.81000        0.29290
H         -1.01290        0.77290        0.90860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

