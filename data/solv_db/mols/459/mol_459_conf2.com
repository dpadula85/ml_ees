%nproc=16
%mem=2GB
%chk=mol_459_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.33640        0.21630        0.24270
C         -0.09750        0.60280       -0.03490
C          1.05520       -0.31020        0.03950
Cl         0.60470       -1.94560        0.53130
H         -2.17270        0.86750        0.19170
H         -1.48650       -0.81120        0.53280
H          0.08110        1.62320       -0.32720
H          1.82510        0.14300        0.71010
H          1.52700       -0.38570       -0.96280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_459_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

