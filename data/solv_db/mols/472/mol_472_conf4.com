%nproc=16
%mem=2GB
%chk=mol_472_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73970        0.14910        0.10010
O          1.61820        0.93560       -0.22900
C          0.31750        0.44800       -0.13390
C         -0.75790        1.25230       -0.46610
C         -2.06830        0.82640       -0.39270
C         -2.32860       -0.45130        0.02760
C         -1.26260       -1.26210        0.36140
C          0.05340       -0.84350        0.29080
H          2.55600       -0.48600        1.01520
H          2.95260       -0.52980       -0.73480
H          3.64500        0.77840        0.28740
H         -0.56420        2.27220       -0.80260
H         -2.90460        1.47490       -0.65890
H         -3.36600       -0.77950        0.08290
H         -1.49050       -2.26490        0.69000
H          0.86040       -1.51980        0.56260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

