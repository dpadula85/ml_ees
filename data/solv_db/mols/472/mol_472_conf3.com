%nproc=16
%mem=2GB
%chk=mol_472_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.64870        0.35080        0.32470
O         -1.74210       -0.69530        0.02850
C         -0.39670       -0.42530        0.01680
C          0.49390       -1.43590       -0.27020
C          1.85570       -1.20010       -0.29080
C          2.34450        0.06070       -0.02200
C          1.44850        1.06600        0.26400
C          0.09340        0.83340        0.28500
H         -2.59360        1.14830       -0.45100
H         -2.41090        0.80460        1.33190
H         -3.69730       -0.04470        0.42250
H          0.10460       -2.44790       -0.48600
H          2.50120       -2.01520       -0.51820
H          3.41410        0.28210       -0.02970
H          1.82080        2.04740        0.47370
H         -0.58750        1.67110        0.51970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

