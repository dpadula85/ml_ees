%nproc=16
%mem=2GB
%chk=mol_472_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.69850        0.08590        0.32570
O         -1.66990       -0.84850        0.32480
C         -0.34430       -0.47790        0.10720
C          0.68820       -1.39100        0.10290
C          1.99610       -1.00610       -0.11450
C          2.34150        0.31120       -0.33900
C          1.30380        1.22020       -0.33350
C         -0.00390        0.83860       -0.11660
H         -2.91370        0.41580       -0.72340
H         -3.62190       -0.35360        0.75070
H         -2.41290        0.92860        0.98500
H          0.41850       -2.43360        0.28020
H          2.80840       -1.73810       -0.11610
H          3.36770        0.60600       -0.50860
H          1.55970        2.27140       -0.51010
H         -0.81880        1.57130       -0.11470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

