%nproc=16
%mem=2GB
%chk=mol_472_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.63820       -0.12060       -0.75350
O          1.61020       -1.03570       -0.42590
C          0.32200       -0.53080       -0.19690
C          0.06250        0.81910       -0.28780
C         -1.19510        1.33610       -0.06710
C         -2.25250        0.50990        0.25710
C         -1.98880       -0.84470        0.34770
C         -0.73080       -1.35960        0.12660
H          2.80590        0.56060        0.11170
H          2.27410        0.46150       -1.65070
H          3.55510       -0.63680       -1.08380
H          0.89490        1.47680       -0.54330
H         -1.40860        2.41250       -0.13810
H         -3.25470        0.88700        0.43690
H         -2.81480       -1.50560        0.60270
H         -0.51760       -2.42960        0.19710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

