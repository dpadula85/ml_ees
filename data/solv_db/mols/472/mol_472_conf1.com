%nproc=16
%mem=2GB
%chk=mol_472_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65730        0.41190        0.22180
O          1.74190       -0.61870        0.47080
C          0.37850       -0.41910        0.16670
C         -0.55500       -1.39780        0.39260
C         -1.89020       -1.22010        0.10170
C         -2.26670       -0.01260       -0.43080
C         -1.36150        1.00330       -0.67580
C         -0.03940        0.78000       -0.36940
H          2.25700        1.32500        0.74490
H          3.65360        0.14810        0.56780
H          2.62750        0.67590       -0.87490
H         -0.23440       -2.33760        0.81130
H         -2.63690       -2.00520        0.28320
H         -3.31550        0.16080       -0.67220
H         -1.71210        1.95330       -1.10400
H          0.69570        1.55270       -0.54810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_472_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

