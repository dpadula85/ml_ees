%nproc=16
%mem=2GB
%chk=mol_506_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.71930        1.19680       -0.26280
C         -0.01090       -0.14970       -0.30160
C         -0.72050       -1.19570        0.48430
C          1.40890        0.09760        0.17920
Br         0.13560       -0.67640       -2.17380
H         -0.49850        1.78840       -1.18700
H         -1.80210        0.98030       -0.15010
H         -0.33220        1.71730        0.64430
H         -0.10630       -1.63150        1.30960
H         -1.62060       -0.73450        0.97010
H         -1.06810       -1.99610       -0.21900
H          1.96840       -0.83390        0.35150
H          1.44600        0.80590        1.02920
H          1.91960        0.63150       -0.67380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

