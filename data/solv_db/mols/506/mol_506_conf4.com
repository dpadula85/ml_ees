%nproc=16
%mem=2GB
%chk=mol_506_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.21080       -0.73790        0.21170
C          0.01880       -0.02560       -0.33760
C          0.02920        1.39640        0.22220
C         -1.29190       -0.65900        0.00130
Br         0.20550        0.14790       -2.26850
H          1.05130       -1.82100        0.38790
H          1.63950       -0.22330        1.11300
H          2.03880       -0.68780       -0.55060
H         -0.95090        1.85530        0.01590
H          0.83950        1.92400       -0.31880
H          0.25210        1.29380        1.30430
H         -1.89340       -0.80750       -0.93550
H         -1.93960       -0.02290        0.65260
H         -1.20970       -1.63220        0.50200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

