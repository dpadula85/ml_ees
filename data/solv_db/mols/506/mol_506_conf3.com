%nproc=16
%mem=2GB
%chk=mol_506_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.13770        1.33840        0.33190
C          0.01020       -0.01600       -0.36910
C         -1.18910       -0.82890        0.03460
C          1.31300       -0.60840        0.04340
Br        -0.03490        0.29520       -2.28900
H         -0.40630        1.16770        1.39360
H          0.84220        1.82930        0.29940
H         -0.91870        1.91780       -0.21930
H         -1.08330       -1.90030       -0.11110
H         -1.45400       -0.57460        1.08440
H         -2.03700       -0.48970       -0.62390
H          2.03020        0.23670        0.24470
H          1.79420       -1.17730       -0.79800
H          1.27140       -1.18980        0.97840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

