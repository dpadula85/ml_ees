%nproc=16
%mem=2GB
%chk=mol_506_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.13060       -0.86150        0.18910
C          0.05020        0.06270        0.38310
C          1.35090       -0.55440       -0.06010
C         -0.22930        1.29390       -0.48650
Br         0.21210        0.63580        2.21640
H         -1.63770       -0.67530       -0.79280
H         -0.83720       -1.91060        0.32290
H         -1.89860       -0.56260        0.95370
H          2.02270        0.24150       -0.47790
H          1.11790       -1.21640       -0.91910
H          1.86310       -1.08270        0.76740
H          0.23240        1.06210       -1.47120
H          0.21420        2.15920        0.02110
H         -1.33000        1.40820       -0.64590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

