%nproc=16
%mem=2GB
%chk=mol_506_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.50850        1.30970        0.04870
C          0.03970       -0.07360        0.36150
C          1.39670       -0.16960       -0.30190
C         -0.92160       -1.12210       -0.13400
Br         0.32540       -0.26350        2.27810
H         -1.52250        1.21540       -0.42180
H          0.12360        1.85410       -0.68130
H         -0.65480        1.86860        0.99290
H          2.21420        0.01000        0.44390
H          1.52510        0.63150       -1.07460
H          1.59640       -1.16220       -0.73640
H         -1.84410       -0.62810       -0.54550
H         -0.46600       -1.71210       -0.94030
H         -1.30350       -1.75820        0.71060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_506_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

