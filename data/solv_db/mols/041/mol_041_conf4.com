%nproc=16
%mem=2GB
%chk=mol_041_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17830        1.05290        0.01920
C         -1.36780       -0.22430       -0.05100
C          0.07640        0.14610        0.08090
C          0.99300       -1.06490        0.02420
C          2.37510       -0.60120        0.16110
C          3.50870       -0.20770        0.28740
H         -2.12050        1.55690       -0.97820
H         -1.68250        1.73660        0.74440
H         -3.21780        0.87730        0.34430
H         -1.62220       -0.91490        0.77980
H         -1.53300       -0.69900       -1.04280
H          0.21320        0.57400        1.10810
H          0.40490        0.91030       -0.62350
H          0.73120       -1.82070        0.76400
H          0.90550       -1.45910       -1.02610
H          4.51400        0.13760        0.41610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

