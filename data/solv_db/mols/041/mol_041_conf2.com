%nproc=16
%mem=2GB
%chk=mol_041_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.13120       -0.97380        0.65370
C         -1.20420        0.48880        0.33850
C         -0.28670        0.98390       -0.70390
C          1.16290        0.89110       -0.51500
C          1.64120       -0.46890       -0.41130
C          2.09020       -1.61470       -0.33970
H         -0.95760       -1.61270       -0.20460
H         -2.14000       -1.33370        1.05920
H         -0.36950       -1.14390        1.41960
H         -1.10010        1.10160        1.29120
H         -2.28400        0.71200        0.04990
H         -0.53770        2.09420       -0.83110
H         -0.59690        0.55840       -1.72190
H          1.69940        1.41540       -1.37800
H          1.53650        1.49920        0.36610
H          2.47770       -2.59690       -0.26130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

