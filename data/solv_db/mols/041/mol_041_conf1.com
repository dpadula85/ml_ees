%nproc=16
%mem=2GB
%chk=mol_041_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.23180        0.49960       -0.19530
C         -0.80270        0.40730        0.23870
C         -0.12850       -0.83270       -0.30210
C          1.29620       -0.96480        0.10570
C          2.13100        0.14350       -0.33660
C          2.83850        1.05320       -0.69490
H         -2.83790       -0.37550        0.11000
H         -2.31800        0.54070       -1.31590
H         -2.64140        1.46010        0.16600
H         -0.27100        1.29150       -0.19580
H         -0.75800        0.48530        1.32380
H         -0.17040       -0.82200       -1.41050
H         -0.67180       -1.73330        0.04390
H          1.39800       -1.13070        1.20610
H          1.70110       -1.88120       -0.37160
H          3.46680        1.85890       -1.00260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

