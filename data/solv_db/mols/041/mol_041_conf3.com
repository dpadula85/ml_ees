%nproc=16
%mem=2GB
%chk=mol_041_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97840        0.55020        0.21420
C         -1.32140       -0.81780        0.30400
C          0.09340       -0.66490       -0.19640
C          0.85710        0.33290        0.64790
C          2.21940        0.39810        0.06620
C          3.32140        0.46330       -0.41800
H         -1.59880        1.14400        1.07240
H         -1.69600        1.00550       -0.76710
H         -3.09950        0.42350        0.27350
H         -1.28930       -1.19050        1.34470
H         -1.85270       -1.47200       -0.39510
H          0.65190       -1.63860       -0.12870
H          0.11930       -0.35810       -1.25920
H          0.89900       -0.01510        1.69560
H          0.38030        1.33070        0.60920
H          4.29420        0.50870       -0.85870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_041_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

