%nproc=16
%mem=2GB
%chk=mol_591_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.04610        0.02070       -0.00150
F          1.27010       -0.41300       -0.14570
H         -0.73950       -0.57700       -0.64690
H         -0.35980       -0.12030        1.05050
H         -0.12460        1.08960       -0.25630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_591_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_591_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_591_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

