%nproc=16
%mem=2GB
%chk=mol_591_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.06580        0.00670       -0.00540
F          1.30340       -0.08130        0.19370
H         -0.63180       -0.05370        0.94270
H         -0.35670       -0.85840       -0.62540
H         -0.24910        0.98670       -0.50560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_591_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_591_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_591_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

