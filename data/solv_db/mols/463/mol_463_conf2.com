%nproc=16
%mem=2GB
%chk=mol_463_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.52790       -0.05770       -0.54790
C          2.15880       -0.58890       -0.30110
C          1.19240        0.55760       -0.00040
C         -0.16240       -0.13060        0.23030
C         -1.21750        0.88230        0.53870
C         -2.51820        0.17390        0.76030
C         -2.95440       -0.60860       -0.44300
Cl        -4.52030       -1.43330       -0.12290
H          3.52880        0.58180       -1.44350
H          3.82140        0.56850        0.31030
H          4.22790       -0.90840       -0.61060
H          2.16450       -1.20850        0.64340
H          1.75340       -1.20700       -1.11400
H          1.10290        1.25960       -0.83620
H          1.52910        1.09530        0.89220
H         -0.36110       -0.65220       -0.72970
H         -0.05940       -0.88010        1.04140
H         -1.33950        1.52090       -0.36190
H         -0.91040        1.45320        1.42810
H         -3.28610        0.90820        1.06420
H         -2.39100       -0.52520        1.61950
H         -3.04270        0.08820       -1.30730
H         -2.24400       -1.40300       -0.70980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

