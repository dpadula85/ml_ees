%nproc=16
%mem=2GB
%chk=mol_463_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.13770        0.63310        0.21110
C         -2.60560       -0.72340       -0.08940
C         -1.18650       -0.93150        0.39800
C         -0.21810        0.04350       -0.22870
C          1.15580       -0.28320        0.35040
C          2.21070        0.65110       -0.22250
C          3.54120        0.28370        0.38250
Cl         4.81460        1.32880       -0.23790
H         -2.70000        1.13190        1.07940
H         -3.09800        1.33400       -0.64630
H         -4.23780        0.51670        0.44050
H         -2.67300       -0.89000       -1.18470
H         -3.20940       -1.54550        0.37200
H         -1.14540       -0.78880        1.49730
H         -0.92690       -1.98240        0.15930
H         -0.42510        1.08730        0.05130
H         -0.12530       -0.09670       -1.32210
H          1.36790       -1.32590        0.05000
H          1.09490       -0.11650        1.42430
H          2.28330        0.40350       -1.31440
H          1.93830        1.70420       -0.10500
H          3.76720       -0.75260        0.06470
H          3.51490        0.31860        1.50150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

