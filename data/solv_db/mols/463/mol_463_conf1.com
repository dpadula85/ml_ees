%nproc=16
%mem=2GB
%chk=mol_463_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.58650        0.35840       -0.00650
C         -2.25440        0.37500       -0.72450
C         -1.21080       -0.07560        0.24510
C          0.18420       -0.09390       -0.38900
C          1.11180       -0.56100        0.69250
C          2.53610       -0.73970        0.36490
C          3.32260        0.42960       -0.12310
Cl         2.72840        1.04730       -1.66280
H         -4.21300        1.21440       -0.32500
H         -3.44750        0.48490        1.10630
H         -4.10140       -0.61300       -0.12130
H         -2.06630        1.39630       -1.11620
H         -2.31410       -0.28190       -1.62980
H         -1.47720       -1.09550        0.58990
H         -1.18740        0.63390        1.08290
H          0.24660       -0.69890       -1.29020
H          0.42720        0.98620       -0.63770
H          1.06610        0.12500        1.58480
H          0.72720       -1.54080        1.13050
H          2.69500       -1.54620       -0.41330
H          3.05210       -1.08230        1.32440
H          3.38880        1.24570        0.65220
H          4.37260        0.03210       -0.29920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

