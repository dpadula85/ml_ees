%nproc=16
%mem=2GB
%chk=mol_463_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.22300        0.21350        1.01390
C         -1.88370       -0.30620        0.53000
C         -1.44320        0.53940       -0.63760
C         -0.12200        0.11130       -1.19900
C          0.99760        0.18150       -0.20530
C          2.26370       -0.28020       -0.91800
C          3.45030       -0.23860        0.02400
Cl         3.13470       -1.31300        1.40420
H         -3.31780        0.17720        2.11870
H         -4.06570       -0.37730        0.55230
H         -3.38900        1.27330        0.71100
H         -1.97410       -1.35160        0.18440
H         -1.13030       -0.21450        1.35760
H         -2.20460        0.39110       -1.44100
H         -1.48800        1.60590       -0.36740
H         -0.22830       -0.93690       -1.55060
H          0.12710        0.80660       -2.02860
H          1.14030        1.18520        0.19660
H          0.85670       -0.52490        0.64590
H          2.09490       -1.34360       -1.17770
H          2.43970        0.26880       -1.84400
H          4.31570       -0.65190       -0.55500
H          3.64880        0.78500        0.40340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

