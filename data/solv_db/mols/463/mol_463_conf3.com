%nproc=16
%mem=2GB
%chk=mol_463_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.67490        0.56700       -0.35330
C         -2.52980       -0.20520        0.22570
C         -1.23740        0.30680       -0.36500
C         -0.04820       -0.46180        0.20620
C          1.18890        0.10920       -0.43210
C          2.44910       -0.56680        0.04870
C          3.61820        0.09560       -0.66480
Cl         5.14920       -0.65570       -0.14380
H         -4.10230        1.21720        0.42010
H         -4.45650       -0.13860       -0.75630
H         -3.31790        1.24160       -1.17630
H         -2.66510       -1.28450        0.01540
H         -2.50750       -0.09740        1.32140
H         -1.09370        1.37530       -0.16770
H         -1.29360        0.17320       -1.46460
H          0.01090       -0.24870        1.29200
H         -0.14780       -1.54160        0.07670
H          1.28080        1.17490       -0.24070
H          1.15060       -0.03160       -1.53430
H          2.45790       -1.65580       -0.16290
H          2.61290       -0.45400        1.13580
H          3.60240        1.17590       -0.52460
H          3.55410       -0.09510       -1.76990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_463_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

