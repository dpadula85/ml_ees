%nproc=16
%mem=2GB
%chk=mol_424_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36460        0.37640        0.49940
C          0.92860        0.25600        0.08790
C          0.14330        1.30530        0.22600
C         -1.28930        1.18300       -0.18430
C         -1.79490       -0.19900        0.21280
C         -0.98430       -1.28280       -0.42070
C          0.48410       -1.05180       -0.45980
H          2.99310       -0.40350        0.02080
H          2.46100        0.22660        1.61000
H          2.74090        1.36850        0.18590
H          0.50010        2.25040        0.62550
H         -1.93330        1.91200        0.33340
H         -1.38690        1.29390       -1.26800
H         -1.73210       -0.32810        1.31480
H         -2.84660       -0.26470       -0.12750
H         -1.21600       -2.23130        0.14770
H         -1.32790       -1.48280       -1.46000
H          0.89300       -1.09400       -1.51100
H          1.00250       -1.83400        0.16710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

