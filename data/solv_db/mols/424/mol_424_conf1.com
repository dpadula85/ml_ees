%nproc=16
%mem=2GB
%chk=mol_424_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.40860       -0.01460        0.57170
C         -0.95320        0.04250        0.25080
C         -0.28750        1.13270        0.51810
C          1.14440        1.29310        0.24280
C          1.85340        0.08950       -0.21560
C          1.05420       -0.99250       -0.82080
C         -0.34060       -1.15490       -0.35970
H         -2.60740       -0.10820        1.64950
H         -2.91430       -0.82560        0.00100
H         -2.93220        0.93160        0.24710
H         -0.80920        1.97580        0.96970
H          1.24100        2.14310       -0.49400
H          1.61520        1.70640        1.18270
H          2.48930       -0.29920        0.63370
H          2.62000        0.41990       -0.98090
H          1.59850       -1.96290       -0.61130
H          1.08070       -0.93110       -1.94930
H         -0.99630       -1.44890       -1.21440
H         -0.44730       -1.99650        0.37900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

