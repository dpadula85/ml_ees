%nproc=16
%mem=2GB
%chk=mol_424_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47010        0.12180       -0.08230
C         -0.97520        0.07180       -0.07960
C         -0.28580        1.15100        0.25190
C          1.19400        1.25810        0.30320
C          1.83810        0.03470       -0.23270
C          1.06100       -1.16680        0.31990
C         -0.24130       -1.16720       -0.45240
H         -2.86700       -0.89760       -0.16990
H         -2.83000        0.73160       -0.93440
H         -2.87810        0.56800        0.83810
H         -0.89580        2.04700        0.51110
H          1.46600        2.18080       -0.24720
H          1.52030        1.40460        1.36540
H          2.88230       -0.03060        0.13170
H          1.83280        0.04060       -1.32590
H          1.62330       -2.10870        0.17240
H          0.83560       -0.97100        1.39390
H          0.03050       -1.18700       -1.52690
H         -0.84070       -2.08120       -0.23610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

