%nproc=16
%mem=2GB
%chk=mol_424_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.41480        0.47770       -0.14470
C          0.95880        0.25270        0.07580
C          0.15650        1.27650        0.22560
C         -1.29200        1.05690        0.44520
C         -1.83290       -0.14380       -0.24970
C         -0.94230       -1.31470       -0.31100
C          0.45710       -1.12090        0.12030
H          2.62970        1.07500       -1.04200
H          2.87940        1.02040        0.72760
H          2.97130       -0.47730       -0.21590
H          0.54060        2.29120        0.19080
H         -1.84170        1.97060        0.08150
H         -1.53340        0.98840        1.52870
H         -2.82110       -0.39290        0.23720
H         -2.12890        0.18820       -1.28820
H         -0.98220       -1.75180       -1.35220
H         -1.38890       -2.13190        0.32990
H          0.64070       -1.50720        1.16500
H          1.11440       -1.75700       -0.52390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

