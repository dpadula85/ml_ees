%nproc=16
%mem=2GB
%chk=mol_424_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.46560        0.32680        0.19210
C         -0.99990        0.22610       -0.00260
C         -0.18250        1.25590        0.16330
C          1.27280        1.20760       -0.01690
C          1.87820       -0.13520       -0.01540
C          0.96870       -1.29450        0.04960
C         -0.42670       -1.06610       -0.39590
H         -2.88170       -0.68380        0.43100
H         -2.94430        0.74630       -0.70630
H         -2.70970        0.96420        1.06240
H         -0.61930        2.20420        0.45270
H          1.73660        1.86300        0.77870
H          1.49550        1.76820       -0.97330
H          2.54850       -0.22390       -0.92090
H          2.60190       -0.21660        0.84930
H          1.38740       -2.12790       -0.59000
H          0.95780       -1.74500        1.08600
H         -0.55360       -1.19020       -1.50220
H         -1.06410       -1.87900        0.05830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_424_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

