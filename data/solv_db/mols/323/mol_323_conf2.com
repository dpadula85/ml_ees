%nproc=16
%mem=2GB
%chk=mol_323_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.89260        0.31610       -0.01570
C          2.45120       -0.02240       -0.12360
C          2.09130       -1.24190       -0.65150
C          0.75800       -1.54970       -0.74900
C         -0.17810       -0.62280       -0.31300
C         -1.51200       -0.92330       -0.40760
C         -2.43450       -0.01230        0.02090
C         -2.06200        1.19760        0.54430
C         -0.73000        1.53170        0.65360
C          0.19780        0.60020        0.21600
C          1.53030        0.89440        0.30760
C         -3.89800       -0.30470       -0.06630
H          4.21840        0.91100       -0.90950
H          4.01530        0.95960        0.87740
H          4.52340       -0.59400        0.06220
H          2.81320       -1.97430       -0.99560
H          0.39540       -2.50010       -1.15790
H         -1.84540       -1.88380       -0.82200
H         -2.80820        1.91570        0.88250
H         -0.44930        2.48700        1.06720
H          1.81380        1.84890        0.72080
H         -4.42790        0.62480       -0.39530
H         -4.24020       -0.51020        0.96830
H         -4.11530       -1.14740       -0.74920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

