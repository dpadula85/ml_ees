%nproc=16
%mem=2GB
%chk=mol_323_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.89310        0.54980        0.16560
C          2.46530        0.32150       -0.17540
C          2.03440        0.14590       -1.47360
C          0.70190       -0.06390       -1.75660
C         -0.23120       -0.10220       -0.73850
C         -1.57250       -0.30860       -0.97120
C         -2.44090       -0.33380        0.09230
C         -2.02140       -0.15940        1.39380
C         -0.68180        0.04820        1.64160
C          0.19790        0.07330        0.56030
C          1.54870        0.28650        0.85320
C         -3.89960       -0.55410       -0.11430
H          4.40950       -0.37350        0.48840
H          4.01130        1.28550        0.99160
H          4.40200        1.02780       -0.72210
H          2.78200        0.17780       -2.27360
H          0.34890       -0.20310       -2.76620
H         -1.92070       -0.44560       -1.96500
H         -2.70510       -0.17950        2.22860
H         -0.32360        0.18810        2.65210
H          1.89240        0.42490        1.86920
H         -4.49410        0.07880        0.58550
H         -4.21120       -0.28270       -1.15930
H         -4.18550       -1.60180        0.09770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

