%nproc=16
%mem=2GB
%chk=mol_323_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.97020        0.06230        0.24960
C          2.49560        0.24410        0.16330
C          2.02150        1.51520       -0.10650
C          0.65350        1.67000       -0.18380
C         -0.24690        0.64090       -0.00880
C         -1.61330        0.80710       -0.08830
C         -2.50900       -0.22230        0.08690
C         -1.99750       -1.47730        0.35480
C         -0.64980       -1.68910        0.44280
C          0.23620       -0.63120        0.26150
C          1.60770       -0.81130        0.34400
C         -3.97970       -0.02740       -0.00180
H          4.39530        0.48390        1.18070
H          4.46930        0.47120       -0.64140
H          4.17120       -1.03770        0.27700
H          2.72730        2.32320       -0.24390
H          0.27630        2.66060       -0.39450
H         -2.00230        1.77770       -0.29530
H         -2.67890       -2.30630        0.49720
H         -0.27160       -2.67420        0.65230
H          2.01970       -1.80710        0.55680
H         -4.48560       -0.98840        0.08390
H         -4.32970        0.59470        0.86990
H         -4.27930        0.42150       -0.97150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

