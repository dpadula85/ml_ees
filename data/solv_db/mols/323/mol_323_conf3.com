%nproc=16
%mem=2GB
%chk=mol_323_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.92680       -0.08510        0.04070
C          2.43720       -0.17950        0.11610
C          1.92090       -1.16470        0.95710
C          0.56870       -1.31610        1.08180
C         -0.25290       -0.47530        0.35950
C         -1.60720       -0.60220        0.46320
C         -2.43250        0.23010       -0.25180
C         -1.96550        1.22210       -1.09880
C         -0.59490        1.34460       -1.19880
C          0.24260        0.51260       -0.48400
C          1.60920        0.65790       -0.60340
C         -3.90500        0.03200       -0.08730
H          4.23960       -0.68110       -0.82860
H          4.18570        0.97930       -0.15900
H          4.37930       -0.44020        1.00140
H          2.57780       -1.82000        1.51990
H          0.15300       -2.08330        1.73680
H         -1.99370       -1.38350        1.13040
H         -2.64950        1.85760       -1.64500
H         -0.14850        2.09390       -1.83830
H          2.08590        1.39670       -1.23370
H         -4.08980       -1.05340        0.01870
H         -4.22360        0.49340        0.88520
H         -4.46370        0.46420       -0.94240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

