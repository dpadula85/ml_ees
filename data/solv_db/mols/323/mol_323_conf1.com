%nproc=16
%mem=2GB
%chk=mol_323_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.98160        0.14710       -0.10460
C         -2.51420       -0.13650       -0.10580
C         -2.00590       -1.39940       -0.32280
C         -0.64030       -1.65640       -0.32230
C          0.26020       -0.63880       -0.10120
C          1.62410       -0.88030       -0.09790
C          2.49860        0.17040        0.12840
C          2.01630        1.43030        0.34570
C          0.64680        1.64870        0.33790
C         -0.24840        0.62490        0.11580
C         -1.60410        0.88240        0.11580
C          3.96870       -0.11330        0.12740
H         -4.50700       -0.81490       -0.32010
H         -4.18290        0.79630       -0.98430
H         -4.31250        0.58680        0.84200
H         -2.74110       -2.19460       -0.49660
H         -0.27780       -2.66960       -0.49810
H          2.03310       -1.86890       -0.26730
H          2.71580        2.23070        0.51920
H          0.23790        2.63560        0.50710
H         -2.01240        1.87880        0.28670
H          4.16200       -1.00430       -0.52690
H          4.54780        0.71700       -0.32860
H          4.31680       -0.37210        1.15060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_323_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

