%nproc=16
%mem=2GB
%chk=mol_628_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.48980       -0.00170       -0.39410
C         -0.82260        1.25780        0.01220
C          0.46850        1.54310        0.10790
C          1.59480        0.63690       -0.16500
C          1.52020       -0.69400       -0.02160
C          0.38740       -1.51270        0.40820
C         -0.89100       -1.19630        0.24420
H         -2.56970        0.04750       -0.14150
H         -1.42160       -0.11090       -1.51720
H         -1.47840        2.10250        0.28090
H          0.76980        2.55370        0.38550
H          2.53450        1.04140       -0.48670
H          2.42360       -1.27270       -0.23620
H          0.59980       -2.47090        0.88770
H         -1.62540       -1.92360        0.63580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

