%nproc=16
%mem=2GB
%chk=mol_628_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.40150       -0.54500       -0.34800
C         -0.38870       -1.43860        0.23960
C          0.91260       -1.26360        0.34400
C          1.70160       -0.11080       -0.09360
C          1.21810        1.14170       -0.16950
C         -0.14740        1.60970        0.15280
C         -1.22760        0.86750        0.07010
H         -1.29040       -0.62180       -1.46960
H         -2.43380       -0.88880       -0.13240
H         -0.70730       -2.38590        0.65880
H          1.51720       -2.06220        0.78200
H          2.73220       -0.27330       -0.37210
H          1.91020        1.90390       -0.48630
H         -0.24970        2.65680        0.45680
H         -2.14550        1.41030        0.36730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

