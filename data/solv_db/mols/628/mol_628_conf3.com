%nproc=16
%mem=2GB
%chk=mol_628_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.51230        0.34200       -0.23230
C         -0.51790        1.37770        0.01140
C          0.80660        1.39890        0.07780
C          1.70470        0.26500       -0.09540
C          1.37460       -1.01620       -0.02510
C          0.06890       -1.59710        0.23540
C         -1.12060       -1.01280        0.14270
H         -2.46210        0.63970        0.29980
H         -1.80090        0.32950       -1.31820
H         -0.92970        2.38510        0.18280
H          1.29290        2.35070        0.28160
H          2.75140        0.49980       -0.30340
H          2.22220       -1.66770       -0.18990
H          0.08370       -2.63330        0.54120
H         -1.96150       -1.66140        0.39160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_628_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

