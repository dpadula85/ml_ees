%nproc=16
%mem=2GB
%chk=mol_434_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.14730       -0.58870       -0.17920
C         -1.08450        0.89570        0.09830
O          0.06060        1.13610        0.84650
C          1.19600        0.61630        0.26030
C          1.05570       -0.88770        0.06350
O         -0.01390       -1.05130       -0.84040
H         -2.03470       -0.88370       -0.75450
H         -1.13960       -1.09090        0.82410
H         -1.96110        1.12520        0.75480
H         -1.15720        1.51370       -0.81000
H          2.05360        0.77120        0.91790
H          1.36440        1.10690       -0.71340
H          0.85710       -1.36400        1.03290
H          1.95060       -1.29890       -0.41230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

