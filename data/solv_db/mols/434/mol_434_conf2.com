%nproc=16
%mem=2GB
%chk=mol_434_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.11570       -0.77060       -0.36780
C          1.17840        0.71270       -0.14920
O          0.03710        1.38780       -0.47160
C         -1.13550        0.78370       -0.10540
C         -1.16450       -0.69590       -0.25000
O         -0.01900       -1.28270        0.24540
H          1.99180       -1.23550        0.14540
H          1.15240       -0.97970       -1.45440
H          1.51230        0.87990        0.90330
H          1.99490        1.09840       -0.82820
H         -1.36590        1.04520        0.95400
H         -1.94670        1.20160       -0.76720
H         -2.01600       -1.08170        0.37200
H         -1.33510       -1.06320       -1.27350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

