%nproc=16
%mem=2GB
%chk=mol_434_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.23440        0.11760        0.51020
C          0.45810        1.22790       -0.12000
O         -0.48940        0.77480       -1.04760
C         -1.32170       -0.10920       -0.33470
C         -0.44820       -1.34880       -0.10960
O          0.48100       -0.98400        0.85070
H          1.67830        0.50840        1.46590
H          2.06040       -0.17450       -0.18020
H         -0.11490        1.83800        0.61740
H          1.17130        1.90800       -0.64900
H         -1.47340        0.36700        0.66810
H         -2.24910       -0.33730       -0.85240
H         -1.04590       -2.17870        0.25440
H          0.05910       -1.60940       -1.07320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

