%nproc=16
%mem=2GB
%chk=mol_434_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.11860        0.72340       -0.09760
C         -1.17270       -0.76630        0.22800
O         -0.05940       -1.34820       -0.31920
C          1.13030       -0.73280       -0.01330
C          1.11000        0.75260       -0.08500
O         -0.01470        1.22060        0.63420
H         -0.87420        0.78880       -1.17600
H         -2.01620        1.26000        0.20340
H         -1.26590       -0.94270        1.32110
H         -2.07600       -1.15930       -0.25750
H          1.48960       -1.02830        0.99150
H          1.88580       -1.09310       -0.76740
H          1.98670        1.15970        0.44160
H          0.99520        1.16560       -1.10380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

