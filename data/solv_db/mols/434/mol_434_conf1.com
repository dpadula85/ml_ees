%nproc=16
%mem=2GB
%chk=mol_434_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.32590       -0.49170       -0.29090
C          0.81700       -0.34650        1.11460
O         -0.50990       -0.66540        1.26890
C         -1.34400       -0.17930        0.29860
C         -0.82560       -0.33310       -1.10150
O          0.37480       -1.02730       -1.13170
H          2.21650       -1.16740       -0.33640
H          1.63630        0.52630       -0.63100
H          1.39930       -1.06420        1.76520
H          1.03790        0.65680        1.54050
H         -2.31070       -0.75820        0.37590
H         -1.61100        0.88930        0.48860
H         -0.66670        0.64600       -1.59860
H         -1.54000       -0.89080       -1.76210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_434_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

