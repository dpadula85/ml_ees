%nproc=16
%mem=2GB
%chk=mol_394_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.58910       -0.35880        0.25750
C          3.18680       -0.89920        0.35800
O          2.98660       -2.13960        0.32990
O          2.09650       -0.06360        0.48210
C          0.79430       -0.64120        0.57270
C         -0.26070        0.43910        0.70270
C         -1.58270       -0.21730        0.96500
O         -2.02850       -1.02710       -0.08190
C         -3.23390       -1.68060        0.05080
O         -3.90130       -1.52860        1.11560
C         -3.81860       -2.56520       -0.97970
O         -0.22150        1.27230       -0.40240
C          0.10070        2.60750       -0.33660
O          0.36790        3.05280        0.81640
C          0.15320        3.52400       -1.48460
H          4.58600        0.71610        0.48000
H          5.15730       -0.89100        1.05660
H          4.95960       -0.60380       -0.74510
H          0.64250       -1.21920       -0.37190
H          0.74100       -1.38130        1.39650
H          0.00440        1.01020        1.63180
H         -2.36760        0.51710        1.27130
H         -1.44510       -0.87860        1.85610
H         -3.15610       -2.76660       -1.82000
H         -4.81200       -2.16240       -1.29190
H         -4.04820       -3.54520       -0.48170
H          1.14210        4.05960       -1.56720
H         -0.03840        3.01880       -2.43360
H         -0.59340        4.35200       -1.34620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

