%nproc=16
%mem=2GB
%chk=mol_394_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.89700       -2.49230       -1.08510
C         -3.36590       -1.10830       -1.01780
O         -4.11610       -0.12950       -1.34640
O         -2.09610       -0.80880       -0.61740
C         -1.58410        0.51840       -0.55190
C         -0.12730        0.47410       -0.06160
C          0.60470       -0.43930       -0.99820
O          1.92890       -0.68180       -0.69950
C          2.41530       -1.24770        0.44460
O          1.58600       -1.57930        1.30560
C          3.84970       -1.49310        0.73110
O          0.42700        1.74720        0.02230
C          0.79010        2.34850        1.21550
O          0.59760        1.68660        2.26220
C          1.36980        3.70910        1.16060
H         -3.17620       -3.18580       -1.58990
H         -4.83450       -2.48700       -1.67000
H         -4.12730       -2.92080       -0.10220
H         -2.19970        1.04000        0.21430
H         -1.66380        0.94050       -1.55100
H         -0.20850        0.07410        0.97140
H          0.55050        0.01750       -2.01030
H         -0.01530       -1.37930       -1.07820
H          4.52670       -0.82370        0.17390
H          4.02390       -1.37000        1.82140
H          4.13740       -2.53730        0.48910
H          0.81560        4.39200        0.49440
H          2.45090        3.61930        0.87400
H          1.33770        4.11670        2.19920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

