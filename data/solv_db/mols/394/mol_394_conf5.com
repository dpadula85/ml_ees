%nproc=16
%mem=2GB
%chk=mol_394_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.70750       -2.21070        0.75490
C         -2.68500       -1.26810        0.13230
O         -3.04770       -0.12400       -0.23170
O         -1.38690       -1.65210       -0.03280
C         -0.35130       -0.92550       -0.55600
C          0.17270        0.25570        0.16870
C          1.38590        0.75250       -0.63560
O          2.26400       -0.32920       -0.90330
C          3.43120       -0.16030       -1.60960
O          3.70350        1.01040       -2.02130
C          4.37920       -1.26020       -1.91340
O         -0.70260        1.30690        0.44130
C         -0.86380        1.85930        1.71350
O         -0.16000        1.34430        2.61950
C         -1.77450        2.96560        2.03940
H         -4.19110       -1.62820        1.56840
H         -3.20120       -3.08770        1.16300
H         -4.46540       -2.46430       -0.01240
H         -0.61000       -0.54000       -1.60820
H          0.48820       -1.64920       -0.81710
H          0.63280       -0.04340        1.16290
H          1.93480        1.53690       -0.10620
H          1.00080        1.14760       -1.58590
H          5.23830       -1.21380       -1.21350
H          3.87390       -2.25380       -1.80060
H          4.80200       -1.14980       -2.94020
H         -2.35340        3.20180        1.10740
H         -1.25570        3.90100        2.32540
H         -2.55130        2.67840        2.79110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

