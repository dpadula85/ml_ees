%nproc=16
%mem=2GB
%chk=mol_394_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.39780       -0.76710        0.94300
C         -3.04910       -1.34760        0.61570
O         -2.76530       -2.51600        0.94330
O         -2.11480       -0.60020       -0.04650
C         -0.82590       -1.05850       -0.39590
C         -0.07910        0.04940       -1.11850
C          1.31060       -0.40510       -1.49020
O          2.08810       -0.71870       -0.35020
C          3.38960       -1.15260       -0.51140
O          3.82120       -1.24710       -1.68860
C          4.26620       -1.50100        0.65810
O          0.06120        1.19640       -0.31760
C         -0.42010        2.44440       -0.63190
O         -1.03410        2.54540       -1.73290
C         -0.24990        3.63390        0.24500
H         -4.40910        0.31690        0.80420
H         -4.58300       -0.95980        2.03010
H         -5.15000       -1.27510        0.33340
H         -0.28080       -1.30180        0.54350
H         -0.86890       -1.98220       -0.99580
H         -0.62740        0.23650       -2.05590
H          1.77590        0.43970       -2.06870
H          1.30300       -1.27250       -2.17590
H          4.37800       -2.59900        0.74590
H          5.26360       -1.00980        0.47260
H          3.86000       -1.03980        1.58090
H          0.65150        4.22610       -0.03080
H         -1.11820        4.31360        0.04490
H         -0.19520        3.35150        1.32160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

