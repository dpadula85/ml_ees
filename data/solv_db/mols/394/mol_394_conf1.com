%nproc=16
%mem=2GB
%chk=mol_394_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.75970       -1.52920       -0.13380
C         -3.80210       -0.40190        0.10290
O         -4.29980        0.65190        0.58660
O         -2.45130       -0.51570       -0.19360
C         -1.61640        0.60700        0.06460
C         -0.18650        0.34040       -0.30350
C          0.41750       -0.78030        0.52390
O          1.77310       -0.90720        0.14200
C          2.60140       -1.85390        0.72340
O          2.10210       -2.59750        1.60500
C          4.02310       -1.99860        0.33370
O          0.55280        1.51430       -0.00820
C          1.25300        2.23360       -0.93650
O          1.26090        1.85390       -2.14000
C          1.98300        3.44560       -0.48250
H         -4.22810       -2.37950       -0.64730
H         -5.26750       -1.84670        0.78370
H         -5.49440       -1.15490       -0.88010
H         -1.93940        1.50650       -0.49900
H         -1.67470        0.90820        1.14800
H         -0.10360        0.11450       -1.37000
H         -0.10540       -1.72750        0.40060
H          0.41800       -0.53280        1.60500
H          4.19310       -2.99270       -0.16280
H          4.28790       -1.23940       -0.42360
H          4.65790       -1.92600        1.24010
H          2.80720        3.66420       -1.19380
H          1.24970        4.29390       -0.42390
H          2.34820        3.24970        0.53920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_394_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

