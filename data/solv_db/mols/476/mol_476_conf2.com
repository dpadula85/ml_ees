%nproc=16
%mem=2GB
%chk=mol_476_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.03150       -0.86240       -0.88640
O         -0.69410       -0.74670       -1.08280
C          0.04590        0.01570       -0.17600
C         -0.03600       -0.52170        1.20220
F          0.80560        0.22740        2.02010
F         -1.31750       -0.27850        1.74250
F          0.17320       -1.86210        1.34520
O         -0.50590        1.29230       -0.24550
C          0.01150        2.26440        0.53380
O          1.36610        0.15660       -0.62320
C          2.05420       -1.00940       -0.78530
H         -2.39530       -1.35370        0.00840
H         -2.50870       -1.44320       -1.73980
H         -2.52950        0.15570       -0.96460
H          1.12390        2.38760        0.37900
H         -0.19980        2.22380        1.60470
H         -0.40480        3.26420        0.13400
H          3.08280       -0.71350       -1.16430
H          1.65500       -1.71480       -1.51060
H          2.30490       -1.48180        0.20830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

