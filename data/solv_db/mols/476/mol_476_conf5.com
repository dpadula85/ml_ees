%nproc=16
%mem=2GB
%chk=mol_476_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.36370       -0.01020       -0.03610
O         -1.23480        0.67760        0.17290
C         -0.01880        0.06430        0.30860
C          0.04000       -0.67510        1.62080
F          1.27750       -1.27450        1.85390
F         -0.30680        0.17720        2.64980
F         -0.90940       -1.71550        1.66200
O          0.90260        1.14200        0.49200
C          0.85950        1.98090       -0.62510
O          0.35460       -0.61490       -0.80290
C          1.49690       -1.28960       -0.90950
H         -2.73440       -0.71560        0.69380
H         -2.32610       -0.52520       -1.06130
H         -3.24070        0.70430       -0.20490
H          1.12970        1.44040       -1.53990
H          1.58490        2.80770       -0.49120
H         -0.18940        2.35110       -0.68690
H          1.70120       -2.14800       -0.26230
H          1.54670       -1.71050       -1.96830
H          2.43060       -0.66660       -0.86520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

