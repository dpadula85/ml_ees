%nproc=16
%mem=2GB
%chk=mol_476_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93750       -1.31910       -0.16120
O         -0.65610       -1.14180       -0.52420
C          0.03970       -0.02320       -0.15110
C          0.13740        0.15540        1.31130
F         -1.11090        0.32880        1.86250
F          0.54920       -1.07950        1.92480
F          0.95250        1.15190        1.74740
O         -0.67750        1.04280       -0.70420
C         -0.24810        2.29650       -0.51570
O          1.24420       -0.02600       -0.82480
C          2.08230       -1.05650       -0.63280
H         -2.25570       -2.30330       -0.66690
H         -2.67490       -0.58760       -0.47720
H         -2.11140       -1.58540        0.92900
H          0.78550        2.53280       -0.84280
H         -0.40550        2.62160        0.51930
H         -0.96580        2.99240       -1.09870
H          3.00870       -0.89820       -1.26460
H          1.73840       -2.05830       -0.84560
H          2.50550       -1.04310        0.41530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

