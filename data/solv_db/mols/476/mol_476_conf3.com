%nproc=16
%mem=2GB
%chk=mol_476_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.70190        1.50360       -1.31510
O          0.11290        0.52830       -0.87480
C         -0.07760       -0.06040        0.33070
C         -0.18960        0.81060        1.54170
F         -1.28170        1.67300        1.48250
F         -0.27840        0.00960        2.64510
F          0.89230        1.65670        1.73160
O          0.90850       -1.01310        0.50320
C          2.19820       -0.60020        0.48070
O         -1.34900       -0.73380        0.28150
C         -1.32650       -1.68660       -0.73030
H         -0.68870        2.40980       -0.68450
H         -0.45670        1.85270       -2.36400
H         -1.77710        1.21860       -1.39350
H          2.48050       -0.07850       -0.48410
H          2.88910       -1.51500        0.48570
H          2.56930        0.02030        1.30090
H         -0.50720       -2.43020       -0.51290
H         -2.26180       -2.27930       -0.69660
H         -1.15460       -1.28620       -1.72780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

