%nproc=16
%mem=2GB
%chk=mol_476_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.44370       -1.78330       -0.45720
O         -1.21270       -0.46820       -0.72090
C         -0.06850        0.08200       -0.19830
C          0.14250       -0.25110        1.22610
F          1.25200        0.37270        1.79980
F          0.39000       -1.65120        1.34470
F         -0.94900       -0.03140        2.02720
O          0.07710        1.43060       -0.50630
C         -0.92190        2.24200       -0.06140
O          0.96890       -0.51210       -0.96960
C          2.22290       -0.13380       -0.65120
H         -2.37750       -2.07520       -1.03410
H         -0.66570       -2.48550       -0.75520
H         -1.78220       -1.96500        0.59920
H         -1.05710        2.24250        1.03620
H         -1.91110        2.05500       -0.52190
H         -0.64980        3.28630       -0.36030
H          2.89100       -0.64790       -1.44500
H          2.63100       -0.62920        0.27230
H          2.46370        0.92280       -0.62430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_476_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

