%nproc=16
%mem=2GB
%chk=mol_582_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32910       -0.35670        0.49570
C         -0.51730       -1.26000       -0.44000
C          0.87660       -0.69160       -0.35680
C          0.77670        0.65740        0.29170
C         -0.70420        0.99410        0.12100
O          1.53330        1.63220       -0.33820
H         -1.08350       -0.52900        1.54880
H         -2.40060       -0.37370        0.25770
H         -0.58520       -2.31100       -0.16560
H         -0.90220       -1.03240       -1.45910
H          1.31070       -0.52910       -1.37660
H          1.54650       -1.35840        0.22560
H          0.98950        0.54740        1.36330
H         -1.03500        1.79970        0.77090
H         -0.94110        1.15140       -0.94590
H          2.46490        1.65990        0.00740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

