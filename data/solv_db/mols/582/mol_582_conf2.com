%nproc=16
%mem=2GB
%chk=mol_582_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.37100       -1.16820        0.51980
C          1.04150       -0.78820        0.16720
C          0.90640        0.44910       -0.70250
C         -0.42410        1.04430       -0.32140
C         -1.28530       -0.20250       -0.18860
O         -0.41500        1.66830        0.90910
H         -0.48670       -0.98170        1.61700
H         -0.58860       -2.21510        0.27420
H          1.58590       -1.60550       -0.36910
H          1.65630       -0.53730        1.06860
H          1.75380        1.12370       -0.57930
H          0.79410        0.06990       -1.75020
H         -0.82730        1.68660       -1.13760
H         -2.14850        0.01380        0.46070
H         -1.64920       -0.56080       -1.15680
H          0.45800        2.00360        1.18890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

