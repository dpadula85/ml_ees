%nproc=16
%mem=2GB
%chk=mol_582_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.74280       -1.21530       -0.01090
C          0.57400       -1.06990        0.76300
C          1.22190        0.02210       -0.10260
C          0.11210        1.05530       -0.00470
C         -1.07310        0.20370       -0.44110
O          0.31230        2.15150       -0.79230
H         -1.53060       -1.66160        0.59380
H         -0.49180       -1.84610       -0.91060
H          1.15350       -1.98590        0.78160
H          0.36150       -0.62950        1.76260
H          2.17110        0.35900        0.28630
H          1.23360       -0.38460       -1.12730
H         -0.02410        1.27090        1.07230
H         -2.01700        0.53550        0.02150
H         -1.10490        0.26870       -1.54540
H         -0.15570        2.92620       -0.34600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

