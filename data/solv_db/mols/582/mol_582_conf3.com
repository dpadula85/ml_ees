%nproc=16
%mem=2GB
%chk=mol_582_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.89720        0.90890        0.45680
C         -1.34610       -0.43920       -0.12200
C         -0.06230       -1.22830        0.02280
C          0.93220       -0.26270       -0.62280
C          0.48350        1.06840       -0.10070
O          2.20910       -0.53970       -0.16800
H         -1.60190        1.69550        0.17340
H         -0.88490        0.81810        1.56320
H         -2.16100       -0.79720        0.49990
H         -1.60060       -0.26710       -1.17630
H          0.16110       -1.35620        1.09680
H         -0.07800       -2.19600       -0.47570
H          0.86630       -0.37310       -1.71100
H          0.55160        1.81400       -0.91490
H          1.19010        1.42690        0.69200
H          2.23800       -0.27260        0.78650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

