%nproc=16
%mem=2GB
%chk=mol_582_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.68440        1.13010        0.20860
C         -1.37510       -0.23800        0.13970
C         -0.19250       -1.11650        0.57300
C          0.87160       -0.61660       -0.40820
C          0.50820        0.81990       -0.70900
O          2.12990       -0.74750        0.15340
H         -0.36640        1.25250        1.25700
H         -1.30620        1.93300       -0.17410
H         -2.22370       -0.31690        0.80500
H         -1.56170       -0.44090       -0.92020
H         -0.45450       -2.15770        0.41940
H          0.10550       -0.84180        1.59520
H          0.78430       -1.22040       -1.34620
H          0.08420        0.93870       -1.74100
H          1.29820        1.53490       -0.47640
H          2.38250        0.08720        0.62390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_582_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

