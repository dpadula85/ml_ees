%nproc=16
%mem=2GB
%chk=mol_571_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.60650       -0.49570       -0.00180
C         -1.49170        0.86780        0.16950
C         -0.23700        1.44020        0.19480
C          0.91260        0.66150        0.04990
C          0.81330       -0.70190       -0.12200
C         -0.45070       -1.25510       -0.14440
O          1.93200       -1.52220       -0.27100
I          2.83400        1.49700        0.08370
H         -2.57210       -1.00020       -0.02940
H         -2.36670        1.46320        0.28020
H         -0.10400        2.49730        0.32590
H         -0.51880       -2.34520       -0.28230
H          2.85560       -1.10660       -0.25310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_571_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_571_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_571_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

