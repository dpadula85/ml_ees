%nproc=16
%mem=2GB
%chk=mol_571_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.27620       -0.89600       -0.03580
C         -1.67440        0.42450       -0.01450
C         -0.68570        1.38600        0.01870
C          0.64910        1.04030        0.03010
C          1.05200       -0.27610        0.00890
C          0.06060       -1.23440       -0.02430
O          2.41450       -0.62260        0.02060
I          2.13700        2.52370        0.08110
H         -2.05160       -1.65240       -0.06200
H         -2.72420        0.71080       -0.02300
H         -1.00660        2.42970        0.03540
H          0.38930       -2.26890       -0.04080
H          2.71630       -1.56470        0.00560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_571_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_571_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_571_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

