%nproc=16
%mem=2GB
%chk=mol_599_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.73630       -0.14010       -0.01140
C          0.41590        0.50380        0.32090
C         -0.76100       -0.31530       -0.12940
N         -1.96960        0.38800        0.23580
H          1.78080       -1.14810        0.44480
H          2.52660        0.45840        0.51960
H          1.92530       -0.25570       -1.07960
H          0.36980        0.58570        1.42640
H          0.38720        1.53900       -0.07590
H         -0.78190       -0.48870       -1.21580
H         -0.71800       -1.28410        0.37140
H         -2.15500        0.20960        1.25500
H         -2.75660       -0.05250       -0.32400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

