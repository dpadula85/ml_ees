%nproc=16
%mem=2GB
%chk=mol_599_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.66740       -0.04110       -0.02530
C          0.24470        0.20930        0.40630
C         -0.76480       -0.32230       -0.58970
N         -2.08470       -0.00870       -0.04000
H          1.75310       -0.41210       -1.06250
H          2.18190       -0.79610        0.61640
H          2.25880        0.89650        0.09300
H          0.05670        1.30900        0.47680
H          0.08940       -0.28950        1.38300
H         -0.64720       -1.41340       -0.72820
H         -0.61240        0.21240       -1.56670
H         -2.07800       -0.39170        0.93930
H         -2.06490        1.04760        0.09770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

