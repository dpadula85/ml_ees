%nproc=16
%mem=2GB
%chk=mol_599_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.33830       -0.03190        0.57450
C          0.52130        0.07340       -0.67670
C         -0.90040       -0.35550       -0.49880
N         -1.64410        0.37090        0.45280
H          0.87200        0.42810        1.45670
H          1.65620       -1.08750        0.75420
H          2.29130        0.54250        0.38420
H          0.57550        1.10110       -1.11300
H          0.98250       -0.63980       -1.42000
H         -1.40020       -0.26790       -1.49100
H         -0.90980       -1.43150       -0.23350
H         -1.55820        1.38360        0.45510
H         -1.82440       -0.08550        1.35540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

