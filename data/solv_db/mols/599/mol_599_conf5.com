%nproc=16
%mem=2GB
%chk=mol_599_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.48440        0.21320        0.15030
C         -0.39730       -0.50050       -0.60280
C          0.91880       -0.43710        0.13570
N          1.39260        0.88730        0.35450
H         -1.26120        0.13030        1.24270
H         -1.52820        1.24350       -0.19130
H         -2.45660       -0.32440        0.00560
H         -0.67690       -1.57140       -0.68580
H         -0.30230       -0.13460       -1.65490
H          0.72330       -0.89790        1.14220
H          1.62380       -1.09240       -0.42110
H          2.43820        0.87550        0.27220
H          1.01020        1.60870       -0.28570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

