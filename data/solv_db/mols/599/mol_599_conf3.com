%nproc=16
%mem=2GB
%chk=mol_599_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.68130        0.11570       -0.00910
C          0.27780        0.47690        0.44840
C         -0.63590       -0.49870       -0.29370
N         -1.99370       -0.24270        0.06870
H          1.65650       -0.24470       -1.06250
H          1.97170       -0.78110        0.60880
H          2.39560        0.94240        0.12910
H          0.12530        0.28340        1.52610
H          0.00200        1.51510        0.15050
H         -0.43510       -0.32580       -1.37510
H         -0.31130       -1.53140       -0.08140
H         -2.59850       -0.29020       -0.77530
H         -2.13560        0.58120        0.66560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_599_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

