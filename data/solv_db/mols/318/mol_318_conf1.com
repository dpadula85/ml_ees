%nproc=16
%mem=2GB
%chk=mol_318_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97930       -1.64210       -0.07770
C         -1.88930       -0.62700        0.13000
C         -1.40170        0.66060        0.21840
C         -0.05310        0.93610        0.10480
C          0.84910       -0.08460       -0.10240
C          0.36990       -1.37460       -0.19230
N          2.23170        0.22820       -0.21170
O          2.97540       -0.52550       -0.87710
O          2.80290        1.32610        0.38120
O          0.44730        2.22820        0.19270
H         -1.32000       -2.66850       -0.15340
H         -2.95720       -0.83480        0.22110
H         -2.07220        1.49360        0.38260
H          1.11920       -2.15590       -0.35860
H         -0.12270        3.04050        0.34240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

