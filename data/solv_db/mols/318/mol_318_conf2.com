%nproc=16
%mem=2GB
%chk=mol_318_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.70380        0.83440        0.01590
C          2.01730       -0.49030       -0.21340
C          0.95790       -1.35740       -0.30610
C         -0.38290       -0.94010       -0.16580
C         -0.67960        0.37610        0.08710
C          0.41220        1.27190        0.16700
N         -1.97740        0.88210        0.36200
O         -2.16970        2.12360        0.15400
O         -2.99270        0.19760        0.86860
O         -1.38270       -1.88940       -0.31660
H          2.51730        1.54970        0.07760
H          3.04720       -0.80230       -0.30860
H          1.10520       -2.41490       -0.49640
H          0.16310        2.31560        0.35070
H         -2.33910       -1.65650       -0.27600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

