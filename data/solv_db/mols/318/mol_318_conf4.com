%nproc=16
%mem=2GB
%chk=mol_318_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.42690       -1.23090        0.24840
C         -1.97100        0.04350        0.26720
C         -1.12270        1.12490        0.10260
C          0.24200        0.94430       -0.07800
C          0.77500       -0.33440       -0.09460
C         -0.06530       -1.42070        0.06870
N          2.16600       -0.57310       -0.26480
O          2.68630       -1.61090        0.23970
O          3.01370        0.25290       -0.93830
O          1.12910        2.00300       -0.24860
H         -2.10700       -2.06120        0.38460
H         -3.02980        0.20480        0.40380
H         -1.52930        2.12920        0.11060
H          0.35940       -2.43570        0.05860
H          0.88040        2.96430       -0.25990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

