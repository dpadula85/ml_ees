%nproc=16
%mem=2GB
%chk=mol_318_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.77220       -0.59030       -0.24660
C         -1.82540        0.74580        0.08410
C         -0.63890        1.40800        0.31070
C          0.55720        0.73370        0.20460
C          0.62920       -0.60230       -0.12530
C         -0.56810       -1.25210       -0.34980
N          1.88080       -1.25490       -0.21340
O          2.89770       -0.66200       -0.58470
O          1.95900       -2.57430        0.11660
O          1.72490        1.42120        0.43710
H         -2.67570       -1.15120       -0.43270
H         -2.79220        1.23770        0.15580
H         -0.65970        2.44350        0.56860
H         -0.48660       -2.30520       -0.60960
H          1.77020        2.40250        0.68450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_318_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

