%nproc=16
%mem=2GB
%chk=mol_211_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.35680        1.12840        0.27940
C          1.02250        0.76550       -0.11230
C          1.34280       -0.67800       -0.13530
O          0.30980       -1.53180       -0.36310
C         -0.87880       -1.22860        0.22820
C         -1.34660        0.12780       -0.31310
H         -0.62450        2.14250       -0.10480
H         -0.50010        1.12720        1.38820
H          1.72970        1.29140        0.58400
H          1.22950        1.23120       -1.11560
H          1.92730       -0.94460        0.79420
H          2.09160       -0.84670       -0.96680
H         -1.61940       -2.00090       -0.04570
H         -0.74260       -1.10390        1.31550
H         -1.20140        0.18250       -1.41580
H         -2.38300        0.33790       -0.01690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

