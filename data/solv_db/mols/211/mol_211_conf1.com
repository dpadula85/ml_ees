%nproc=16
%mem=2GB
%chk=mol_211_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.20540       -0.66930        1.01420
C          0.99930       -0.82270        0.08590
C          1.24080        0.36300       -0.75750
O          0.21580        1.16870       -1.06580
C         -0.88540        1.19250       -0.28680
C         -1.32000       -0.21120        0.05820
H         -0.45530       -1.60750        1.49490
H         -0.06040        0.13770        1.74150
H          1.84630       -1.10170        0.74120
H          0.73760       -1.69520       -0.58230
H          1.70310       -0.00340       -1.72420
H          2.07770        0.95780       -0.28440
H         -0.65030        1.71560        0.68400
H         -1.70570        1.72390       -0.78860
H         -2.30350       -0.27330        0.51620
H         -1.23470       -0.87500       -0.84650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

