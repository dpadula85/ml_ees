%nproc=16
%mem=2GB
%chk=mol_211_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.48730       -1.15160       -0.01300
C          1.32090        0.07320        0.12440
C          0.67810        1.33230       -0.34060
O         -0.58900        1.13220       -0.87190
C         -1.43550        0.46600       -0.00620
C         -0.97100       -0.95430        0.19510
H          0.67770       -1.66200       -0.99240
H          0.84040       -1.87940        0.77540
H          1.69860        0.22730        1.16020
H          2.24680       -0.08190       -0.49620
H          1.27010        1.83910       -1.15930
H          0.57010        2.09580        0.47910
H         -2.44950        0.47450       -0.48490
H         -1.50100        1.00740        0.97150
H         -1.53870       -1.58730       -0.54020
H         -1.30530       -1.33150        1.19910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

