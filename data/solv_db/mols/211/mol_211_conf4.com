%nproc=16
%mem=2GB
%chk=mol_211_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.09050        1.24970       -0.03600
C         -1.23500        0.57710        0.03250
C         -1.17370       -0.91550       -0.10950
O         -0.15240       -1.51970        0.54960
C          1.08950       -1.01550        0.39730
C          1.22840        0.29860       -0.25180
H          0.08370        1.94890       -0.91490
H          0.27600        1.88450        0.85460
H         -1.73020        0.82110        1.00780
H         -1.94970        0.94730       -0.74600
H         -1.26160       -1.20240       -1.19400
H         -2.13310       -1.30580        0.34540
H          1.71150       -1.76480       -0.17590
H          1.58480       -0.99240        1.41360
H          1.42470        0.18460       -1.34550
H          2.14670        0.80420        0.17270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

