%nproc=16
%mem=2GB
%chk=mol_211_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.27020       -1.02010        0.65490
C         -0.90380       -0.95070       -0.32750
C         -1.38790        0.48510       -0.30340
O         -0.39970        1.23990       -0.96180
C          0.74180        1.27760       -0.14200
C          1.33370       -0.08950        0.06770
H         -0.10680       -0.61440        1.62100
H          0.64880       -2.04140        0.75050
H         -0.45080       -1.12100       -1.33880
H         -1.67820       -1.68160       -0.10030
H         -2.37670        0.62330       -0.74060
H         -1.38910        0.87200        0.75050
H          0.45680        1.67750        0.83720
H          1.48490        1.88720       -0.68870
H          1.63300       -0.53430       -0.89140
H          2.12380       -0.00950        0.81270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_211_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

