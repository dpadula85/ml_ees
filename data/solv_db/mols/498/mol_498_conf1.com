%nproc=16
%mem=2GB
%chk=mol_498_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.34990       -1.00860       -0.52160
C         -0.02450       -1.41740       -0.36840
C          0.92220       -0.50140        0.04300
C          0.55210        0.80590        0.29860
C         -0.76090        1.23810        0.15370
C         -1.67780        0.29360       -0.25910
O         -1.14850        2.53890        0.40420
C          2.32100       -0.86550        0.22130
O          3.18660       -0.03830        0.59510
H         -2.07210       -1.75810       -0.84840
H          0.29140       -2.43740       -0.56220
H          1.27510        1.55170        0.62410
H         -2.71480        0.57650       -0.38810
H         -1.46270        2.89280        1.29130
H          2.66270       -1.87090        0.03540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

