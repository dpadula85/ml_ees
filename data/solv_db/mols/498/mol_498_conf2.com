%nproc=16
%mem=2GB
%chk=mol_498_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.40500        1.67400        0.26300
C          0.85580        1.11670        0.33300
C          0.98820       -0.22550        0.04130
C         -0.10090       -0.99550       -0.31070
C         -1.36330       -0.43720       -0.38070
C         -1.49650        0.90400       -0.08940
O         -2.43260       -1.24320       -0.73790
C          2.33380       -0.80980        0.11840
O          3.32660       -0.14360        0.43110
H         -0.51270        2.74570        0.49550
H          1.71240        1.74520        0.61500
H          0.04840       -2.06370       -0.53570
H         -2.48490        1.32260       -0.14870
H         -2.94370       -1.72750        0.01050
H          2.47450       -1.86210       -0.10450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

