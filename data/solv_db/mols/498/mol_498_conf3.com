%nproc=16
%mem=2GB
%chk=mol_498_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.62840        1.54030        0.42050
C          0.70010        1.16620        0.31630
C          1.09420       -0.10800       -0.03130
C          0.09190       -1.02090       -0.27800
C         -1.23680       -0.65980       -0.17730
C         -1.62130        0.61800        0.17120
O         -2.20830       -1.62960       -0.43940
C          2.51500       -0.44920       -0.12720
O          2.90410       -1.60180       -0.44190
H         -0.89240        2.55250        0.69620
H          1.49740        1.88050        0.50960
H          0.40210       -2.03800       -0.55510
H         -2.67310        0.88350        0.24540
H         -3.18930       -1.45360       -0.38970
H          3.24490        0.31980        0.08070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

