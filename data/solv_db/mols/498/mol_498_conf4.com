%nproc=16
%mem=2GB
%chk=mol_498_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77270        1.51660        0.38420
C          0.57620        1.23620        0.34340
C          1.07700        0.00720       -0.02330
C          0.13520       -0.96600       -0.36010
C         -1.21780       -0.69710       -0.32300
C         -1.68950        0.54480        0.04870
O         -2.16830       -1.65300       -0.65470
C          2.49950       -0.24960       -0.05440
O          2.98260       -1.35130       -0.38190
H         -1.10590        2.49540        0.67880
H          1.31130        1.99260        0.60530
H          0.49430       -1.92420       -0.64720
H         -2.76910        0.74730        0.07470
H         -2.51730       -2.24260        0.09320
H          3.16450        0.54370        0.21650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_498_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

