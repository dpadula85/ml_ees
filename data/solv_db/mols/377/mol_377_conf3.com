%nproc=16
%mem=2GB
%chk=mol_377_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30690       -1.50700        0.73380
C          1.41970       -0.41520        0.24750
C          1.92130        0.78970       -0.20680
C          1.07940        1.78740       -0.65330
C         -0.29120        1.64010       -0.67080
C         -0.79670        0.43980       -0.21850
C          0.04790       -0.55640        0.22750
O         -2.16150        0.27910       -0.23040
C         -2.84300       -0.86580        0.18970
O         -1.11460        2.66960       -1.12890
H          3.34990       -1.31310        0.44240
H          1.91410       -2.44860        0.29240
H          2.26700       -1.53100        1.84650
H          3.00600        0.92120       -0.19890
H          1.48350        2.72930       -1.00780
H         -0.33870       -1.51700        0.59110
H         -3.82940       -0.98160       -0.33360
H         -2.24710       -1.77560       -0.07930
H         -3.05740       -0.86410        1.28190
H         -2.11600        2.51950       -1.12460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

