%nproc=16
%mem=2GB
%chk=mol_377_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51390       -1.27310        0.11360
C         -1.41780       -0.25620        0.04270
C         -1.68130        1.09390       -0.02320
C         -0.65050        2.01260       -0.08820
C          0.67690        1.61730       -0.08980
C          0.93890        0.26260       -0.02370
C         -0.09640       -0.65200        0.04120
O          2.27310       -0.11640       -0.02630
C          2.69760       -1.46600        0.03680
O          1.69790        2.54600       -0.15520
H         -3.48350       -0.78670       -0.10950
H         -2.52810       -1.78030        1.10030
H         -2.36760       -2.03190       -0.70310
H         -2.72370        1.40660       -0.02210
H         -0.86470        3.08060       -0.14020
H          0.12020       -1.71190        0.09280
H          2.00850       -2.10360       -0.58140
H          2.72160       -1.85750        1.07710
H          3.73140       -1.51290       -0.33950
H          1.46150        3.52900       -0.20240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

