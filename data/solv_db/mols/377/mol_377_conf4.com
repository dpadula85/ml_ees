%nproc=16
%mem=2GB
%chk=mol_377_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49580       -1.12070       -0.49450
C         -1.37040       -0.18320       -0.21890
C         -1.67100        1.14400       -0.10330
C         -0.62270        2.01810        0.15380
C          0.66410        1.54130        0.28350
C          0.93950        0.18370        0.16060
C         -0.08150       -0.68410       -0.09180
O          2.24420       -0.25750        0.29700
C          2.66750       -1.57590        0.20020
O          1.69260        2.42950        0.53950
H         -3.15830       -1.14010        0.41220
H         -3.13850       -0.74160       -1.32950
H         -2.15110       -2.11300       -0.79800
H         -2.68160        1.54120       -0.20270
H         -0.86380        3.07110        0.24510
H          0.05760       -1.73630       -0.19770
H          3.07080       -1.72990       -0.84100
H          1.91570       -2.33770        0.39500
H          3.48880       -1.71740        0.96240
H          1.49390        3.40880        0.62810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

