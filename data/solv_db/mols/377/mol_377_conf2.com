%nproc=16
%mem=2GB
%chk=mol_377_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.38110       -1.47910        0.03190
C         -1.42490       -0.34580        0.06270
C         -1.87370        0.94780        0.20200
C         -0.96200        1.97740        0.22670
C          0.38770        1.72080        0.11330
C          0.82710        0.42270       -0.02580
C         -0.07510       -0.61900       -0.05210
O          2.16840        0.17650       -0.13770
C          2.75450       -1.08490       -0.28370
O          1.25860        2.80150        0.14500
H         -2.02800       -2.14890       -0.80680
H         -2.23320       -2.08260        0.95210
H         -3.40790       -1.13560       -0.10500
H         -2.95630        1.14290        0.29190
H         -1.35940        2.98400        0.33860
H          0.24700       -1.64640       -0.16030
H          2.21580       -1.82090        0.34500
H          3.80210       -0.99950        0.12750
H          2.77370       -1.45330       -1.32840
H          2.26660        2.64240        0.06300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

