%nproc=16
%mem=2GB
%chk=mol_377_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.42550       -1.07100        0.88160
C         -1.35790       -0.17740        0.34360
C         -1.62500        1.11100       -0.07390
C         -0.63870        1.93640       -0.57080
C          0.66720        1.50880       -0.67370
C          0.95330        0.22410       -0.26140
C         -0.04670       -0.59820        0.23720
O          2.26900       -0.20790       -0.36470
C          2.57190       -1.52780        0.05710
O          1.69260        2.31040       -1.16980
H         -3.21780       -1.18760        0.08700
H         -1.96860       -2.04600        1.07540
H         -2.93430       -0.62470        1.75720
H         -2.66340        1.43470        0.01400
H         -0.87500        2.95450       -0.89590
H          0.19520       -1.61410        0.56050
H          1.84550       -2.19940       -0.48590
H          3.61450       -1.80640       -0.17020
H          2.40650       -1.67110        1.13530
H          1.53700        3.25170       -1.48270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_377_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

