%nproc=16
%mem=2GB
%chk=mol_238_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.12260        1.84700        0.06560
C          1.27260        1.07240        0.06670
C          1.21210       -0.31600        0.01850
C         -0.03570       -0.88610       -0.03020
C         -1.20710       -0.18310       -0.03420
C         -1.10020        1.20830        0.01520
N         -2.48940       -0.79360       -0.08490
O         -3.55030       -0.14780       -0.08810
O         -2.55110       -2.16260       -0.13240
N          2.39570       -1.10590        0.01960
H          0.17140        2.92080        0.10280
H          2.23380        1.54330        0.10540
H         -0.12880       -1.98030       -0.06930
H         -2.02860        1.78820        0.01280
H          2.72920       -1.58890        0.88490
H          2.95360       -1.21560       -0.85250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

