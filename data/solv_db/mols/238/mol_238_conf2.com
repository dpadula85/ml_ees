%nproc=16
%mem=2GB
%chk=mol_238_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.02840        1.65910        0.70810
C          1.20620        1.15610        0.32010
C          1.20230       -0.11430       -0.21900
C          0.04760       -0.85420       -0.37020
C         -1.18250       -0.31990        0.03050
C         -1.18790        0.93810        0.56560
N         -2.38280       -1.07380       -0.12010
O         -3.45010       -0.55330        0.25180
O         -2.36130       -2.34540       -0.66330
N          2.43640       -0.66510       -0.62700
H         -0.08270        2.64870        1.13630
H          2.11130        1.73960        0.44010
H          0.07930       -1.85040       -0.79780
H         -2.15310        1.35400        0.87850
H          2.61310       -0.81350       -1.63000
H          3.13250       -0.90560        0.09650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

