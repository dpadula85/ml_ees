%nproc=16
%mem=2GB
%chk=mol_238_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.32490        1.79150       -0.19830
C         -1.35380        0.89230       -0.20440
C         -1.17070       -0.47070       -0.06340
C          0.13210       -0.90330        0.08880
C          1.20770       -0.03810        0.10220
C          0.96730        1.33090       -0.04440
N          2.52900       -0.51850        0.26050
O          2.72640       -1.73530        0.38940
O          3.59740        0.33390        0.27440
N         -2.25880       -1.37700       -0.07390
H         -0.46770        2.86630       -0.30950
H         -2.37280        1.27290       -0.32710
H          0.30980       -1.98160        0.20310
H          1.83150        1.97430       -0.02880
H         -2.69720       -1.61640        0.84910
H         -2.65530       -1.82110       -0.91770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

