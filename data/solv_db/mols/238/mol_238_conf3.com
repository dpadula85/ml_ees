%nproc=16
%mem=2GB
%chk=mol_238_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.34360       -1.73130        0.45330
C          0.95150       -1.32960        0.18360
C          1.26350       -0.02230       -0.16610
C          0.19260        0.85610       -0.22780
C         -1.10070        0.49900        0.03110
C         -1.37000       -0.80450        0.37410
N         -2.15750        1.43310       -0.04550
O         -2.02620        2.63280       -0.34570
O         -3.42490        0.99520        0.22910
N          2.60150        0.39160       -0.44470
H         -0.59960       -2.75020        0.72750
H          1.75550       -2.04700        0.24360
H          0.40280        1.88300       -0.49760
H         -2.39760       -1.12040        0.58730
H          3.20230       -0.08370       -1.16330
H          3.05040        1.19800        0.06100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

