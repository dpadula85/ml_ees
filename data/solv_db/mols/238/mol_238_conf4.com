%nproc=16
%mem=2GB
%chk=mol_238_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.22220        1.64770        0.78280
C         -1.30430        0.95990        0.28250
C         -1.18230       -0.28970       -0.31680
C          0.08010       -0.82150       -0.39220
C          1.19770       -0.18470        0.08820
C          1.02510        1.06930        0.68220
N          2.49570       -0.74620        0.00350
O          2.63050       -1.86150       -0.52750
O          3.58090       -0.07220        0.49760
N         -2.33110       -0.97260       -0.82470
H         -0.29180        2.61950        1.25270
H         -2.28550        1.38760        0.34820
H          0.18090       -1.81050       -0.86580
H          1.91290        1.54690        1.04980
H         -2.20390       -1.58940       -1.64320
H         -3.28280       -0.88260       -0.41740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_238_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

