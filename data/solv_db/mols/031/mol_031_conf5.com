%nproc=16
%mem=2GB
%chk=mol_031_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.98420       -0.05500        0.02770
C          0.49450       -0.01220        0.01650
C         -0.26360       -1.15700       -0.16730
C         -1.65050       -1.11660       -0.17760
N         -2.29180        0.05460       -0.00640
C         -1.58290        1.18750        0.17500
C         -0.20040        1.16850        0.18860
H          2.36410        0.85740        0.52650
H          2.35640        0.00130       -1.03370
H          2.38620       -0.99170        0.43370
H          0.25100       -2.11910       -0.30820
H         -2.18220       -2.04730       -0.32600
H         -2.06510        2.16280        0.31830
H          0.40010        2.06680        0.33290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

