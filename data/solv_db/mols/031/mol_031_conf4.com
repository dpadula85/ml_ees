%nproc=16
%mem=2GB
%chk=mol_031_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.98290       -0.04810        0.16300
C          0.48940       -0.02300        0.04500
C         -0.22410        1.15720        0.11120
C         -1.60440        1.20830        0.00520
N         -2.24480        0.03670       -0.16940
C         -1.57360       -1.13630       -0.23810
C         -0.19370       -1.20660       -0.13420
H          2.36190       -0.11230       -0.88990
H          2.34580       -0.85420        0.80160
H          2.29250        0.94260        0.56310
H          0.33660        2.06670        0.25170
H         -2.15760        2.15560        0.05970
H         -2.15360       -2.03610       -0.37900
H          0.34260       -2.15050       -0.18980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

