%nproc=16
%mem=2GB
%chk=mol_031_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96180       -0.20940        0.08470
C          0.49650       -0.02640        0.02820
C         -0.31440       -1.14130       -0.00010
C         -1.69230       -0.98700       -0.05350
N         -2.24910        0.23930       -0.07820
C         -1.49590        1.36140       -0.05210
C         -0.12050        1.20000        0.00120
H          2.27740       -0.92560       -0.72630
H          2.31300       -0.66030        1.03400
H          2.43760        0.77200       -0.13380
H          0.08010       -2.16220        0.01770
H         -2.30490       -1.89080       -0.07480
H         -1.90750        2.35630       -0.07050
H          0.51820        2.07400        0.02340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

