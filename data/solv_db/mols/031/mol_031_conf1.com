%nproc=16
%mem=2GB
%chk=mol_031_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96210        0.01820        0.04030
C          0.47900        0.01850        0.00860
C         -0.25970        1.18710        0.01680
C         -1.64070        1.18670       -0.01310
N         -2.24630       -0.01580       -0.05100
C         -1.54770       -1.16500       -0.05960
C         -0.16690       -1.20150       -0.03050
H          2.39550       -0.74150       -0.66190
H          2.37480        1.01120       -0.21570
H          2.30640       -0.26830        1.05370
H          0.23510        2.15230        0.04760
H         -2.23030        2.11020       -0.00690
H         -2.04490       -2.13840       -0.09040
H          0.38350       -2.15380       -0.03790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

