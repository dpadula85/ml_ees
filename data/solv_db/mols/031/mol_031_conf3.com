%nproc=16
%mem=2GB
%chk=mol_031_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95530       -0.10620       -0.07340
C          0.47460       -0.01860       -0.01910
C         -0.25350       -1.18210       -0.08780
C         -1.63610       -1.08910       -0.03640
N         -2.22400        0.11220        0.07710
C         -1.54590        1.27320        0.14720
C         -0.17240        1.19330        0.09710
H          2.43770        0.86070        0.19410
H          2.32900       -0.36990       -1.08790
H          2.35900       -0.84930        0.66700
H          0.18690       -2.16980       -0.17980
H         -2.21380       -2.02080       -0.09170
H         -2.07570        2.23140        0.23960
H          0.37880        2.13480        0.15390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_031_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

