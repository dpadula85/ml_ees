%nproc=16
%mem=2GB
%chk=mol_215_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.47250       -0.34230       -0.05690
C          1.15900        0.33570        0.00800
C          1.10270        1.71360        0.10980
C         -0.11400        2.36290        0.17120
C         -1.26000        1.59440        0.12770
C         -1.24450        0.21780        0.02640
C         -0.00530       -0.41860       -0.03430
C          0.12390       -1.87330       -0.14290
C         -2.48200       -0.59890       -0.01980
H          2.94630       -0.29070       -1.05280
H          2.41200       -1.40080        0.25320
H          3.17580        0.14400        0.66810
H          2.02190        2.28590        0.14140
H         -0.19090        3.45620        0.25220
H         -2.22910        2.09760        0.17560
H         -0.79110       -2.39350       -0.47200
H          0.42350       -2.35150        0.82230
H          0.90800       -2.14530       -0.91060
H         -2.75180       -0.79900       -1.08130
H         -2.33750       -1.52770        0.53790
H         -3.33940       -0.06650        0.47690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

