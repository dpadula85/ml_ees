%nproc=16
%mem=2GB
%chk=mol_215_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36880       -0.83880        0.19780
C          1.22090        0.10300        0.13850
C          1.41960        1.45950        0.24780
C          0.35870        2.32750        0.19290
C         -0.91230        1.80930        0.02550
C         -1.16110        0.45300       -0.08900
C         -0.06440       -0.38830       -0.02860
C         -0.28330       -1.85500       -0.14670
C         -2.50350       -0.12490       -0.26800
H          2.77290       -0.91490        1.23090
H          2.11510       -1.82370       -0.24030
H          3.14650       -0.40610       -0.49670
H          2.42700        1.82860        0.37770
H          0.52370        3.40140        0.28020
H         -1.75540        2.48700       -0.01890
H          0.35300       -2.37930        0.62450
H         -1.32630       -2.13650        0.11380
H         -0.01980       -2.25750       -1.12280
H         -3.24560        0.68890       -0.47850
H         -2.55230       -0.78530       -1.17530
H         -2.88210       -0.64800        0.63510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

