%nproc=16
%mem=2GB
%chk=mol_215_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.50830        0.05500       -0.00810
C          1.11300        0.52180        0.00480
C          0.81720        1.85300        0.03370
C         -0.50780        2.30220        0.04620
C         -1.50080        1.35280        0.02820
C         -1.24150       -0.00510       -0.00120
C          0.08770       -0.39140       -0.01230
C          0.40980       -1.82380       -0.04380
C         -2.31370       -1.01920       -0.02010
H          2.99720        0.45990        0.92700
H          2.61900       -1.02760        0.01530
H          3.09780        0.53650       -0.84080
H          1.57550        2.60630        0.04810
H         -0.70180        3.37330        0.06930
H         -2.54060        1.68030        0.03740
H          1.11890       -2.08010       -0.88100
H          0.85770       -2.18820        0.90420
H         -0.49380       -2.45480       -0.25040
H         -2.31240       -1.63860       -0.94010
H         -2.29190       -1.63200        0.90100
H         -3.29780       -0.48030       -0.01750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

