%nproc=16
%mem=2GB
%chk=mol_215_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.52930       -0.10240       -0.15190
C         -1.13940        0.42680       -0.02400
C         -0.93760        1.78730        0.10970
C          0.32340        2.32980        0.23140
C          1.41120        1.48040        0.21820
C          1.25650        0.11590        0.08680
C         -0.02850       -0.40090       -0.03400
C         -0.27980       -1.85590       -0.17900
C          2.41650       -0.82790        0.06920
H         -2.98350       -0.32200        0.83690
H         -2.55780       -1.04240       -0.74120
H         -3.17780        0.61790       -0.68070
H         -1.79670        2.45800        0.12020
H          0.44420        3.40930        0.33530
H          2.42420        1.84790        0.30980
H         -1.22590       -2.17000        0.32860
H         -0.23840       -2.16410       -1.22960
H          0.55370       -2.37710        0.34890
H          3.35950       -0.28180       -0.14360
H          2.26340       -1.51600       -0.80020
H          2.44200       -1.41280        0.98940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

