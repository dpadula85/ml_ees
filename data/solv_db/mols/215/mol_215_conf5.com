%nproc=16
%mem=2GB
%chk=mol_215_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51620       -0.05470        0.13340
C         -1.12210       -0.51170        0.03290
C         -0.79590       -1.84630       -0.04790
C          0.50050       -2.31060       -0.14300
C          1.50700       -1.36300       -0.15570
C          1.21960       -0.03390       -0.07730
C         -0.09650        0.39670        0.01730
C         -0.40000        1.83590        0.10160
C          2.29810        1.00360       -0.09000
H         -2.59330        0.87750        0.71160
H         -3.00490        0.09610       -0.84710
H         -3.06710       -0.84270        0.72580
H         -1.58820       -2.59000       -0.03790
H          0.72840       -3.38460       -0.20590
H          2.54690       -1.67870       -0.22940
H          0.36140        2.41330       -0.50010
H         -0.39890        2.21160        1.14340
H         -1.37920        2.09780       -0.38300
H          2.58780        1.29410       -1.11180
H          2.03620        1.87200        0.53520
H          3.17630        0.51740        0.42780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_215_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

