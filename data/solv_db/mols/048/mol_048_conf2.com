%nproc=16
%mem=2GB
%chk=mol_048_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.35320       -1.18190        0.20520
C         -1.02530       -1.08610        0.05390
C         -1.61570        0.15000       -0.11790
C         -0.79350        1.25370       -0.13160
C          0.58790        1.15530        0.02020
C          1.17480       -0.08170        0.19170
O          2.54330       -0.25970        0.34940
Br        -3.51190        0.33770       -0.33120
H          0.79500       -2.15290        0.33780
H         -1.66460       -1.96770        0.06650
H         -1.25200        2.22160       -0.26560
H          1.21440        2.01710        0.00790
H          3.19430       -0.40550       -0.38620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_048_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_048_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_048_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

