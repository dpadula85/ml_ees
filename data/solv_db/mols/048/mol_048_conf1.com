%nproc=16
%mem=2GB
%chk=mol_048_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.53080        1.14570       -0.07560
C          0.85450        1.24860        0.01040
C          1.60750        0.09980        0.03810
C          0.97540       -1.11830       -0.01960
C         -0.41550       -1.21700       -0.10600
C         -1.16970       -0.06840       -0.13380
O         -2.54850       -0.14010       -0.21890
Br         3.50480        0.19040        0.15510
H         -1.06930        2.08900       -0.09330
H          1.36110        2.20890        0.05640
H          1.51080       -2.07150       -0.00190
H         -0.89580       -2.18370       -0.15020
H         -3.18440       -0.18320        0.53940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_048_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_048_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_048_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

