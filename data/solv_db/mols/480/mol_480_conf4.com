%nproc=16
%mem=2GB
%chk=mol_480_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.25360       -0.22730       -0.07970
O          3.24550       -0.19010       -1.06060
C          1.92320       -0.11190       -0.72510
C          1.57800       -0.06880        0.60750
C          0.23340        0.01030        0.92680
C         -0.76860        0.04770       -0.01780
C         -0.38740        0.00290       -1.33720
C          0.93610       -0.07600       -1.71180
C         -2.18410        0.13190        0.38540
O         -2.52570        0.17220        1.58730
O         -3.18220        0.16890       -0.57490
C         -4.54800        0.25030       -0.18170
H          5.20020       -0.57430       -0.55400
H          4.04710       -0.93900        0.74180
H          4.42850        0.79940        0.32670
H          2.30450       -0.09410        1.38290
H         -0.01490        0.04320        1.98700
H         -1.16480        0.03170       -2.08420
H          1.27670       -0.11310       -2.73560
H         -4.65300        1.13280        0.45380
H         -5.19290        0.28520       -1.07610
H         -4.80510       -0.68200        0.36960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

