%nproc=16
%mem=2GB
%chk=mol_480_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.31280       -0.16080        0.08080
O         -3.28800        0.57930       -0.54680
C         -1.95060        0.32880       -0.28640
C         -1.54420       -0.66350        0.60560
C         -0.19700       -0.89050        0.84550
C          0.77340       -0.14390        0.21080
C          0.36830        0.83640       -0.67060
C         -0.97610        1.07210       -0.91810
C          2.20990       -0.37540        0.45770
O          2.56900       -1.25990        1.25290
O          3.14210        0.39600       -0.19990
C          4.52140        0.25330       -0.03330
H         -5.29030        0.37510       -0.03790
H         -4.34880       -1.15680       -0.41140
H         -4.04250       -0.24320        1.16450
H         -2.28060       -1.26970        1.12190
H          0.07300       -1.67200        1.54660
H          1.12440        1.42600       -1.17220
H         -1.31730        1.83460       -1.60420
H          4.82300       -0.77890        0.21570
H          5.08080        0.51600       -0.97100
H          4.86280        0.99700        0.72250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

