%nproc=16
%mem=2GB
%chk=mol_480_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.08770        0.85950       -0.19980
O         -3.45770       -0.28110        0.35460
C         -2.06660       -0.32410        0.36370
C         -1.28820        0.69470       -0.14300
C          0.10040        0.64950       -0.13280
C          0.74850       -0.45360        0.40290
C         -0.03410       -1.46470        0.90590
C         -1.40430       -1.42480        0.89810
C          2.20440       -0.50660        0.41620
O          2.74600       -1.52450        0.91110
O          2.99790        0.50570       -0.08750
C          4.41990        0.36300       -0.02990
H         -3.75970        1.75010        0.38860
H         -5.16970        0.76150       -0.26880
H         -3.64330        1.00740       -1.22310
H         -1.79230        1.54720       -0.55700
H          0.66310        1.45940       -0.53520
H          0.43970       -2.34280        1.33260
H         -2.01920       -2.23280        1.29990
H          4.85020        0.68450       -0.99870
H          4.70460       -0.68540        0.13280
H          4.84800        0.95770        0.80080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

