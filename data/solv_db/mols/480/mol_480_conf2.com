%nproc=16
%mem=2GB
%chk=mol_480_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.34820        0.08330        0.13050
O         -3.28340       -0.54830        0.77000
C         -1.94220       -0.29800        0.52400
C         -1.02400       -1.02530        1.25900
C          0.33190       -0.82380        1.06220
C          0.81080        0.08950        0.14610
C         -0.13240        0.79740       -0.56950
C         -1.48590        0.60620       -0.38300
C          2.23940        0.30650       -0.06590
O          2.59710        1.15690       -0.91930
O          3.18500       -0.39190        0.64040
C          4.56090       -0.12680        0.37980
H         -5.28320       -0.21540        0.67800
H         -4.32180        1.18540        0.17480
H         -4.41380       -0.23260       -0.92850
H         -1.34830       -1.76720        2.00340
H          1.08980       -1.38330        1.62780
H          0.25430        1.52460       -1.29890
H         -2.20380        1.17970       -0.96250
H          5.14250       -1.05090        0.59450
H          4.67990        0.20420       -0.67850
H          4.89540        0.72990        1.00860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

