%nproc=16
%mem=2GB
%chk=mol_480_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.17050       -0.05710        0.12220
O         -3.26800       -1.08050        0.48260
C         -1.89540       -0.80970        0.38280
C         -0.98200       -1.80550        0.73350
C          0.38850       -1.54920        0.63890
C          0.85510       -0.32670        0.20410
C         -0.06530        0.64300       -0.13730
C         -1.42450        0.41160       -0.05160
C          2.28620       -0.06340        0.10650
O          3.14990       -0.92290        0.40860
O          2.76500        1.16190       -0.32940
C          4.13350        1.40790       -0.42080
H         -5.20570       -0.30970        0.44460
H         -3.89910        0.85850        0.70380
H         -4.13000        0.18870       -0.96520
H         -1.36650       -2.75560        1.07130
H          1.08590       -2.30750        0.90590
H          0.32220        1.59430       -0.47560
H         -2.15790        1.15890       -0.31510
H          4.55250        1.23170       -1.42650
H          4.72580        0.84460        0.33780
H          4.30040        2.48660       -0.18250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_480_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

