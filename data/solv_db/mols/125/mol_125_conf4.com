%nproc=16
%mem=2GB
%chk=mol_125_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.91310        0.14750        0.02220
O         -1.75510        0.95350       -0.08860
C         -0.48530        0.37530       -0.03590
C         -0.32540       -0.99240        0.12530
C          0.94830       -1.54290        0.17430
C          2.08200       -0.77350        0.06770
C          1.91400        0.58120       -0.09170
C          0.65470        1.14190       -0.14210
O          3.06260        1.35970       -0.19950
H         -3.82260        0.67370       -0.31810
H         -2.82800       -0.77550       -0.62130
H         -3.06550       -0.20230        1.06580
H         -1.19310       -1.65760        0.21740
H          1.05940       -2.62440        0.30170
H          3.07190       -1.23920        0.11030
H          0.56320        2.20770       -0.26800
H          3.03200        2.36730       -0.31920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

