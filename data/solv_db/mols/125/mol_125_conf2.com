%nproc=16
%mem=2GB
%chk=mol_125_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.91080        0.01690       -0.19930
O         -1.81410        0.88490       -0.25050
C         -0.53670        0.37310       -0.05510
C         -0.30760       -0.97170        0.19000
C          0.97040       -1.47160        0.38350
C          2.06620       -0.62780        0.33610
C          1.86090        0.72520        0.09170
C          0.58010        1.20910       -0.09930
O          2.94530        1.61590        0.03550
H         -3.77740        0.52630       -0.71460
H         -2.75840       -0.92510       -0.77970
H         -3.20560       -0.24620        0.85790
H         -1.14670       -1.67030        0.23480
H          1.13430       -2.52680        0.57460
H          3.07910       -0.99750        0.48500
H          0.37870        2.25490       -0.29160
H          3.44240        1.83080       -0.79890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

