%nproc=16
%mem=2GB
%chk=mol_125_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.74430        0.45370        0.35730
O          1.91240       -0.68600        0.42010
C          0.56120       -0.59250        0.15740
C         -0.25660       -1.69380        0.21550
C         -1.61180       -1.56270       -0.05490
C         -2.14350       -0.32930       -0.38270
C         -1.34200        0.77760       -0.44490
C          0.01860        0.62020       -0.16850
O         -1.87130        2.00470       -0.77110
H          3.72740        0.23400        0.84380
H          2.84740        0.75500       -0.70060
H          2.30540        1.28380        0.97110
H          0.14310       -2.68230        0.47330
H         -2.23130       -2.46070        0.00100
H         -3.22320       -0.28140       -0.58790
H          0.65840        1.48900       -0.21510
H         -2.23850        2.67060       -0.11400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

