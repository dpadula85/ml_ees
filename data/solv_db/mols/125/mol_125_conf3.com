%nproc=16
%mem=2GB
%chk=mol_125_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.98620       -0.05250       -0.17770
O          1.79720       -0.80260       -0.21350
C          0.53270       -0.27670       -0.32070
C          0.23280        1.05370       -0.40910
C         -1.06950        1.48630       -0.51380
C         -2.13150        0.61580       -0.53520
C         -1.83030       -0.73650       -0.44550
C         -0.52670       -1.17070       -0.34070
O         -2.90990       -1.62750       -0.46690
H          3.23530        0.42450       -1.14780
H          3.80960       -0.77420        0.05690
H          2.99000        0.71270        0.62530
H          1.02700        1.76190       -0.39620
H         -1.33150        2.55280       -0.58590
H         -3.16830        0.92470       -0.61660
H         -0.34480       -2.24060       -0.27320
H         -3.29840       -1.85140        0.45410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

