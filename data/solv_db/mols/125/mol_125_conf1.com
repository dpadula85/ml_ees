%nproc=16
%mem=2GB
%chk=mol_125_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.86570       -0.43790       -0.19480
O         -1.65160       -1.14620       -0.24860
C         -0.43770       -0.46520       -0.05610
C         -0.45970        0.89280        0.18340
C          0.72240        1.57200        0.37360
C          1.94640        0.93340        0.33280
C          1.92260       -0.42780        0.09010
C          0.76120       -1.14070       -0.10500
O          3.14990       -1.11720        0.04040
H         -3.66120       -1.16330       -0.53290
H         -3.03120       -0.10500        0.85020
H         -2.89320        0.45180       -0.83680
H         -1.43030        1.41300        0.21820
H          0.70150        2.65370        0.56410
H          2.87640        1.47820        0.48440
H          0.75490       -2.21190       -0.29520
H          3.59540       -1.17960       -0.86780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_125_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

