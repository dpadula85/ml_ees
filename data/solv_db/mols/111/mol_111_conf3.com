%nproc=16
%mem=2GB
%chk=mol_111_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.74120       -0.01500       -0.15270
O         -2.38380        0.27530       -0.01300
C         -1.40620       -0.70510       -0.03700
O         -1.77650       -1.89260       -0.19120
C         -0.01590       -0.32730        0.11470
C          1.02080       -1.24280        0.10260
C          2.33640       -0.87250        0.24720
C          2.65940        0.47340        0.41390
C          1.64210        1.40180        0.42890
C          0.34160        1.00250        0.28240
O          3.99230        0.84380        0.56000
H         -4.29480        0.30170        0.76920
H         -4.18130        0.62860       -0.97030
H         -3.95850       -1.05060       -0.45060
H          0.80380       -2.29160       -0.02430
H          3.14530       -1.57230        0.23900
H          1.87440        2.43480        0.55540
H         -0.44070        1.72660        0.29480
H          4.38270        0.88140        1.47310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

