%nproc=16
%mem=2GB
%chk=mol_111_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.75050        0.15160        0.23890
O         -2.35260        0.31390        0.21150
C         -1.45070       -0.69970        0.39530
O         -1.85820       -1.86370        0.60450
C         -0.00950       -0.38970        0.34150
C          0.42540        0.89710        0.11020
C          1.75140        1.25820        0.04710
C          2.69520        0.27930        0.22480
C          2.30490       -1.02220        0.45880
C          0.94760       -1.36350        0.51830
O          4.04630        0.64050        0.16170
H         -4.18340        0.84460       -0.52690
H         -4.21060        0.42310        1.20420
H         -4.08330       -0.86550       -0.04360
H         -0.35210        1.64810       -0.02630
H          1.98550        2.30290       -0.14080
H          3.08700       -1.75540        0.59190
H          0.66930       -2.38350        0.70170
H          4.33820        1.58390       -0.00780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

