%nproc=16
%mem=2GB
%chk=mol_111_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.81880        0.27800        0.05170
O          2.43510        0.44490       -0.26770
C          1.47830       -0.30470        0.39270
O          1.89120       -1.11440        1.26200
C          0.05210       -0.18690        0.11880
C         -0.84150       -0.97380        0.82400
C         -2.19850       -0.88230        0.58420
C         -2.71230       -0.01360       -0.35630
C         -1.82690        0.78290       -1.07220
C         -0.47800        0.66640       -0.80790
O         -4.07160        0.07920       -0.59760
H          4.36720       -0.25820       -0.72830
H          3.87220       -0.19540        1.04950
H          4.21990        1.31990        0.14420
H         -0.44660       -1.66000        1.56590
H         -2.89630       -1.49670        1.13480
H         -2.18200        1.48880       -1.82980
H          0.18870        1.29640       -1.37800
H         -4.67000        0.72950       -0.08990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

