%nproc=16
%mem=2GB
%chk=mol_111_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.74130        0.54420       -0.11090
O          2.32900        0.60430       -0.28240
C          1.51690       -0.30360        0.36020
O          2.05200       -1.17190        1.09000
C          0.05500       -0.24200        0.18320
C         -0.53150        0.71160       -0.61820
C         -1.90300        0.76520       -0.78100
C         -2.73670       -0.13410       -0.14710
C         -2.13820       -1.08720        0.65500
C         -0.76870       -1.14150        0.81820
O         -4.12570       -0.08580       -0.30720
H          4.06970        1.50280        0.37660
H          3.99670       -0.25110        0.64390
H          4.26820        0.42100       -1.06030
H          0.09030        1.41850       -1.11950
H         -2.36880        1.51780       -1.41390
H         -2.74590       -1.80370        1.16500
H         -0.30110       -1.88830        1.44650
H         -4.49940        0.62380       -0.89810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

