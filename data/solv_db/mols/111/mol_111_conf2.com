%nproc=16
%mem=2GB
%chk=mol_111_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.82760        0.27870       -0.17330
O          2.42550        0.57700       -0.14220
C          1.46920       -0.43360       -0.02460
O          1.91010       -1.60450        0.05110
C          0.04050       -0.15060        0.00890
C         -0.83370       -1.21040        0.12730
C         -2.20660       -1.03190        0.16660
C         -2.71890        0.24080        0.08540
C         -1.84900        1.31470       -0.03390
C         -0.46460        1.10610       -0.07140
O         -4.08840        0.45340        0.12190
H          3.98940       -0.82280       -0.28040
H          4.33700        0.76460       -1.01300
H          4.25560        0.66320        0.78000
H         -0.45500       -2.22320        0.19300
H         -2.86560       -1.87990        0.25980
H         -2.23670        2.32230       -0.09880
H          0.18050        1.96550       -0.16500
H         -4.71680       -0.32940        0.20860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_111_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

