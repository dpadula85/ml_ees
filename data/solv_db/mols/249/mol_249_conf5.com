%nproc=16
%mem=2GB
%chk=mol_249_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.81200       -0.05590       -0.05640
C          0.69050       -0.02390        0.18340
Cl         1.29380        1.46040       -0.61250
Cl         1.50600       -1.46670       -0.38250
H         -1.28600       -0.96350        0.32060
H         -0.99480        0.00650       -1.13510
H         -1.20810        0.88410        0.39810
H          0.81050        0.15910        1.28430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

