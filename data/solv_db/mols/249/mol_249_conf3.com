%nproc=16
%mem=2GB
%chk=mol_249_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79190       -0.16520       -0.14830
C          0.63140        0.17110        0.28060
Cl         1.70420       -0.92300       -0.64200
Cl         1.06550        1.83710       -0.08630
H         -1.49210        0.66160        0.01970
H         -1.09680       -1.07090        0.43560
H         -0.80870       -0.46180       -1.22590
H          0.78850       -0.04880        1.36670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

