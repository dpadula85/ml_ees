%nproc=16
%mem=2GB
%chk=mol_249_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76770       -0.18240        0.09840
C          0.67790        0.17790        0.01970
Cl         0.82760        1.75750       -0.76560
Cl         1.64160       -1.06770       -0.82550
H         -1.39890        0.33600       -0.65520
H         -0.84730       -1.28610       -0.04610
H         -1.20480        0.07270        1.09990
H          1.07150        0.19230        1.07430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

