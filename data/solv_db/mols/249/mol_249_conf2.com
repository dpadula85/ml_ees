%nproc=16
%mem=2GB
%chk=mol_249_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.82920       -0.17020       -0.01090
C          0.67390        0.07570        0.18340
Cl         1.64020       -1.13730       -0.64530
Cl         1.00430        1.65970       -0.57120
H         -1.31490        0.72700        0.46150
H         -0.97790       -0.15520       -1.11790
H         -1.14340       -1.10900        0.45280
H          0.94690        0.10930        1.24750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

