%nproc=16
%mem=2GB
%chk=mol_249_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.80720       -0.07560        0.01190
C         -0.69290        0.09110        0.15450
Cl        -1.42480       -1.24500       -0.79810
Cl        -1.17970        1.66870       -0.45750
H          1.33460        0.89930       -0.01440
H          1.13960       -0.68970        0.86870
H          1.05160       -0.57520       -0.96640
H         -1.03570       -0.07360        1.20130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_249_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

