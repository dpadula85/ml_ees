%nproc=16
%mem=2GB
%chk=mol_427_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.11200        0.25970       -0.21410
C          0.13760       -0.05930        0.60040
C          1.28940       -0.21530       -0.32080
O          2.33250        0.38740       -0.23430
H         -1.02810        1.29230       -0.62780
H         -1.97010        0.13480        0.47060
H         -1.11320       -0.48880       -1.04660
H         -0.04260       -1.02840        1.08670
H          0.29980        0.70660        1.36900
H          1.20680       -0.98900       -1.08310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

