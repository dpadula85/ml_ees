%nproc=16
%mem=2GB
%chk=mol_427_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.06070        0.34870        0.04900
C         -0.08790       -0.58970       -0.30180
C         -1.33550        0.07300        0.12490
O         -2.00450        0.82100       -0.55740
H          2.04040       -0.07390       -0.21790
H          1.03000        0.44530        1.15900
H          0.94400        1.34720       -0.40610
H          0.05960       -1.48910        0.34240
H         -0.06590       -0.87090       -1.36500
H         -1.64090       -0.01150        1.17290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

