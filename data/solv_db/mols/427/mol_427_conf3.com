%nproc=16
%mem=2GB
%chk=mol_427_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.00590       -0.49620        0.09810
C         -0.00370        0.64210       -0.03670
C          1.33230        0.01350       -0.05590
O          1.73170       -0.50090       -1.06410
H         -1.94150       -0.16720       -0.41980
H         -0.58760       -1.43820       -0.32800
H         -1.15740       -0.63130        1.18140
H         -0.10190        1.36250        0.80030
H         -0.19970        1.20160       -1.00090
H          1.93380        0.01410        0.82550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

