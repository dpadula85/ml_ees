%nproc=16
%mem=2GB
%chk=mol_427_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.14330        0.12420       -0.14270
C          0.17160       -0.42450        0.41020
C          1.29860        0.24290       -0.27890
O          2.44220       -0.04070        0.00370
H         -1.33110       -0.21620       -1.17550
H         -1.02150        1.23100       -0.14620
H         -1.95170       -0.20100        0.56030
H          0.20230       -1.52830        0.31830
H          0.17600       -0.18160        1.49620
H          1.15680        0.99410       -1.04550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

