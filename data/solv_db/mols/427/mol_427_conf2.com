%nproc=16
%mem=2GB
%chk=mol_427_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.10690        0.13080        0.22580
C          0.14260       -0.49600       -0.31520
C          1.34690        0.34220       -0.06010
O          1.49860        1.40090       -0.61910
H         -0.95210        0.33420        1.30360
H         -1.89830       -0.65950        0.18310
H         -1.39610        1.06980       -0.26250
H          0.06940       -0.72960       -1.39100
H          0.25830       -1.48000        0.20630
H          2.03770        0.08710        0.72900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_427_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

