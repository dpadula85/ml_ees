%nproc=16
%mem=2GB
%chk=mol_122_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.97230       -0.25880        0.49350
C          1.08460        0.81780       -0.07660
C         -0.31770        0.31690       -0.33250
O         -0.93800       -0.13550        0.81210
C         -2.21440       -0.57140        0.46980
H          1.60890       -0.64020        1.46570
H          2.05470       -1.05100       -0.28190
H          2.97600        0.19920        0.64630
H          1.50910        1.07270       -1.08660
H          1.10970        1.69520        0.58920
H         -0.91690        1.15840       -0.73480
H         -0.25510       -0.50910       -1.06280
H         -2.18440       -1.40280       -0.28110
H         -2.74330       -0.95190        1.38640
H         -2.74550        0.26050       -0.00810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

