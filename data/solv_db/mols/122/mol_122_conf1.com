%nproc=16
%mem=2GB
%chk=mol_122_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.03310        0.42340       -0.18430
C         -0.63830       -0.12400       -0.47540
C          0.07020       -0.36490        0.83790
O          1.31650       -0.86380        0.59900
C          2.17400       -0.09080       -0.13020
H         -2.74720       -0.39830       -0.38290
H         -2.23760        1.23310       -0.89740
H         -2.12310        0.80210        0.84640
H         -0.06220        0.63350       -1.06120
H         -0.68130       -1.08230       -1.01050
H         -0.56210       -1.11750        1.38230
H          0.06650        0.59930        1.39470
H          1.88670        0.17450       -1.14030
H          2.43090        0.83850        0.44300
H          3.14020       -0.66290       -0.22100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

