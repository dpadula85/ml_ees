%nproc=16
%mem=2GB
%chk=mol_122_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.03000       -0.24960       -0.25870
C          0.64780       -0.42890        0.38280
C         -0.11050        0.84030        0.12360
O         -1.38310        0.85680        0.63310
C         -2.20680       -0.12830        0.14240
H          2.44630        0.67490        0.21090
H          1.84210        0.00820       -1.32150
H          2.65560       -1.13180       -0.12180
H          0.20740       -1.31500       -0.10580
H          0.75390       -0.63230        1.47350
H          0.45810        1.66540        0.60980
H         -0.06010        1.04740       -0.97120
H         -1.76040       -1.11090        0.39100
H         -2.30920       -0.10840       -0.97150
H         -3.21090        0.01200        0.60990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

