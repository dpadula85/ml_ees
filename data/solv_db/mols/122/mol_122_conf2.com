%nproc=16
%mem=2GB
%chk=mol_122_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04390        0.42860       -0.12810
C         -0.65300        0.17290        0.44470
C          0.11460       -0.79840       -0.41570
O          1.34820       -1.01880        0.11320
C          2.18600        0.04450        0.25390
H         -1.99000        1.13940       -0.96390
H         -2.51810       -0.54940       -0.42390
H         -2.66520        0.87250        0.68500
H         -0.77460       -0.26150        1.45370
H         -0.10770        1.13710        0.43650
H         -0.43920       -1.77210       -0.41860
H          0.12960       -0.45550       -1.48100
H          1.83490        0.86800        0.90610
H          2.44340        0.53960       -0.72300
H          3.13510       -0.34690        0.72610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

