%nproc=16
%mem=2GB
%chk=mol_122_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.94620       -0.44280       -0.33730
C         -1.15070        0.52600        0.47150
C          0.29210        0.62400        0.01220
O          0.90360       -0.60100        0.11520
C          2.23120       -0.59430       -0.28780
H         -3.03240       -0.20640       -0.28100
H         -1.66500       -0.28740       -1.41340
H         -1.75200       -1.50510       -0.06950
H         -1.61590        1.53910        0.32560
H         -1.18030        0.33800        1.55400
H          0.80960        1.42440        0.58050
H          0.23570        0.93010       -1.07240
H          2.69160       -1.59710       -0.19130
H          2.85740        0.09400        0.33020
H          2.32140       -0.24150       -1.34180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_122_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

