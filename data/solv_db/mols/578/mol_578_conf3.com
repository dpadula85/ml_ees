%nproc=16
%mem=2GB
%chk=mol_578_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19970       -1.18440       -0.01940
C          1.06570       -0.19650        0.02470
C          1.27310        1.14770        0.16370
C          0.20570        2.02120        0.19970
N         -1.03280        1.51590        0.09490
C         -1.28940        0.21110       -0.04250
C         -0.22580       -0.66850       -0.07920
C         -2.69200       -0.28880       -0.15450
H          3.14430       -0.60020       -0.07760
H          2.05670       -1.84070       -0.88030
H          2.16060       -1.79830        0.90280
H          2.28670        1.50760        0.24420
H          0.37990        3.10190        0.31190
H         -0.37520       -1.73840       -0.18910
H         -2.69040       -1.38500       -0.26890
H         -3.23490       -0.01360        0.77500
H         -3.23190        0.20920       -1.00550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

