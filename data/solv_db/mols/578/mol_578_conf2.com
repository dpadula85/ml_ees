%nproc=16
%mem=2GB
%chk=mol_578_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32500       -0.86410        0.11050
C         -1.06900       -0.05870        0.00310
C         -1.12650        1.30860       -0.18430
C          0.07250        1.98670       -0.27450
N          1.27160        1.39670       -0.19090
C          1.33030        0.07200       -0.00930
C          0.16870       -0.66660        0.08930
C          2.64570       -0.64510        0.09200
H         -3.15620       -0.20390        0.48200
H         -2.56810       -1.26140       -0.89180
H         -2.14620       -1.71380        0.82720
H         -2.08930        1.83210       -0.25810
H         -0.01550        3.06980       -0.42310
H          0.21560       -1.73720        0.23590
H          2.64620       -1.20330        1.06000
H          2.66130       -1.35950       -0.77180
H          3.48400        0.04760        0.10390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

