%nproc=16
%mem=2GB
%chk=mol_578_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33800       -0.87110        0.20880
C          1.07680       -0.05980        0.11810
C          1.11600        1.31330        0.23930
C         -0.06940        2.02940        0.14990
N         -1.23090        1.40190       -0.05010
C         -1.32990        0.05740       -0.17640
C         -0.15670       -0.65300       -0.08810
C         -2.64650       -0.62010       -0.40000
H          2.89710       -0.85790       -0.73220
H          2.06650       -1.93240        0.47740
H          2.96030       -0.52810        1.08470
H          2.06330        1.82800        0.40270
H         -0.04350        3.10980        0.24450
H         -0.16530       -1.72590       -0.18000
H         -2.73740       -0.89940       -1.46030
H         -2.66350       -1.58570        0.18260
H         -3.47480       -0.00650       -0.02080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

