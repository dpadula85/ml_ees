%nproc=16
%mem=2GB
%chk=mol_578_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33930       -0.84570        0.23410
C          1.09190       -0.04540        0.09080
C          1.14670        1.33420        0.04690
C         -0.04940        2.01420       -0.08810
N         -1.23870        1.36170       -0.17520
C         -1.31850        0.01720       -0.13480
C         -0.14710       -0.68010       -0.00150
C         -2.63120       -0.67400       -0.23180
H          2.56400       -1.32860       -0.72870
H          3.14790       -0.18170        0.59540
H          2.20780       -1.62180        1.04410
H          2.08220        1.87440        0.11450
H         -0.01190        3.10610       -0.12350
H         -0.21100       -1.76300        0.03120
H         -2.81180       -1.31690        0.66380
H         -2.71590       -1.33050       -1.10610
H         -3.44430        0.07980       -0.23120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_578_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

