%nproc=16
%mem=2GB
%chk=mol_520_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.82840       -0.50980        0.06160
C         -0.70070       -0.34300       -0.89420
C          0.53000        0.27610       -0.30790
C          0.26170        1.66130        0.24080
C          1.20240       -0.58440        0.72880
O          1.60600       -1.81020        0.22140
H         -2.44580        0.42530        0.10640
H         -1.56660       -0.79290        1.06990
H         -2.51390       -1.28730       -0.34650
H         -1.04580        0.31990       -1.71500
H         -0.43250       -1.30720       -1.35880
H          1.26130        0.39590       -1.13790
H         -0.81380        1.81700        0.48670
H          0.92230        1.82770        1.12280
H          0.50760        2.45990       -0.50920
H          0.57760       -0.77710        1.61290
H          2.12230       -0.04220        1.04690
H          2.35610       -1.72900       -0.42890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

