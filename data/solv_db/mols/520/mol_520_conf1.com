%nproc=16
%mem=2GB
%chk=mol_520_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.13750       -0.02230       -0.08640
C          0.80800        0.59160        0.18910
C         -0.34980       -0.20100       -0.39650
C         -0.41250       -1.59660        0.15240
C         -1.60730        0.54870        0.04730
O         -1.49100        1.83420       -0.48950
H          2.53840       -0.56690        0.79150
H          2.84810        0.80440       -0.32840
H          2.12640       -0.68710       -0.96730
H          0.73500        1.60980       -0.25490
H          0.63910        0.67070        1.26980
H         -0.30970       -0.24190       -1.50540
H         -1.50100       -1.89070        0.18250
H          0.08890       -2.34960       -0.47400
H         -0.02230       -1.61660        1.19110
H         -2.52180        0.03230       -0.30420
H         -1.54200        0.64120        1.14410
H         -2.16410        2.43980       -0.10310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

