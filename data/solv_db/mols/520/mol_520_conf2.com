%nproc=16
%mem=2GB
%chk=mol_520_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.08020        0.06350        0.39650
C          0.74210        0.65450        0.11570
C         -0.22270       -0.25670       -0.55230
C         -0.54350       -1.49610        0.21300
C         -1.52210        0.53590       -0.70490
O         -1.90070        0.87280        0.61180
H          2.57260       -0.18840       -0.57180
H          2.67780        0.84420        0.93230
H          2.00550       -0.78860        1.08590
H          0.29560        1.04310        1.06350
H          0.88690        1.54310       -0.53070
H          0.15410       -0.50030       -1.56770
H         -1.53570       -1.87140       -0.17450
H         -0.58280       -1.34060        1.29610
H          0.17410       -2.31160       -0.00290
H         -2.31780       -0.07900       -1.15880
H         -1.29050        1.45730       -1.24550
H         -1.67300        1.81850        0.79400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

