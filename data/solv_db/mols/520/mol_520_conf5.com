%nproc=16
%mem=2GB
%chk=mol_520_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67750       -1.15270       -0.02160
C         -1.06970        0.13880        0.40940
C          0.17830        0.50320       -0.33790
C          0.66790        1.83900        0.22020
C          1.29240       -0.48150       -0.16790
O          0.98000       -1.75270       -0.59960
H         -1.23430       -2.04490        0.44510
H         -1.70380       -1.21420       -1.13580
H         -2.75250       -1.14130        0.29800
H         -0.89470        0.13610        1.49830
H         -1.81410        0.94410        0.21710
H         -0.03390        0.60710       -1.41460
H          0.32240        2.67280       -0.42060
H          1.77720        1.86140        0.27570
H          0.29930        1.96010        1.26590
H          1.65750       -0.49770        0.88880
H          2.15050       -0.12340       -0.77140
H          1.85500       -2.25430       -0.64900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

