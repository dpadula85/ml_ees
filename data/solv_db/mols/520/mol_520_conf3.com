%nproc=16
%mem=2GB
%chk=mol_520_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11510       -0.35420       -0.05210
C          0.71620       -0.72720       -0.38200
C         -0.31500        0.27840        0.04430
C         -0.12460        1.62610       -0.58160
C         -1.67160       -0.30890       -0.32530
O         -1.82560       -1.51820        0.35490
H          2.27170        0.09300        0.92900
H          2.51630        0.26510       -0.89790
H          2.73620       -1.29070       -0.07610
H          0.63470       -0.91460       -1.47040
H          0.42870       -1.69800        0.11550
H         -0.32870        0.41450        1.14520
H         -0.84860        2.32070       -0.08120
H          0.88080        1.99680       -0.35070
H         -0.37220        1.64950       -1.65680
H         -1.58440       -0.58260       -1.41570
H         -2.49530        0.39150       -0.14220
H         -2.73370       -1.64130        0.72420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_520_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

