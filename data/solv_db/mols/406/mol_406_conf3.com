%nproc=16
%mem=2GB
%chk=mol_406_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19690       -0.08840        0.59420
C          1.39800        0.11940       -0.66720
C         -0.03960        0.04360       -0.34000
C         -0.76970       -1.12950       -0.36660
C         -2.11310       -1.18390       -0.05650
N         -2.72310       -0.03460        0.28480
C         -2.05260        1.14260        0.32620
C         -0.70700        1.19300        0.01480
H          1.54470        0.09140        1.49510
H          2.51290       -1.14710        0.66280
H          3.03140        0.62800        0.65540
H          1.70890        1.04840       -1.14060
H          1.66020       -0.71010       -1.38210
H         -0.26940       -2.05050       -0.64260
H         -2.66370       -2.12330       -0.08560
H         -2.53610        2.07190        0.59960
H         -0.17840        2.12920        0.04840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

