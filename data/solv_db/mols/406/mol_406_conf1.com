%nproc=16
%mem=2GB
%chk=mol_406_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.14880       -0.10620        0.62240
C         -1.35040       -0.25180       -0.65500
C          0.06710       -0.03980       -0.33210
C          0.59410        1.24360       -0.39810
C          1.92920        1.37810       -0.08380
N          2.69330        0.33410        0.26930
C          2.15920       -0.89910        0.32580
C          0.84000       -1.12270        0.02930
H         -2.99960        0.55110        0.44360
H         -1.53230        0.26300        1.46280
H         -2.56310       -1.09420        0.95610
H         -1.46680       -1.28690       -1.04540
H         -1.70020        0.42220       -1.45560
H         -0.01090        2.06770       -0.67730
H          2.32870        2.40040       -0.14150
H          2.77430       -1.75790        0.61290
H          0.38600       -2.10150        0.06650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

