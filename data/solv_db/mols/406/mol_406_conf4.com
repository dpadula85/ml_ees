%nproc=16
%mem=2GB
%chk=mol_406_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31350        0.05260        0.50490
C         -1.27620       -0.42980       -0.46070
C          0.09600       -0.09400       -0.09310
C          0.79700       -0.98870        0.68940
C          2.10200       -0.71760        1.06570
N          2.70260        0.43080        0.66790
C          2.04320        1.31070       -0.08930
C          0.74780        1.06890       -0.47820
H         -2.11730       -0.41640        1.47840
H         -3.29830       -0.36070        0.15360
H         -2.35120        1.12840        0.64230
H         -1.36440       -1.53200       -0.54560
H         -1.44820       -0.03600       -1.50090
H          0.34520       -1.93160        1.03260
H          2.61430       -1.44610        1.67920
H          2.54850        2.21330       -0.38850
H          0.17260        1.74840       -1.08990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

