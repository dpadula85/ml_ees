%nproc=16
%mem=2GB
%chk=mol_406_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31320       -0.05920       -0.33610
C         -1.28060       -0.35730        0.71150
C          0.09400       -0.08190        0.29410
C          0.86780       -1.05960       -0.32410
C          2.16350       -0.80780       -0.72000
N          2.68970        0.41640       -0.50100
C          1.98760        1.39230        0.09010
C          0.69360        1.14670        0.48740
H         -2.35230        0.99820       -0.62810
H         -3.30320       -0.32240        0.09110
H         -2.14230       -0.65450       -1.25220
H         -1.35270       -1.45950        0.92490
H         -1.56960        0.15120        1.65950
H          0.45810       -2.03230       -0.50100
H          2.79770       -1.53830       -1.20290
H          2.44600        2.35520        0.24580
H          0.11580        1.91270        0.96120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_406_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

