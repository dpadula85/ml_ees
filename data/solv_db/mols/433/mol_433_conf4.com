%nproc=16
%mem=2GB
%chk=mol_433_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.37110       -0.29160       -0.08180
C         -1.66910        0.98820        0.33300
C         -0.28390        0.94750       -0.12070
C         -0.10180       -0.35400       -0.92210
C         -1.36900       -1.07160       -0.92280
Cl        -1.32960       -2.77420       -0.46720
C          1.10360       -0.97810       -0.29010
C          2.23340       -0.05620       -0.65170
C          2.07640        1.06960        0.03410
C          0.81140        0.90110        0.87260
C          1.06300       -0.57620        1.19300
Cl         0.04240       -1.37870        2.23750
Cl         2.76830       -0.65830        1.74800
Cl         0.75500        1.94730        2.21760
Cl         3.13890        2.44300       -0.04530
Cl         3.52660       -0.38850       -1.76850
Cl         1.41420       -2.62670       -0.59540
Cl        -1.92020        1.32020        2.05210
H         -2.72720       -0.88310        0.79650
H         -3.32260       -0.07990       -0.65470
H         -2.21390        1.82090       -0.20880
H         -0.05210        1.81530       -0.80990
H          0.22260       -0.08070       -1.97240
H         -1.79540       -1.05540       -1.97300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

