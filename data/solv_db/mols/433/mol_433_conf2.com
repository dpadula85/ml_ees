%nproc=16
%mem=2GB
%chk=mol_433_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.36260       -0.34470        0.05270
C         -1.59460        0.53370        1.02830
C         -0.28840        0.82640        0.46160
C         -0.22210        0.21090       -0.94780
C         -1.49790       -0.45790       -1.18820
Cl        -1.40580       -2.12340       -1.77770
C          1.02520       -0.58170       -0.91450
C          2.12660        0.47340       -0.83400
C          2.07330        1.00210        0.38620
C          0.94380        0.31100        1.12910
C          1.21850       -1.07470        0.52600
Cl         0.33750       -2.39090        1.06130
Cl         2.99120       -1.32860        0.70200
Cl         1.04520        0.40600        2.83000
Cl         3.12150        2.25180        0.98150
Cl         3.24990        0.90830       -2.10630
Cl         1.29840       -1.74800       -2.12960
Cl        -1.63380       -0.19630        2.62720
H         -3.38210        0.08040       -0.18030
H         -2.59190       -1.33990        0.50300
H         -2.20240        1.48580        1.11100
H         -0.13880        1.94420        0.36350
H         -0.04930        1.04870       -1.69010
H         -2.06150        0.10330       -1.99510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

