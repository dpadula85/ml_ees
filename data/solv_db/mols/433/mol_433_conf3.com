%nproc=16
%mem=2GB
%chk=mol_433_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15180       -1.82420       -1.02380
C         -1.82610       -0.60480       -0.42300
C         -0.84580        0.46210       -0.27560
C          0.48600       -0.05030       -0.85350
C          0.27560       -1.40640       -1.34310
Cl         1.41440       -2.63540       -0.78960
C          1.45880        0.19890        0.24180
C          1.56620        1.71430        0.30210
C          0.41240        2.15170        0.80070
C         -0.46960        0.93650        1.07630
C          0.68820        0.05830        1.56270
Cl         1.56800        1.07020        2.76130
Cl         0.36380       -1.46690        2.15890
Cl        -1.70510        1.22210        2.22090
Cl         0.05100        3.82470        1.06150
Cl         2.99590        2.55170       -0.22110
Cl         2.97190       -0.58870        0.18950
Cl        -2.74940       -1.02730        1.02590
H         -1.19210       -2.67890       -0.31350
H         -1.69310       -2.20110       -1.93590
H         -2.60680       -0.29360       -1.18230
H         -1.14750        1.38130       -0.86320
H          0.79160        0.63220       -1.70340
H          0.34350       -1.42640       -2.47370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

