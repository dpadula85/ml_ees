%nproc=16
%mem=2GB
%chk=mol_433_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.39310        0.08520        0.00990
C         -1.57120       -1.17570        0.19980
C         -0.20490       -0.80500        0.55420
C         -0.14620        0.73120        0.63950
C         -1.47570        1.24800        0.34840
Cl        -1.59130        2.51050       -0.87570
C          0.99560        1.08720       -0.24370
C          2.21660        0.55940        0.50550
C          2.15840       -0.76930        0.43160
C          0.91160       -1.13640       -0.36600
C          1.04750        0.03050       -1.35080
Cl         2.76620       -0.00710       -1.88480
Cl        -0.02360        0.14000       -2.63240
Cl         0.96910       -2.69530       -1.06070
Cl         3.31340       -1.86420        1.13020
Cl         3.41480        1.57450        1.28390
Cl         1.15130        2.70700       -0.76370
Cl        -1.75320       -2.30420       -1.15030
H         -3.32610        0.08440        0.63360
H         -2.74620        0.22090       -1.02610
H         -2.05130       -1.70810        1.07800
H          0.08500       -1.22780        1.56390
H          0.17240        1.00710        1.69030
H         -1.91900        1.70720        1.28520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_433_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

