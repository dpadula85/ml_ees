%nproc=16
%mem=2GB
%chk=mol_499_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.03820        0.16310        0.26050
S          1.07030       -0.32470       -1.19350
S         -0.74290       -1.26670       -0.66630
C         -2.09570       -0.03750       -0.48330
H          1.43710        0.75840        0.97830
H          2.41590       -0.71410        0.82940
H          2.86460        0.81330       -0.11710
H         -1.87780        0.52210        0.46960
H         -2.08570        0.68390       -1.31230
H         -3.02410       -0.59800       -0.36070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

