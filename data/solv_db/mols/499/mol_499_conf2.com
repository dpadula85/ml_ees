%nproc=16
%mem=2GB
%chk=mol_499_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.88980        0.04820       -0.18950
S          0.79820       -1.19850       -0.76230
S         -0.74110       -1.01940        0.69280
C         -1.87890        0.08330        0.01570
H          2.38330       -0.18400        0.79870
H          1.46660        1.06560       -0.06780
H          2.75590        0.14970       -0.90260
H         -2.17570        0.87950        0.76100
H         -2.85640       -0.44740       -0.19490
H         -1.64160        0.62310       -0.90790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

