%nproc=16
%mem=2GB
%chk=mol_499_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04570       -0.02760       -0.19730
S         -0.75670       -1.29600       -0.08480
S          0.77660       -0.72490        1.26870
C          2.07980        0.19400        0.39030
H         -1.69510        0.86740       -0.77800
H         -2.87090       -0.47410       -0.79590
H         -2.45880        0.25030        0.78830
H          2.36200       -0.32020       -0.55070
H          2.93550        0.32000        1.07770
H          1.67320        1.21110        0.15310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

