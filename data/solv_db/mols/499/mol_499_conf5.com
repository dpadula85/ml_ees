%nproc=16
%mem=2GB
%chk=mol_499_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.07350        0.18030       -0.10010
S          1.00250       -0.93760       -1.04340
S         -0.83560       -1.28620       -0.02500
C         -2.09090       -0.04300       -0.51760
H          1.41470        0.65860        0.67380
H          2.94220       -0.30700        0.32850
H          2.37270        0.99120       -0.78370
H         -3.10140       -0.45640       -0.50600
H         -1.82390        0.39140       -1.52160
H         -1.95390        0.80860        0.19720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

