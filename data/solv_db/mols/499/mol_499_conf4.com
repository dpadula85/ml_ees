%nproc=16
%mem=2GB
%chk=mol_499_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.08240       -0.13300        0.36720
S          0.72080       -1.06550       -0.38500
S         -0.95480        0.15840       -0.77900
C         -2.09870        0.20810        0.63150
H          1.70160        0.55950        1.17040
H          2.68250        0.42090       -0.37400
H          2.71650       -0.86850        0.90050
H         -2.45090        1.25100        0.71430
H         -2.90240       -0.52470        0.56190
H         -1.49710       -0.00610        1.56070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_499_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

