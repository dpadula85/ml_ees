%nproc=16
%mem=2GB
%chk=mol_228_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17010       -1.10770        0.55750
C         -1.46160       -0.94040       -0.76630
O         -0.94340        0.31600       -0.91960
C         -0.02730        0.73010        0.00860
C         -0.66450        1.93310        0.74840
O          1.16550        1.21280       -0.51100
C          2.22550        0.57900        0.07600
C          2.20000       -0.90000       -0.17310
H         -1.47220       -1.10100        1.39130
H         -2.74480       -2.05110        0.58170
H         -2.90090       -0.28850        0.66070
H         -2.21570       -1.08640       -1.60190
H         -0.69110       -1.70470       -0.92690
H          0.16410       -0.07540        0.74510
H         -1.76680        1.82380        0.70000
H         -0.34740        1.86850        1.79930
H         -0.39240        2.87830        0.27560
H          3.21180        0.96450       -0.25630
H          2.23920        0.70710        1.19390
H          1.69990       -1.04000       -1.17640
H          3.24550       -1.26100       -0.22050
H          1.64700       -1.45700        0.60900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

