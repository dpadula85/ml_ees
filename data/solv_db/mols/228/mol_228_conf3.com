%nproc=16
%mem=2GB
%chk=mol_228_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.17240        0.83490       -0.64050
C          1.87920        0.09600       -0.58680
O          1.23220        0.18970        0.62830
C          0.03880       -0.54900        0.53200
C          0.03410       -1.69320        1.52630
O         -1.06970        0.26590        0.75080
C         -1.93960        0.23220       -0.35750
C         -3.13590        1.09450       -0.15410
H          3.05550        1.90750       -0.33430
H          4.00310        0.33760       -0.12570
H          3.47050        0.89640       -1.72890
H          2.06900       -0.96660       -0.79560
H          1.21340        0.49990       -1.38690
H         -0.07330       -1.00620       -0.47170
H          0.82440       -1.58740        2.29360
H          0.19510       -2.66970        1.02610
H         -0.98400       -1.75580        1.98420
H         -1.34410        0.60350       -1.22050
H         -2.25900       -0.79050       -0.60340
H         -2.89400        1.92980        0.54140
H         -4.01620        0.54430        0.22470
H         -3.47210        1.58620       -1.10140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

