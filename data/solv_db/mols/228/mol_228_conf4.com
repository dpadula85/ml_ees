%nproc=16
%mem=2GB
%chk=mol_228_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.13380        0.41240        0.97140
C          1.96670       -0.36060        0.35330
O          1.11190        0.55680       -0.18870
C         -0.00500        0.03830       -0.79340
C         -0.14400        0.41380       -2.24350
O         -1.12470        0.48510       -0.11760
C         -1.87290       -0.52290        0.45090
C         -3.05800        0.14720        1.14870
H          4.01140       -0.27650        1.04830
H          2.90070        0.69640        2.02960
H          3.35290        1.33810        0.43570
H          2.43510       -1.04880       -0.39150
H          1.44640       -0.97260        1.11470
H          0.02180       -1.06730       -0.71150
H          0.58180       -0.17300       -2.85530
H         -1.15630        0.06830       -2.56750
H         -0.09890        1.48520       -2.42800
H         -1.25690       -1.03100        1.22430
H         -2.22460       -1.21390       -0.32490
H         -3.47690       -0.51890        1.91480
H         -3.79480        0.40850        0.34540
H         -2.74920        1.13560        1.58460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

