%nproc=16
%mem=2GB
%chk=mol_228_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.74820       -1.03470       -0.99940
C         -1.33120       -0.48150       -0.91560
O         -1.15590        0.30560        0.20330
C          0.12770        0.82730        0.30490
C          0.06970        2.34180        0.01780
O          0.77250        0.57670        1.47890
C          1.98480       -0.03980        1.43080
C          1.86640       -1.40280        0.80140
H         -3.21770       -1.10400        0.00020
H         -3.41110       -0.39070       -1.61220
H         -2.69320       -2.02390       -1.50620
H         -1.24190        0.18360       -1.82690
H         -0.58410       -1.26680       -1.07520
H          0.72520        0.43610       -0.54120
H         -0.34650        2.52320       -0.98630
H          1.04970        2.80260        0.08470
H         -0.57870        2.76980        0.79720
H          2.78820        0.57160        0.96310
H          2.32230       -0.21050        2.49650
H          0.84910       -1.83080        0.91060
H          2.54510       -2.14700        1.31370
H          2.20800       -1.40600       -0.26240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

