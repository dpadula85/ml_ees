%nproc=16
%mem=2GB
%chk=mol_228_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.10420        0.73140       -0.26890
C         -2.23820       -0.48910        0.09450
O         -1.11060       -0.46050       -0.53250
C         -0.08830        0.24550       -0.69520
C         -0.10880        0.97820        0.89770
O          0.92300       -0.91540       -0.12600
C          2.12760       -0.31050       -0.38450
C          3.30150       -1.14570        0.07040
H         -3.23710        0.71350       -1.36250
H         -4.11850        0.53470        0.18300
H         -2.67650        1.67280        0.06300
H         -2.33250       -0.75450        1.16510
H         -2.87090       -1.33670       -0.37890
H          0.71500        0.80040       -0.82300
H         -0.20860        2.01840        0.60770
H          0.83860        0.72090        1.27780
H         -0.94290        0.58740        1.36540
H          2.21170       -0.31120       -1.52670
H          2.20830        0.73730       -0.07910
H          2.98440       -2.20000       -0.00300
H          3.54960       -0.80960        1.09640
H          4.17730       -1.00720       -0.58330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_228_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

