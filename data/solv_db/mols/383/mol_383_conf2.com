%nproc=16
%mem=2GB
%chk=mol_383_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.76560        0.71320       -0.02520
C          1.23190       -0.66900        0.25810
C         -0.16410       -0.82430       -0.34580
C         -1.01730        0.23680        0.31870
N         -2.37940        0.20980       -0.15930
O         -3.26510       -0.00450        0.68480
O         -2.72420        0.40250       -1.46810
H          2.86910        0.64780       -0.18960
H          1.25900        1.12440       -0.90460
H          1.59050        1.41820        0.82200
H          1.20400       -0.78550        1.35850
H          1.88010       -1.46190       -0.14260
H         -0.12360       -0.63220       -1.42330
H         -0.58380       -1.81520       -0.15600
H         -0.95480        0.21840        1.40500
H         -0.58780        1.22140       -0.03250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

