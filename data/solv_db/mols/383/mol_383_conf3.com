%nproc=16
%mem=2GB
%chk=mol_383_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.27830        0.31400       -0.90860
C          1.10010        0.05730       -0.03010
C         -0.21250        0.04250       -0.78090
C         -1.29590       -0.23240        0.27390
N         -2.60230       -0.27140       -0.31490
O         -3.32970       -1.28020       -0.25690
O         -3.08730        0.83210       -0.97070
H          3.03700       -0.51390       -0.79350
H          1.99650        0.41480       -1.97610
H          2.75980        1.27580       -0.62520
H          1.01520        0.82960        0.75820
H          1.18980       -0.97160        0.42030
H         -0.15800       -0.79650       -1.50300
H         -0.43530        0.97530       -1.30070
H         -1.24380        0.52250        1.08440
H         -1.01180       -1.19790        0.73320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

