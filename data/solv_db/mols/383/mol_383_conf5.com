%nproc=16
%mem=2GB
%chk=mol_383_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.98250       -0.03470       -0.16130
C         -0.95740       -0.66780        0.73340
C          0.42020       -0.67640        0.10380
C          0.89340        0.74200       -0.19590
N          2.20140        0.62340       -0.78710
O          2.38400        1.06410       -1.93220
O          3.27230        0.04510       -0.15320
H         -2.21530        1.02840        0.13890
H         -2.97800       -0.55930       -0.09140
H         -1.65940        0.02570       -1.22610
H         -1.20740       -1.72030        0.98000
H         -0.94010       -0.08530        1.67680
H          0.41600       -1.21720       -0.86190
H          1.15240       -1.14820        0.78720
H          0.97410        1.33680        0.74740
H          0.22640        1.24360       -0.92030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

