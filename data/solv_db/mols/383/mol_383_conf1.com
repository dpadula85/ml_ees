%nproc=16
%mem=2GB
%chk=mol_383_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.13840        0.70550       -0.28040
C         -0.79720        0.26820        0.32650
C          0.02950       -0.41170       -0.69850
C          1.36280       -0.90470       -0.28670
N          2.23620        0.14530        0.16650
O          1.90580        1.32860        0.08170
O          3.50070       -0.18010        0.62370
H         -2.86450        0.72640        0.55930
H         -2.04500        1.72950       -0.69480
H         -2.50490       -0.03750       -1.00340
H         -0.38930        1.13690        0.81960
H         -1.05370       -0.50070        1.10860
H         -0.58870       -1.26170       -1.10600
H          0.13310        0.28480       -1.57380
H          1.87410       -1.36080       -1.18580
H          1.33950       -1.66800        0.51360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

