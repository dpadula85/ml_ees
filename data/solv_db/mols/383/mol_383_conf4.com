%nproc=16
%mem=2GB
%chk=mol_383_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.64850        0.10590        0.50710
C         -0.75550        0.84190       -0.46840
C          0.18620       -0.16280       -1.07100
C          0.98820       -0.80590        0.02180
N          1.81630        0.05580        0.79800
O          3.07040       -0.10760        0.70260
O          1.42070        1.04490        1.63540
H         -2.72320        0.41800        0.34200
H         -1.65580       -0.99040        0.34690
H         -1.36210        0.28100        1.56050
H         -1.37930        1.33370       -1.27130
H         -0.17880        1.63470        0.02990
H         -0.49430       -0.93080       -1.55280
H          0.82800        0.28410       -1.86280
H          1.58380       -1.63500       -0.46000
H          0.30380       -1.36750        0.74220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_383_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

