%nproc=16
%mem=2GB
%chk=mol_128_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.61950        0.97220       -0.93980
C         -1.40010        0.50160       -0.14490
C         -0.57710       -0.36300       -1.06010
C          0.64030       -0.88700       -0.39320
C          1.62300        0.10200        0.06650
O          1.47420        1.27470       -0.16260
C          2.81490       -0.35470        0.82400
H         -3.47370        0.97410       -0.26780
H         -2.37530        2.00310       -1.31300
H         -2.77630        0.30940       -1.82650
H         -1.78750       -0.14150        0.70360
H         -0.84660        1.34430        0.26200
H         -0.37010        0.18670       -2.00220
H         -1.18900       -1.26660       -1.35770
H          1.19650       -1.60630       -1.07310
H          0.32270       -1.57930        0.45160
H          2.93850        0.32620        1.68960
H          3.73010       -0.41450        0.19710
H          2.67500       -1.38160        1.25590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

