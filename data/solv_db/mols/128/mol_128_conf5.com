%nproc=16
%mem=2GB
%chk=mol_128_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73340        0.70850       -0.10310
C         -1.76390       -0.31830        0.47220
C         -0.61900       -0.64370       -0.45630
C          0.64120       -0.00250        0.05520
C          1.82770       -0.29710       -0.77450
O          1.83950       -0.86010       -1.84910
C          3.10790        0.11540       -0.12700
H         -3.31150        0.13310       -0.85700
H         -3.35760        1.06690        0.73590
H         -2.20680        1.52490       -0.62330
H         -2.31960       -1.18550        0.80090
H         -1.37220        0.19490        1.40890
H         -0.84140       -0.23050       -1.47880
H         -0.47770       -1.75480       -0.56940
H          0.50440        1.05470        0.12400
H          0.82510       -0.46380        1.09050
H          3.36410       -0.47370        0.71760
H          2.98080        1.17480        0.24770
H          3.91220        0.25690       -0.87810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

