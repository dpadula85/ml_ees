%nproc=16
%mem=2GB
%chk=mol_128_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08580        0.88430        0.33590
C         -1.63270       -0.14900       -0.67000
C         -0.70920       -1.16130       -0.09410
C          0.55690       -0.64350        0.48440
C          1.45440        0.05350       -0.45480
O          1.17390        0.13450       -1.62260
C          2.70500        0.64170        0.10870
H         -1.50780        0.84830        1.27950
H         -3.14790        0.71740        0.57250
H         -1.95570        1.91510       -0.06260
H         -2.53530       -0.71840       -1.03240
H         -1.19930        0.34300       -1.52950
H         -0.40870       -1.82840       -0.95870
H         -1.21280       -1.85910        0.62130
H          0.40430       -0.00940        1.38810
H          1.16010       -1.53490        0.84020
H          2.39170        1.59730        0.60360
H          3.48780        0.79980       -0.64040
H          3.06090       -0.03080        0.92480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

