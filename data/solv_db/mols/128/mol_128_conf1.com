%nproc=16
%mem=2GB
%chk=mol_128_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.13750        0.78220        0.17140
C          1.60810       -0.60110        0.40390
C          0.61330       -1.03930       -0.61850
C         -0.61510       -0.21430       -0.73770
C         -1.46050       -0.10790        0.45260
O         -1.17060       -0.68210        1.46690
C         -2.68520        0.71630        0.40480
H          2.29090        1.25010        1.17180
H          1.51790        1.40060       -0.48850
H          3.14920        0.74520       -0.29400
H          2.49030       -1.30680        0.29890
H          1.26220       -0.74040        1.43430
H          0.34530       -2.12220       -0.46650
H          1.10720       -1.01100       -1.63640
H         -1.21030       -0.68510       -1.58460
H         -0.29800        0.78860       -1.13460
H         -2.44450        1.78440        0.58310
H         -3.19050        0.64670       -0.60010
H         -3.44720        0.39600        1.17320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

