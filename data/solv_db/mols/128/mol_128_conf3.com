%nproc=16
%mem=2GB
%chk=mol_128_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.85230        0.25440       -0.29200
C         -1.34040        0.52360       -0.34990
C         -0.60990       -0.51060        0.41470
C          0.85230       -0.26110        0.37020
C          1.47230       -0.28720       -0.96640
O          0.90280       -0.52330       -2.01340
C          2.95530        0.01310       -0.98500
H         -3.32870        1.22040       -0.62060
H         -3.18270        0.07790        0.72910
H         -3.14020       -0.50820       -1.02740
H         -1.10040        0.66410       -1.39520
H         -1.22020        1.49560        0.21860
H         -0.81870       -1.55320        0.09880
H         -0.94810       -0.42500        1.48630
H          1.37110       -1.04100        1.00430
H          1.11940        0.70980        0.87550
H          3.38300       -0.60380       -0.17310
H          3.41470       -0.34860       -1.92630
H          3.07070        1.10320       -0.82660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_128_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

