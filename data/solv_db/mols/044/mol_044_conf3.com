%nproc=16
%mem=2GB
%chk=mol_044_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51550        0.84160       -0.60920
C         -2.12350        0.11000        0.65310
C         -1.19620       -1.06110        0.41030
C          0.07980       -0.68170       -0.23350
O          0.09300       -0.03350       -1.30100
O          1.28840       -1.01720        0.33560
C          2.52110       -0.67440       -0.20780
C          2.77270        0.79670       -0.36540
H         -2.76880        0.17000       -1.43740
H         -1.71600        1.54840       -0.94040
H         -3.39540        1.50490       -0.33050
H         -1.73700        0.82680        1.37220
H         -3.09050       -0.29070        1.08220
H         -1.04750       -1.55500        1.38760
H         -1.75320       -1.81410       -0.21620
H          2.75450       -1.17900       -1.16870
H          3.29980       -1.04230        0.53330
H          3.28090        1.01310       -1.35550
H          3.45010        1.19520        0.45310
H          1.80320        1.34210       -0.28520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

