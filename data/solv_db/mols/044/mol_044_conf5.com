%nproc=16
%mem=2GB
%chk=mol_044_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.46390       -0.00350       -0.10920
C         -2.04900       -0.52490        0.09640
C         -1.05770        0.42290       -0.51220
C          0.31440       -0.06980       -0.31920
O          0.77840       -1.02610       -0.99260
O          1.17050        0.50020        0.61450
C          2.49440        0.04130        0.80320
C          3.34400        0.17450       -0.43710
H         -3.56020        0.21410       -1.19470
H         -4.20250       -0.76920        0.16120
H         -3.66570        0.91600        0.44780
H         -1.99450       -1.50710       -0.42890
H         -1.83210       -0.64140        1.17180
H         -1.28550        0.56520       -1.60650
H         -1.17370        1.43090       -0.04090
H          2.96900        0.64060        1.60990
H          2.45530       -1.01740        1.14180
H          4.02270        1.06180       -0.35560
H          3.96200       -0.73900       -0.52320
H          2.77420        0.33070       -1.36680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

