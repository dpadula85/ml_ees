%nproc=16
%mem=2GB
%chk=mol_044_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.40280       -0.30770        0.19600
C          2.04090        0.33650       -0.02290
C          0.94110       -0.56040        0.53810
C         -0.35580        0.10320        0.30350
O         -0.45860        1.21500       -0.26650
O         -1.51510       -0.51970        0.72680
C         -2.76790        0.07270        0.52530
C         -2.96130        0.25730       -0.96650
H          3.25210       -1.24850        0.77350
H          3.83800       -0.50860       -0.80270
H          4.05840        0.33500        0.82680
H          1.84130        0.51290       -1.10580
H          1.99740        1.30960        0.50400
H          1.16210       -0.70100        1.62790
H          0.97320       -1.56990        0.09470
H         -2.89820        1.03530        1.07250
H         -3.53380       -0.63520        0.94600
H         -3.91330       -0.23440       -1.26220
H         -2.96080        1.32860       -1.24550
H         -2.14250       -0.22070       -1.55020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

