%nproc=16
%mem=2GB
%chk=mol_044_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.26890        0.48640       -0.22290
C          1.81650        0.14470       -0.51720
C          1.23860       -0.59480        0.67240
C         -0.15610       -0.93060        0.32170
O         -0.48960       -2.10900        0.08960
O         -1.11550        0.04950        0.21200
C         -2.42960       -0.37110       -0.16740
C         -3.26680        0.88640       -0.26070
H          3.93520       -0.30860       -0.63290
H          3.40510        0.61570        0.87150
H          3.50490        1.40770       -0.78490
H          1.78440       -0.48150       -1.40870
H          1.23250        1.09090       -0.58910
H          1.26180        0.06600        1.56250
H          1.78640       -1.53810        0.87400
H         -2.43900       -0.98370       -1.07170
H         -2.88860       -0.94460        0.69720
H         -2.90890        1.57190        0.52840
H         -4.34330        0.61460       -0.08700
H         -3.19700        1.32840       -1.28740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

