%nproc=16
%mem=2GB
%chk=mol_044_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87210        0.92020        0.53730
C         -2.00390       -0.58420        0.63060
C         -1.50910       -1.24580       -0.63490
C         -0.08360       -0.94350       -0.90660
O          0.42420       -1.44880       -1.94020
O          0.71430       -0.15120       -0.10670
C          2.08940        0.10400       -0.42450
C          2.73590        0.98790        0.59350
H         -1.31280        1.23050       -0.34720
H         -1.42350        1.29980        1.47870
H         -2.88300        1.41330        0.45330
H         -1.49160       -0.97990        1.52960
H         -3.08400       -0.81640        0.72050
H         -1.63870       -2.34320       -0.56960
H         -2.18060       -0.90680       -1.45870
H          2.05570        0.66900       -1.39190
H          2.65850       -0.82650       -0.54340
H          3.30420        0.39550        1.34510
H          3.49440        1.60890        0.06770
H          2.00630        1.61720        1.13210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_044_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

