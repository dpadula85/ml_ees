%nproc=16
%mem=2GB
%chk=mol_635_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.07710        0.48660        0.34830
C          1.58020        0.38570        0.62780
C          0.89670       -0.48850       -0.37860
C         -0.56780       -0.58940       -0.05850
C         -1.20570       -1.46660       -1.08950
C         -1.14770        0.73630        0.14180
C         -2.61320        0.79160        0.45400
H          3.49010        1.42650        0.75260
H          3.28690        0.43230       -0.72860
H          3.55660       -0.38860        0.86080
H          1.20850        1.42780        0.50810
H          1.39370       -0.00790        1.62720
H          1.36010       -1.49670       -0.44070
H          1.02220       -0.06850       -1.41990
H         -0.61560       -1.16890        0.91620
H         -1.86610       -2.23190       -0.62960
H         -1.82650       -0.92030       -1.82040
H         -0.46000       -2.04180       -1.69240
H         -0.88190        1.42590       -0.67910
H         -0.65700        1.17680        1.06660
H         -3.23310        0.12050       -0.13610
H         -2.95770        1.83440        0.24090
H         -2.83990        0.62450        1.52910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

