%nproc=16
%mem=2GB
%chk=mol_635_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.89200       -0.83370       -0.01330
C          1.72620       -0.04560       -0.53930
C          0.45300       -0.34950        0.21020
C         -0.65180        0.48930       -0.39300
C         -0.36130        1.95250       -0.29030
C         -1.98060        0.18990        0.29670
C         -2.34380       -1.27460        0.13560
H          3.78830       -0.53150       -0.57650
H          2.69920       -1.89950       -0.15550
H          3.09280       -0.65560        1.05560
H          1.60110       -0.28670       -1.60840
H          1.95090        1.01950       -0.43540
H          0.19660       -1.41480        0.13060
H          0.55220       -0.02040        1.26970
H         -0.79240        0.16010       -1.46230
H         -0.18010        2.37250       -1.31600
H         -1.24080        2.49050        0.12700
H          0.49360        2.19700        0.37430
H         -1.86090        0.46960        1.34840
H         -2.77380        0.78130       -0.17770
H         -1.72800       -1.84690        0.83600
H         -2.12090       -1.53420       -0.92890
H         -3.41130       -1.42940        0.31030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

