%nproc=16
%mem=2GB
%chk=mol_635_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.80610        0.82410       -0.38750
C          1.37030        0.76360        0.12910
C          0.73090       -0.54460       -0.25630
C         -0.65220       -0.77160        0.19770
C         -1.05380       -2.17880       -0.31920
C         -1.73340        0.10770       -0.28530
C         -1.67230        1.55210        0.01260
H          3.45520        0.31400        0.35340
H          2.92400        0.34050       -1.36260
H          3.16640        1.87630       -0.43780
H          1.35630        0.87810        1.21860
H          0.89230        1.65830       -0.31580
H          1.39670       -1.32350        0.23740
H          0.88840       -0.76530       -1.34840
H         -0.62710       -0.86260        1.31290
H         -2.00780       -2.46400        0.15810
H         -0.23910       -2.89530       -0.16910
H         -1.23550       -2.02600       -1.40340
H         -1.85030       -0.04910       -1.38410
H         -2.69560       -0.30780        0.15430
H         -2.72790        1.90850        0.21820
H         -1.36540        2.17220       -0.85540
H         -1.12610        1.79330        0.94670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

