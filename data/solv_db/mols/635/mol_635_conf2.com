%nproc=16
%mem=2GB
%chk=mol_635_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.19260        0.19500       -0.64760
C          1.95570        0.05180        0.20940
C          0.71670        0.32920       -0.59770
C         -0.51500        0.19120        0.23780
C         -0.67510       -1.18500        0.81950
C         -1.71160        0.45970       -0.66550
C         -2.97710        0.33010        0.14060
H          2.96370        0.88940       -1.47100
H          3.40040       -0.79240       -1.11720
H          4.04060        0.54790       -0.02750
H          2.04820        0.76480        1.04110
H          1.98120       -0.97870        0.62880
H          0.78570        1.33330       -1.01000
H          0.67620       -0.38270       -1.44430
H         -0.48160        0.93620        1.08090
H         -0.83280       -1.90890       -0.02980
H          0.17890       -1.52780        1.40970
H         -1.60280       -1.16670        1.45540
H         -1.68310       -0.31190       -1.46020
H         -1.57660        1.43640       -1.13590
H         -2.84000        0.75470        1.15560
H         -3.84740        0.78680       -0.36050
H         -3.19670       -0.75220        0.25110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

