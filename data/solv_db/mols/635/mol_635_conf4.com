%nproc=16
%mem=2GB
%chk=mol_635_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65810        0.21430       -0.24130
C          1.60420        0.99460        0.46180
C          0.22410        0.87080       -0.05320
C         -0.39420       -0.47370       -0.03940
C          0.27610       -1.54180       -0.82890
C         -1.84620       -0.41730       -0.51910
C         -2.68580        0.47740        0.35660
H          2.44250       -0.06930       -1.27670
H          3.62820        0.80280       -0.30290
H          2.94370       -0.67570        0.39210
H          1.63140        0.77790        1.56330
H          1.91840        2.08060        0.41610
H         -0.42730        1.52600        0.60190
H          0.16340        1.34140       -1.06820
H         -0.42120       -0.82870        1.02550
H         -0.48820       -2.37590       -0.92450
H          0.46890       -1.25670       -1.88430
H          1.14470       -2.01760       -0.37610
H         -2.20440       -1.46480       -0.52500
H         -1.80020       -0.04960       -1.56800
H         -2.97850        1.40140       -0.22210
H         -2.21750        0.74460        1.30600
H         -3.64010       -0.06080        0.58600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_635_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

