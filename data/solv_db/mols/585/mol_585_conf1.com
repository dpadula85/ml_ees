%nproc=16
%mem=2GB
%chk=mol_585_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.16610        0.00530       -0.00530
Br         1.76260       -0.02680       -0.05030
H         -0.61190       -0.60920       -0.81600
H         -0.49770        1.05390       -0.08550
H         -0.48700       -0.42310        0.95710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_585_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_585_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_585_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

