%nproc=16
%mem=2GB
%chk=mol_150_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.43070        1.19140        0.03670
C         -0.95220        1.15370       -0.11400
C         -1.64370       -0.03350       -0.23700
C         -0.90400       -1.19690       -0.20460
C          0.47050       -1.16230       -0.05500
C          1.16450        0.02210        0.06820
N          2.57670        0.04290        0.22140
O          3.18520       -1.04300        0.24260
O          3.30160        1.19330        0.34650
O         -3.03350       -0.04790       -0.38750
H          0.96700        2.11500        0.13210
H         -1.53500        2.07030       -0.13950
H         -1.43190       -2.13300       -0.29970
H          1.06420       -2.08350       -0.02830
H         -3.66020       -0.08860        0.41810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

