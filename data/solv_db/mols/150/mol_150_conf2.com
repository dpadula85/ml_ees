%nproc=16
%mem=2GB
%chk=mol_150_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.52810        1.13110        0.11240
C         -0.84750        1.23990        0.18360
C         -1.65440        0.12040        0.14050
C         -1.01390       -1.09570        0.02370
C          0.36450       -1.21180       -0.04820
C          1.16180       -0.08710       -0.00420
N          2.58510       -0.16760       -0.07550
O          3.28190        0.86550       -0.03350
O          3.21710       -1.38170       -0.19180
O         -3.04280        0.20780        0.21060
H          1.17850        2.00380        0.14510
H         -1.28500        2.22810        0.27470
H         -1.67010       -1.97030       -0.00870
H          0.83290       -2.18460       -0.13950
H         -3.63630        0.30220       -0.58930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

