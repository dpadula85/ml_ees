%nproc=16
%mem=2GB
%chk=mol_150_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.42480        1.01150        0.64130
C         -0.96740        1.05320        0.56810
C         -1.63970        0.07980       -0.13310
C         -0.90780       -0.91280       -0.74520
C          0.47790       -0.96640       -0.68050
C          1.14330        0.01300        0.02430
N          2.55900       -0.05750        0.07960
O          3.19420        0.80810        0.70570
O          3.27760       -1.04810       -0.53230
O         -3.02370        0.14120       -0.19250
H          0.97840        1.76790        1.18830
H         -1.49250        1.87270        1.07910
H         -1.42930       -1.69060       -1.30450
H          1.03520       -1.77080       -1.17910
H         -3.63000       -0.30130        0.48080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_150_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

