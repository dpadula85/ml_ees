%nproc=16
%mem=2GB
%chk=mol_049_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43720        1.18560       -0.04490
C          1.03870        0.63000        0.29350
C          0.89560       -0.74740       -0.25570
C         -0.37300       -1.46060       -0.01850
C         -1.62240       -0.86280       -0.53500
C         -2.05780        0.41720        0.00450
O         -1.94450        0.65450        1.22740
O         -2.57540        1.38090       -0.83900
H          3.17100        0.36170        0.04910
H          2.41090        1.54780       -1.09230
H          2.61530        2.03260        0.65800
H          0.89600        0.66230        1.39010
H          0.35630        1.36610       -0.19380
H          1.19090       -0.77870       -1.35030
H          1.72750       -1.36120        0.21930
H         -0.26960       -2.46190       -0.55060
H         -0.42990       -1.75810        1.05430
H         -2.44500       -1.64110       -0.41750
H         -1.51320       -0.83000       -1.66930
H         -3.50840        1.66320       -0.96570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

