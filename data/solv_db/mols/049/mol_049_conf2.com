%nproc=16
%mem=2GB
%chk=mol_049_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.12280       -0.13170        0.37800
C         -1.84600        0.46690       -0.22560
C         -0.70830       -0.40160        0.27520
C          0.63330        0.06620       -0.24000
C          1.67800       -0.87240        0.32430
C          3.01620       -0.49410       -0.12420
O          4.00140       -1.17030        0.25330
O          3.17080        0.60350       -0.95720
H         -3.06310        0.02910        1.46800
H         -4.02660        0.28260       -0.09120
H         -3.07350       -1.21750        0.20640
H         -1.85130        0.40440       -1.32850
H         -1.73550        1.51270        0.12320
H         -0.84610       -1.45740        0.00530
H         -0.64230       -0.36470        1.39110
H          0.87110        1.09470        0.09610
H          0.69890        0.00450       -1.35000
H          1.62090       -0.76720        1.44600
H          1.40250       -1.92800        0.09370
H          3.82220        0.46490       -1.74390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

