%nproc=16
%mem=2GB
%chk=mol_049_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.43740        0.59020       -0.42330
C         -1.89510       -0.64780        0.19950
C         -0.44640       -0.58100        0.53280
C          0.36590       -0.34410       -0.71570
C          1.84820       -0.29980       -0.41280
C          2.22020        0.76950        0.51630
O          1.89840        1.95170        0.28210
O          2.91460        0.46490        1.67210
H         -2.34890        1.49480        0.23850
H         -3.50910        0.43450       -0.69580
H         -1.94150        0.82230       -1.38080
H         -2.51890       -0.89780        1.10960
H         -2.07120       -1.48530       -0.53890
H         -0.08290       -1.53260        0.99620
H         -0.26160        0.24750        1.28660
H          0.19350       -1.23200       -1.37450
H          0.02620        0.53530       -1.27060
H          2.34600       -0.11160       -1.40850
H          2.15750       -1.31450       -0.07900
H          3.54250        1.13560        2.08560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

