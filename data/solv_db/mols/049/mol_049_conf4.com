%nproc=16
%mem=2GB
%chk=mol_049_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.50940        1.19070       -0.71870
C          1.16340        0.75680       -0.06090
C          0.92320       -0.68110       -0.34450
C         -0.25410       -1.36560        0.19290
C         -1.62650       -0.94290       -0.10250
C         -2.17300        0.29440        0.36260
O         -1.64070        1.07490        1.15070
O         -3.45250        0.66510       -0.10440
H          2.28940        1.27770       -1.79670
H          3.24030        0.40110       -0.45420
H          2.73800        2.15830       -0.22520
H          1.19100        0.97620        0.99860
H          0.44880        1.44400       -0.56230
H          1.85320       -1.20790        0.09070
H          1.08970       -0.91480       -1.45380
H         -0.17070       -1.52590        1.31860
H         -0.21190       -2.46250       -0.15210
H         -1.84580       -0.98760       -1.21830
H         -2.30460       -1.77850        0.27370
H         -3.76650        1.62770       -0.08250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

