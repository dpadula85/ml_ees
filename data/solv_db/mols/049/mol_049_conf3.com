%nproc=16
%mem=2GB
%chk=mol_049_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.46320        0.72070        0.14300
C         -2.02390       -0.70760       -0.06770
C         -0.61460       -0.93870        0.42430
C          0.39010       -0.08000       -0.26920
C          1.75890       -0.38780        0.29620
C          2.82040        0.40500       -0.30380
O          3.60120       -0.14960       -1.13230
O          3.05490        1.76500       -0.06930
H         -2.14770        1.37020       -0.70850
H         -3.57780        0.73570        0.16160
H         -2.05250        1.06420        1.11550
H         -2.70620       -1.32500        0.57450
H         -2.16570       -1.05100       -1.10530
H         -0.51580       -0.78560        1.51130
H         -0.36350       -2.00030        0.21270
H          0.15800        0.98620       -0.03190
H          0.42640       -0.21730       -1.35000
H          1.95420       -1.47410        0.05350
H          1.69360       -0.28500        1.40860
H          2.77300        2.35470       -0.86340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_049_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

