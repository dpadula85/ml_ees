%nproc=16
%mem=2GB
%chk=mol_471_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
N         -0.64870       -0.24020       -0.31390
N          0.64720        0.01270        0.00840
H         -1.09330       -0.74100        0.50800
H         -1.15360        0.69240       -0.33860
H          1.36180       -0.62670       -0.27950
H          0.88660        0.90270        0.41560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

