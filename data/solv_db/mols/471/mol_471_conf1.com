%nproc=16
%mem=2GB
%chk=mol_471_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
N          0.68590        0.09810       -0.21600
N         -0.67980       -0.18890       -0.31870
H          0.88670        0.79490        0.50870
H          1.28610       -0.73410       -0.22360
H         -1.00890       -0.71770        0.53310
H         -1.16990        0.74770       -0.28350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

