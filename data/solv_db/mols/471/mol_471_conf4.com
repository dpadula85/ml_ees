%nproc=16
%mem=2GB
%chk=mol_471_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
N         -0.65900       -0.14070       -0.16350
N          0.66870       -0.11130        0.11900
H         -1.26850       -0.60410        0.55870
H         -1.08140        0.70840       -0.55360
H          1.03730        0.72010        0.59100
H          1.30300       -0.57240       -0.55160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

