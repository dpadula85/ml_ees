%nproc=16
%mem=2GB
%chk=mol_471_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
N         -0.67120        0.03170       -0.19410
N          0.66450       -0.00910       -0.03620
H         -1.17580       -0.84760        0.02320
H         -1.15470        0.87560        0.16540
H          1.27220        0.09900       -0.83650
H          1.06500       -0.14970        0.87820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

