%nproc=16
%mem=2GB
%chk=mol_471_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
N         -0.66570       -0.25140       -0.24860
N          0.66150        0.12510       -0.18660
H         -1.22050        0.59550       -0.53740
H         -1.01120       -0.56420        0.69460
H          0.89710        0.73050        0.62890
H          1.33890       -0.63560       -0.35080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_471_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

