%nproc=16
%mem=2GB
%chk=mol_292_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.55910        0.94090        0.99270
C          1.96010       -0.30200        1.14920
C          1.32580       -1.19920        0.12280
C          0.63810       -0.30380       -0.84660
C          0.63390        1.02800       -0.12390
C         -0.81040        1.18470        0.25200
C         -1.31900        0.02770        1.03550
C         -1.34680       -1.05730        0.27510
C         -0.82300       -0.63270       -1.06090
C         -1.45100        0.77090       -1.09610
Cl        -3.18540        0.68030       -0.88130
Cl        -0.90970        1.73440       -2.42300
Cl        -1.20300       -1.63800       -2.38210
Cl        -1.88180       -2.67600        0.68280
Cl        -1.80820        0.07890        2.70280
Cl        -1.29480        2.73400        0.80170
Cl         2.62520       -2.13060       -0.66050
H          1.87110        1.78090        1.60160
H          2.64240       -0.66810        1.89110
H          0.69630       -1.90230        0.66120
H          1.19430       -0.24630       -1.81400
H          0.88650        1.79560       -0.88000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

