%nproc=16
%mem=2GB
%chk=mol_292_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.06410       -1.46780        0.45760
C          1.66170       -1.20130       -0.67570
C          1.58550        0.24550       -0.95490
C          0.75930        0.87150        0.10220
C          0.51140       -0.24880        1.08380
C         -0.99140       -0.39860        1.09560
C         -1.36070       -0.92900       -0.25320
C         -1.13260        0.03700       -1.13440
C         -0.61440        1.22920       -0.41620
C         -1.41190        1.06840        0.89540
Cl        -0.83720        2.11410        2.15870
Cl        -3.12990        1.16580        0.57570
Cl        -0.78400        2.73720       -1.18610
Cl        -1.38990       -0.05140       -2.85770
Cl        -1.97470       -2.54390       -0.52560
Cl        -1.68490       -1.18180        2.44070
Cl         3.23190        0.90390       -1.03150
H          0.97420       -2.47600        0.90390
H          2.15710       -1.92350       -1.34790
H          1.15720        0.35980       -1.97280
H          1.28090        1.73360        0.56010
H          0.92840       -0.04410        2.08230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

