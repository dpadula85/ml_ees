%nproc=16
%mem=2GB
%chk=mol_292_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.97800        1.41380        0.62460
C          1.75290        0.47930        1.13990
C          1.78920       -0.73870        0.30340
C          0.74720       -0.49140       -0.73120
C          0.36150        0.97690       -0.65060
C         -1.14050        0.94900       -0.45890
C         -1.35830        0.46630        0.93310
C         -0.98970       -0.80700        0.94670
C         -0.53270       -1.20970       -0.41000
C         -1.52750       -0.33930       -1.20800
Cl        -3.18540       -0.71560       -0.79140
Cl        -1.17620       -0.28230       -2.89910
Cl        -0.59780       -2.87710       -0.77270
Cl        -1.02810       -1.86190        2.33200
Cl        -1.98350        1.47810        2.21230
Cl        -1.99690        2.34520       -0.92850
Cl         3.42610       -0.86000       -0.39010
H          0.81120        2.38690        1.07920
H          2.30540        0.58380        2.07240
H          1.60820       -1.67290        0.87490
H          1.05750       -0.76900       -1.75700
H          0.67940        1.54580       -1.52090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

