%nproc=16
%mem=2GB
%chk=mol_292_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12590       -1.18910        1.05960
C         -1.68220       -0.05520        1.45760
C         -1.55120        1.01820        0.45940
C         -0.79240        0.41470       -0.66750
C         -0.53490       -1.04620       -0.27280
C          0.99240       -1.15270       -0.25200
C          1.51510       -0.30180        0.83550
C          1.28040        0.98540        0.49430
C          0.59070        0.97920       -0.84190
C          1.28240       -0.24830       -1.46420
Cl         2.97720       -0.00470       -1.73400
Cl         0.39330       -0.85710       -2.84580
Cl         0.73010        2.41890       -1.75130
Cl         1.70590        2.37810        1.44080
Cl         2.30450       -0.79190        2.32120
Cl         1.61840       -2.73780       -0.37280
Cl        -3.15870        1.57210       -0.10270
H         -1.10670       -2.10520        1.63750
H         -2.18510        0.07750        2.41970
H         -0.98630        1.89640        0.84960
H         -1.31550        0.46380       -1.63890
H         -0.95150       -1.71430       -1.03130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_292_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

