%nproc=16
%mem=2GB
%chk=mol_167_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.58940        0.48330       -0.68650
C          1.92830       -0.11660        0.50130
C          0.46580       -0.07450        0.48410
C         -0.30170        0.93910        0.98750
C         -1.69130        0.90960        0.93520
C         -2.34890       -0.16690        0.36120
C         -1.58190       -1.19380       -0.14910
C         -0.19750       -1.15160       -0.09030
O         -3.73830       -0.23970        0.28640
H          3.66710        0.14740       -0.67990
H          2.51800        1.56730       -0.77230
H          2.17280        0.03150       -1.62500
H          2.28660        0.40970        1.42350
H          2.27640       -1.18690        0.57370
H          0.20940        1.78880        1.44050
H         -2.29540        1.71740        1.33670
H         -2.11330       -2.02360       -0.59260
H          0.35760       -1.98930       -0.50840
H         -4.20300        0.14910       -0.54120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

