%nproc=16
%mem=2GB
%chk=mol_167_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.76220        0.34530       -0.03970
C         -1.72680       -0.08870       -1.01340
C         -0.34870       -0.09710       -0.46210
C          0.42460        1.04220       -0.56430
C          1.71090        1.06820       -0.06190
C          2.20820       -0.07020        0.54560
C          1.46360       -1.22750        0.66580
C          0.18240       -1.21370        0.15110
O          3.51710       -0.02480        1.05000
H         -3.71130       -0.20150       -0.26300
H         -2.98200        1.41140       -0.18360
H         -2.42160        0.15510        0.99360
H         -1.90660       -1.10810       -1.42230
H         -1.73460        0.61820       -1.88540
H          0.02260        1.94450       -1.04880
H          2.32460        1.97790       -0.14540
H          1.88940       -2.11590        1.15500
H         -0.41000       -2.11250        0.23860
H          4.26060       -0.30290        0.39870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

