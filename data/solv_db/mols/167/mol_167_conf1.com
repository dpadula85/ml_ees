%nproc=16
%mem=2GB
%chk=mol_167_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65930       -0.64260       -0.37640
C          1.87160        0.63510       -0.19280
C          0.41220        0.40280       -0.13480
C         -0.23910        0.16210        1.05770
C         -1.60300       -0.05360        1.09980
C         -2.38290       -0.04060       -0.03750
C         -1.73420        0.19970       -1.23130
C         -0.36640        0.41630       -1.27560
O         -3.75560       -0.25820        0.01000
H          2.95580       -1.09120        0.60070
H          2.13430       -1.40840       -0.94970
H          3.63630       -0.33020       -0.84760
H          2.26350        1.10820        0.74890
H          2.13410        1.38070       -0.99860
H          0.38900        0.15460        1.95000
H         -2.07830       -0.23990        2.06190
H         -2.34480        0.21140       -2.13630
H          0.14340        0.60620       -2.22000
H         -4.09520       -1.21240       -0.07670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

