%nproc=16
%mem=2GB
%chk=mol_167_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.57740        0.73580        0.22710
C         -1.90280       -0.45870       -0.44870
C         -0.43410       -0.33890       -0.25440
C          0.33570        0.33830       -1.17770
C          1.69870        0.43730       -0.97520
C          2.31730       -0.12550        0.13180
C          1.53180       -0.79930        1.04500
C          0.16660       -0.90580        0.85440
O          3.70240       -0.01350        0.31880
H         -3.66150        0.67860        0.13020
H         -2.23930        1.67060       -0.28280
H         -2.29640        0.73130        1.31250
H         -2.17770       -0.46210       -1.51940
H         -2.24560       -1.37590        0.04270
H         -0.09940        0.79820       -2.06370
H          2.29530        0.96530       -1.69600
H          2.01740       -1.23650        1.90610
H         -0.43140       -1.44940        1.60030
H          4.00050        0.81030        0.84900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

