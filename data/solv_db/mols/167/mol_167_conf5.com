%nproc=16
%mem=2GB
%chk=mol_167_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47140        1.12600        0.23350
C         -1.91040        0.00350       -0.58850
C         -0.46410       -0.19880       -0.30340
C          0.49110        0.47730       -1.03280
C          1.84000        0.30090       -0.78180
C          2.29440       -0.55320        0.20250
C          1.34750       -1.23470        0.93830
C         -0.00500       -1.05100        0.67900
O          3.63970       -0.74810        0.47420
H         -3.24950        1.63970       -0.34110
H         -1.72820        1.87560        0.53590
H         -2.97670        0.65660        1.12680
H         -2.48720       -0.91410       -0.41470
H         -2.05820        0.26700       -1.67740
H          0.12960        1.15910       -1.81860
H          2.58040        0.84080       -1.36520
H          1.66980       -1.91580        1.72160
H         -0.75710       -1.57810        1.24760
H          4.11520       -0.15280        1.16420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_167_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

