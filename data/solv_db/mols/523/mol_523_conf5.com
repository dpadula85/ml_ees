%nproc=16
%mem=2GB
%chk=mol_523_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07350       -0.68760        0.58200
N         -0.02150        0.12790       -0.01690
C          1.22660       -0.48770       -0.45610
C         -0.25210        1.52030       -0.15520
O          0.66880        2.21130       -0.67550
H         -1.56270       -0.13890        1.41150
H         -0.58060       -1.59200        0.97380
H         -1.83410       -0.98520       -0.15800
H          2.03410        0.05070        0.07660
H          1.39070       -0.38470       -1.53880
H          1.18530       -1.56550       -0.22310
H         -1.18090        1.93140        0.17970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

