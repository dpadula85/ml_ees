%nproc=16
%mem=2GB
%chk=mol_523_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.27630       -0.48910        0.20100
N         -0.02490        0.16940        0.04330
C         -1.06750       -0.63370       -0.55900
C         -0.26300        1.48880        0.44320
O          0.61600        2.20400        0.96470
H          1.08760       -1.33530        0.90870
H          2.03560        0.17980        0.60380
H          1.52400       -0.89320       -0.81000
H         -1.81070       -0.00950       -1.08450
H         -1.51650       -1.23140        0.27580
H         -0.61130       -1.36260       -1.28480
H         -1.24570        1.91290        0.29790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

