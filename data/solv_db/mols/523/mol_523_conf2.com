%nproc=16
%mem=2GB
%chk=mol_523_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.11610        0.20420       -0.75270
N         -0.09210        0.10700        0.04890
C         -0.54390       -1.23340        0.41990
C         -0.81490        1.24140        0.46400
O         -0.39250        2.36570        0.12470
H          0.98850        0.94700       -1.57440
H          1.42620       -0.76590       -1.13230
H          1.90140        0.62340       -0.08750
H         -1.55850       -1.18390        0.85000
H         -0.54290       -1.82000       -0.54500
H          0.20630       -1.63020        1.13600
H         -1.69370        1.14470        1.04840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

