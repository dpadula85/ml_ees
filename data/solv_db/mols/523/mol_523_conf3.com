%nproc=16
%mem=2GB
%chk=mol_523_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32160       -0.38970        0.00620
N          0.05170        0.08840        0.00310
C          1.17110       -0.80170       -0.06590
C          0.24140        1.48960        0.07170
O         -0.73000        2.29570        0.13330
H         -1.69300       -0.31520        1.04940
H         -1.88650        0.29090       -0.67350
H         -1.41400       -1.42380       -0.35540
H          2.09670       -0.23260       -0.25470
H          0.96340       -1.53850       -0.88850
H          1.26080       -1.31380        0.90480
H          1.26000        1.85060        0.06970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

