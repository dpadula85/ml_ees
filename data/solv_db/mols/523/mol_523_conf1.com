%nproc=16
%mem=2GB
%chk=mol_523_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.82630       -1.11450       -0.10140
N          0.10960       -0.01820        0.00990
C         -0.34440        1.34060       -0.20710
C          1.45870       -0.32030        0.33270
O          2.25710        0.65260        0.42440
H         -0.46730       -1.78860       -0.87440
H         -0.79970       -1.65130        0.87830
H         -1.84470       -0.72870       -0.24260
H          0.00820        1.92580        0.68210
H          0.12810        1.68390       -1.14840
H         -1.44600        1.36180       -0.24230
H          1.76670       -1.34310        0.48860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_523_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

