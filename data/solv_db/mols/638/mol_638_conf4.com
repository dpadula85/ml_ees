%nproc=16
%mem=2GB
%chk=mol_638_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.03000        0.28320       -0.46660
C         -2.26470       -0.38400        0.63730
C         -0.81030       -0.26070        0.35890
C         -0.12220       -1.20500       -0.36620
C          1.22280       -1.07190       -0.61260
C          1.90340        0.00460       -0.14000
C          1.20300        0.94450        0.58550
C         -0.12820        0.83300        0.84130
C          3.35530        0.23800       -0.35280
H         -3.85640       -0.35480       -0.83880
H         -3.48110        1.25270       -0.10840
H         -2.40390        0.55160       -1.33660
H         -2.46050        0.07010        1.64110
H         -2.47770       -1.47140        0.72770
H         -0.64290       -2.08080       -0.75850
H          1.76350       -1.81280       -1.18180
H          1.73880        1.82390        0.97630
H         -0.61630        1.61830        1.42400
H          3.82760       -0.50970       -0.98820
H          3.81200        0.24540        0.67990
H          3.46770        1.28570       -0.72170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

