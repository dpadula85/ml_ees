%nproc=16
%mem=2GB
%chk=mol_638_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.98420       -0.86790       -0.30270
C         -2.33020        0.22110        0.55700
C         -0.86230        0.21790        0.32140
C         -0.28210        0.99240       -0.67120
C          1.08240        0.99400       -0.89530
C          1.92620        0.21570       -0.13010
C          1.33420       -0.54950        0.85370
C         -0.02520       -0.56330        1.09110
C          3.40610        0.20170       -0.35490
H         -2.18180       -1.64520       -0.44590
H         -3.27890       -0.41600       -1.27230
H         -3.80070       -1.37070        0.22300
H         -2.80450        1.18470        0.29660
H         -2.57190       -0.02330        1.61310
H         -0.96990        1.60230       -1.26640
H          1.50510        1.61210       -1.68100
H          2.00110       -1.16620        1.46030
H         -0.46220       -1.18230        1.87990
H          3.78380       -0.84950       -0.47660
H          3.60890        0.71050       -1.29960
H          3.90600        0.68140        0.49990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

