%nproc=16
%mem=2GB
%chk=mol_638_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.04390       -0.73840       -0.14160
C         -2.26220        0.47530        0.31280
C         -0.80100        0.30930        0.12710
C          0.01450       -0.22670        1.09950
C          1.37220       -0.37400        0.90990
C          1.88860        0.03770       -0.29770
C          1.09700        0.58260       -1.30130
C         -0.24250        0.71010       -1.07090
C          3.35760       -0.10800       -0.53710
H         -3.25180       -0.61780       -1.23220
H         -2.50160       -1.66220        0.10080
H         -4.03050       -0.75890        0.40270
H         -2.61110        1.39680       -0.19750
H         -2.47540        0.60700        1.39840
H         -0.44070       -0.53920        2.04170
H          2.02600       -0.79310        1.66380
H          1.57660        0.88210       -2.22120
H         -0.86940        1.12820       -1.83300
H          3.66170        0.51870       -1.41950
H          3.87840        0.32110        0.36600
H          3.65740       -1.15070       -0.71120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

