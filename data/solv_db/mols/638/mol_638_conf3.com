%nproc=16
%mem=2GB
%chk=mol_638_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.09170        0.50690       -0.18550
C         -2.17030       -0.49970       -0.78750
C         -0.75110       -0.29270       -0.47150
C          0.04960        0.48840       -1.28150
C          1.36940        0.67040       -0.97210
C          1.89770        0.06200        0.16500
C          1.13140       -0.72520        0.99900
C         -0.19600       -0.88940        0.65990
C          3.32970        0.23630        0.53480
H         -3.97330        0.63100       -0.88080
H         -3.47100        0.16290        0.81230
H         -2.67400        1.49940       -0.03460
H         -2.28730       -0.58670       -1.90400
H         -2.44700       -1.51260       -0.40110
H         -0.42820        0.93460       -2.15490
H          1.99210        1.28210       -1.60800
H          1.55820       -1.20140        1.89490
H         -0.81470       -1.50440        1.30250
H          3.84650        0.74870       -0.27500
H          3.75690       -0.79650        0.61530
H          3.37300        0.78580        1.49430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

