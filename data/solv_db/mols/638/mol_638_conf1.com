%nproc=16
%mem=2GB
%chk=mol_638_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.98040       -0.03220       -0.82650
C         -2.29270        0.03220        0.49730
C         -0.81350        0.01540        0.31360
C         -0.15330       -1.19420        0.29240
C          1.21950       -1.18350        0.12060
C          1.91510        0.00060       -0.02610
C          1.22150        1.19340        0.00000
C         -0.14070        1.21010        0.16920
C          3.37780        0.02030       -0.20990
H         -3.39670       -1.06750       -0.93390
H         -2.26960        0.18340       -1.64600
H         -3.86860        0.65110       -0.86660
H         -2.55230       -0.81800        1.17260
H         -2.58240        0.97490        1.02680
H         -0.71950       -2.11230        0.40950
H          1.72720       -2.14770        0.10610
H          1.75320        2.13620       -0.11410
H         -0.70690        2.15280        0.19220
H          3.68720       -0.79590       -0.89810
H          3.85660       -0.20230        0.76490
H          3.71860        0.98320       -0.62920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_638_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

