%nproc=16
%mem=2GB
%chk=mol_078_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44470       -1.67120       -0.11700
C         -2.49740       -0.33170       -0.82030
C         -1.57390        0.61240       -0.16780
C         -1.67680        1.94650       -0.92640
N         -0.21500        0.30300       -0.04790
C          0.53600       -0.42440       -0.90860
O         -0.04450       -0.87220       -1.90200
C          1.86420       -0.67700       -0.73610
C          2.51340       -0.17240        0.37590
N          1.75700        0.55300        1.22940
C          0.44710        0.78180        1.02670
O         -0.13750        1.48060        1.91240
C          3.95730       -0.42440        0.60040
Br         2.81120       -1.71700       -2.01990
H         -3.23250       -1.78380        0.65710
H         -1.47370       -1.78910        0.44830
H         -2.59130       -2.50820       -0.83650
H         -2.45990       -0.39760       -1.90650
H         -3.54460        0.05310       -0.60850
H         -1.98860        0.83180        0.83980
H         -2.51300        2.55600       -0.53180
H         -1.94780        1.72930       -2.00060
H         -0.72650        2.48150       -0.94680
H          2.25570        0.93720        2.07740
H          4.56740        0.49850        0.43000
H          4.07230       -0.78840        1.65330
H          4.28610       -1.20740       -0.09730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

