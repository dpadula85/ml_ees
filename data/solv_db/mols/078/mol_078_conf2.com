%nproc=16
%mem=2GB
%chk=mol_078_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.63120        0.08980       -0.52760
C         -2.19670        0.52830       -0.40090
C         -1.30130       -0.58270        0.08060
C         -1.71440       -1.10280        1.43960
N          0.09480       -0.24580        0.00550
C          0.86860       -0.67140       -1.02200
O          0.39570       -1.37960       -1.94970
C          2.20310       -0.34810       -1.09530
C          2.72090        0.42890       -0.07710
N          1.91400        0.82960        0.92500
C          0.61060        0.51720        1.00190
O         -0.12740        0.90110        1.94570
C          4.15480        0.82190       -0.08190
Br         3.29190       -0.95100       -2.54670
H         -3.75770       -0.99910       -0.54990
H         -4.12420        0.49880       -1.44800
H         -4.25460        0.46900        0.32400
H         -1.87110        0.86760       -1.40490
H         -2.18100        1.41780        0.25350
H         -1.46030       -1.43770       -0.62830
H         -2.35880       -2.02710        1.35240
H         -0.77710       -1.47590        1.92810
H         -2.24570       -0.35310        2.04930
H          2.27990        1.42710        1.72240
H          4.52190        0.87670        0.97930
H          4.74170        0.06840       -0.64580
H          4.20310        1.83210       -0.53210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

