%nproc=16
%mem=2GB
%chk=mol_078_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.59600       -0.74820       -0.10260
C         -2.11700       -0.84490       -0.28160
C         -1.42090        0.24730        0.51210
C         -1.86540        1.62170        0.05610
N          0.02490        0.10900        0.44220
C          0.73360       -0.30790        1.51220
O          0.12770       -0.57840        2.57840
C          2.10490       -0.43000        1.41890
C          2.75150       -0.12940        0.24300
N          2.02200        0.28420       -0.80900
C          0.67650        0.39630       -0.69330
O         -0.01070        0.78290       -1.67520
C          4.22950       -0.27380        0.18340
Br         3.12900       -1.01700        2.91630
H         -3.88040       -1.05270        0.94140
H         -4.11860       -1.39290       -0.85270
H         -3.99390        0.27350       -0.24660
H         -1.87110       -0.81410       -1.35110
H         -1.78610       -1.81690        0.13870
H         -1.77300        0.13770        1.55890
H         -2.22760        1.61730       -0.98980
H         -2.59260        2.07300        0.73960
H         -0.94640        2.26840        0.05820
H          2.48660        0.51920       -1.71380
H          4.52010       -1.32030       -0.03720
H          4.68970        0.40240       -0.56820
H          4.70380       -0.00620        1.15880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

