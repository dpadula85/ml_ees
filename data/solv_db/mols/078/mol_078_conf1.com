%nproc=16
%mem=2GB
%chk=mol_078_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.22130       -1.68200        0.51310
C         -2.50830       -0.40370       -0.18810
C         -1.56860        0.70590        0.19280
C         -1.92680        1.98430       -0.56770
N         -0.18740        0.37950        0.08010
C          0.60270        0.42450        1.16840
O          0.12840        0.75710        2.30880
C          1.94500        0.12660        1.15080
C          2.49130       -0.23860       -0.07350
N          1.66340       -0.26880       -1.12980
C          0.35840        0.02440       -1.09470
O         -0.36550       -0.02810       -2.14880
C          3.94910       -0.58120       -0.18930
Br         3.05000        0.19560        2.70190
H         -1.82040       -2.45160       -0.17750
H         -3.07940       -2.13380        1.03060
H         -1.43990       -1.50760        1.27570
H         -2.59390       -0.58590       -1.25670
H         -3.53290       -0.06770        0.15270
H         -1.78860        0.93050        1.25680
H         -2.61220        2.62410        0.00450
H         -0.99920        2.56470       -0.72930
H         -2.44320        1.71910       -1.51600
H          2.04820       -0.54350       -2.08000
H          4.19030       -1.31420        0.60140
H          4.54820        0.34460       -0.08700
H          4.11250       -0.97400       -1.19880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

