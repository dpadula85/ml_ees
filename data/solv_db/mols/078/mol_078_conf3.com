%nproc=16
%mem=2GB
%chk=mol_078_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.45500        0.81190       -0.94810
C         -1.99150        0.88630       -0.66820
C         -1.42610       -0.42130       -0.14810
C         -2.06630       -0.86020        1.12560
N          0.01650       -0.31640       -0.06820
C          0.84510       -0.81170       -1.00300
O          0.33820       -1.39580       -1.99670
C          2.23150       -0.67450       -0.86240
C          2.75600       -0.03840        0.22200
N          1.88840        0.44980        1.14700
C          0.54730        0.30470        0.98900
O         -0.17640        0.77790        1.87710
C          4.22280        0.13100        0.41420
Br         3.36810       -1.39140       -2.21200
H         -4.02790        1.63620       -0.47440
H         -3.61770        0.94300       -2.05750
H         -3.87650       -0.19380       -0.71340
H         -1.80880        1.64140        0.13840
H         -1.40850        1.22240       -1.53910
H         -1.64370       -1.18380       -0.93500
H         -2.49560       -0.04380        1.73050
H         -2.92670       -1.55050        0.90360
H         -1.33750       -1.48320        1.68450
H          2.30780        0.93460        1.97200
H          4.58050        0.78660       -0.40290
H          4.74650       -0.83190        0.46530
H          4.40970        0.67110        1.35970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_078_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

