%nproc=16
%mem=2GB
%chk=mol_366_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.80690       -1.03470       -0.13580
C         -0.41980       -0.52940       -0.01990
C          0.57680       -1.39030        0.37220
C          1.85230       -0.90380        0.53120
C          2.15040        0.27890       -0.10620
C          1.15620        1.14300       -0.47810
C         -0.18140        0.79570       -0.36070
N         -1.18400        1.79440       -0.21410
O         -0.82410        2.99660       -0.13540
O         -2.39690        1.51880        0.39560
H         -2.37300       -0.54250       -0.94810
H         -2.33110       -0.97220        0.85160
H         -1.71900       -2.10720       -0.44760
H          0.42110       -2.44740        0.56120
H          2.53970       -1.32130        1.24620
H          3.15830        0.65820       -0.09180
H          1.38140        2.06320       -1.02040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

