%nproc=16
%mem=2GB
%chk=mol_366_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.28780       -1.64300        0.26810
C          0.21900       -0.62580        0.11860
C         -1.10930       -1.02520        0.16270
C         -2.14730       -0.13000       -0.00290
C         -1.90850        1.21220       -0.22120
C         -0.59200        1.61240       -0.26480
C          0.46560        0.70740       -0.09640
N          1.80010        1.15710       -0.16280
O          2.12540        2.15220       -0.83090
O          2.76800        0.49130        0.51820
H          0.83890       -2.68050        0.19660
H          2.06080       -1.61460       -0.51070
H          1.78750       -1.63370        1.24450
H         -1.30090       -2.08540        0.33280
H         -3.18020       -0.45930        0.03410
H         -2.72670        1.91050       -0.35080
H         -0.38820        2.65430       -0.43500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

