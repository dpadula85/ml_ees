%nproc=16
%mem=2GB
%chk=mol_366_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.80670        1.08540       -0.07190
C         -0.43600        0.56320       -0.24110
C          0.63360        1.39590        0.06870
C          1.84770        0.87690        0.46540
C          2.16770       -0.38040       -0.03330
C          1.12700       -1.19860       -0.45730
C         -0.18620       -0.79270       -0.28140
N         -1.22840       -1.75490       -0.12020
O         -2.34850       -1.44980        0.30670
O         -0.87280       -3.08200        0.05610
H         -2.11940        0.90900        0.96780
H         -1.78240        2.19790       -0.19610
H         -2.52480        0.72050       -0.81860
H          0.55370        2.48360        0.02610
H          2.51510        1.38090        1.16460
H          3.14460       -0.80820        0.12530
H          1.31580       -2.14680       -0.96080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

