%nproc=16
%mem=2GB
%chk=mol_366_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.86760       -0.92180        0.18160
C         -0.54990       -0.42620       -0.27900
C          0.45950       -1.32740       -0.60490
C          1.79630       -1.03650       -0.45190
C          2.16550        0.12300        0.17010
C          1.20820        1.09470        0.42200
C         -0.09530        0.85750        0.00360
N         -1.09770        1.87240        0.06420
O         -1.93840        1.87690       -0.87050
O         -0.69480        3.13340        0.47350
H         -1.97010       -1.96310       -0.21830
H         -1.91980       -1.04930        1.28920
H         -2.72050       -0.35790       -0.21230
H          0.17280       -2.27480       -1.03990
H          2.54010       -1.80610       -0.66090
H          3.04820        0.17570        0.80990
H          1.46350        2.02950        0.92350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

