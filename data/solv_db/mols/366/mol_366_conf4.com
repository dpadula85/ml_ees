%nproc=16
%mem=2GB
%chk=mol_366_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.88590       -0.85060        0.23480
C          0.51000       -0.55480       -0.23420
C         -0.47890       -1.51290       -0.05820
C         -1.79720       -1.15270        0.11280
C         -2.14820        0.18390        0.25060
C         -1.20360        1.11610       -0.14330
C          0.09990        0.73850       -0.43620
N          1.13140        1.72920       -0.52440
O          1.05010        2.47940       -1.52990
O          1.42900        2.34790        0.67870
H          1.99570       -1.95730        0.35370
H          2.10350       -0.33950        1.20700
H          2.64980       -0.43580       -0.46940
H         -0.22270       -2.53010       -0.29510
H         -2.61440       -1.87620        0.10670
H         -2.90220        0.46970        0.98570
H         -1.48810        2.14530       -0.23930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_366_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

