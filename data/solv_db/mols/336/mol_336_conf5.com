%nproc=16
%mem=2GB
%chk=mol_336_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88680        1.44850        0.13010
N         -1.31900        0.12790        0.07480
C         -2.12860       -1.07450        0.06510
C          0.09130       -0.01170        0.01750
C          0.83860       -0.08470        1.16000
C          2.20880       -0.21800        1.08440
C          2.81680       -0.27770       -0.15120
C          2.04840       -0.20320       -1.28920
C          0.68070       -0.06910       -1.22560
H         -1.11260        2.16670        0.50820
H         -2.13550        1.82850       -0.90280
H         -2.77240        1.52980        0.78630
H         -2.02540       -1.62970       -0.91030
H         -3.19690       -0.83370        0.25980
H         -1.69700       -1.75490        0.82930
H          0.33210       -0.03390        2.13550
H          2.77500       -0.26900        2.01060
H          3.89780       -0.38370       -0.17490
H          2.52440       -0.24850       -2.27740
H          0.06040       -0.00910       -2.13000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

