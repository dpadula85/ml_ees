%nproc=16
%mem=2GB
%chk=mol_336_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99510        0.35210       -1.23600
N         -1.32040        0.07270        0.02490
C         -2.06410       -0.16340        1.25390
C          0.09320        0.02250       -0.01050
C          0.69440       -1.17440       -0.31440
C          2.07600       -1.24610       -0.30800
C          2.83980       -0.14530       -0.00530
C          2.22200        1.03970        0.29610
C          0.82970        1.13470        0.29610
H         -1.41280        1.14070       -1.75190
H         -3.04280        0.68350       -1.09380
H         -1.90820       -0.55420       -1.87050
H         -2.71490        0.70100        1.53270
H         -1.37710       -0.34310        2.07440
H         -2.71400       -1.06880        1.14470
H          0.11980       -2.07000       -0.56490
H          2.53560       -2.20320       -0.55620
H          3.93440       -0.18660        0.00150
H          2.83790        1.90810        0.54420
H          0.36650        2.10010        0.54290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

