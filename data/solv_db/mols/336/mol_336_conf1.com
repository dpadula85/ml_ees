%nproc=16
%mem=2GB
%chk=mol_336_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.96800       -0.66560       -1.09470
N         -1.30330       -0.09480        0.08320
C         -2.01520        0.25270        1.27730
C          0.10030        0.02730       -0.01070
C          0.63230        0.82630       -0.99800
C          1.99050        0.95870       -1.05380
C          2.80150        0.30380       -0.13470
C          2.23830       -0.48990        0.84200
C          0.86790       -0.64820        0.92190
H         -2.01680        0.13970       -1.85750
H         -1.31060       -1.46180       -1.51010
H         -2.93860       -1.10070       -0.84420
H         -2.31720       -0.68180        1.83700
H         -2.98420        0.75070        1.05040
H         -1.36050        0.83710        1.93180
H         -0.00140        1.34560       -1.71680
H          2.45370        1.54000       -1.81620
H          3.88420        0.41220       -0.19360
H          2.85950       -0.99140        1.58820
H          0.38770       -1.26000        1.69850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

