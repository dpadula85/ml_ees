%nproc=16
%mem=2GB
%chk=mol_336_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11550       -0.45970        1.07810
N         -1.34670        0.05450       -0.03390
C         -1.91850        0.63090       -1.22370
C          0.06560        0.00310        0.06040
C          0.69970       -1.15350       -0.32120
C          2.06900       -1.19990       -0.32230
C          2.86440       -0.13250        0.04550
C          2.20040        1.02030        0.43000
C          0.81710        1.08670        0.43570
H         -1.54300       -1.30970        1.55540
H         -2.27670        0.28160        1.88150
H         -3.07490       -0.90660        0.76890
H         -2.87970        1.16370       -0.97600
H         -1.24320        1.44640       -1.58850
H         -2.05720       -0.05900       -2.05640
H          0.12330       -2.04660       -0.61990
H          2.52700       -2.13380       -0.60710
H          3.96100       -0.18360        0.01570
H          2.80060        1.88880        0.73090
H          0.32740        2.00910        0.74700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

