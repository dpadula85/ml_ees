%nproc=16
%mem=2GB
%chk=mol_336_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.82980       -0.91840        1.18340
N         -1.31400       -0.19770        0.04060
C         -2.14230        0.35070       -1.00960
C          0.08760        0.01550       -0.05840
C          0.57370        1.20750        0.43360
C          1.93760        1.40100        0.41360
C          2.78250        0.41920       -0.08390
C          2.26010       -0.77370       -0.57220
C          0.90990       -0.98070       -0.56440
H         -1.02860       -1.62210        1.52620
H         -2.09750       -0.27270        2.03420
H         -2.67250       -1.60570        0.88170
H         -3.16930       -0.05670       -0.97780
H         -2.11740        1.46080       -0.90030
H         -1.62470        0.13360       -1.98190
H         -0.08580        1.97330        0.82380
H          2.35480        2.32380        0.77660
H          3.84770        0.56240       -0.09540
H          2.90470       -1.55900       -0.95190
H          0.42340       -1.86100       -0.91800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_336_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

