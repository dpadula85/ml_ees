%nproc=16
%mem=2GB
%chk=mol_184_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.01810        0.12350       -0.42950
O          1.39350        0.32560        0.77970
C          0.09480       -0.11390        0.81970
O         -0.71810        0.51990       -0.12600
C         -2.01370        0.05530       -0.05820
H          3.06580        0.52750       -0.31900
H          1.55890        0.69910       -1.24930
H          2.14450       -0.94030       -0.69570
H         -0.32090        0.17960        1.82110
H         -0.02940       -1.19920        0.71170
H         -2.08210       -1.03390       -0.24800
H         -2.63320        0.56450       -0.82290
H         -2.47820        0.29210        0.92610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

