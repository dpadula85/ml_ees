%nproc=16
%mem=2GB
%chk=mol_184_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.07980        0.11460        0.07350
O          1.24480        0.08000        1.16510
C         -0.04740       -0.27440        0.87090
O         -0.63330        0.63060       -0.04030
C         -1.95720        0.22860       -0.31340
H          2.15640       -0.87540       -0.44410
H          1.82350        0.91840       -0.64870
H          3.10210        0.34380        0.44020
H         -0.14210       -1.29430        0.44490
H         -0.66390       -0.24360        1.79120
H         -2.50630        0.23530        0.66530
H         -1.98590       -0.79370       -0.74350
H         -2.47060        0.93020       -0.96800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

