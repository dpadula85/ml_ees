%nproc=16
%mem=2GB
%chk=mol_184_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.20510       -0.19380        0.01080
O         -1.07180       -1.01500       -0.06750
C          0.03060       -0.17820        0.01220
O          1.23320       -0.89100       -0.05580
C          2.25250        0.07920        0.03660
H         -2.21490        0.37310        0.97750
H         -3.13610       -0.79880       -0.00010
H         -2.19100        0.58370       -0.78530
H         -0.04130        0.39250        0.97000
H         -0.06010        0.54600       -0.83080
H          3.25170       -0.35140       -0.00450
H          2.05530        0.84010       -0.76500
H          2.09690        0.61330        0.99970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

