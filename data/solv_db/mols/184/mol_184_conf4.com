%nproc=16
%mem=2GB
%chk=mol_184_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.27660       -0.09840        0.14580
O          1.16730       -0.38940       -0.63770
C         -0.02320        0.01190       -0.03420
O         -1.04740       -0.34500       -0.93620
C         -2.26040        0.05250       -0.34410
H          2.18080       -0.62680        1.11550
H          2.41390        0.97510        0.28600
H          3.16090       -0.50090       -0.40220
H         -0.23310       -0.58600        0.89150
H         -0.04530        1.09720        0.18830
H         -3.12340       -0.18080       -0.96810
H         -2.14760        1.12730       -0.10470
H         -2.31910       -0.53660        0.61630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

