%nproc=16
%mem=2GB
%chk=mol_184_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.23030       -0.03640       -0.33540
O          1.17160       -0.88060        0.02670
C          0.01180       -0.13710       -0.07390
O         -1.11240       -0.91200        0.27440
C         -2.22430       -0.05540        0.13510
H          2.09870        0.32890       -1.37190
H          2.31270        0.78890        0.40860
H          3.13850       -0.67510       -0.34710
H          0.07140        0.64840        0.73400
H         -0.14630        0.30970       -1.06030
H         -2.12620        0.83330        0.81440
H         -3.15910       -0.56450        0.44390
H         -2.26670        0.35200       -0.89580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_184_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

