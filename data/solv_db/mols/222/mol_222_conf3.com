%nproc=16
%mem=2GB
%chk=mol_222_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.53700       -0.13720       -0.53350
C          2.43430        0.12860        0.47510
C          1.12070       -0.07800       -0.24010
C         -0.06540        0.15860        0.66410
C         -1.31110       -0.08520       -0.18960
C         -2.54640        0.12750        0.62880
O         -2.51070        0.44870        1.80350
C         -3.84660       -0.07450       -0.06960
H          3.64150       -1.22980       -0.68880
H          3.17510        0.27550       -1.50810
H          4.48650        0.35290       -0.24400
H          2.51010       -0.56810        1.32280
H          2.56130        1.16980        0.82350
H          1.07080       -1.09220       -0.63270
H          1.06880        0.68790       -1.04390
H         -0.10030        1.19950        1.04310
H         -0.07710       -0.51280        1.54520
H         -1.27590        0.60190       -1.05630
H         -1.34370       -1.11110       -0.56660
H         -3.72450       -0.80830       -0.89260
H         -4.15670        0.93040       -0.44590
H         -4.64780       -0.38430        0.64100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

