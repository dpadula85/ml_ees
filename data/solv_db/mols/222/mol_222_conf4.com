%nproc=16
%mem=2GB
%chk=mol_222_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.96250       -0.79260        0.15790
C         -2.39170        0.54290        0.57840
C         -1.11390        0.88440       -0.13530
C         -0.01010       -0.10430        0.09080
C          1.19610        0.36230       -0.67690
C          2.36500       -0.52350       -0.54330
O          2.36160       -1.56250       -1.18040
C          3.55410       -0.25460        0.30760
H         -3.83210       -0.99200        0.82260
H         -3.30500       -0.71240       -0.88750
H         -2.21770       -1.57760        0.27230
H         -2.31710        0.65990        1.67480
H         -3.14370        1.31630        0.24380
H         -0.77240        1.87100        0.18390
H         -1.35310        0.95140       -1.21790
H          0.17360       -0.29260        1.14400
H         -0.34520       -1.08030       -0.36420
H          0.93470        0.43350       -1.75450
H          1.51390        1.37710       -0.32830
H          4.32140        0.34770       -0.22920
H          3.29230        0.33150        1.21300
H          4.05210       -1.18570        0.62850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

