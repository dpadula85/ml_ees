%nproc=16
%mem=2GB
%chk=mol_222_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.57690        0.51910       -0.21260
C          2.36820       -0.37480       -0.24040
C          1.13830        0.52180       -0.05020
C         -0.06100       -0.39130       -0.08090
C         -1.35820        0.34960        0.09500
C         -2.45800       -0.65350        0.04680
O         -2.24630       -1.84540       -0.11160
C         -3.89390       -0.26020        0.19000
H          3.68850        0.95330       -1.24500
H          3.34460        1.37790        0.45410
H          4.50320       -0.02160        0.09800
H          2.33390       -0.87730       -1.21690
H          2.33770       -1.11860        0.56090
H          1.04120        1.25310       -0.86510
H          1.19670        1.05950        0.89640
H         -0.11790       -0.94000       -1.03540
H          0.01850       -1.14530        0.73000
H         -1.30070        0.85480        1.09750
H         -1.46400        1.12940       -0.68890
H         -4.57360       -0.97920       -0.26410
H         -4.07730       -0.12710        1.27660
H         -3.99690        0.71590       -0.36760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

