%nproc=16
%mem=2GB
%chk=mol_222_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.50050        0.36170       -0.50940
C          2.26190        0.19260        0.33840
C          1.03790       -0.08000       -0.47010
C         -0.17350       -0.23810        0.45530
C         -1.42380       -0.51670       -0.37950
C         -2.54740       -0.65670        0.57200
O         -2.69050       -1.68970        1.17240
C         -3.48100        0.47470        0.77360
H          3.88690       -0.63900       -0.79390
H          4.31440        0.84150        0.10030
H          3.28040        0.99600       -1.40370
H          2.07350        1.07360        0.97840
H          2.39610       -0.69350        1.01730
H          0.86230        0.68320       -1.24210
H          1.14930       -1.08780       -0.96800
H         -0.36770        0.73220        0.96200
H         -0.04300       -1.08090        1.13210
H         -1.28990       -1.41920       -1.00980
H         -1.58320        0.40130       -0.97460
H         -3.28940        0.98100        1.75460
H         -4.52500        0.09550        0.72090
H         -3.34860        1.26820       -0.01130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

