%nproc=16
%mem=2GB
%chk=mol_222_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.01050        0.68260       -0.41340
C          2.51070       -0.58590        0.21720
C          1.11930       -0.94850       -0.21810
C          0.09470        0.09170        0.11650
C         -1.24870       -0.41500       -0.38240
C         -2.26670        0.62680       -0.04590
O         -1.97430        1.66560        0.52770
C         -3.70490        0.43780       -0.40220
H          4.11550        0.60740       -0.48060
H          2.57340        0.75590       -1.42860
H          2.72030        1.56140        0.21750
H          2.61090       -0.50560        1.30370
H          3.17940       -1.41230       -0.12470
H          1.10440       -1.13280       -1.30600
H          0.83920       -1.87350        0.35110
H          0.27570        1.06150       -0.35900
H         -0.01820        0.22580        1.21470
H         -1.48800       -1.36440        0.14070
H         -1.26290       -0.59400       -1.46440
H         -4.30270        0.53280        0.51860
H         -4.03440        1.18300       -1.16170
H         -3.85300       -0.60030       -0.79860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_222_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

