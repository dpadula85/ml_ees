%nproc=16
%mem=2GB
%chk=mol_317_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05710       -0.27490        0.15070
C          0.58800       -0.32930        0.37180
C         -0.01340        1.00760       -0.08020
C         -1.48500        0.61550       -0.15150
C         -1.39070       -0.87510       -0.33590
O         -0.06250       -1.19920       -0.50540
H          2.25380        0.17140       -0.83610
H          2.47690       -1.27880        0.27080
H          2.50840        0.37320        0.91110
H          0.31110       -0.48560        1.42260
H          0.30290        1.27730       -1.09630
H          0.13470        1.77500        0.67940
H         -2.00960        1.07930       -0.99000
H         -1.95920        0.79080        0.83740
H         -1.74810       -1.43760        0.56550
H         -1.96440       -1.20960       -1.21400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

