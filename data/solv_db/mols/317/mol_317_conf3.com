%nproc=16
%mem=2GB
%chk=mol_317_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09050        0.03360       -0.03130
C          0.63850       -0.32570        0.03620
C         -0.20880        0.75930        0.67280
C         -1.58880        0.40650        0.10090
C         -1.27430       -0.73980       -0.81210
O          0.04510       -0.43310       -1.22070
H          2.48110        0.02890        1.00200
H          2.19630        1.03520       -0.47160
H          2.60510       -0.73160       -0.62650
H          0.45870       -1.28490        0.55440
H          0.11380        1.76180        0.34170
H         -0.16020        0.62600        1.75300
H         -2.01380        1.27960       -0.41380
H         -2.26930        0.10860        0.92650
H         -1.16030       -1.65170       -0.16840
H         -1.95360       -0.87280       -1.64310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

