%nproc=16
%mem=2GB
%chk=mol_317_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.75730        0.30360       -0.49090
C          0.63670        0.51580        0.48720
C         -0.64940        0.94450       -0.16460
C         -1.39200       -0.36390       -0.29810
C         -0.68520       -1.28360        0.63270
O          0.39600       -0.64080        1.17470
H          2.71070        0.36840        0.09090
H          1.68280       -0.66060       -1.04200
H          1.76330        1.17880       -1.18360
H          0.95240        1.30990        1.19990
H         -1.17810        1.65970        0.51250
H         -0.51600        1.46770       -1.12120
H         -2.43970       -0.24160        0.05880
H         -1.38230       -0.72880       -1.35790
H         -0.34040       -2.16920        0.03680
H         -1.31630       -1.65990        1.46500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

