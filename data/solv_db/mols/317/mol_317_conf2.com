%nproc=16
%mem=2GB
%chk=mol_317_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.75700       -0.11080        0.64970
C          0.79510        0.14260       -0.49760
C         -0.23140       -0.93580       -0.60340
C         -1.47980       -0.29000       -0.08160
C         -1.08340        1.12550        0.20220
O          0.15140        1.34770       -0.35790
H          2.53650        0.68800        0.58170
H          2.25490       -1.07970        0.51370
H          1.19400       -0.02100        1.58810
H          1.35930        0.18420       -1.46660
H          0.07580       -1.81420        0.00960
H         -0.38530       -1.24600       -1.64860
H         -2.29990       -0.39740       -0.80660
H         -1.80140       -0.78880        0.85250
H         -1.80960        1.85800       -0.23620
H         -1.03310        1.33770        1.30070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

