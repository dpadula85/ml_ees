%nproc=16
%mem=2GB
%chk=mol_317_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.91190       -0.85030       -0.18450
C          0.59760       -0.37740        0.27900
C          0.14840        0.92690       -0.39550
C         -1.25240        0.97420        0.23330
C         -1.62710       -0.48520        0.08150
O         -0.46320       -1.20460       -0.03340
H          2.55410        0.03070       -0.43860
H          1.86990       -1.51320       -1.05310
H          2.43660       -1.34700        0.67030
H          0.53840       -0.17100        1.37050
H          0.76100        1.77550       -0.11540
H         -0.00190        0.75780       -1.46090
H         -1.07820        1.20400        1.30540
H         -1.91490        1.65940       -0.28250
H         -2.22220       -0.54310       -0.87620
H         -2.25800       -0.83680        0.89990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_317_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

