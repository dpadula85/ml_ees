%nproc=16
%mem=2GB
%chk=mol_500_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07650       -0.01600        0.62940
C         -0.04040        0.70840       -0.23780
C          0.99570       -0.37070       -0.59200
Br         1.76720       -1.05410        1.04340
H         -0.50900       -0.65090        1.35580
H         -1.75530        0.68500        1.12200
H         -1.59070       -0.71720       -0.07110
H         -0.52710        1.05440       -1.17300
H          0.44570        1.53500        0.31870
H          1.77840        0.03000       -1.25550
H          0.51190       -1.20390       -1.13990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

