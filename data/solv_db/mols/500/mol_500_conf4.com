%nproc=16
%mem=2GB
%chk=mol_500_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.13940        0.50100        0.06000
C         -0.15020       -0.60300        0.34920
C          1.16190       -0.21500       -0.33550
Br         1.82510        1.45060        0.33780
H         -0.58940        1.43950       -0.11900
H         -1.63190        0.24500       -0.90660
H         -1.87440        0.64130        0.87690
H         -0.50450       -1.57440       -0.04980
H         -0.00670       -0.72220        1.42720
H          1.88750       -1.05560       -0.20960
H          1.02190       -0.10730       -1.43050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

