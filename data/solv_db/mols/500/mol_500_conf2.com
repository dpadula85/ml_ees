%nproc=16
%mem=2GB
%chk=mol_500_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.15670       -0.34450       -0.26560
C          0.14520        0.54420        0.43080
C         -1.22400        0.21400       -0.12490
Br        -1.58920       -1.66730        0.23390
H          1.18060       -0.19820       -1.35440
H          0.82800       -1.40280       -0.05360
H          2.17520       -0.21690        0.15790
H          0.12890        0.21900        1.50400
H          0.39250        1.60080        0.37900
H         -2.01620        0.84870        0.31500
H         -1.17780        0.40310       -1.22200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

