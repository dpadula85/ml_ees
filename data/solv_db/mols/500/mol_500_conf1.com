%nproc=16
%mem=2GB
%chk=mol_500_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06420       -0.07600        0.62850
C         -0.15390        0.61080       -0.39640
C          1.04570       -0.31870       -0.56160
Br         1.89150       -0.48790        1.18980
H         -1.98280        0.50470        0.80940
H         -1.24210       -1.09000        0.24340
H         -0.46470       -0.16460        1.55600
H         -0.67040        0.75580       -1.36500
H          0.17280        1.57150        0.05070
H          0.72570       -1.34040       -0.83230
H          1.74240        0.03490       -1.32240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

