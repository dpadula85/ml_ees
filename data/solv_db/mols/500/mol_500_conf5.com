%nproc=16
%mem=2GB
%chk=mol_500_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.28960       -0.26900       -0.11520
C         -0.04260        0.57800       -0.28110
C          1.10950       -0.23840        0.29820
Br         2.78410        0.72700        0.15420
H         -1.05580       -1.24770       -0.62160
H         -2.18440        0.19790       -0.57230
H         -1.40590       -0.44950        0.96950
H         -0.16510        1.49070        0.33610
H          0.14840        0.88260       -1.30890
H          0.89550       -0.45750        1.35690
H          1.20590       -1.21430       -0.21570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_500_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

