%nproc=16
%mem=2GB
%chk=mol_413_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.00570       -0.00620        0.00040
Cl         1.33930       -1.16580       -0.06640
Cl         0.66970        1.63320       -0.16240
Cl        -1.14710       -0.32280       -1.32420
Cl        -0.85620       -0.13840        1.55260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_413_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_413_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_413_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

