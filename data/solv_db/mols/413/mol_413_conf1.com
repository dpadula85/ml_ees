%nproc=16
%mem=2GB
%chk=mol_413_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00920       -0.00320        0.01250
Cl        -1.34250        0.53080        1.02780
Cl        -0.55500       -1.33080       -1.02460
Cl         1.37510       -0.56930        0.98690
Cl         0.51320        1.37250       -1.00260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_413_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_413_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_413_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

