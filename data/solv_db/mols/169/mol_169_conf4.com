%nproc=16
%mem=2GB
%chk=mol_169_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06260        1.14590       -0.13410
C         -0.49360       -0.14460        0.37940
C         -1.50770       -1.25570        0.10100
C          0.79250       -0.56730       -0.29910
C          1.86470        0.42600       -0.11700
O          2.20810        1.15790       -1.07140
O          2.46560        0.55850        1.11520
H         -2.12240        1.03600       -0.50180
H         -1.13750        1.89900        0.70660
H         -0.48780        1.57450       -0.97900
H         -0.26850       -0.07950        1.47310
H         -1.17210       -2.21050        0.55310
H         -2.46360       -0.91180        0.50680
H         -1.62550       -1.30270       -0.99740
H          0.56170       -0.70900       -1.38230
H          1.07580       -1.54200        0.09240
H          3.37300        0.92500        1.29030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

