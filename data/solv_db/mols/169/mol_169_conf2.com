%nproc=16
%mem=2GB
%chk=mol_169_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.91030       -1.28690        0.23370
C         -0.76570        0.04810       -0.48660
C         -1.20630        1.12190        0.45690
C          0.64960        0.25120       -0.98390
C          1.64440        0.24500        0.08800
O          1.75330        1.27410        0.79870
O          2.45970       -0.84570        0.34090
H         -0.48600       -2.12070       -0.36810
H         -0.47530       -1.25620        1.25150
H         -2.00140       -1.46680        0.32560
H         -1.40620       -0.02650       -1.39080
H         -2.29220        1.30310        0.26640
H         -0.66290        2.08300        0.32540
H         -1.08320        0.79260        1.52950
H          0.63670        1.20140       -1.56910
H          0.85190       -0.55070       -1.73190
H          3.29400       -0.76700        0.91380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

