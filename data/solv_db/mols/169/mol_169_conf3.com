%nproc=16
%mem=2GB
%chk=mol_169_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.52120        1.46190       -0.16510
C         -0.74940        0.08530        0.39130
C         -1.27350       -0.83100       -0.66710
C          0.44130       -0.45600        1.12180
C          1.64570       -0.57180        0.29530
O          2.68600       -1.02010        0.84200
O          1.74230       -0.22890       -1.04550
H         -0.75720        1.45800       -1.26100
H         -1.20360        2.22990        0.27400
H          0.53020        1.79750       -0.06310
H         -1.56830        0.19020        1.16230
H         -2.36920       -1.04020       -0.54900
H         -1.17670       -0.34950       -1.67190
H         -0.74140       -1.80590       -0.72090
H          0.65210        0.18420        1.98980
H          0.18010       -1.47510        1.48130
H          2.48250        0.37170       -1.41400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

