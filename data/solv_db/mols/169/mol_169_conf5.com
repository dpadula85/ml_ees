%nproc=16
%mem=2GB
%chk=mol_169_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.92520        1.31310       -0.06280
C         -0.54050       -0.08000       -0.50850
C         -1.63750       -1.01560        0.01730
C          0.74760       -0.54540        0.10600
C          1.89500        0.31010       -0.22480
O          1.84820        1.33660       -0.94500
O          3.13450       -0.03430        0.29500
H         -0.59780        1.42560        1.00870
H         -2.05090        1.36710       -0.07200
H         -0.48750        2.08930       -0.71090
H         -0.53150       -0.08880       -1.60730
H         -1.36270       -1.24430        1.06830
H         -1.67860       -1.95280       -0.58260
H         -2.61110       -0.47860        0.02310
H          0.63610       -0.55840        1.20020
H          0.94830       -1.56200       -0.29380
H          3.21350       -0.28160        1.28920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

