%nproc=16
%mem=2GB
%chk=mol_169_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69520       -0.96180        0.00250
C         -0.58880       -0.00860       -0.46970
C         -0.76840        1.20280        0.39910
C          0.70420       -0.74280       -0.23110
C          1.89310        0.02910       -0.64720
O          2.50310       -0.13920       -1.73230
O          2.37650        1.00300        0.20630
H         -2.61980       -0.36140        0.00830
H         -1.75750       -1.84610       -0.65630
H         -1.42140       -1.20590        1.06070
H         -0.66650        0.27830       -1.50740
H         -1.68320        1.73060        0.01910
H          0.08670        1.86560        0.42700
H         -1.08380        0.87550        1.43460
H          0.64970       -1.66650       -0.89010
H          0.78010       -1.09320        0.82290
H          3.29110        1.04060        0.58050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_169_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

