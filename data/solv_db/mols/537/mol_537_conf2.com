%nproc=16
%mem=2GB
%chk=mol_537_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.95350        0.17460        0.29030
C          0.50230       -0.07370        0.30820
C          1.69570       -0.26510        0.32080
H         -1.22220        1.05260       -0.31110
H         -1.36970        0.27050        1.30970
H         -1.39370       -0.70860       -0.23240
H          2.74090       -0.45040        0.33080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_537_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_537_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_537_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

