%nproc=16
%mem=2GB
%chk=mol_537_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.95640        0.07780       -0.03110
C          0.50220       -0.05840       -0.04020
C          1.71150       -0.15630       -0.03440
H         -1.45170       -0.89740        0.23950
H         -1.20940        0.88440        0.68280
H         -1.36570        0.37350       -1.00560
H          2.76960       -0.22370       -0.02880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_537_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_537_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_537_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

