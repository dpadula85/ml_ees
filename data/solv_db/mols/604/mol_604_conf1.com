%nproc=16
%mem=2GB
%chk=mol_604_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01710       -0.19840       -0.00720
Br        -1.52520        0.98450       -0.01330
Br         1.61170        0.88130       -0.00410
H         -0.05860       -0.80650        0.91330
H         -0.04500       -0.86090       -0.88870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_604_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_604_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_604_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

