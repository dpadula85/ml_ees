%nproc=16
%mem=2GB
%chk=mol_604_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00700       -0.18740        0.00300
Br        -1.55420        0.94820       -0.03620
Br         1.62890        0.86470       -0.04570
H         -0.05240       -0.86990       -0.88100
H         -0.02930       -0.75550        0.95990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_604_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_604_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_604_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

