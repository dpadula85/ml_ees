%nproc=16
%mem=2GB
%chk=mol_641_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31130       -0.35370       -0.03320
C          0.03570        0.24240       -0.32870
C          1.14330       -0.42720        0.49170
F          0.91280       -0.28360        1.84080
F          1.33860       -1.71530        0.09580
F          2.31020        0.27730        0.16440
O          0.00590        1.60220       -0.02530
H         -1.20610       -1.43130        0.18820
H         -1.78780        0.21260        0.80460
H         -1.96450       -0.28940       -0.94700
H          0.25680        0.05790       -1.39410
H          0.26640        2.10810       -0.85730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

