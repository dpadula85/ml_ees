%nproc=16
%mem=2GB
%chk=mol_641_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.19120       -0.61010        0.23290
C         -0.16270        0.23870       -0.47220
C          1.18790       -0.11410        0.09040
F          1.17290        0.15320        1.44760
F          1.37800       -1.46940       -0.11420
F          2.19810        0.57630       -0.51640
O         -0.45810        1.59200       -0.29660
H         -0.73080       -1.33130        0.95050
H         -1.86230        0.03200        0.80650
H         -1.72830       -1.23040       -0.52870
H         -0.15750        0.04920       -1.55800
H          0.35400        2.11370       -0.04180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

