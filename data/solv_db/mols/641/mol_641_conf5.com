%nproc=16
%mem=2GB
%chk=mol_641_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21030       -0.37840        0.36580
C         -0.05370        0.34700       -0.30260
C          1.20650       -0.46320       -0.10120
F          1.08340       -1.70960       -0.65060
F          2.24330        0.22600       -0.69270
F          1.49600       -0.62600        1.23870
O          0.09560        1.61340        0.22670
H         -2.12550        0.23520        0.37110
H         -1.33280       -1.38250       -0.04560
H         -0.91430       -0.50200        1.43370
H         -0.24850        0.37560       -1.39100
H         -0.23980        2.26460       -0.45220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

