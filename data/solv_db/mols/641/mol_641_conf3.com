%nproc=16
%mem=2GB
%chk=mol_641_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.11260       -0.56160       -0.40160
C         -0.08200        0.38070        0.15150
C          1.32250       -0.00100       -0.29740
F          1.40240        0.03310       -1.65590
F          1.59030       -1.24500        0.19960
F          2.20550        0.92310        0.22110
O         -0.14130        0.35720        1.55570
H         -0.78700       -1.02500       -1.33920
H         -1.27870       -1.35650        0.34470
H         -2.05970        0.01130       -0.50040
H         -0.27550        1.42320       -0.15260
H         -0.78380        1.06030        1.87440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

