%nproc=16
%mem=2GB
%chk=mol_641_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.29520       -0.17320       -0.48170
C          0.13280        0.37890        0.29640
C         -1.03420       -0.59050        0.31140
F         -0.63050       -1.77510        0.89890
F         -2.09810       -0.02440        0.95670
F         -1.38750       -0.88210       -0.98550
O         -0.25670        1.57010       -0.29620
H          2.05610        0.63000       -0.57660
H          1.73990       -1.01120        0.10590
H          0.95170       -0.54350       -1.46870
H          0.41120        0.58660        1.33830
H         -1.18000        1.83440       -0.09900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_641_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

