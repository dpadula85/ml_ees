%nproc=16
%mem=2GB
%chk=mol_411_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.57930        0.05970        0.11700
C         -1.39120       -0.17940       -0.74350
O         -1.57580       -0.47680       -1.95230
C         -0.05340       -0.06410       -0.16890
C          1.09950       -0.26950       -0.89700
C          2.35670       -0.15880       -0.34750
C          2.47370        0.17130        0.98730
N          1.35430        0.37370        1.70560
C          0.10760        0.26410        1.16170
H         -2.45630        0.98260        0.73840
H         -2.81680       -0.82220        0.76210
H         -3.47230        0.27940       -0.53380
H          1.00560       -0.52760       -1.94130
H          3.25820       -0.32610       -0.94260
H          3.46930        0.26100        1.43080
H         -0.77990        0.43270        1.76390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

