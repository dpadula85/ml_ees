%nproc=16
%mem=2GB
%chk=mol_411_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.37500        0.00010       -0.82040
C         -1.56620       -0.14330        0.41780
O         -2.17040       -0.33020        1.49170
C         -0.10030       -0.06670        0.37430
C          0.55320        0.13960       -0.81010
C          1.94350        0.21100       -0.85500
C          2.59760        0.06250        0.35010
N          1.94280       -0.13980        1.50780
C          0.60100       -0.20900        1.55350
H         -2.46670        1.07980       -1.05350
H         -1.86670       -0.48820       -1.70250
H         -3.35920       -0.48470       -0.67690
H          0.03080        0.25480       -1.74260
H          2.46930        0.37360       -1.78470
H          3.67850        0.11550        0.33220
H          0.08780       -0.37510        2.51130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

