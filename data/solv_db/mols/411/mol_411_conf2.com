%nproc=16
%mem=2GB
%chk=mol_411_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.41740        0.28600        0.45180
C          1.54530       -0.26410       -0.60520
O          2.05930       -0.76780       -1.63030
C          0.10280       -0.20740       -0.42970
C         -0.46360        0.34790        0.70020
C         -1.83760        0.40520        0.87540
C         -2.68300       -0.10020       -0.09250
N         -2.10860       -0.63630       -1.18470
C         -0.77810       -0.70180       -1.37420
H          2.38300        1.40320        0.48880
H          3.44570       -0.10430        0.32320
H          2.09830       -0.04700        1.48280
H          0.21960        0.74720        1.46280
H         -2.27470        0.84400        1.76910
H         -3.77740       -0.06610        0.02460
H         -0.34830       -1.13840       -2.26210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

