%nproc=16
%mem=2GB
%chk=mol_411_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.42040        0.37920       -0.47750
C          1.53640       -0.38030        0.45810
O          2.06840       -1.11710        1.32540
C          0.08180       -0.28190        0.38260
C         -0.53570        0.52830       -0.56890
C         -1.90860        0.59160       -0.60770
C         -2.65620       -0.14150        0.28850
N         -2.01920       -0.91550        1.19630
C         -0.67970       -1.01020        1.27340
H          2.44990        1.46640       -0.17780
H          1.99080        0.35850       -1.52210
H          3.42120       -0.07710       -0.46860
H          0.10490        1.07940       -1.24290
H         -2.37350        1.22190       -1.34830
H         -3.73450       -0.06970        0.23170
H         -0.16640       -1.63180        2.00250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

