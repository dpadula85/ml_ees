%nproc=16
%mem=2GB
%chk=mol_411_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35410       -0.47490        0.55160
C         -1.49380        0.65470        0.11560
O         -1.98810        1.81460       -0.07530
C         -0.08170        0.43490       -0.09400
C          0.43880       -0.82680        0.11670
C          1.80670       -0.97880       -0.10090
C          2.60040        0.07330       -0.50530
N          2.03880        1.28700       -0.69670
C          0.72330        1.49390       -0.50220
H         -3.28490       -0.03070        0.97920
H         -1.87460       -1.06960        1.36740
H         -2.55120       -1.11290       -0.33050
H         -0.16340       -1.67300        0.43510
H          2.23870       -1.95950        0.05760
H          3.67680       -0.09600       -0.66430
H          0.26800        2.46390       -0.65400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_411_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

