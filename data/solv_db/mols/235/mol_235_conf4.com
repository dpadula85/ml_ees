%nproc=16
%mem=2GB
%chk=mol_235_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.84710        0.03710        0.05950
C         -0.64610       -0.03140       -0.14030
Cl        -1.11910       -1.35500       -1.21360
Cl        -1.33420        1.49790       -0.68920
Cl        -1.34600       -0.37360        1.47860
H          1.41110        0.11110       -0.86580
H          1.14470       -0.83830        0.70030
H          1.04250        0.95220        0.67050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

