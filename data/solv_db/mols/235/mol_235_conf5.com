%nproc=16
%mem=2GB
%chk=mol_235_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.81510       -0.01830        0.07470
C          0.68470       -0.01670       -0.03550
Cl         1.43070        0.03760        1.58650
Cl         1.19370       -1.48910       -0.88900
Cl         1.14490        1.44970       -0.95120
H         -1.19990        0.96640        0.43630
H         -1.21720       -0.79270        0.73500
H         -1.22190       -0.13680       -0.95680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

