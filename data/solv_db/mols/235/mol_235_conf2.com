%nproc=16
%mem=2GB
%chk=mol_235_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.83250        0.00630        0.08690
C         -0.65540       -0.07170       -0.12640
Cl        -1.36740       -0.67870        1.40040
Cl        -1.23140        1.61270       -0.33210
Cl        -1.13080       -1.09780       -1.45940
H          1.23490        0.72420       -0.65330
H          1.00230        0.47690        1.09310
H          1.31530       -0.97190       -0.00910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

