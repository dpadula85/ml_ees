%nproc=16
%mem=2GB
%chk=mol_235_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.81400       -0.05560        0.08660
C          0.68810        0.00090       -0.04830
Cl         1.16930       -1.17300       -1.28120
Cl         1.08490        1.65760       -0.57440
Cl         1.50930       -0.37210        1.45430
H         -1.17200       -0.87760        0.71820
H         -1.18790        0.89170        0.56470
H         -1.27760       -0.07180       -0.91980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

