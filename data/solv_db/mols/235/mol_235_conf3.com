%nproc=16
%mem=2GB
%chk=mol_235_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.82260       -0.10770       -0.12720
C          0.64930        0.06110        0.10540
Cl         1.58470       -1.37610       -0.36630
Cl         0.87320        0.34780        1.85900
Cl         1.29280        1.47790       -0.79210
H         -0.99120       -0.97280       -0.77560
H         -1.17130        0.78980       -0.69550
H         -1.41500       -0.22010        0.79240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_235_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

