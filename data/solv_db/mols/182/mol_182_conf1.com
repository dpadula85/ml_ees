%nproc=16
%mem=2GB
%chk=mol_182_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.03140        0.90010       -0.01430
C         -1.32150       -0.44840        0.02870
C         -0.27920       -1.35030        0.04290
C          1.03600       -0.91670        0.01470
C          1.32080        0.43760       -0.02850
C          0.27810        1.34310       -0.04270
H         -1.82600        1.65080       -0.02700
H         -2.34320       -0.79720        0.05100
H         -0.47930       -2.42190        0.07690
H          1.85720       -1.63820        0.02640
H          2.32870        0.83250       -0.05220
H          0.45990        2.40870       -0.07590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_182_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_182_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_182_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

