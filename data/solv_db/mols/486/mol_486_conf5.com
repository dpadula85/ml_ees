%nproc=16
%mem=2GB
%chk=mol_486_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.71050        0.06830       -0.30000
C          1.58080       -0.74070        0.18010
O          1.87100       -1.88430        0.67260
C          0.18620       -0.29760        0.11600
C         -0.17030        0.92540       -0.40310
C         -1.50380        1.30840       -0.45320
C         -2.50820        0.48480        0.01130
C         -2.15580       -0.74360        0.53360
C         -0.80970       -1.12440        0.58140
H          3.09000        0.68620        0.56360
H          3.52000       -0.59110       -0.61820
H          2.43670        0.77060       -1.13130
H          0.60660        1.56910       -0.76020
H         -1.78040        2.26620       -0.86030
H         -3.56310        0.77990       -0.02460
H         -2.93380       -1.39130        0.89800
H         -0.57670       -2.08590        0.99420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

