%nproc=16
%mem=2GB
%chk=mol_486_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.69960       -0.03380        0.13130
C          1.53060        0.87250        0.23100
O          1.72770        2.09310        0.45520
C          0.16490        0.36940        0.07290
C         -0.10190       -0.96120       -0.17320
C         -1.38850       -1.43700       -0.32430
C         -2.47420       -0.59190       -0.23430
C         -2.21730        0.74060        0.01180
C         -0.92460        1.21030        0.16210
H          3.63490        0.53110       -0.10470
H          2.56970       -0.82280       -0.63870
H          2.90970       -0.56990        1.08450
H          0.74990       -1.65190       -0.24710
H         -1.59970       -2.47850       -0.51720
H         -3.48960       -0.96500       -0.35310
H         -3.06900        1.42470        0.08610
H         -0.72210        2.27040        0.35770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

