%nproc=16
%mem=2GB
%chk=mol_486_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.52430        0.84410        0.03580
C          1.70530       -0.35370        0.32510
O          2.27530       -1.39130        0.73720
C          0.26530       -0.30050        0.12600
C         -0.35040        0.84790       -0.32820
C         -1.71550        0.89890       -0.51700
C         -2.52300       -0.19060       -0.26160
C         -1.90540       -1.33800        0.19240
C         -0.54120       -1.39090        0.38180
H          2.71940        0.98610       -1.04590
H          3.53520        0.77870        0.52410
H          2.04420        1.73560        0.50390
H          0.28520        1.71640       -0.53280
H         -2.17200        1.81600       -0.87530
H         -3.60430       -0.13790       -0.41520
H         -2.50280       -2.22320        0.40730
H         -0.03950       -2.29780        0.74250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

