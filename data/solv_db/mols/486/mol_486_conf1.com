%nproc=16
%mem=2GB
%chk=mol_486_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.64590       -0.55560       -0.02840
C          1.67140        0.52310       -0.31950
O          2.08190        1.62420       -0.73270
C          0.25440        0.29810       -0.12480
C         -0.64520        1.30700       -0.39800
C         -2.00740        1.14340       -0.22930
C         -2.52650       -0.05240        0.22520
C         -1.63500       -1.07140        0.50240
C         -0.26830       -0.89790        0.33000
H          3.65360       -0.33460       -0.37840
H          2.66680       -0.77870        1.07510
H          2.22160       -1.48660       -0.48730
H         -0.23990        2.24870       -0.75530
H         -2.69540        1.94060       -0.44710
H         -3.59260       -0.19330        0.36150
H         -2.02630       -2.01460        0.85930
H          0.44090       -1.70000        0.54740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

