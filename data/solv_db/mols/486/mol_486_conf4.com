%nproc=16
%mem=2GB
%chk=mol_486_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.67690        0.21410       -0.18720
C         -1.57380       -0.77410       -0.25790
O         -1.85400       -1.97890       -0.47730
C         -0.18960       -0.33640       -0.07300
C          0.14560        0.97300        0.16810
C          1.46700        1.34250        0.33670
C          2.50140        0.43100        0.27270
C          2.15530       -0.88280        0.03000
C          0.83590       -1.25960       -0.13960
H         -3.64580       -0.27270        0.04940
H         -2.83630        0.73390       -1.16470
H         -2.51530        0.95470        0.64580
H         -0.64280        1.69890        0.22170
H          1.76110        2.37550        0.52940
H          3.54130        0.70940        0.40330
H          2.93830       -1.63480       -0.02870
H          0.58860       -2.29370       -0.32860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_486_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

