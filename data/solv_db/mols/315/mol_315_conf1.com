%nproc=16
%mem=2GB
%chk=mol_315_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.39550        1.11820       -0.09580
C         -0.93960        1.47640       -0.12700
N         -1.85710        0.49700       -0.04440
C         -1.49410       -0.78870        0.06490
C         -0.17270       -1.18080        0.09900
C          0.79280       -0.20020        0.01630
C          2.16700       -0.58190        0.04970
N          3.28690       -0.88680        0.09000
H          1.17900        1.86020       -0.15840
H         -1.22420        2.52990       -0.21670
H         -2.23210       -1.59920        0.13320
H          0.09860       -2.24410        0.18930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_315_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_315_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_315_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

