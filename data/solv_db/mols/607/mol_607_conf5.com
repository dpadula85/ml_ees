%nproc=16
%mem=2GB
%chk=mol_607_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.57930        1.19560       -0.43440
C          1.82360       -0.20290        0.03170
C          0.69890       -0.92360        0.63940
C         -0.52320       -1.20410       -0.11760
C         -1.38610       -0.11370       -0.60480
C         -1.91730        0.67850        0.59190
Br        -3.05200        2.12330        0.05310
H          0.93050        1.71310        0.27270
H          1.15310        1.23890       -1.43820
H          2.56720        1.76200       -0.48480
H          2.69930       -0.12390        0.76130
H          2.24490       -0.80460       -0.81380
H          0.50670       -0.45330        1.64390
H          1.11910       -1.94590        0.94300
H         -1.15530       -1.92020        0.51160
H         -0.23390       -1.87220       -0.99560
H         -1.03270        0.51560       -1.40970
H         -2.33530       -0.61320       -1.00510
H         -1.11870        1.00020        1.27740
H         -2.56820       -0.04970        1.16160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

