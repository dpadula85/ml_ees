%nproc=16
%mem=2GB
%chk=mol_607_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.52230        0.48140       -0.29360
C          1.83060       -0.82550       -0.06000
C          0.48480       -0.51510        0.56560
C         -0.32310        0.34530       -0.36020
C         -1.66870        0.72030        0.13990
C         -2.59070       -0.40980        0.42880
Br        -4.27750        0.31460        1.06550
H          2.44360        0.77670       -1.36200
H          3.61000        0.41590       -0.00170
H          2.11600        1.30620        0.33790
H          1.73210       -1.42880       -0.98660
H          2.40880       -1.41110        0.69900
H         -0.04400       -1.50420        0.64110
H          0.68190       -0.07940        1.53840
H         -0.39460       -0.10510       -1.37140
H          0.25580        1.29350       -0.50440
H         -1.58570        1.37050        1.05190
H         -2.14720        1.36760       -0.62580
H         -2.82830       -0.98150       -0.49330
H         -2.22630       -1.13150        1.16630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

