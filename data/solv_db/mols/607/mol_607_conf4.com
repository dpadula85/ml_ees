%nproc=16
%mem=2GB
%chk=mol_607_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.37510        0.65650        0.14490
C          1.73870       -0.53510        0.78070
C          0.24170       -0.52710        0.71560
C         -0.28730       -0.50040       -0.69360
C         -1.77200       -0.49230       -0.77470
C         -2.40190        0.70140       -0.09340
Br        -1.84330        2.36790       -0.83500
H          1.83040        1.59400        0.32280
H          2.55650        0.52580       -0.95250
H          3.39400        0.79930        0.58920
H          2.01640       -0.52390        1.86250
H          2.14900       -1.46510        0.31220
H         -0.16210       -1.40150        1.24890
H         -0.10090        0.38840        1.27140
H          0.07960       -1.41650       -1.19650
H          0.10390        0.35490       -1.26220
H         -2.26270       -1.43500       -0.46180
H         -2.03390       -0.38460       -1.86910
H         -3.50450        0.65770       -0.10640
H         -2.11690        0.63570        0.99710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

