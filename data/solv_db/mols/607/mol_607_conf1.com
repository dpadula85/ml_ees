%nproc=16
%mem=2GB
%chk=mol_607_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.56500        0.53120       -0.63320
C         -2.03820       -0.53980        0.24010
C         -0.60260       -0.90620       -0.00130
C          0.35610        0.21610        0.19250
C          1.74910       -0.30820       -0.08560
C          2.79320        0.77770        0.09010
Br         4.53400        0.02370       -0.28280
H         -2.08970        0.60800       -1.62940
H         -3.65140        0.32120       -0.83360
H         -2.58110        1.53040       -0.10470
H         -2.19370       -0.27130        1.30410
H         -2.63680       -1.47610        0.07560
H         -0.44050       -1.35290       -0.99510
H         -0.34300       -1.70150        0.74650
H          0.35660        0.64040        1.20930
H          0.17300        1.01790       -0.58090
H          1.94740       -1.17810        0.57030
H          1.84320       -0.68820       -1.13250
H          2.76600        1.18110        1.12710
H          2.62350        1.57440       -0.65790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

