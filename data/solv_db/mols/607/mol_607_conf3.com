%nproc=16
%mem=2GB
%chk=mol_607_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.95110        0.21530        0.01010
C         -1.48620        0.58410       -0.19250
C         -0.75420       -0.66730       -0.54470
C          0.71040       -0.48940       -0.78080
C          1.45340        0.05220        0.40270
C          2.92470        0.17020       -0.00990
Br         4.00570        0.85460        1.41260
H         -3.62240        0.96350       -0.46130
H         -3.18400       -0.75310       -0.51950
H         -3.20080        0.03940        1.06540
H         -1.13960        1.07270        0.72340
H         -1.45410        1.31820       -1.04690
H         -0.92160       -1.41840        0.22690
H         -1.19670       -1.06350       -1.48680
H          1.13600       -1.49340       -1.00020
H          0.95030        0.14430       -1.64960
H          1.09810        1.05840        0.70420
H          1.40740       -0.65410        1.27500
H          3.25040       -0.81060       -0.39330
H          2.97430        0.87690       -0.86010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_607_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

