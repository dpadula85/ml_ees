%nproc=16
%mem=2GB
%chk=mol_378_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.01040       -0.13010       -0.67590
C          1.06390        0.41570        0.33390
C         -0.36870       -0.00560        0.12310
C         -1.16120        0.65300        1.24970
C         -0.57460       -1.48230        0.22870
C         -0.94470        0.56650       -1.16250
H          1.67340       -1.07100       -1.14080
H          2.26400        0.57800       -1.49520
H          2.99740       -0.34420       -0.16990
H          1.33710        0.09690        1.36560
H          1.05310        1.54490        0.34040
H         -0.73270        1.68330        1.39410
H         -1.04900        0.08060        2.18420
H         -2.20950        0.78260        0.92580
H         -1.64560       -1.69180        0.13990
H         -0.04870       -2.07340       -0.52660
H         -0.24350       -1.81520        1.21670
H         -0.87690       -0.11210       -2.00960
H         -2.04580        0.76030       -1.00230
H         -0.49850        1.56390       -1.31940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

