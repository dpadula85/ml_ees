%nproc=16
%mem=2GB
%chk=mol_378_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04570        0.02810        0.62750
C          0.93300        0.77700       -0.01950
C         -0.35440        0.03170       -0.11860
C         -1.36050        0.96050       -0.80540
C         -0.26160       -1.20280       -0.97910
C         -0.94390       -0.36220        1.19960
H          2.44070       -0.79310       -0.04010
H          2.88480        0.75470        0.79010
H          1.77260       -0.46830        1.57350
H          1.24570        1.08670       -1.02330
H          0.75000        1.70090        0.56300
H         -0.81600        1.68050       -1.42800
H         -2.08090        0.33830       -1.39280
H         -1.94500        1.47630       -0.03290
H         -1.29270       -1.41520       -1.34730
H          0.41220       -1.08230       -1.84590
H          0.01640       -2.09120       -0.35590
H         -1.51220       -1.32740        1.10250
H         -1.71510        0.39430        1.52130
H         -0.21890       -0.48650        2.01110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

