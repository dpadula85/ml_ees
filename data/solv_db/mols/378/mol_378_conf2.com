%nproc=16
%mem=2GB
%chk=mol_378_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.01420        0.56090        0.24470
C          1.00950       -0.13340       -0.61880
C         -0.37100       -0.17980       -0.04290
C         -1.25300       -0.91630       -1.04740
C         -0.47720       -0.77910        1.30560
C         -0.91970        1.23980        0.02240
H          2.81590        0.94950       -0.41720
H          1.59190        1.43550        0.79630
H          2.52700       -0.15000        0.94190
H          1.40240       -1.15130       -0.82750
H          0.97800        0.39110       -1.59620
H         -2.32080       -0.81260       -0.72690
H         -1.00590       -1.97260       -1.13610
H         -1.11860       -0.38480       -2.01100
H          0.39210       -1.35590        1.63880
H         -0.68490        0.00060        2.09590
H         -1.36320       -1.46980        1.30800
H         -0.76780        1.75320       -0.96120
H         -2.02190        1.23060        0.17680
H         -0.42700        1.74460        0.85480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

