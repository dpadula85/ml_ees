%nproc=16
%mem=2GB
%chk=mol_378_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.87860       -0.79840       -0.59720
C          1.14920        0.37370       -0.01710
C         -0.33460        0.15440        0.11750
C         -0.67240       -1.00100        1.00010
C         -0.90490        1.41740        0.76610
C         -1.01060        0.01340       -1.23260
H          1.49750       -1.77790       -0.24330
H          2.95280       -0.71000       -0.25890
H          1.89490       -0.82690       -1.69420
H          1.28410        1.22890       -0.71960
H          1.55390        0.69960        0.95420
H         -1.09770       -1.81360        0.35160
H          0.19710       -1.38900        1.57110
H         -1.44620       -0.74630        1.76200
H         -1.54920        1.16120        1.64190
H         -1.55370        1.97910        0.06990
H         -0.10750        2.07460        1.15760
H         -2.10550       -0.10780       -1.01560
H         -0.69540       -0.87360       -1.78790
H         -0.93050        0.94220       -1.82570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

