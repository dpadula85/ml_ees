%nproc=16
%mem=2GB
%chk=mol_378_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04220       -0.48970        0.05920
C          1.02550        0.53460       -0.32630
C         -0.39510        0.11600       -0.02720
C         -0.63530       -0.15330        1.41980
C         -1.27630        1.32380       -0.38700
C         -0.81340       -1.08750       -0.82440
H          2.93460       -0.04510        0.58910
H          2.49360       -1.00580       -0.83400
H          1.66680       -1.28120        0.72660
H          1.10000        0.75150       -1.41290
H          1.26700        1.48480        0.19080
H          0.01230        0.47270        2.04690
H         -1.69690        0.11150        1.66540
H         -0.53770       -1.23420        1.60440
H         -1.21970        1.50850       -1.47580
H         -0.79960        2.17790        0.14100
H         -2.30600        1.12180       -0.04510
H          0.03290       -1.63230       -1.27920
H         -1.35460       -1.84820       -0.19780
H         -1.54020       -0.82580       -1.63370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_378_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

