%nproc=16
%mem=2GB
%chk=mol_236_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00690       -0.16000       -0.00000
Cl        -1.45760        0.85790       -0.04720
Cl         1.46270        0.84840       -0.04780
H         -0.00680       -0.85720       -0.86940
H         -0.00510       -0.68910        0.96450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_236_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_236_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_236_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

