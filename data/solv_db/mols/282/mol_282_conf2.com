%nproc=16
%mem=2GB
%chk=mol_282_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49070        0.75330        0.96990
C         -1.34930        0.55470       -0.01680
C         -0.42910       -0.55190        0.45510
C          0.57880       -1.00400       -0.52430
C          1.61770       -0.04240       -0.98190
C          2.63990        0.32620        0.08540
O          2.10120        0.91740        1.20240
O          2.36370       -0.64740       -2.06200
O          1.24200       -2.15100        0.00840
O         -1.29820       -1.68110        0.74310
O         -0.71510        1.71100       -0.31760
O         -3.44170        1.62140        0.44840
H         -2.09930        1.18570        1.91000
H         -2.97530       -0.20940        1.20890
H         -1.84220        0.19340       -0.96970
H          0.01430       -0.28820        1.41640
H          0.06380       -1.40250       -1.44340
H          1.18050        0.83830       -1.44290
H          3.34330        1.08140       -0.36420
H          3.29000       -0.54600        0.32370
H          2.84450        1.11140        1.82390
H          1.68120       -0.70020       -2.79550
H          0.75450       -2.97490       -0.23750
H         -1.89570       -1.79340       -0.03860
H         -0.95110        2.07690       -1.20640
H         -4.22790        1.62130        1.03880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

