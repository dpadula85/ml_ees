%nproc=16
%mem=2GB
%chk=mol_282_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.08180       -0.39330        0.16400
C         -1.74090        0.29810        0.36610
C         -0.60450       -0.55700       -0.20670
C          0.65980        0.23700       -0.02150
C          1.87670       -0.43960       -0.57800
C          3.05440        0.46480       -0.24490
O          3.10260        0.54680        1.15190
O          2.13210       -1.67080        0.01070
O          0.50490        1.45230       -0.68210
O         -0.89380       -0.81430       -1.52910
O         -1.51590        0.59860        1.70450
O         -4.05710        0.48410        0.61980
H         -3.12980       -1.37120        0.67510
H         -3.21960       -0.47720       -0.94420
H         -1.81260        1.26690       -0.16830
H         -0.60120       -1.51650        0.34280
H          0.80160        0.44140        1.04340
H          1.83110       -0.54330       -1.68120
H          2.76820        1.49110       -0.60810
H          3.98590        0.13090       -0.69580
H          3.77070        1.20600        1.46320
H          3.12060       -1.82230       -0.01330
H          0.09330        1.27490       -1.56080
H         -1.39120       -1.65560       -1.60030
H         -1.93450       -0.04750        2.32410
H         -3.71890        1.41550        0.66890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

