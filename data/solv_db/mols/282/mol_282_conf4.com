%nproc=16
%mem=2GB
%chk=mol_282_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.01090       -0.53990       -0.09080
C          1.79850        0.32140        0.13240
C          0.50840       -0.43550       -0.21030
C         -0.60980        0.54770       -0.01910
C         -1.93370        0.09510       -0.51660
C         -2.57250       -0.98530        0.30400
O         -3.83700       -1.25550       -0.27100
O         -2.80080        1.21130       -0.44550
O         -0.65470        1.00950        1.29560
O          0.39390       -1.54000        0.59330
O          1.88600        1.42750       -0.69230
O          4.12700        0.22960        0.22410
H          3.03620       -1.44020        0.53290
H          3.08040       -0.83210       -1.15490
H          1.71740        0.65970        1.16400
H          0.60610       -0.70010       -1.28300
H         -0.32940        1.45520       -0.63310
H         -1.87440       -0.19740       -1.56150
H         -1.99970       -1.93330        0.17130
H         -2.75750       -0.71340        1.34140
H         -4.42150       -0.45770       -0.23110
H         -2.64640        1.84180       -1.16790
H         -1.00520        1.94700        1.30450
H          0.85380       -2.33090        0.27590
H          2.18850        2.23900       -0.25290
H          4.23520        0.37660        1.19070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

