%nproc=16
%mem=2GB
%chk=mol_282_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.55190        0.49480       -0.52850
C          1.77260       -0.77010       -0.45760
C          0.48870       -0.70550        0.30060
C         -0.55960        0.15580       -0.35850
C         -1.90020       -0.11330        0.23690
C         -3.03580        0.62770       -0.37730
O         -2.93960        1.98920       -0.34780
O         -2.15850       -1.47910        0.12190
O         -0.14850        1.45170       -0.32860
O          0.65320       -0.24920        1.60730
O          2.59650       -1.68270        0.26350
O          3.76380        0.17650       -1.18940
H          2.10560        1.29900       -1.14830
H          2.83840        0.87560        0.46390
H          1.66730       -1.22450       -1.45880
H          0.06030       -1.74910        0.38310
H         -0.56200       -0.16020       -1.44510
H         -1.86430        0.10880        1.32360
H         -3.14820        0.32970       -1.46210
H         -3.99480        0.33150        0.09470
H         -3.05930        2.38630        0.55530
H         -2.83930       -1.65200       -0.59920
H         -0.22830        1.78140        0.61080
H          1.45000       -0.67470        2.01070
H          2.01200       -2.38620        0.62870
H          4.47830        0.83870       -0.93960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

