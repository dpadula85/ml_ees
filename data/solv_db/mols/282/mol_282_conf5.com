%nproc=16
%mem=2GB
%chk=mol_282_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.75800        1.02460        0.12800
C          1.86790       -0.20780        0.16850
C          0.43120        0.31880        0.03480
C         -0.57260       -0.76370        0.19590
C         -1.95750       -0.37320        0.57470
C         -2.83120        0.13250       -0.52060
O         -2.36760        1.28140       -1.12850
O         -2.58240       -1.52770        1.12230
O         -0.52230       -1.76630       -0.77200
O          0.40210        1.00620       -1.16840
O          2.04210       -0.92200        1.33820
O          4.07760        0.59260        0.21360
H          2.62660        1.60360       -0.78420
H          2.50920        1.66990        0.98220
H          2.04060       -0.85960       -0.70840
H          0.33720        1.04290        0.87890
H         -0.19520       -1.34120        1.13750
H         -1.92340        0.36250        1.39830
H         -3.86840        0.29260       -0.10100
H         -2.99210       -0.60680       -1.33420
H         -2.54310        2.06900       -0.56840
H         -2.73690       -2.20110        0.41660
H         -1.10270       -1.48150       -1.52400
H          0.31810        0.42090       -1.93680
H          2.34050       -0.32820        2.04740
H          4.44430        0.56150       -0.68580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_282_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

