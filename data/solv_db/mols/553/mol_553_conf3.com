%nproc=16
%mem=2GB
%chk=mol_553_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09110       -0.15350       -0.26680
N          0.68690       -0.04270       -0.27440
C          0.10780        1.21320       -0.11450
C         -1.30920        1.15950        0.42970
O         -1.98450        0.19650       -0.33860
C         -1.45220       -1.04070        0.04380
C         -0.08310       -1.13920       -0.63800
H          2.52070        0.79490        0.15010
H          2.40290       -1.03090        0.35810
H          2.55960       -0.28300       -1.27180
H          0.06330        1.80180       -1.06630
H          0.71520        1.81640        0.61340
H         -1.32330        0.81170        1.49370
H         -1.79420        2.15300        0.33930
H         -1.23980       -0.98120        1.13950
H         -2.11340       -1.86510       -0.22630
H          0.37270       -2.07960       -0.21770
H         -0.22050       -1.33100       -1.72110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

