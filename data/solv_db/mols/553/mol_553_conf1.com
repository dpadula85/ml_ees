%nproc=16
%mem=2GB
%chk=mol_553_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92420       -0.62460        0.63670
N         -0.76840       -0.25950       -0.14460
C          0.29140       -1.18350       -0.10580
C          1.66660       -0.62640       -0.17100
O          1.78220        0.65470        0.37340
C          0.95780        1.48210       -0.37600
C         -0.46060        1.11390       -0.14240
H         -2.29150       -1.62190        0.32870
H         -2.71730        0.12860        0.43050
H         -1.72670       -0.65990        1.72110
H          0.15500       -1.84080        0.80000
H          0.17550       -1.88840       -0.98140
H          2.32990       -1.27850        0.45010
H          2.05910       -0.66890       -1.22090
H          1.21550        1.50770       -1.44950
H          1.12600        2.52150        0.01550
H         -0.79820        1.63950        0.79300
H         -1.07190        1.60450       -0.95740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

