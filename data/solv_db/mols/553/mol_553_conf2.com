%nproc=16
%mem=2GB
%chk=mol_553_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.89450       -0.61090        0.07740
N         -0.69800       -0.08770       -0.51900
C          0.31990       -1.02990       -0.75940
C          1.37970       -0.88720        0.32310
O          1.93440        0.35310        0.19260
C          1.08070        1.41150        0.30620
C         -0.31600        1.20050       -0.08890
H         -1.90160       -1.72340        0.09210
H         -2.76260       -0.28020       -0.54660
H         -2.09380       -0.24800        1.09500
H          0.83310       -0.76350       -1.72140
H         -0.10190       -2.04070       -0.81390
H          0.97790       -1.11140        1.32930
H          2.16160       -1.64150        0.08330
H          1.12950        1.75870        1.38080
H          1.54800        2.27100       -0.26070
H         -0.57970        1.92620       -0.91580
H         -1.01660        1.50340        0.74590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

