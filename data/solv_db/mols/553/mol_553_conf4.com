%nproc=16
%mem=2GB
%chk=mol_553_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.62250       -0.86780       -0.55300
N         -0.78100       -0.32790        0.46590
C         -0.61830        1.08430        0.41350
C          0.56370        1.51770       -0.44430
O          1.68920        0.92090        0.09050
C          1.64420       -0.45200        0.07910
C          0.42410       -1.04040        0.70600
H         -1.87070       -1.92600       -0.30160
H         -1.21060       -0.81750       -1.57210
H         -2.57670       -0.30890       -0.55890
H         -0.42770        1.50480        1.43660
H         -1.53140        1.57600        0.04940
H          0.41080        1.31630       -1.51010
H          0.66460        2.61870       -0.27640
H          2.53090       -0.80580        0.68240
H          1.74470       -0.84890       -0.94510
H          0.59910       -1.03970        1.82120
H          0.36760       -2.10370        0.41700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

