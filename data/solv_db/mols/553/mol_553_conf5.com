%nproc=16
%mem=2GB
%chk=mol_553_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.77070       -0.44520        0.57270
N          0.81960       -0.18980       -0.44770
C         -0.20890       -1.14620       -0.60870
C         -1.50680       -0.84030        0.02870
O         -1.82020        0.43660        0.28010
C         -0.87730        1.38820        0.27790
C          0.34080        1.14630       -0.53650
H          2.79900       -0.49340        0.12410
H          1.80510        0.43500        1.26410
H          1.55600       -1.31870        1.21300
H         -0.34560       -1.31920       -1.71740
H          0.15250       -2.14590       -0.23030
H         -1.60600       -1.43230        0.98640
H         -2.31900       -1.27560       -0.62770
H         -1.33980        2.35090       -0.09200
H         -0.54430        1.61860        1.32590
H          0.17530        1.38920       -1.62100
H          1.14880        1.84220       -0.19160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_553_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

