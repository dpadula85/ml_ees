%nproc=16
%mem=2GB
%chk=mol_091_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.37600        0.37730        0.15170
C         -0.76320       -0.99000        0.12710
S          0.94310       -1.04170        0.69330
C          1.90470        0.39180        0.15760
H         -1.06480        1.00510        0.99080
H         -1.21720        0.92490       -0.79840
H         -2.48480        0.25910        0.23360
H         -1.37470       -1.64860        0.78990
H         -0.84500       -1.38680       -0.90380
H          1.38690        1.00790       -0.60710
H          2.90920        0.04250       -0.18700
H          1.98180        1.05850        1.05390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

