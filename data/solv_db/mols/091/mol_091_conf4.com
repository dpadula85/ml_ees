%nproc=16
%mem=2GB
%chk=mol_091_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.80250        0.38430        0.52290
C          0.70190       -0.48700       -0.04130
S         -0.63160       -0.77880        1.12260
C         -2.18210       -0.00270        0.64070
H          1.63310        0.61000        1.60430
H          2.80980       -0.08700        0.42280
H          1.87200        1.35180       -0.02950
H          1.17790       -1.47390       -0.26220
H          0.39150       -0.06490       -1.00300
H         -2.50090       -0.22320       -0.38270
H         -2.11990        1.10500        0.75370
H         -2.95420       -0.33370        1.38060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

