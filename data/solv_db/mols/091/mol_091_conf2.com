%nproc=16
%mem=2GB
%chk=mol_091_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97760        0.17680       -0.43690
C         -0.60360       -0.02270        0.12680
S          0.67820        0.12970       -1.16590
C          2.24750       -0.11280       -0.25960
H         -2.30610        1.19380       -0.18360
H         -2.00670       -0.05990       -1.51330
H         -2.67620       -0.51970        0.07330
H         -0.44510       -1.03750        0.56870
H         -0.40710        0.69900        0.94990
H          3.04510       -0.16050       -1.01600
H          2.32550        0.77150        0.41230
H          2.12610       -1.05780        0.30420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

