%nproc=16
%mem=2GB
%chk=mol_091_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.55060       -0.13550       -0.55930
C          0.80240       -0.11060        0.76640
S         -0.63460        0.96210        0.74140
C         -2.04770        0.24010       -0.11010
H          1.98980        0.87650       -0.72260
H          2.42960       -0.80530       -0.37850
H          0.92770       -0.48020       -1.39780
H          1.53230        0.30870        1.50340
H          0.58450       -1.15330        1.04950
H         -1.78200       -0.62750       -0.73780
H         -2.84480       -0.11000        0.59350
H         -2.50760        1.03510       -0.74820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

