%nproc=16
%mem=2GB
%chk=mol_091_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93660       -0.02050        0.23690
C          0.63870       -0.22810       -0.53400
S         -0.60160        0.94660        0.03970
C         -2.24300        0.21080        0.20650
H          2.82510       -0.04170       -0.43980
H          2.10650       -0.81210        0.99020
H          1.95830        1.00120        0.69130
H          0.88630       -0.08420       -1.61610
H          0.34180       -1.28390       -0.40380
H         -2.22310       -0.75030        0.74490
H         -2.78920        0.15320       -0.76430
H         -2.83630        0.90890        0.84870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_091_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

