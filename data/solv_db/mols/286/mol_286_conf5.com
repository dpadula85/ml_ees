%nproc=16
%mem=2GB
%chk=mol_286_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53880        0.19650       -0.15170
O          1.14050        0.17340        0.05140
C          0.30120       -0.21130       -0.98740
O          0.76010       -0.54120       -2.11040
C         -1.14270       -0.23180       -0.76760
C         -1.80600        0.75220        0.14940
C         -1.57010       -0.63420        0.62280
H          3.05130        0.91100        0.50730
H          2.74160        0.41430       -1.21120
H          2.95750       -0.82030        0.05150
H         -1.79570       -0.54520       -1.61970
H         -2.84930        1.04050        0.00380
H         -1.15560        1.58090        0.48370
H         -2.38570       -1.35520        0.65590
H         -0.78590       -0.72960        1.39890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

