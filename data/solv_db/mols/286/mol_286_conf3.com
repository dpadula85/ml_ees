%nproc=16
%mem=2GB
%chk=mol_286_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.82580       -0.07230       -0.05050
O          1.55020       -0.29470       -0.56830
C          0.39630        0.01710        0.11960
O          0.45750        0.52880        1.27340
C         -0.91440       -0.25510       -0.51090
C         -1.99320        0.78210       -0.21060
C         -2.09810       -0.54440        0.39470
H          3.34530        0.73880       -0.59710
H          2.78410        0.06440        1.03210
H          3.45550       -0.99070       -0.22280
H         -0.88200       -0.69690       -1.50580
H         -1.71700        1.61650        0.46970
H         -2.54670        1.10660       -1.09900
H         -2.75360       -1.34670       -0.00350
H         -1.90950       -0.65340        1.47910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

