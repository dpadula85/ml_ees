%nproc=16
%mem=2GB
%chk=mol_286_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.83760       -0.35510       -0.04920
O          1.49310       -0.76540        0.11030
C          0.41980        0.06130       -0.19830
O          0.70600        1.20520       -0.63120
C         -0.95130       -0.42870       -0.00850
C         -2.08530        0.07250       -0.81610
C         -1.90550        0.56770        0.61390
H          3.10600        0.45950        0.64700
H          3.08990       -0.16480       -1.09500
H          3.46120       -1.23560        0.27090
H         -1.06500       -1.45830        0.38070
H         -2.95250       -0.60110       -0.99280
H         -1.95950        0.81860       -1.61870
H         -2.64840        0.20910        1.35830
H         -1.54610        1.61500        0.76350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

