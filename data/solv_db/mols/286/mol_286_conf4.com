%nproc=16
%mem=2GB
%chk=mol_286_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.50710        0.56930        0.15560
O          1.09640        0.57500        0.07800
C          0.48560       -0.58420       -0.39180
O          1.19050       -1.57460       -0.72380
C         -0.98410       -0.70930       -0.52180
C         -1.74840       -0.15480        0.68200
C         -1.84030        0.48980       -0.65440
H          2.94110        1.58370        0.16910
H          2.85690        0.07300        1.10860
H          2.90090       -0.05190       -0.67380
H         -1.39000       -1.65710       -0.90060
H         -2.61760       -0.74780        1.06140
H         -1.17210        0.30170        1.50850
H         -1.42700        1.50860       -0.71700
H         -2.79900        0.37870       -1.20610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

