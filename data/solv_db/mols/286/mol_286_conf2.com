%nproc=16
%mem=2GB
%chk=mol_286_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.78530       -0.58260        0.36890
O          1.45030       -0.52990        0.81730
C          0.41020        0.01920        0.07210
O          0.69350        0.48220       -1.04430
C         -0.99460        0.05330        0.58980
C         -1.87730        1.12480        0.08390
C         -2.02410       -0.27000       -0.45880
H          3.27170        0.39090        0.46650
H          3.29680       -1.34870        1.02210
H          2.86160       -1.01510       -0.66480
H         -1.13730       -0.38930        1.60310
H         -2.75200        1.48210        0.64960
H         -1.42770        1.89760       -0.57660
H         -2.83210       -0.92870       -0.13470
H         -1.72430       -0.38580       -1.52020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_286_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

