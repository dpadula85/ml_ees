%nproc=16
%mem=2GB
%chk=mol_291_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26250        0.82860        0.34770
C         -2.06210       -0.67170        0.19480
C         -0.74650       -0.89760       -0.56150
O          0.24690       -0.32150        0.21520
C          1.50930       -0.44350       -0.33930
C          2.47310        0.24080        0.64100
O          2.05240        1.56560        0.73130
H         -2.73350        1.05140        1.34000
H         -1.25110        1.28500        0.35290
H         -2.83570        1.25380       -0.48830
H         -2.85430       -1.15470       -0.37530
H         -2.01850       -1.08910        1.20470
H         -0.77530       -0.33800       -1.51580
H         -0.58870       -1.94990       -0.79610
H          1.53060        0.04380       -1.31410
H          1.73540       -1.52660       -0.43240
H          3.51420        0.13690        0.32420
H          2.36290       -0.19250        1.65440
H          2.70350        2.17920        0.31560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

