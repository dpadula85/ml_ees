%nproc=16
%mem=2GB
%chk=mol_291_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.14770        0.57170        0.19350
C          2.01280       -0.40880        0.17520
C          0.67150        0.28500        0.13390
O         -0.38070       -0.62710        0.11680
C         -1.59950        0.02360        0.07910
C         -2.76910       -0.90350        0.05890
O         -3.93330       -0.12780        0.02130
H          3.11360        1.28120       -0.67080
H          4.09920        0.02900        0.24850
H          3.08710        1.20330        1.10680
H          2.05890       -0.98550        1.12140
H          2.11260       -1.11130       -0.66840
H          0.56310        0.91640       -0.76880
H          0.54200        0.89640        1.05000
H         -1.71620        0.69210        0.96300
H         -1.62850        0.61600       -0.87090
H         -2.84190       -1.52110        0.97840
H         -2.80400       -1.55800       -0.82950
H         -3.73550        0.72860        0.47540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

