%nproc=16
%mem=2GB
%chk=mol_291_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.02710        0.13880        0.16410
C          1.75550        0.57960       -0.56030
C          0.61550       -0.10800        0.13340
O         -0.62510        0.20140       -0.42370
C         -1.55170       -0.51940        0.34100
C         -2.96390       -0.30200       -0.12590
O         -3.27480        1.04850       -0.02690
H          3.28530       -0.89960       -0.15280
H          3.84210        0.83490       -0.04000
H          2.82870        0.11220        1.27190
H          1.81490        0.16120       -1.59880
H          1.66650        1.66310       -0.61270
H          0.65680        0.08840        1.21150
H          0.79220       -1.21520        0.00190
H         -1.31100       -1.59920        0.15860
H         -1.38370       -0.27730        1.39300
H         -3.62900       -0.88910        0.50900
H         -3.00490       -0.58510       -1.19940
H         -2.54060        1.56680       -0.44390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

