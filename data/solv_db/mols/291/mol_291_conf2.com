%nproc=16
%mem=2GB
%chk=mol_291_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73700       -0.15280       -0.04700
C          1.61340       -1.14720        0.15060
C          0.33790       -0.34200        0.43190
O          0.04220        0.49590       -0.62110
C         -1.09660        1.22210       -0.34000
C         -2.32610        0.41200       -0.10890
O         -2.58040       -0.33660       -1.26650
H          2.90770       -0.02320       -1.12710
H          3.65780       -0.44300        0.51300
H          2.44940        0.84770        0.38620
H          1.82900       -1.74400        1.03970
H          1.47970       -1.81500       -0.70740
H         -0.49840       -1.02500        0.69690
H          0.57620        0.27980        1.32180
H         -0.90920        1.84650        0.58520
H         -1.32370        1.97740       -1.12960
H         -2.32000       -0.22560        0.78690
H         -3.21580        1.10340        0.02920
H         -3.36010       -0.93050       -1.14490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

