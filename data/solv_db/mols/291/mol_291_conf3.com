%nproc=16
%mem=2GB
%chk=mol_291_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.77880        0.48890       -1.03340
C         -1.87910        0.50610        0.17820
C         -0.51990       -0.10010       -0.12470
O          0.22600       -0.03680        1.04590
C          1.49400       -0.55620        0.92420
C          2.34600        0.13640       -0.10910
O          3.59380       -0.52080       -0.09490
H         -3.70150       -0.08520       -0.81830
H         -3.08140        1.51890       -1.30520
H         -2.26100       -0.03220       -1.87100
H         -2.36190       -0.04980        1.00080
H         -1.76490        1.55320        0.52380
H         -0.61050       -1.12500       -0.53360
H          0.00180        0.58330       -0.85600
H          2.00820       -0.41390        1.90250
H          1.48060       -1.66150        0.71410
H          1.94680        0.04650       -1.13850
H          2.45810        1.19410        0.12250
H          3.40380       -1.44600       -0.41330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_291_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

