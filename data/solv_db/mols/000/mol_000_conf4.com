%nproc=16
%mem=2GB
%chk=mol_000_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.92600        1.06150        0.22990
C         -2.68310       -0.08460       -0.71540
C         -1.55110       -0.97630       -0.27430
C         -0.24400       -0.21570       -0.17400
C          0.81310       -1.17490        0.26680
C          2.14410       -0.58190        0.41030
O          3.07670       -1.34230        0.77990
O          2.45660        0.74860        0.17150
C          3.80790        1.18840        0.35430
H         -2.51080        0.77230        1.23430
H         -4.02810        1.28970        0.32090
H         -2.36290        1.93620       -0.10970
H         -2.51260        0.27100       -1.76180
H         -3.60620       -0.71280       -0.69910
H         -1.82350       -1.39890        0.72070
H         -1.44210       -1.78520       -1.00060
H         -0.04900        0.24940       -1.15050
H         -0.40100        0.59680        0.56400
H          0.85860       -2.02830       -0.42580
H          0.51110       -1.56530        1.26870
H          4.16830        0.82790        1.35250
H          4.41630        0.65300       -0.41340
H          3.88760        2.27120        0.28650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

