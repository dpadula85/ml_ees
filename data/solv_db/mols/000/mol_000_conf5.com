%nproc=16
%mem=2GB
%chk=mol_000_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.96230       -0.43290       -0.19830
C         -2.75600        0.46210       -0.39010
C         -1.52860       -0.37480       -0.14670
C         -0.24900        0.41480       -0.31010
C          0.91380       -0.50890       -0.04640
C          2.21850        0.21810       -0.19340
O          2.21230        1.43530       -0.49920
O          3.41750       -0.40900       -0.00320
C          4.67620        0.23080       -0.12950
H         -4.82600       -0.10420       -0.81090
H         -4.22800       -0.42480        0.86520
H         -3.66800       -1.45520       -0.48540
H         -2.75240        0.75850       -1.46700
H         -2.80950        1.34100        0.28180
H         -1.57040       -0.87190        0.82160
H         -1.52600       -1.17330       -0.93590
H         -0.12950        0.79510       -1.34360
H         -0.29610        1.27420        0.37430
H          0.86160       -1.03040        0.91320
H          0.90990       -1.27890       -0.84510
H          4.98330        0.33840       -1.20150
H          5.42590       -0.44890        0.32830
H          4.68270        1.24490        0.32760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

