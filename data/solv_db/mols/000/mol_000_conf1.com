%nproc=16
%mem=2GB
%chk=mol_000_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.45370        0.03960        0.68000
C         -2.49650        0.50450       -0.37820
C         -1.13780       -0.20350       -0.23110
C         -0.18480        0.25910       -1.28430
C          1.17460       -0.36660       -1.24720
C          1.92580       -0.08940       -0.00850
O          1.47630        0.66450        0.87290
O          3.17380       -0.65080        0.24590
C          3.86110       -0.35780        1.44790
H         -2.88090       -0.39260        1.52440
H         -4.12730       -0.77260        0.30570
H         -4.13280        0.84010        1.04450
H         -2.36740        1.59930       -0.39140
H         -2.94840        0.22950       -1.37050
H         -0.79360        0.00700        0.78950
H         -1.35710       -1.30100       -0.31600
H         -0.66550        0.04380       -2.28520
H         -0.02860        1.37240       -1.23950
H          1.76550        0.13980       -2.07090
H          1.14470       -1.45500       -1.48760
H          3.26220       -0.75430        2.30260
H          4.86890       -0.79260        1.46380
H          3.92140        0.75140        1.63320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

