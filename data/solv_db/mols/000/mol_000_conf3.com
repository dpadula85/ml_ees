%nproc=16
%mem=2GB
%chk=mol_000_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.98030        1.30660        0.58860
C          2.86500       -0.18100        0.44370
C          1.62910       -0.64830       -0.22850
C          0.33250       -0.27810        0.47790
C         -0.75030       -0.85200       -0.41810
C         -2.11520       -0.66260        0.05720
O         -2.80800       -1.62000        0.55090
O         -2.78760        0.56190       -0.00110
C         -4.12640        0.69950        0.44550
H          3.84840        1.72960        0.00840
H          3.15510        1.55680        1.64750
H          2.08760        1.84430        0.15790
H          3.72130       -0.51040       -0.21770
H          3.01370       -0.72880        1.37920
H          1.63990       -1.77290       -0.20540
H          1.51700       -0.34080       -1.26490
H          0.26720       -0.72630        1.48960
H          0.20060        0.81890        0.51880
H         -0.53490       -1.95680       -0.44740
H         -0.57070       -0.47550       -1.45210
H         -4.80760       -0.03890       -0.05270
H         -4.49650        1.71200        0.18340
H         -4.26060        0.56290        1.52660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

