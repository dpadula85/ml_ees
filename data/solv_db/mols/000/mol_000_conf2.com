%nproc=16
%mem=2GB
%chk=mol_000_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.99160        0.23690       -0.08910
C         -2.57180        0.52740       -0.46790
C         -1.56730       -0.24210        0.36420
C         -0.19830        0.13910       -0.11520
C          0.89780       -0.58290        0.66010
C          2.21560       -0.13340        0.10570
O          2.26990        0.71250       -0.85080
O          3.41730       -0.60170        0.59090
C          4.62450       -0.15270        0.04090
H         -4.61050        1.09690       -0.45730
H         -4.09200        0.19920        1.00770
H         -4.40660       -0.65820       -0.58670
H         -2.41490        1.62400       -0.39580
H         -2.36570        0.25670       -1.53110
H         -1.75600       -1.31900        0.16190
H         -1.70940       -0.07370        1.44910
H         -0.13370       -0.13780       -1.18700
H         -0.05720        1.23810       -0.06000
H          0.76770       -1.67400        0.48460
H          0.80960       -0.41570        1.73600
H          5.02900        0.71060        0.61260
H          4.49650        0.22480       -1.00970
H          5.34740       -0.97510       -0.01260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_000_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

