%nproc=16
%mem=2GB
%chk=mol_403_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.86790        1.09930       -0.36070
C          0.13530       -0.03800       -0.38950
C          1.38590        0.26010        0.36720
C         -0.52650       -1.31030        0.04640
Br        -1.21780       -1.14990        1.83470
H         -0.56960        1.94290       -1.00070
H         -1.81030        0.68970       -0.79460
H         -1.08490        1.39350        0.67960
H          0.40830       -0.16690       -1.47640
H          2.29430       -0.01820       -0.24350
H          1.49340       -0.33470        1.30630
H          1.48060        1.32060        0.66490
H         -1.35910       -1.55370       -0.63440
H          0.23840       -2.13430        0.00080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

