%nproc=16
%mem=2GB
%chk=mol_403_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23340       -0.72080        0.16700
C         -0.10050        0.06400       -0.42410
C          1.20300       -0.72390       -0.28090
C          0.04340        1.44330        0.19900
Br         0.37350        1.36490        2.08510
H         -2.16320       -0.60290       -0.45750
H         -1.45730       -0.46490        1.21140
H         -0.97260       -1.80950        0.06540
H         -0.27490        0.23400       -1.50880
H          1.82140       -0.63480       -1.17920
H          0.99540       -1.79960       -0.11000
H          1.75320       -0.26910        0.58760
H         -0.88750        1.98990       -0.04300
H          0.89980        1.92940       -0.31220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

