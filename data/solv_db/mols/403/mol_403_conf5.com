%nproc=16
%mem=2GB
%chk=mol_403_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.16500        1.37080       -0.03200
C          0.04150       -0.07210        0.32300
C         -1.21680       -0.82390       -0.01700
C          1.27710       -0.66140       -0.28080
Br         2.79770        0.34260        0.36210
H         -0.38630        1.52300       -1.10740
H          0.64440        2.03300        0.33380
H         -1.08810        1.70530        0.51460
H          0.16600       -0.11790        1.43490
H         -1.94590       -0.61470        0.82110
H         -1.70500       -0.41840       -0.92670
H         -1.04800       -1.91370       -0.10470
H          1.25770       -0.64790       -1.39050
H          1.37070       -1.70480        0.06960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

