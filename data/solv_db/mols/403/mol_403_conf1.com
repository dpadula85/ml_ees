%nproc=16
%mem=2GB
%chk=mol_403_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.14910       -1.20700       -0.59620
C          0.06190       -0.10520        0.42220
C          1.38400        0.54510        0.07110
C         -1.09130        0.87250        0.34400
Br        -2.71030       -0.10330        0.72870
H         -0.29260       -0.69060       -1.58030
H         -1.01270       -1.85370       -0.34230
H          0.75430       -1.81860       -0.70030
H          0.12000       -0.52140        1.45140
H          1.68810        1.30040        0.81550
H          2.12490       -0.29120       -0.01100
H          1.27700        0.94780       -0.95310
H         -1.14950        1.22710       -0.69730
H         -1.00470        1.69810        1.04760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

