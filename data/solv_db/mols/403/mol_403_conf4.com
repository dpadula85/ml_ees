%nproc=16
%mem=2GB
%chk=mol_403_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.95190       -0.91270        0.49950
C         -0.07670       -0.10270       -0.44680
C          1.36790       -0.43900       -0.27280
C         -0.38890        1.35400       -0.19050
Br         0.01070        1.79230        1.66080
H         -1.24390       -1.85920        0.01170
H         -0.41150       -1.02630        1.43720
H         -1.89290       -0.35010        0.70540
H         -0.33940       -0.31060       -1.50800
H          1.83840        0.06940        0.59360
H          1.43760       -1.55380       -0.09990
H          1.97950       -0.23850       -1.16820
H          0.14230        2.03450       -0.87010
H         -1.47120        1.54270       -0.35190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_403_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

