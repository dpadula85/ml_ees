%nproc=16
%mem=2GB
%chk=mol_010_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.38600       -0.31680       -0.29360
C          2.09820        0.23890        0.23570
C          1.29550       -0.79880        1.00130
C         -0.11410       -0.20460        1.12240
C         -0.75940       -0.44490       -0.23710
C         -0.06510        0.45590       -1.21790
C          1.26450        0.89660       -0.78130
O          1.73200        1.91390       -1.31390
C         -2.20570       -0.18730       -0.11440
C         -3.10280       -1.12200       -0.38940
C         -2.67920        1.15120        0.32940
H          3.84300        0.44450       -0.94450
H          3.27800       -1.30250       -0.78850
H          4.07920       -0.41580        0.58690
H          2.36870        1.02650        0.99560
H          1.26070       -1.70170        0.35280
H          1.70490       -1.00620        1.99950
H         -0.67520       -0.65000        1.94060
H         -0.04170        0.88880        1.24100
H         -0.53950       -1.50110       -0.49320
H          0.05590       -0.08700       -2.19220
H         -0.69720        1.34940       -1.39730
H         -4.15740       -0.95220       -0.30580
H         -2.77940       -2.11140       -0.71580
H         -2.03610        1.97390        0.00920
H         -3.71670        1.32970       -0.07040
H         -2.79730        1.13300        1.44100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

