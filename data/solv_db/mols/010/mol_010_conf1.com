%nproc=16
%mem=2GB
%chk=mol_010_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.03780       -0.60010        0.60560
C          2.18070        0.17520       -0.36150
C          1.30920        1.19750        0.31800
C          0.05110        0.65730        0.88750
C         -0.63580       -0.37760        0.05710
C         -0.07300       -0.35950       -1.37060
C          1.35350       -0.67980       -1.21920
O          1.82320       -1.62090       -1.78770
C         -2.10570       -0.26400        0.06560
C         -2.83640       -1.22880        0.60090
C         -2.84270        0.89820       -0.50480
H          2.64970       -1.61530        0.81640
H          4.07590       -0.72870        0.23570
H          3.12060       -0.04060        1.57130
H          2.89760        0.72220       -1.04240
H          1.91470        1.62250        1.16010
H          1.10510        2.04520       -0.38790
H          0.24460        0.32440        1.94060
H         -0.67270        1.51760        0.99510
H         -0.35990       -1.37230        0.46050
H         -0.17440        0.64900       -1.81910
H         -0.57940       -1.10690       -1.99710
H         -3.92700       -1.17050        0.62080
H         -2.31420       -2.09180        1.02560
H         -2.21690        1.56600       -1.08780
H         -3.69320        0.45090       -1.09630
H         -3.33230        1.43070        0.33940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

