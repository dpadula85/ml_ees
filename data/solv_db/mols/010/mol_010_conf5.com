%nproc=16
%mem=2GB
%chk=mol_010_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.46530        0.05830       -0.03020
C          2.04070       -0.34460        0.17930
C          1.33250       -0.47350       -1.16910
C         -0.13650       -0.82170       -0.89730
C         -0.74120        0.39420       -0.23930
C         -0.14220        0.49580        1.17040
C          1.31110        0.64820        0.98700
O          1.88830        1.57280        1.49250
C         -2.19140        0.31700       -0.08650
C         -2.96370        1.24080       -0.63300
C         -2.78900       -0.80230        0.68340
H          3.56030        1.09710       -0.36730
H          3.99730       -0.04840        0.94470
H          3.92970       -0.67340       -0.71710
H          2.02160       -1.36030        0.64810
H          1.34490        0.53180       -1.61850
H          1.82980       -1.24270       -1.77210
H         -0.18660       -1.68190       -0.20860
H         -0.63550       -1.03860       -1.84600
H         -0.46510        1.29580       -0.78520
H         -0.59210        1.34060        1.73200
H         -0.31970       -0.46090        1.72770
H         -4.04900        1.18020       -0.51680
H         -2.60160        2.07820       -1.20370
H         -3.89060       -0.63660        0.84190
H         -2.72080       -1.70650        0.02140
H         -2.29650       -0.95960        1.66220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

