%nproc=16
%mem=2GB
%chk=mol_010_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.25270       -0.26850       -0.26800
C          2.11840        0.52460        0.32630
C          1.27530       -0.27580        1.27430
C          0.11520       -0.96360        0.61520
C         -0.73410       -0.08360       -0.22240
C         -0.16340        1.33340       -0.33200
C          1.25910        1.15430       -0.69740
O          1.64170        1.51910       -1.78110
C         -2.15610       -0.04520        0.17670
C         -2.80880        1.08600        0.39920
C         -2.88350       -1.32700        0.33420
H          3.25410       -0.22720       -1.37140
H          3.21060       -1.31750        0.11890
H          4.23360        0.13000        0.07670
H          2.55280        1.37430        0.93130
H          1.93020       -1.08320        1.69580
H          0.87210        0.33440        2.10510
H         -0.51980       -1.38180        1.43720
H          0.50940       -1.84900        0.06550
H         -0.68520       -0.47730       -1.28120
H         -0.27070        1.82550        0.65900
H         -0.73380        1.92030       -1.06420
H         -3.83890        1.06110        0.68660
H         -2.26280        2.03850        0.27980
H         -2.16250       -2.15830        0.47520
H         -3.52050       -1.30610        1.23920
H         -3.48500       -1.53730       -0.58260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

