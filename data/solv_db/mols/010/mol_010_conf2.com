%nproc=16
%mem=2GB
%chk=mol_010_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.35550        0.56330       -0.38570
C         -2.13230        0.02260        0.35860
C         -1.20320        1.14030        0.69660
C          0.23620        0.70160        0.85480
C          0.71070       -0.04540       -0.37140
C         -0.04520       -1.33550       -0.43350
C         -1.52340       -1.05090       -0.44000
O         -2.21720       -1.74540       -1.13670
C          2.15620       -0.33320       -0.23960
C          2.64100       -1.56940       -0.19210
C          3.10130        0.80490       -0.15680
H         -4.27750        0.30760        0.18230
H         -3.32190        1.66260       -0.44360
H         -3.37960        0.16740       -1.42000
H         -2.48220       -0.41680        1.33840
H         -1.48640        1.63070        1.65760
H         -1.23480        1.92880       -0.07470
H          0.84230        1.62240        0.93700
H          0.32880        0.03920        1.72800
H          0.54000        0.56930       -1.28590
H          0.15370       -1.90940        0.49410
H          0.24490       -1.94580       -1.29360
H          3.69260       -1.78860       -0.09520
H          1.97390       -2.41060       -0.25840
H          3.03030        1.42820       -1.09460
H          2.86280        1.50610        0.67700
H          4.14450        0.45590       -0.05220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_010_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

