%nproc=16
%mem=2GB
%chk=mol_084_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.92400        1.21560       -0.68540
C          3.16870       -0.20760       -0.24890
C          1.85420       -0.95060       -0.28570
C          0.83110       -0.30760        0.64960
C         -0.42290       -1.14360        0.51410
C         -1.52780       -0.62680        1.39790
C         -1.87050        0.78290        1.03370
C         -2.32060        0.86490       -0.39610
C         -3.52310        0.00920       -0.59930
O         -4.01400       -0.03120       -1.72190
H          2.34930        1.74270        0.08370
H          3.91650        1.71000       -0.84760
H          2.35940        1.15050       -1.65810
H          3.67690       -0.28450        0.71940
H          3.81910       -0.67570       -1.01580
H          1.97560       -1.98490        0.03470
H          1.44170       -0.84040       -1.31060
H          0.59630        0.72380        0.30790
H          1.19990       -0.28550        1.67350
H         -0.76430       -1.20890       -0.54210
H         -0.20420       -2.22080        0.78250
H         -1.20650       -0.71560        2.46320
H         -2.40430       -1.31110        1.28010
H         -1.04160        1.47200        1.18960
H         -2.71450        1.16490        1.67640
H         -1.50730        0.58790       -1.09450
H         -2.61160        1.94450       -0.55940
H         -3.97970       -0.57380        0.19220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

