%nproc=16
%mem=2GB
%chk=mol_084_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.84190        0.68140        1.29540
C          3.06480       -0.25540        0.11800
C          1.79870       -0.38540       -0.70070
C          0.66270       -0.93980        0.13430
C         -0.60640       -1.07520       -0.67150
C         -1.09440        0.23110       -1.23400
C         -1.39600        1.25150       -0.16510
C         -2.46240        0.77860        0.79070
C         -3.74680        0.49610        0.08620
O         -4.71800        0.10470        0.70110
H          2.42000        1.64690        0.95310
H          2.14120        0.19710        2.00290
H          3.82730        0.91020        1.75320
H          3.86250        0.18250       -0.48850
H          3.38790       -1.23610        0.50650
H          2.00580       -0.98060       -1.58960
H          1.54750        0.66510       -1.02740
H          0.43720       -0.26080        0.97700
H          0.94730       -1.96160        0.45580
H         -0.32580       -1.70450       -1.56770
H         -1.38220       -1.65250       -0.15080
H         -1.96000        0.02110       -1.87120
H         -0.29830        0.63560       -1.89030
H         -1.82300        2.14180       -0.69540
H         -0.49030        1.48840        0.38780
H         -2.65810        1.62290        1.49290
H         -2.13120       -0.08770        1.38130
H         -3.85210        0.62980       -0.98400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

