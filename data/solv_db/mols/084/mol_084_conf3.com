%nproc=16
%mem=2GB
%chk=mol_084_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.18470        0.77330        0.07510
C         -2.84510        0.24650       -0.44140
C         -1.71060        1.06660        0.11390
C         -0.36950        0.56520       -0.42000
C         -0.12490       -0.84790       -0.03930
C          1.11040       -1.50170       -0.46440
C          2.43130       -1.09960        0.03890
C          2.86560        0.30720       -0.19000
C          4.29780        0.48920        0.28320
O          4.86430       -0.46760        0.75380
H         -4.07360        1.28530        1.03580
H         -4.93780       -0.04040        0.15620
H         -4.55240        1.50080       -0.65780
H         -2.78830       -0.79060       -0.00520
H         -2.81420        0.13710       -1.52650
H         -1.70580        1.11760        1.20530
H         -1.88410        2.11700       -0.24400
H          0.34830        1.32120       -0.11550
H         -0.45600        0.55340       -1.56350
H         -1.00020       -1.45460       -0.43200
H         -0.21990       -0.97660        1.08030
H          1.15480       -1.57100       -1.62080
H          0.98960       -2.61880       -0.16700
H          2.48710       -1.34770        1.16240
H          3.24420       -1.78690       -0.39700
H          2.27610        0.93850        0.54990
H          2.79150        0.65330       -1.20030
H          4.80610        1.43120        0.20940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

