%nproc=16
%mem=2GB
%chk=mol_084_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.31440        1.21960       -0.16710
C         -3.38510       -0.28940       -0.13190
C         -2.22530       -0.91140        0.61540
C         -0.91990       -0.52710       -0.05560
C          0.24690       -1.15480        0.69070
C          1.56250       -0.80560        0.06310
C          1.79870        0.68260        0.05060
C          3.11600        1.01710       -0.58410
C          4.28920        0.43920        0.07080
O          4.62010       -0.72100       -0.08040
H         -3.17380        1.56140        0.88790
H         -2.45800        1.60670       -0.75570
H         -4.29580        1.57390       -0.58430
H         -3.41920       -0.71390       -1.17090
H         -4.32700       -0.58110        0.36490
H         -2.25210       -0.54690        1.66350
H         -2.33100       -2.01040        0.68440
H         -0.85810        0.57080       -0.05910
H         -0.90840       -0.90780       -1.08560
H          0.26110       -0.81670        1.73580
H          0.13220       -2.25790        0.68040
H          2.35120       -1.30010        0.68290
H          1.65800       -1.18580       -0.97640
H          1.01260        1.15070       -0.60760
H          1.66990        1.13320        1.04430
H          3.07920        0.59970       -1.62970
H          3.16750        2.12640       -0.67840
H          4.90300        1.04850        0.71930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

