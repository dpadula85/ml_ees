%nproc=16
%mem=2GB
%chk=mol_084_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.11060        0.56250       -0.19250
C         -3.30600       -0.71010        0.00510
C         -1.85710       -0.49990       -0.41450
C         -1.29880        0.59600        0.44020
C          0.13190        0.92530        0.14800
C          1.06230       -0.22690        0.34560
C          2.47170        0.27730        0.00670
C          3.48280       -0.84790        0.18560
C          4.82570       -0.33090       -0.14780
O          5.83990       -0.99880       -0.09900
H         -4.10530        1.20250        0.71550
H         -3.71770        1.10240       -1.08390
H         -5.17730        0.31510       -0.45020
H         -3.34730       -0.94170        1.08540
H         -3.77660       -1.50700       -0.57640
H         -1.82640       -0.25320       -1.49060
H         -1.32100       -1.44610       -0.19890
H         -1.45110        0.35220        1.50020
H         -1.90040        1.51630        0.24010
H          0.43330        1.76240        0.79010
H          0.21120        1.26430       -0.90710
H          0.84930       -1.04410       -0.39320
H          1.01930       -0.67160        1.33650
H          2.77040        1.09230        0.69780
H          2.50250        0.67800       -1.01440
H          3.22640       -1.61640       -0.58220
H          3.39750       -1.26560        1.19430
H          4.97110        0.71360       -0.46470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_084_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

