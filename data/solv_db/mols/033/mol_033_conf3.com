%nproc=16
%mem=2GB
%chk=mol_033_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99160        0.75560       -0.00380
C         -0.53370        0.56900       -0.36470
C         -0.01380       -0.68920        0.28830
C          1.42200       -0.96130       -0.00940
S          2.44840        0.42050        0.58530
H         -2.36680        1.74910       -0.26610
H         -2.04320        0.62470        1.11610
H         -2.57970       -0.07340       -0.44590
H         -0.40170        0.57780       -1.45070
H         -0.00010        1.46600        0.03420
H         -0.12460       -0.56220        1.37970
H         -0.65140       -1.54060       -0.01140
H          1.73040       -1.85810        0.56620
H          1.62220       -1.18890       -1.07200
H          3.48370        0.71100       -0.30800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

