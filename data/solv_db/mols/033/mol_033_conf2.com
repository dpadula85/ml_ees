%nproc=16
%mem=2GB
%chk=mol_033_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67850       -0.71180       -0.33270
C         -1.14560        0.49230        0.37780
C          0.25240        0.87150       -0.05060
C          1.23110       -0.25170        0.21500
S          2.90140        0.22010       -0.30450
H         -0.96010       -1.19890       -1.00880
H         -2.01890       -1.45210        0.44540
H         -2.56890       -0.45080       -0.97090
H         -1.85860        1.32710        0.22930
H         -1.10990        0.32930        1.48750
H          0.30150        1.25950       -1.07170
H          0.56310        1.71200        0.62730
H          1.27510       -0.35780        1.33920
H          0.94070       -1.20120       -0.26280
H          3.87520       -0.58740        0.26930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

