%nproc=16
%mem=2GB
%chk=mol_033_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97910        0.46850       -0.14180
C         -0.69490       -0.21030       -0.63710
C          0.30710       -0.06050        0.49740
C          1.63690       -0.69730        0.13100
S          2.35570        0.05990       -1.33530
H         -2.75180       -0.29900        0.08590
H         -1.79950        1.10750        0.72790
H         -2.36630        1.11010       -0.97910
H         -0.93530       -1.27620       -0.82430
H         -0.30310        0.29670       -1.54730
H         -0.11280       -0.60230        1.36870
H          0.42610        0.99620        0.79960
H          1.53340       -1.77710       -0.05480
H          2.33310       -0.57650        0.98630
H          2.35050        1.46030       -1.26860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

