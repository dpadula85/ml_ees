%nproc=16
%mem=2GB
%chk=mol_033_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38900        0.84940        0.16470
C         -0.98420       -0.57780        0.30790
C          0.21320       -0.99490       -0.46110
C          1.48140       -0.27580       -0.14050
S          1.42610        1.49990       -0.41580
H         -0.93430        1.52350        0.91930
H         -1.23050        1.17160       -0.87750
H         -2.50820        0.89530        0.31250
H         -0.81210       -0.84060        1.38710
H         -1.83600       -1.20830       -0.05500
H          0.01870       -0.95430       -1.57220
H          0.40040       -2.07660       -0.26130
H          1.72770       -0.51560        0.93440
H          2.27340       -0.69300       -0.80470
H          2.15350        2.19720        0.56210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

