%nproc=16
%mem=2GB
%chk=mol_033_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.40530        0.74260        0.48810
C         -0.99290       -0.12890       -0.62840
C          0.19870       -0.99530       -0.44060
C          1.47470       -0.26320       -0.15940
S          1.32190        0.68430        1.34930
H         -0.98410        1.77030        0.48760
H         -2.51790        0.93580        0.35380
H         -1.33470        0.24580        1.46880
H         -0.79430        0.49030       -1.54910
H         -1.85120       -0.78940       -0.91130
H          0.32840       -1.54630       -1.42180
H          0.03420       -1.82420        0.28800
H          2.29160       -1.02260       -0.01280
H          1.73240        0.38250       -1.04340
H          2.49850        1.31830        1.73130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_033_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

