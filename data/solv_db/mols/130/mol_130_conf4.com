%nproc=16
%mem=2GB
%chk=mol_130_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.68880        0.13490       -0.38940
C          0.70010       -0.03010        0.21700
Cl         1.45580        1.53230        0.56640
Cl         1.73350       -0.89930       -0.92430
Cl         0.49390       -0.93190        1.74420
Cl        -1.72740        1.02330        0.76660
Cl        -1.35320       -1.49840       -0.62230
H         -0.61390        0.66930       -1.35810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

