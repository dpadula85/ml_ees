%nproc=16
%mem=2GB
%chk=mol_130_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.71210       -0.33270        0.10500
C         -0.71240        0.16530        0.06320
Cl        -1.18550        0.97680        1.54590
Cl        -0.83280        1.31400       -1.28670
Cl        -1.82150       -1.18290       -0.27760
Cl         1.77020        1.09090        0.35450
Cl         1.15300       -1.02950       -1.46100
H          0.91700       -1.00180        0.95670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

