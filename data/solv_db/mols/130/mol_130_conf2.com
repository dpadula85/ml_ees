%nproc=16
%mem=2GB
%chk=mol_130_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.78500        0.01050       -0.11910
C         -0.72470       -0.01260       -0.06320
Cl        -1.25420       -0.56070        1.54360
Cl        -1.41460       -1.09040       -1.28890
Cl        -1.28160        1.66060       -0.32340
Cl         1.32230        1.14990        1.15340
Cl         1.51250       -1.55420        0.22870
H          1.05520        0.39710       -1.13120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

