%nproc=16
%mem=2GB
%chk=mol_130_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.63960       -0.42060       -0.16370
C         -0.67600        0.22190        0.23310
Cl        -0.52370        1.96860       -0.15340
Cl        -1.03220        0.02390        1.94000
Cl        -1.96590       -0.44720       -0.77050
Cl         1.99780        0.29350        0.73730
Cl         0.92760       -0.12200       -1.89450
H          0.63290       -1.51810        0.07180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_130_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

