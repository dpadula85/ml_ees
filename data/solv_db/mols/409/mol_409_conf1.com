%nproc=16
%mem=2GB
%chk=mol_409_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.10990        2.42100        0.04190
O         -0.13670        1.62130        0.56510
P         -0.35180       -0.00830        0.34130
O         -0.65020       -0.67850        1.68060
C          1.20320       -0.71480       -0.28480
C          2.29350       -0.29310        0.68580
Cl         3.84970       -0.94660        0.14150
Cl         2.44990        1.47150        0.77770
Cl         1.83890       -0.89860        2.28700
O          1.45760       -0.26180       -1.57860
O         -1.59870       -0.40510       -0.70080
C         -1.90240       -1.76430       -0.49910
H         -1.98390        2.40210        0.71350
H         -0.73450        3.46240        0.01600
H         -1.44110        2.16740       -0.96800
H          1.18780       -1.80500       -0.31800
H          1.55380        0.72280       -1.56660
H         -0.97280       -2.36890       -0.35560
H         -2.43170       -2.19210       -1.36690
H         -2.52090       -1.93140        0.38800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

