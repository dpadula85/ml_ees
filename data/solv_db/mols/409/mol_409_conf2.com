%nproc=16
%mem=2GB
%chk=mol_409_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.62530        2.29620        0.16710
O         -0.52600        1.56120        0.60200
P         -0.48080        0.00930       -0.01390
O         -1.37960       -0.08090       -1.22910
C          1.19970       -0.44160       -0.59190
C          2.24870        0.10640        0.34630
Cl         3.83560       -0.35890       -0.26080
Cl         2.15860        1.89830        0.30130
Cl         2.01310       -0.41370        2.00640
O          1.32530        0.12620       -1.85730
O         -0.97570       -1.07800        1.17440
C         -1.29670       -2.31460        0.59420
H         -2.17900        1.81470       -0.64970
H         -2.36290        2.43100        1.00300
H         -1.25750        3.27420       -0.20970
H          1.24180       -1.54430       -0.62090
H          2.19920        0.52010       -2.01610
H         -0.37930       -2.90850        0.55030
H         -1.63730       -2.11880       -0.44060
H         -2.12200       -2.77850        1.14480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

