%nproc=16
%mem=2GB
%chk=mol_409_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67410        1.96570        0.12560
O         -0.30650        1.63120        0.22620
P         -0.19520        0.00860       -0.27130
O         -0.41940       -0.00470       -1.76210
C          1.46450       -0.56860        0.09750
C          2.32570        0.53450        0.70800
Cl         2.48620        1.91350       -0.37840
Cl         1.61900        1.08830        2.23890
Cl         3.92640       -0.17570        1.02390
O          2.12240       -0.93590       -1.09620
O         -1.38530       -0.84400        0.55980
C         -1.76920       -2.00680       -0.07540
H         -2.04530        1.49640       -0.81630
H         -2.26530        1.53570        0.94230
H         -1.72740        3.07300        0.07150
H          1.51200       -1.42060        0.77640
H          2.03640       -0.20190       -1.75490
H         -1.70750       -1.97250       -1.17060
H         -2.83040       -2.26030        0.20710
H         -1.16700       -2.85600        0.34800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

