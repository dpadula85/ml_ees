%nproc=16
%mem=2GB
%chk=mol_409_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.50030        2.10950       -0.22280
O         -0.62290        1.71950        0.78900
P         -0.30410        0.04760        0.64050
O         -0.20810       -0.54710        2.04410
C          1.22020       -0.22460       -0.26470
C          2.45290        0.09700        0.54110
Cl         2.51440        1.76520        1.07240
Cl         3.90730       -0.26010       -0.41540
Cl         2.47640       -0.97880        1.97630
O          1.19710        0.61950       -1.39470
O         -1.57370       -0.62820       -0.21550
C         -1.68520       -1.99770       -0.04570
H         -1.57040        3.19190       -0.30770
H         -2.50690        1.72670        0.06280
H         -1.21350        1.63320       -1.19270
H          1.24860       -1.26460       -0.63460
H          1.44540        0.11400       -2.20560
H         -1.83020       -2.26830        1.02250
H         -0.84440       -2.54130       -0.52090
H         -2.60240       -2.31340       -0.58560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

