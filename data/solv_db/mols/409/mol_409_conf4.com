%nproc=16
%mem=2GB
%chk=mol_409_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25900       -1.51600       -0.23280
O         -1.48000       -1.18660        0.83690
P         -0.26430       -0.08620        0.51180
O          0.00970        0.66940        1.81590
C          1.26140       -0.94790        0.07960
C          2.48860       -0.11640        0.28630
Cl         2.45100        1.35700       -0.69770
Cl         2.72080        0.35410        1.97950
Cl         3.89550       -1.09490       -0.19370
O          1.22350       -1.33010       -1.25720
O         -0.69820        1.06580       -0.62990
C         -0.94860        2.31580       -0.06110
H         -2.60450       -0.68980       -0.85360
H         -1.76410       -2.28200       -0.88300
H         -3.17660       -2.02740        0.15930
H          1.35190       -1.85260        0.69630
H          0.93360       -0.55750       -1.80530
H         -1.11430        3.03910       -0.86530
H         -1.89330        2.21840        0.52490
H         -0.13300        2.66760        0.58880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_409_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

