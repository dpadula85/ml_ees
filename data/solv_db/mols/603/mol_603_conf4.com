%nproc=16
%mem=2GB
%chk=mol_603_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95050       -0.52600       -0.88210
C          1.31660       -0.20510        0.23070
C          0.00050       -0.82820        0.49480
C         -1.08830        0.13910        0.62240
C         -2.14470        0.14660       -0.16940
H          1.56380       -1.22040       -1.61620
H          2.91110       -0.07530       -1.08130
H          1.78560        0.50710        0.91500
H         -0.21100       -1.55600       -0.30990
H          0.11270       -1.40320        1.43970
H         -1.02480        0.88220        1.40100
H         -2.21150       -0.59850       -0.95270
H         -2.96060        0.85270       -0.09190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

