%nproc=16
%mem=2GB
%chk=mol_603_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.25640       -0.30920        0.25040
C          1.09680       -0.51090       -0.34110
C          0.05750        0.54140       -0.17510
C         -1.16540        0.00160        0.47790
C         -2.31210        0.06370       -0.17650
H          2.49040        0.56080        0.83790
H          3.01820       -1.06100        0.13950
H          0.90870       -1.41290       -0.92760
H         -0.17600        1.03600       -1.14380
H          0.42950        1.32550        0.48940
H         -1.04340       -0.41870        1.46980
H         -2.31950        0.50420       -1.16150
H         -3.24120       -0.32040        0.26060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

