%nproc=16
%mem=2GB
%chk=mol_603_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.74640        0.91330       -0.51850
C          0.74240        0.59410        0.28820
C          0.28930       -0.80570        0.41120
C         -1.13630       -0.93370        0.01020
C         -1.77460        0.15900       -0.38140
H          2.26160        0.14650       -1.11100
H          2.10580        1.91910       -0.63620
H          0.27560        1.39400        0.84700
H          0.33040       -1.16300        1.48240
H          0.92480       -1.50980       -0.14780
H         -1.60730       -1.91020        0.05000
H         -1.34900        1.14630       -0.43670
H         -2.80910        0.05020       -0.66920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

