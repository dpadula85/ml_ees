%nproc=16
%mem=2GB
%chk=mol_603_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.98080        0.21960        0.46930
C          1.01760       -0.04520       -0.40010
C         -0.10940       -0.92150       -0.08010
C         -1.42340       -0.27880       -0.17380
C         -1.60970        0.99330       -0.51830
H          1.97710       -0.20600        1.46820
H          2.81330        0.87000        0.22560
H          1.07440        0.40730       -1.37910
H         -0.10850       -1.84880       -0.72380
H         -0.01140       -1.32100        0.96940
H         -2.29920       -0.87780        0.05180
H         -2.59570        1.43110       -0.57620
H         -0.70580        1.57780       -0.74170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

