%nproc=16
%mem=2GB
%chk=mol_603_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.73770        0.15740       -0.99640
C         -0.93130       -0.36150       -0.09650
C         -0.16570        0.49210        0.86290
C          1.26770        0.26510        0.66520
C          1.69530       -0.57330       -0.24670
H         -2.27870       -0.46320       -1.67590
H         -1.84270        1.22380       -1.03510
H         -0.85530       -1.45550       -0.06270
H         -0.41720        0.24450        1.91520
H         -0.44240        1.55020        0.67820
H          1.97510        0.79380        1.27710
H          0.97590       -1.10800       -0.86710
H          2.75700       -0.76550       -0.41810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_603_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

