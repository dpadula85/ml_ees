%nproc=16
%mem=2GB
%chk=mol_326_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.37540       -0.47940        0.08400
C          0.00700       -0.03150       -0.35150
C         -1.09130       -0.95380        0.05410
C         -0.27650        1.41220        0.07640
H          1.38100       -1.42010        0.64750
H          2.08890       -0.58370       -0.77410
H          1.78700        0.33110        0.73190
H          0.02450        0.00440       -1.47410
H         -0.81400       -1.71940        0.78420
H         -1.88970       -0.33340        0.53660
H         -1.59290       -1.43060       -0.82870
H          0.67510        1.96670       -0.05280
H         -0.59380        1.42100        1.14580
H         -1.08070        1.81640       -0.57940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

