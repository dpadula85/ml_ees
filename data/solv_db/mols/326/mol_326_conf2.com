%nproc=16
%mem=2GB
%chk=mol_326_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.40280        0.09670        0.41230
C          0.08670        0.09640       -0.31250
C         -0.88760        1.07190        0.27570
C         -0.52830       -1.28160       -0.40040
H          1.68360        1.16560        0.56920
H          1.35100       -0.45330        1.37900
H          2.19490       -0.31820       -0.26040
H          0.27580        0.41860       -1.36680
H         -1.60010        1.35980       -0.53860
H         -1.46620        0.65240        1.12960
H         -0.32480        1.98850        0.56150
H          0.18730       -2.01430       -0.79020
H         -1.03710       -1.58120        0.52540
H         -1.33790       -1.20120       -1.18370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

