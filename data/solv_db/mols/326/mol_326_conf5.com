%nproc=16
%mem=2GB
%chk=mol_326_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.19450       -1.40620        0.17280
C          0.10740        0.00020       -0.34380
C         -1.32010        0.48110       -0.08580
C          1.11600        0.93860        0.18290
H         -0.73970       -1.92840       -0.18910
H          1.02610       -1.96230       -0.32650
H          0.18880       -1.49880        1.25760
H          0.23080       -0.06560       -1.46290
H         -1.71200        0.06540        0.87620
H         -1.95420        0.07230       -0.90180
H         -1.34040        1.57430       -0.12010
H          2.00200        0.47770        0.64840
H          1.51910        1.59490       -0.63950
H          0.68180        1.65690        0.93160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

