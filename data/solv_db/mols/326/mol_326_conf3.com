%nproc=16
%mem=2GB
%chk=mol_326_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.59550       -1.24730       -0.32870
C         -0.01800       -0.03040        0.39130
C          1.45500        0.11580        0.16400
C         -0.81700        1.16410       -0.06020
H         -0.92760       -0.94050       -1.35350
H          0.15140       -2.06120       -0.38540
H         -1.50750       -1.54420        0.24650
H         -0.19470       -0.19410        1.49080
H          2.02910       -0.19740        1.08420
H          1.84180       -0.55420       -0.64430
H          1.77180        1.16890       -0.02390
H         -1.24000        0.99730       -1.08400
H         -0.23990        2.09450       -0.10940
H         -1.70890        1.22880        0.61270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

