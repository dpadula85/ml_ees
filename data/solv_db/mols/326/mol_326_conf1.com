%nproc=16
%mem=2GB
%chk=mol_326_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07000        0.89590       -0.37650
C          0.03680        0.19930        0.34440
C          1.37520        0.32170       -0.31010
C         -0.31650       -1.27290        0.43030
H         -1.14460        0.62480       -1.44730
H         -2.03620        0.52090        0.07530
H         -1.08210        1.98850       -0.25850
H          0.11770        0.65870        1.35710
H          2.13180       -0.05510        0.44240
H          1.66610        1.38640       -0.47700
H          1.51170       -0.31220       -1.19770
H         -0.37740       -1.53520        1.51780
H         -1.28270       -1.49880       -0.09550
H          0.47020       -1.92200       -0.00470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_326_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

