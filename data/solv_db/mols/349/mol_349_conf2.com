%nproc=16
%mem=2GB
%chk=mol_349_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.65720       -0.28940        0.04870
N         -0.25050        0.05120        0.01000
C          0.77040       -0.80750       -0.17670
C          1.95430       -0.09350       -0.14790
N          1.64260        1.19990        0.05710
C          0.28330        1.29160        0.15490
H         -2.17370       -0.11700       -0.91220
H         -2.12820        0.31520        0.86880
H         -1.74890       -1.34830        0.37080
H          0.67660       -1.87500       -0.32410
H          2.94430       -0.51200       -0.26960
H         -0.31300        2.18470        0.32020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

