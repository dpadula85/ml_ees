%nproc=16
%mem=2GB
%chk=mol_349_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.65790       -0.21260       -0.03420
N         -0.23320        0.08400        0.01680
C          0.72040       -0.83150       -0.15490
C          1.92960       -0.19350       -0.04030
N          1.65960        1.09730        0.19890
C          0.33090        1.26170        0.23280
H         -1.83270       -1.28190        0.15940
H         -2.19290        0.36530        0.74840
H         -2.01880        0.05440       -1.05650
H          0.57310       -1.89650       -0.35120
H          2.90660       -0.64940       -0.12710
H         -0.18460        2.20270        0.40800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

