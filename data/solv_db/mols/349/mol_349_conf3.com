%nproc=16
%mem=2GB
%chk=mol_349_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69300       -0.09380       -0.03300
N         -0.23420        0.06720       -0.00290
C          0.70350       -0.88510       -0.11540
C          1.95840       -0.31200       -0.03540
N          1.74250        1.00620        0.12760
C          0.41160        1.23880        0.14760
H         -2.12060        0.34680        0.91060
H         -2.14870        0.38200       -0.90420
H         -1.92630       -1.17140       -0.01580
H          0.48890       -1.93410       -0.24770
H          2.92340       -0.82700       -0.09310
H         -0.10550        2.18240        0.26180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

