%nproc=16
%mem=2GB
%chk=mol_349_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69730       -0.20810       -0.05600
N         -0.25950        0.04370       -0.00600
C          0.74690       -0.83260       -0.16480
C          1.96730       -0.19200       -0.04380
N          1.67150        1.09630        0.19280
C          0.32190        1.22650        0.21330
H         -2.08070        0.02440        0.97880
H         -1.88880       -1.27800       -0.24840
H         -2.21100        0.46650       -0.77070
H          0.60670       -1.88540       -0.35910
H          2.97000       -0.63990       -0.12370
H         -0.14710        2.17860        0.38750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_349_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

