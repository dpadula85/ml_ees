%nproc=16
%mem=2GB
%chk=mol_452_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.28600       -1.46320       -0.17230
C          1.15340       -0.54720        0.04850
C         -0.12690       -1.04450       -0.00730
C         -1.17410       -0.19680        0.19690
C         -0.95300        1.15840        0.45930
C          0.31590        1.67410        0.51920
C          1.36870        0.78670        0.30690
O          0.60440        2.99040        0.77160
C         -2.58950       -0.62550        0.15960
H          1.92740       -2.40790       -0.66620
H          3.04630       -1.04000       -0.86090
H          2.73500       -1.74190        0.81050
H         -0.31920       -2.08140       -0.20730
H         -1.77230        1.82990        0.62070
H          2.37410        1.14660        0.34470
H         -0.04430        3.71940        0.93860
H         -2.72970       -1.52330       -0.48030
H         -2.91630       -0.82330        1.21400
H         -3.18580        0.18950       -0.30220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

