%nproc=16
%mem=2GB
%chk=mol_452_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35790       -1.37110       -0.08100
C         -1.15330       -0.49650       -0.01000
C          0.13640       -1.00630       -0.02180
C          1.23510       -0.17370        0.04490
C          1.10130        1.19750        0.12620
C         -0.17620        1.70500        0.13810
C         -1.27280        0.87470        0.07140
O         -0.33250        3.07770        0.21920
C          2.58050       -0.80670        0.02610
H         -2.10030       -2.36270        0.36960
H         -3.20140       -0.95590        0.50750
H         -2.70140       -1.48910       -1.12910
H          0.21660       -2.08380       -0.08590
H          1.98800        1.82850        0.17760
H         -2.29290        1.28230        0.08100
H         -0.38670        3.70720       -0.54910
H          2.68190       -1.50750       -0.84500
H          2.65190       -1.38930        0.98790
H          3.38380       -0.03030       -0.02770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

