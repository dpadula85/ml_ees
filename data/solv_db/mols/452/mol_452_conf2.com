%nproc=16
%mem=2GB
%chk=mol_452_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91960       -1.95370        0.17650
C         -0.99580       -0.76720        0.12660
C          0.36270       -0.92530       -0.06600
C          1.24620        0.13480       -0.11830
C          0.73470        1.40700        0.03030
C         -0.62230        1.60050        0.22490
C         -1.48340        0.51680        0.27270
O         -1.11020        2.89900        0.37200
C          2.71600       -0.10580       -0.33120
H         -2.90830       -1.65470       -0.24780
H         -1.43940       -2.74740       -0.41830
H         -1.98780       -2.23110        1.25190
H          0.73790       -1.93370       -0.17980
H          1.42290        2.24710       -0.00950
H         -2.53430        0.70560        0.42580
H         -1.40220        3.42240       -0.43420
H          2.84600       -0.98000       -0.97030
H          3.21820        0.74960       -0.78320
H          3.11860       -0.38400        0.67790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

