%nproc=16
%mem=2GB
%chk=mol_452_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.57070       -0.75580       -0.32170
C          1.21670       -0.19690       -0.19630
C          0.16060       -1.05090        0.04380
C         -1.10500       -0.51120        0.15930
C         -1.33890        0.84750        0.04190
C         -0.24560        1.66870       -0.19970
C          1.03600        1.16470       -0.32130
O         -0.46550        3.03220       -0.31950
C         -2.27440       -1.38120        0.41630
H          2.62290       -1.73800        0.19010
H          3.27740       -0.09130        0.24990
H          2.83610       -0.79450       -1.38530
H          0.31440       -2.12520        0.14120
H         -2.31810        1.28260        0.12930
H          1.89330        1.82740       -0.51280
H         -0.43910        3.67310        0.46370
H         -1.91060       -2.36440        0.74330
H         -2.94360       -0.97020        1.19000
H         -2.88730       -1.51660       -0.51220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

