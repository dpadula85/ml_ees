%nproc=16
%mem=2GB
%chk=mol_452_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.55310       -0.88450        0.30260
C          1.19830       -0.26200        0.10110
C          0.06800       -1.04750        0.02550
C         -1.17650       -0.48120       -0.15930
C         -1.22360        0.89370       -0.26220
C         -0.09230        1.67950       -0.18640
C          1.14830        1.10500       -0.00150
O         -0.14950        3.06460       -0.29120
C         -2.41840       -1.29220       -0.24610
H          3.25040       -0.42850       -0.42360
H          2.51110       -1.97560        0.27510
H          2.88540       -0.50420        1.31170
H          0.14510       -2.10890        0.10970
H         -2.18420        1.35820       -0.40670
H          2.05940        1.68330        0.06330
H         -0.27480        3.56370        0.59950
H         -3.27160       -0.67980        0.16010
H         -2.66070       -1.45910       -1.31620
H         -2.36760       -2.22440        0.34440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_452_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

