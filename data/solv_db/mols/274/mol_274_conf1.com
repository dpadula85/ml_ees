%nproc=16
%mem=2GB
%chk=mol_274_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.65480       -0.20310       -0.00070
C         -0.67520       -0.18220        0.00200
Cl        -1.55650        1.33740       -0.00130
Cl         1.58370        1.30760       -0.00770
H          1.20790       -1.13590        0.00180
H         -1.21460       -1.12370        0.00590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_274_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_274_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_274_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

