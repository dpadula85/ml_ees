%nproc=16
%mem=2GB
%chk=mol_616_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32710        0.43810       -0.59800
C         -0.90020       -0.02010       -0.85580
C         -0.47530       -1.10690        0.06880
C          0.54610       -0.63410        1.08930
C          1.66660        0.14340        0.46130
C          1.24040        0.80980       -0.83670
O         -0.09850        1.12970       -0.72610
O          1.95390        1.98740       -1.05820
O          2.74870       -0.69230        0.16040
O         -0.05770        0.12440        2.08300
O          0.15340       -2.14270       -0.64980
O         -2.46580        0.98060        0.66710
H         -3.02330       -0.41950       -0.71240
H         -2.63170        1.20630       -1.32520
H         -0.84700       -0.36590       -1.90870
H         -1.30780       -1.56080        0.64720
H          0.96850       -1.53850        1.55660
H          2.04850        0.92650        1.17100
H          1.42330        0.09490       -1.67270
H          1.42940        2.74630       -0.72240
H          3.22880       -0.84430        1.00720
H          0.11220        1.09270        1.99340
H         -0.50580       -2.69860       -1.14080
H         -2.87940        0.34360        1.30140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

