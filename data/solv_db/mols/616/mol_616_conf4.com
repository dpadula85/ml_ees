%nproc=16
%mem=2GB
%chk=mol_616_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.51010       -0.66800        0.30920
C          1.10530       -0.38800       -0.12990
C          0.47500        0.74110        0.66190
C         -0.90660        0.94440        0.03520
C         -1.65230       -0.35630        0.28490
C         -0.92540       -1.41700       -0.55200
O          0.36770       -1.55110       -0.05950
O         -1.54340       -2.64950       -0.45380
O         -2.98690       -0.27930       -0.02660
O         -1.55240        2.04620        0.51690
O          1.25850        1.86620        0.58030
O          3.30630        0.48430        0.25700
H          2.53890       -1.16180        1.28930
H          2.94330       -1.38980       -0.43140
H          1.13400       -0.02180       -1.19770
H          0.31870        0.39650        1.71270
H         -0.75810        0.99280       -1.07660
H         -1.50140       -0.62850        1.35010
H         -0.91780       -1.01500       -1.58640
H         -2.52270       -2.56190       -0.45080
H         -3.17130        0.66650       -0.30270
H         -1.72350        2.67190       -0.25680
H          0.70440        2.59510        0.21030
H          3.49960        0.68310       -0.68320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

