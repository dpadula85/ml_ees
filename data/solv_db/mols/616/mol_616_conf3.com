%nproc=16
%mem=2GB
%chk=mol_616_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.14880       -1.03990       -0.13800
C         -0.94180       -0.56850        0.66840
C         -0.52420        0.80080        0.26420
C          0.93930        1.06020        0.44390
C          1.77040       -0.03340       -0.16140
C          1.01840       -1.33920       -0.29740
O          0.04460       -1.51160        0.65870
O          0.54710       -1.50870       -1.59450
O          2.92510       -0.16450        0.60870
O          1.24390        2.27480       -0.16320
O         -1.20560        1.76230        1.04550
O         -3.15060       -0.09610       -0.13530
H         -2.54530       -1.93400        0.41070
H         -1.88630       -1.28060       -1.18100
H         -1.32090       -0.49800        1.73080
H         -0.79470        1.06130       -0.79480
H          1.16420        1.18470        1.53170
H          2.11850        0.32130       -1.17170
H          1.77920       -2.14820       -0.13350
H          0.32240       -0.66640       -2.04090
H          3.41530       -0.99720        0.38290
H          0.71570        2.28640       -1.01820
H         -0.60330        2.26730        1.62100
H         -2.88270        0.76690       -0.53680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

