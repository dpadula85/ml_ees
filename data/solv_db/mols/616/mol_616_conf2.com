%nproc=16
%mem=2GB
%chk=mol_616_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.45340       -0.17310       -0.36830
C          1.08380        0.03520        0.15070
C          0.13680       -0.91710       -0.59620
C         -1.14700       -0.84860        0.19660
C         -1.67090        0.56870       -0.01660
C         -0.63260        1.57700        0.35440
O          0.62760        1.30530       -0.09500
O         -1.03300        2.79540       -0.24430
O         -2.82850        0.80280        0.69850
O         -2.06140       -1.79940       -0.14410
O          0.65800       -2.17390       -0.70920
O          3.36440        0.69940        0.20830
H          2.48090       -0.00490       -1.45630
H          2.79810       -1.21450       -0.14440
H          1.04430       -0.22070        1.23820
H         -0.07740       -0.42900       -1.58390
H         -0.91580       -0.93380        1.28330
H         -1.89750        0.65850       -1.08890
H         -0.66780        1.79410        1.46220
H         -0.23860        3.36660       -0.39260
H         -3.50030        0.13460        0.39100
H         -2.30560       -2.41160        0.60820
H          0.27810       -2.85420       -0.11530
H          4.05070        0.24330        0.74670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

