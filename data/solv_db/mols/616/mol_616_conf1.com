%nproc=16
%mem=2GB
%chk=mol_616_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51020        0.03240        0.07400
C         -1.08980        0.25110       -0.30270
C         -0.27470       -0.95920        0.14060
C          1.12310       -0.69950       -0.40970
C          1.61890        0.49540        0.41660
C          0.72550        1.65090        0.03830
O         -0.58080        1.35820        0.32750
O          1.10350        2.75330        0.79530
O          2.94890        0.74820        0.22730
O          1.94980       -1.78170       -0.32090
O         -0.80430       -2.14810       -0.25920
O         -2.96780       -1.10890       -0.60140
H         -3.11390        0.87400       -0.29760
H         -2.60270       -0.07290        1.14680
H         -1.01720        0.29130       -1.40610
H         -0.23810       -0.88780        1.26310
H          1.00820       -0.29580       -1.45180
H          1.38100        0.26400        1.49430
H          0.90790        1.85470       -1.03920
H          0.97420        3.57630        0.26480
H          3.46440       -0.00100        0.60510
H          2.48230       -1.81850       -1.17890
H         -0.85610       -2.77160        0.53390
H         -3.63220       -1.60470       -0.06000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_616_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

