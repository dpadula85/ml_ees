%nproc=16
%mem=2GB
%chk=mol_071_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.19890        1.13420       -0.05930
C          1.25140       -0.38190       -0.17740
C          0.00900       -0.88110        0.53820
C         -1.17240       -0.41020       -0.31070
C         -1.08920        1.08720       -0.37750
O          0.12700        1.56130       -0.82150
O         -1.43340        1.57590        0.89030
O         -2.34550       -0.78710        0.29830
O         -0.05920       -2.24710        0.62190
O          2.41550       -0.90330        0.32760
H          0.99110        1.39790        0.98680
H          2.11830        1.57750       -0.43280
H          1.15470       -0.59680       -1.27250
H         -0.04050       -0.35040        1.50160
H         -1.02420       -0.81260       -1.32110
H         -1.85800        1.42720       -1.11210
H         -1.14220        2.52240        0.90600
H         -2.96020       -1.10900       -0.42190
H          0.82630       -2.69790        0.66770
H          3.03270       -1.10620       -0.43140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

