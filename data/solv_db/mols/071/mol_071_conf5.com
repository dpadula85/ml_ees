%nproc=16
%mem=2GB
%chk=mol_071_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.02930       -1.42720       -0.24160
C         -1.34230        0.04810       -0.21420
C         -0.30300        0.76820        0.60710
C          1.03890        0.52200       -0.06030
C          1.20110       -0.93770       -0.46030
O          0.25160       -1.70480        0.20510
O          2.47290       -1.40800       -0.13280
O          2.10250        0.85240        0.77970
O         -0.51820        2.13850        0.59840
O         -2.62680        0.20440        0.26440
H         -1.21660       -1.82340       -1.26140
H         -1.73730       -1.98220        0.42490
H         -1.26780        0.45570       -1.24910
H         -0.23460        0.40450        1.64430
H          1.07730        1.15780       -0.97630
H          1.03670       -1.08050       -1.55520
H          2.48470       -1.59090        0.83170
H          2.13680        1.84240        0.85930
H         -0.61140        2.42350       -0.35130
H         -2.91530        1.13720        0.35040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

