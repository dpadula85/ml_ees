%nproc=16
%mem=2GB
%chk=mol_071_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.11560        1.22890       -0.27820
C         -1.34970       -0.02920        0.46420
C         -0.12430       -0.82340        0.78030
C          0.94840       -0.69180       -0.30510
C          1.08280        0.82420       -0.50590
O         -0.07130        1.21430       -1.16550
O          2.24410        1.14240       -1.16040
O          0.44760       -1.23090       -1.49030
O          0.46920       -0.28200        1.94380
O         -2.29060       -0.85930       -0.16350
H         -2.04600        1.45610       -0.87100
H         -0.93520        2.08870        0.42590
H         -1.82370        0.24080        1.45370
H         -0.38110       -1.88350        0.90400
H          1.87270       -1.15060        0.03350
H          1.06780        1.28310        0.52230
H          2.95970        1.28890       -0.48000
H          0.70280       -2.18920       -1.55900
H          0.67060       -0.99270        2.58520
H         -2.32830       -0.63480       -1.13390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

