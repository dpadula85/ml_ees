%nproc=16
%mem=2GB
%chk=mol_071_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.78780       -1.44770       -0.07370
C          1.31360       -0.13290        0.30140
C          0.45620        1.04660        0.02570
C         -0.99530        0.77270       -0.10810
C         -1.38780       -0.65150        0.15820
O         -0.53720       -1.59200       -0.27630
O         -2.61990       -0.86160       -0.49900
O         -1.49090        1.13480       -1.38210
O          0.65680        2.03300        1.02150
O          2.55130        0.08670       -0.35390
H          1.30170       -1.81150       -1.00820
H          1.10170       -2.19900        0.70460
H          1.59410       -0.10560        1.39750
H          0.79010        1.56170       -0.92410
H         -1.58570        1.40550        0.60890
H         -1.63320       -0.76980        1.25260
H         -2.71220       -1.84770       -0.65770
H         -2.46250        1.37410       -1.32630
H          1.57920        2.35360        1.04170
H          3.29230       -0.34970        0.09730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

