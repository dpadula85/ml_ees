%nproc=16
%mem=2GB
%chk=mol_071_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.51600        1.63970       -0.14550
C         -1.25800        0.35090        0.11250
C         -0.57600       -0.84150       -0.49680
C          0.82480       -0.92100        0.06490
C          1.30080        0.47730        0.44730
O          0.82370        1.40080       -0.46290
O          2.70190        0.48710        0.47270
O          1.71320       -1.52780       -0.80960
O         -1.26250       -1.97680       -0.08810
O         -2.57210        0.50200       -0.28620
H         -0.94940        2.19600       -0.98580
H         -0.50700        2.26650        0.77280
H         -1.27810        0.19360        1.22360
H         -0.58110       -0.79950       -1.59680
H          0.85450       -1.51300        1.01730
H          0.91430        0.72790        1.45720
H          3.05640        1.05720        1.19830
H          1.26150       -1.65860       -1.68590
H         -1.13290       -2.06050        0.88040
H         -2.81790       -0.00040       -1.08930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_071_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

