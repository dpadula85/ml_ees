%nproc=16
%mem=2GB
%chk=mol_495_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.52250        0.69700        0.15960
C          1.05120        0.46080        0.43340
C          0.35000       -0.08570       -0.78570
C         -1.11240       -0.28680       -0.40840
C         -1.74660        1.02140        0.01000
C         -1.27580       -1.26590        0.70540
O          0.94800       -1.29770       -1.12000
H          2.97340       -0.15010       -0.38210
H          3.02190        0.77670        1.12940
H          2.69830        1.61040       -0.40590
H          0.62500        1.44000        0.73620
H          1.01710       -0.30230        1.21930
H          0.43440        0.57500       -1.66960
H         -1.68190       -0.65240       -1.29070
H         -1.40080        1.38160        0.98210
H         -2.84420        0.80270        0.12700
H         -1.68390        1.77380       -0.80240
H         -1.09460       -0.85320        1.70930
H         -2.35680       -1.59240        0.70190
H         -0.70420       -2.21320        0.53830
H          0.25940       -1.83960       -1.58690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

