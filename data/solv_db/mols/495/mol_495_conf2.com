%nproc=16
%mem=2GB
%chk=mol_495_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65180        0.22370       -0.10810
C          1.24140        0.02240        0.43610
C          0.29530        0.21930       -0.68960
C         -1.14980        0.06540       -0.36710
C         -1.57020       -1.25370        0.15770
C         -1.63870        1.16110        0.55800
O          0.67070       -0.62190       -1.73640
H          2.87720        1.28320       -0.28930
H          2.65000       -0.27000       -1.11480
H          3.40180       -0.31230        0.49070
H          1.23660       -0.98340        0.87410
H          1.12700        0.81570        1.23230
H          0.43750        1.26080       -1.07420
H         -1.70790        0.22800       -1.33780
H         -2.50330       -1.16250        0.75390
H         -0.83650       -1.77450        0.78450
H         -1.82500       -1.96680       -0.67700
H         -2.33810        0.75790        1.32390
H         -2.18320        1.96390        0.01920
H         -0.80860        1.65740        1.09980
H         -0.02790       -1.31390       -1.80100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

