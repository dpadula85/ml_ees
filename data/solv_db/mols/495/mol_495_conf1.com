%nproc=16
%mem=2GB
%chk=mol_495_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73610       -0.06020       -0.12350
C          1.41130        0.63070        0.04510
C          0.26580       -0.25360       -0.39280
C         -1.06680        0.39660       -0.24070
C         -2.12220       -0.58400       -0.70960
C         -1.38660        0.76480        1.18750
O          0.29000       -1.41200        0.35310
H          3.47830        0.57360       -0.63830
H          3.17520       -0.32340        0.86400
H          2.63330       -0.99820       -0.70430
H          1.28350        0.87690        1.13790
H          1.45140        1.58630       -0.48090
H          0.41830       -0.47770       -1.46500
H         -1.17520        1.30260       -0.90560
H         -1.77140       -1.17090       -1.58120
H         -2.39800       -1.24100        0.13470
H         -3.00030       -0.01420       -1.03190
H         -2.48620        0.97450        1.22260
H         -0.88190        1.72440        1.41810
H         -1.08680       -0.06110        1.84330
H          0.23210       -2.23400       -0.19530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

