%nproc=16
%mem=2GB
%chk=mol_495_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33320       -0.18010       -0.35730
C          1.41730       -0.22580        0.81860
C          0.12030        0.49830        0.62270
C         -0.70760       -0.01810       -0.50340
C         -1.07800       -1.47490       -0.30810
C         -2.01410        0.76570       -0.51230
O          0.38000        1.84410        0.43120
H          3.33590       -0.53420       -0.01170
H          1.99920       -0.90820       -1.12010
H          2.48400        0.83240       -0.75310
H          1.94420        0.28680        1.66820
H          1.24300       -1.25090        1.13670
H         -0.46650        0.38930        1.55750
H         -0.24010        0.14410       -1.50040
H         -0.26030       -2.09590        0.04770
H         -1.46110       -1.91090       -1.25390
H         -1.91530       -1.53080        0.42470
H         -2.08110        1.46620        0.34920
H         -2.11700        1.39080       -1.42420
H         -2.89730        0.11410       -0.46260
H         -0.01870        2.39800        1.15070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

