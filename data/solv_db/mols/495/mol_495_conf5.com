%nproc=16
%mem=2GB
%chk=mol_495_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.60840       -0.67070        0.24470
C          1.17330       -0.83370       -0.23440
C          0.38970        0.35670        0.27080
C         -1.04510        0.25610       -0.17090
C         -1.63670       -0.99980        0.38310
C         -1.80800        1.46700        0.34840
O          0.93900        1.52270       -0.20640
H          3.07880        0.24750       -0.09740
H          3.16960       -1.54500       -0.18150
H          2.64120       -0.75200        1.34610
H          0.81190       -1.78440        0.16560
H          1.17400       -0.86280       -1.32890
H          0.40420        0.34980        1.37830
H         -1.13340        0.27830       -1.27670
H         -1.87450       -1.74930       -0.40690
H         -2.57830       -0.75150        0.93300
H         -0.97250       -1.51850        1.11800
H         -1.68030        1.52660        1.43380
H         -1.42790        2.39740       -0.09820
H         -2.87710        1.33750        0.07590
H          0.64350        1.72810       -1.12520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_495_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

