%nproc=16
%mem=2GB
%chk=mol_171_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.46650       -1.20380        0.43650
C         -1.22390       -0.64320        0.22950
C         -1.18260        0.67490       -0.14340
C         -2.35200        1.40050       -0.30160
C         -3.58220        0.79630       -0.08290
C         -3.64960       -0.52750        0.29270
Cl        -5.17750       -1.33190        0.57960
Cl        -5.04300        1.73500       -0.28960
O          0.03640        1.29950       -0.36750
C          1.23190        0.58950       -0.21470
C          1.17460       -0.74620        0.16390
C          2.37810       -1.40160        0.30100
C          3.59700       -0.79730        0.08260
C          3.60590        0.54820       -0.29680
C          2.43060        1.24150       -0.44570
Cl         2.40770        2.92660       -0.91960
Cl         5.15250        1.33700       -0.58020
Cl         5.12230       -1.64240        0.26050
Cl         2.36420       -3.08180        0.77490
O         -0.04710       -1.36060        0.38520
H         -2.49490       -2.24120        0.72990
H         -2.28200        2.42850       -0.59400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_171_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_171_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_171_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

