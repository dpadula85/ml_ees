%nproc=16
%mem=2GB
%chk=mol_142_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.21680       -0.11320        0.61930
C          2.77430       -0.03400        0.26660
C          2.37420        0.47410       -0.95010
C          1.05890        0.56840       -1.32320
C          0.02300        0.14470       -0.47090
C          0.42670       -0.34740        0.73950
C          1.77820       -0.44900        1.11760
C         -1.34920        0.52620       -0.79750
O         -1.50120        1.35370       -1.79580
N         -2.53030        0.12260       -0.20060
C         -3.79690        0.68300       -0.70420
C         -2.62440       -1.08930        0.58100
H          4.35400       -0.11720        1.73270
H          4.79020        0.75390        0.24110
H          4.69600       -1.01530        0.17770
H          3.19040        0.80370       -1.61080
H          0.80110        0.97570       -2.29240
H         -0.30150       -0.51530        1.52610
H          2.04720       -0.85570        2.08420
H         -3.68220        1.77950       -0.86100
H         -4.07130        0.20910       -1.67990
H         -4.61440        0.47250        0.01740
H         -1.82750       -1.78750        0.27240
H         -3.62440       -1.59900        0.38320
H         -2.60770       -0.94420        1.66550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

