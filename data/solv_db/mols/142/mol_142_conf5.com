%nproc=16
%mem=2GB
%chk=mol_142_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.17890        0.41240       -0.11350
C          2.73980        0.04630       -0.04480
C          1.87510        0.47960       -1.01530
C          0.52180        0.13210       -0.94440
C         -0.00440       -0.61650        0.07070
C          0.93350       -1.04250        1.03500
C          2.26500       -0.72730        0.98910
C         -1.42350       -0.73010        0.37290
O         -1.74320       -1.57490        1.30010
N         -2.47590       -0.00860       -0.20990
C         -2.39930        0.99450       -1.23540
C         -3.84730       -0.33960        0.26930
H          4.22450        1.44390       -0.53040
H          4.60260        0.43590        0.91700
H          4.72890       -0.31580       -0.73970
H          2.18800        1.09450       -1.85520
H         -0.04650        0.24750       -1.85690
H          0.54620       -1.65140        1.83350
H          2.96580       -1.07910        1.76120
H         -3.35670        1.60570       -1.22470
H         -1.62160        1.77270       -1.00570
H         -2.34700        0.60270       -2.26160
H         -4.41290        0.60220        0.34520
H         -4.28740       -0.99150       -0.50450
H         -3.80440       -0.79280        1.27250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

