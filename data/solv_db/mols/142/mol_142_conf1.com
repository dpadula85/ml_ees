%nproc=16
%mem=2GB
%chk=mol_142_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.09250        0.55460        0.88700
C          2.70740        0.22150        0.43080
C          1.77890        1.18580        0.14680
C          0.49290        0.86440       -0.27580
C          0.08420       -0.43820       -0.41690
C          1.06480       -1.40110       -0.12430
C          2.33450       -1.08060        0.28620
C         -1.26510       -0.89510       -0.56450
O         -1.42740       -2.16060       -0.79240
N         -2.47250       -0.18230       -0.49010
C         -3.76080       -0.87300       -0.69270
C         -2.55410        1.14280        0.05850
H          4.56480       -0.34950        1.28720
H          4.69050        1.01280        0.09890
H          4.00340        1.31850        1.70340
H          2.07800        2.23410        0.26220
H         -0.12630        1.67400       -0.67660
H          0.85030       -2.45640       -0.21630
H          3.06130       -1.84800        0.50240
H         -4.09990       -0.64050       -1.71000
H         -3.67200       -1.94620       -0.48210
H         -4.53380       -0.47570        0.00250
H         -1.85100        1.34010        0.89810
H         -2.47050        1.87360       -0.77530
H         -3.56990        1.32490        0.53750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

