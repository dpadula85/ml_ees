%nproc=16
%mem=2GB
%chk=mol_142_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.22620        0.42860        0.03100
C          2.76970        0.11820       -0.07910
C          2.32540       -1.04590       -0.65560
C          0.99320       -1.28140       -0.73880
C         -0.00750       -0.41830       -0.27500
C          0.47000        0.72980        0.30660
C          1.84380        1.01080        0.40430
C         -1.37560       -0.86830       -0.14690
O         -1.55830       -2.12620       -0.35680
N         -2.51460       -0.12460        0.15970
C         -3.81310       -0.80900        0.26520
C         -2.58350        1.29240       -0.05820
H          4.81890       -0.23270       -0.62830
H          4.33420        1.49340       -0.25850
H          4.54980        0.36900        1.10150
H          3.08480       -1.73840       -1.02470
H          0.65130       -2.19460       -1.20470
H         -0.15950        1.39840        0.86980
H          2.16570        1.93370        0.85970
H         -4.13020       -0.68540        1.32180
H         -4.52830       -0.39210       -0.46220
H         -3.69130       -1.90030        0.10630
H         -3.60660        1.59610       -0.45890
H         -1.80560        1.59320       -0.79000
H         -2.45890        1.85370        0.88370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

