%nproc=16
%mem=2GB
%chk=mol_142_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.16290        0.28210        0.55240
C          2.75130        0.05560        0.16250
C          1.79850       -0.23970        1.10790
C          0.47020       -0.45130        0.74110
C         -0.00020       -0.37020       -0.54370
C          1.00450       -0.07660       -1.49450
C          2.31120        0.12400       -1.13890
C         -1.39030       -0.26970       -0.94080
O         -1.63070       -0.32420       -2.20510
N         -2.49250       -0.10610       -0.10270
C         -3.83510       -0.04520       -0.75380
C         -2.44660        0.28320        1.28770
H          4.63980       -0.70190        0.72140
H          4.26480        0.92860        1.44990
H          4.69860        0.77580       -0.27150
H          2.04090       -0.31790        2.15880
H         -0.19650       -0.94080        1.46420
H          0.66210       -0.03040       -2.52950
H          3.05790        0.35140       -1.91560
H         -3.87860       -0.84020       -1.51560
H         -3.91390        0.92970       -1.22990
H         -4.60430       -0.20990        0.01310
H         -1.61700        0.95810        1.53500
H         -2.48680       -0.63940        1.90180
H         -3.37000        0.87500        1.54590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_142_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

