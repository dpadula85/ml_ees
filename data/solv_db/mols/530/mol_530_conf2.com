%nproc=16
%mem=2GB
%chk=mol_530_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35130        0.21570        0.60040
C         -1.59480       -0.15640       -0.67910
C         -0.15780       -0.09750       -0.36120
C          0.51280        1.08320       -0.53960
C          1.87850        1.15620       -0.24250
C          2.47510        0.01870        0.21870
C          1.84130       -1.19290        0.41440
C          0.47410       -1.22120        0.10420
H         -3.13820       -0.51620        0.82770
H         -2.73350        1.24830        0.44430
H         -1.67240        0.23730        1.47870
H         -1.88560        0.52360       -1.51560
H         -1.92010       -1.19450       -0.94470
H          0.03750        1.99480       -0.90840
H          2.41710        2.09240       -0.38230
H          3.53460        0.05250        0.45560
H          2.34830       -2.07580        0.78260
H         -0.06570       -2.16810        0.24670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

