%nproc=16
%mem=2GB
%chk=mol_530_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51180        0.08780       -0.26630
C         -1.50310       -0.60090        0.61010
C         -0.10720       -0.20730        0.30020
C          0.60100       -0.93180       -0.63790
C          1.90920       -0.61680       -0.97370
C          2.54900        0.44610       -0.36980
C          1.83120        1.16200        0.56540
C          0.52740        0.85570        0.90650
H         -2.05350        0.79780       -0.97980
H         -3.25740        0.64530        0.33750
H         -3.09080       -0.63860       -0.87750
H         -1.71580       -0.48320        1.69770
H         -1.58660       -1.70610        0.42000
H          0.11130       -1.78280       -1.13150
H          2.47000       -1.17770       -1.70580
H          3.56690        0.74020       -0.58940
H          2.29960        2.00160        1.05940
H         -0.03940        1.40860        1.63470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

