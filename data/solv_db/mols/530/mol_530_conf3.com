%nproc=16
%mem=2GB
%chk=mol_530_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49310        0.15470       -0.42040
C         -1.57620       -0.12090        0.75280
C         -0.14930       -0.08500        0.34990
C          0.56650        1.09360        0.40120
C          1.90110        1.15960        0.03140
C          2.56380        0.03430       -0.40500
C          1.85980       -1.14630       -0.46000
C          0.51900       -1.20940       -0.08780
H         -2.33470       -0.67120       -1.14310
H         -3.53290        0.10140       -0.04200
H         -2.27430        1.13200       -0.88230
H         -1.70590        0.65170        1.54710
H         -1.80450       -1.11230        1.16260
H          0.05730        2.00370        0.74630
H          2.45850        2.09300        0.07460
H          3.60670        0.09710       -0.69170
H          2.37010       -2.04260       -0.80250
H         -0.03160       -2.13340       -0.13110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

