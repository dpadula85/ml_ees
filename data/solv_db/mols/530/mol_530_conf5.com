%nproc=16
%mem=2GB
%chk=mol_530_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51080        0.19780       -0.14260
C         -1.48730       -0.52350        0.66760
C         -0.09670       -0.21600        0.27820
C          0.61370        0.82460        0.85130
C          1.90900        1.08190        0.46310
C          2.53160        0.32060       -0.49680
C          1.79700       -0.71370       -1.05290
C          0.49760       -0.99940       -0.68760
H         -2.16970        0.31750       -1.20500
H         -2.71870        1.17330        0.33700
H         -3.48050       -0.37050       -0.13770
H         -1.64710       -1.61360        0.60970
H         -1.60330       -0.19840        1.74000
H          0.15190        1.45020        1.61650
H          2.43750        1.89810        0.92630
H          3.55090        0.53820       -0.78910
H          2.27290       -1.33220       -1.81860
H         -0.04790       -1.83510       -1.15960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

