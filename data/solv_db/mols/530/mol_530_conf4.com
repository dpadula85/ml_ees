%nproc=16
%mem=2GB
%chk=mol_530_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.48100        0.30850        0.31020
C          1.57090       -0.56300       -0.48710
C          0.13650       -0.25910       -0.23910
C         -0.55350        0.68190       -0.99990
C         -1.88390        0.97100       -0.77790
C         -2.56750        0.31740        0.22420
C         -1.91380       -0.63200        1.00900
C         -0.57440       -0.89730        0.75700
H          2.18490        1.37220        0.31460
H          2.49030       -0.03600        1.36960
H          3.51570        0.18490       -0.07210
H          1.79020       -1.63090       -0.20510
H          1.74590       -0.45390       -1.57760
H          0.02940        1.17880       -1.78740
H         -2.38660        1.70730       -1.38840
H         -3.60270        0.55440        0.38340
H         -2.42470       -1.16210        1.80400
H         -0.03770       -1.64190        1.36260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_530_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

