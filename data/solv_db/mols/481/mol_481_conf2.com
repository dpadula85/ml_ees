%nproc=16
%mem=2GB
%chk=mol_481_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.47390        1.31190        0.29470
N         -2.28760        0.44220        0.27980
C         -2.55300       -0.96290        0.44820
C         -1.09640        0.95470       -0.21970
O         -1.17580        2.17540       -0.66310
C          0.20610        0.29310       -0.31610
C          1.25360        0.94480       -0.98910
C          2.55960        0.53300       -0.90180
C          2.92230       -0.55550       -0.14250
C          1.91750       -1.20610        0.52620
C          0.57730       -0.78490        0.44550
N          4.28060       -0.96520       -0.06680
O          4.61940       -1.94630        0.62010
O          5.27110       -0.29930       -0.74460
H         -3.19430        2.34330        0.60130
H         -4.23680        0.93400        0.98690
H         -3.92770        1.38050       -0.71490
H         -3.62760       -1.20910        0.17470
H         -1.97690       -1.59570       -0.28860
H         -2.44730       -1.33620        1.47420
H          0.95350        1.79490       -1.60240
H          3.34710        1.07170       -1.44310
H          2.19740       -2.06170        1.12250
H         -0.10810       -1.25690        1.11860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

