%nproc=16
%mem=2GB
%chk=mol_481_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61900        0.68300        0.50240
N         -2.30700        0.00990        0.32980
C         -2.28620       -1.31890       -0.22470
C         -1.19330        0.82200        0.40420
O         -1.40080        2.08640        0.57810
C          0.22000        0.49700        0.31150
C          0.66230       -0.79450        0.26900
C          1.95360       -1.09950       -0.10740
C          2.87400       -0.13860       -0.44380
C          2.44110        1.16390       -0.38620
C          1.15660        1.48190       -0.02090
N          4.19310       -0.49100       -0.82650
O          4.52690       -1.67510       -0.86060
O          5.09630        0.49410       -1.15890
H         -4.42900        0.04830        0.14200
H         -3.73090        0.95270        1.57040
H         -3.56220        1.62020       -0.06840
H         -3.27090       -1.49050       -0.77440
H         -2.15090       -2.12810        0.51630
H         -1.51370       -1.41280       -1.00610
H          0.09350       -1.60820        0.74580
H          2.25400       -2.15360       -0.13870
H          3.17360        1.93610       -0.64690
H          0.81880        2.51540        0.03090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

