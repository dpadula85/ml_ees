%nproc=16
%mem=2GB
%chk=mol_481_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.65500        0.41730        0.37500
N         -2.28050       -0.13380        0.14940
C         -2.18720       -1.27790       -0.73730
C         -1.23530        0.67630        0.59710
O         -1.55190        1.81410        1.12200
C          0.18340        0.39920        0.52950
C          0.64990       -0.81610        0.11960
C          1.98620       -1.00850       -0.23840
C          2.87280        0.04040       -0.16390
C          2.41070        1.26330        0.26710
C          1.10410        1.46260        0.61100
N          4.23860       -0.15750       -0.53050
O          4.66730       -1.25630       -0.92090
O          5.07830        0.92010       -0.43750
H         -4.22430        0.22050       -0.53060
H         -4.04230       -0.11260        1.28080
H         -3.60420        1.49710        0.60060
H         -1.39340       -1.13570       -1.49180
H         -2.14600       -2.24700       -0.21740
H         -3.13180       -1.34890       -1.36790
H          0.08720       -1.73450        0.29090
H          2.33780       -1.98400       -0.57780
H          3.11960        2.10090        0.33440
H          0.71580        2.40110        0.95540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

