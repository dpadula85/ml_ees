%nproc=16
%mem=2GB
%chk=mol_481_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.57370        0.86940       -0.74690
N         -2.22070        0.69540       -0.15620
C         -2.21480        0.64670        1.28680
C         -1.16490        0.38300       -1.00210
O         -1.44330        0.24300       -2.25490
C          0.23200        0.17240       -0.67180
C          0.77610        0.36640        0.57570
C          2.03240       -0.11260        0.94360
C          2.82090       -0.80620        0.05710
C          2.30880       -0.99930       -1.20250
C          1.06820       -0.53160       -1.55730
N          4.11060       -1.29900        0.44090
O          4.79730       -1.91620       -0.36530
O          4.60780       -1.09760        1.70390
H         -4.16600        0.02720       -0.33210
H         -3.52320        0.82040       -1.84480
H         -3.99040        1.82100       -0.36010
H         -1.61000       -0.22300        1.67950
H         -1.94950        1.60680        1.75990
H         -3.24250        0.41940        1.71060
H          0.30840        1.09170        1.23340
H          2.43180        0.04660        1.94660
H          2.93370       -1.54020       -1.90370
H          0.67110       -0.68340       -2.57090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

