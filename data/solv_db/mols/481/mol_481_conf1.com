%nproc=16
%mem=2GB
%chk=mol_481_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61550        0.68670       -0.47430
N         -2.30740        0.25080        0.04600
C         -2.31020       -0.93210        0.84140
C         -1.15130        0.81810       -0.48660
O         -1.36150        1.67500       -1.43050
C          0.23170        0.57960       -0.14610
C          0.66640       -0.19700        0.88700
C          1.95580       -0.66460        1.05400
C          2.88640       -0.30350        0.10540
C          2.49840        0.49380       -0.94840
C          1.20700        0.93570       -1.08850
N          4.21070       -0.75520        0.23180
O          5.04820       -0.42530       -0.62950
O          4.59800       -1.54760        1.27860
H         -4.33740       -0.13850       -0.50470
H         -3.97310        1.47710        0.22730
H         -3.50290        1.15140       -1.45750
H         -1.52340       -1.67230        0.61560
H         -3.26850       -1.52330        0.70250
H         -2.29810       -0.71260        1.94460
H          0.01280       -0.28980        1.77180
H          2.22270       -1.29890        1.91110
H          3.20160        0.81150       -1.72200
H          0.90960        1.58100       -1.93150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_481_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

