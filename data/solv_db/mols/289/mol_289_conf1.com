%nproc=16
%mem=2GB
%chk=mol_289_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.13150        1.89880       -0.03980
C          1.24470        1.11980       -0.29750
C          1.15320       -0.24600       -0.05470
C         -0.08740       -0.84500        0.10990
C         -1.23060       -0.07930        0.25110
C         -1.05540        1.29620        0.35840
C         -2.52800       -0.61080       -0.25470
F         -3.53020        0.30860       -0.09570
F         -2.92040       -1.75460        0.41250
F         -2.48770       -0.94060       -1.60050
C          2.42940       -0.96060        0.27650
F          3.21950       -0.21880        1.14900
F          3.20090       -1.13950       -0.86260
F          2.17490       -2.19570        0.83790
H          0.20440        2.96870       -0.09400
H          2.06790        1.48980       -0.91400
H         -0.12780       -1.92540        0.00060
H         -1.85900        1.83420        0.81750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

