%nproc=16
%mem=2GB
%chk=mol_289_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.09440        1.95120        0.00770
C         -1.24450        1.20490       -0.12990
C         -1.16110       -0.16530       -0.05670
C          0.02940       -0.81510        0.10600
C          1.18550       -0.06990        0.19980
C          1.12510        1.32340        0.16350
C          2.49730       -0.73600        0.04720
F          3.49620        0.17060       -0.33160
F          2.46360       -1.67810       -0.97370
F          2.86310       -1.39090        1.19290
C         -2.42990       -0.95730       -0.07780
F         -2.52360       -1.78020        1.03140
F         -2.38830       -1.72530       -1.21900
F         -3.52440       -0.09230       -0.02820
H         -0.17500        3.04800       -0.03910
H         -2.21990        1.68240       -0.39130
H          0.09460       -1.90550        0.19520
H          2.00620        1.93560        0.30350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

