%nproc=16
%mem=2GB
%chk=mol_289_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.02280        1.90800       -0.39720
C          1.21390        1.21430       -0.30760
C          1.17110       -0.17040       -0.25160
C         -0.04540       -0.79960       -0.20650
C         -1.23020       -0.08790       -0.33350
C         -1.21310        1.29210       -0.32920
C         -2.45540       -0.81870        0.07270
F         -2.39640       -2.05920       -0.54510
F         -2.32840       -0.92450        1.44940
F         -3.60950       -0.20660       -0.31920
C          2.40990       -0.85550        0.18410
F          2.85290       -0.27620        1.35420
F          2.14570       -2.19410        0.41730
F          3.40660       -0.84000       -0.78410
H          0.07020        2.98290       -0.43970
H          2.10100        1.75020        0.07750
H         -0.05050       -1.78240        0.30650
H         -2.06510        1.86770        0.05200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

