%nproc=16
%mem=2GB
%chk=mol_289_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.10080        1.93180       -0.27960
C         -1.21670        1.19670        0.05260
C         -1.18450       -0.15410        0.06860
C          0.04090       -0.77410        0.14230
C          1.16710       -0.02450        0.02310
C          1.14400        1.31290       -0.27640
C          2.47820       -0.71820        0.17340
F          3.21450       -0.04990        1.14270
F          3.13290       -0.62230       -1.04530
F          2.31220       -2.01410        0.59410
C         -2.39540       -1.00480       -0.00190
F         -3.21030       -0.61610       -1.04420
F         -2.04720       -2.32660       -0.23070
F         -3.08720       -0.82880        1.16320
H         -0.24690        2.89210       -0.78870
H         -2.13810        1.71310        0.32830
H          0.09900       -1.80350        0.48730
H          2.03840        1.89050       -0.50870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

