%nproc=16
%mem=2GB
%chk=mol_289_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.04360        1.96880       -0.36410
C          1.13160        1.26290       -0.50280
C          1.15830       -0.10440       -0.35040
C         -0.02150       -0.78830       -0.12400
C         -1.18380       -0.10590        0.04420
C         -1.22580        1.27560       -0.05190
C         -2.41780       -0.87650        0.50920
F         -2.18570       -1.28780        1.81250
F         -3.50300       -0.09730        0.30800
F         -2.38880       -2.02070       -0.27580
C          2.42990       -0.81140       -0.07500
F          2.75260       -1.71880       -1.02200
F          3.48470        0.08420        0.09170
F          2.23750       -1.56060        1.10770
H         -0.12640        3.04670       -0.54700
H          2.06820        1.77330       -0.62380
H         -0.03650       -1.86470       -0.12270
H         -2.12990        1.82480        0.18600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_289_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

