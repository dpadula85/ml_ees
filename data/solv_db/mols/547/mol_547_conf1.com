%nproc=16
%mem=2GB
%chk=mol_547_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.72570        0.60360       -0.36010
C         -1.88700       -0.57190       -0.67090
C         -1.02640       -1.08500        0.42760
C         -0.02120       -0.13730        0.97790
C          0.95510        0.34360       -0.02650
C          2.24570        0.10550        0.16010
C          3.30270        0.54420       -0.78530
H         -2.43840        1.18350        0.52620
H         -2.73730        1.31600       -1.21740
H         -3.80900        0.32820       -0.20890
H         -1.24070       -0.36190       -1.57440
H         -2.54760       -1.40670       -0.99260
H         -1.64830       -1.45400        1.26880
H         -0.47550       -1.97090        0.04430
H          0.54010       -0.67250        1.76970
H         -0.44420        0.75930        1.46320
H          0.65870        0.87910       -0.90280
H          2.58810       -0.44170        1.04800
H          2.85730        0.85780       -1.75050
H          3.80890        1.46060       -0.36530
H          4.04480       -0.27950       -0.90360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

