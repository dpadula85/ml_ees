%nproc=16
%mem=2GB
%chk=mol_547_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.36030        0.15760       -0.13240
C         -2.26840       -0.43120        0.74520
C         -0.94480       -0.12980        0.06810
C          0.22130       -0.66810        0.85000
C          1.46120       -0.32220        0.10680
C          2.40810        0.42730        0.64740
C          3.62220        0.73680       -0.14640
H         -4.22130       -0.52680       -0.10200
H         -3.04370        0.22840       -1.19830
H         -3.61060        1.19420        0.18190
H         -2.32830        0.04660        1.74160
H         -2.38760       -1.51550        0.89330
H         -0.87530        0.97790        0.00060
H         -0.94830       -0.58230       -0.92250
H          0.24630       -0.37190        1.89670
H          0.15810       -1.78780        0.83650
H          1.63520       -0.67080       -0.89940
H          2.33550        0.82910        1.65270
H          3.46370        1.59180       -0.83530
H          4.45880        0.98540        0.53810
H          3.97840       -0.16870       -0.71750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

