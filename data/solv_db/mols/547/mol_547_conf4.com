%nproc=16
%mem=2GB
%chk=mol_547_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.45930       -0.38970       -0.11860
C          2.22050        0.40100       -0.47100
C          1.00510       -0.11860        0.27100
C         -0.19250        0.71070       -0.12410
C         -1.43320        0.29430        0.53730
C         -2.45130       -0.10330       -0.21810
C         -3.74510       -0.54190        0.37240
H          4.34330        0.18620       -0.45950
H          3.36720       -1.37120       -0.64710
H          3.55050       -0.57260        0.95130
H          2.02270        0.36520       -1.54990
H          2.34810        1.46870       -0.13900
H          0.85300       -1.18940        0.13560
H          1.20540        0.07160        1.35870
H         -0.33620        0.70850       -1.21930
H         -0.00100        1.77610        0.20000
H         -1.58440        0.28700        1.61640
H         -2.38020       -0.12450       -1.30500
H         -4.55760       -0.18570       -0.28680
H         -3.87720       -0.02980        1.35960
H         -3.81640       -1.64250        0.46410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

