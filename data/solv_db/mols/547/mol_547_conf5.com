%nproc=16
%mem=2GB
%chk=mol_547_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.25740        0.42720        0.39200
C         -2.14000        0.17650       -0.57950
C         -0.77220        0.23260        0.09150
C          0.19710       -0.04100       -1.03830
C          1.59950       -0.03880       -0.63390
C          2.00480        0.18240        0.61320
C          3.46110        0.16140        0.91380
H         -4.16820        0.61820       -0.21650
H         -3.41960       -0.50620        0.96330
H         -2.99800        1.29110        1.01320
H         -2.19300       -0.81480       -1.07030
H         -2.11830        0.94580       -1.37870
H         -0.59080        1.23380        0.54290
H         -0.66510       -0.58900        0.81620
H         -0.02020        0.66840       -1.86520
H         -0.06950       -1.07040       -1.41850
H          2.35290       -0.22770       -1.39000
H          1.26580        0.37780        1.40730
H          4.03030        0.66700        0.08580
H          3.71290        0.53610        1.89810
H          3.78790       -0.91160        0.85360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

