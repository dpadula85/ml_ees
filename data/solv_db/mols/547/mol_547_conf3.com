%nproc=16
%mem=2GB
%chk=mol_547_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.07580        0.88830        0.33630
C         -2.21490       -0.28080       -0.09150
C         -0.72700        0.08200        0.00390
C          0.04570       -1.13560       -0.43890
C          1.51570       -0.90860       -0.39070
C          2.02300        0.25440        0.00580
C          3.51250        0.48450        0.05470
H         -3.38100        0.70520        1.38940
H         -2.46520        1.81480        0.28550
H         -3.97580        0.94800       -0.30760
H         -2.41600       -1.19270        0.51820
H         -2.47810       -0.54580       -1.13550
H         -0.56700        0.94890       -0.64050
H         -0.48880        0.36590        1.04840
H         -0.21980       -1.36480       -1.49100
H         -0.28210       -1.98760        0.17760
H          2.20170       -1.69530       -0.68080
H          1.32300        1.03410        0.29370
H          3.99830       -0.27350       -0.61050
H          3.91120        0.33670        1.07720
H          3.76050        1.52190       -0.23400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_547_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

