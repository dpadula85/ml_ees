%nproc=16
%mem=2GB
%chk=mol_488_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.35630        1.28760        0.32480
C          2.45450       -0.02640       -0.41540
C          1.21600       -0.84130       -0.11440
O          0.05130       -0.18780       -0.51530
C         -1.00750       -1.03440       -0.18580
C         -2.34090       -0.44420       -0.56880
C         -2.58440        0.86690        0.13330
H          2.60140        2.11480       -0.39250
H          1.33630        1.41850        0.74260
H          3.04920        1.33190        1.18800
H          3.31270       -0.60350        0.01820
H          2.64260        0.08580       -1.48370
H          1.11890       -1.03350        0.97250
H          1.26320       -1.84170       -0.59710
H         -0.86420       -1.96770       -0.76780
H         -1.04010       -1.22050        0.91110
H         -2.47250       -0.33090       -1.65460
H         -3.11880       -1.15380       -0.19900
H         -2.92000        0.63760        1.18130
H         -1.67830        1.49250        0.21240
H         -3.37550        1.45010       -0.37440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

