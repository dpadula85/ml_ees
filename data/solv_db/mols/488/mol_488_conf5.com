%nproc=16
%mem=2GB
%chk=mol_488_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.14620        0.08040        0.24080
C         -2.22370       -1.00160       -0.23820
C         -0.86420       -0.42260       -0.65770
O         -0.30650        0.19730        0.42450
C          0.90990        0.77850        0.24200
C          1.96370       -0.21460       -0.18810
C          3.26020        0.56440       -0.35130
H         -2.66070        1.07680        0.27680
H         -4.03460        0.19650       -0.41680
H         -3.50410       -0.20180        1.25600
H         -2.04860       -1.79420        0.52820
H         -2.72170       -1.50980       -1.08930
H         -0.22640       -1.31580       -0.92580
H         -0.97680        0.19710       -1.55660
H          1.24880        1.16860        1.23700
H          0.90590        1.66000       -0.42940
H          2.09230       -0.98820        0.58990
H          1.70530       -0.72650       -1.14660
H          3.03350        1.48880       -0.90140
H          3.62360        0.82970        0.65030
H          3.97060       -0.06280       -0.93010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

