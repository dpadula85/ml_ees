%nproc=16
%mem=2GB
%chk=mol_488_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.77140        0.51850        0.63290
C         -1.61610       -0.45650        0.45240
C         -1.09730       -0.22520       -0.95090
O         -0.05820       -1.08370       -1.17310
C          1.02230       -0.99280       -0.33630
C          1.61160        0.39580       -0.43340
C          2.82110        0.54030        0.46870
H         -2.33570        1.52890        0.58930
H         -3.52450        0.34080       -0.14110
H         -3.21450        0.39460        1.63710
H         -1.92240       -1.50620        0.53440
H         -0.80410       -0.19090        1.19160
H         -1.94970       -0.53160       -1.63540
H         -0.88190        0.84950       -1.15380
H          0.83060       -1.26710        0.72290
H          1.78720       -1.72640       -0.73050
H          0.83460        1.15770       -0.18360
H          1.95200        0.57280       -1.47320
H          2.50910        0.28340        1.47980
H          3.59600       -0.15800        0.13450
H          3.21130        1.55600        0.36760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

