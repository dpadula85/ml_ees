%nproc=16
%mem=2GB
%chk=mol_488_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.87720       -0.84360       -0.31140
C          2.34670        0.18910        0.64190
C          0.84620        0.33440        0.56560
O          0.41110        0.71140       -0.68640
C         -0.93400        0.86010       -0.81170
C         -1.73980       -0.37910       -0.52360
C         -3.23040       -0.05770       -0.70710
H          2.11130       -1.54020       -0.68650
H          3.74450       -1.42100        0.12190
H          3.32570       -0.28390       -1.17860
H          2.60380       -0.15360        1.66850
H          2.84470        1.18400        0.53130
H          0.40160       -0.66770        0.77380
H          0.55080        1.10270        1.29660
H         -1.14350        1.16540       -1.86960
H         -1.28410        1.70080       -0.18730
H         -1.61920       -0.65640        0.54470
H         -1.52020       -1.21720       -1.17890
H         -3.41370       -0.16400       -1.80160
H         -3.41190        0.96800       -0.33500
H         -3.76700       -0.83150       -0.11720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

