%nproc=16
%mem=2GB
%chk=mol_488_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.99800        0.34550        0.66300
C         -1.93550        0.63020       -0.39970
C         -0.77560       -0.33170       -0.16100
O          0.16960       -0.06940       -1.11660
C          1.29980       -0.84140       -1.07540
C          2.00260       -0.67290        0.23460
C          2.40010        0.78400        0.43710
H         -3.13080       -0.74350        0.73240
H         -2.60620        0.70950        1.63280
H         -3.94480        0.83650        0.39790
H         -1.60600        1.68030       -0.29260
H         -2.33040        0.41680       -1.39920
H         -1.11520       -1.36260       -0.08710
H         -0.37090       -0.01440        0.84390
H          2.01190       -0.55390       -1.87420
H          1.10300       -1.92840       -1.21870
H          1.37720       -1.01490        1.07530
H          2.97560       -1.23720        0.22690
H          3.09820        1.05250       -0.39840
H          2.87180        0.91190        1.42230
H          1.50350        1.40300        0.35670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_488_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

