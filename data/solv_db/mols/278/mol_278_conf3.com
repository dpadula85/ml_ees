%nproc=16
%mem=2GB
%chk=mol_278_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.68150       -0.05690       -0.25530
C          0.78050       -0.04850        0.12190
F          1.57740        0.30730       -0.92260
F          1.11140       -1.31150        0.53480
F          0.99280        0.84700        1.15040
Cl        -1.11890        1.57520       -0.78120
Br        -1.78000       -0.56960        1.23140
H         -0.88180       -0.74290       -1.07950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

