%nproc=16
%mem=2GB
%chk=mol_278_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.68040        0.03720       -0.26120
C         -0.78760       -0.09390        0.09330
F         -1.47000        1.04490       -0.26910
F         -1.35940       -1.17680       -0.50140
F         -0.88100       -0.19670        1.47550
Cl         1.60360       -1.39490        0.18140
Br         1.45480        1.56440        0.63360
H          0.75920        0.21580       -1.35210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

