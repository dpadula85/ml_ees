%nproc=16
%mem=2GB
%chk=mol_278_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.66760        0.19130        0.18520
C         -0.75560       -0.22610       -0.10130
F         -0.84460       -1.58650        0.10090
F         -1.17450        0.15350       -1.36050
F         -1.59640        0.37920        0.81090
Cl         0.90840        1.92610       -0.04100
Br         1.94050       -0.80580       -0.85240
H          0.85470       -0.03170        1.25800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_278_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

