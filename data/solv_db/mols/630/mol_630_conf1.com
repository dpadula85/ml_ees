%nproc=16
%mem=2GB
%chk=mol_630_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.68770       -0.58460        0.19140
C         -0.62810       -1.47900        0.09940
C          0.66780       -1.02280       -0.04880
C          0.94120        0.33520       -0.10910
C         -0.12870        1.20060       -0.01540
C         -1.42360        0.76560        0.13220
Cl         0.23820        2.92340       -0.09410
C          2.28860        0.82590       -0.26360
N          3.38340        1.21950       -0.37100
Cl         2.02700       -2.13660       -0.16740
H         -2.70100       -0.98450        0.30830
H         -0.75640       -2.55050        0.13770
H         -2.22060        1.48770        0.20030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_630_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_630_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_630_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

