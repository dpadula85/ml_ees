%nproc=16
%mem=2GB
%chk=mol_356_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.40280       -0.91550       -0.16100
C         -1.22680        0.00030       -0.05470
C         -1.37250        1.37050        0.03080
C         -0.26000        2.18060        0.12800
C          1.02750        1.68420        0.14510
C          1.17210        0.31560        0.05960
C          0.05670       -0.50730       -0.03850
N          0.20330       -1.90850       -0.12570
C          2.54700       -0.24490        0.07590
H         -2.46400       -1.39940       -1.14260
H         -3.32960       -0.32290       -0.01490
H         -2.39490       -1.67980        0.63640
H         -2.38030        1.79120        0.01930
H         -0.34800        3.26560        0.19730
H          1.88110        2.35240        0.22250
H          0.36330       -2.32140       -1.07980
H          0.16250       -2.54930        0.69090
H          3.19980        0.20040       -0.70680
H          3.02210        0.03580        1.05880
H          2.54370       -1.34750        0.05950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

