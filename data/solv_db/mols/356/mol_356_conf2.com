%nproc=16
%mem=2GB
%chk=mol_356_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53520        0.18410        0.18250
C          1.09640        0.49330        0.03800
C          0.74550        1.81980       -0.11230
C         -0.59820        2.11280       -0.24750
C         -1.56220        1.12710       -0.23440
C         -1.20970       -0.20890       -0.08310
C          0.13060       -0.47690        0.04950
N          0.53460       -1.83280        0.20720
C         -2.23600       -1.28890       -0.06620
H          2.85260       -0.72470       -0.34130
H          3.16390        1.01600       -0.20680
H          2.79140        0.09660        1.26440
H          1.48920        2.61350       -0.12530
H         -0.88350        3.16120       -0.36710
H         -2.61270        1.35450       -0.34000
H          1.47120       -2.11580        0.57790
H         -0.10070       -2.62810       -0.04610
H         -2.41950       -1.70280       -1.05940
H         -1.96090       -2.11570        0.61610
H         -3.22720       -0.88410        0.29390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

