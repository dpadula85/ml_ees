%nproc=16
%mem=2GB
%chk=mol_356_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53030       -0.23690       -0.01370
C          1.15170        0.35760       -0.06500
C          0.97930        1.72240       -0.24380
C         -0.30340        2.21180       -0.28300
C         -1.38570        1.37720       -0.14940
C         -1.20650        0.01640        0.02870
C          0.07620       -0.48520        0.06950
N          0.26660       -1.90240        0.25500
C         -2.35770       -0.91760        0.17680
H          3.30630        0.54540        0.10360
H          2.73120       -0.74750       -0.98940
H          2.60380       -1.03060        0.75650
H          1.86980        2.32510       -0.34250
H         -0.46430        3.25560       -0.41900
H         -2.41970        1.74580       -0.17750
H          0.47030       -2.55160       -0.51740
H          0.19250       -2.28380        1.22650
H         -2.44730       -1.30960        1.21060
H         -2.31520       -1.75140       -0.56330
H         -3.27820       -0.34050       -0.06320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

