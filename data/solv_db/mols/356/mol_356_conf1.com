%nproc=16
%mem=2GB
%chk=mol_356_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.39530        0.87340        0.16790
C          1.20460       -0.02350        0.03970
C          1.38460       -1.40040       -0.07440
C          0.28300       -2.23050       -0.19340
C         -0.98250       -1.68400       -0.19780
C         -1.19480       -0.32280       -0.08640
C         -0.08610        0.48660        0.03110
N         -0.26040        1.88470        0.14800
C         -2.53210        0.29870       -0.08630
H          3.18780        0.41990        0.80240
H          2.03920        1.76980        0.71700
H          2.81910        1.13590       -0.83280
H          2.38230       -1.82920       -0.07040
H          0.47370       -3.28940       -0.27890
H         -1.83110       -2.34780       -0.29200
H          0.00130        2.39390        1.02020
H         -0.66530        2.39300       -0.68160
H         -3.28920       -0.28230       -0.65280
H         -2.89120        0.40780        0.97880
H         -2.43830        1.34610       -0.45840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

