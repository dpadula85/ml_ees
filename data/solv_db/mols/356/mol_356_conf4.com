%nproc=16
%mem=2GB
%chk=mol_356_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.51180       -0.35840       -0.02110
C          1.17870        0.29540       -0.02370
C          1.03130        1.67010       -0.05450
C         -0.22110        2.25770       -0.05600
C         -1.35850        1.45250       -0.02620
C         -1.23510        0.09060        0.00440
C          0.03250       -0.47010        0.00530
N          0.19300       -1.88630        0.03660
C         -2.38350       -0.85090        0.03720
H          2.95860       -0.27460        1.00830
H          2.43720       -1.40350       -0.32890
H          3.20670        0.18150       -0.72810
H          1.92600        2.27500       -0.07750
H         -0.36430        3.32800       -0.07970
H         -2.36090        1.86290       -0.02600
H         -0.15800       -2.45800       -0.78410
H          0.63220       -2.41010        0.80900
H         -2.34810       -1.61070       -0.74820
H         -3.35540       -0.32560        0.01520
H         -2.32310       -1.36560        1.03780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_356_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

