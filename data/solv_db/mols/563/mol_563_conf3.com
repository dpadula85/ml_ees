%nproc=16
%mem=2GB
%chk=mol_563_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.13340        1.20130       -0.12710
C          1.25160        1.11320       -0.11860
C          1.82820       -0.14100       -0.17440
C          1.06340       -1.28150       -0.23720
C         -0.30850       -1.14260       -0.24320
C         -0.94210        0.08750       -0.18900
N         -2.36610        0.19260       -0.19700
Cl         3.55970       -0.31730       -0.16730
H         -0.58690        2.18890       -0.08320
H          1.85190        2.02900       -0.06820
H          1.50000       -2.28670       -0.28240
H         -0.92460       -2.04750       -0.29310
H         -2.88110        0.98180       -0.64720
H         -2.91200       -0.57790        0.28270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

