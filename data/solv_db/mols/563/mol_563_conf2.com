%nproc=16
%mem=2GB
%chk=mol_563_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.20400        1.20420        0.05920
C          1.17810        1.14510        0.00340
C          1.87200       -0.04520       -0.07270
C          1.13320       -1.21010       -0.09250
C         -0.25720       -1.15870       -0.03670
C         -0.94120        0.03750        0.03920
N         -2.36340        0.04740        0.09440
Cl         3.63230       -0.10080       -0.14300
H         -0.74550        2.13880        0.11890
H          1.75760        2.04960        0.01850
H          1.62890       -2.17810       -0.15160
H         -0.85970       -2.05720       -0.05080
H         -2.92860       -0.64980        0.62780
H         -2.90250        0.77720       -0.41430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

