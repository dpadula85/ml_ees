%nproc=16
%mem=2GB
%chk=mol_563_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.02840       -1.20480       -0.08640
C         -1.32090       -0.98090        0.09840
C         -1.82920        0.29320        0.25110
C         -0.94210        1.34940        0.21370
C          0.40910        1.11900        0.02830
C          0.92920       -0.14820       -0.12550
N          2.32080       -0.37460       -0.31600
Cl        -3.55510        0.55020        0.48530
H          0.42820       -2.20800       -0.20650
H         -2.02300       -1.80370        0.12890
H         -1.28440        2.35930        0.32720
H          1.13120        1.93460       -0.00530
H          2.91230        0.36990       -0.74640
H          2.79540       -1.25550       -0.04700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

