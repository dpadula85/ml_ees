%nproc=16
%mem=2GB
%chk=mol_563_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.19380        1.17960        0.05550
C         -1.17960        1.16130        0.11580
C         -1.84710       -0.05190        0.08080
C         -1.12100       -1.21630       -0.01390
C          0.25580       -1.16980       -0.07300
C          0.95400        0.03010       -0.04000
N          2.37950        0.04840       -0.10240
Cl        -3.59960       -0.05610        0.15840
H          0.74350        2.11190        0.08080
H         -1.76760        2.07990        0.19110
H         -1.66270       -2.15350       -0.03970
H          0.82120       -2.08890       -0.14750
H          2.84990        0.71800       -0.75250
H          2.97990       -0.59270        0.48660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

