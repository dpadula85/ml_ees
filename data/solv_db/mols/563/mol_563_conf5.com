%nproc=16
%mem=2GB
%chk=mol_563_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.06900        1.20780        0.06690
C         -1.30560        1.02170        0.12750
C         -1.84870       -0.25110        0.07670
C         -0.96590       -1.30510       -0.03540
C          0.40420       -1.12610       -0.09620
C          0.93750        0.14520       -0.04500
N          2.35410        0.30950       -0.10920
Cl        -3.59090       -0.50480        0.15220
H          0.47470        2.18730        0.10680
H         -1.99840        1.83630        0.21480
H         -1.37040       -2.31080       -0.07700
H          1.05830       -1.96800       -0.18300
H          2.90360        0.19390       -0.97960
H          2.87850        0.56400        0.78060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_563_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

