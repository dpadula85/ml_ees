%nproc=16
%mem=2GB
%chk=mol_454_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12800       -0.15650       -0.03850
O          0.03460       -0.76040        0.47370
C          1.13210        0.05260        0.21560
H         -2.01890       -0.75780        0.13950
H         -0.93490       -0.02200       -1.12750
H         -1.23910        0.80600        0.50070
H          1.22880        0.18410       -0.88930
H          2.03160       -0.40360        0.66610
H          0.89380        1.05760        0.63980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

