%nproc=16
%mem=2GB
%chk=mol_454_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.14470       -0.13790       -0.00500
O          0.05580        0.18610       -0.75970
C         -1.14660        0.19150       -0.08360
H          1.09010       -1.15110        0.45420
H          1.34710        0.62370        0.78170
H          2.03030       -0.13980       -0.67460
H         -1.20900        0.88820        0.75100
H         -1.30820       -0.85140        0.29360
H         -2.00420        0.39090       -0.75760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

