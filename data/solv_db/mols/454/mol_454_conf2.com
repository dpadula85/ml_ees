%nproc=16
%mem=2GB
%chk=mol_454_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.16450       -0.14610       -0.28060
O          0.06610       -0.75700       -0.44900
C          1.14100        0.05410       -0.20660
H         -1.95340       -0.88990       -0.50150
H         -1.29360        0.29040        0.73700
H         -1.26310        0.64420       -1.07890
H          2.05430       -0.56540       -0.37970
H          1.21770        0.92010       -0.91430
H          1.19550        0.44960        0.82720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

