%nproc=16
%mem=2GB
%chk=mol_454_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.11480       -0.05470       -0.07490
O          0.03550       -0.69240       -0.50190
C          1.14020       -0.02140        0.05070
H         -1.13890        1.01870       -0.40020
H         -1.26940       -0.16520        1.01460
H         -1.98890       -0.52550       -0.58390
H          1.09960        1.00690       -0.30980
H          2.07610       -0.47620       -0.34890
H          1.16060       -0.09010        1.15430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_454_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

