%nproc=16
%mem=2GB
%chk=mol_321_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.33610        0.87970       -0.31930
C         -2.06270        0.24990        0.24810
C         -1.44930       -0.54480       -0.88460
C         -0.16680       -1.23460       -0.47020
C          0.85100       -0.21790       -0.02120
C          2.14830       -0.81900        0.40530
C          3.03940        0.35910        0.82050
O          3.20160        1.20590       -0.27730
H         -3.75700        0.17150       -1.03480
H         -4.03130        1.04620        0.55270
H         -3.13640        1.88910       -0.73670
H         -2.34180       -0.35390        1.10880
H         -1.41120        1.10830        0.52120
H         -1.20150        0.20730       -1.68200
H         -2.20460       -1.23890       -1.26270
H          0.24510       -1.69490       -1.40540
H         -0.32320       -2.04420        0.23570
H          1.06770        0.52990       -0.81060
H          0.41780        0.28790        0.88950
H          2.68490       -1.35160       -0.39430
H          2.05910       -1.44680        1.32640
H          2.58510        0.90690        1.68960
H          4.00690       -0.05150        1.10170
H          3.11500        2.15660       -0.04460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

