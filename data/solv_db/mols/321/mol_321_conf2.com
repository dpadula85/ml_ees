%nproc=16
%mem=2GB
%chk=mol_321_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.91660       -0.75600       -0.47140
C          1.94650        0.34480       -0.72300
C          1.05860        0.66540        0.43490
C          0.20760       -0.46810        0.89240
C         -0.70800       -0.97190       -0.18810
C         -1.64590        0.08940       -0.68870
C         -2.52000        0.61590        0.42050
O         -3.25300       -0.45490        0.93410
H          3.72040       -0.68390       -1.25880
H          2.47380       -1.77240       -0.61700
H          3.44910       -0.67780        0.49230
H          2.54270        1.27140       -0.94160
H          1.38990        0.11890       -1.65220
H          0.40600        1.51520        0.16200
H          1.69010        1.07370        1.28040
H          0.79850       -1.29560        1.35500
H         -0.42290       -0.09590        1.73650
H         -1.30670       -1.84790        0.13930
H         -0.08800       -1.33660       -1.05990
H         -2.27020       -0.37260       -1.49630
H         -1.01530        0.91740       -1.12280
H         -3.21970        1.38730       -0.02260
H         -1.97130        1.07810        1.25200
H         -4.17880       -0.21020        1.14300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

