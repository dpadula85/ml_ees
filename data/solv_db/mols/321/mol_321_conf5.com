%nproc=16
%mem=2GB
%chk=mol_321_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.81200        0.29040       -0.29330
C          2.61910       -0.62950       -0.42240
C          1.33830        0.15520       -0.24530
C          0.11750       -0.72810       -0.36720
C         -1.10030        0.15970       -0.17340
C         -2.38600       -0.62270       -0.27560
C         -3.51930        0.37600       -0.06460
O         -3.32900        0.92240        1.20870
H          4.36250        0.39350       -1.25040
H          3.49190        1.33070       -0.02410
H          4.54830       -0.09260        0.45270
H          2.62460       -1.43860        0.32060
H          2.57450       -1.09810       -1.43630
H          1.33810        0.53360        0.81710
H          1.27760        1.04450       -0.88540
H          0.05180       -1.26480       -1.31730
H          0.14670       -1.43930        0.50420
H         -1.08670        0.66320        0.81040
H         -1.08670        0.95510       -0.94640
H         -2.46310       -1.35370        0.55120
H         -2.50740       -1.14110       -1.23420
H         -4.50810       -0.07220       -0.19420
H         -3.31630        1.21360       -0.78900
H         -3.00000        1.84250        1.18780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

