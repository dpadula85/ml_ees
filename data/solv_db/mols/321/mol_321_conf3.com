%nproc=16
%mem=2GB
%chk=mol_321_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.45330       -0.00720       -1.26120
C         -2.32450       -0.83270       -0.03990
C         -1.48770       -0.31090        1.06700
C         -0.04290       -0.10470        0.79040
C          0.34900        0.84270       -0.28080
C          1.87950        0.92060       -0.32640
C          2.52590       -0.39240       -0.61180
O          3.92000       -0.15720       -0.62640
H         -2.19120        1.07220       -1.13760
H         -1.92730       -0.46430       -2.15060
H         -3.54680        0.02990       -1.55980
H         -1.85330       -1.81680       -0.34630
H         -3.36740       -1.06620        0.34650
H         -1.52710       -1.11410        1.87590
H         -1.94910        0.56800        1.58860
H          0.52320        0.17270        1.73350
H          0.34550       -1.12010        0.46680
H          0.08890        0.45150       -1.31430
H         -0.05560        1.84130       -0.16070
H          2.21970        1.26660        0.66900
H          2.16660        1.62020       -1.13910
H          2.20600       -0.84370       -1.56460
H          2.35180       -1.14000        0.21660
H          4.15000        0.58460       -0.01880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

