%nproc=16
%mem=2GB
%chk=mol_321_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.41270        0.05650       -0.35750
C         -2.50380       -0.56540        0.64210
C         -1.08760       -0.03370        0.60830
C         -0.41650       -0.24370       -0.70580
C          0.97940        0.32140       -0.63630
C          1.82500       -0.33790        0.43460
C          3.22290        0.29190        0.43100
O          3.84440        0.11940       -0.80300
H         -4.16710       -0.71760       -0.67940
H         -2.94950        0.42650       -1.26940
H         -4.02150        0.89450        0.06760
H         -2.90600       -0.30810        1.66340
H         -2.47580       -1.66500        0.59690
H         -0.54130       -0.60540        1.39350
H         -1.04160        1.03440        0.86020
H         -0.93660        0.25720       -1.53380
H         -0.32540       -1.31340       -0.99260
H          0.85950        1.39090       -0.30280
H          1.47880        0.32920       -1.60700
H          1.89940       -1.41280        0.28030
H          1.42990       -0.14160        1.45250
H          3.05790        1.37410        0.59010
H          3.80250       -0.08950        1.27470
H          4.38580        0.93830       -0.99880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_321_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

