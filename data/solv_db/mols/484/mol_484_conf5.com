%nproc=16
%mem=2GB
%chk=mol_484_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.88770        0.62140       -0.85020
C         -2.87600       -0.39840       -0.40910
C         -1.48910        0.15150       -0.38580
O         -1.36660        1.34620       -0.72640
N         -0.43120       -0.66440        0.00180
C          0.93550       -0.32690        0.08220
C          1.85400       -1.20020        0.68390
C          3.20400       -0.97890        0.70800
C          3.71380        0.14060        0.12470
C          2.87810        1.02540       -0.47750
C          1.49580        0.78300       -0.49600
Cl         3.55040        2.45600       -1.22100
Cl         5.44590        0.40690        0.16970
H         -3.52960        1.23320       -1.70630
H         -4.78840        0.03730       -1.20240
H         -4.24270        1.26810       -0.02180
H         -2.92290       -1.26540       -1.07860
H         -3.14900       -0.68760        0.63910
H         -0.67390       -1.67610        0.25670
H          1.45420       -2.09280        1.14990
H          3.90300       -1.67730        1.18560
H          0.92230        1.49870       -1.06000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

