%nproc=16
%mem=2GB
%chk=mol_484_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.89680       -0.68710        0.28430
C          2.86440        0.38690        0.36030
C          1.45400       -0.13770        0.19320
O          1.40620       -1.37710        0.02130
N          0.35590        0.73240        0.23710
C         -0.97450        0.32650        0.09630
C         -1.45170       -0.95660        0.13120
C         -2.77290       -1.25790       -0.22430
C         -3.64150       -0.28120       -0.61210
C         -3.16180        1.00430       -0.63710
C         -1.87990        1.32200       -0.29980
Cl        -4.28260        2.27970       -1.13340
Cl        -5.29260       -0.67230       -1.05490
H          3.52460       -1.58310       -0.25940
H          4.18670       -0.96310        1.31530
H          4.80220       -0.31770       -0.27860
H          3.02280        1.09210       -0.48080
H          2.91490        0.97780        1.29820
H          0.54120        1.76550        0.43360
H         -0.83830       -1.73700        0.55250
H         -3.09520       -2.28650       -0.18560
H         -1.57870        2.37010       -0.34690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

