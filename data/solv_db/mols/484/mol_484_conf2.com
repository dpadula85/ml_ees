%nproc=16
%mem=2GB
%chk=mol_484_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.97030        0.38750        0.28950
C         -2.86590       -0.36010       -0.40680
C         -1.52660        0.06820        0.12070
O         -1.48680        0.93480        1.01380
N         -0.36650       -0.50740       -0.39420
C          0.97000       -0.25490       -0.05090
C          1.96650       -1.08470       -0.58440
C          3.29970       -0.89610       -0.39610
C          3.73380        0.17060        0.36410
C          2.80120        1.02200        0.91350
C          1.43220        0.79840        0.69690
Cl         3.37090        2.36190        1.86870
Cl         5.43610        0.45030        0.63480
H         -4.45070       -0.22040        1.08570
H         -3.59540        1.36800        0.69270
H         -4.73880        0.65380       -0.46980
H         -2.93210       -0.23320       -1.50390
H         -2.99070       -1.44060       -0.19790
H         -0.53890       -1.21770       -1.18580
H          1.66480       -1.93460       -1.17650
H          4.01910       -1.58230       -0.84220
H          0.76820        1.51670        1.09820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

