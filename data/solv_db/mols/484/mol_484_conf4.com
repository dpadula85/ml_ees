%nproc=16
%mem=2GB
%chk=mol_484_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.86300       -0.66220        0.09030
C         -2.82890        0.32150        0.52490
C         -1.46750       -0.09320        0.01350
O         -1.43980       -1.14040       -0.65510
N         -0.33810        0.69860        0.30870
C          0.95860        0.37500       -0.12360
C          1.87610        1.43520       -0.11300
C          3.18670        1.18230       -0.39150
C          3.62590       -0.09440       -0.68210
C          2.73030       -1.13970       -0.68830
C          1.38340       -0.90080       -0.40080
Cl         3.29950       -2.75430       -1.05810
Cl         5.32170       -0.39960       -1.04550
H         -3.42380       -1.68380       -0.05120
H         -4.32830       -0.39240       -0.88680
H         -4.63080       -0.75530        0.89310
H         -2.75170        0.41270        1.62790
H         -3.05810        1.36250        0.16040
H         -0.48030        1.57590        0.91760
H          1.54120        2.45690        0.11630
H          3.94140        1.96860       -0.39760
H          0.74540       -1.77300       -0.28450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

