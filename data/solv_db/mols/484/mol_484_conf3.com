%nproc=16
%mem=2GB
%chk=mol_484_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.90740        0.80540       -0.19220
C         -2.90260       -0.28090       -0.00740
C         -1.49760        0.23240       -0.04910
O         -1.32360        1.44580       -0.22420
N         -0.43080       -0.67550        0.11130
C          0.94980       -0.38560        0.09760
C          1.86170       -1.35690        0.53170
C          3.22200       -1.19810        0.43620
C          3.72640       -0.04100       -0.10660
C          2.89310        0.94590       -0.55220
C          1.51160        0.74580       -0.44060
Cl         3.51690        2.44540       -1.25550
Cl         5.46110        0.21080       -0.24710
H         -3.41320        1.79320       -0.36440
H         -4.53700        0.93950        0.73520
H         -4.64020        0.59280       -1.00090
H         -3.04710       -1.03180       -0.82310
H         -3.06650       -0.79310        0.95030
H         -0.66860       -1.72560        0.22760
H          1.47100       -2.25150        0.96260
H          3.91730       -1.94100        0.76810
H          0.90380        1.52370       -0.88970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_484_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

