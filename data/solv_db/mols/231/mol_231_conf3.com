%nproc=16
%mem=2GB
%chk=mol_231_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26740       -1.97420        0.54290
C         -0.90540       -1.37940        0.32150
C          0.23360       -2.15590        0.36720
C          1.49290       -1.58160        0.15840
C          1.62810       -0.23330       -0.09680
C          0.49950        0.55820       -0.14610
C         -0.74140       -0.03470        0.06450
C         -1.86110        0.78910        0.00790
C         -1.72770        2.13770       -0.24820
C         -0.49930        2.72870       -0.45760
C          0.60810        1.91020       -0.40090
C          2.97980        0.34790       -0.31080
H         -2.83940       -1.38960        1.29250
H         -2.13670       -2.98920        0.95970
H         -2.79430       -2.05790       -0.42670
H          0.16180       -3.21820        0.56540
H          2.34510       -2.25590        0.20940
H         -2.82740        0.31930        0.17360
H         -2.59560        2.78080       -0.29300
H         -0.40490        3.79260       -0.65870
H          1.57630        2.35940       -0.56420
H          3.77930       -0.33480        0.04340
H          3.10850        1.29660        0.25910
H          3.18750        0.58410       -1.36230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

