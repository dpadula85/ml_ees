%nproc=16
%mem=2GB
%chk=mol_231_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.82760       -0.89100       -0.25240
C         -1.35540       -0.91230       -0.15370
C         -0.67210       -2.10020        0.00540
C          0.70390       -2.11760        0.09720
C          1.37380       -0.91280        0.02510
C          0.67870        0.27480       -0.13470
C         -0.69550        0.30810       -0.22790
C         -1.40200        1.49200       -0.38770
C         -0.69440        2.67710       -0.45500
C          0.67810        2.69300       -0.36660
C          1.33980        1.49650       -0.20860
C          2.84420       -0.87470        0.11710
H         -3.27200       -0.17300        0.44330
H         -3.23850       -1.89530        0.02190
H         -3.10180       -0.62950       -1.29250
H         -1.17150       -3.05790        0.06490
H          1.25180       -3.05110        0.22320
H         -2.48300        1.47950       -0.45730
H         -1.21130        3.63160       -0.58030
H          1.23120        3.63720       -0.42090
H          2.40880        1.52660       -0.14150
H          3.22790       -0.49130       -0.84440
H          3.13610       -0.23840        0.97930
H          3.25090       -1.87150        0.29810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

