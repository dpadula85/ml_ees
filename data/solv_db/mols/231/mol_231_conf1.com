%nproc=16
%mem=2GB
%chk=mol_231_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.71360       -1.21250       -0.34660
C         -1.25850       -1.04600       -0.21710
C         -0.39850       -2.11610       -0.28140
C          0.96450       -1.95780       -0.15970
C          1.50070       -0.70420        0.03180
C          0.67080        0.40020        0.10240
C         -0.71000        0.21000       -0.02450
C         -1.56320        1.28260        0.04030
C         -1.05620        2.55110        0.23120
C          0.32500        2.72930        0.35670
C          1.18880        1.66110        0.29320
C          2.98580       -0.52440        0.16590
H         -3.06920       -0.87140       -1.35310
H         -2.93940       -2.29870       -0.30310
H         -3.28630       -0.74750        0.47830
H         -0.83800       -3.11340       -0.43470
H          1.64430       -2.81140       -0.21150
H         -2.64070        1.11040       -0.06180
H         -1.75180        3.37460        0.27800
H          0.66210        3.75010        0.50500
H          2.25290        1.79070        0.38880
H          3.49500       -1.44890       -0.19180
H          3.19970       -0.31000        1.22520
H          3.33580        0.30220       -0.51150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

