%nproc=16
%mem=2GB
%chk=mol_231_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.55450       -1.53690       -0.11470
C         -1.11830       -1.20460       -0.10210
C         -0.14540       -2.19080       -0.20670
C          1.18730       -1.85490       -0.19280
C          1.55230       -0.53390       -0.07440
C          0.58010        0.47260        0.03240
C         -0.76270        0.12360        0.01730
C         -1.72340        1.12580        0.12480
C         -1.30120        2.43340        0.24300
C          0.03390        2.77980        0.25830
C          0.97970        1.78540        0.15170
C          2.96340       -0.14440       -0.05670
H         -2.67570       -2.64920        0.02390
H         -3.05630       -1.29460       -1.07460
H         -3.10420       -1.09120        0.75460
H         -0.49610       -3.21240       -0.29740
H          1.95270       -2.60660       -0.27290
H         -2.75060        0.82270        0.10960
H         -2.03130        3.21950        0.32690
H          0.34840        3.83280        0.35400
H          2.04110        1.98190        0.15870
H          3.56700       -1.03590       -0.32420
H          3.22310        0.63000       -0.80350
H          3.29040        0.14800        0.96500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

