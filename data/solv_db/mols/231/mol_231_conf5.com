%nproc=16
%mem=2GB
%chk=mol_231_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.02910        0.13860        0.01350
C          1.62710       -0.29600        0.04170
C          1.34430       -1.63700        0.16060
C          0.07490       -2.15470        0.19620
C         -1.00540       -1.30300        0.10930
C         -0.75570        0.04810       -0.01130
C          0.53860        0.56770       -0.04690
C          0.78420        1.91990       -0.17110
C         -0.30880        2.75740       -0.25970
C         -1.59310        2.25460       -0.22490
C         -1.83500        0.89300       -0.10030
C         -2.40380       -1.81340        0.14350
H          3.64250       -0.64630        0.50120
H          3.22310        1.07000        0.59460
H          3.42490        0.21700       -1.03210
H          2.17900       -2.33000        0.23100
H         -0.12020       -3.20870        0.29010
H          1.80480        2.29230       -0.20010
H         -0.14730        3.82180       -0.35750
H         -2.46080        2.88620       -0.29180
H         -2.82680        0.48980       -0.07210
H         -2.35700       -2.88420        0.43480
H         -3.01130       -1.30270        0.91450
H         -2.84740       -1.78040       -0.86330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_231_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

