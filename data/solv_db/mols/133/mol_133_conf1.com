%nproc=16
%mem=2GB
%chk=mol_133_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61540        0.22350       -0.13890
C         -2.19970        0.65730        0.08760
C         -1.22820       -0.50860        0.04010
C          0.16230        0.08550        0.28590
C          1.21260       -1.00530        0.25800
C          2.56180       -0.42240        0.49920
C          2.98810        0.61000       -0.51600
Br         4.77090        1.20260        0.02710
H         -4.18430        0.10710        0.80690
H         -4.12750        1.02390       -0.71330
H         -3.69660       -0.71140       -0.74480
H         -1.89870        1.36400       -0.71580
H         -2.13300        1.09900        1.09220
H         -1.45720       -1.19940        0.87730
H         -1.26270       -1.06910       -0.89310
H          0.41180        0.82700       -0.49550
H          0.16860        0.58770        1.27130
H          1.01500       -1.73570        1.06510
H          1.22300       -1.53660       -0.71020
H          2.58000        0.05880        1.51980
H          3.31520       -1.24400        0.44700
H          3.06160        0.09390       -1.51130
H          2.33220        1.49220       -0.55680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

