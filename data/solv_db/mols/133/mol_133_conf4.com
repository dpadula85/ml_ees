%nproc=16
%mem=2GB
%chk=mol_133_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21270        1.16160        0.32310
C          2.15240        0.23000       -0.82330
C          1.35190       -1.00630       -0.58030
C         -0.08620       -0.88930       -0.30890
C         -0.56380       -0.12850        0.86510
C         -2.09510       -0.25820        0.90260
C         -2.78080        0.28020       -0.29660
Br        -2.34130        2.15840       -0.47890
H          3.28430        1.52140        0.46990
H          1.56150        2.03900        0.11390
H          1.92130        0.72360        1.28750
H          3.22290       -0.04290       -1.09650
H          1.75140        0.74240       -1.73600
H          1.43690       -1.63720       -1.53030
H          1.87740       -1.65480        0.18290
H         -0.49350       -1.94420       -0.17770
H         -0.58790       -0.51010       -1.24850
H         -0.21680       -0.60120        1.80950
H         -0.36570        0.96640        0.85390
H         -2.47300        0.28520        1.79710
H         -2.33260       -1.33130        1.07470
H         -3.89900        0.16750       -0.19040
H         -2.53690       -0.27190       -1.21290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

