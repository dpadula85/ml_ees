%nproc=16
%mem=2GB
%chk=mol_133_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.55570        1.21270       -0.13620
C          2.53760       -0.25310        0.17780
C          1.31920       -0.95630       -0.36800
C          0.05510       -0.36390        0.21180
C         -1.17730       -1.04340       -0.31290
C         -2.42500       -0.46510        0.25820
C         -2.60280        1.00810       -0.04760
Br        -4.25510        1.66710        0.73290
H          3.62340        1.56120       -0.24780
H          2.01390        1.48300       -1.05270
H          2.14900        1.82090        0.70190
H          3.46550       -0.70530       -0.17750
H          2.54340       -0.36270        1.29610
H          1.31650       -0.94990       -1.46310
H          1.37420       -2.01010       -0.01410
H          0.07270       -0.48510        1.31210
H          0.02130        0.71790       -0.00020
H         -1.12190       -2.11050        0.05250
H         -1.23920       -1.02900       -1.41730
H         -3.33560       -0.96920       -0.14620
H         -2.45500       -0.55960        1.38150
H         -1.76700        1.58920        0.38840
H         -2.66830        1.20310       -1.12950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

