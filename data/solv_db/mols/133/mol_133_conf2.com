%nproc=16
%mem=2GB
%chk=mol_133_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99260        0.56670        0.35420
C          2.56660       -0.13360       -0.90020
C          1.27350       -0.91110       -0.72180
C          0.21050        0.09060       -0.32090
C         -1.13490       -0.59160       -0.11530
C         -2.10920        0.50690        0.28000
C         -3.49400       -0.03340        0.51670
Br        -4.61360        1.48210        1.01030
H          2.43850        0.26180        1.25450
H          4.09430        0.40040        0.53780
H          2.90010        1.68500        0.23670
H          3.38820       -0.81870       -1.22540
H          2.40270        0.62010       -1.69600
H          1.05710       -1.40380       -1.68160
H          1.38310       -1.71010        0.03800
H          0.51350        0.53680        0.63750
H          0.05190        0.86250       -1.08910
H         -1.08610       -1.33930        0.70020
H         -1.48010       -1.11780       -1.02010
H         -2.17300        1.29040       -0.49560
H         -1.73510        0.96120        1.20610
H         -3.91170       -0.43170       -0.43190
H         -3.53470       -0.77360        1.33120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

