%nproc=16
%mem=2GB
%chk=mol_133_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.38720       -0.75270        0.92920
C         -2.11260       -0.62480       -0.56440
C         -1.59850        0.74100       -0.86880
C         -0.32410        1.04640       -0.13500
C          0.80590        0.09640       -0.46910
C          2.00110        0.55030        0.34840
C          3.21570       -0.31560        0.11410
Br         4.63810        0.40360        1.23090
H         -2.86880       -1.74900        1.11000
H         -3.10190        0.03100        1.26900
H         -1.46180       -0.73520        1.52440
H         -1.38040       -1.40770       -0.83510
H         -3.08680       -0.81480       -1.07380
H         -1.48470        0.92600       -1.94180
H         -2.37060        1.47090       -0.49720
H          0.00960        2.09500       -0.28590
H         -0.46300        0.95890        0.98260
H          0.47170       -0.91920       -0.17770
H          0.99190        0.10080       -1.54840
H          2.21630        1.59450        0.06260
H          1.69140        0.52060        1.41280
H          3.04150       -1.36830        0.34610
H          3.55730       -0.17890       -0.93270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_133_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

