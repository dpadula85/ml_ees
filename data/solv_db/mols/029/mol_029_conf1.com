%nproc=16
%mem=2GB
%chk=mol_029_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.62760        0.74360       -0.25330
C         -3.66250       -0.63590       -0.21730
C         -2.47120       -1.31610       -0.12220
C         -1.25990       -0.63910       -0.06320
C         -1.27480        0.73600       -0.10230
C         -2.45000        1.44760       -0.19730
O         -0.03830        1.41330       -0.04170
C          1.13000        0.67690        0.05370
C          2.35620        1.32510        0.11450
C          3.52820        0.59960        0.20980
C          3.52720       -0.78120        0.24790
C          2.31200       -1.43090        0.18790
C          1.14860       -0.70030        0.09300
O         -0.05800       -1.37460        0.03420
Cl         5.04880       -1.67110        0.37020
Cl         5.08380        1.42010        0.28730
H         -4.55280        1.31700       -0.32830
H         -4.60090       -1.17380       -0.26250
H         -2.44180       -2.39830       -0.09050
H         -2.40090        2.53890       -0.22450
H          2.37070        2.41630        0.08530
H          2.33330       -2.51310        0.21920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_029_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_029_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_029_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

