%nproc=16
%mem=2GB
%chk=mol_027_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.54450       -0.48650       -0.31210
C         -1.18530        0.09640       -0.18450
C         -0.95940        1.44370       -0.21220
C          0.31690        1.95620       -0.09080
C          1.38910        1.08880        0.06190
C          1.18760       -0.26600        0.09250
C         -0.09960       -0.74720       -0.03130
C          2.26970       -1.26680        0.25140
H         -3.04990       -0.40210        0.68970
H         -3.18030        0.05850       -1.03810
H         -2.46490       -1.55960       -0.55690
H         -1.81790        2.09380       -0.33250
H          0.51470        3.02310       -0.11060
H          2.40440        1.44410        0.16040
H         -0.30240       -1.82700       -0.01160
H          3.25300       -0.85930        0.00910
H          2.18810       -1.66100        1.28560
H          2.08070       -2.12900       -0.42110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

