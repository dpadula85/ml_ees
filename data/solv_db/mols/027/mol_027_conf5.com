%nproc=16
%mem=2GB
%chk=mol_027_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44640       -0.90810       -0.02700
C         -1.21130       -0.08660       -0.00270
C         -1.23190        1.28440        0.04380
C         -0.02010        1.95780        0.06360
C          1.17230        1.28390        0.03790
C          1.20920       -0.07910       -0.00840
C         -0.00620       -0.75900       -0.02850
C          2.48500       -0.85580       -0.03780
H         -2.42620       -1.54730        0.88910
H         -2.33530       -1.60650       -0.89640
H         -3.36560       -0.30240       -0.10030
H         -2.18810        1.81990        0.06410
H         -0.08620        3.04540        0.10050
H          2.10560        1.82720        0.05430
H          0.05000       -1.84500       -0.06570
H          3.28650       -0.32170        0.50720
H          2.27570       -1.81820        0.49240
H          2.73310       -1.08890       -1.08610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

