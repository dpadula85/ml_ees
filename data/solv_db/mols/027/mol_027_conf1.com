%nproc=16
%mem=2GB
%chk=mol_027_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.45660       -1.01190       -0.00030
C         -1.18980       -0.21080       -0.02610
C         -1.22220        1.15300       -0.12720
C         -0.07810        1.93270       -0.15460
C          1.16220        1.33160       -0.07790
C          1.20320       -0.04350        0.02430
C          0.04590       -0.81600        0.05080
C          2.53540       -0.72180        0.10860
H         -2.90850       -0.97740        1.03140
H         -2.23650       -2.08070       -0.19180
H         -3.22040       -0.59210       -0.68650
H         -2.20250        1.62150       -0.18730
H         -0.12110        3.02620       -0.23640
H          2.08820        1.90540       -0.09570
H          0.08630       -1.89510        0.13130
H          2.50130       -1.57240       -0.58320
H          3.33400       -0.00200       -0.14840
H          2.67930       -1.04650        1.16920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

