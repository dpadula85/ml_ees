%nproc=16
%mem=2GB
%chk=mol_027_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53660       -0.79210       -0.14520
C          1.21770       -0.08280       -0.01020
C          1.16710        1.26350        0.28570
C         -0.05250        1.90460        0.40760
C         -1.22310        1.19570        0.23270
C         -1.20240       -0.15230       -0.06400
C          0.02550       -0.76210       -0.17880
C         -2.49090       -0.90580       -0.25050
H          3.00570       -0.62790       -1.11290
H          3.14700       -0.44770        0.72350
H          2.39500       -1.88520       -0.00120
H          2.07390        1.84210        0.42770
H         -0.12590        2.95780        0.63820
H         -2.20740        1.66170        0.31990
H          0.04900       -1.80860       -0.40910
H         -3.29750       -0.18330       -0.35600
H         -2.61800       -1.59070        0.62860
H         -2.39990       -1.58670       -1.13600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

