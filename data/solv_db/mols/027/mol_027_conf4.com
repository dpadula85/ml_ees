%nproc=16
%mem=2GB
%chk=mol_027_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.49070       -0.76120        0.21070
C          1.20620       -0.02090        0.04840
C          1.14540        1.33480       -0.15410
C         -0.09770        1.94430       -0.29450
C         -1.27270        1.22640       -0.23640
C         -1.19360       -0.12830       -0.03340
C          0.03150       -0.73940        0.10650
C         -2.39580       -0.99910        0.04720
H          2.34770       -1.53560        0.99340
H          3.31620       -0.05630        0.44680
H          2.67070       -1.29330       -0.76260
H          2.07530        1.87880       -0.19660
H         -0.10460        3.02640       -0.45430
H         -2.23260        1.71270       -0.34720
H          0.09210       -1.82790        0.26920
H         -2.20290       -1.78110        0.83260
H         -2.59240       -1.55100       -0.88850
H         -3.28350       -0.42940        0.41280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_027_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

