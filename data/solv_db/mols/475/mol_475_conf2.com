%nproc=16
%mem=2GB
%chk=mol_475_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.36420       -0.17230       -0.04410
C         -0.11630       -0.22050       -0.41640
C         -0.90330        0.80510        0.34000
O         -0.54150       -1.51380       -0.22540
H          1.48490       -0.79470        0.87070
H          1.71500        0.86010        0.09640
H          1.91750       -0.71540       -0.85150
H         -0.25620        0.01090       -1.50990
H         -0.77660        1.79520       -0.18460
H         -0.51560        0.98360        1.37410
H         -1.96850        0.57480        0.34320
H         -1.40370       -1.61310        0.20750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

