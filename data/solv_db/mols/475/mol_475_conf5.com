%nproc=16
%mem=2GB
%chk=mol_475_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.20540       -0.68680        0.10860
C          0.15250        0.38590        0.19470
C         -1.24180       -0.13770       -0.00500
O          0.45390        1.42930       -0.70380
H          1.53340       -0.97160        1.13850
H          2.12830       -0.31160       -0.39480
H          0.82840       -1.62920       -0.35080
H          0.19920        0.87460        1.20910
H         -1.27290       -1.21850       -0.26150
H         -1.87210       -0.04290        0.92370
H         -1.76100        0.49270       -0.76510
H         -0.35320        1.81570       -1.09360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

