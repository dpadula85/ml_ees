%nproc=16
%mem=2GB
%chk=mol_475_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.08520       -0.71810       -0.02410
C          0.07360        0.38140       -0.00760
C         -1.33910       -0.10000       -0.24770
O          0.17290        1.12200        1.17470
H          2.01640       -0.36120       -0.54640
H          1.42170       -1.04410        0.97760
H          0.75230       -1.62620       -0.55720
H          0.30840        1.07570       -0.86490
H         -1.95810       -0.05120        0.68430
H         -1.88260        0.52570       -0.99030
H         -1.39130       -1.13380       -0.60010
H          0.74060        1.92980        1.00150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

