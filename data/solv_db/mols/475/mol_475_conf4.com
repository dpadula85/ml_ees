%nproc=16
%mem=2GB
%chk=mol_475_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.29750        0.03240        0.11560
C         -0.04260       -0.07350       -0.55350
C         -1.14130       -0.50660        0.36810
O         -0.36660        1.21770       -1.02210
H          2.05440       -0.65110       -0.34390
H          1.74590        1.05340        0.03260
H          1.19290       -0.17480        1.20890
H         -0.02830       -0.78310       -1.41990
H         -1.91580        0.29460        0.51030
H         -0.79600       -0.83590        1.36200
H         -1.72070       -1.37440       -0.05130
H         -0.27960        1.80140       -0.20670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

