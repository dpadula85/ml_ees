%nproc=16
%mem=2GB
%chk=mol_475_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.32660       -0.02180       -0.07070
C         -0.08130        0.50010        0.15330
C         -0.98620       -0.69610        0.26950
O         -0.38990        1.33360       -0.88980
H          1.33580       -1.12000       -0.26830
H          1.91120        0.12540        0.85490
H          1.80350        0.44440       -0.95890
H         -0.16100        1.09170        1.09350
H         -0.64870       -1.42420       -0.51800
H         -2.05460       -0.46020        0.20240
H         -0.72770       -1.19230        1.24530
H         -1.32770        1.41940       -1.11320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_475_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

