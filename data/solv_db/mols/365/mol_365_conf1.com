%nproc=16
%mem=2GB
%chk=mol_365_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90930        0.20710       -0.01700
C         -0.41410        0.16200       -0.03620
C          0.38140       -0.90020        0.28070
N          1.64210       -0.48960        0.12320
C          1.67560        0.78430       -0.27880
N          0.40240        1.17600       -0.37380
H         -2.27680        1.17130        0.38790
H         -2.22070        0.07650       -1.08890
H         -2.30540       -0.63100        0.60820
H          0.03900       -1.85980        0.59050
H          2.46500       -1.09790        0.29630
H          2.52090        1.40140       -0.49200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

