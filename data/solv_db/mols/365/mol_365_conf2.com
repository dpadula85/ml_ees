%nproc=16
%mem=2GB
%chk=mol_365_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.89830       -0.16030        0.17790
C          0.42340       -0.23830        0.06120
C         -0.37450        0.87480       -0.08390
N         -1.63920        0.49630       -0.16250
C         -1.67880       -0.83920       -0.07140
N         -0.40530       -1.29490        0.06690
H          2.16260        0.76190        0.73360
H          2.33260       -1.06700        0.63850
H          2.31320       -0.05610       -0.86070
H         -0.04700        1.91610       -0.13030
H         -2.46260        1.11900       -0.27520
H         -2.52270       -1.51240       -0.09420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

