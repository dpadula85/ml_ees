%nproc=16
%mem=2GB
%chk=mol_365_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90990        0.21530        0.01140
C          0.42490        0.23530       -0.06380
C         -0.36530       -0.84150        0.28150
N         -1.65620       -0.49880        0.10130
C         -1.66560        0.76960       -0.34830
N         -0.39780        1.22990       -0.45350
H          2.21630        0.16380        1.08080
H          2.35200        1.10140       -0.50520
H          2.25000       -0.73400       -0.45490
H         -0.04050       -1.81170        0.64000
H         -2.46340       -1.13350        0.28940
H         -2.56420        1.30410       -0.57860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

