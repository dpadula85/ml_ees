%nproc=16
%mem=2GB
%chk=mol_365_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88330       -0.16600        0.21740
C         -0.42040       -0.20380        0.09890
C          0.37870        0.87760       -0.27450
N          1.66390        0.48010       -0.27570
C          1.68580       -0.81030        0.08420
N          0.42340       -1.22330        0.31080
H         -2.27070        0.85100        0.46540
H         -2.26590       -0.87090        1.00050
H         -2.31800       -0.43330       -0.78200
H         -0.00120        1.84460       -0.51210
H          2.45980        1.10030       -0.51930
H          2.54810       -1.44600        0.18660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_365_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

