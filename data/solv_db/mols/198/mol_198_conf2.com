%nproc=16
%mem=2GB
%chk=mol_198_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.96230        0.23230        0.04910
C          2.61010       -1.11120       -0.01380
C          1.30690       -1.53450       -0.04950
C          0.29480       -0.60450       -0.02270
C         -1.02230       -0.98690       -0.05690
C         -2.04310       -0.07300       -0.03090
C         -1.75090        1.26710        0.03110
C         -0.42130        1.67180        0.06630
C          0.59160        0.74220        0.03950
C          1.92250        1.13740        0.07440
N         -3.39340       -0.47900       -0.06660
H          4.01150        0.54660        0.07660
H          3.44950       -1.81870       -0.03320
H          1.10710       -2.59670       -0.09800
H         -1.26120       -2.03380       -0.10550
H         -2.57380        1.96400        0.05040
H         -0.22270        2.73260        0.11480
H          2.18100        2.19540        0.12370
H         -3.80120       -0.93460        0.77180
H         -3.94750       -0.31650       -0.92060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_198_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_198_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_198_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

