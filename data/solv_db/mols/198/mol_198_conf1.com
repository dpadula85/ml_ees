%nproc=16
%mem=2GB
%chk=mol_198_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.96330        0.03710        0.36200
C          2.66020       -1.04870       -0.43310
C          1.33500       -1.30330       -0.76160
C          0.30780       -0.49860       -0.31410
C         -1.01880       -0.77680       -0.65920
C         -2.05220        0.02240       -0.21630
C         -1.72970        1.11360        0.58480
C         -0.42220        1.39210        0.92800
C          0.61390        0.59390        0.48600
C          1.93680        0.84210        0.80970
N         -3.41230       -0.22690       -0.54520
H          3.96640        0.30310        0.66190
H          3.46050       -1.68060       -0.78510
H          1.08530       -2.14770       -1.38160
H         -1.23610       -1.63630       -1.28600
H         -2.53840        1.72300        0.92010
H         -0.19960        2.24710        1.55220
H          2.15050        1.69560        1.43190
H         -3.67260       -0.69660       -1.43970
H         -4.19790        0.04560        0.08530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_198_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_198_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_198_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

