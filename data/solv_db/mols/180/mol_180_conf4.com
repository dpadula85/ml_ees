%nproc=16
%mem=2GB
%chk=mol_180_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26710       -0.57200       -0.61900
C         -1.61040        0.10750        0.56570
C         -0.14750       -0.12530        0.54340
C          0.43330       -1.04990        1.39260
C          1.80200       -1.26210        1.35030
C          2.58240       -0.54720        0.45690
C          2.02560        0.38050       -0.39980
C          0.64740        0.57470       -0.33610
C          0.00410        1.57220       -1.21950
H         -2.92560       -1.41820       -0.29530
H         -2.93310        0.11610       -1.18580
H         -1.48760       -1.02530       -1.29350
H         -2.09540       -0.28850        1.47210
H         -1.84680        1.20590        0.45370
H         -0.22290       -1.58050        2.06670
H          2.24990       -1.97670        2.01260
H          3.64180       -0.73070        0.44490
H          2.60030        0.95830       -1.11070
H         -0.53430        2.35350       -0.60590
H          0.79280        2.15100       -1.75450
H         -0.70890        1.15670       -1.93870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

