%nproc=16
%mem=2GB
%chk=mol_180_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36100       -0.53180        0.25200
C          1.42980       -0.15740       -0.89230
C          0.04170       -0.25730       -0.50540
C         -0.69460       -1.38800       -0.86330
C         -2.02090       -1.51360       -0.42490
C         -2.55710       -0.50760        0.35190
C         -1.83750        0.61780        0.71150
C         -0.52570        0.72110        0.26410
C          0.20630        1.93010        0.58030
H          2.87760        0.32470        0.70030
H          1.81750       -1.07150        1.05430
H          3.18310       -1.21400       -0.12050
H          1.77350        0.81080       -1.26640
H          1.62960       -0.88440       -1.71380
H         -0.24250       -2.16060       -1.47210
H         -2.59130       -2.38880       -0.70150
H         -3.59980       -0.62130        0.68720
H         -2.29890        1.39480        1.33040
H          0.13490        2.11190        1.67880
H         -0.32550        2.78190        0.05720
H          1.23870        2.00310        0.29220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

