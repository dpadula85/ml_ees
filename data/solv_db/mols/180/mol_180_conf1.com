%nproc=16
%mem=2GB
%chk=mol_180_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43390       -0.09460        0.42630
C          1.53860       -0.03570       -0.79000
C          0.12870       -0.32170       -0.46890
C         -0.42470       -1.56310       -0.69010
C         -1.73780       -1.88430       -0.33550
C         -2.52620       -0.92420        0.26610
C         -1.96380        0.30820        0.47730
C         -0.66860        0.64130        0.12830
C         -0.12460        2.01790        0.37510
H          3.36250       -0.63350        0.16290
H          2.72660        0.92210        0.78430
H          1.99590       -0.62290        1.28200
H          1.89210       -0.72620       -1.57650
H          1.62130        0.98540       -1.19930
H          0.15560       -2.35220       -1.16790
H         -2.15420       -2.86650       -0.52200
H         -3.54680       -1.11960        0.56820
H         -2.56220        1.08850        0.96160
H         -0.98020        2.65310        0.72480
H          0.20470        2.49320       -0.57800
H          0.62930        2.03480        1.17120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

