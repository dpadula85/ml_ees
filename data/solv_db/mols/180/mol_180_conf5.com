%nproc=16
%mem=2GB
%chk=mol_180_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.59750       -0.65930        0.13220
C          1.54800        0.38810        0.15650
C          0.17920       -0.12660        0.00840
C         -0.09010       -1.48830       -0.14230
C         -1.39270       -1.90030       -0.27570
C         -2.46900       -1.04550       -0.27080
C         -2.19480        0.29940       -0.12140
C         -0.88000        0.75410        0.01710
C         -0.58910        2.20200        0.17710
H          3.55900       -0.18020        0.47960
H          2.73570       -1.01880       -0.91310
H          2.42290       -1.49740        0.81640
H          1.57860        1.00100        1.08870
H          1.72700        1.08020       -0.71100
H          0.75390       -2.16510       -0.14730
H         -1.58710       -2.95060       -0.39080
H         -3.49520       -1.39230       -0.37850
H         -2.99300        1.02220       -0.10890
H          0.03550        2.35320        1.08120
H          0.04340        2.50580       -0.68620
H         -1.48970        2.81850        0.18880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

