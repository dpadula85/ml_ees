%nproc=16
%mem=2GB
%chk=mol_180_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.39360        0.13520        0.42610
C         -1.53240        0.19150       -0.83620
C         -0.18600       -0.21280       -0.48280
C          0.20020       -1.52330       -0.75150
C          1.41760       -2.03560       -0.40260
C          2.33820       -1.23090        0.25600
C          1.96270        0.06400        0.52200
C          0.73350        0.58170        0.16720
C          0.44220        1.99060        0.41230
H         -2.65360        1.12970        0.80500
H         -3.35180       -0.39170        0.21590
H         -1.87490       -0.40510        1.25400
H         -1.97530       -0.52490       -1.55940
H         -1.63840        1.22460       -1.21200
H         -0.48130       -2.20090       -1.26870
H          1.66570       -3.07380       -0.64200
H          3.31710       -1.63400        0.54080
H          2.65470        0.71940        1.03270
H          1.13370        2.33070        1.23980
H          0.77870        2.61880       -0.46570
H         -0.55700        2.24660        0.74940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_180_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

