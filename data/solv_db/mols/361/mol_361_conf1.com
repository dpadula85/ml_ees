%nproc=16
%mem=2GB
%chk=mol_361_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.46250        0.28750       -0.10660
C          1.97220        0.20740       -0.05870
C          1.19000        1.33830        0.04360
C         -0.18610        1.25150        0.08720
C         -0.84800        0.03160        0.03100
C         -2.22500       -0.06190        0.07580
C         -2.84780       -1.31150        0.01740
C         -2.09000       -2.45990       -0.08540
C         -0.72010       -2.31400       -0.12640
C         -0.06530       -1.10020       -0.07140
C          1.31650       -1.00490       -0.11480
C         -0.99940        2.46710        0.19690
H          3.88820       -0.02440        0.86980
H          3.75890        1.32730       -0.30730
H          3.78720       -0.39350       -0.90510
H          1.69010        2.30100        0.08880
H         -2.82620        0.83570        0.15790
H         -3.92770       -1.35810        0.05440
H         -2.60310       -3.40210       -0.12790
H         -0.10210       -3.19670       -0.20690
H          1.92180       -1.89780       -0.19540
H         -1.33140        2.64250        1.25210
H         -1.85060        2.49520       -0.51060
H         -0.36450        3.33990       -0.05840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

