%nproc=16
%mem=2GB
%chk=mol_361_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.34180       -0.81940        0.32200
C         -1.89520       -0.42050        0.18200
C         -1.52070        0.89950        0.14410
C         -0.19720        1.25640        0.01580
C          0.76850        0.25830       -0.07610
C          2.08690        0.65940       -0.20430
C          3.06770       -0.32240       -0.29780
C          2.70540       -1.65270       -0.26090
C          1.38960       -2.03530       -0.13310
C          0.39770       -1.06730       -0.03850
C         -0.93310       -1.39910        0.09050
C          0.21500        2.67920       -0.02630
H         -3.83750       -0.82780       -0.67070
H         -3.37120       -1.81310        0.81860
H         -3.81570       -0.02110        0.93340
H         -2.29800        1.65050        0.21820
H          2.33720        1.71590       -0.23050
H          4.09690       -0.00330       -0.39780
H          3.44410       -2.43600       -0.33140
H          1.05650       -3.06380       -0.09960
H         -1.19450       -2.44980        0.11730
H         -0.66610        3.29450        0.24440
H          0.51090        3.00340       -1.04760
H          0.99460        2.91500        0.72830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

