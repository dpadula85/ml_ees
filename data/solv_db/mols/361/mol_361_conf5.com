%nproc=16
%mem=2GB
%chk=mol_361_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.41260        0.43360        0.28670
C          1.93580        0.20450        0.15520
C          1.44860       -1.07210       -0.07580
C          0.09000       -1.27910       -0.19600
C         -0.77120       -0.20640       -0.08390
C         -2.13490       -0.38280       -0.19960
C         -3.00020        0.68600       -0.08830
C         -2.55270        1.96960        0.14130
C         -1.19160        2.15280        0.25780
C         -0.32560        1.08400        0.14660
C          1.04240        1.25050        0.26100
C         -0.44680       -2.64850       -0.44410
H          3.71640        1.37590       -0.21020
H          3.72650        0.43430        1.35080
H          3.90570       -0.39160       -0.27110
H          2.09590       -1.92430       -0.16730
H         -2.47560       -1.39640       -0.38010
H         -4.06790        0.52580       -0.18230
H         -3.26060        2.80070        0.22550
H         -0.80470        3.14280        0.43730
H          1.47570        2.24260        0.44400
H         -0.88610       -2.69430       -1.43750
H          0.33730       -3.40860       -0.25840
H         -1.26920       -2.89930        0.28820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

