%nproc=16
%mem=2GB
%chk=mol_361_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.35890       -0.83250        0.20870
C          1.91860       -0.42350        0.11300
C          1.54820        0.90390        0.03430
C          0.21570        1.25350       -0.05250
C         -0.78200        0.30800       -0.06400
C         -2.12420        0.63860       -0.15070
C         -3.13060       -0.30430       -0.16290
C         -2.74750       -1.62840       -0.08360
C         -1.40410       -1.98650        0.00440
C         -0.41770       -1.02130        0.01450
C          0.92340       -1.38060        0.10230
C         -0.14470        2.69440       -0.13470
H          3.41200       -1.74580        0.87180
H          3.91550       -0.04800        0.78230
H          3.81170       -1.01660       -0.76750
H          2.33520        1.64820        0.04310
H         -2.38370        1.67410       -0.21050
H         -4.17370       -0.04740       -0.23030
H         -3.49250       -2.41610       -0.08860
H         -1.08650       -3.02440        0.06740
H          1.14150       -2.42620        0.16060
H         -0.65650        2.93780       -1.09630
H         -0.79830        2.91630        0.73160
H          0.76130        3.32670       -0.09250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

