%nproc=16
%mem=2GB
%chk=mol_361_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.42750        0.19650        0.09130
C         -1.95220        0.05260        0.10570
C         -1.30720       -1.18010        0.02760
C          0.05950       -1.28960        0.04260
C          0.83240       -0.17170        0.13620
C          2.21000       -0.22040        0.15590
C          2.94810        0.94110        0.25210
C          2.33710        2.18160        0.33130
C          0.95710        2.22220        0.31100
C          0.20340        1.07280        0.21540
C         -1.17020        1.18180        0.20020
C          0.67580       -2.62840       -0.04390
H         -3.82600        0.24390       -0.95550
H         -3.77560        1.04560        0.70220
H         -3.83320       -0.72200        0.59080
H         -1.88890       -2.08650       -0.04760
H          2.68210       -1.17700        0.09480
H          4.01650        0.84850        0.26330
H          2.94050        3.07460        0.40580
H          0.49760        3.19980        0.37370
H         -1.60090        2.17020        0.26430
H          0.98690       -2.78680       -1.10190
H          1.51700       -2.76190        0.67180
H         -0.08230       -3.40680        0.25270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_361_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

