%nproc=16
%mem=2GB
%chk=mol_160_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.47820       -1.33640        0.43250
C         -2.16980       -1.72280        0.63540
C         -1.11180       -0.85690        0.40350
C         -1.33260        0.43010       -0.04120
C         -2.63960        0.82100       -0.24550
C         -3.69220       -0.04920       -0.01200
Cl        -5.35730        0.46050       -0.27640
O         -0.25600        1.28120       -0.26710
C          1.05400        0.86150       -0.05290
C          2.09780        1.72240       -0.28390
C          3.40170        1.34680       -0.08490
C          3.64120        0.06050        0.36060
C          2.59750       -0.80240        0.59230
C          1.29150       -0.41060        0.38770
O          0.20890       -1.24010        0.60600
Cl         2.93340       -2.42650        1.15660
Cl         5.29490       -0.42990        0.61760
Cl         4.75830        2.43130       -0.37360
H         -4.32940       -1.98870        0.60420
H         -1.98830       -2.73030        0.98400
H         -2.82840        1.84340       -0.59960
H          1.90450        2.73500       -0.63480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_160_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_160_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_160_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

