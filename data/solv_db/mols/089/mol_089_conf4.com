%nproc=16
%mem=2GB
%chk=mol_089_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.23200        0.48360        0.45080
C         -1.51860        0.58380       -0.34220
F         -2.44340       -0.22370        0.29330
F         -1.32430        0.05700       -1.60330
F         -1.92320        1.88250       -0.45870
O          0.21970       -0.82800        0.48630
C          1.45710       -0.96910       -0.10020
F          1.46370       -1.81850       -1.17440
F          2.39360       -1.35350        0.85960
Cl        -0.42320        1.05740        2.10680
H          0.51830        1.10440       -0.04480
H          1.81230        0.02410       -0.47330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

