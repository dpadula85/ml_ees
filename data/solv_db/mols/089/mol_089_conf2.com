%nproc=16
%mem=2GB
%chk=mol_089_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.31650        0.07900        0.62010
C         -1.65110       -0.03370       -0.07460
F         -2.58200        0.74640        0.55200
F         -1.49230        0.37480       -1.37280
F         -2.04350       -1.35940        0.01940
O          0.58090       -0.71600       -0.03960
C          1.68040       -0.03010       -0.54800
F          1.86350       -0.26140       -1.87740
F          2.81850       -0.50570        0.10710
Cl        -0.48410       -0.46670        2.30890
H         -0.01830        1.11870        0.63630
H          1.64460        1.05400       -0.33170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

