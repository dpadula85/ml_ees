%nproc=16
%mem=2GB
%chk=mol_089_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.49930        0.39450        0.01180
C         -1.44660       -0.24350        0.97400
F         -2.70670       -0.25400        0.37920
F         -1.56100        0.39290        2.16800
F         -1.11310       -1.59160        1.14660
O          0.78330        0.50510        0.39420
C          1.74390       -0.09120       -0.36800
F          2.37230       -1.03830        0.42670
F          2.73380        0.76780       -0.80650
Cl        -1.13900        2.00090       -0.42540
H         -0.54250       -0.21410       -0.93510
H          1.37490       -0.62850       -1.25850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

