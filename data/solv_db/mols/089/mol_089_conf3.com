%nproc=16
%mem=2GB
%chk=mol_089_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.40040        0.32570        0.56420
C          1.58830       -0.31000       -0.16690
F          1.13110       -1.43990       -0.80950
F          2.62270       -0.58270        0.68860
F          1.97480        0.60340       -1.12490
O         -0.52820        0.62300       -0.44230
C         -1.72690       -0.02670       -0.28260
F         -2.78470        0.85750       -0.23510
F         -1.93150       -0.81200       -1.42040
Cl         1.01960        1.80840        1.32580
H          0.00800       -0.37460        1.30470
H         -1.77380       -0.67200        0.59850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_089_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

