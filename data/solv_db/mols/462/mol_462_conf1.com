%nproc=16
%mem=2GB
%chk=mol_462_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.89610       -1.01360       -0.04780
C         -0.45310       -1.29840       -0.04110
C         -1.38110       -0.29100        0.00690
C         -0.90630        1.00460        0.04760
C          0.44410        1.29000        0.04090
C          1.38010        0.27680       -0.00740
Br         3.26170        0.64210       -0.01800
Br        -3.26160       -0.61060        0.01920
H          1.65130       -1.80040       -0.08570
H         -0.78600       -2.32550       -0.07380
H         -1.64170        1.80450        0.08570
H          0.79660        2.32150        0.07360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_462_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_462_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_462_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

