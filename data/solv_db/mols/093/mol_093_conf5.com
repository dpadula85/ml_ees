%nproc=16
%mem=2GB
%chk=mol_093_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.18280        0.40060       -0.02540
C          0.09070       -0.65450       -0.12940
C         -1.24010        0.02200        0.10420
H          0.73630        1.42470       -0.06130
H          1.88420        0.35580       -0.88710
H          1.67330        0.28590        0.95010
H          0.24990       -1.45020        0.64190
H          0.16200       -1.07760       -1.15040
H         -1.08920        1.01820        0.60530
H         -1.87310       -0.57940        0.79250
H         -1.77700        0.25440       -0.84040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

