%nproc=16
%mem=2GB
%chk=mol_093_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.24200       -0.20120       -0.04610
C         -0.01340        0.67590       -0.07060
C         -1.18330       -0.27540        0.04240
H          2.13200        0.34050        0.25080
H          1.03390       -0.96630        0.75450
H          1.28370       -0.74150       -0.98960
H         -0.05240        1.30540       -0.97740
H          0.06340        1.36930        0.78990
H         -1.66170       -0.08710        1.01550
H         -0.89780       -1.33290       -0.02540
H         -1.94640       -0.08670       -0.74400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

