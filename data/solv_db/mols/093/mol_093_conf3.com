%nproc=16
%mem=2GB
%chk=mol_093_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.26660        0.17840       -0.09910
C         -0.02710       -0.56090       -0.07810
C         -1.22560        0.32700       -0.38340
H          1.24340        0.96710       -0.88010
H          2.08650       -0.52550       -0.42640
H          1.59040        0.56030        0.89270
H         -0.23610       -1.08940        0.86240
H         -0.01080       -1.32250       -0.91200
H         -2.10050       -0.36800       -0.43310
H         -1.15460        0.78190       -1.39690
H         -1.43240        1.05170        0.41390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

