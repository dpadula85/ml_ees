%nproc=16
%mem=2GB
%chk=mol_093_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.25340        0.23320       -0.17450
C          0.01790       -0.12700        0.53290
C          1.22580       -0.14980       -0.32720
H         -1.61550       -0.53920       -0.87440
H         -1.16480        1.24240       -0.64860
H         -2.04770        0.29770        0.62690
H          0.14440        0.61810        1.36520
H         -0.12260       -1.10370        1.02920
H          2.09670        0.27900        0.25070
H          1.16370        0.45630       -1.24620
H          1.55560       -1.20710       -0.53400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

