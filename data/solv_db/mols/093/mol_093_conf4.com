%nproc=16
%mem=2GB
%chk=mol_093_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.20190       -0.05720       -0.21590
C         -0.02150       -0.08740        0.70390
C         -1.20950        0.08240       -0.25020
H          1.22790        0.97830       -0.62960
H          1.03160       -0.72910       -1.09240
H          2.10390       -0.28670        0.35120
H         -0.09290       -1.04970        1.23110
H          0.06820        0.77270        1.37840
H         -0.92480        0.92280       -0.93350
H         -1.25210       -0.86560       -0.83020
H         -2.13270        0.31950        0.28710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_093_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

