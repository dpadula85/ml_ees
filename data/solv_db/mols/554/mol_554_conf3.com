%nproc=16
%mem=2GB
%chk=mol_554_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.74760        0.00640        0.24660
C         -0.36030       -0.57520        0.33220
C          0.61710        0.51070       -0.03080
C          2.03440        0.02550        0.02610
O          2.35890       -1.08760        0.32300
H         -2.45230       -0.69830       -0.22840
H         -2.08120        0.31370        1.27760
H         -1.75690        0.94080       -0.36880
H         -0.28130       -1.35170       -0.48160
H         -0.13510       -1.06750        1.28170
H          0.45040        0.86850       -1.05480
H          0.56010        1.35570        0.68880
H          2.79380        0.75900       -0.22470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

