%nproc=16
%mem=2GB
%chk=mol_554_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.74030       -0.18130        0.27790
C         -0.38370        0.44690        0.09820
C          0.71550       -0.54820       -0.18210
C          1.97120        0.23150       -0.33350
O          2.76320        0.45060        0.53490
H         -1.93120       -0.37790        1.35110
H         -2.48890        0.59430       -0.02560
H         -1.93480       -1.05000       -0.38210
H         -0.36270        1.20390       -0.70290
H         -0.10040        0.95670        1.05610
H          0.55950       -1.15370       -1.08320
H          0.76820       -1.21000        0.72730
H          2.16440        0.63700       -1.33600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

