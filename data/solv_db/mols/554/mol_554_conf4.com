%nproc=16
%mem=2GB
%chk=mol_554_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.75990       -0.22580       -0.05270
C          0.29900       -0.50950        0.08680
C         -0.60140        0.68450       -0.05600
C         -1.99710        0.20700        0.16350
O         -2.60710        0.53840        1.16140
H          2.29710       -1.18950       -0.02840
H          2.16590        0.38100        0.78260
H          1.97140        0.23730       -1.03690
H          0.08590       -1.05120        0.99880
H          0.01590       -1.19030       -0.76050
H         -0.57240        1.08920       -1.09010
H         -0.32680        1.47060        0.69670
H         -2.49040       -0.44150       -0.56600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

