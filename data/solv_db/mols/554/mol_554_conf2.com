%nproc=16
%mem=2GB
%chk=mol_554_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.75500        0.13240        0.09690
C          0.31430        0.49850        0.12510
C         -0.64520       -0.65000       -0.00650
C         -2.01580       -0.09070        0.02000
O         -2.65300       -0.04330       -1.00530
H          2.36320        1.05340        0.03610
H          2.04330       -0.35390        1.05150
H          2.04290       -0.48500       -0.78190
H          0.12640        0.98430        1.12020
H          0.11050        1.24160       -0.66790
H         -0.45260       -1.33050       -0.83830
H         -0.53390       -1.25780        0.94130
H         -2.45510        0.30100        0.92610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

