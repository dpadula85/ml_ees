%nproc=16
%mem=2GB
%chk=mol_554_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.30600       -0.73580       -0.04630
C         -0.64780        0.61140        0.11610
C          0.77930        0.60310       -0.38740
C          1.64420       -0.33650        0.32680
O          1.75820       -0.24620        1.55130
H         -2.27050       -0.68480        0.49480
H         -0.64730       -1.53850        0.29040
H         -1.55390       -0.86550       -1.13250
H         -0.68080        0.92620        1.17850
H         -1.24220        1.33710       -0.45650
H          1.13240        1.67660       -0.29880
H          0.83260        0.38120       -1.47480
H          2.20180       -1.12820       -0.16150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_554_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

