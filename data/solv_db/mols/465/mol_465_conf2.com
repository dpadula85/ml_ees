%nproc=16
%mem=2GB
%chk=mol_465_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.39510        1.64760       -0.48050
C         -0.85510        1.08090       -0.46740
C         -1.02610       -0.21300       -0.01740
C          0.06220       -0.93810        0.42000
C          1.30900       -0.35180        0.39920
C          1.50110        0.94850       -0.04980
O          2.40190       -1.08080        0.83880
N         -2.30770       -0.83540        0.01120
O         -3.28690       -0.17320       -0.38600
O         -2.43830       -2.11870        0.46370
H          0.57970        2.66320       -0.82580
H         -1.71600        1.63600       -0.80680
H         -0.04850       -1.96320        0.78000
H          2.50530        1.34780       -0.04000
H          2.92420       -1.64990        0.16070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_465_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_465_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_465_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

