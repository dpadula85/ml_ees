%nproc=16
%mem=2GB
%chk=mol_465_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.57260        1.66510       -0.31160
C         -0.72760        1.21690       -0.22350
C         -1.02410       -0.12200       -0.02430
C         -0.00880       -1.06460        0.09410
C          1.28390       -0.61700        0.00620
C          1.57810        0.71200       -0.19160
O          2.32530       -1.54210        0.12160
N         -2.36540       -0.55740        0.06280
O         -3.27010        0.28480       -0.04300
O         -2.65900       -1.87600        0.25900
H          0.82000        2.71440       -0.46810
H         -1.54810        1.94080       -0.31360
H         -0.26220       -2.11310        0.25040
H          2.61290        1.06660       -0.26130
H          2.67250       -1.70830        1.04300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_465_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_465_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_465_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

