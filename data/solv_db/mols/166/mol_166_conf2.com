%nproc=16
%mem=2GB
%chk=mol_166_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.48590       -1.03030        0.13700
C          2.23490       -0.23600        0.15540
O          2.08070        0.81000        0.83520
O          1.10530       -0.60510       -0.60420
C         -0.06740        0.15790       -0.57380
C         -0.22370        1.25970       -1.40880
C         -1.37060        1.98410       -1.35520
C         -2.40000        1.66590       -0.49390
C         -2.22100        0.57130        0.31930
C         -1.06120       -0.21310        0.30590
C         -0.94890       -1.39960        1.13560
O          0.03460       -2.16130        1.11330
O         -1.98490       -1.70390        1.99670
H          3.17780       -2.08220       -0.13320
H          4.03110       -1.00870        1.07940
H          4.14080       -0.64890       -0.66750
H          0.61220        1.46390       -2.06250
H         -1.46780        2.84170       -2.01950
H         -3.32030        2.25300       -0.45330
H         -3.00600        0.27250        1.02300
H         -2.83150       -2.19110        1.67100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

