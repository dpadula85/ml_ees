%nproc=16
%mem=2GB
%chk=mol_166_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.39880        0.61160        0.31600
C          2.08100       -0.00170        0.13560
O          1.97860       -1.08810       -0.49010
O          0.88220        0.53960        0.61880
C         -0.33960       -0.08960        0.41590
C         -0.70560       -1.11120        1.30410
C         -1.90090       -1.76380        1.13410
C         -2.72150       -1.39760        0.08570
C         -2.36980       -0.39950       -0.78190
C         -1.15970        0.27360       -0.61860
C         -0.81430        1.40390       -1.44630
O         -1.51680        1.79230       -2.42000
O          0.33180        2.13360       -1.19390
H          3.55220        1.05110        1.32560
H          3.63620        1.37970       -0.47680
H          4.18120       -0.16990        0.20620
H         -0.03690       -1.35480        2.09570
H         -2.15600       -2.55160        1.83990
H         -3.65760       -1.92270       -0.03520
H         -2.98710       -0.08090       -1.62000
H          0.32390        2.74610       -0.39450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

