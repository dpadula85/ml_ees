%nproc=16
%mem=2GB
%chk=mol_166_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.56190       -0.20530       -0.67020
C         -2.21070       -0.37860       -0.06970
O         -2.00160       -0.62500        1.12890
O         -1.07850       -0.25950       -0.89730
C          0.17440       -0.41640       -0.35980
C          0.70370       -1.67150       -0.23340
C          1.97370       -1.88630        0.31140
C          2.67790       -0.77130        0.72020
C          2.17120        0.49370        0.60250
C          0.89930        0.67010        0.05120
C          0.39940        2.02750       -0.14820
O         -0.71190        2.22850       -0.71010
O          1.18860        3.07580        0.30200
H         -3.54180        0.70730       -1.27790
H         -3.81410       -1.10620       -1.29440
H         -4.31080       -0.04860        0.13490
H          0.10620       -2.51530       -0.57150
H          2.37580       -2.88690        0.40340
H          3.67320       -0.88270        1.15390
H          2.73750        1.36450        0.92970
H          2.15060        3.08600       -0.06180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

