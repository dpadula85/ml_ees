%nproc=16
%mem=2GB
%chk=mol_166_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.43850       -1.07160        0.22480
C          2.16840       -0.35480        0.30650
O          1.92640        0.32700        1.33380
O          1.17700       -0.36870       -0.69370
C         -0.00790        0.36680       -0.49340
C         -0.04720        1.71920       -0.79200
C         -1.22820        2.38870       -0.57000
C         -2.35560        1.74000       -0.06200
C         -2.27650        0.38700        0.22110
C         -1.09810       -0.30870        0.00310
C         -1.01570       -1.74690        0.18540
O         -0.03580       -2.45440       -0.07320
O         -2.16140       -2.37640        0.70390
H          3.55260       -1.89990        0.93940
H          4.26380       -0.34160        0.47180
H          3.66930       -1.47210       -0.77830
H          0.80540        2.24820       -1.18510
H         -1.31690        3.42890       -0.78090
H         -3.27790        2.26330        0.11350
H         -3.11470       -0.17610        0.61740
H         -3.06530       -2.29790        0.30790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

