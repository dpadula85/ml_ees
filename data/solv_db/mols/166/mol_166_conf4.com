%nproc=16
%mem=2GB
%chk=mol_166_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.36090        0.95580        0.43710
C          2.08230        0.32240        0.01320
O          1.87860       -0.06320       -1.16720
O          1.02820        0.12110        0.91710
C         -0.15240       -0.47310        0.48910
C         -0.32520       -1.83990        0.45380
C         -1.47670       -2.46250        0.03440
C         -2.51820       -1.65930       -0.37460
C         -2.40530       -0.28820       -0.36030
C         -1.21880        0.30470        0.07470
C         -1.17000        1.74300        0.13430
O         -2.15530        2.41820       -0.25430
O         -0.08340        2.43210        0.60580
H          3.21130        1.85600        1.05440
H          3.95230        0.16280        0.95290
H          3.92790        1.17830       -0.50590
H          0.49260       -2.46070        0.77450
H         -1.59120       -3.54860        0.01500
H         -3.45110       -2.12770       -0.71650
H         -3.24070        0.33760       -0.68790
H         -0.14570        3.09120        1.37550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_166_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

