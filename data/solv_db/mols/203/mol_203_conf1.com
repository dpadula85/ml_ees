%nproc=16
%mem=2GB
%chk=mol_203_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.71710        0.97320       -0.79130
C         -2.45490        0.36960        0.57030
C         -1.39080       -0.69460        0.55310
C         -1.84910       -1.78910       -0.38230
C         -0.04730       -0.19130        0.30910
C          0.19660        1.15050        0.29420
C          1.50460        1.59830        0.13160
C          2.55360        0.72930       -0.01720
C          2.31010       -0.65820       -0.00610
C          1.01350       -1.08360        0.16410
O          0.72370       -2.42130        0.21000
N          3.45190       -1.46230       -0.11690
O          4.53680       -1.02850        0.39060
O          3.55350       -2.70950       -0.65820
N          1.76110        2.99450        0.14550
O          0.82390        3.82220        0.27760
O          3.04240        3.43650        0.01420
H         -2.62840        2.08100       -0.77870
H         -3.73210        0.74500       -1.16090
H         -2.01490        0.58000       -1.55870
H         -2.24940        1.15810        1.33840
H         -3.39810       -0.12100        0.89370
H         -1.38200       -1.11210        1.60610
H         -2.93790       -1.66090       -0.64040
H         -1.31170       -1.81610       -1.33680
H         -1.78810       -2.77000        0.14210
H         -0.56180        1.92380        0.42980
H          3.57990        1.07820       -0.15190
H          1.41190       -3.12160        0.12910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

