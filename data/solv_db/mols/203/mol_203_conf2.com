%nproc=16
%mem=2GB
%chk=mol_203_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.70750        0.41780       -0.24480
C          2.25140        0.69200       -0.25520
C          1.38590       -0.52720       -0.13100
C          1.60440       -1.30500        1.12610
C         -0.04440       -0.12330       -0.16030
C         -0.42000        1.18840       -0.27200
C         -1.76620        1.56240       -0.28320
C         -2.73490        0.60400       -0.17320
C         -2.38650       -0.74040       -0.05050
C         -1.07100       -1.08140       -0.05610
O         -0.61840       -2.37710        0.01780
N         -3.42990       -1.67950        0.04770
O         -4.52680       -1.39620       -0.52940
O         -3.30020       -2.85420        0.71280
N         -2.14590        2.91870       -0.40350
O         -1.26420        3.78290       -0.50340
O         -3.45120        3.28100       -0.41120
H          4.21630        1.05860        0.50830
H          3.96650       -0.65440       -0.11950
H          4.14510        0.70880       -1.21850
H          2.01420        1.40300        0.57920
H          1.99190        1.24750       -1.17910
H          1.56360       -1.17990       -1.01120
H          1.71410       -2.38820        0.91190
H          2.47690       -0.98440        1.71160
H          0.72680       -1.23730        1.82950
H          0.29710        1.99420       -0.34300
H         -3.78660        0.87800       -0.18120
H         -1.11540       -3.20900        0.08170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

