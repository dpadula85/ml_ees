%nproc=16
%mem=2GB
%chk=mol_203_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.23450        1.62180        1.03620
C         -2.43730        0.70510       -0.12340
C         -1.55640       -0.50190       -0.00320
C         -1.83020       -1.40970       -1.21650
C         -0.10690       -0.19010        0.07130
C          0.44300        1.00790       -0.30080
C          1.81050        1.24920       -0.35970
C          2.63160        0.19610       -0.02530
C          2.15420       -1.03640        0.35370
C          0.77280       -1.20870        0.39790
O          0.32380       -2.45830        0.79460
N          3.01100       -2.12560        0.66990
O          4.12050       -2.21340        0.16350
O          2.55610       -3.07640        1.54420
N          2.30990        2.52250       -0.72200
O          1.52270        3.43400       -1.00340
O          3.65800        2.72890       -0.74540
H         -3.11350        1.52450        1.71960
H         -1.34900        1.33660        1.64090
H         -2.13570        2.67900        0.72530
H         -2.31530        1.23540       -1.09200
H         -3.48220        0.33350       -0.13720
H         -1.86270       -1.07130        0.88490
H         -2.03860       -0.71720       -2.06120
H         -0.97560       -2.05640       -1.42070
H         -2.72900       -2.00330       -0.95880
H         -0.21500        1.80260       -0.63240
H          3.70690        0.34010       -0.05590
H         -0.63900       -2.64860        0.85580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

