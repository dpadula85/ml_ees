%nproc=16
%mem=2GB
%chk=mol_203_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.74660        0.90490        0.90610
C         -2.44410       -0.50380        0.70180
C         -1.32730       -0.94240       -0.17090
C         -1.62100       -0.44120       -1.63020
C         -0.01910       -0.33910        0.19280
C          0.26420        0.99500       -0.09860
C          1.52960        1.47580       -0.38290
C          2.57450        0.56640       -0.44000
C          2.37780       -0.76640       -0.15200
C          1.09450       -1.15960        0.14800
O          0.89380       -2.48110        0.53290
N          3.42800       -1.55800        0.43030
O          4.10490       -1.06180        1.33610
O          3.46220       -2.88950        0.19910
N          1.84600        2.87530       -0.21960
O          0.94360        3.70140       -0.03990
O          3.14380        3.29650       -0.11680
H         -3.07200        1.46940        0.02390
H         -1.96340        1.46330        1.49280
H         -3.62310        0.93850        1.62910
H         -2.24830       -0.97300        1.70820
H         -3.35790       -1.04340        0.33070
H         -1.31510       -2.02260       -0.26170
H         -2.53940       -0.96810       -1.99500
H         -1.70480        0.64040       -1.67400
H         -0.77980       -0.82030       -2.22900
H         -0.53980        1.67370       -0.30960
H          3.56920        0.88400       -0.75340
H          0.06950       -2.91430        0.84190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_203_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

