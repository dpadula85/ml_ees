%nproc=16
%mem=2GB
%chk=mol_202_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.42350        0.40280       -1.12700
C          0.68980       -0.06160       -0.04210
C          1.42290       -0.55790        1.01580
C          2.80890       -0.60240        1.01570
C          3.49450       -0.12870       -0.08870
C          2.80240        0.37380       -1.15860
Cl         3.71090        0.96740       -2.54210
Cl         5.23160       -0.18260       -0.09250
Cl         3.67900       -1.24300        2.38080
C         -0.75230        0.02940       -0.02410
C         -1.40490        1.20310       -0.32580
C         -2.77300        1.33330       -0.26910
C         -3.52720        0.22260        0.11190
C         -2.91300       -0.97270        0.42120
C         -1.52040       -1.04830        0.34530
Cl        -0.73970       -2.57930        0.66980
Cl        -3.90450       -2.31750        0.88790
Cl        -5.25770        0.36750        0.19040
Cl        -3.55850        2.84920       -0.66230
H          0.94060        0.81300       -2.00650
H          0.94200       -0.91240        1.91680
H         -0.79490        2.04430       -0.61700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

