%nproc=16
%mem=2GB
%chk=mol_202_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.48020        0.68820       -0.99320
C          0.73230        0.09390        0.03170
C          1.37140       -0.48010        1.11120
C          2.74550       -0.46820        1.18700
C          3.49400        0.11730        0.18050
C          2.85120        0.68850       -0.89890
Cl         3.78740        1.43150       -2.18040
Cl         5.23880        0.10290        0.32900
Cl         3.49100       -1.21360        2.58870
C         -0.72520        0.02940       -0.05380
C         -1.31260       -0.44820       -1.20640
C         -2.70240       -0.57860       -1.31200
C         -3.50280       -0.23010       -0.26180
C         -2.93320        0.25390        0.90670
C         -1.56540        0.37770        0.99460
Cl        -0.88440        1.06030        2.45510
Cl        -3.90730        0.71210        2.28240
Cl        -5.23760       -0.38800       -0.37660
Cl        -3.43760       -1.18990       -2.78880
H          0.96380        1.13720       -1.84240
H          0.75980       -0.97050        1.88520
H         -0.70710       -0.72590       -2.03780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

