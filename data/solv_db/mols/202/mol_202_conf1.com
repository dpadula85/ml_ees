%nproc=16
%mem=2GB
%chk=mol_202_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.49130        0.94460       -0.61240
C          0.71990       -0.02150       -0.00000
C          1.41050       -1.06690        0.62390
C          2.78780       -1.10030        0.60750
C          3.51350       -0.10230       -0.02520
C          2.86110        0.94240       -0.64860
Cl         3.77050        2.22620       -1.46160
Cl         5.25990       -0.09860       -0.07600
Cl         3.69270       -2.39150        1.37720
C         -0.74010        0.00070       -0.01400
C         -1.42240       -0.18050        1.17450
C         -2.80450       -0.13210        1.24580
C         -3.56390        0.10400        0.10650
C         -2.89430        0.28500       -1.08400
C         -1.50160        0.23210       -1.13860
Cl        -0.70020        0.41390       -2.67530
Cl        -3.86950        0.58210       -2.50790
Cl        -5.30980        0.16270        0.22000
Cl        -3.65550       -0.36290        2.75340
H          0.96300        1.76900       -1.07100
H          0.85170       -1.83750        1.11260
H         -0.86020       -0.36870        2.09340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

