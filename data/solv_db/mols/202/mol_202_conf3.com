%nproc=16
%mem=2GB
%chk=mol_202_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29260        1.26840       -0.28460
C         -0.70290        0.07350        0.06250
C         -1.55360       -0.95760        0.42010
C         -2.92660       -0.81110        0.43070
C         -3.49200        0.39840        0.07560
C         -2.65910        1.42960       -0.28010
Cl        -3.38560        2.95570       -0.72740
Cl        -5.25250        0.59740        0.08450
Cl        -4.00490       -2.11440        0.88270
C          0.74100       -0.12670        0.01450
C          1.43200       -0.66010        1.10120
C          2.80080       -0.78420        1.05360
C          3.53180       -0.39350       -0.05000
C          2.87530        0.13230       -1.13170
C          1.48330        0.25400       -1.07680
Cl         0.62550        0.84940       -2.49030
Cl         3.72020        0.65380       -2.57290
Cl         5.27530       -0.56580       -0.07190
Cl         3.68460       -1.45540        2.41740
H         -0.67540        2.12940       -0.53770
H         -1.13320       -1.89350        0.69570
H          0.90860       -0.97940        1.98500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_202_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

