%nproc=16
%mem=2GB
%chk=mol_245_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.52670        0.51340        0.03460
C          2.81740       -0.78190        0.22930
O          1.43580       -0.73000        0.01880
C          0.55200        0.06380        0.71990
O          1.01850        0.80150        1.62090
C         -0.88320        0.05110        0.42790
C         -1.38980       -0.75070       -0.55130
C         -2.73260       -0.78580       -0.85050
C         -3.57210        0.02640       -0.12120
C         -3.07180        0.83370        0.86280
C         -1.72090        0.86700        1.16160
H          3.56390        1.09470        0.98870
H          4.59510        0.28140       -0.20200
H          3.14350        1.11020       -0.80700
H          3.22820       -1.55860       -0.46040
H          3.01730       -1.14710        1.26840
H         -0.72450       -1.39650       -1.13140
H         -3.09130       -1.42830       -1.62590
H         -4.63510       -0.02290       -0.38170
H         -3.76860        1.45570        1.41080
H         -1.30840        1.50300        1.94030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

