%nproc=16
%mem=2GB
%chk=mol_245_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.66680        0.88650        0.37480
C          2.77970        0.36840       -0.70630
O          1.46840        0.24670       -0.21010
C          0.39390       -0.20730       -0.95530
O          0.59050       -0.52800       -2.13210
C         -0.92030       -0.28860       -0.32590
C         -1.12990        0.07230        0.99020
C         -2.36000        0.00520        1.60810
C         -3.46350       -0.43990        0.91020
C         -3.26680       -0.80330       -0.40570
C         -2.03420       -0.73490       -1.02080
H          4.40040        0.08880        0.67700
H          4.18720        1.79150       -0.00420
H          3.12010        1.21040        1.29080
H          2.74320        0.96970       -1.61480
H          3.08890       -0.66830       -1.02180
H         -0.25960        0.42140        1.53400
H         -2.51630        0.28970        2.63960
H         -4.44400       -0.50060        1.37930
H         -4.14330       -1.15390       -0.94940
H         -1.90110       -1.02570       -2.05750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

