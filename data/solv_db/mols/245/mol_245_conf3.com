%nproc=16
%mem=2GB
%chk=mol_245_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.53490       -0.33550        0.51650
C          2.82200        0.51700       -0.49000
O          1.45640        0.27210       -0.59170
C          0.55360        0.40750        0.44360
O          1.00740        0.77750        1.54800
C         -0.88590        0.12790        0.25660
C         -1.78030        0.26510        1.29460
C         -3.12140        0.00900        1.13520
C         -3.57710       -0.39960       -0.11300
C         -2.71640       -0.54650       -1.16630
C         -1.37850       -0.27920       -0.96390
H          3.33360       -0.03820        1.56670
H          3.40460       -1.42110        0.33890
H          4.63890       -0.14310        0.36760
H          3.01610        1.57690       -0.22610
H          3.28610        0.34810       -1.48360
H         -1.40380        0.58680        2.26660
H         -3.83750        0.11790        1.96260
H         -4.62900       -0.59160       -0.20590
H         -3.06420       -0.86420       -2.14100
H         -0.65980       -0.38680       -1.78800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

