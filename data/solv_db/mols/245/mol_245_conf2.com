%nproc=16
%mem=2GB
%chk=mol_245_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.55470       -0.47630        0.33230
C         -2.80640        0.07890       -0.82960
O         -1.42900       -0.05660       -0.77690
C         -0.58180        0.44580        0.17890
O         -1.10700        1.10310        1.11350
C          0.87540        0.24050        0.13720
C          1.67930        0.76650        1.12170
C          3.05850        0.56020        1.06320
C          3.64440       -0.16170        0.03840
C          2.83210       -0.68170       -0.93830
C          1.44940       -0.47840       -0.88520
H         -3.62520       -1.59820        0.25370
H         -3.22380       -0.14300        1.31410
H         -4.62420       -0.13510        0.22860
H         -3.16620       -0.45850       -1.74560
H         -3.14740        1.14250       -0.96980
H          1.20540        1.32140        1.90490
H          3.68770        0.97070        1.83170
H          4.71640       -0.30630        0.01780
H          3.29030       -1.24990       -1.74570
H          0.82660       -0.88400       -1.64490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

