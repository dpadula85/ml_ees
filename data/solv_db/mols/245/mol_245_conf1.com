%nproc=16
%mem=2GB
%chk=mol_245_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.84490        0.35710       -0.71320
C         -2.82110       -0.22280        0.20870
O         -1.49860        0.07410       -0.16580
C         -0.46990       -0.41330        0.61790
O         -0.75540       -1.10110        1.63210
C          0.93710       -0.16820        0.32520
C          1.92810       -0.67470        1.13530
C          3.27640       -0.44520        0.86270
C          3.67470        0.29860       -0.22940
C          2.68040        0.80350       -1.03720
C          1.34620        0.57940       -0.77200
H         -4.80080       -0.17200       -0.55270
H         -4.03880        1.43450       -0.54270
H         -3.56080        0.15390       -1.77550
H         -2.97760       -1.34040        0.20180
H         -3.04240        0.12340        1.25240
H          1.64730       -1.27090        2.01250
H          4.05720       -0.85530        1.51720
H          4.72910        0.47590       -0.43920
H          2.97790        1.38580       -1.89230
H          0.55570        0.97780       -1.40950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_245_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

