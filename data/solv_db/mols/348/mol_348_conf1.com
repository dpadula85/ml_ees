%nproc=16
%mem=2GB
%chk=mol_348_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.25830        0.11190        0.45070
C          1.34340        0.24360       -0.28260
F          2.50880        0.54480        0.32370
F          1.25110        0.07270       -1.61510
C         -1.08770       -0.21800       -0.09290
F         -1.11650       -0.37900       -1.44680
F         -1.95480        0.75860        0.36390
F         -1.51410       -1.41290        0.49120
F          0.31160        0.27830        1.80780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

