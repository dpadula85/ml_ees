%nproc=16
%mem=2GB
%chk=mol_348_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01150        0.32650       -0.46570
C         -1.19790        0.60740       -0.07640
F         -1.70540        0.07220        1.04880
F         -1.99270        1.44600       -0.78380
C          0.86700       -0.58650        0.30820
F          2.01680        0.04190        0.77500
F          0.26630       -1.19530        1.37240
F          1.23960       -1.62810       -0.55720
F          0.49490        0.91590       -1.62110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

