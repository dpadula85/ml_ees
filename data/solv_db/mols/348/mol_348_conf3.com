%nproc=16
%mem=2GB
%chk=mol_348_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.24230        0.48680        0.09240
C         -1.20620       -0.07930       -0.58820
F         -2.36440        0.62900       -0.82200
F         -1.11190       -1.32130       -1.06480
C          1.01340       -0.22410        0.36940
F          1.12430       -0.30290        1.76140
F          1.10230       -1.48170       -0.17780
F          2.07160        0.55060       -0.13070
F         -0.38670        1.74280        0.56020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_348_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

