%nproc=16
%mem=2GB
%chk=mol_253_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.39050        1.00330       -0.15580
C         -1.20780       -0.47550       -0.10410
C          0.13030       -0.90760        0.42840
C          1.25940       -0.40190       -0.38400
C          2.17850        0.37900        0.15320
H         -0.91060        1.54860        0.67090
H         -2.49970        1.20470       -0.07860
H         -1.09370        1.46190       -1.12810
H         -1.30410       -0.88700       -1.12650
H         -1.97970       -0.92190        0.57090
H          0.15780       -2.01870        0.36220
H          0.26990       -0.66450        1.50180
H          1.30530       -0.69530       -1.43680
H          2.09850        0.64610        1.19370
H          2.98650        0.72890       -0.46710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

