%nproc=16
%mem=2GB
%chk=mol_253_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11010        0.36420        0.60110
C          0.89660        0.21600       -0.26370
C         -0.26490       -0.42250        0.46290
C         -1.39140       -0.50680       -0.50630
C         -2.53840        0.09850       -0.27150
H          2.74510        1.19100        0.18960
H          1.89280        0.54960        1.65320
H          2.71330       -0.56330        0.48930
H          0.54130        1.18900       -0.67410
H          1.10970       -0.43640       -1.14570
H          0.08330       -1.44840        0.75750
H         -0.57320        0.12180        1.35350
H         -1.30590       -1.07220       -1.44220
H         -3.37470        0.06120       -0.94640
H         -2.64360        0.65830        0.64120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

