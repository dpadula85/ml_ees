%nproc=16
%mem=2GB
%chk=mol_253_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88940       -0.26430       -0.11550
C         -0.79070        0.48950        0.58240
C          0.36850        0.80090       -0.32930
C          0.99260       -0.42570       -0.88630
C          2.27000       -0.71980       -0.66570
H         -2.39840        0.37210       -0.86350
H         -1.53500       -1.21250       -0.55410
H         -2.64950       -0.51970        0.65520
H         -0.44320       -0.05120        1.47170
H         -1.20180        1.46210        0.92370
H          1.13040        1.32310        0.28700
H          0.07040        1.52010       -1.11890
H          0.43000       -1.11750       -1.48770
H          2.74130       -1.60120       -1.05860
H          2.90480       -0.05610       -0.05880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

