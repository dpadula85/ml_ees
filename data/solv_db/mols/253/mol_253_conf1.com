%nproc=16
%mem=2GB
%chk=mol_253_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.15300        0.07280        0.14590
C         -0.87600       -0.43630       -0.47060
C          0.29130        0.10500        0.33440
C          1.59180       -0.34830       -0.20490
C          2.50670        0.48710       -0.63130
H         -2.79790       -0.76110        0.53180
H         -1.95570        0.76610        1.00400
H         -2.78320        0.64090       -0.56260
H         -0.83360       -0.16930       -1.52850
H         -0.89110       -1.55560       -0.37870
H          0.11870       -0.26860        1.38600
H          0.19670        1.19390        0.38890
H          1.79640       -1.40420       -0.24930
H          2.34740        1.55190       -0.60640
H          3.44150        0.12580       -1.01730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

