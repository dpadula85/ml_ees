%nproc=16
%mem=2GB
%chk=mol_253_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87170        0.24090       -0.21770
C         -0.86540       -0.81920        0.06120
C          0.29630       -0.38010        0.89440
C          1.09170        0.70060        0.32740
C          2.37660        0.50690        0.07290
H         -1.65330        0.86300       -1.10720
H         -2.07460        0.89420        0.67780
H         -2.84780       -0.26210       -0.43170
H         -0.56450       -1.27680       -0.88940
H         -1.39220       -1.62600        0.64600
H          0.93540       -1.27130        1.05370
H         -0.01370       -0.06630        1.92590
H          0.71560        1.69240        0.08330
H          3.04380        1.25750       -0.34960
H          2.82400       -0.45360        0.28890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_253_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

