%nproc=16
%mem=2GB
%chk=mol_016_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17890        0.35220       -0.95290
C         -1.56230       -0.51240        0.20940
C         -0.53890       -1.49220        0.62950
C          0.80500       -1.33160       -0.01160
C          1.43210       -0.00400        0.16020
C          0.52040        1.07730        0.64800
C         -0.44770        1.56500       -0.38980
O          2.04230        0.46960       -1.02310
H         -0.53830       -0.16000       -1.69290
H         -2.09210        0.67520       -1.52520
H         -1.84690        0.15650        1.07180
H         -2.53900       -1.00710       -0.07220
H         -0.90670       -2.51930        0.33610
H         -0.47360       -1.52430        1.73950
H          1.47130       -2.11810        0.45130
H          0.73580       -1.63290       -1.08600
H          2.28310       -0.06330        0.90090
H          0.01510        0.81210        1.58540
H          1.17910        1.95990        0.90210
H          0.00370        2.16580       -1.17610
H         -1.22670        2.16640        0.14300
H          2.86330        0.96540       -0.84730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

