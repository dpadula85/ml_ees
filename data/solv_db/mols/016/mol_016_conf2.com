%nproc=16
%mem=2GB
%chk=mol_016_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.00430        1.31140       -0.50230
C         -1.84200        0.21800        0.15050
C         -1.24740       -1.10450       -0.34400
C         -0.01700       -1.32790        0.54030
C          1.22300       -0.87110       -0.14170
C          1.38440        0.58760       -0.28730
C          0.28590        1.41760        0.30300
O          2.31790       -1.44000        0.53950
H         -1.56110        2.25930       -0.53280
H         -0.71960        1.00960       -1.53010
H         -1.71260        0.24930        1.26240
H         -2.90510        0.30830       -0.07410
H         -1.95990       -1.93280       -0.26970
H         -0.90920       -0.93980       -1.40170
H         -0.11100       -0.81520        1.51140
H          0.06330       -2.42050        0.71320
H          1.23560       -1.34450       -1.16430
H          2.33590        0.90150        0.20310
H          1.47320        0.88860       -1.36560
H          0.12440        1.26740        1.37200
H          0.60610        2.49570        0.18910
H          2.93960       -0.71770        0.82920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

