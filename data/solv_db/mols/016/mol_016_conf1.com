%nproc=16
%mem=2GB
%chk=mol_016_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.98090        1.39290       -0.17570
C          1.45320        0.31660        0.75960
C          1.26430       -1.07450        0.20390
C          0.12920       -1.08580       -0.78760
C         -1.14930       -0.86220       -0.01530
C         -1.26330        0.52370        0.54700
C         -0.49190        1.55240       -0.23800
O         -2.20400       -1.16480       -0.90160
H          1.41870        2.36090        0.18240
H          1.46770        1.20950       -1.16510
H          0.99720        0.40450        1.75580
H          2.55850        0.46000        0.88530
H          2.19850       -1.49440       -0.22080
H          0.95800       -1.76780        1.03770
H          0.08940       -2.08570       -1.23790
H          0.23050       -0.35510       -1.59160
H         -1.13970       -1.57990        0.84660
H         -1.05270        0.54450        1.62680
H         -2.35090        0.81550        0.44980
H         -0.85400        2.55970        0.06450
H         -0.80660        1.43630       -1.30670
H         -2.43370       -2.10630       -0.71920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

