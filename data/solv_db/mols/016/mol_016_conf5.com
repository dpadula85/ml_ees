%nproc=16
%mem=2GB
%chk=mol_016_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.38160        0.52300        0.59730
C          0.48970        1.70920        0.32110
C         -0.66110        1.30080       -0.59200
C         -1.41700        0.23410        0.08720
C         -1.03290       -1.16630       -0.18050
C          0.34960       -1.52280       -0.43480
C          1.38160       -0.45200       -0.52850
O         -1.57710       -1.93730        0.88410
H          1.11690       -0.01200        1.52320
H          2.40780        0.91990        0.72920
H          1.02690        2.58130       -0.04840
H          0.02300        1.98810        1.28990
H         -0.33070        1.08210       -1.61650
H         -1.30600        2.21050       -0.66600
H         -2.51030        0.37410       -0.17290
H         -1.37850        0.43810        1.18090
H         -1.66350       -1.51830       -1.05530
H          0.71230       -2.23170        0.37170
H          0.47750       -2.14670       -1.37220
H          2.38710       -0.97580       -0.48660
H          1.42300        0.04350       -1.53620
H         -1.29970       -1.44170        1.70510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

