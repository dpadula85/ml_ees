%nproc=16
%mem=2GB
%chk=mol_016_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.68970        0.37840       -0.21310
C         -1.42990       -0.99960        0.24710
C         -0.06650       -1.24580        0.80880
C          1.02430       -1.15330       -0.20970
C          1.62360        0.21170       -0.32410
C          0.59940        1.17020       -0.87810
C         -0.58940        1.35760       -0.00540
O          2.19760        0.68000        0.83630
H         -2.02700        0.39290       -1.28990
H         -2.58850        0.76680        0.34870
H         -1.56040       -1.70720       -0.61510
H         -2.16820       -1.36500        1.01450
H         -0.06390       -2.31140        1.18570
H          0.12410       -0.63390        1.72350
H          1.84540       -1.85370        0.07750
H          0.60810       -1.42840       -1.19190
H          2.44110        0.14840       -1.08770
H          0.33470        0.86880       -1.89630
H          1.12150        2.16960       -0.94680
H         -0.31790        1.39620        1.06190
H         -1.01200        2.38040       -0.23760
H          1.59350        0.77740        1.59170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_016_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

