%nproc=16
%mem=2GB
%chk=mol_239_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.67010        0.19250        0.86700
C          2.45360        0.77040        0.14350
C          1.31840       -0.19830        0.38570
C          0.03340        0.23850       -0.27250
C         -1.01770       -0.78020        0.02580
C         -2.35450       -0.46960       -0.57140
O         -2.89160        0.74760       -0.12510
N         -4.11800        1.12280       -0.61620
O         -4.29830        1.63450       -1.72590
O         -5.21390        0.91170        0.18640
H          3.35890       -0.23050        1.83940
H          4.10750       -0.62850        0.25730
H          4.40260        1.01020        0.97240
H          2.73710        0.82180       -0.92230
H          2.18530        1.75900        0.51020
H          1.61850       -1.19240       -0.05490
H          1.13820       -0.33190        1.46360
H          0.15570        0.45640       -1.34410
H         -0.25770        1.20950        0.21330
H         -1.05950       -0.92770        1.11870
H         -0.67810       -1.74980       -0.41530
H         -3.05530       -1.27390       -0.25380
H         -2.23470       -0.48150       -1.68210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

