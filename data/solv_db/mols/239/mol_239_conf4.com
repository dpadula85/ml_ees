%nproc=16
%mem=2GB
%chk=mol_239_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.41550        1.13150       -0.04930
C          2.46400       -0.01280       -0.25520
C          1.28680        0.02170        0.65710
C          0.22730       -0.88860       -0.01050
C         -1.05170       -0.12520       -0.05550
C         -2.11110       -0.79240        0.85340
O         -2.91410        0.26790        1.30530
N         -4.20040        0.49420        1.02000
O         -4.36730        1.70390        0.55950
O         -5.24220       -0.35320        0.91030
H          4.36000        0.87490       -0.56350
H          3.61710        1.41360        0.97250
H          2.98200        1.98960       -0.63760
H          2.99330       -0.99360       -0.07870
H          2.12940       -0.03580       -1.30430
H          1.61350       -0.45780        1.63630
H          0.89720        0.99570        0.91740
H          0.09040       -1.78480        0.66670
H          0.55190       -1.27290       -0.97980
H         -1.47700       -0.30120       -1.11730
H         -1.01410        0.92730        0.14080
H         -2.69700       -1.52950        0.31370
H         -1.55330       -1.27240        1.64810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

