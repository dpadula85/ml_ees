%nproc=16
%mem=2GB
%chk=mol_239_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.37450       -0.11220       -0.07400
C         -2.35010        0.76600        0.55010
C         -0.98480        0.13250        0.65500
C         -0.40480       -0.25640       -0.65690
C          0.94440       -0.88500       -0.53310
C          1.95540        0.02330        0.10830
O          3.21320       -0.61670        0.19820
N          4.24050        0.07860        0.75870
O          4.63300       -0.21090        1.89100
O          4.83480        1.09190        0.07240
H         -3.57880        0.24880       -1.11520
H         -4.33100        0.01990        0.47330
H         -3.13450       -1.18900       -0.05000
H         -2.28500        1.72620       -0.00180
H         -2.65520        1.02470        1.59490
H         -0.32910        0.94270        1.09290
H         -0.97250       -0.69520        1.38120
H         -0.29220        0.64290       -1.30730
H         -1.06490       -0.95000       -1.21980
H          1.34530       -1.12990       -1.55400
H          0.92820       -1.85960        0.00360
H          2.07870        0.93640       -0.51370
H          1.58380        0.27100        1.12860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

