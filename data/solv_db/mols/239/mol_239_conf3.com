%nproc=16
%mem=2GB
%chk=mol_239_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.79300       -0.68560        1.26250
C         -2.40580       -0.64340       -0.18950
C         -1.67180        0.62870       -0.50280
C         -0.26430        0.66200        0.00270
C          0.68730       -0.22400       -0.73910
C          2.05390       -0.04510       -0.09160
O          3.08770       -0.61290       -0.86030
N          4.37910       -0.30320       -0.47320
O          4.88910        0.75250       -0.92620
O          4.69300       -0.59420        0.84150
H         -3.47490        0.12030        1.56750
H         -1.91770       -0.64740        1.93770
H         -3.32180       -1.64320        1.48340
H         -1.79660       -1.53110       -0.41560
H         -3.35970       -0.60830       -0.77470
H         -1.77890        0.83330       -1.57910
H         -2.22250        1.44790        0.02520
H          0.11480        1.69230       -0.05180
H         -0.27650        0.36600        1.06330
H          0.43410       -1.30580       -0.60350
H          0.74080        0.05160       -1.81890
H          2.23320        1.05610       -0.07990
H          1.97070       -0.47810        0.92240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

