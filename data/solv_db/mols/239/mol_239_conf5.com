%nproc=16
%mem=2GB
%chk=mol_239_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.53100        0.42580        0.69860
C         -2.09830        0.59630        0.21250
C         -1.58400       -0.79210       -0.10370
C         -0.15620       -0.71490       -0.59730
C          0.77720       -0.10840        0.40680
C          2.16250       -0.09300       -0.21400
O          3.12130        0.46600        0.66470
N          4.40780        0.54010        0.23100
O          5.22680       -0.31450        0.56990
O          4.78520        1.56400       -0.57960
H         -4.03800       -0.19920       -0.09080
H         -3.48800       -0.21140        1.60870
H         -4.04510        1.38000        0.85600
H         -1.51260        1.08490        1.01540
H         -2.14480        1.20670       -0.70280
H         -2.19490       -1.16310       -0.95760
H         -1.69540       -1.43810        0.76640
H          0.22530       -1.71930       -0.85440
H         -0.13100       -0.08080       -1.50820
H          0.85260       -0.76150        1.30660
H          0.49930        0.89740        0.73830
H          2.44440       -1.12460       -0.52180
H          2.11690        0.55950       -1.12310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_239_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

