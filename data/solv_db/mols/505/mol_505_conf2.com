%nproc=16
%mem=2GB
%chk=mol_505_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.73780        0.04650       -0.25900
C         -0.71690       -0.23660        0.04510
Br        -1.55280        1.25630        0.95040
Cl         1.69080        0.35770        1.18480
H          1.14970       -0.81150       -0.79720
H          0.74050        0.99240       -0.86890
H         -0.84560       -1.14530        0.67440
H         -1.20340       -0.45950       -0.92960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

