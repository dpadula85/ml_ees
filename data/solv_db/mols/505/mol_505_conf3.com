%nproc=16
%mem=2GB
%chk=mol_505_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.71140       -0.27840       -0.07140
C         -0.66950        0.36450        0.06510
Br        -1.95300       -1.09780       -0.13800
Cl         2.00790        0.90790        0.08710
H          0.80130       -0.99610        0.76880
H          0.78040       -0.77840       -1.05320
H         -0.82850        0.77050        1.07170
H         -0.85010        1.10770       -0.73010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

