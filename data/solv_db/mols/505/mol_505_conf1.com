%nproc=16
%mem=2GB
%chk=mol_505_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.79500       -0.10450       -0.09670
C         -0.72330       -0.28880        0.05440
Br        -1.48580        1.43560       -0.43730
Cl         1.19330        1.21830        1.04410
H          1.34470       -1.02130        0.11650
H          0.98430        0.29950       -1.11830
H         -0.98990       -0.51760        1.09610
H         -1.11820       -1.02120       -0.65890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

