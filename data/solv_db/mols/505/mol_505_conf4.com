%nproc=16
%mem=2GB
%chk=mol_505_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.72220       -0.19320        0.01890
C         -0.72670        0.25360       -0.02430
Br        -1.79630       -1.35670       -0.23640
Cl         1.83500        1.16630        0.21000
H          1.00100       -0.72760       -0.90800
H          0.87280       -0.86430        0.89510
H         -0.88020        0.95240       -0.86010
H         -1.02780        0.76970        0.90480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_505_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

