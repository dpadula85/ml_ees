%nproc=16
%mem=2GB
%chk=mol_279_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76380        1.12460       -0.00820
C          0.61160        1.21700       -0.00320
C          1.41220        0.09490        0.00510
C          0.76190       -1.12270        0.00820
C         -0.61570       -1.21920        0.00320
C         -1.40690       -0.09490       -0.00510
Cl        -3.15740       -0.23050       -0.01160
Cl         3.16100        0.22940        0.01160
H         -1.40820        2.01630       -0.01480
H          1.07220        2.21050       -0.00610
H          1.39820       -2.00870        0.01470
H         -1.06520       -2.21670        0.00620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_279_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_279_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_279_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

