%nproc=16
%mem=2GB
%chk=mol_592_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00380        0.00570        0.00580
H         -0.59610       -0.44820       -0.79120
H         -0.47790        0.94990        0.31780
H          0.05480       -0.71960        0.83840
H          1.01530        0.21220       -0.37080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_592_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_592_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_592_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

