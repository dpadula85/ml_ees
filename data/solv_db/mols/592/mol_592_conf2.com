%nproc=16
%mem=2GB
%chk=mol_592_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01410       -0.00370       -0.01070
H          0.94700       -0.47070       -0.37630
H         -0.03940        1.06250       -0.28430
H         -0.02760       -0.09670        1.09790
H         -0.89410       -0.49140       -0.42660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_592_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_592_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_592_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

