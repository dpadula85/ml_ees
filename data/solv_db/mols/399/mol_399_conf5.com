%nproc=16
%mem=2GB
%chk=mol_399_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.93130       -0.36690        0.26080
C         -3.43350        0.89250       -0.41760
C         -2.07330        0.69630       -1.01840
C         -1.04670        0.31810        0.02150
C          0.30020        0.13380       -0.63770
C          1.36650       -0.24750        0.36920
C          2.65480       -0.40430       -0.38760
C          3.81660       -0.78690        0.49590
N          4.03040        0.23810        1.51060
H         -4.09300       -1.11850       -0.51790
H         -3.27030       -0.68630        1.08010
H         -4.92570       -0.08930        0.71940
H         -3.45640        1.71030        0.31290
H         -4.12880        1.13930       -1.24560
H         -2.16840       -0.08620       -1.80050
H         -1.79550        1.63420       -1.55010
H         -1.33860       -0.67880        0.42470
H         -1.02670        1.04910        0.82430
H          0.28220       -0.66810       -1.40310
H          0.61250        1.06820       -1.16190
H          1.07120       -1.15190        0.91540
H          1.46310        0.56570        1.14310
H          2.90610        0.56990       -0.85300
H          2.50560       -1.21110       -1.13410
H          4.72490       -0.76700       -0.14920
H          3.72810       -1.78690        0.93740
H          3.93670        1.16650        1.03730
H          3.28920        0.10800        2.22420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

