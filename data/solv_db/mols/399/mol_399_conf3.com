%nproc=16
%mem=2GB
%chk=mol_399_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.29180        1.09390        0.42440
C         -3.20390       -0.41130        0.36450
C         -1.71570       -0.75480        0.21940
C         -1.26920       -0.10330       -1.05010
C          0.16070       -0.37760       -1.31960
C          1.02790        0.15180       -0.20650
C          2.47380       -0.18600       -0.62790
C          3.40490        0.31130        0.43470
N          3.15710       -0.28780        1.72460
H         -3.33630        1.53470       -0.59020
H         -4.17640        1.39000        1.04270
H         -2.36420        1.45790        0.91040
H         -3.60930       -0.85010        1.27920
H         -3.76260       -0.72600       -0.53960
H         -1.52550       -1.82590        0.28550
H         -1.14700       -0.27290        1.06790
H         -1.40630        0.99340       -0.94360
H         -1.92390       -0.38700       -1.91670
H          0.38680       -1.45920       -1.47190
H          0.43650        0.20470       -2.24900
H          0.89400        1.23890       -0.10310
H          0.81060       -0.33480        0.77090
H          2.70460        0.36870       -1.56070
H          2.54210       -1.25890       -0.81430
H          3.24640        1.40300        0.54820
H          4.45080        0.11460        0.09070
H          3.63420        0.27210        2.46550
H          3.40200       -1.29940        1.76470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

