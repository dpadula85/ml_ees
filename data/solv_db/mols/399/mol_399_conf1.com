%nproc=16
%mem=2GB
%chk=mol_399_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.16020        0.79190        0.02790
C          2.65990        0.59550        0.17090
C          2.24080       -0.80190       -0.12060
C          0.76330       -0.98640        0.04300
C         -0.00650       -0.09290       -0.89140
C         -1.50870       -0.30040       -0.74310
C         -1.99510        0.00060        0.64870
C         -3.50840       -0.21960        0.73040
N         -4.21400        0.63320       -0.18760
H          4.45600        1.58060        0.74990
H          4.40530        1.20970       -0.99000
H          4.73600       -0.11220        0.26440
H          2.18730        1.30200       -0.54500
H          2.37180        0.85000        1.20680
H          2.72350       -1.53260        0.59430
H          2.54640       -1.15560       -1.12860
H          0.46930       -2.03980       -0.15080
H          0.43080       -0.77900        1.09490
H          0.23920       -0.34710       -1.94240
H          0.24900        0.96310       -0.65460
H         -1.72550       -1.33480       -1.02810
H         -1.98530        0.40790       -1.44380
H         -1.56230       -0.70780        1.39580
H         -1.78030        1.03100        0.96670
H         -3.75800       -1.27070        0.45370
H         -3.87990       -0.05810        1.75260
H         -3.63250        1.41840       -0.49550
H         -5.08210        0.95510        0.25540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

