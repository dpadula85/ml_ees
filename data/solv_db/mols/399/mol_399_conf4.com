%nproc=16
%mem=2GB
%chk=mol_399_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.02040       -0.20510        0.33620
C         -3.06080        0.91120       -0.01580
C         -1.71680        0.35960       -0.47270
C         -1.16150       -0.44860        0.66310
C          0.17820       -1.06230        0.35340
C          1.23110       -0.03380        0.02990
C          2.51930       -0.77300       -0.25990
C          3.64610        0.17200       -0.59780
N          3.86810        1.06050        0.52430
H         -3.93620       -1.07820       -0.32430
H         -3.89350       -0.53990        1.39060
H         -5.06920        0.15770        0.26120
H         -3.54540        1.53600       -0.77510
H         -2.94600        1.52970        0.90710
H         -1.09990        1.22260       -0.74460
H         -1.90810       -0.22380       -1.38110
H         -1.86960       -1.28030        0.85710
H         -1.08950        0.20410        1.54270
H          0.05630       -1.78480       -0.47810
H          0.48490       -1.64760        1.24470
H          1.36990        0.71320        0.80630
H          0.92380        0.43700       -0.94730
H          2.35520       -1.45160       -1.14180
H          2.83310       -1.43300        0.56540
H          4.56800       -0.46550       -0.68340
H          3.44690        0.71470       -1.54340
H          4.74440        1.63640        0.35090
H          3.09170        1.77270        0.53020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

