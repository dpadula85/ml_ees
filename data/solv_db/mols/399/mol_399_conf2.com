%nproc=16
%mem=2GB
%chk=mol_399_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.97610       -0.49640        1.15600
C          3.10160       -0.47430       -0.09670
C          1.63210       -0.34470        0.33950
C          0.84210       -0.32970       -0.93470
C         -0.66040       -0.21900       -0.73370
C         -0.98490        1.05880       -0.01630
C         -2.42050        1.31580        0.19670
C         -3.22940        0.37670        0.99150
N         -3.36300       -0.96100        0.52360
H          3.60820        0.28340        1.84070
H          5.01750       -0.28120        0.79160
H          3.89610       -1.50530        1.57290
H          3.36150        0.32870       -0.78370
H          3.27370       -1.43790       -0.60260
H          1.50790        0.54360        0.97670
H          1.38780       -1.23210        0.98230
H          1.21150        0.50720       -1.54820
H          1.09630       -1.26550       -1.48160
H         -1.10000       -0.24190       -1.75270
H         -1.03310       -1.10770       -0.17670
H         -0.38550        1.13320        0.94520
H         -0.57370        1.93640       -0.61030
H         -2.87430        1.53130       -0.81060
H         -2.50150        2.32120        0.72970
H         -4.28340        0.79990        1.02270
H         -2.90710        0.42940        2.06010
H         -4.12960       -0.99500       -0.18470
H         -3.46600       -1.67400        1.25520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_399_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

