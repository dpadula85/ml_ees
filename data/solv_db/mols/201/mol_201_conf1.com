%nproc=16
%mem=2GB
%chk=mol_201_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.25350       -0.94760        0.26070
C         -0.16770        0.01040       -0.18970
C         -0.85890        1.33120       -0.45270
O          0.75610        0.14610        0.83920
C          1.98180       -0.30950        0.37670
H         -0.77740       -1.76570        0.80840
H         -1.86160       -1.29040       -0.61200
H         -1.94930       -0.36530        0.89680
H          0.28870       -0.41480       -1.09700
H         -1.52720        1.15660       -1.33770
H         -1.53420        1.52630        0.40540
H         -0.14900        2.14410       -0.65630
H          2.75290       -0.21510        1.18130
H          2.33260        0.34120       -0.44720
H          1.96680       -1.34740        0.02410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

