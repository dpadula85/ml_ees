%nproc=16
%mem=2GB
%chk=mol_201_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31630        0.66930        0.65650
C         -0.20460       -0.08230       -0.07830
C         -0.70860       -0.42860       -1.45030
O          0.97240        0.55810       -0.00440
C          2.00390       -0.03910        0.65470
H         -1.30280        1.73550        0.37160
H         -1.22980        0.58650        1.73850
H         -2.30290        0.26670        0.34890
H         -0.10640       -1.05660        0.45510
H         -1.76940       -0.78330       -1.29870
H         -0.77570        0.41510       -2.13910
H         -0.18670       -1.31970       -1.87920
H          2.89370        0.62220        0.61970
H          1.71950       -0.12390        1.72610
H          2.31380       -1.02000        0.27910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

