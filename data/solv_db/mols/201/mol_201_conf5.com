%nproc=16
%mem=2GB
%chk=mol_201_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38590        0.45990        0.74510
C         -0.25710       -0.16870       -0.01590
C         -0.61870       -0.48200       -1.44130
O          0.79780        0.73400       -0.03900
C          1.92680        0.18910        0.58880
H         -1.08680        1.41920        1.24890
H         -1.76780       -0.18990        1.57330
H         -2.25540        0.72410        0.10370
H          0.08840       -1.09810        0.50570
H         -0.76250       -1.58660       -1.52990
H          0.21290       -0.24060       -2.14560
H         -1.53490        0.05780       -1.76360
H          2.22330       -0.74630        0.03970
H          2.72910        0.95350        0.47550
H          1.69080       -0.02520        1.65460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

