%nproc=16
%mem=2GB
%chk=mol_201_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.34360        1.43690       -0.77220
C         -0.12700        0.23750        0.11660
C         -1.49950       -0.25630        0.53270
O          0.51270       -0.80480       -0.52990
C          1.69220       -1.18640        0.09630
H         -0.90020        1.18050       -1.70730
H          0.59760        1.95760       -1.02750
H         -0.95130        2.16240       -0.18000
H          0.40270        0.58930        1.01560
H         -2.11390       -0.28120       -0.40940
H         -1.94880        0.34060        1.32380
H         -1.41610       -1.33250        0.85070
H          1.53640       -1.52890        1.14270
H          2.49280       -0.42810        0.00150
H          2.06620       -2.08690       -0.45360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

