%nproc=16
%mem=2GB
%chk=mol_201_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.89470        1.33730        0.34280
C         -0.21090        0.05090       -0.09950
C         -1.22500       -1.04010       -0.37960
O          0.75500       -0.34890        0.79580
C          2.01850       -0.34120        0.22450
H         -0.53930        2.21370       -0.23740
H         -1.97210        1.21270        0.11320
H         -0.78350        1.50260        1.43180
H          0.28840        0.27860       -1.06330
H         -1.21800       -1.35920       -1.43700
H         -2.23760       -0.62430       -0.14640
H         -1.09090       -1.92090        0.29210
H          2.24690        0.70890       -0.11610
H          2.08980       -0.99350       -0.67380
H          2.77350       -0.67660        0.95310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_201_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

