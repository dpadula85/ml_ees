%nproc=16
%mem=2GB
%chk=mol_483_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.34790        1.48400       -0.01380
C         -0.65270        0.23240       -0.53880
C         -1.45090       -0.94760       -0.05880
C          0.74260        0.24040        0.00750
O          1.45280       -0.90960       -0.44720
C          2.76800       -1.12160       -0.07160
O          3.30710       -0.27170        0.67720
H         -1.41630        1.36420        1.08310
H         -0.70380        2.36400       -0.29220
H         -2.35120        1.60280       -0.43350
H         -0.66240        0.23060       -1.64470
H         -1.34450       -1.11110        1.03750
H         -2.51720       -0.75370       -0.33810
H         -1.14880       -1.88030       -0.58120
H          1.25950        1.14820       -0.35460
H          0.70520        0.29620        1.10930
H          3.36050       -1.96730       -0.37870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

