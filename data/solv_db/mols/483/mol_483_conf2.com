%nproc=16
%mem=2GB
%chk=mol_483_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.43660       -1.38410        0.07890
C         -0.62660        0.04100       -0.27250
C         -1.84750        0.55440        0.52990
C          0.47740        0.99900       -0.04780
O          1.67810        0.86640       -0.66870
C          2.67150       -0.04140       -0.59190
O          2.60690       -1.00340        0.17720
H          0.10640       -1.94140       -0.70270
H         -1.45010       -1.91980        0.15290
H          0.01210       -1.57780        1.05060
H         -0.98910        0.15470       -1.35020
H         -2.59190       -0.23530        0.62110
H         -2.30600        1.42410        0.00720
H         -1.54750        0.86900        1.54010
H          0.08510        2.03710       -0.33430
H          0.58540        1.13070        1.08410
H          3.57230        0.02690       -1.19790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

