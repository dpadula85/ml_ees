%nproc=16
%mem=2GB
%chk=mol_483_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.49320       -0.90380       -0.93280
C         -0.40980       -0.32970       -0.03640
C         -0.98130        0.76750        0.85990
C          0.69510        0.30790       -0.83800
O          1.66690        0.82470        0.03420
C          2.44270        0.12300        0.92410
O          2.29780       -1.12100        0.98980
H         -1.78140       -1.93370       -0.59800
H         -2.37430       -0.24410       -0.83770
H         -1.20600       -0.93160       -1.98710
H         -0.07220       -1.12740        0.64690
H         -0.40330        0.83420        1.79830
H         -2.04420        0.62030        1.07590
H         -0.86100        1.77340        0.36240
H          0.22210        1.13250       -1.44430
H          1.12150       -0.38050       -1.59690
H          3.18070        0.58820        1.57970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

