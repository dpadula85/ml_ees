%nproc=16
%mem=2GB
%chk=mol_483_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72200        0.90670       -0.53630
C         -0.73370        0.29440        0.46350
C         -1.10050       -1.14560        0.72110
C          0.61750        0.32500       -0.21420
O          1.63810       -0.20830        0.58480
C          2.91840       -0.23460        0.07280
O          3.12370        0.21380       -1.07550
H         -1.24000        1.87130       -0.85230
H         -2.70820        1.03400       -0.09740
H         -1.69690        0.25570       -1.42120
H         -0.76270        0.87080        1.38480
H         -0.60540       -1.82680       -0.02090
H         -2.19100       -1.32870        0.60080
H         -0.77230       -1.50720        1.71680
H          0.59390       -0.24810       -1.15450
H          0.91930        1.36500       -0.48170
H          3.72170       -0.63730        0.66160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

