%nproc=16
%mem=2GB
%chk=mol_483_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29100       -0.61830        1.05800
C         -0.73610        0.40790        0.11840
C         -1.54850        0.36860       -1.17180
C          0.69940        0.13090       -0.26510
O          1.53920        0.11790        0.87200
C          2.88760       -0.12410        0.67740
O          3.28680       -0.31800       -0.49770
H         -0.64810       -0.61860        1.97700
H         -2.28400       -0.23630        1.40670
H         -1.37880       -1.62620        0.60700
H         -0.84010        1.39670        0.59170
H         -2.57760       -0.01640       -0.99440
H         -1.01030       -0.25190       -1.93590
H         -1.58320        1.41900       -1.55040
H          0.81290       -0.81600       -0.81500
H          1.08750        0.92960       -0.93010
H          3.58440       -0.14480        1.52970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_483_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

