%nproc=16
%mem=2GB
%chk=mol_132_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.44070        3.50010       -0.53350
C         -0.83440        2.22720        0.13590
C         -1.16570        1.01120       -0.71420
N         -0.63850       -0.24860       -0.20880
C         -1.38360       -0.53740        1.01610
C         -2.82260       -0.77040        0.70560
C         -3.36840       -1.83900       -0.13860
C         -3.50880       -1.99300        1.34390
C          0.72240       -0.62540       -0.43210
C          1.47260        0.27950       -1.18100
C          2.81080        0.52570       -0.79030
C          3.46160       -0.15010        0.21790
C          2.68620       -1.09010        0.88750
C          1.34750       -1.34150        0.58640
N          0.78970       -2.61750        0.89110
O          1.35920       -3.18720        1.86100
O         -0.20320       -3.29860        0.33550
C          4.94050       -0.14080        0.34960
F          5.36010       -0.86880       -0.80830
F          5.37510       -0.81590        1.41780
F          5.48510        1.08580        0.15040
N          1.17430        0.63070       -2.51100
O          1.51100        1.79100       -2.89650
O          0.39800       -0.01860       -3.39690
H         -0.68250        4.17960       -0.91150
H         -2.10320        3.16260       -1.34550
H         -2.12690        4.01920        0.18030
H          0.21910        2.45920        0.25380
H         -1.42140        2.13920        1.04190
H         -2.33110        1.00770       -0.63390
H         -1.13240        1.24210       -1.76690
H         -0.95960       -1.18920        1.77140
H         -1.42630        0.41320        1.67000
H         -3.48190        0.13030        0.82160
H         -4.29950       -1.60030       -0.69690
H         -2.77410       -2.62980       -0.63350
H         -4.54980       -1.83610        1.72560
H         -2.89760       -2.71340        1.85720
H          3.30390        1.34480       -1.30390
H          3.13520       -1.63740        1.68280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_132_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_132_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_132_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

