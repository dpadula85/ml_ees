%nproc=16
%mem=2GB
%chk=mol_233_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47490       -0.00790        0.42140
C         -1.23040       -0.22170       -0.39550
C          0.03370        0.08620        0.39750
C          1.19560       -0.15420       -0.49160
C          2.53380        0.10320        0.13650
Br         2.77740        1.88580        0.73910
H         -2.49640        0.99070        0.90940
H         -3.33270       -0.01180       -0.29440
H         -2.67670       -0.82810        1.13510
H         -1.28800        0.37660       -1.31740
H         -1.23900       -1.29590       -0.71640
H          0.01750       -0.58130        1.27280
H         -0.07370        1.12790        0.74960
H          1.12580        0.50020       -1.39000
H          1.13460       -1.19350       -0.87270
H          2.63980       -0.61020        1.00070
H          3.35350       -0.16600       -0.56010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

