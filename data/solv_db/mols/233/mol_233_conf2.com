%nproc=16
%mem=2GB
%chk=mol_233_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.42510       -0.43360        0.04730
C         -1.28120        0.57410       -0.04010
C         -0.01140       -0.23450        0.11230
C          1.22020        0.63870        0.04550
C          2.48360       -0.17710        0.19830
Br         2.63640       -1.50300       -1.19470
H         -2.09020       -1.33550       -0.46640
H         -3.30290        0.04830       -0.42750
H         -2.58540       -0.60520        1.14230
H         -1.35520        1.05970       -1.03320
H         -1.41950        1.29410        0.77500
H         -0.09350       -0.77860        1.06770
H         -0.00080       -0.99270       -0.70310
H          1.18570        1.32220        0.93350
H          1.21620        1.25360       -0.86790
H          3.36210        0.49720        0.17020
H          2.46100       -0.62770        1.20820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

