%nproc=16
%mem=2GB
%chk=mol_233_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.82870        0.87320        0.46200
C          1.46620       -0.46510       -0.16180
C          0.03050       -0.46100       -0.66600
C         -0.85530       -0.19490        0.50370
C         -2.32200       -0.16260        0.14620
Br        -2.69840        1.21610       -1.15070
H          1.56110        1.70520       -0.21330
H          1.39720        0.96320        1.46610
H          2.93420        0.89410        0.58050
H          2.17340       -0.75410       -0.94450
H          1.50570       -1.20450        0.68070
H         -0.17040       -1.50350       -1.01590
H         -0.05880        0.24770       -1.48740
H         -0.60100        0.79410        0.93740
H         -0.63510       -0.94130        1.29250
H         -2.87740        0.10910        1.07450
H         -2.67870       -1.11550       -0.27530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

