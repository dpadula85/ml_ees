%nproc=16
%mem=2GB
%chk=mol_233_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43360       -0.39860       -0.12350
C          1.28600        0.52000        0.13430
C         -0.01870       -0.20790       -0.09160
C         -1.19750        0.69340        0.16090
C         -2.45330       -0.12650       -0.09150
Br        -2.52840       -1.65320        1.08930
H          3.30500       -0.12170        0.52360
H          2.74090       -0.43680       -1.19680
H          2.11100       -1.44840        0.11420
H          1.31960        1.36950       -0.56280
H          1.30220        0.82330        1.20130
H         -0.09930       -0.59140       -1.11350
H         -0.11050       -1.05000        0.65650
H         -1.14860        1.02540        1.23140
H         -1.18890        1.61530       -0.43990
H         -3.36100        0.49260        0.09290
H         -2.39220       -0.50500       -1.12740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

