%nproc=16
%mem=2GB
%chk=mol_233_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95200       -0.22990       -0.62180
C          1.37500        0.11810        0.72980
C         -0.04080        0.67480        0.52670
C         -0.83110       -0.40810       -0.14220
C         -2.25960       -0.00730       -0.41140
Br        -3.07500        0.40350        1.30240
H          3.04420        0.05350       -0.66690
H          1.89940       -1.31500       -0.84160
H          1.41450        0.28640       -1.44920
H          2.01430        0.83300        1.27550
H          1.28180       -0.84020        1.29930
H         -0.46740        0.99720        1.49150
H          0.00510        1.54800       -0.15890
H         -0.88100       -1.27070        0.57890
H         -0.29150       -0.75130       -1.03200
H         -2.35950        0.83600       -1.09940
H         -2.78050       -0.92800       -0.78060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_233_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

