%nproc=16
%mem=2GB
%chk=mol_396_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.56110        0.58840        0.17590
C         -0.25030        0.96810        0.14910
C          0.75710        0.03950       -0.04020
C          0.36970       -1.28210       -0.19970
N         -0.92700       -1.62080       -0.16810
C         -1.92730       -0.74150        0.01400
C          2.15950        0.43000       -0.07120
O          2.49150        1.62740        0.07420
H         -2.32480        1.34380        0.32660
H          0.07140        2.01120        0.27370
H          1.16980       -2.00570       -0.34810
H         -2.96190       -1.05810        0.03280
H          2.93350       -0.30010       -0.21900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_396_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_396_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_396_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

