%nproc=16
%mem=2GB
%chk=mol_396_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.55750        0.62520        0.18040
C         -0.22840        0.98250        0.31470
C          0.74130        0.04530        0.02500
C          0.34120       -1.21570       -0.38880
N         -0.96870       -1.51480       -0.50400
C         -1.94540       -0.63240       -0.23210
C          2.16220        0.34220        0.14130
O          3.05450       -0.49810       -0.11820
H         -2.28300        1.39850        0.42050
H          0.09720        1.96330        0.63670
H          1.12210       -1.94140       -0.61250
H         -3.00340       -0.87920       -0.32680
H          2.46790        1.32460        0.46390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_396_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_396_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_396_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

