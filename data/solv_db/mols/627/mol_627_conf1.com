%nproc=16
%mem=2GB
%chk=mol_627_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.49770       -0.41900        0.12470
C          2.78240       -0.63060       -1.03500
C          1.41490       -0.45460       -1.09880
C          0.69100       -0.05710        0.00600
C          1.42590        0.15610        1.17690
C          2.78970       -0.01880        1.23830
C         -0.76080        0.09590       -0.00170
C         -1.35760        1.26750        0.48450
C         -2.71970        1.45590        0.43180
C         -3.50130        0.46900       -0.11060
C         -2.90960       -0.68030       -0.58520
C         -1.54470       -0.89110       -0.54090
Cl        -0.82540       -2.38860       -1.08120
H          4.57730       -0.55750        0.17730
H          3.36710       -0.94700       -1.90270
H          0.90860       -0.60040       -2.05080
H          0.84330        0.47180        2.04230
H          3.30920        0.15910        2.16680
H         -0.72850        2.03900        0.91050
H         -3.17430        2.36360        0.80920
H         -4.56890        0.63660       -0.14410
H         -3.51610       -1.46940       -1.01710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

