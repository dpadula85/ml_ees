%nproc=16
%mem=2GB
%chk=mol_627_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.52000        0.08050       -0.02460
C          2.81100        1.01630        0.67430
C          1.42560        0.97440        0.72060
C          0.71260       -0.01010        0.06500
C          1.44310       -0.94280       -0.63480
C          2.82630       -0.90880       -0.68550
C         -0.74550       -0.00260        0.10420
C         -1.47020        1.15210       -0.16260
C         -2.84650        1.19910       -0.17280
C         -3.52140        0.02740        0.09880
C         -2.86390       -1.15400        0.37290
C         -1.47760       -1.13330        0.36830
Cl        -0.67140       -2.64600        0.77440
H          4.61300        0.08070       -0.08370
H          3.36840        1.79650        1.19430
H          0.90940        1.73730        1.28700
H          0.90810       -1.70740       -1.17610
H          3.38080       -1.65100       -1.23940
H         -0.89750        2.04410       -0.36940
H         -3.40470        2.10270       -0.38160
H         -4.61910        0.01930        0.10050
H         -3.40040       -2.07450        0.58630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

