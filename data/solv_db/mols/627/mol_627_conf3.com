%nproc=16
%mem=2GB
%chk=mol_627_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.50890       -0.13290        0.05350
C          2.76510       -0.13290        1.21490
C          1.38860       -0.05490        1.18200
C          0.68430        0.02400       -0.00100
C          1.44130        0.02490       -1.16690
C          2.81920       -0.05190       -1.13650
C         -0.77620        0.05200       -0.07060
C         -1.42520        1.04680       -0.81660
C         -2.79280        1.06800       -0.78700
C         -3.52990        0.14620       -0.05070
C         -2.87260       -0.82030        0.67090
C         -1.48310       -0.87010        0.66020
Cl        -0.62060       -2.14140        1.49390
H          4.58700       -0.19450        0.09530
H          3.25280       -0.19570        2.20190
H          0.86320       -0.01520        2.13050
H          0.92020        0.08770       -2.10670
H          3.42200       -0.05190       -2.04150
H         -0.81630        1.74730       -1.37790
H         -3.33170        1.83560       -1.36140
H         -4.61120        0.18970       -0.04590
H         -3.39280       -1.56050        1.25950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_627_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

