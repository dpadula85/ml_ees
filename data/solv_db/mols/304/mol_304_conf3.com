%nproc=16
%mem=2GB
%chk=mol_304_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87640        0.23910       -0.03810
C         -1.31360       -1.01850       -0.04290
C          0.05450       -1.18340       -0.01640
C          0.90110       -0.09290        0.01580
C          0.33080        1.16060        0.02040
C         -1.04470        1.35160       -0.00590
N          2.30250       -0.30600        0.04230
H         -2.95310        0.41240       -0.05840
H         -2.00210       -1.87710       -0.06870
H          0.43950       -2.21540       -0.02200
H          0.96610        2.02450        0.04510
H         -1.48450        2.33730       -0.00220
H          2.66930       -1.20290        0.43540
H          3.01060        0.37080       -0.30440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

