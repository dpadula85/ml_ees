%nproc=16
%mem=2GB
%chk=mol_304_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.83950        0.56000        0.02250
C          0.80820        1.47260       -0.01690
C         -0.52930        1.07510       -0.03340
C         -0.85740       -0.25460       -0.01040
C          0.15780       -1.17970        0.02890
C          1.49860       -0.77650        0.04530
N         -2.21630       -0.66680       -0.02710
H          2.88670        0.85200        0.03580
H          1.06980        2.52370       -0.03500
H         -1.32260        1.81980       -0.06460
H         -0.11340       -2.23660        0.04700
H          2.26110       -1.54310        0.07650
H         -2.98440        0.00390       -0.24630
H         -2.49850       -1.64980        0.17750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

