%nproc=16
%mem=2GB
%chk=mol_304_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90430       -0.18030       -0.05450
C         -1.29890        1.04800        0.14480
C          0.07340        1.19110        0.19000
C          0.89030        0.08000        0.03260
C          0.26890       -1.13550       -0.16510
C         -1.10230       -1.28910       -0.21190
N          2.30640        0.21990        0.07810
H         -2.96850       -0.26510       -0.08530
H         -1.92230        1.93390        0.27060
H          0.55930        2.16060        0.34750
H          0.94570       -1.99200       -0.28550
H         -1.52640       -2.27960       -0.37170
H          2.83760        0.62380        0.86150
H          2.84110       -0.11550       -0.75120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

