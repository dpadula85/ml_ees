%nproc=16
%mem=2GB
%chk=mol_304_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91270       -0.17690       -0.04130
C         -1.09040       -1.28030       -0.16560
C          0.28070       -1.11660       -0.13240
C          0.88550        0.11900        0.02180
C          0.07380        1.20780        0.14440
C         -1.30090        1.05590        0.11260
N          2.31300        0.19370        0.04480
H         -3.00110       -0.27950       -0.06430
H         -1.51400       -2.25880       -0.28740
H          0.94170       -1.98750       -0.23000
H          0.54810        2.17740        0.26530
H         -1.95080        1.93430        0.21160
H          2.84100        1.03050       -0.25600
H          2.88600       -0.61900        0.37640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_304_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

