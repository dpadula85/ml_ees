%nproc=16
%mem=2GB
%chk=mol_502_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.69980        0.68590       -0.00200
C         -1.09540       -0.63600       -0.00250
N          0.02500       -1.40740        0.00050
C          1.10850       -0.58710        0.00290
N          0.64690        0.66750        0.00140
H         -1.27360        1.59360       -0.00370
H         -2.13170       -0.94590       -0.00490
H          2.15440       -0.89150        0.00560
H          1.26570        1.52080        0.00260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_502_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_502_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_502_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

