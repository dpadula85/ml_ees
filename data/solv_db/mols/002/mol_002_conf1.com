%nproc=16
%mem=2GB
%chk=mol_002_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21240       -0.76710        1.16730
C         -2.57950       -0.91400        1.39080
C         -3.49100       -0.27830        0.56260
C         -3.00110        0.48990       -0.47180
C         -1.62770        0.62940       -0.68680
C         -0.72370        0.00140        0.13370
C          0.72890        0.08880       -0.07310
C          1.48640       -1.08580       -0.08700
C          2.84560       -1.08780       -0.31090
C          3.50060        0.08370       -0.52950
C          2.75810        1.24180       -0.51540
C          1.39300        1.27040       -0.29200
Cl         0.49570        2.76820       -0.23530
Cl         3.58180        2.76820       -0.79550
Cl         5.23250        0.12230       -0.82020
Cl         3.75770       -2.59630       -0.31860
Cl        -4.13990        1.29860       -1.52630
Cl        -5.22110       -0.42220        0.79110
H         -0.53760       -1.28670        1.84570
H         -2.94700       -1.52100        2.20660
H         -1.27450        1.22110       -1.52160
H          0.97520       -2.02470        0.08590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_002_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_002_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_002_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

