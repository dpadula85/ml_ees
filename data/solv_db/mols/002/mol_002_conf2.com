%nproc=16
%mem=2GB
%chk=mol_002_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.39590        1.13860        0.36870
C          2.78060        1.20380        0.36430
C          3.52740        0.11070       -0.01410
C          2.87180       -1.03710       -0.38430
C          1.49830       -1.12050       -0.38730
C          0.75220       -0.02050       -0.00780
C         -0.70430       -0.02170       -0.02750
C         -1.39950       -1.04910        0.60940
C         -2.77620       -1.08660        0.67700
C         -3.52930       -0.08400        0.10240
C         -2.86300        0.94080       -0.53370
C         -1.47250        0.96970       -0.59840
Cl        -0.66000        2.25660       -1.48300
Cl        -3.81940        2.21190       -1.26230
Cl        -5.28670       -0.11330        0.17940
Cl        -3.62550       -2.40090        1.49100
Cl         3.80440       -2.43220       -0.86530
Cl         5.26190        0.26370        0.00420
H          0.86120        2.00160        0.70160
H          3.24710        2.13080        0.66440
H          0.96100       -2.00500       -0.67210
H         -0.82540       -1.85730        1.07350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_002_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_002_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_002_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

