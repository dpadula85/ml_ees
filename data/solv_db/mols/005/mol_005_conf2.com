%nproc=16
%mem=2GB
%chk=mol_005_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.18790        0.27490        0.37480
C         -1.08120       -0.71320        0.11850
C          0.24420       -0.05520       -0.03870
O          0.30320        0.92280       -0.81660
O          1.34440       -0.49180        0.68220
C          2.56170        0.17020        0.55290
H         -3.07260       -0.33670        0.69960
H         -2.49480        0.88280       -0.46460
H         -1.93370        0.93720        1.22930
H         -1.02430       -1.40470        0.98920
H         -1.32270       -1.28060       -0.82270
H          3.25710       -0.03470        1.39910
H          2.34800        1.27480        0.55210
H          3.05850       -0.14590       -0.39080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

