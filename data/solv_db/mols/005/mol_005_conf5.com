%nproc=16
%mem=2GB
%chk=mol_005_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17960        0.30510       -0.24890
C         -1.13140       -0.60410        0.36490
C          0.22270       -0.32280       -0.16600
O          0.49190       -0.33810       -1.38550
O          1.28590       -0.00090        0.70360
C          2.55930        0.28880        0.14380
H         -2.91560       -0.29010       -0.80560
H         -1.71630        0.99750       -1.00620
H         -2.69860        0.90300        0.52890
H         -1.14100       -0.40770        1.46320
H         -1.44320       -1.66820        0.25060
H          2.51870        0.32930       -0.95750
H          3.27630       -0.45660        0.53410
H          2.87090        1.26480        0.58050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

