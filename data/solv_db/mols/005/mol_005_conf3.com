%nproc=16
%mem=2GB
%chk=mol_005_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91270        0.38470        0.51700
C         -1.17100       -0.85670        0.12400
C          0.27780       -0.62210       -0.07680
O          0.97550       -1.61630       -0.40970
O          0.86650        0.62530        0.08750
C          2.25960        0.73410       -0.13060
H         -2.04200        1.00690       -0.40210
H         -1.36860        0.98160        1.26320
H         -2.90480        0.07170        0.92100
H         -1.27250       -1.60480        0.94190
H         -1.64950       -1.30330       -0.77540
H          2.78320        0.70420        0.86330
H          2.52240        1.63780       -0.71160
H          2.63610       -0.14320       -0.69620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

