%nproc=16
%mem=2GB
%chk=mol_005_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32590        0.04840       -0.43820
C         -1.03090       -0.60150       -0.05570
C          0.15270        0.26490       -0.26210
O          0.25360        1.12020       -1.16280
O          1.32930       -0.18280        0.34710
C          2.60840        0.23470       -0.05000
H         -2.21820        1.06140       -0.85800
H         -2.85300       -0.63630       -1.15080
H         -2.99700        0.10130        0.45670
H         -0.82330       -1.54240       -0.61800
H         -1.04110       -0.85620        1.02970
H          3.33130       -0.27480        0.62640
H          2.87110       -0.06560       -1.07920
H          2.74310        1.32860        0.13200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

