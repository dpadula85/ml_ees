%nproc=16
%mem=2GB
%chk=mol_005_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17580        0.27390        0.19960
C         -1.07650       -0.69680        0.61010
C          0.22010       -0.31260        0.01290
O          0.16030        0.33630       -1.07500
O          1.43210       -0.36980        0.68140
C          2.55380        0.24060        0.07100
H         -2.81710        0.55590        1.04780
H         -1.65180        1.20440       -0.12590
H         -2.75410       -0.08040       -0.66780
H         -1.05670       -0.80780        1.69900
H         -1.33730       -1.68260        0.14790
H          3.10310       -0.45190       -0.60100
H          2.18250        1.05160       -0.60650
H          3.21740        0.73920        0.80000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_005_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

