%nproc=16
%mem=2GB
%chk=mol_075_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.04450       -2.24100        0.14240
O         -1.77800       -1.64980        0.25380
P         -1.85400       -0.11350       -0.49500
S         -1.69560       -0.25470       -2.20940
O         -3.40580        0.48380       -0.18620
C         -3.41010        1.85680       -0.44860
O         -0.66460        0.91490        0.16920
C          0.67890        0.57410        0.15490
C          1.60370        1.44590        0.70840
C          2.95310        1.17310        0.72930
C          3.42860       -0.00690        0.18460
C          2.48600       -0.87100       -0.36720
C          1.13660       -0.60230       -0.39020
N          4.81300       -0.32410        0.18710
O          5.60200        0.48490        0.69250
O          5.29550       -1.48110       -0.34500
H         -3.09400       -3.05090        0.88990
H         -3.13970       -2.64050       -0.89890
H         -3.85910       -1.50040        0.27820
H         -2.60140        2.06740       -1.20630
H         -4.37560        2.15580       -0.85260
H         -3.23100        2.40840        0.48600
H          1.22670        2.37850        1.13890
H          3.66690        1.87990        1.17380
H          2.85590       -1.80720       -0.80020
H          0.40640       -1.28030       -0.82290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

