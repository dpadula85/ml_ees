%nproc=16
%mem=2GB
%chk=mol_075_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.01650        1.04130       -0.71630
O         -2.71290        1.27100       -1.07830
P         -1.72560       -0.09060       -0.91480
S         -0.78910       -0.20090       -2.38350
O         -2.73950       -1.43880       -0.79630
C         -2.82510       -1.78320        0.54980
O         -0.67040        0.03760        0.42360
C          0.69770        0.15420        0.29650
C          1.50350       -0.98370        0.26330
C          2.85830       -0.87420        0.13760
C          3.50220        0.34650        0.03750
C          2.71720        1.46640        0.06940
C          1.35220        1.35940        0.19590
N          4.90670        0.36700       -0.08980
O          5.57540       -0.68180       -0.11360
O          5.57150        1.56230       -0.19100
H         -4.41170        0.06050       -0.98210
H         -4.14420        1.25290        0.36320
H         -4.64350        1.80490       -1.25540
H         -1.87790       -2.33270        0.79810
H         -3.70760       -2.44220        0.72710
H         -2.91060       -0.86660        1.13990
H          1.05820       -1.95190        0.33680
H          3.46010       -1.76850        0.11430
H          3.20480        2.42820       -0.00770
H          0.76680        2.26300        0.21740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

