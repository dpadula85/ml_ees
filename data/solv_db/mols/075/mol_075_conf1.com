%nproc=16
%mem=2GB
%chk=mol_075_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73910       -2.33090        0.31340
O         -3.24200       -1.03910        0.18640
P         -2.14500        0.13750        0.67890
S         -2.82330        1.09210        1.92530
O         -1.71920        1.05980       -0.67560
C         -2.53970        2.14910       -0.83710
O         -0.71830       -0.63880        1.26830
C          0.51590       -0.39680        0.73050
C          0.99380       -1.15110       -0.31520
C          2.24900       -0.89030       -0.84760
C          3.06370        0.12950       -0.35300
C          2.55740        0.86790        0.69480
C          1.30440        0.61190        1.23060
N          4.34680        0.38290       -0.91030
O          4.82440       -0.26290       -1.84870
O          5.07990        1.40500       -0.36230
H         -3.50830       -2.90490        0.88720
H         -2.58700       -2.77110       -0.71300
H         -1.74760       -2.34900        0.81190
H         -3.10210        2.34900        0.09230
H         -1.92510        3.04410       -1.06840
H         -3.26500        2.04430       -1.66090
H          0.38190       -1.94880       -0.71800
H          2.64010       -1.46440       -1.66060
H          3.17880        1.67440        1.10040
H          0.92550        1.20060        2.05040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

