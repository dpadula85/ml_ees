%nproc=16
%mem=2GB
%chk=mol_075_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.86630        2.39080        0.45220
O         -3.31150        1.35460       -0.36510
P         -2.06490        0.29340       -0.77150
S         -1.12680        0.92030       -2.09520
O         -2.78340       -1.18360       -1.19340
C         -2.78090       -2.08860       -0.15390
O         -1.00540        0.03470        0.55000
C          0.36480       -0.08720        0.41290
C          1.16820        1.02570        0.47960
C          2.53500        0.95470        0.34910
C          3.16020       -0.26830        0.14150
C          2.35360       -1.37390        0.07600
C          0.98160       -1.31630        0.20520
N          4.56240       -0.30440        0.01240
O          5.11720       -1.40110       -0.17300
O          5.33090        0.82270        0.08400
H         -1.97350        2.90520        0.05450
H         -3.70330        3.12650        0.52110
H         -2.64090        1.99350        1.46750
H         -2.54730       -1.64870        0.82620
H         -2.00790       -2.87270       -0.39740
H         -3.77920       -2.58280       -0.04600
H          0.70800        1.99030        0.64050
H          3.12970        1.84970        0.40730
H          2.80670       -2.34290       -0.08480
H          0.37260       -2.19160        0.15070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

