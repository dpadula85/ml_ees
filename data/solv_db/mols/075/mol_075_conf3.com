%nproc=16
%mem=2GB
%chk=mol_075_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.69420       -2.73060        0.49840
O         -1.84330       -2.05460       -0.34610
P         -1.87810       -0.39230       -0.15520
S         -1.47410        0.08690        1.46080
O         -3.36600        0.28550       -0.56580
C         -3.32510        1.62930       -0.15330
O         -0.67530        0.27640       -1.18630
C          0.62780        0.32540       -0.71450
C          1.49380       -0.72060       -0.93040
C          2.76980       -0.61230       -0.43870
C          3.20980        0.49420        0.25570
C          2.31580        1.51800        0.45100
C          1.00860        1.45690       -0.02810
N          4.54130        0.56190        0.74660
O          4.95610        1.55280        1.37560
O          5.41620       -0.47240        0.53960
H         -2.10980       -3.52490        1.01230
H         -3.16960       -2.09720        1.26370
H         -3.45000       -3.24700       -0.12760
H         -3.76570        2.30170       -0.91530
H         -2.28280        1.96150        0.04480
H         -3.88290        1.72110        0.80500
H          1.14600       -1.59040       -1.47690
H          3.47280       -1.41860       -0.59300
H          2.61220        2.39890        0.98540
H          0.34670        2.29030        0.15350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_075_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

