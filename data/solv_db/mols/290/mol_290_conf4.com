%nproc=16
%mem=2GB
%chk=mol_290_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.49450        0.43400        0.19080
C          2.03610        0.38330        0.62390
C          1.25370       -0.14220       -0.55600
C         -0.18650       -0.25300       -0.30030
C         -0.79540       -1.38580        0.21230
C         -2.16170       -1.38830        0.42080
C         -2.94110       -0.28990        0.13180
C         -2.34560        0.84190       -0.37840
C         -0.98940        0.83280       -0.58110
O         -4.31940       -0.36220        0.36800
H          4.18520        0.45330        1.04410
H          3.61570        1.38280       -0.37370
H          3.67070       -0.39120       -0.52150
H          1.66430        1.39190        0.91940
H          1.90520       -0.28910        1.49340
H          1.40700        0.56640       -1.39510
H          1.71620       -1.10160       -0.87510
H         -0.16470       -2.22530        0.42790
H         -2.60420       -2.30480        0.82790
H         -2.96270        1.69780       -0.60210
H         -0.54030        1.75080       -0.98970
H         -4.93780        0.39810        0.17990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

