%nproc=16
%mem=2GB
%chk=mol_290_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.52790        0.05250        0.38280
C          2.09970       -0.45470        0.42640
C          1.21740        0.69240       -0.03240
C         -0.22350        0.30720       -0.03040
C         -0.80980       -0.24770       -1.14830
C         -2.14670       -0.60530       -1.14760
C         -2.95180       -0.42740       -0.04240
C         -2.36880        0.12830        1.08030
C         -1.03080        0.48410        1.07370
O         -4.29590       -0.78190       -0.02740
H          3.60790        0.84610       -0.38990
H          4.26260       -0.75050        0.21930
H          3.73270        0.56910        1.35640
H          1.79540       -0.71760        1.44210
H          1.96570       -1.29360       -0.30380
H          1.57200        1.00240       -1.03140
H          1.37090        1.54900        0.64550
H         -0.17080       -0.38800       -2.02240
H         -2.58300       -1.03860       -2.03670
H         -2.99330        0.27530        1.96340
H         -0.56250        0.92060        1.94770
H         -5.01540       -0.12170       -0.30440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

