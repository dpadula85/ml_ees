%nproc=16
%mem=2GB
%chk=mol_290_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.46930       -0.04630       -0.47870
C          2.02450        0.40960       -0.50770
C          1.23630       -0.60860        0.28320
C         -0.20260       -0.30040        0.34720
C         -0.76400        0.47130        1.34440
C         -2.11830        0.75790        1.39920
C         -2.92580        0.24260        0.40810
C         -2.40220       -0.53300       -0.60530
C         -1.04280       -0.79880       -0.62790
O         -4.29320        0.49870        0.41410
H          3.87130        0.13670       -1.50150
H          4.04710        0.49720        0.29750
H          3.52280       -1.13030       -0.31280
H          1.63400        0.45340       -1.53410
H          1.88750        1.38890       -0.01150
H          1.35700       -1.58380       -0.26940
H          1.69400       -0.78720        1.27940
H         -0.10850        0.85430        2.09630
H         -2.53340        1.37650        2.20810
H         -3.00970       -0.94810       -1.39220
H         -0.61350       -1.41140       -1.42400
H         -4.72990        1.06090        1.13290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

