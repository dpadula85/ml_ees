%nproc=16
%mem=2GB
%chk=mol_290_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.15680        1.34740        0.11840
C          2.41370       -0.12050       -0.14020
C          1.33580       -0.66900       -1.03500
C         -0.01770       -0.54740       -0.45390
C         -0.74340        0.59940       -0.71550
C         -2.01960        0.79790       -0.21210
C         -2.61190       -0.16450        0.58020
C         -1.88970       -1.31070        0.84310
C         -0.60760       -1.50070        0.33230
O         -3.89810        0.04020        1.08500
H          3.05000        1.73630        0.65050
H          2.04260        1.83850       -0.87230
H          1.28640        1.53400        0.75950
H          3.36650       -0.17860       -0.70750
H          2.53440       -0.70060        0.79190
H          1.40820       -0.10660       -2.00400
H          1.51050       -1.73720       -1.27130
H         -0.34200        1.40430       -1.33330
H         -2.58780        1.71360       -0.42620
H         -2.34670       -2.07820        1.46810
H         -0.05100       -2.40030        0.54320
H         -3.98940        0.50280        1.99910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

