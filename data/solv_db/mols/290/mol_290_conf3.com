%nproc=16
%mem=2GB
%chk=mol_290_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.54670        0.06180       -0.10620
C         -2.07350        0.06580       -0.45360
C         -1.22280       -0.07620        0.79560
C          0.22360       -0.07290        0.46420
C          0.92810       -1.22460        0.18260
C          2.27520       -1.19220       -0.12270
C          2.96090        0.01960       -0.15330
C          2.27140        1.16710        0.12440
C          0.92650        1.11360        0.42700
O          4.29900        0.11050       -0.45040
H         -3.69170        0.32740        0.97980
H         -4.01670       -0.94290       -0.30220
H         -4.08280        0.85760       -0.67070
H         -1.77000        0.92000       -1.07070
H         -1.89040       -0.85730       -1.06950
H         -1.46990        0.77960        1.45000
H         -1.49000       -1.00270        1.34300
H          0.41630       -2.18520        0.19960
H          2.84610       -2.08150       -0.34620
H          2.74710        2.14570        0.11750
H          0.37720        2.01480        0.64680
H          4.98300        0.05190        0.31920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_290_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

