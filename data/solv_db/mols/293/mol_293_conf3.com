%nproc=16
%mem=2GB
%chk=mol_293_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.22020        0.24470       -0.05240
C         -2.15430        0.00930       -1.08590
O         -1.08460       -0.73270       -0.60280
C         -0.45120       -0.11930        0.46450
C          0.69310       -1.03520        0.89080
O          1.57200       -1.21800       -0.13100
C          2.18320       -0.10730       -0.62740
C          3.00720        0.66290        0.36870
H         -3.05680        1.26280        0.36080
H         -4.20550        0.22460       -0.55730
H         -3.21490       -0.49450        0.76450
H         -2.60870       -0.61620       -1.90920
H         -1.85480        0.96620       -1.54350
H         -0.05190        0.86560        0.18410
H         -1.16780        0.04260        1.28640
H          1.17630       -0.64160        1.83060
H          0.28190       -2.02700        1.17630
H          1.47580        0.56200       -1.13230
H          2.90130       -0.44850       -1.42210
H          3.71440        1.28850       -0.22060
H          3.64270       -0.04080        0.96480
H          2.42300        1.35180        0.99300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

