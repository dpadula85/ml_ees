%nproc=16
%mem=2GB
%chk=mol_293_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.43230        0.99870        0.60570
C         -2.53670       -0.21780       -0.26580
O         -1.34520       -0.62590       -0.80420
C         -0.41230       -0.94620        0.17550
C          0.89570       -1.39440       -0.39120
O          1.51760       -0.45660       -1.18670
C          1.80720        0.72200       -0.53150
C          2.72020        0.55410        0.64920
H         -1.95880        0.83060        1.57250
H         -1.92380        1.84270        0.11260
H         -3.48620        1.34340        0.80960
H         -3.25080        0.01060       -1.07730
H         -3.03120       -1.01590        0.35830
H         -0.84360       -1.83380        0.72230
H         -0.27220       -0.09150        0.86130
H          1.58470       -1.59740        0.46580
H          0.79940       -2.35020       -0.93430
H          0.86090        1.22730       -0.20380
H          2.33010        1.37760       -1.28860
H          2.20950        0.70380        1.62670
H          3.53540        1.33390        0.62790
H          3.23200       -0.41490        0.69150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

