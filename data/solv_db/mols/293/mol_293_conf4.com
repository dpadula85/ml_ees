%nproc=16
%mem=2GB
%chk=mol_293_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.12090       -0.40420       -0.07380
C         -2.40850        0.50270       -1.08370
O         -1.49590        1.32940       -0.51050
C         -0.46460        0.76080        0.18710
C          0.35020       -0.12380       -0.71200
O          1.38970       -0.71440       -0.05620
C          2.26320        0.20310        0.46890
C          3.37810       -0.59900        1.13150
H         -4.14950       -0.52970       -0.47210
H         -3.07050       -0.01710        0.94310
H         -2.64230       -1.42290       -0.07230
H         -1.97970       -0.15910       -1.85010
H         -3.16000        1.14910       -1.60640
H         -0.78610        0.15710        1.05840
H          0.13200        1.64090        0.57600
H          0.71880        0.43850       -1.61830
H         -0.31860       -0.94550       -1.07650
H          1.75860        0.78700        1.29220
H          2.63560        0.88100       -0.29920
H          3.71200       -0.07170        2.02950
H          4.20910       -0.64880        0.38300
H          3.04930       -1.62620        1.36160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

