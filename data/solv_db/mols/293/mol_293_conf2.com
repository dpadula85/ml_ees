%nproc=16
%mem=2GB
%chk=mol_293_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.28310        1.16980       -0.09910
C         -2.31320       -0.21770       -0.68630
O         -1.72200       -1.11770        0.08940
C         -0.44260       -1.18330        0.42000
C          0.24560       -0.08940        1.14430
O          1.58680       -0.40760        1.39940
C          2.36220       -0.63310        0.30400
C          2.33850        0.60680       -0.57140
H         -3.31910        1.63920       -0.16910
H         -1.56300        1.83480       -0.64600
H         -2.07480        1.19130        0.98830
H         -3.40080       -0.44080       -0.90850
H         -1.87790       -0.12260       -1.72170
H          0.15300       -1.45860       -0.51220
H         -0.31190       -2.13170        1.04800
H          0.22260        0.86280        0.52570
H         -0.20170        0.15960        2.12640
H          2.09580       -1.55920       -0.22390
H          3.43070       -0.82210        0.65380
H          2.08050        1.50930       -0.01670
H          3.38040        0.74550       -0.97880
H          1.61390        0.46470       -1.39900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

