%nproc=16
%mem=2GB
%chk=mol_293_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.11140       -0.38220        0.01810
C         -2.50390        0.70910        0.88720
O         -1.12130        0.52460        0.87800
C         -0.72210        0.63320       -0.45370
C          0.79780        0.43710       -0.51880
O          1.05540       -0.81770       -0.01900
C          2.38660       -1.16580       -0.00200
C          3.13060       -0.17470        0.86170
H         -3.19170       -0.05290       -1.04330
H         -4.14650       -0.65180        0.37040
H         -2.50670       -1.31130        0.11100
H         -2.90910        0.67870        1.91760
H         -2.69310        1.67970        0.38770
H         -1.15490       -0.23800       -0.98930
H         -1.00790        1.59860       -0.90120
H          1.06310        0.54000       -1.59560
H          1.28570        1.25080        0.03340
H          2.83840       -1.22100       -1.02760
H          2.45150       -2.18730        0.44310
H          3.78260        0.45800        0.20970
H          3.82160       -0.74560        1.54720
H          2.45540        0.43860        1.49360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_293_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

