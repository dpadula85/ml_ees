%nproc=16
%mem=2GB
%chk=mol_583_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.19810        3.99900       -0.69340
C         -3.14970        2.87810        0.29820
O         -1.85760        2.26070        0.21900
C         -1.57480        1.18420        1.04460
O         -2.51430        0.82540        1.82680
C         -0.32290        0.43040        1.08350
C         -0.14190       -0.54870       -0.01210
C         -1.18230       -1.61000       -0.09260
O         -2.13800       -1.73490        0.70730
O         -1.12330       -2.56200       -1.12430
C         -2.09680       -3.55260       -1.20070
C         -2.19430       -4.44250        0.01150
S          1.44220       -1.41170        0.09500
P          3.10390       -0.19530       -0.19890
S          4.51530       -1.23040       -0.14530
O          3.32760        0.94160        1.02850
C          4.04510        0.39440        2.09870
O          2.99110        0.59600       -1.66420
C          4.19500        1.17220       -2.05850
H         -4.21520        4.45050       -0.74650
H         -2.86200        3.59820       -1.67900
H         -2.50350        4.78590       -0.40310
H         -3.95730        2.13130        0.11090
H         -3.34330        3.27830        1.32030
H         -0.15370       -0.07700        2.06600
H          0.50390        1.18280        0.98300
H         -0.15790       -0.01710       -1.00000
H         -3.11590       -3.17410       -1.42690
H         -1.82650       -4.22490       -2.06640
H         -2.60540       -5.43310       -0.32500
H         -1.25180       -4.64040        0.52340
H         -2.96680       -4.06660        0.73750
H          3.49960       -0.48670        2.46190
H          5.08050        0.12700        1.77500
H          4.14720        1.10990        2.93220
H          5.06100        0.50530       -1.94260
H          4.17240        1.49020       -3.11320
H          4.36850        2.06660       -1.43040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_583_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_583_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_583_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

