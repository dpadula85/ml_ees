%nproc=16
%mem=2GB
%chk=mol_468_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.39830       -0.12810       -0.21880
C         -2.12660       -0.47470        0.53430
C         -0.98240        0.05180       -0.27860
C          0.35770       -0.22650        0.36030
C          1.46640        0.32510       -0.50170
C          2.77080        0.03200        0.16010
O          2.82040       -0.58870        1.26960
N          4.00730        0.42160       -0.40010
H         -3.19310       -0.31420       -1.28770
H         -3.54280        0.96410       -0.09750
H         -4.24740       -0.72460        0.16190
H         -2.20680        0.02300        1.52950
H         -2.11190       -1.58150        0.63430
H         -1.15420        1.13500       -0.43030
H         -1.02520       -0.44180       -1.27200
H          0.37960        0.36840        1.31290
H          0.47370       -1.27380        0.62530
H          1.33740        1.42370       -0.54560
H          1.44120       -0.14540       -1.50310
H          4.48320        1.26320       -0.02890
H          4.45110       -0.10860       -1.16970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

