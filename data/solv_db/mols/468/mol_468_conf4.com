%nproc=16
%mem=2GB
%chk=mol_468_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.12700        0.73010        0.08000
C         -2.02200       -0.18200       -0.34040
C         -0.74550        0.04880        0.45500
C          0.28640       -0.91210       -0.04640
C          1.61580       -0.85910        0.60350
C          2.28900        0.43960        0.43720
O          1.90370        1.36470       -0.29620
N          3.52160        0.70450        1.15620
H         -2.76790        1.66950        0.55050
H         -3.74240        0.94500       -0.84120
H         -3.79810        0.19450        0.78210
H         -1.74880       -0.07940       -1.42480
H         -2.35790       -1.23070       -0.20660
H         -0.38980        1.07990        0.29970
H         -0.93380       -0.05900        1.54100
H          0.45620       -0.68890       -1.14290
H         -0.17170       -1.92280       -0.03480
H          2.24960       -1.64660        0.10120
H          1.59460       -1.13860        1.68750
H          4.25170       -0.04940        1.09780
H          3.63660        1.59200        1.68200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

