%nproc=16
%mem=2GB
%chk=mol_468_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.11350       -0.25930        0.61820
C          1.71550        0.34980        0.52120
C          1.01940       -0.40120       -0.58310
C         -0.39280        0.07350       -0.83670
C         -1.22260       -0.10440        0.40540
C         -2.63600        0.36070        0.15370
O         -2.93750        0.80820       -0.97480
N         -3.53460        0.25250        1.23970
H          3.16750       -1.25120        0.13660
H          3.32860       -0.42980        1.70500
H          3.85400        0.45580        0.24020
H          1.18370        0.23880        1.48470
H          1.76690        1.43630        0.31680
H          1.01670       -1.48160       -0.25270
H          1.62330       -0.39800       -1.52130
H         -0.40440        1.15420       -1.12840
H         -0.79510       -0.51500       -1.69300
H         -0.80110        0.40570        1.28100
H         -1.31880       -1.17720        0.67130
H         -3.90390       -0.64440        1.60100
H         -3.84210        1.12640        1.70230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

