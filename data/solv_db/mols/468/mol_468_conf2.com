%nproc=16
%mem=2GB
%chk=mol_468_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.83620        0.42210        0.17710
C         -2.24530       -0.75630       -0.53880
C         -0.85110       -1.09130       -0.09240
C          0.10920        0.05330       -0.31610
C          1.50620       -0.27720        0.12910
C          2.38430        0.89150       -0.12260
O          1.89290        1.91510       -0.62970
N          3.77530        0.88170        0.19740
H         -2.22050        1.31930        0.17010
H         -3.14450        0.14620        1.21390
H         -3.78850        0.67800       -0.35180
H         -2.30110       -0.57730       -1.64430
H         -2.89620       -1.62580       -0.34670
H         -0.50260       -1.94030       -0.72050
H         -0.88540       -1.34750        0.97420
H          0.16300        0.20470       -1.43030
H         -0.20750        0.99300        0.13280
H          1.55840       -0.47410        1.22700
H          1.90440       -1.19190       -0.36500
H          4.44890        1.28100       -0.49420
H          4.13630        0.49570        1.08400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

