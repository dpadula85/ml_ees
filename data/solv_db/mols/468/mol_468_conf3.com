%nproc=16
%mem=2GB
%chk=mol_468_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.35610        0.22570        0.41360
C         -2.12420       -0.61500        0.66100
C         -0.93750        0.08100        0.02650
C          0.34000       -0.69500        0.22730
C          1.44350        0.10580       -0.45180
C          2.73570       -0.56870       -0.31270
O          2.81350       -1.65720        0.30280
N          3.90840       -0.02270       -0.85760
H         -3.48730        0.99890        1.21350
H         -4.26420       -0.39680        0.30310
H         -3.23510        0.80450       -0.53760
H         -1.96250       -0.60670        1.77240
H         -2.24040       -1.64570        0.33300
H         -0.78780        1.07690        0.46750
H         -1.09350        0.19140       -1.06150
H          0.30330       -1.70830       -0.19690
H          0.62920       -0.71500        1.31890
H          1.12330        0.25560       -1.50000
H          1.43810        1.10530        0.03810
H          3.90070        0.85690       -1.43610
H          4.85280       -0.45210       -0.72350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_468_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

