%nproc=16
%mem=2GB
%chk=mol_267_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.68190       -1.15460        0.01700
C         -0.69990       -1.19830        0.03280
O         -1.26510       -2.29850        0.06200
N         -1.39330       -0.02050        0.01540
C         -0.75070        1.16050       -0.01630
O         -1.43100        2.22210       -0.03140
N          0.58460        1.19870       -0.03150
C          1.32840        0.07150       -0.01580
Cl         3.06500        0.07480       -0.03480
H          1.22990       -2.09520        0.03100
H         -2.44750       -0.07500        0.02790
H          1.09770        2.11440       -0.05630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_267_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_267_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_267_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

