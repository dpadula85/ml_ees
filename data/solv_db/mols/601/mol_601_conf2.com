%nproc=16
%mem=2GB
%chk=mol_601_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.74470        0.11420       -0.14360
O         -1.03570       -0.50830        0.83370
S          0.56950       -0.73260        0.55430
O          0.87380       -2.04450       -0.14840
O          1.25330       -0.84840        1.91360
C          1.35580        0.56750       -0.31510
H         -2.59510       -0.56720       -0.43460
H         -2.25980        1.00340        0.31540
H         -1.18790        0.43170       -1.02740
H          1.92300        1.24520        0.37690
H          2.14370        0.15100       -0.99940
H          0.70420        1.18800       -0.92540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

