%nproc=16
%mem=2GB
%chk=mol_601_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.98150        0.30610        0.22840
O         -0.69640        0.80740        0.08900
S          0.46780       -0.34030        0.50660
O          0.75540       -0.18600        1.97860
O         -0.08290       -1.68700        0.18350
C          1.91080        0.03260       -0.47360
H         -1.92900       -0.77740        0.46530
H         -2.45550        0.89240        1.05950
H         -2.52710        0.51390       -0.71220
H          1.95000        1.13960       -0.64130
H          1.74880       -0.44430       -1.45890
H          2.83950       -0.25700        0.02600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

