%nproc=16
%mem=2GB
%chk=mol_601_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.78600        0.47360       -0.10640
O         -0.73480        0.20160       -0.97940
S          0.39490       -0.81810       -0.22540
O          0.88880       -1.76080       -1.30480
O         -0.25570       -1.52590        0.89790
C          1.72540        0.24410        0.31240
H         -2.09500        1.52650       -0.31300
H         -1.48140        0.36570        0.95870
H         -2.61390       -0.20740       -0.38510
H          2.37560        0.38860       -0.59070
H          2.25410       -0.14170        1.17770
H          1.32790        1.25370        0.55810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

