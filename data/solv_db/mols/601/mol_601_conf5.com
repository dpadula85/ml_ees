%nproc=16
%mem=2GB
%chk=mol_601_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.84100        0.53210        0.17970
O          0.92120        0.05760       -0.76680
S         -0.28800       -0.74950        0.07830
O          0.08420       -0.92340        1.52470
O         -0.51800       -2.12140       -0.52110
C         -1.79130        0.21490       -0.07350
H          2.39350       -0.37900        0.53330
H          1.20630        0.91200        1.03410
H          2.49770        1.31140       -0.21110
H         -2.48770       -0.25670       -0.80960
H         -1.56780        1.25090       -0.36360
H         -2.29110        0.15120        0.92480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

