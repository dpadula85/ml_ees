%nproc=16
%mem=2GB
%chk=mol_601_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.54520       -0.67510        0.02070
O          1.25780        0.62240        0.32950
S         -0.29760        1.09400        0.04740
O         -0.74940        2.03940        1.14300
O         -0.32420        1.86750       -1.25740
C         -1.45210       -0.24230        0.00490
H          1.07410       -1.04250       -0.92040
H          2.65440       -0.73340       -0.17360
H          1.32930       -1.39880        0.83690
H         -2.32410        0.09490       -0.61330
H         -1.76810       -0.50630        1.03740
H         -0.94530       -1.11960       -0.45510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_601_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

