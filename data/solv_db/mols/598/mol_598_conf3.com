%nproc=16
%mem=2GB
%chk=mol_598_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.73490       -0.25390       -0.22870
C          1.25230       -0.26920       -0.63190
C          0.64860        1.07320       -0.58780
C         -0.39550        1.30280        0.44050
C         -1.32730        0.18660        0.68650
C         -0.87220       -1.14880        0.16190
C          0.60310       -1.30020        0.23730
C         -2.71680        0.40770        0.08830
H          2.86080        0.23570        0.74610
H          3.37040        0.15810       -1.01130
H          3.00080       -1.33190       -0.08990
H          1.24090       -0.67130       -1.67940
H          1.47430        1.82190       -0.40160
H          0.25840        1.35680       -1.60400
H         -0.96880        2.22790        0.13610
H          0.09750        1.60950        1.40850
H         -1.50170        0.05440        1.78350
H         -1.18770       -1.20970       -0.89580
H         -1.32890       -1.96280        0.78710
H          0.88280       -2.31540       -0.10990
H          0.98000       -1.15310        1.27060
H         -3.17010        1.34250        0.41130
H         -3.33340       -0.45010        0.46770
H         -2.60250        0.28930       -1.00220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

