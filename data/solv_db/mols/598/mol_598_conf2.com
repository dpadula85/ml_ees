%nproc=16
%mem=2GB
%chk=mol_598_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70420        0.25340       -0.62120
C         -1.39740        0.30070        0.15050
C         -0.90580       -1.07100        0.43510
C          0.55930       -1.26120        0.43420
C          1.41590       -0.06990        0.54090
C          0.83690        1.22550        0.12640
C         -0.43010        1.11870       -0.66640
C          2.68180       -0.30080       -0.29980
H         -2.84850        1.15470       -1.23230
H         -3.52290        0.07330        0.09370
H         -2.63460       -0.66830       -1.27020
H         -1.60560        0.85290        1.10750
H         -1.39070       -1.76170       -0.31790
H         -1.37280       -1.39040        1.41450
H          0.81040       -1.94460        1.30110
H          0.85570       -1.87970       -0.46570
H          1.81310        0.05870        1.59140
H          0.60110        1.87300        1.02150
H          1.56100        1.83790       -0.47100
H         -0.86000        2.15890       -0.72350
H         -0.24850        0.67710       -1.64670
H          3.23880       -1.11710        0.20220
H          3.24940        0.61680       -0.44740
H          2.29760       -0.73690       -1.26430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

