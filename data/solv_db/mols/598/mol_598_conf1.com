%nproc=16
%mem=2GB
%chk=mol_598_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.82090       -0.18090       -0.08550
C         -1.38590       -0.05620        0.27700
C         -0.69930        1.12820       -0.40210
C          0.60030        1.33070        0.28580
C          1.43170        0.13150        0.49180
C          0.74950       -1.17640        0.51980
C         -0.55320       -1.25960       -0.18220
C          2.56630        0.11230       -0.54280
H         -3.17820       -1.20950        0.05610
H         -3.08010        0.21170       -1.08510
H         -3.40680        0.44470        0.64880
H         -1.21580        0.07220        1.35640
H         -1.29880        2.04530       -0.35020
H         -0.52010        0.80950       -1.45830
H          0.39910        1.86590        1.25760
H          1.18560        2.08630       -0.31660
H          1.96960        0.25100        1.48030
H          0.59520       -1.57030        1.56660
H          1.43500       -1.94260        0.05130
H         -1.04600       -2.20610        0.03020
H         -0.35860       -1.15470       -1.28630
H          2.31570       -0.55570       -1.39250
H          2.81990        1.13870       -0.84050
H          3.49600       -0.31590       -0.07960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

