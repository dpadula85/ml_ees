%nproc=16
%mem=2GB
%chk=mol_598_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.52650       -0.23630       -0.74050
C         -1.47840       -0.07830        0.34420
C         -0.55700       -1.24700        0.35350
C          0.62240       -1.15290       -0.53400
C          1.25770        0.18060       -0.63740
C          0.63930        1.26330        0.16450
C         -0.83760        1.25080        0.25500
C          2.73130        0.09060       -0.24480
H         -2.56160        0.70650       -1.35450
H         -2.29240       -1.05730       -1.43900
H         -3.53510       -0.39210       -0.35630
H         -2.05090       -0.12000        1.31880
H         -0.23890       -1.52100        1.39980
H         -1.16390       -2.14070        0.02050
H          0.33970       -1.53900       -1.55790
H          1.37640       -1.91330       -0.17240
H          1.26670        0.51180       -1.71150
H          0.94110        2.24550       -0.30640
H          1.13260        1.28870        1.17290
H         -1.24870        1.81600       -0.63350
H         -1.10470        1.91780        1.12950
H          3.32210        0.88950       -0.71660
H          2.85350        0.13160        0.85430
H          3.11300       -0.89470       -0.60210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

