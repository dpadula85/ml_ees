%nproc=16
%mem=2GB
%chk=mol_598_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.78280        0.32870       -0.22160
C         -1.42630       -0.01710        0.36490
C         -0.62070        1.24240        0.46240
C          0.83510        1.08100        0.54240
C          1.40890       -0.27510        0.41250
C          0.63430       -1.31170       -0.27870
C         -0.80880       -1.06160       -0.51190
C          2.79110       -0.16980       -0.25400
H         -3.14600       -0.49500       -0.90250
H         -3.51870        0.59840        0.54210
H         -2.67930        1.21500       -0.89150
H         -1.63170       -0.42510        1.38690
H         -0.93440        1.88960       -0.40620
H         -1.04330        1.79560        1.35550
H          1.30780        1.74190       -0.23530
H          1.19240        1.55590        1.50740
H          1.66370       -0.64780        1.44650
H          1.07710       -1.54650       -1.29260
H          0.71860       -2.30940        0.25190
H         -1.00970       -0.74400       -1.57670
H         -1.41910       -1.98760       -0.38870
H          3.04330       -1.14830       -0.73950
H          2.77500        0.63200       -1.04530
H          3.57350        0.05860        0.47190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_598_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

