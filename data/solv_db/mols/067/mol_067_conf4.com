%nproc=16
%mem=2GB
%chk=mol_067_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.41320        0.22920        0.08510
C         -1.09350       -0.49800       -0.00240
C         -0.00160        0.37080       -0.49720
O         -0.15050        1.53760       -0.79800
C          1.35270       -0.22310       -0.62880
C          2.19340        0.17580        0.57870
H         -2.80100        0.53620       -0.89330
H         -3.14660       -0.52550        0.49190
H         -2.36530        1.04490        0.82670
H         -0.82150       -0.76760        1.05730
H         -1.19840       -1.40970       -0.59690
H          1.33590       -1.33210       -0.66670
H          1.87510        0.10610       -1.54760
H          1.62090       -0.01460        1.50280
H          3.10520       -0.45810        0.57330
H          2.50830        1.22830        0.51510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

