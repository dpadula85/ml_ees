%nproc=16
%mem=2GB
%chk=mol_067_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.18930       -0.19130       -0.56360
C         -1.35150        0.56230        0.46110
C          0.05730        0.51050       -0.00890
O          0.37830        1.28970       -0.88320
C          1.03830       -0.44030        0.55160
C          2.37630       -0.24410       -0.13220
H         -2.55220        0.55510       -1.30510
H         -3.01960       -0.68930       -0.01990
H         -1.56080       -0.92040       -1.12890
H         -1.65190        1.65220        0.48910
H         -1.48570        0.17790        1.48670
H          0.74280       -1.50990        0.32660
H          1.18330       -0.29210        1.64330
H          3.07660       -1.07410        0.05290
H          2.16510       -0.12970       -1.21450
H          2.79310        0.74360        0.19840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

