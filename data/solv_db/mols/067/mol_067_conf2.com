%nproc=16
%mem=2GB
%chk=mol_067_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.19590       -0.24650        0.20420
C         -1.23490        0.85330       -0.18990
C          0.11210        0.64320        0.37910
O          0.48960        1.18930        1.38180
C          1.00680       -0.30920       -0.30980
C          2.31910       -0.51740        0.40640
H         -2.68090       -0.67510       -0.70150
H         -1.70260       -1.07950        0.74100
H         -2.98630        0.14440        0.85730
H         -1.19990        1.01510       -1.28920
H         -1.65690        1.82470        0.20050
H          1.23960        0.16730       -1.29120
H          0.47300       -1.25650       -0.51630
H          3.17570       -0.44590       -0.29690
H          2.39050       -1.52690        0.86950
H          2.45120        0.21960        1.21930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

