%nproc=16
%mem=2GB
%chk=mol_067_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.38610        0.36940        0.21580
C          1.11260       -0.29770       -0.22760
C         -0.02620       -0.04650        0.68100
O          0.12030        0.64190        1.65850
C         -1.33810       -0.64320        0.37290
C         -2.21950        0.41710       -0.28460
H          3.21790       -0.33900       -0.00200
H          2.60200        1.27790       -0.42280
H          2.43330        0.61550        1.27220
H          1.21230       -1.41330       -0.29560
H          0.86720        0.05210       -1.25820
H         -1.27510       -1.52080       -0.30750
H         -1.85490       -1.04290        1.28670
H         -1.66830        0.76570       -1.18580
H         -3.18440       -0.05370       -0.51640
H         -2.38530        1.21760        0.42980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

