%nproc=16
%mem=2GB
%chk=mol_067_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.77230        0.60420       -0.51410
C         -1.08470       -0.02420        0.67140
C         -0.12620       -1.09070        0.29080
O         -0.37270       -2.25990        0.58750
C          1.30590       -0.83790       -0.02530
C          1.63560        0.61910       -0.15590
H         -1.27960        1.55290       -0.82730
H         -1.81200       -0.04350       -1.39960
H         -2.81370        0.89050       -0.21980
H         -1.88540       -0.45190        1.32750
H         -0.60170        0.77860        1.28290
H          1.47310       -1.29380       -1.03350
H          2.00000       -1.34280        0.68050
H          2.71190        0.67000       -0.46000
H          1.55200        1.17590        0.78850
H          1.06980        1.05350       -0.99370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_067_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

