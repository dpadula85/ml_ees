%nproc=16
%mem=2GB
%chk=mol_141_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.02580       -0.40630       -0.08480
C         -1.05830       -1.22160        0.58140
O         -2.16380       -0.44990        0.98470
N         -3.00970        0.14770        0.05460
O         -3.96400       -0.44580       -0.39760
O         -3.17790        1.51900        0.29170
C          1.07460        0.14510        0.80230
O          1.66020        1.23950        0.11430
N          2.96500        1.36720       -0.20500
O          3.26010        2.33380       -0.96770
O          3.78900        0.25540       -0.32940
H         -0.44190        0.43530       -0.67690
H          0.50820       -1.04210       -0.86710
H         -0.57290       -1.48360        1.60880
H         -1.27870       -2.20810        0.14650
H          1.84670       -0.65000        0.96910
H          0.58930        0.46430        1.72580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

