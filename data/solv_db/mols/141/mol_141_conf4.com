%nproc=16
%mem=2GB
%chk=mol_141_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.17820       -0.62720       -0.27560
C         -0.97630        0.61990        0.12100
O         -2.11190        0.07760        0.70030
N         -3.38760        0.05470        0.32360
O         -3.92950       -1.10770        0.49810
O         -4.16550        1.08390       -0.00140
C          1.23930       -0.51730        0.11290
O          1.91580        0.57650       -0.49480
N          3.22870        0.68010       -0.03040
O          3.32950        0.96200        1.17900
O          4.13790       -0.18960       -0.55210
H         -0.17330       -0.66450       -1.41700
H         -0.62230       -1.54470        0.11620
H         -1.14050        1.32040       -0.69030
H         -0.33830        1.13680        0.86990
H          1.78960       -1.44770       -0.18070
H          1.38270       -0.41310        1.23100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

