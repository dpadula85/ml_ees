%nproc=16
%mem=2GB
%chk=mol_141_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.03160       -0.72950       -0.47560
C         -1.15010       -0.12780        0.23610
O         -2.32970       -0.32500       -0.52610
N         -3.56850       -0.05190        0.01140
O         -3.65020        0.19340        1.23940
O         -4.46440        0.61640       -0.80060
C          1.32020       -0.26600        0.13780
O          2.05580        0.59640       -0.72750
N          3.32680        0.91880       -0.28770
O          4.01960       -0.02080        0.17830
O          3.57820        2.17220        0.23400
H         -0.02400       -0.51350       -1.56010
H         -0.04520       -1.84110       -0.37150
H         -1.02510        0.95750        0.44400
H         -1.26680       -0.69670        1.18980
H          1.17940        0.23710        1.12760
H          2.01260       -1.11950        0.34990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

