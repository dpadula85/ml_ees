%nproc=16
%mem=2GB
%chk=mol_141_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.03220       -0.14060       -1.18170
C         -0.97140        0.01320       -0.03340
O         -2.16050        0.51150       -0.60760
N         -3.28950        0.77870        0.10840
O         -4.37410        0.94980       -0.47070
O         -3.22130        0.85750        1.47380
C          1.32230       -0.68230       -0.55080
O          1.72040        0.29100        0.40020
N          2.85250        0.06580        1.11970
O          3.18200       -1.06490        1.50860
O          3.66280        1.12510        1.42860
H          0.21560        0.83950       -1.64020
H         -0.32910       -0.85000       -1.92800
H         -1.21620       -0.97810        0.40170
H         -0.58250        0.72290        0.70730
H          1.10310       -1.60470        0.02510
H          2.05370       -0.83430       -1.34050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

