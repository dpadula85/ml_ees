%nproc=16
%mem=2GB
%chk=mol_141_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.08070       -0.47000        0.96660
C         -1.03910       -0.84830        0.06150
O         -2.21940       -0.29820        0.62590
N         -3.14410        0.15000       -0.25960
O         -3.78050       -0.58260       -0.98060
O         -3.27110        1.56070       -0.28770
C          1.38760       -0.45760        0.22350
O          1.47890        0.82010       -0.38670
N          2.68540        1.47730       -0.11060
O          2.63640        2.73730       -0.10420
O          3.77510        0.82680        0.36470
H         -0.15540        0.66240        1.21520
H          0.15240       -0.92860        1.92720
H         -0.87620       -0.68420       -0.97680
H         -1.14940       -2.00800        0.19370
H          1.24660       -1.17980       -0.69560
H          2.19210       -0.77730        0.84520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_141_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

