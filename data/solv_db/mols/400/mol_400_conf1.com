%nproc=16
%mem=2GB
%chk=mol_400_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73540        0.78510       -0.13070
C         -1.70460        0.04890        0.69450
C         -0.45870       -0.08560       -0.18810
C          0.64120       -0.81270        0.54770
C          1.84680       -0.92790       -0.34790
C          2.38250        0.41710       -0.77490
I          2.96300        1.57480        0.91120
H         -2.49910        0.71640       -1.21160
H         -3.73570        0.37780        0.11120
H         -2.63920        1.86910        0.13990
H         -1.48180        0.58150        1.63080
H         -2.09540       -0.95230        0.96580
H         -0.10510        0.90070       -0.51630
H         -0.77640       -0.66050       -1.08170
H          0.33940       -1.79830        0.89800
H          0.89850       -0.21530        1.46240
H          1.49430       -1.44350       -1.28220
H          2.65640       -1.51500        0.10790
H          1.70070        0.93510       -1.45290
H          3.30890        0.20450       -1.38470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

