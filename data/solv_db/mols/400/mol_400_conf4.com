%nproc=16
%mem=2GB
%chk=mol_400_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.19210        0.10420        0.09670
C          1.78540        0.37880        0.53910
C          0.74620       -0.22480       -0.36960
C         -0.61190        0.13130        0.19660
C         -1.73780       -0.42690       -0.64160
C         -3.03580        0.01040        0.04830
I         -4.70310       -0.71520       -1.03800
H          3.61340       -0.75750        0.63200
H          3.20880       -0.01510       -1.00200
H          3.80730        0.98610        0.38130
H          1.61380        1.49300        0.50730
H          1.56970        0.02960        1.56690
H          0.81330       -1.34390       -0.31660
H          0.82520        0.10110       -1.41600
H         -0.74630       -0.27020        1.21980
H         -0.77310        1.22770        0.25950
H         -1.70600       -1.50690       -0.77350
H         -1.72290        0.06450       -1.63550
H         -3.08240       -0.37850        1.08300
H         -3.05620        1.11230        0.12390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

