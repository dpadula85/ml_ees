%nproc=16
%mem=2GB
%chk=mol_400_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.18100        0.68800        0.61440
C         -1.75370       -0.19430       -0.51350
C         -0.71300       -1.19720       -0.13410
C          0.55140       -0.61860        0.37430
C          1.25580        0.28500       -0.60610
C          2.53550        0.82820        0.00080
I          3.78430       -0.84370        0.48150
H         -2.78180        0.14940        1.38630
H         -2.87970        1.45470        0.18890
H         -1.35640        1.23020        1.09170
H         -1.38730        0.42590       -1.34140
H         -2.65960       -0.77190       -0.84260
H         -0.46910       -1.77780       -1.06760
H         -1.08430       -1.93960        0.58570
H          1.24990       -1.44830        0.60590
H          0.42090       -0.04980        1.31850
H          1.50320       -0.29450       -1.49950
H          0.59150        1.15120       -0.85080
H          2.32270        1.42050        0.91920
H          3.05080        1.50270       -0.71160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

