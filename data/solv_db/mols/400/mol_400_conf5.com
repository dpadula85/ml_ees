%nproc=16
%mem=2GB
%chk=mol_400_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.74810        0.82020        0.19030
C          1.81510       -0.22260       -0.35750
C          0.38480        0.02530        0.12850
C         -0.56000       -1.00080       -0.40480
C         -1.94880       -0.73220        0.08890
C         -2.46230        0.62680       -0.33900
I         -2.50810        0.79460       -2.46500
H          2.12870        1.72920        0.42390
H          3.17690        0.50420        1.16110
H          3.52040        1.14620       -0.50940
H          1.85210       -0.32790       -1.45160
H          2.14210       -1.20900        0.07490
H          0.36130       -0.00290        1.22010
H          0.10750        1.01570       -0.30640
H         -0.28710       -2.02680       -0.06660
H         -0.55150       -1.05390       -1.52240
H         -2.63240       -1.49860       -0.31420
H         -1.95060       -0.84290        1.18360
H         -1.84840        1.45660        0.04490
H         -3.48780        0.79880        0.04850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

