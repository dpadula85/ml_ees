%nproc=16
%mem=2GB
%chk=mol_400_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.88120       -0.08430       -0.30370
C          1.71230       -0.34930        0.61600
C          0.43460        0.29580        0.10020
C         -0.66000       -0.02770        1.08400
C         -1.98890        0.55860        0.68510
C         -2.45150        0.03900       -0.66650
I         -2.63730       -2.08680       -0.52550
H          2.91230       -0.95030       -1.02070
H          3.84480       -0.11740        0.25550
H          2.82450        0.88560       -0.80440
H          1.50640       -1.42150        0.74390
H          1.95880        0.05780        1.61790
H          0.20620       -0.07830       -0.90860
H          0.56840        1.39050        0.04820
H         -0.79370       -1.12600        1.17680
H         -0.39920        0.38820        2.07060
H         -2.00260        1.65760        0.73310
H         -2.73500        0.19830        1.42400
H         -3.44430        0.51490       -0.85770
H         -1.73700        0.25530       -1.47230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_400_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

