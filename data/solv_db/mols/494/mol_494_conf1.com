%nproc=16
%mem=2GB
%chk=mol_494_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31440        0.66600       -0.02270
C         -1.21540       -0.71760       -0.01560
C          0.00180       -1.33600        0.00540
C          1.18890       -0.61680        0.02050
C          1.08530        0.75990        0.01340
C         -0.14120        1.39550       -0.00780
Cl         2.75510       -1.43250        0.04780
H         -2.23680        1.19540       -0.03890
H         -2.11960       -1.27440       -0.02710
H          0.11750       -2.42860        0.01120
H          2.02970        1.30900        0.02570
H         -0.15080        2.48020       -0.01200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_494_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_494_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_494_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

