%nproc=16
%mem=2GB
%chk=mol_581_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.42310       -0.85430       -0.56540
C          0.92680       -0.75800        0.16020
C          1.08940        0.73510        0.17310
C         -0.10200        1.32300        0.16220
C         -1.15170        0.26970        0.14340
H         -0.89670       -1.82910       -0.42580
H         -0.32440       -0.54370       -1.62460
H          1.72410       -1.26910       -0.37680
H          0.73730       -1.14650        1.18530
H          2.06050        1.19730        0.18870
H         -0.21050        2.40570        0.16730
H         -1.35980       -0.11380        1.18290
H         -2.06990        0.58370       -0.37060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

