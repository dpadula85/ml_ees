%nproc=16
%mem=2GB
%chk=mol_581_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.97480       -0.34760        0.43000
C          0.23980       -1.18720        0.07330
C          1.31200       -0.12860        0.10370
C          0.77420        1.05230       -0.16820
C         -0.68390        0.88440       -0.40100
H         -0.88720       -0.04400        1.49090
H         -1.91650       -0.82950        0.17820
H          0.12960       -1.48860       -0.98680
H          0.43190       -1.99860        0.76790
H          2.35670       -0.28960        0.31220
H          1.28230        2.00400       -0.22070
H         -0.78870        0.61400       -1.47040
H         -1.27530        1.75930       -0.10910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

