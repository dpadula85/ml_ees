%nproc=16
%mem=2GB
%chk=mol_581_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.55170       -0.87410       -0.41980
C         -1.09930        0.27300        0.40660
C         -0.01800        1.29290        0.38800
C          1.11340        0.72720        0.00500
C          0.91770       -0.71600       -0.27140
H         -0.82900       -0.77440       -1.47180
H         -0.96380       -1.81600       -0.02250
H         -2.03400        0.63090       -0.05840
H         -1.23350       -0.03390        1.46280
H         -0.18090        2.32560        0.65520
H          2.04160        1.24270       -0.09120
H          1.34620       -1.27140        0.58910
H          1.49120       -1.00640       -1.17160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

