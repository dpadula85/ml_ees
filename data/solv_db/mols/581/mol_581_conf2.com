%nproc=16
%mem=2GB
%chk=mol_581_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.29210       -0.94000        0.51450
C         -0.95350       -0.68500       -0.30710
C         -1.01940        0.80840       -0.28020
C          0.19490        1.30720       -0.07230
C          1.18730        0.19590        0.05980
H          0.72500       -1.92340        0.34760
H          0.10090       -0.76250        1.60060
H         -0.72350       -1.01470       -1.34000
H         -1.84740       -1.15460        0.11870
H         -1.93790        1.37780       -0.41380
H          0.41000        2.34810       -0.01170
H          1.99550        0.42240        0.76120
H          1.57600        0.02040       -0.97740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

