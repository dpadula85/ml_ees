%nproc=16
%mem=2GB
%chk=mol_581_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.31750       -0.93360        0.50740
C         -1.13080        0.03260       -0.31800
C         -0.33190        1.27070       -0.15700
C          0.93310        0.92170       -0.01290
C          1.06780       -0.57830       -0.05180
H         -0.34700       -0.56710        1.56550
H         -0.57230       -1.97220        0.38010
H         -1.06180       -0.28130       -1.38390
H         -2.17020        0.13410        0.03490
H         -0.78380        2.26960       -0.16650
H          1.76870        1.58980        0.11570
H          1.87430       -0.94630        0.57750
H          1.07160       -0.93980       -1.09090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_581_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

