%nproc=16
%mem=2GB
%chk=mol_614_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.53830        0.00000        0.25470
C         -2.78340       -0.67950        1.19000
C         -1.40710       -0.66870        1.14380
C         -0.80430        0.04450        0.13030
C         -1.52200        0.73400       -0.82270
C         -2.92080        0.70830       -0.75350
C          0.64710        0.04240        0.02790
C          1.32740       -1.17780        0.08230
C          2.69740       -1.25610       -0.05010
C          3.44780       -0.12950       -0.24150
C          2.81130        1.07600       -0.29770
C          1.42200        1.15540       -0.16240
Cl         0.67800        2.71120       -0.18510
Cl         3.72260        2.55170       -0.54100
Cl         5.19660       -0.23860       -0.41120
Cl         3.53130       -2.80880        0.02250
H         -4.63670       -0.02210        0.31170
H         -3.26510       -1.23530        1.98020
H         -0.84740       -1.21760        1.90040
H         -1.05250        1.28080       -1.64480
H         -3.49860        1.23850       -1.48370
H          0.79480       -2.10900        0.23160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

