%nproc=16
%mem=2GB
%chk=mol_614_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.53670       -0.18680        0.19110
C          2.76880       -0.93280        1.04590
C          1.38640       -0.85090        0.91710
C          0.79470       -0.04980       -0.03020
C          1.59290        0.70070       -0.89160
C          2.95650        0.61060       -0.75580
C         -0.67180        0.05870       -0.10490
C         -1.35900       -0.11510       -1.28820
C         -2.75090       -0.07400       -1.31690
C         -3.47780        0.14110       -0.16460
C         -2.77770        0.31510        1.01110
C         -1.37660        0.27800        1.05480
Cl        -0.58870        0.59280        2.57220
Cl        -3.69790        0.58810        2.46990
Cl        -5.24450        0.19380       -0.17620
Cl        -3.58110       -0.29760       -2.83050
H          4.62860       -0.19190        0.22320
H          3.23020       -1.56020        1.79120
H          0.77220       -1.46760        1.55610
H          1.10100        1.31990       -1.62810
H          3.54480        1.21270       -1.44820
H         -0.78680       -0.28500       -2.19750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

