%nproc=16
%mem=2GB
%chk=mol_614_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.51570       -0.48310        0.02140
C          2.78240       -0.75840        1.15790
C          1.42430       -0.52410        1.10110
C          0.77400       -0.04200       -0.00970
C          1.54090        0.22800       -1.14140
C          2.90630        0.00650       -1.12090
C         -0.66640        0.13690       -0.05890
C         -1.23790        1.32810       -0.45930
C         -2.62470        1.48370       -0.41930
C         -3.46330        0.47480        0.01280
C         -2.88170       -0.71100        0.40820
C         -1.50260       -0.86830        0.36740
Cl        -0.77980       -2.40710        0.77700
Cl        -3.86640       -2.04350        0.96970
Cl        -5.21170        0.70240        0.05420
Cl        -3.33580        3.00440       -0.93210
H          4.57320       -0.65650        0.03870
H          3.25810       -1.14790        2.07050
H          0.85780       -0.69370        2.00430
H          1.03080        0.60950       -2.01550
H          3.49360        0.22520       -2.02240
H         -0.58670        2.13580       -0.80370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_614_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

