%nproc=16
%mem=2GB
%chk=mol_417_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.77140       -0.23220       -0.05070
C          1.92770       -0.35900        1.18660
C          0.76470        0.61370        1.10440
C         -0.06230        0.26360       -0.13890
C         -1.20170        1.25180       -0.17940
C         -2.21690        0.47900       -1.02370
C         -2.21070       -0.85460       -0.26890
C         -0.74730       -1.05580        0.01850
H          2.86920        0.86220       -0.30780
H          2.31680       -0.75140       -0.93310
H          3.81310       -0.60390        0.10680
H          1.57670       -1.40480        1.26930
H          2.56230       -0.05070        2.04960
H          0.13610        0.57540        2.00690
H          1.15480        1.64680        1.01080
H          0.59180        0.33300       -1.02870
H         -0.94130        2.21290       -0.62000
H         -1.62260        1.31880        0.84010
H         -1.87300        0.32010       -2.04520
H         -3.22030        0.95970       -0.95180
H         -2.78490       -0.65510        0.66950
H         -2.64870       -1.66270       -0.87530
H         -0.31530       -1.82560       -0.68490
H         -0.63980       -1.38090        1.07000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

