%nproc=16
%mem=2GB
%chk=mol_417_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.21080        0.33050        0.12790
C          1.88920        0.19560       -0.61200
C          0.84660       -0.49770        0.18830
C         -0.41060       -0.62180       -0.61290
C         -0.97420        0.70820       -1.01100
C         -1.69710        1.08520        0.29250
C         -2.45970       -0.19130        0.56990
C         -1.51690       -1.28660        0.20450
H          3.50440        1.37830        0.27990
H          3.97870       -0.12150       -0.53570
H          3.21250       -0.22410        1.08820
H          1.55570        1.23640       -0.79940
H          1.99290       -0.28960       -1.59000
H          0.64570        0.07300        1.14450
H          1.17970       -1.52120        0.49820
H         -0.25720       -1.20740       -1.55360
H         -1.82590        0.58840       -1.73820
H         -0.26780        1.45690       -1.34020
H         -2.40440        1.91030        0.09170
H         -0.96560        1.29900        1.07020
H         -3.31940       -0.21570       -0.13800
H         -2.82150       -0.24650        1.61790
H         -1.08180       -1.80580        1.08930
H         -2.01420       -2.03250       -0.41860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

