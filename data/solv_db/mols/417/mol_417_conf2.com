%nproc=16
%mem=2GB
%chk=mol_417_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.30530        0.06480        0.10550
C         -2.00100       -0.45900       -0.46580
C         -0.91740        0.48900       -0.08020
C          0.44720        0.13630       -0.55650
C          1.39960        1.21960       -0.06190
C          2.65550        0.43350        0.17040
C          2.08200       -0.80880        0.86670
C          0.98330       -1.17020       -0.14270
H         -3.33600       -0.08100        1.22400
H         -3.39300        1.17050       -0.03590
H         -4.18740       -0.39790       -0.36080
H         -1.87700       -1.47780       -0.07760
H         -2.12830       -0.44060       -1.58040
H         -1.17650        1.48740       -0.49850
H         -0.94740        0.61100        1.02340
H          0.48560        0.20110       -1.67330
H          0.99510        1.61760        0.89660
H          1.52060        2.02380       -0.81270
H          3.06370        0.12520       -0.81150
H          3.39460        0.94350        0.80940
H          2.82070       -1.60180        0.97320
H          1.60110       -0.49280        1.81230
H          0.28040       -1.89760        0.25200
H          1.53950       -1.69570       -0.97540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

