%nproc=16
%mem=2GB
%chk=mol_417_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.16140        0.08100        0.47740
C          1.63170        0.05400        0.60880
C          1.08070       -0.10890       -0.76500
C         -0.40790       -0.15750       -0.84290
C         -1.05270       -1.25130       -0.09370
C         -2.37070       -0.67870        0.38330
C         -2.22760        0.80840        0.41160
C         -0.98470        1.15640       -0.39020
H          3.46900        0.54350       -0.48400
H          3.62290        0.67420        1.28930
H          3.56550       -0.94940        0.46640
H          1.35700        0.97590        1.12140
H          1.37620       -0.84360        1.22740
H          1.51610       -1.04950       -1.18970
H          1.48040        0.70990       -1.39700
H         -0.67470       -0.27280       -1.92950
H         -1.30770       -2.11150       -0.76680
H         -0.50650       -1.61930        0.79120
H         -2.60220       -1.03200        1.41600
H         -3.20870       -0.95270       -0.31630
H         -2.14600        1.19880        1.46070
H         -3.14180        1.30000       -0.01560
H         -1.31510        1.77950       -1.24830
H         -0.31440        1.74540        0.24270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

