%nproc=16
%mem=2GB
%chk=mol_417_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.80910        0.79520        0.44510
C          2.02040        0.27310       -0.73330
C          0.84540       -0.57740       -0.31340
C         -0.13820        0.17400        0.54960
C         -1.24500       -0.82610        0.87560
C         -2.19100       -0.58440       -0.29730
C         -2.27090        0.94800       -0.26270
C         -0.80090        1.30340       -0.19970
H          3.90580        0.69390        0.29160
H          2.58610        0.22570        1.38560
H          2.58110        1.86210        0.55760
H          1.63050        1.10400       -1.36780
H          2.67950       -0.36560       -1.34580
H          0.32230       -0.89930       -1.21370
H          1.17910       -1.47220        0.25020
H          0.28170        0.52290        1.49390
H         -0.85990       -1.85240        0.89780
H         -1.76850       -0.52270        1.79690
H         -3.17890       -1.01770       -0.13170
H         -1.71810       -0.83750       -1.25540
H         -2.79540        1.18140        0.70040
H         -2.78220        1.34080       -1.14430
H         -0.66600        2.28310        0.26420
H         -0.42640        1.28590       -1.24310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_417_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

