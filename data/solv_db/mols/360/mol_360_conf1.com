%nproc=16
%mem=2GB
%chk=mol_360_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
N         -0.01720        0.00590        0.27740
H         -0.57830       -0.80450       -0.09390
H         -0.35860        0.91220       -0.09560
H          0.95400       -0.11350       -0.08790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_360_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_360_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_360_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

