%nproc=16
%mem=2GB
%chk=mol_342_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07740        0.60200        0.82910
C         -1.24590        0.42270       -0.43180
C         -1.84710       -0.69370       -1.24500
C          0.18250        0.18730       -0.10820
C          1.16250        1.06720       -0.51530
C          2.49600        0.85940       -0.22090
C          2.89040       -0.23490        0.48990
C          1.93530       -1.13590        0.91290
C          0.58570       -0.91780        0.61010
H         -1.59610        0.16200        1.71560
H         -3.09310        0.17570        0.67740
H         -2.25150        1.69910        1.03310
H         -1.36130        1.35910       -1.01170
H         -1.03880       -1.36780       -1.60930
H         -2.49370       -0.32640       -2.07160
H         -2.48220       -1.32290       -0.58080
H          0.92960        1.96940       -1.08950
H          3.24300        1.57560       -0.55870
H          3.92820       -0.41700        0.73140
H          2.24180       -2.01280        1.48110
H         -0.10780       -1.65010        0.96450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

