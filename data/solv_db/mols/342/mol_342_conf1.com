%nproc=16
%mem=2GB
%chk=mol_342_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.79330       -0.80860        0.95760
C         -1.24010       -0.36980       -0.40660
C         -2.09940        0.73640       -0.89320
C          0.16510       -0.10860       -0.25080
C          1.08060       -0.92870       -0.90480
C          2.44120       -0.73530       -0.79940
C          2.93880        0.29950       -0.02540
C          2.02080        1.12470        0.63270
C          0.66650        0.91950        0.51800
H         -2.79270       -1.28810        0.74890
H         -1.91040        0.03600        1.64000
H         -1.15540       -1.57310        1.42700
H         -1.39340       -1.24370       -1.06890
H         -1.66230        1.73860       -0.84770
H         -3.07070        0.77200       -0.35570
H         -2.38560        0.57930       -1.97640
H          0.73920       -1.76010       -1.52840
H          3.15410       -1.36980       -1.30560
H          3.99240        0.49540        0.09150
H          2.36090        1.94630        1.24900
H         -0.05640        1.53810        1.01220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

