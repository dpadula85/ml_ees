%nproc=16
%mem=2GB
%chk=mol_342_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99180       -0.69400        1.04000
C         -1.31690        0.26680        0.11390
C         -1.93690        0.10880       -1.28020
C          0.14000        0.15860        0.07680
C          0.91910        1.25540        0.42110
C          2.29750        1.25120        0.41640
C          2.96680        0.10070        0.05160
C          2.20430       -1.01310       -0.29800
C          0.83020       -0.97110       -0.28140
H         -3.09380       -0.62000        0.84230
H         -1.71920       -1.73650        0.91550
H         -1.87500       -0.32760        2.09730
H         -1.58580        1.29110        0.43720
H         -1.29810        0.65980       -1.99650
H         -2.93160        0.58900       -1.30650
H         -2.03770       -0.95590       -1.56080
H          0.45710        2.20410        0.72160
H          2.89070        2.11490        0.68760
H          4.04920        0.08680        0.04450
H          2.74310       -1.92650       -0.58750
H          0.28880       -1.84270       -0.55500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

