%nproc=16
%mem=2GB
%chk=mol_342_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.00650       -0.45580       -1.08210
C         -1.32080       -0.34530        0.27580
C         -1.82930        0.93110        0.90930
C          0.15440       -0.27280        0.12350
C          0.66410        0.80360       -0.59190
C          2.02540        0.93430       -0.77250
C          2.88060       -0.02430       -0.22990
C          2.37540       -1.09460        0.48130
C          1.00090       -1.22080        0.66000
H         -2.02620       -1.50840       -1.35630
H         -3.04780       -0.10360       -0.92930
H         -1.49960        0.20770       -1.80880
H         -1.61780       -1.22250        0.85140
H         -1.98980        1.66850        0.07730
H         -1.00270        1.31410        1.55630
H         -2.76560        0.79400        1.45780
H         -0.00630        1.56320       -1.02240
H          2.41850        1.77700       -1.33180
H          3.94330        0.12680       -0.40170
H          3.06960       -1.83050        0.89480
H          0.58000       -2.04180        1.20810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

