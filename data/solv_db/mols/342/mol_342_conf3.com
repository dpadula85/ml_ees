%nproc=16
%mem=2GB
%chk=mol_342_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.60120        1.39330        0.79950
C         -1.24980        0.60250       -0.46740
C         -2.10370       -0.64280       -0.57120
C          0.15420        0.14880       -0.28460
C          1.18860        0.54070       -1.10430
C          2.48050        0.06890       -0.86480
C          2.70790       -0.79230        0.20020
C          1.68310       -1.19390        1.02990
C          0.40950       -0.71240        0.77220
H         -1.68110        0.66200        1.63150
H         -0.74680        2.07360        1.03640
H         -2.49590        2.00380        0.66540
H         -1.36270        1.27910       -1.32380
H         -1.52050       -1.51160       -0.98020
H         -2.35560       -0.95850        0.48250
H         -2.97820       -0.53010       -1.20850
H          0.96530        1.21290       -1.92230
H          3.29090        0.37910       -1.51110
H          3.72610       -1.12650        0.33940
H          1.88670       -1.86530        1.85210
H         -0.39730       -1.03140        1.42920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_342_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

