%nproc=16
%mem=2GB
%chk=mol_608_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.14560       -0.01950        0.17900
C          2.81660       -0.29370       -0.47000
C          1.66390        0.32070        0.30700
C          0.42140       -0.04360       -0.46810
C         -0.83980        0.48130        0.14900
C         -1.99420        0.03870       -0.73050
C         -3.29650        0.50920       -0.20530
O         -3.64000        1.63820       -0.40760
C         -4.12010       -0.47990        0.56470
H          4.26290        0.99940        0.55450
H          4.25020       -0.73680        1.03760
H          4.93590       -0.30010       -0.54770
H          2.80660        0.05180       -1.51040
H          2.66420       -1.39170       -0.49010
H          1.65110       -0.13200        1.33600
H          1.78380        1.41610        0.40640
H          0.41240       -1.14360       -0.57780
H          0.56470        0.39490       -1.49340
H         -0.84240        1.54440        0.35500
H         -0.97580       -0.05410        1.12610
H         -1.78130        0.46080       -1.74260
H         -1.96690       -1.06570       -0.83210
H         -3.94310       -0.33760        1.67060
H         -3.79930       -1.51300        0.34770
H         -5.17990       -0.34440        0.30740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

