%nproc=16
%mem=2GB
%chk=mol_608_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.06480       -0.26540        0.65880
C         -1.91910       -1.02980        0.05540
C         -1.36590       -0.34350       -1.17610
C         -0.87090        1.03180       -0.81050
C          0.21710        1.01790        0.23250
C          1.38710        0.25120       -0.26600
C          2.51080        0.20260        0.71670
O          2.47050        0.73120        1.80040
C          3.72820       -0.54760        0.29970
H         -3.46420       -0.89040        1.49300
H         -2.76430        0.69860        1.10680
H         -3.89180       -0.15460       -0.08130
H         -2.32930       -2.01570       -0.27260
H         -1.15310       -1.23210        0.80060
H         -0.51540       -0.90940       -1.59470
H         -2.13200       -0.25630       -1.97360
H         -0.52130        1.56910       -1.72560
H         -1.78010        1.59200       -0.44060
H         -0.16190        0.67930        1.21460
H          0.54730        2.08280        0.34260
H          1.78070        0.76290       -1.16840
H          1.16510       -0.82300       -0.49590
H          4.17910       -1.11190        1.13910
H          3.50470       -1.26440       -0.50740
H          4.44350        0.22450       -0.09870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

