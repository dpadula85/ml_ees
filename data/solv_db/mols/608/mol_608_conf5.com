%nproc=16
%mem=2GB
%chk=mol_608_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.91370       -0.11370       -0.25340
C          2.43170       -0.48790       -0.23790
C          1.81300        0.24380        0.91380
C          0.35070        0.00160        1.09500
C         -0.43720        0.42540       -0.12720
C         -1.88820        0.13620        0.17210
C         -2.78730        0.49740       -0.93100
O         -2.42230        0.98180       -1.98790
C         -4.24610        0.25730       -0.75080
H          4.07780        0.90820        0.12870
H          4.50940       -0.81600        0.36850
H          4.24380       -0.14080       -1.29340
H          2.36960       -1.57590       -0.06660
H          1.95080       -0.27000       -1.19730
H          1.92870        1.34470        0.69730
H          2.36490        0.04930        1.85280
H          0.11300       -1.04900        1.38020
H          0.01640        0.62410        1.95250
H         -0.24080        1.48880       -0.28990
H         -0.07310       -0.15890       -1.01010
H         -2.16110        0.75380        1.05490
H         -1.95880       -0.92370        0.47050
H         -4.50310        0.47850        0.32570
H         -4.86480        0.92000       -1.37500
H         -4.50070       -0.81080       -0.89140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

