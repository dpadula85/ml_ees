%nproc=16
%mem=2GB
%chk=mol_608_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.36340        0.82440       -0.39950
C         -1.82850        0.70140       -0.26540
C         -1.58700       -0.47270        0.57460
C         -0.25230       -0.91640        0.97000
C          0.77710       -1.30410        0.00880
C          1.34740       -0.27570       -0.91170
C          2.04900        0.80470       -0.17820
O          1.48500        1.83030        0.13360
C          3.50720        0.61350        0.19470
H         -3.87680       -0.07260       -0.00930
H         -3.67620        0.96650       -1.43850
H         -3.70870        1.73180        0.15910
H         -1.48520        0.66110       -1.31290
H         -1.46900        1.61980        0.23920
H         -2.19240       -1.32940        0.12030
H         -2.16610       -0.30810        1.55270
H          0.24000       -0.18260        1.70410
H         -0.43780       -1.80610        1.67450
H          0.50460       -2.24760       -0.56760
H          1.68550       -1.68370        0.59640
H          2.19170       -0.82940       -1.46030
H          0.68760        0.11670       -1.68480
H          4.13020        0.57650       -0.71330
H          3.80920        1.37280        0.92500
H          3.62900       -0.39130        0.69110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

