%nproc=16
%mem=2GB
%chk=mol_608_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.49450        0.19610        0.90300
C          2.75180       -0.53360       -0.16500
C          1.46980        0.17240       -0.58900
C          0.60930        0.26300        0.64640
C         -0.70200        0.93810        0.42660
C         -1.49980        0.18440       -0.60020
C         -2.82320        0.81300       -0.85620
O         -2.85740        1.81280       -1.54690
C         -4.09170        0.27380       -0.30730
H          3.38460       -0.28860        1.90320
H          4.56800        0.21750        0.62030
H          3.15960        1.24280        0.96590
H          3.36770       -0.77310       -1.06140
H          2.41960       -1.52120        0.26540
H          0.99380       -0.52700       -1.32110
H          1.65600        1.13560       -1.07240
H          1.23160        0.76960        1.42470
H          0.48400       -0.78190        1.02220
H         -1.24840        0.93610        1.39490
H         -0.54590        2.00530        0.13910
H         -0.93050        0.03360       -1.54980
H         -1.65290       -0.84620       -0.18740
H         -4.84230        0.06130       -1.09590
H         -4.54190        0.98360        0.41770
H         -3.85430       -0.67390        0.22320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_608_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

