%nproc=16
%mem=2GB
%chk=mol_574_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.28080       -0.08570        0.49760
C         -2.68160       -0.40360       -0.85100
C         -1.18790       -0.72560       -0.71510
C         -0.53330        0.49200       -0.12870
C          0.91560        0.33410        0.06310
C          1.76090        0.69400       -0.96170
C          3.12000        0.57730       -0.86080
C          3.67290        0.08490        0.29920
C          2.83020       -0.27540        1.32470
C          1.46330       -0.15700        1.22060
H         -4.30890       -0.52680        0.58640
H         -3.44280        1.02100        0.62860
H         -2.60480       -0.48220        1.26600
H         -3.22610       -1.26290       -1.29110
H         -2.83800        0.45710       -1.52560
H         -1.06940       -1.59920       -0.04350
H         -0.77620       -1.00580       -1.68680
H         -0.98600        0.69050        0.85820
H         -0.74830        1.34010       -0.83260
H          1.32460        1.08640       -1.88650
H          3.77640        0.86680       -1.68180
H          4.76550       -0.01630        0.39870
H          3.26390       -0.66620        2.24610
H          0.79090       -0.43750        2.02150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

