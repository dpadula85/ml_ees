%nproc=16
%mem=2GB
%chk=mol_574_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.62670        0.50080        0.81980
C          2.74070        0.02810       -0.32190
C          1.31130        0.20220        0.15660
C          0.38460       -0.23950       -0.90620
C         -1.03380       -0.15350       -0.64620
C         -1.83340        0.94000       -0.92450
C         -3.18590        0.94040       -0.64330
C         -3.76190       -0.18320       -0.06670
C         -2.99210       -1.29510        0.22530
C         -1.65120       -1.25500       -0.07000
H          3.20990        0.03360        1.74530
H          3.49260        1.60530        0.88690
H          4.68290        0.20610        0.68420
H          2.96050       -1.00230       -0.63070
H          2.89590        0.73700       -1.16630
H          1.20880        1.22550        0.50540
H          1.21050       -0.45490        1.06750
H          0.64070       -1.28260       -1.18750
H          0.60840        0.36750       -1.83090
H         -1.38180        1.79210       -1.36460
H         -3.77210        1.81370       -0.87670
H         -4.80670       -0.20320        0.15870
H         -3.47100       -2.16320        0.67810
H         -1.08360       -2.15990        0.17950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

