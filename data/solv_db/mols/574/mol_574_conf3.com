%nproc=16
%mem=2GB
%chk=mol_574_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.64580        0.82540        0.60090
C          2.10680       -0.53700        0.32590
C          1.53000       -0.70210       -1.04820
C          0.37640        0.18760       -1.34540
C         -0.80210        0.02380       -0.49250
C         -1.79980       -0.90730       -0.77390
C         -2.93810       -0.95380       -0.00030
C         -3.12210       -0.09260        1.06070
C         -2.15700        0.83450        1.35730
C         -1.01280        0.87140        0.56540
H          2.11400        1.28840        1.48030
H          2.53900        1.47880       -0.26900
H          3.72600        0.79970        0.93040
H          2.94360       -1.26110        0.45690
H          1.30450       -0.79930        1.06960
H          1.32270       -1.77070       -1.26560
H          2.34360       -0.41580       -1.78070
H          0.74470        1.24850       -1.21390
H          0.11440        0.12270       -2.43640
H         -1.64420       -1.58090       -1.61240
H         -3.69020       -1.70550       -0.26590
H         -4.03210       -0.15850        1.65120
H         -2.29530        1.52130        2.19710
H         -0.31770        1.68250        0.80840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

