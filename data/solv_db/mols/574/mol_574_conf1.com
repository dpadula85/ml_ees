%nproc=16
%mem=2GB
%chk=mol_574_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.77000       -0.60110       -0.30190
C          2.63300       -0.29840        0.65160
C          1.29520       -0.23170       -0.08010
C          0.24560        0.06920        0.93740
C         -1.11460        0.16890        0.39760
C         -1.57010        1.40110       -0.03220
C         -2.84660        1.56360       -0.55320
C         -3.69950        0.46270       -0.65020
C         -3.25870       -0.77280       -0.22540
C         -1.97900       -0.89660        0.28890
H          3.42700       -0.97860       -1.27050
H          4.46740       -1.34320        0.13460
H          4.33770        0.34060       -0.49370
H          2.83830        0.66500        1.18730
H          2.58360       -1.07400        1.45320
H          1.15960       -1.16760       -0.62370
H          1.39290        0.59220       -0.81200
H          0.48980        1.00310        1.44910
H          0.26260       -0.77840        1.68300
H         -0.95410        2.28990        0.01850
H         -3.22040        2.51880       -0.89420
H         -4.70200        0.55190       -1.05130
H         -3.93790       -1.62990       -0.30690
H         -1.61980       -1.85470        0.62520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_574_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

