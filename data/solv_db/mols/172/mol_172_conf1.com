%nproc=16
%mem=2GB
%chk=mol_172_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.09350        1.14680        0.01700
C         -1.16080        0.57110        0.00170
C         -1.22780       -0.81140       -0.01850
N         -0.11390       -1.56920       -0.02310
C          1.12560       -1.04480       -0.00870
C          1.20910        0.33550        0.01160
H          0.18340        2.23780        0.03320
H         -2.07880        1.18130        0.00520
H         -2.22350       -1.23670       -0.03020
H          2.01680       -1.63400       -0.01210
H          2.17650        0.82360        0.02370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_172_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_172_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_172_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

