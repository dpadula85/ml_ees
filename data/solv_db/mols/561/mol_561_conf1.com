%nproc=16
%mem=2GB
%chk=mol_561_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.66960        0.92080        0.83450
C         -1.17510        0.16080       -0.36310
C         -2.27710       -0.86280       -0.68070
N         -0.00350       -0.63190       -0.11690
C          1.15840        0.11400        0.25730
C          2.25670       -0.90940        0.54010
C          1.68180        0.99580       -0.85260
H         -2.09420        1.90620        0.51800
H         -0.90690        1.12290        1.59330
H         -2.51360        0.40900        1.36260
H         -1.02290        0.78580       -1.26040
H         -2.23090       -1.58660        0.17450
H         -3.26290       -0.39110       -0.75680
H         -1.93500       -1.38300       -1.58600
H         -0.20210       -1.34530        0.59970
H          1.03760        0.69840        1.17920
H          2.84010       -0.65140        1.44430
H          1.81570       -1.91800        0.74520
H          2.90880       -1.04520       -0.34190
H          2.48400        1.62490       -0.40190
H          0.91380        1.64150       -1.29920
H          2.19710        0.34440       -1.58940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

