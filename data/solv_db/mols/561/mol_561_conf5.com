%nproc=16
%mem=2GB
%chk=mol_561_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24740        0.88260       -0.52290
C         -1.16740       -0.18070       -0.20600
C         -1.82090       -1.22530        0.64320
N         -0.01990        0.45950        0.35320
C          1.24580        0.16030       -0.24070
C          1.79040       -1.20900        0.05600
C          2.24750        1.20690        0.15810
H         -1.95130        1.79390        0.04430
H         -3.23280        0.59710       -0.15600
H         -2.27110        1.15390       -1.58000
H         -0.95230       -0.62130       -1.21390
H         -2.11610       -0.74330        1.58920
H         -1.21430       -2.13950        0.75250
H         -2.72820       -1.57200        0.06590
H         -0.14640        1.48750        0.38460
H          1.14310        0.23780       -1.38890
H          2.75080       -1.13840        0.64590
H          2.09010       -1.72010       -0.87520
H          1.13600       -1.84210        0.65100
H          2.41010        1.14510        1.25010
H          1.85180        2.23380       -0.02700
H          3.20280        1.03330       -0.38360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

