%nproc=16
%mem=2GB
%chk=mol_561_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05090        0.87560        1.13770
C          1.12460       -0.11570        0.44020
C          1.89320       -0.58890       -0.79800
N         -0.01410        0.63830       -0.02300
C         -1.07430       -0.21500       -0.51830
C         -2.21220        0.67770       -0.97410
C         -1.63670       -1.09800        0.57650
H          1.52430        1.25590        2.04070
H          2.18660        1.74020        0.45160
H          3.01230        0.40040        1.39400
H          0.86930       -0.94550        1.11040
H          1.96570        0.27990       -1.49200
H          2.91700       -0.84470       -0.45810
H          1.38760       -1.46980       -1.24100
H         -0.30720        1.40740        0.58260
H         -0.69600       -0.79380       -1.36280
H         -2.01650        1.74300       -0.72890
H         -2.34380        0.62580       -2.07590
H         -3.17140        0.40450       -0.51390
H         -1.75670       -0.52670        1.51120
H         -2.65150       -1.41070        0.24480
H         -1.05100       -2.03990        0.69640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

