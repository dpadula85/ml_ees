%nproc=16
%mem=2GB
%chk=mol_561_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.40730       -1.36420       -0.43730
C         -1.16660        0.11290       -0.59260
C         -2.46350        0.84010       -0.18190
N         -0.08930        0.66780        0.10270
C          1.21180        0.28080       -0.20970
C          1.56310       -1.14420        0.04720
C          2.27550        1.14320        0.47410
H         -0.71880       -2.00680       -0.97570
H         -1.40340       -1.60510        0.63780
H         -2.45850       -1.60080       -0.81850
H         -1.06000        0.30970       -1.70270
H         -3.06540        1.05350       -1.07490
H         -2.19650        1.75680        0.37420
H         -3.03410        0.16430        0.50810
H         -0.28620        1.38630        0.78120
H          1.41270        0.45220       -1.32290
H          1.60310       -1.69260       -0.91860
H          0.93720       -1.63730        0.81570
H          2.60940       -1.21320        0.44720
H          3.24580        0.60250        0.50900
H          2.47980        2.06970       -0.11950
H          2.01130        1.42430        1.49160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

