%nproc=16
%mem=2GB
%chk=mol_561_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.86430       -0.83860       -1.11290
C          1.21010       -0.16410        0.01690
C          2.21490        0.72270        0.77070
N          0.00810        0.51370       -0.13100
C         -1.19500       -0.12640        0.15600
C         -2.00230       -0.93020       -0.76030
C         -2.09960        0.91520        0.87130
H          2.31440        0.01260       -1.76610
H          2.77140       -1.39090       -0.68540
H          1.19900       -1.44810       -1.72340
H          0.99430       -1.02340        0.77270
H          3.10130        0.15290        0.91090
H          2.42100        1.61910        0.15660
H          1.81470        1.01630        1.75480
H          0.00490        1.50430       -0.35530
H         -0.89220       -0.89820        1.00520
H         -2.90590       -1.31940       -0.15340
H         -2.57660       -0.16270       -1.41870
H         -1.52370       -1.69450       -1.35800
H         -2.61920        0.40770        1.66480
H         -1.38360        1.72240        1.27740
H         -2.72020        1.40990        0.10730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_561_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

