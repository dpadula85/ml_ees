%nproc=16
%mem=2GB
%chk=mol_386_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.53630       -0.63140        0.23970
C         -2.03730       -0.55740       -0.06660
C         -1.57520        0.85700       -0.19280
C         -0.11680        0.93910       -0.49350
C          0.75790        0.30740        0.57020
C          2.19090        0.43020        0.20360
C          2.91000       -0.65110        0.05000
C          4.30860       -0.57170       -0.30680
O          4.84760        0.53660       -0.47290
H         -3.75150       -1.40500        1.00940
H         -3.91130        0.34710        0.61650
H         -4.10890       -0.91740       -0.66610
H         -1.89210       -1.04800       -1.05340
H         -1.50540       -1.15650        0.68900
H         -2.11840        1.31570       -1.03810
H         -1.77160        1.46740        0.71360
H          0.06680        0.44730       -1.47060
H          0.23700        1.98370       -0.60760
H          0.56840        0.76900        1.55620
H          0.48430       -0.76080        0.66950
H          2.65810        1.39050        0.05970
H          2.41560       -1.62580        0.20120
H          4.87970       -1.46590       -0.42780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

