%nproc=16
%mem=2GB
%chk=mol_386_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.66020        0.38400       -0.28300
C         -2.26980       -0.21620       -0.27460
C         -1.45590        0.26210        0.88890
C         -0.06770       -0.30930        0.95380
C          0.67250        0.08470       -0.30670
C          2.07740       -0.46760       -0.26810
C          3.14350        0.30750       -0.31640
C          4.48900       -0.25400       -0.27730
O          5.48230        0.51020       -0.32500
H         -4.37170       -0.47020       -0.43580
H         -3.81190        1.08470       -1.11390
H         -3.90740        0.90190        0.65310
H         -2.28410       -1.32530       -0.25910
H         -1.79620        0.08380       -1.25470
H         -1.99140       -0.01050        1.82710
H         -1.41820        1.37880        0.84220
H          0.47480        0.10400        1.85030
H         -0.06090       -1.40170        1.11010
H          0.78820        1.18140       -0.39400
H          0.16660       -0.32230       -1.19340
H          2.18500       -1.55150       -0.19760
H          2.99040        1.36370       -0.38530
H          4.62550       -1.31830       -0.20790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

