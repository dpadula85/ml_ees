%nproc=16
%mem=2GB
%chk=mol_386_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.54590        0.14600        0.33460
C         -2.08270       -0.29270        0.23540
C         -1.51350        0.40630       -0.97250
C         -0.08590        0.11290       -1.25820
C          0.75480        0.53490       -0.08580
C          2.20210        0.26820       -0.31750
C          2.92000       -0.52660        0.45600
C          4.33160       -0.77720        0.21740
O          4.97540       -1.54060        0.97900
H         -3.84180        0.82080       -0.48000
H         -4.21480       -0.74300        0.31570
H         -3.74280        0.66670        1.28220
H         -2.00240       -1.39520        0.16300
H         -1.60550       -0.00580        1.18950
H         -2.15620        0.08270       -1.83990
H         -1.68710        1.50670       -0.80160
H          0.01730       -0.97470       -1.46000
H          0.22000        0.64540       -2.17790
H          0.66760        1.62620        0.09440
H          0.38040        0.02660        0.81250
H          2.71490        0.73250       -1.14800
H          2.42210       -0.98960        1.27920
H          4.87260       -0.33060       -0.60180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

