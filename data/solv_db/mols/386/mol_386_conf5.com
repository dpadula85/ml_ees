%nproc=16
%mem=2GB
%chk=mol_386_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.66850       -0.06970       -0.29360
C         -2.24540        0.45220       -0.13600
C         -1.46880       -0.57800        0.67220
C         -0.04690       -0.14640        0.87900
C          0.70090        0.04510       -0.41600
C          2.08330        0.46820       -0.07350
C          3.13370       -0.23680       -0.43390
C          4.49750        0.13520       -0.12340
O          4.71370        1.18060        0.52210
H         -4.28470        0.59710       -0.91040
H         -4.10640       -0.26210        0.70850
H         -3.58440       -1.05790       -0.79570
H         -1.79640        0.62970       -1.11100
H         -2.31090        1.37360        0.48740
H         -1.51580       -1.58200        0.20110
H         -1.95270       -0.60820        1.67170
H         -0.07420        0.86300        1.38170
H          0.46640       -0.83380        1.54380
H          0.26470        0.80650       -1.05820
H          0.69780       -0.92450       -0.95240
H          2.24030        1.37830        0.48690
H          2.94180       -1.15840       -1.00290
H          5.31490       -0.47170       -0.43910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

