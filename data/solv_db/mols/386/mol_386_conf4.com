%nproc=16
%mem=2GB
%chk=mol_386_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.86170       -0.30200       -0.28350
C          2.41910       -0.67070       -0.53970
C          1.48550        0.39880        0.02380
C          0.07770       -0.06290       -0.28290
C         -0.95890        0.91590        0.22650
C         -2.29710        0.38470       -0.11710
C         -3.22920        0.09370        0.78080
C         -4.52180       -0.42400        0.38270
O         -4.81630       -0.61890       -0.81460
H          4.48870       -1.01410       -0.85390
H          4.02450        0.73710       -0.60350
H          4.13360       -0.39870        0.77270
H          2.26220       -0.68800       -1.64270
H          2.13510       -1.63460       -0.10310
H          1.71130        1.32780       -0.54000
H          1.66850        0.51220        1.10780
H         -0.05860       -1.07130        0.15780
H          0.00760       -0.17530       -1.38560
H         -0.83820        0.92490        1.34690
H         -0.77680        1.93640       -0.10590
H         -2.55700        0.21360       -1.15230
H         -2.97520        0.26160        1.81130
H         -5.24630       -0.64610        1.14010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_386_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

