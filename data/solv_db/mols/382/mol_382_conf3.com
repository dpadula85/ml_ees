%nproc=16
%mem=2GB
%chk=mol_382_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08130       -0.15210        0.04430
C         -0.60010        0.05320       -0.00500
C          0.27100       -1.00560        0.15690
C          1.63150       -0.77870        0.10520
C          2.17600        0.47280       -0.10350
N          1.29770        1.48180       -0.25710
C         -0.03170        1.29390       -0.21200
H         -2.43910       -0.41880       -0.95970
H         -2.33840       -0.90450        0.82420
H         -2.60540        0.78230        0.33680
H         -0.14190       -1.99550        0.32170
H          2.33120       -1.60060        0.23060
H          3.26100        0.64170       -0.14280
H         -0.73050        2.13030       -0.33960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

