%nproc=16
%mem=2GB
%chk=mol_382_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.08490        0.16430       -0.03470
C         -0.60340       -0.05460       -0.01730
C          0.29040        0.99670        0.09590
C          1.65790        0.78640        0.11150
C          2.17630       -0.48910        0.01370
N          1.29460       -1.49700       -0.09550
C         -0.04270       -1.31250       -0.11260
H         -2.43340        0.29900       -1.08310
H         -2.38270        1.05180        0.54860
H         -2.63530       -0.69460        0.38150
H         -0.13020        2.00010        0.17270
H          2.35910        1.60770        0.20000
H          3.25080       -0.68930        0.02290
H         -0.71650       -2.16890       -0.20340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

