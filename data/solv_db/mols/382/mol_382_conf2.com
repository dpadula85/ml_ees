%nproc=16
%mem=2GB
%chk=mol_382_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04620        0.13860        0.00400
C          0.57840       -0.09850       -0.01050
C         -0.24370        1.01000        0.06350
C         -1.60240        0.77660        0.04930
C         -2.12960       -0.49320       -0.03430
N         -1.27120       -1.52650       -0.10340
C          0.05780       -1.36490       -0.09390
H          2.25000        0.90280       -0.77600
H          2.26580        0.64180        0.98750
H          2.61090       -0.79480       -0.12920
H          0.17090        2.01590        0.12950
H         -2.28290        1.62480        0.10620
H         -3.21920       -0.65090       -0.04410
H          0.76900       -2.18180       -0.14850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

