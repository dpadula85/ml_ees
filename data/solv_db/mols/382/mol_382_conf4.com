%nproc=16
%mem=2GB
%chk=mol_382_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06560        0.17550        0.06450
C          0.61200       -0.06210        0.00520
C         -0.28980        0.98570        0.08730
C         -1.64300        0.74900        0.03060
C         -2.14350       -0.52390       -0.10830
N         -1.27240       -1.53730       -0.18780
C          0.04530       -1.31320       -0.13310
H          2.60600       -0.62350        0.62280
H          2.22910        1.14040        0.62600
H          2.45760        0.22340       -0.95660
H          0.09960        2.00790        0.19860
H         -2.30030        1.60430        0.09960
H         -3.22650       -0.67700       -0.15010
H          0.76030       -2.14910       -0.19870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

