%nproc=16
%mem=2GB
%chk=mol_382_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06670        0.12320        0.07890
C          0.58790       -0.04490        0.01180
C         -0.28160        1.01840        0.12060
C         -1.63590        0.77050        0.04710
C         -2.16230       -0.49270       -0.13020
N         -1.29870       -1.50690       -0.23290
C          0.03400       -1.29710       -0.16500
H          2.55840       -0.01900       -0.89670
H          2.29030        1.16500        0.40600
H          2.53060       -0.61000        0.79380
H          0.14200        2.00430        0.25940
H         -2.33040        1.61380        0.13320
H         -3.24380       -0.60360       -0.17770
H          0.74290       -2.12090       -0.24830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_382_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

