%nproc=16
%mem=2GB
%chk=mol_095_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.38950        0.10810        1.08710
C         -3.41670        0.59060        0.03620
C         -2.09920       -0.08730        0.16550
C         -2.06710       -1.46660        0.13030
C         -0.88780       -2.17400        0.25470
C          0.30570       -1.45030        0.41260
C          0.33250       -0.08230        0.43870
C         -0.90420        0.55050        0.31130
C          1.55060        0.70360        0.49180
O          1.35400        1.93280        0.87950
C          2.88390        0.37320        0.13140
C          3.63700        1.38810       -0.48030
C          4.98640        1.30090       -0.70300
C          5.67760        0.15610       -0.30560
C          4.95190       -0.84430        0.30940
C          3.57100       -0.72760        0.52800
C         -4.06950        0.42160       -1.26880
O         -3.51950       -0.21610       -2.18480
O         -5.30570        0.98240       -1.47620
H         -5.05490        0.91800        1.36230
H         -4.99000       -0.67990        0.63420
H         -3.84910       -0.30660        1.95670
H         -3.32130        1.69990        0.18710
H         -3.02440       -1.99600        0.01890
H         -0.85850       -3.24880        0.28150
H          1.19140       -2.06390        0.46930
H         -0.92230        1.62720        0.32190
H          3.15880        2.31460       -0.80800
H          5.53680        2.10610       -1.17220
H          6.74530        0.08220       -0.47340
H          5.48410       -1.73880        0.61220
H          3.18630       -1.46570        1.24600
H         -5.87360        1.29220       -0.70060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_095_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_095_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_095_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

