%nproc=16
%mem=2GB
%chk=mol_076_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90220       -0.10930        0.23350
C          0.53590        0.47730        0.17860
C         -0.53430       -0.53300       -0.15100
C         -1.90090        0.07500       -0.20180
H          2.39780       -0.19340       -0.75400
H          1.93430       -1.07590        0.77610
H          2.53880        0.58920        0.84790
H          0.52890        1.23450       -0.65020
H          0.26240        1.01650        1.10000
H         -0.52180       -1.32500        0.60890
H         -0.32650       -1.00410       -1.13600
H         -2.36480        0.05130       -1.21250
H         -1.86340        1.16840        0.06820
H         -2.58850       -0.37140        0.57170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

