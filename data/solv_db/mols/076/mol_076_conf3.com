%nproc=16
%mem=2GB
%chk=mol_076_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38940        0.07790       -0.60640
C         -0.55920        0.76420        0.47260
C          0.55460       -0.19360        0.83490
C          1.38670       -0.47150       -0.39930
H         -2.10780       -0.57460       -0.06700
H         -0.75670       -0.56690       -1.24520
H         -1.94180        0.83290       -1.19530
H         -1.21660        0.95630        1.33870
H         -0.13200        1.71910        0.10580
H          1.22470        0.22900        1.61970
H          0.15640       -1.15610        1.17940
H          2.46280       -0.58600       -0.07870
H          1.06230       -1.41400       -0.87870
H          1.25610        0.38320       -1.08040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

