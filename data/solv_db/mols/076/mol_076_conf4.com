%nproc=16
%mem=2GB
%chk=mol_076_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88030        0.18990       -0.00560
C         -0.42600        0.51470       -0.18190
C          0.47110       -0.67920        0.05620
C          1.90070       -0.16470       -0.16570
H         -2.13880       -0.71560       -0.57430
H         -2.19150        0.07850        1.03490
H         -2.45380        1.06320       -0.43010
H         -0.24630        0.90500       -1.19210
H         -0.15720        1.28860        0.56830
H          0.36510       -1.04730        1.10460
H          0.22080       -1.44980       -0.69270
H          1.96240        0.77430        0.42840
H          2.65360       -0.90660        0.08900
H          1.92020        0.14900       -1.24270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

