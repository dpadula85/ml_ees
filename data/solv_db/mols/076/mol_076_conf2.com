%nproc=16
%mem=2GB
%chk=mol_076_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.26420        0.74490       -0.23040
C          0.79410       -0.59240        0.30430
C         -0.59220       -0.84310       -0.26700
C         -1.45420        0.32230        0.23160
H          0.77610        0.91440       -1.21230
H          1.04380        1.56980        0.46490
H          2.35430        0.64030       -0.44210
H          1.46120       -1.42040        0.06580
H          0.76500       -0.49340        1.40780
H         -0.56930       -0.82330       -1.37440
H         -1.00730       -1.81780        0.02950
H         -1.10960        0.55780        1.25650
H         -2.52100        0.06190        0.19990
H         -1.20510        1.17910       -0.43410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

