%nproc=16
%mem=2GB
%chk=mol_076_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.54230        0.40420       -0.08110
C          0.64420       -0.76230        0.19680
C         -0.72640       -0.53860       -0.34160
C         -1.42890        0.64620        0.21550
H          2.55710        0.18380        0.35160
H          1.58730        0.53940       -1.18350
H          1.16610        1.33470        0.36960
H          1.06630       -1.65890       -0.32580
H          0.66850       -1.01360        1.27560
H         -1.34140       -1.44660       -0.07690
H         -0.67740       -0.51620       -1.44390
H         -0.85530        1.57410        0.23750
H         -2.34260        0.82280       -0.42650
H         -1.85990        0.43090        1.23260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_076_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

