%nproc=16
%mem=2GB
%chk=mol_450_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.74960        0.98690       -0.69570
C         -0.84300        0.21100        0.21920
C         -1.55070       -1.09440        0.54410
O          0.39710       -0.10570       -0.29470
C          1.62230        0.19670        0.24530
O          1.58570        0.83140        1.32550
C          2.92360       -0.17030       -0.35310
H         -2.16290        0.36970       -1.52700
H         -2.64090        1.27930       -0.06490
H         -1.32320        1.93580       -1.04320
H         -0.80750        0.77860        1.18670
H         -1.29590       -1.45800        1.54970
H         -2.65950       -0.97860        0.51160
H         -1.30920       -1.89160       -0.19290
H          3.22950        0.50030       -1.17910
H          3.69690       -0.14580        0.45080
H          2.88720       -1.24540       -0.68240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

