%nproc=16
%mem=2GB
%chk=mol_450_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.80720        1.00750       -0.18070
C         -0.84740       -0.10320        0.23880
C         -1.47550       -1.45850       -0.00030
O          0.38770        0.02090       -0.43880
C          1.52440        0.24020        0.31890
O          1.46470        0.32690        1.57160
C          2.87960        0.38430       -0.32230
H         -2.59560        1.12630        0.59870
H         -1.27760        1.94730       -0.33820
H         -2.31530        0.74860       -1.14040
H         -0.72620       -0.00210        1.34310
H         -2.10480       -1.72380        0.87370
H         -0.66610       -2.23270       -0.04100
H         -2.02700       -1.51750       -0.94690
H          2.74860        0.95220       -1.28320
H          3.26890       -0.61810       -0.62910
H          3.56890        0.90170        0.37610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

