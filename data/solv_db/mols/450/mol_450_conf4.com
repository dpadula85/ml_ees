%nproc=16
%mem=2GB
%chk=mol_450_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.47270       -1.40830        0.27900
C         -0.84660       -0.18580       -0.35850
C         -1.76230        0.99430       -0.09880
O          0.36990        0.08200        0.30670
C          1.56860        0.16800       -0.38070
O          1.54510        0.00000       -1.62190
C          2.78960        0.45290        0.40870
H         -1.14150       -2.29240       -0.33890
H         -2.57790       -1.35900        0.28090
H         -1.06550       -1.52240        1.31210
H         -0.73540       -0.31330       -1.44440
H         -1.96220        1.11180        0.98800
H         -1.25680        1.88650       -0.53290
H         -2.69670        0.79160       -0.66690
H          2.71640       -0.13980        1.34330
H          3.71790        0.22250       -0.14870
H          2.81010        1.51130        0.67300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

