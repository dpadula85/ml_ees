%nproc=16
%mem=2GB
%chk=mol_450_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.01920       -0.56780       -0.69630
C         -0.88780        0.14540       -0.04730
C         -1.10450        1.63220        0.10160
O          0.34670       -0.00300       -0.75880
C          1.46060       -0.57890       -0.16250
O          1.31070       -0.95870        1.03860
C          2.76080       -0.75300       -0.86200
H         -2.34360       -1.47040       -0.10050
H         -1.78240       -0.96650       -1.71220
H         -2.93790        0.07110       -0.73940
H         -0.78970       -0.25860        0.99500
H         -1.09570        2.16600       -0.86920
H         -0.21310        2.01420        0.67830
H         -1.98770        1.88880        0.70570
H          2.61650       -0.55250       -1.93330
H          3.52110       -0.02380       -0.47600
H          3.14520       -1.78450       -0.73870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

