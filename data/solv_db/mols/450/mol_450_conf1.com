%nproc=16
%mem=2GB
%chk=mol_450_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.50220       -1.31560       -0.05130
C         -0.90700        0.07680       -0.09940
C         -1.68940        0.93260       -1.08880
O          0.43810       -0.05390       -0.45760
C          1.50790        0.32230        0.31530
O          1.27540        0.82730        1.43980
C          2.87500        0.10890       -0.22640
H         -2.51340       -1.32090       -0.51670
H         -1.51400       -1.73490        0.97880
H         -0.89980       -2.03770       -0.66530
H         -1.05630        0.52650        0.89900
H         -2.40440        0.29720       -1.65490
H         -2.29420        1.70580       -0.58130
H         -1.02830        1.45350       -1.80010
H          2.92480       -0.77720       -0.89290
H          3.62450        0.00410        0.58820
H          3.16330        0.98530       -0.83730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_450_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

