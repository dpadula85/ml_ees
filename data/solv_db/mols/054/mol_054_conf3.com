%nproc=16
%mem=2GB
%chk=mol_054_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.19220        1.35180       -0.25650
C         -0.76680       -0.06390        0.02900
C         -0.75980       -0.90700       -1.24790
C         -1.69890       -0.76850        0.99320
C          0.64000       -0.10190        0.54110
O          0.94850       -0.56020        1.66050
O          1.62990        0.39690       -0.27470
C          3.00100        0.42720        0.07390
H         -1.90150        1.40820       -1.11320
H         -1.75800        1.71450        0.64220
H         -0.27960        1.95460       -0.44210
H         -0.10290       -1.77740       -1.03930
H         -0.40870       -0.31870       -2.11680
H         -1.76200       -1.29680       -1.49000
H         -2.72230       -0.68010        0.61730
H         -1.36410       -1.82450        1.03830
H         -1.66190       -0.26440        1.97490
H          3.40940        1.44160       -0.10770
H          3.15720        0.12420        1.12070
H          3.59260       -0.25590       -0.60300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

