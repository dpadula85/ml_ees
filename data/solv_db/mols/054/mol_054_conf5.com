%nproc=16
%mem=2GB
%chk=mol_054_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.25680       -1.14790        0.93610
C         -0.72220       -0.02160        0.08690
C         -1.37020       -0.08260       -1.26050
C         -1.06150        1.27020        0.81080
C          0.73610       -0.06730       -0.06860
O          1.22210       -0.17450       -1.21900
O          1.60490        0.00360        1.01260
C          2.99850       -0.04790        0.77290
H         -2.29740       -0.85530        1.22530
H         -0.69320       -1.25160        1.88630
H         -1.33410       -2.09480        0.40380
H         -2.47560       -0.00520       -1.17160
H         -1.08930       -1.05350       -1.72270
H         -0.96500        0.76710       -1.86370
H         -1.77400        1.10730        1.64490
H         -1.49920        1.95020        0.04600
H         -0.15700        1.72470        1.26580
H          3.38630        0.97840        0.98490
H          3.21970       -0.25970       -0.30870
H          3.52810       -0.73970        1.44610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

