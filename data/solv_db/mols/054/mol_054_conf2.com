%nproc=16
%mem=2GB
%chk=mol_054_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17540        0.63880       -1.25670
C         -0.69720        0.09670        0.06970
C         -1.67460       -0.97310        0.49530
C         -0.73610        1.24960        1.08150
C          0.64670       -0.47090        0.00930
O          0.81160       -1.68050        0.30460
O          1.77950        0.26440       -0.36380
C          3.02960       -0.39780       -0.38420
H         -1.33510       -0.21660       -1.94510
H         -2.18390        1.08760       -1.06230
H         -0.52610        1.41830       -1.67140
H         -1.17440       -1.76300        1.10030
H         -2.46190       -0.47870        1.08870
H         -2.09560       -1.49690       -0.39930
H          0.12340        1.89470        0.91690
H         -0.74450        0.78050        2.07660
H         -1.70470        1.78560        0.96300
H          2.88900       -1.47680       -0.59560
H          3.75960        0.07510       -1.06560
H          3.47000       -0.33700        0.63800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

