%nproc=16
%mem=2GB
%chk=mol_054_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06300        1.43380       -0.12910
C         -0.78710       -0.02460        0.09590
C         -1.65250       -0.65320        1.13990
C         -0.91200       -0.72970       -1.25970
C          0.65920       -0.18600        0.48410
O          1.00880       -0.68730        1.57590
O          1.58880        0.25080       -0.43310
C          2.96660        0.12710       -0.12810
H         -1.30790        1.67770       -1.18550
H         -0.21800        2.08110        0.20460
H         -1.94430        1.78320        0.48000
H         -2.08230       -1.59880        0.70880
H         -1.07850       -0.98770        2.04680
H         -2.45800        0.04130        1.41770
H          0.06210       -0.62480       -1.80970
H         -1.69680       -0.21780       -1.85820
H         -1.10750       -1.80920       -1.13050
H          3.19710       -0.88940        0.30550
H          3.49880        0.14770       -1.11180
H          3.32650        0.86580        0.58660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

