%nproc=16
%mem=2GB
%chk=mol_054_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06880       -1.46210       -0.36620
C         -0.71620       -0.01400       -0.12790
C         -1.47620        0.86790       -1.08490
C         -1.19050        0.32090        1.28670
C          0.71420        0.27040       -0.23920
O          1.15200        1.09210       -1.08980
O          1.68770       -0.33120        0.56900
C          3.06380       -0.01020        0.40920
H         -1.38610       -1.59930       -1.41870
H         -1.93860       -1.77330        0.27150
H         -0.25670       -2.15730       -0.15180
H         -1.58500        1.86740       -0.61700
H         -0.84110        0.99640       -1.98420
H         -2.45180        0.39020       -1.31330
H         -2.29060        0.12560        1.31090
H         -1.03500        1.38900        1.49090
H         -0.63660       -0.26430        2.04300
H          3.46900       -0.66720       -0.37880
H          3.59130       -0.07940        1.36590
H          3.19510        1.03820        0.02510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_054_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

