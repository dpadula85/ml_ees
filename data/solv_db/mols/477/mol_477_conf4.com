%nproc=16
%mem=2GB
%chk=mol_477_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.82440       -0.67910        0.58580
C          1.52920       -0.02400        0.21270
C          1.59130        1.25310       -0.10270
C          0.35800        1.92740       -0.47100
O          0.26860        3.19850       -0.40130
C         -0.77820        1.08950       -0.92540
C         -0.92890       -0.15850       -0.13300
C          0.32470       -0.84970        0.23190
C         -1.78710       -1.12590       -0.94980
C         -1.74190        0.16770        1.11350
H          3.69510       -0.07340        0.34390
H          2.74430       -0.94670        1.67140
H          2.89560       -1.65130        0.05240
H          2.50650        1.82990       -0.10270
H         -1.71660        1.68190       -0.87200
H         -0.60010        0.85280       -2.00510
H          0.51630       -1.76150       -0.40600
H          0.22460       -1.27360        1.27280
H         -1.74060       -2.11580       -0.44590
H         -2.84190       -0.81570       -0.98490
H         -1.38440       -1.21210       -1.97820
H         -2.81120        0.29610        0.79840
H         -1.72120       -0.71190        1.81870
H         -1.42660        1.10230        1.59240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

