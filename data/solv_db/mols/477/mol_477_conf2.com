%nproc=16
%mem=2GB
%chk=mol_477_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.61870       -1.13320       -0.36060
C          1.49870       -0.17600       -0.34610
C          1.68770        1.07820        0.09700
C          0.53490        1.97400        0.08390
O          0.72070        3.21000       -0.11030
C         -0.84770        1.46070        0.29410
C         -0.94780       -0.01140        0.08880
C          0.15110       -0.53050       -0.80320
C         -2.26720       -0.32560       -0.62880
C         -1.00310       -0.79110        1.39770
H          3.01300       -1.35590       -1.35930
H          3.46830       -0.75320        0.25750
H          2.25880       -2.05940        0.16900
H          2.63560        1.42910        0.45000
H         -1.52500        1.99970       -0.40530
H         -1.14500        1.78570        1.31780
H          0.00070       -0.02750       -1.80150
H          0.06740       -1.59820       -0.98440
H         -2.45330        0.49530       -1.34860
H         -2.15240       -1.30880       -1.13470
H         -3.10010       -0.43180        0.08460
H         -0.09490       -0.67140        2.00060
H         -1.20390       -1.84590        1.10960
H         -1.91510       -0.41290        1.93210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

