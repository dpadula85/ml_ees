%nproc=16
%mem=2GB
%chk=mol_477_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.90490        0.08790       -0.11900
C          1.44950        0.40850       -0.01190
C          1.11250        1.47700        0.69160
C         -0.31750        1.77210        0.78320
O         -0.66810        2.67900        1.56860
C         -1.33190        1.04940       -0.00360
C         -0.90440       -0.39330       -0.15010
C          0.49920       -0.46910       -0.68420
C         -1.07850       -1.06260        1.17710
C         -1.76970       -1.09630       -1.20720
H          3.50750        0.98100       -0.38440
H          3.09930       -0.67200       -0.90330
H          3.24820       -0.29600        0.87620
H          1.87280        2.08140        1.16250
H         -2.30000        1.11900        0.52390
H         -1.35580        1.51010       -1.01880
H          0.87470       -1.50010       -0.68270
H          0.43690       -0.17260       -1.77190
H         -0.88570       -0.34840        2.00310
H         -0.54130       -2.00800        1.29190
H         -2.17400       -1.31820        1.26760
H         -1.62190       -0.53970       -2.15190
H         -1.25400       -2.08800       -1.36900
H         -2.80280       -1.20100       -0.88780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

