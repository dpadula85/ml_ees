%nproc=16
%mem=2GB
%chk=mol_477_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.71170       -0.84350        0.43410
C          1.52180       -0.17050       -0.14070
C          1.60220        1.07440       -0.59850
C          0.37570        1.65060       -1.13020
O          0.42890        2.31390       -2.19780
C         -0.85440        1.41600       -0.35830
C         -0.97100       -0.01770        0.05830
C          0.20940       -0.83390       -0.32290
C         -1.14640       -0.16000        1.57320
C         -2.20220       -0.62210       -0.59260
H          3.39610       -0.13700        0.92660
H          3.20270       -1.48070       -0.33390
H          2.31560       -1.50900        1.25590
H          2.54060        1.62890       -0.62030
H         -1.69910        1.74860       -0.98900
H         -0.81880        2.01580        0.60020
H          0.26560       -1.80120        0.23650
H          0.16970       -1.13630       -1.40810
H         -2.24030       -0.18300        1.82990
H         -0.63280        0.65560        2.11750
H         -0.77080       -1.17220        1.87560
H         -3.11000       -0.33240       -0.04380
H         -2.06760       -1.73510       -0.50870
H         -2.22670       -0.36900       -1.66310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

