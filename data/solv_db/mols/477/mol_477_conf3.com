%nproc=16
%mem=2GB
%chk=mol_477_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.83990       -0.80670        0.02470
C          1.54430       -0.07570        0.20470
C          1.47940        0.85440        1.13010
C          0.19860        1.56000        1.29110
O          0.17990        2.74350        1.75330
C         -1.02090        0.86120        0.90710
C         -0.91070       -0.05470       -0.24840
C          0.44080       -0.45360       -0.67060
C         -1.79040       -1.27890        0.04630
C         -1.58330        0.62990       -1.44750
H          3.11660       -0.83480       -1.05680
H          3.68330       -0.23950        0.51340
H          2.83050       -1.79430        0.50050
H          2.32790        1.11850        1.77230
H         -1.81470        1.64810        0.73540
H         -1.39360        0.31480        1.82330
H          0.70840       -0.09960       -1.71050
H          0.46730       -1.57970       -0.76810
H         -1.93530       -1.36900        1.15790
H         -1.33390       -2.20550       -0.36390
H         -2.79990       -1.16700       -0.37810
H         -0.86340        1.28470       -1.98330
H         -2.48420        1.15110       -1.10920
H         -1.88650       -0.20730       -2.12360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_477_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

