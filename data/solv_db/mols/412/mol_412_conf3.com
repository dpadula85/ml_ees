%nproc=16
%mem=2GB
%chk=mol_412_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09570       -0.93260        0.09980
C          1.19070       -1.96160        0.05260
C         -0.15100       -1.65740       -0.01760
C         -0.65620       -0.36640       -0.04390
C          0.25330        0.66850        0.01170
C          1.61790        0.35290        0.07890
C         -0.13700        2.06380        0.17060
F         -0.95730        2.59830       -0.77530
F         -0.51410        2.39830        1.48070
F          1.04170        2.82890        0.02160
C         -2.07360       -0.08870       -0.26630
F         -2.70260        0.70030        0.63150
F         -2.73810       -1.33330       -0.11940
F         -2.37710        0.21070       -1.60560
H          3.14120       -1.16010        0.15140
H          1.48780       -2.99270        0.06810
H         -0.90550       -2.46310       -0.05790
H          2.38420        1.13430        0.11900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

