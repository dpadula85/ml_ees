%nproc=16
%mem=2GB
%chk=mol_412_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.13950       -0.75050       -0.20430
C          2.21720        0.56460        0.19630
C          1.03900        1.24980        0.37970
C         -0.22800        0.67740        0.17970
C         -0.30580       -0.63800       -0.20420
C          0.90610       -1.32320       -0.39530
C         -1.49220       -1.42590       -0.17290
F         -1.92540       -1.54480        1.16160
F         -1.23160       -2.73260       -0.55250
F         -2.58540       -1.05760       -0.86340
C         -1.42510        1.49540        0.11360
F         -1.03060        2.79220        0.54270
F         -2.42850        1.23210        0.98980
F         -1.86420        1.76630       -1.17080
H          3.06180       -1.29350       -0.35660
H          3.18340        1.05910        0.36640
H          1.06280        2.29740        0.70250
H          0.90700       -2.36830       -0.71230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

