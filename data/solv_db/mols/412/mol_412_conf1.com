%nproc=16
%mem=2GB
%chk=mol_412_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.13980       -0.84280       -0.06010
C          2.21550        0.53220       -0.01840
C          1.06900        1.28300        0.02780
C         -0.21930        0.71330        0.03520
C         -0.28720       -0.65860       -0.01830
C          0.90210       -1.40790       -0.05950
C         -1.56780       -1.33350       -0.22610
F         -1.31360       -2.72770       -0.19530
F         -2.04420       -1.19740       -1.54580
F         -2.52380       -1.15070        0.71420
C         -1.38890        1.54910        0.28250
F         -2.41740        1.47370       -0.59070
F         -0.96090        2.89670        0.17570
F         -1.80240        1.51240        1.62680
H          3.01250       -1.47860       -0.09290
H          3.20400        0.96770       -0.02420
H          1.14640        2.36690        0.06050
H          0.83630       -2.49790       -0.09150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

