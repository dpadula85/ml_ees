%nproc=16
%mem=2GB
%chk=mol_412_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96120       -1.19210        0.16960
C          2.27860        0.14040        0.03760
C          1.27730        1.05530       -0.08340
C         -0.08300        0.73660       -0.08360
C         -0.41700       -0.61440        0.05650
C          0.62380       -1.51300        0.17340
C         -1.79960       -1.05070        0.18830
F         -1.77210       -2.46330        0.29290
F         -2.64040       -0.85840       -0.85140
F         -2.37150       -0.73970        1.43180
C         -1.07790        1.76800       -0.33280
F         -0.37820        2.99880       -0.40370
F         -1.99960        2.01100        0.63110
F         -1.61780        1.73670       -1.61950
H          2.73830       -1.95530        0.26640
H          3.31460        0.42200        0.03390
H          1.58480        2.10230       -0.18530
H          0.37850       -2.58420        0.27830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

