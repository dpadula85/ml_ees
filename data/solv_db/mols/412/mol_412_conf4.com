%nproc=16
%mem=2GB
%chk=mol_412_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.99120       -1.05300        0.35010
C          2.28620        0.26230        0.08870
C          1.23750        1.10640       -0.14880
C         -0.11090        0.70340       -0.14000
C         -0.40200       -0.61440        0.10340
C          0.68340       -1.47840        0.35700
C         -1.72720       -1.16300       -0.17510
F         -1.67930       -2.53700        0.17650
F         -1.98120       -1.24370       -1.55510
F         -2.77660       -0.69920        0.53050
C         -1.16000        1.72160       -0.10590
F         -1.77740        1.87590        1.13010
F         -0.55660        2.97750       -0.31950
F         -2.08530        1.63840       -1.12310
H          2.82480       -1.72930        0.54760
H          3.32100        0.59500        0.07670
H          1.45470        2.15320       -0.35730
H          0.45770       -2.51580        0.56420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_412_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

