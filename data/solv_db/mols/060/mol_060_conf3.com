%nproc=16
%mem=2GB
%chk=mol_060_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77560       -0.06180       -0.08420
C          0.74560        0.08000       -0.08710
F          1.08760        0.40980       -1.39300
Cl         1.22050        1.41830        0.99020
Cl         1.53680       -1.39470        0.46750
F         -1.11400       -1.06120       -0.97590
F         -1.32280        1.15170       -0.43120
Cl        -1.37810       -0.54200        1.51380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

