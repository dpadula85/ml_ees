%nproc=16
%mem=2GB
%chk=mol_060_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.75820       -0.20700       -0.02110
C          0.71030        0.08720        0.11190
F          1.09190        0.29880        1.42690
Cl         1.71640       -1.22830       -0.52350
Cl         1.05640        1.53890       -0.85790
F         -1.01210       -0.39620       -1.35520
F         -1.14760       -1.32190        0.68480
Cl        -1.65700        1.22850        0.53410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

