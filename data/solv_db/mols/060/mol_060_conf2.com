%nproc=16
%mem=2GB
%chk=mol_060_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.77550       -0.00360       -0.18170
C         -0.73480       -0.15090       -0.03270
F         -1.27110       -0.75590       -1.14850
Cl        -1.13400       -1.07960        1.42550
Cl        -1.35730        1.50490        0.19360
F          0.99540        0.77220       -1.29670
F          1.41800       -1.20120       -0.22800
Cl         1.30830        0.91400        1.26860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

