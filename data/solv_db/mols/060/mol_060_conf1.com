%nproc=16
%mem=2GB
%chk=mol_060_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77630       -0.03680       -0.01650
C          0.74160       -0.12450        0.01180
F          1.06020       -1.45410       -0.04640
Cl         1.35470        0.78590       -1.38670
Cl         1.31970        0.65450        1.50460
F         -1.18400       -0.62070       -1.20670
F         -1.25480       -0.84120        0.99840
Cl        -1.26100        1.63680        0.14150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

