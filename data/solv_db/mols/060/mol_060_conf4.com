%nproc=16
%mem=2GB
%chk=mol_060_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76910        0.03950       -0.09780
C          0.72540       -0.16260       -0.02760
F          1.32850       -0.22390       -1.24970
Cl         1.16290       -1.56680        0.94220
Cl         1.39200        1.28450        0.80520
F         -1.21410        0.07750        1.22720
F         -1.41030       -1.00050       -0.73010
Cl        -1.21520        1.55230       -0.86940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_060_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

