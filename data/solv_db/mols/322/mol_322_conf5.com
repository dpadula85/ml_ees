%nproc=16
%mem=2GB
%chk=mol_322_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36280       -0.77330        0.37000
C          2.42950        0.58540        0.14060
C          1.25960        1.30810       -0.04290
C          0.02110        0.70820       -0.00330
C         -0.02440       -0.64560        0.23110
C          1.13820       -1.40390        0.41790
C         -1.46250       -0.99440        0.30650
C         -2.01640       -0.06940       -0.74900
C         -1.37020        1.23760       -0.25170
H          3.26820       -1.36780        0.51600
H          3.39800        1.07860        0.10540
H          1.29360        2.38930       -0.22640
H          1.07700       -2.48210        0.59950
H         -1.81320       -0.70450        1.33090
H         -1.65970       -2.05740        0.10630
H         -1.57210       -0.35720       -1.71360
H         -3.10870        0.00080       -0.74350
H         -1.36300        1.98270       -1.05990
H         -1.85790        1.56510        0.66610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

