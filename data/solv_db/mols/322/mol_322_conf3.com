%nproc=16
%mem=2GB
%chk=mol_322_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.38830        0.86730       -0.15690
C          2.45240       -0.48580        0.08570
C          1.31860       -1.26400        0.17920
C          0.07850       -0.68400        0.02740
C         -0.01270        0.66480       -0.22110
C          1.15320        1.44570       -0.31090
C         -1.44620        0.96320       -0.42440
C         -2.13990       -0.05180        0.45410
C         -1.28150       -1.30040        0.18830
H          3.30370        1.47040       -0.22560
H          3.42630       -0.92220        0.20150
H          1.38370       -2.32960        0.37080
H          1.03430        2.51130       -0.50490
H         -1.71710        1.99780       -0.14800
H         -1.68060        0.72600       -1.48280
H         -2.05620        0.28850        1.48980
H         -3.18850       -0.24040        0.12310
H         -1.35880       -1.91430        1.08720
H         -1.65760       -1.74250       -0.73250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

