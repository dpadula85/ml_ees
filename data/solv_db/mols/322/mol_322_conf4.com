%nproc=16
%mem=2GB
%chk=mol_322_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.47640        0.41460       -0.06160
C          2.34040       -0.95520        0.03450
C          1.08340       -1.51190        0.12600
C         -0.03310       -0.65570        0.11830
C          0.09730        0.68460        0.02520
C          1.37620        1.24640       -0.06800
C         -1.21370        1.33420        0.05780
C         -2.17320        0.22790       -0.30320
C         -1.45250       -1.03540        0.18670
H          3.48110        0.80920       -0.13180
H          3.21650       -1.58640        0.03770
H          0.95290       -2.58240        0.20240
H          1.46240        2.31910       -0.14260
H         -1.29250        2.20130       -0.62770
H         -1.43960        1.69030        1.09230
H         -2.27840        0.22510       -1.40810
H         -3.14990        0.31780        0.22380
H         -1.78840       -1.31400        1.20480
H         -1.66540       -1.82950       -0.56650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

