%nproc=16
%mem=2GB
%chk=mol_322_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.36660       -0.85880        0.05310
C          2.43390        0.50200       -0.07990
C          1.30000        1.28320       -0.18980
C          0.07320        0.66800       -0.16400
C         -0.01980       -0.70200       -0.03070
C          1.11850       -1.47380        0.07840
C         -1.44410       -1.08730       -0.03120
C         -2.09290        0.19250        0.50670
C         -1.29400        1.22800       -0.26060
H          3.27700       -1.41880        0.13460
H          3.38740        0.98800       -0.10070
H          1.41280        2.37050       -0.29440
H          1.04370       -2.54900        0.18290
H         -1.67540       -1.95410        0.58350
H         -1.77760       -1.16610       -1.08370
H         -1.91750        0.27920        1.60290
H         -3.14490        0.26710        0.23390
H         -1.38470        2.23520        0.17410
H         -1.66210        1.19610       -1.31500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

