%nproc=16
%mem=2GB
%chk=mol_322_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.46660        0.55840       -0.09570
C          2.38900       -0.79520        0.15070
C          1.16690       -1.41890        0.21710
C         -0.02220       -0.68820        0.03520
C          0.06940        0.64770       -0.21200
C          1.31890        1.28810       -0.27810
C         -1.30710        1.17170       -0.43510
C         -2.16120        0.19170        0.30190
C         -1.43940       -1.14950        0.13690
H          3.42800        1.05430       -0.14720
H          3.31950       -1.36270        0.29230
H          1.14100       -2.47370        0.41050
H          1.32440        2.35870       -0.47530
H         -1.39340        2.21040       -0.02820
H         -1.56560        1.19180       -1.51770
H         -3.20520        0.12680       -0.08130
H         -2.20990        0.49380        1.36540
H         -1.77500       -1.70270       -0.73800
H         -1.54480       -1.70230        1.09870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_322_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

