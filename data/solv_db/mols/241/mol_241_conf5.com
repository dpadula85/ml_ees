%nproc=16
%mem=2GB
%chk=mol_241_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.25820       -0.13490        0.13990
C         -0.00400        0.59550       -0.08200
O         -0.02310        1.79070       -0.24010
C         -1.27210       -0.17240       -0.10860
H          1.86890        0.47330        0.83920
H          1.80260       -0.35580       -0.81580
H          1.10230       -1.12150        0.63750
H         -2.16010        0.49000       -0.25400
H         -1.21520       -0.84990       -0.97410
H         -1.35770       -0.71500        0.85790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

