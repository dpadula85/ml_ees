%nproc=16
%mem=2GB
%chk=mol_241_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.27560       -0.10150        0.26100
C         -0.03230       -0.43110       -0.39700
O         -0.09470       -1.30640       -1.22020
C         -1.24870        0.31840       -0.03630
H          1.70620       -1.04780        0.61120
H          1.88630        0.39670       -0.52100
H          1.10730        0.64110        1.07120
H         -0.95170        1.24560        0.51160
H         -1.78810        0.56310       -0.96200
H         -1.85980       -0.27820        0.68150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

