%nproc=16
%mem=2GB
%chk=mol_241_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.26070       -0.32750       -0.27360
C          0.07970        0.53040       -0.16840
O          0.21580        1.73740       -0.13950
C         -1.30740        0.00770       -0.09300
H          1.04980       -1.30010        0.22900
H          2.09230        0.15820        0.27300
H          1.58600       -0.49220       -1.32490
H         -1.71380       -0.04100       -1.12230
H         -1.33570       -0.97030        0.42490
H         -1.92720        0.69740        0.51390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

