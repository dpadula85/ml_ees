%nproc=16
%mem=2GB
%chk=mol_241_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.24750        0.26620        0.13930
C          0.05920       -0.51270       -0.29590
O          0.20180       -1.55130       -0.87940
C         -1.28300        0.00390       -0.00310
H          1.39200        0.05200        1.21590
H          2.12100       -0.05040       -0.43790
H          1.13240        1.35030        0.02050
H         -2.02830       -0.79760        0.20350
H         -1.22170        0.64850        0.91810
H         -1.62090        0.59100       -0.88110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

