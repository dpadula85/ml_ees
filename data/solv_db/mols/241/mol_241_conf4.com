%nproc=16
%mem=2GB
%chk=mol_241_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.30720        0.04890        0.16920
C          0.00440       -0.08900       -0.47850
O          0.02410       -0.26700       -1.69840
C          1.31040       -0.03390        0.20110
H         -2.01170       -0.68320       -0.30170
H         -1.31210       -0.07390        1.24970
H         -1.73490        1.05550       -0.09880
H          1.94040       -0.84470       -0.22870
H          1.82410        0.91930       -0.10020
H          1.26240       -0.03200        1.28620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_241_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

