%nproc=16
%mem=2GB
%chk=mol_161_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49930       -0.79460        0.92710
C         -1.25190       -0.38270        0.44960
C         -1.18290        0.60470       -0.50930
C         -2.32540        1.20810       -1.01990
C         -3.55820        0.80580       -0.55250
C         -3.60620       -0.19020        0.41330
Cl        -5.18950       -0.69210        0.99900
Cl        -4.99560        1.55150       -1.18140
O          0.08740        0.98050       -0.95340
C          1.24210        0.39190       -0.45790
C          2.48290        0.77880       -0.91090
C          3.65050        0.18780       -0.41390
C          3.55680       -0.79720        0.54430
C          2.32650       -1.19980        1.01180
C          1.17550       -0.59590        0.50120
O         -0.08860       -0.96620        0.93960
Cl         4.98320       -1.56240        1.19290
Cl         5.22910        0.68030       -0.99040
H         -2.54580       -1.56550        1.67530
H         -2.26260        1.98090       -1.77090
H          2.54800        1.55300       -1.66340
H          2.22410       -1.97670        1.76940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_161_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_161_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_161_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

