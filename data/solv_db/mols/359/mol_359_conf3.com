%nproc=16
%mem=2GB
%chk=mol_359_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.31540        0.15490        0.30800
C         -0.01960       -0.15770       -0.41450
C          0.93320        0.97180       -0.06090
C          0.52550       -1.48710       -0.04860
O          1.70250       -1.72240        0.12810
H         -1.90160       -0.74050        0.55200
H         -1.84770        0.89990       -0.30010
H         -1.08910        0.66640        1.28410
H         -0.18360       -0.13850       -1.51410
H          1.95950        0.78010       -0.39590
H          0.55730        1.86520       -0.62170
H          0.87010        1.20550        1.01770
H         -0.19100       -2.29770        0.06590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

