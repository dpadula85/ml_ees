%nproc=16
%mem=2GB
%chk=mol_359_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.92070       -0.82070        0.55960
C          0.05400       -0.17480       -0.41200
C         -0.10600        1.31920       -0.27350
C          1.42180       -0.64520       -0.13090
O          2.09060       -0.15220        0.74670
H         -0.75680       -0.48500        1.60430
H         -0.93260       -1.92170        0.47400
H         -1.93190       -0.45860        0.27530
H         -0.29700       -0.44120       -1.42980
H          0.83640        1.83430       -0.00250
H         -0.46900        1.79760       -1.20900
H         -0.84290        1.59800        0.51140
H          1.85410       -1.44980       -0.71380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

