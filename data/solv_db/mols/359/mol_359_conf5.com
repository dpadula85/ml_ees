%nproc=16
%mem=2GB
%chk=mol_359_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.99800       -0.95470       -0.24930
C          0.05070        0.15550       -0.41630
C          1.34240       -0.20520        0.27550
C         -0.50540        1.35170        0.25740
O         -0.97020        2.22930       -0.41610
H         -1.84370       -0.64630       -0.91110
H         -0.57410       -1.92530       -0.51630
H         -1.37210       -0.88220        0.78860
H          0.25930        0.30850       -1.48580
H          1.45860       -1.30020        0.42590
H          2.20040        0.16080       -0.34260
H          1.43120        0.30380        1.25970
H         -0.47900        1.40430        1.33040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

