%nproc=16
%mem=2GB
%chk=mol_359_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.30580       -0.19330        0.23960
C         -0.02980        0.16620       -0.41750
C         -0.96050       -0.99030       -0.15090
C         -0.51260        1.43190        0.16390
O          0.27590        2.19810        0.66290
H          1.09700       -0.72570        1.19260
H          1.83220       -0.84850       -0.50080
H          1.83440        0.77190        0.39560
H          0.16080        0.28780       -1.50210
H         -1.90820       -0.77640       -0.65270
H         -0.47070       -1.90070       -0.52810
H         -1.06990       -1.10340        0.94720
H         -1.55450        1.68240        0.15030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

