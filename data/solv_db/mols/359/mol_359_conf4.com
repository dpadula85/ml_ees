%nproc=16
%mem=2GB
%chk=mol_359_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.22150       -0.21920        0.60820
C          0.06170       -0.02870       -0.32860
C         -0.73500        1.21700       -0.03730
C         -0.78970       -1.24750       -0.36330
O         -0.47040       -2.14920       -1.11810
H          1.00270        0.03380        1.65360
H          2.10200        0.37630        0.26280
H          1.53090       -1.29910        0.51360
H          0.48550        0.11830       -1.35060
H         -1.43410        1.34940       -0.90930
H         -0.02210        2.07020       -0.08540
H         -1.28110        1.17360        0.92350
H         -1.67180       -1.39500        0.23100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_359_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

