%nproc=16
%mem=2GB
%chk=mol_064_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.68210        0.39390       -0.18160
C          2.18610       -0.84350       -0.51350
C          0.88400       -1.15390       -0.22020
C          0.02700       -0.27340        0.40270
C          0.51450        0.97900        0.74330
C          1.83140        1.29680        0.44820
C         -1.37900       -0.61500        0.71660
C         -2.29650       -0.23560       -0.43910
O         -2.19460        1.15630       -0.66250
H          3.70750        0.66690       -0.40130
H          2.84060       -1.55110       -1.00290
H          0.51170       -2.15250       -0.49710
H         -0.11270        1.71100        1.23360
H          2.26560        2.25570        0.68930
H         -1.47890       -1.70410        0.85890
H         -1.75290       -0.10050        1.62000
H         -3.33450       -0.42690       -0.17160
H         -1.95040       -0.77270       -1.34070
H         -2.95100        1.36960       -1.28200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

