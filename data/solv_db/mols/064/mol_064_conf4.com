%nproc=16
%mem=2GB
%chk=mol_064_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73420        0.70860        0.09130
C         -1.85150        1.47060       -0.64830
C         -0.54960        1.05210       -0.84010
C         -0.07780       -0.13140       -0.30820
C         -0.96950       -0.88350        0.42870
C         -2.27860       -0.46510        0.62300
C          1.28650       -0.63050       -0.47420
C          2.22760       -0.16170        0.61060
O          3.49050       -0.68310        0.36950
H         -3.75250        1.06740        0.22310
H         -2.15780        2.41170       -1.09450
H          0.13660        1.65080       -1.41860
H         -0.64740       -1.82430        0.86940
H         -2.98200       -1.06790        1.21020
H          1.27260       -1.73980       -0.43640
H          1.69770       -0.38090       -1.49500
H          1.85840       -0.61300        1.56240
H          2.31990        0.93330        0.66330
H          3.71120       -0.71340       -0.59970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

