%nproc=16
%mem=2GB
%chk=mol_064_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.58610       -0.59820        0.51530
C         -2.17840        0.67750        0.80080
C         -0.92580        1.11250        0.37940
C         -0.08510        0.28420       -0.32090
C         -0.53060       -0.98750       -0.58720
C         -1.77050       -1.44600       -0.18040
C          1.25110        0.72410       -0.77970
C          2.32420        0.44220        0.25180
O          2.43050       -0.90030        0.54710
H         -3.55600       -0.95170        0.83520
H         -2.84980        1.34150        1.36080
H         -0.67130        2.13050        0.64040
H          0.09710       -1.67140       -1.13420
H         -2.06260       -2.47740       -0.42860
H          1.26200        1.79520       -1.02290
H          1.56970        0.19100       -1.71170
H          3.28860        0.79080       -0.20040
H          2.12920        0.97760        1.20560
H          2.86380       -1.43460       -0.17030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

