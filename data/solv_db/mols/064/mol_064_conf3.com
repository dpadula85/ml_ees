%nproc=16
%mem=2GB
%chk=mol_064_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.65910       -0.00220        0.57020
C         -1.97410       -1.17140        0.59400
C         -0.69890       -1.22820        0.09080
C         -0.05500       -0.13520       -0.45310
C         -0.74210        1.05250       -0.48080
C         -2.04160        1.11490        0.03090
C          1.30570       -0.27260       -0.97160
C          2.37160       -0.00460        0.07460
O          2.26820        1.28890        0.58380
H         -3.68200        0.05970        0.96960
H         -2.42940       -2.06390        1.00950
H         -0.16110       -2.16090        0.11340
H         -0.29980        1.95310       -0.89290
H         -2.61260        2.03760        0.02450
H          1.47910       -1.33790       -1.30330
H          1.52780        0.35040       -1.84930
H          3.34950       -0.06050       -0.46110
H          2.40500       -0.77770        0.85600
H          2.64910        1.35800        1.49490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

