%nproc=16
%mem=2GB
%chk=mol_064_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.67470       -0.18270        0.58890
C          2.20830        0.97680        0.00080
C          0.93330        1.06670       -0.51890
C          0.06560       -0.02060       -0.46710
C          0.54930       -1.17060        0.12420
C          1.82600       -1.26980        0.64790
C         -1.29600        0.00470       -0.99760
C         -2.31810        0.45530        0.03650
O         -2.27300       -0.42260        1.08750
H          3.68000       -0.25600        0.99950
H          2.87550        1.85180       -0.05280
H          0.56060        1.97890       -0.98370
H         -0.12950       -2.03670        0.17020
H          2.18980       -2.18700        1.11110
H         -1.38200        0.73860       -1.84260
H         -1.62900       -0.96640       -1.39030
H         -2.06640        1.50750        0.31060
H         -3.32670        0.39490       -0.41970
H         -3.14260       -0.46280        1.59560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_064_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

