%nproc=16
%mem=2GB
%chk=mol_179_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.87240       -0.08630        0.02840
C          0.48090        0.49040       -0.07830
O          0.71830        1.68140        0.23940
O          1.54610       -0.26970       -0.53700
H         -1.21780        0.11150        1.08010
H         -0.84480       -1.18130       -0.07280
H         -1.59510        0.34160       -0.69340
H          1.78470       -1.08750        0.03370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

