%nproc=16
%mem=2GB
%chk=mol_179_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.89890       -0.19890        0.03370
C          0.40770        0.43870       -0.27360
O          0.54990        1.63860       -0.57530
O          1.54030       -0.38960       -0.21240
H         -1.67950        0.57540        0.10170
H         -0.78040       -0.75650        1.00430
H         -1.14060       -0.91500       -0.78070
H          2.00150       -0.39260        0.70240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

