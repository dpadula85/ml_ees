%nproc=16
%mem=2GB
%chk=mol_179_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.88830        0.09710       -0.01880
C          0.47920       -0.44260       -0.14010
O          0.66300       -1.44090       -0.88570
O          1.56720        0.07830        0.51180
H         -1.34790       -0.21710        0.96390
H         -1.53920       -0.27810       -0.81660
H         -0.87460        1.20150        0.00360
H          1.94050        1.00180        0.38200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

