%nproc=16
%mem=2GB
%chk=mol_179_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.81080       -0.08560       -0.37670
C          0.52000       -0.44450        0.15930
O          0.92810       -1.62360        0.24930
O          1.35810        0.56850        0.58320
H         -0.67550        0.71200       -1.14470
H         -1.29140       -0.93270       -0.90870
H         -1.48650        0.34270        0.41630
H          1.45800        1.46320        0.16110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_179_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

