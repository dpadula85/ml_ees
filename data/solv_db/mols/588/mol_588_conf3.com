%nproc=16
%mem=2GB
%chk=mol_588_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.66020       -0.25180       -0.05310
C         -2.96790        0.98880       -0.54670
C         -1.55740        0.71190       -1.00950
C         -0.71140        0.14480        0.12310
C          0.69810       -0.13310       -0.33460
C          1.47100       -0.68600        0.82200
C          2.90790       -1.00070        0.46550
C          3.64470        0.22710        0.00020
Br         5.47190       -0.17630       -0.45250
H         -3.70550       -0.99560       -0.87210
H         -3.22900       -0.66120        0.87390
H         -4.71850        0.03650        0.16490
H         -2.97140        1.72320        0.30210
H         -3.55110        1.36590       -1.41130
H         -1.09310        1.60840       -1.42360
H         -1.62800       -0.03820       -1.82670
H         -0.73270        0.81950        0.98250
H         -1.14460       -0.82350        0.45980
H          1.15090        0.85490       -0.63030
H          0.74570       -0.76650       -1.21940
H          1.47930        0.09610        1.61730
H          0.95060       -1.56500        1.25080
H          2.99680       -1.82480       -0.24930
H          3.39720       -1.33800        1.41330
H          3.58360        0.99190        0.81880
H          3.17270        0.69150       -0.88510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

