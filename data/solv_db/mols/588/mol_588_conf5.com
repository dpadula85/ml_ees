%nproc=16
%mem=2GB
%chk=mol_588_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.16590        0.90070       -0.49470
C          2.15560        0.30340        0.47230
C          1.65760       -0.98060       -0.11500
C          0.65100       -1.56510        0.86010
C         -0.51110       -0.64310        1.09240
C         -1.26420       -0.34670       -0.19420
C         -2.38960        0.57060        0.14880
C         -3.23830        0.96050       -1.03450
Br        -4.63080        2.14940       -0.35090
H          2.74870        1.74960       -1.06940
H          4.05070        1.29170        0.07530
H          3.48930        0.13030       -1.24050
H          2.61020        0.13830        1.45640
H          1.30800        1.03080        0.50840
H          1.23020       -0.85830       -1.10670
H          2.52840       -1.69610       -0.11360
H          1.11660       -1.77670        1.83500
H          0.24920       -2.51630        0.46540
H         -1.21700       -1.17780        1.76740
H         -0.24490        0.29410        1.60640
H         -0.51750        0.16640       -0.86860
H         -1.56760       -1.27910       -0.67570
H         -1.93300        1.48690        0.61940
H         -2.99700        0.10850        0.94760
H         -2.69440        1.51100       -1.79850
H         -3.75590        0.04770       -1.39220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

