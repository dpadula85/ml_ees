%nproc=16
%mem=2GB
%chk=mol_588_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.01040        0.09260       -0.66660
C          2.55300        0.44740       -0.44530
C          2.03800       -0.22090        0.79610
C          0.60350        0.06990        1.09940
C         -0.36100       -0.34840        0.03920
C         -1.76020        0.03470        0.53990
C         -2.80370       -0.35330       -0.47400
C         -4.15120        0.05900        0.09730
Br        -4.21130        1.96380        0.41490
H          4.08750       -1.01300       -0.46500
H          4.32000        0.25160       -1.70370
H          4.67820        0.64600        0.02240
H          2.49840        1.53230       -0.42610
H          1.98340        0.04330       -1.31810
H          2.64010        0.16040        1.64820
H          2.24640       -1.30810        0.73770
H          0.34940       -0.52330        2.02120
H          0.43780        1.12530        1.38100
H         -0.24330        0.19320       -0.91120
H         -0.30130       -1.42920       -0.16320
H         -1.79190        1.13620        0.66300
H         -1.97190       -0.52140        1.46900
H         -2.64500        0.09450       -1.46870
H         -2.84750       -1.45270       -0.60350
H         -4.38340       -0.50730        1.01000
H         -4.97440       -0.17250       -0.63620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

