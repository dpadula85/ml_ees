%nproc=16
%mem=2GB
%chk=mol_588_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.69920        1.22590        0.64640
C          2.93400        0.04580        0.10010
C          1.45040        0.11720        0.48780
C          0.80310       -1.08870       -0.10180
C         -0.65760       -1.21810        0.15730
C         -1.49800       -0.10000       -0.35700
C         -2.93620       -0.41700        0.00350
C         -3.85890        0.68520       -0.49140
Br        -3.29020        2.32180        0.37470
H          4.15580        0.90660        1.61330
H          4.54680        1.51110       -0.01510
H          3.06170        2.11290        0.79430
H          3.02530        0.08390       -1.00600
H          3.32940       -0.92330        0.42830
H          1.40010        0.16680        1.58240
H          1.07720        1.06710        0.05370
H          1.02700       -1.10980       -1.18160
H          1.30020       -1.98580        0.33300
H         -0.86280       -1.34250        1.23970
H         -1.00440       -2.15570       -0.32820
H         -1.22430        0.88830        0.01970
H         -1.46080       -0.12210       -1.48490
H         -3.06380       -0.52890        1.10720
H         -3.28730       -1.35030       -0.45510
H         -4.90300        0.46150       -0.23850
H         -3.76290        0.74810       -1.59430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

