%nproc=16
%mem=2GB
%chk=mol_588_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.95570        0.99770        0.07670
C         -2.54900        0.80870        0.58760
C         -1.73280       -0.05700       -0.37200
C         -0.30710       -0.21910        0.19470
C          0.51580       -1.05920       -0.71820
C          1.89930       -1.34660       -0.39090
C          3.04980       -0.49600       -0.39570
C          3.29170        0.76530        0.27890
Br         2.32460        2.31290       -0.32580
H         -4.10370        0.44100       -0.87280
H         -4.72240        0.65980        0.81730
H         -4.18900        2.08980       -0.08490
H         -2.07330        1.80660        0.65690
H         -2.54780        0.35040        1.59780
H         -1.61910        0.51670       -1.30670
H         -2.23760       -1.02320       -0.47350
H         -0.39070       -0.78790        1.17840
H          0.00320        0.76450        0.47300
H          0.40440       -0.58380       -1.76120
H         -0.09680       -2.02170       -0.89510
H          2.17040       -2.26150       -1.08850
H          1.90940       -1.95500        0.62300
H          3.97270       -1.16580       -0.08820
H          3.41740       -0.30260       -1.50170
H          4.39080        1.07580        0.18190
H          3.17570        0.69020        1.41400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_588_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

