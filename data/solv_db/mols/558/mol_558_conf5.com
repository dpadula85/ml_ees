%nproc=16
%mem=2GB
%chk=mol_558_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.61490        0.64410       -0.33880
C          1.18820        1.00770        0.00480
C          0.24590       -0.11230       -0.26400
C          0.65370       -1.30440        0.57520
C         -1.19940        0.24340       -0.06650
C         -1.53600        0.68830        1.31470
C         -2.12380       -0.89730       -0.43290
H          3.25220        1.53380       -0.52380
H          3.09310        0.02570        0.44150
H          2.57200        0.10220       -1.31600
H          0.94840        1.93480       -0.54210
H          1.19850        1.26190        1.10520
H          0.38440       -0.42290       -1.32650
H          1.70970       -1.54140        0.38670
H          0.06900       -2.21600        0.33200
H          0.56050       -1.02500        1.65760
H         -1.45720        1.07990       -0.76020
H         -2.64370        0.53450        1.45460
H         -1.40200        1.80200        1.39860
H         -1.04860        0.14430        2.12080
H         -1.83000       -1.28980       -1.42780
H         -2.11190       -1.72220        0.28210
H         -3.13790       -0.47120       -0.52040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

