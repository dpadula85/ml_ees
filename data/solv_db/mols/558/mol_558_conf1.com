%nproc=16
%mem=2GB
%chk=mol_558_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.60600        0.08740        0.22920
C          1.40590       -0.47930       -0.46530
C          0.19460       -0.23110        0.44000
C          0.05690        1.26910        0.63960
C         -1.02060       -0.70920       -0.31420
C         -2.23040       -0.47850        0.55060
C         -1.09920        0.00040       -1.63160
H          2.60720       -0.24810        1.28360
H          2.64140        1.19580        0.12580
H          3.53500       -0.34030       -0.22460
H          1.24680       -0.05510       -1.46530
H          1.56350       -1.58380       -0.55130
H          0.32720       -0.74330        1.39870
H          0.50190        1.51990        1.64290
H          0.58290        1.84430       -0.16040
H         -1.02590        1.51690        0.69590
H         -0.93620       -1.80740       -0.48830
H         -2.74790        0.46710        0.34810
H         -2.93580       -1.31680        0.43190
H         -1.90630       -0.45960        1.61930
H         -0.66630       -0.63940       -2.42110
H         -0.52500        0.94480       -1.61830
H         -2.17580        0.24630       -1.82820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

