%nproc=16
%mem=2GB
%chk=mol_558_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.65400       -1.00170       -0.78130
C          1.41750       -0.25840        0.45450
C          0.32450        0.72630        0.48530
C          0.53880        1.80190       -0.54560
C         -1.06570        0.21470        0.36670
C         -1.47260       -0.74120        1.44370
C         -1.39370       -0.33390       -1.00400
H          2.74480       -1.25720       -0.87330
H          1.14610       -1.99980       -0.71330
H          1.35780       -0.55870       -1.72170
H          2.36320        0.30010        0.72560
H          1.30620       -0.99700        1.30290
H          0.38540        1.25120        1.48690
H          0.93020        1.42100       -1.49110
H          1.31770        2.50780       -0.13770
H         -0.42990        2.33540       -0.67500
H         -1.73840        1.11950        0.48130
H         -0.65190       -0.96570        2.16910
H         -2.35020       -0.38840        2.04170
H         -1.81510       -1.72030        0.99900
H         -1.16310       -1.41280       -0.99360
H         -0.90520        0.22980       -1.81000
H         -2.50030       -0.27240       -1.21040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

