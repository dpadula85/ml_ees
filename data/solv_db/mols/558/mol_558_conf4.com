%nproc=16
%mem=2GB
%chk=mol_558_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.66950        0.44970        0.06370
C         -1.24230        0.93730       -0.05970
C         -0.30270       -0.16020        0.31900
C         -0.46720       -1.36020       -0.56420
C          1.12400        0.32240        0.40490
C          1.64010        0.88340       -0.88130
C          2.02450       -0.75450        0.92230
H         -3.37080        1.26570        0.34680
H         -2.70740       -0.28100        0.90270
H         -2.98500       -0.08440       -0.85010
H         -1.07680        1.29260       -1.09550
H         -1.07790        1.79410        0.61220
H         -0.58040       -0.48630        1.36010
H         -0.05380       -1.23550       -1.57350
H         -1.55770       -1.53800       -0.68090
H         -0.06240       -2.25870       -0.04510
H          1.13250        1.14960        1.15480
H          2.57580        1.46240       -0.63330
H          0.94380        1.62050       -1.31500
H          1.93510        0.09380       -1.61890
H          1.40030       -1.61770        1.23020
H          2.77680       -1.06520        0.17620
H          2.60110       -0.42970        1.82440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

