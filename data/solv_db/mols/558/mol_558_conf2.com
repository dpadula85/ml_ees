%nproc=16
%mem=2GB
%chk=mol_558_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.54240        0.41670        0.22190
C          1.08800        0.41900        0.59710
C          0.25640       -0.06590       -0.54950
C          0.69480       -1.47300       -0.88710
C         -1.21370        0.06370       -0.37980
C         -1.74780       -0.70550        0.81660
C         -1.78080        1.41950       -0.29790
H          2.98410       -0.56290        0.09640
H          2.67420        0.99130       -0.69900
H          3.09340        0.94300        1.02440
H          0.89730       -0.24260        1.46790
H          0.90250        1.49570        0.84230
H          0.56250        0.56730       -1.41930
H          1.24460       -1.53850       -1.84470
H          1.36520       -1.89860       -0.09120
H         -0.14840       -2.18370       -0.95510
H         -1.68630       -0.45810       -1.26230
H         -2.78990       -0.36550        0.98740
H         -1.80840       -1.79400        0.61650
H         -1.17920       -0.46740        1.74000
H         -1.24680        2.21440       -0.83340
H         -2.83050        1.44440       -0.68050
H         -1.87340        1.78070        0.77010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_558_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

