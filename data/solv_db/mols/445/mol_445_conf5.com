%nproc=16
%mem=2GB
%chk=mol_445_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17610       -0.12780        0.23410
O         -0.86610       -0.53920        0.25210
C         -0.03370        0.23600       -0.52080
C          1.40130       -0.24230       -0.48020
N          1.83340       -0.17740        0.89750
H         -2.80850       -0.77920        0.86860
H         -2.32270        0.91140        0.58680
H         -2.62170       -0.16000       -0.80180
H         -0.08300        1.27170       -0.14020
H         -0.33450        0.24140       -1.60550
H          2.06960        0.39820       -1.07990
H          1.49990       -1.28160       -0.81430
H          2.77240       -0.54430        1.06520
H          1.66980        0.79310        1.22190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

