%nproc=16
%mem=2GB
%chk=mol_445_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.16310        0.38360       -0.15060
O          0.83560        0.26250       -0.50120
C          0.08040       -0.49000        0.36190
C         -1.34450       -0.51200       -0.18230
N         -1.86680        0.85800       -0.25890
H          2.70070       -0.58450       -0.09000
H          2.66170        0.94240       -0.99210
H          2.34530        0.95670        0.76320
H          0.06240       -0.15060        1.39690
H          0.43980       -1.54920        0.29020
H         -1.99980       -1.10690        0.48540
H         -1.38090       -0.98540       -1.18760
H         -1.87980        1.19980        0.73800
H         -2.81710        0.77580       -0.67290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

