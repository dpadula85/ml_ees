%nproc=16
%mem=2GB
%chk=mol_445_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.16080       -0.28960        0.59620
O         -0.82330       -0.56770        0.34760
C         -0.12330        0.46790       -0.19580
C          1.31860       -0.02910       -0.39460
N          1.89030       -0.41770        0.88140
H         -2.21710        0.54880        1.29840
H         -2.59500        0.01430       -0.40260
H         -2.73200       -1.16480        0.92020
H         -0.08310        1.34980        0.47190
H         -0.57350        0.71570       -1.17880
H          1.93880        0.75800       -0.88950
H          1.33750       -0.90170       -1.07170
H          2.04080        0.43880        1.46070
H          2.78210       -0.92260        0.67080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

