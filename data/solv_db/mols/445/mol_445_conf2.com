%nproc=16
%mem=2GB
%chk=mol_445_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95360        0.02870        0.65160
O          1.25270       -0.75000       -0.23180
C          0.01470       -0.28090       -0.57440
C         -0.93290       -0.12320        0.58450
N         -2.19660        0.38330        0.03880
H          2.93420       -0.47680        0.83440
H          2.21010        1.03900        0.26060
H          1.46060        0.11350        1.65500
H         -0.44670       -1.00940       -1.27340
H          0.03110        0.69320       -1.11730
H         -0.56730        0.66300        1.27210
H         -1.09270       -1.03940        1.16130
H         -2.71730       -0.36300       -0.48010
H         -1.90340        1.12190       -0.66180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

