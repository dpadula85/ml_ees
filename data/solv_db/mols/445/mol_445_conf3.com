%nproc=16
%mem=2GB
%chk=mol_445_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.13790       -0.61690        0.01880
O         -0.79210       -0.69260       -0.41330
C         -0.19080        0.46060        0.08800
C          1.26610        0.55530       -0.27650
N          2.04450       -0.54470        0.22210
H         -2.07470       -0.59030        1.13480
H         -2.72180       -1.45120       -0.37220
H         -2.49790        0.38060       -0.31870
H         -0.36440        0.56100        1.18600
H         -0.70650        1.32120       -0.38870
H          1.32540        0.66190       -1.37670
H          1.64790        1.50300        0.15140
H          2.19540       -1.30770       -0.46940
H          3.00690       -0.24020        0.54260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_445_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

