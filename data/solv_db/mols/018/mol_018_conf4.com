%nproc=16
%mem=2GB
%chk=mol_018_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.73260       -0.00370       -0.21770
C         -0.73740        0.01990        0.18580
Cl        -0.88710       -0.32760        1.91030
Cl        -1.47590        1.60130       -0.11260
Cl        -1.57390       -1.22040       -0.75150
Cl         0.92210        0.34060       -1.94060
Cl         1.35050       -1.60720        0.21880
Cl         1.66920        1.19710        0.70750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

