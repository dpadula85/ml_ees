%nproc=16
%mem=2GB
%chk=mol_018_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.70530       -0.07450       -0.25140
C          0.70470       -0.00700        0.23340
Cl         1.36260        1.54390       -0.39570
Cl         1.69720       -1.37560       -0.22060
Cl         0.72400        0.09930        2.02600
Cl        -0.68920       -0.03660       -2.03700
Cl        -1.53110        1.38080        0.40130
Cl        -1.56310       -1.53030        0.24390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

