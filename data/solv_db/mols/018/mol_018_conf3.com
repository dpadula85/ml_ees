%nproc=16
%mem=2GB
%chk=mol_018_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.75170        0.13910       -0.00640
C         -0.71050       -0.21920        0.01380
Cl        -1.06080       -1.66090       -0.95650
Cl        -1.60540        1.17940       -0.63810
Cl        -1.28760       -0.51790        1.67690
Cl         1.19380        0.45010       -1.70560
Cl         0.93580        1.68830        0.86360
Cl         1.78310       -1.05900        0.75240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

