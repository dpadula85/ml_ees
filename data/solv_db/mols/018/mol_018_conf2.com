%nproc=16
%mem=2GB
%chk=mol_018_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.74800       -0.06850        0.07000
C         -0.76940        0.07430       -0.07550
Cl        -1.47430        0.27840        1.52490
Cl        -1.37620       -1.40510       -0.83620
Cl        -1.19570        1.45890       -1.07140
Cl         1.09810       -1.49030        1.07850
Cl         1.44550        1.38490        0.83290
Cl         1.52400       -0.23260       -1.52330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

