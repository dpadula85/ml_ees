%nproc=16
%mem=2GB
%chk=mol_018_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.73180        0.05480        0.18090
C         -0.70470       -0.09670       -0.26540
Cl        -1.15030       -1.79690       -0.49520
Cl        -1.71570        0.51990        1.08910
Cl        -1.07130        0.82580       -1.70870
Cl         1.89160       -0.60000       -0.95780
Cl         0.98380       -0.70380        1.77110
Cl         1.03480        1.79690        0.38610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_018_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

