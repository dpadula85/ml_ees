%nproc=16
%mem=2GB
%chk=mol_634_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06410        0.18310        0.24170
C          0.90110       -0.44600       -0.42280
C         -0.42120        0.20650       -0.16030
C         -1.44000       -0.61970       -0.94720
C         -0.82960        0.20390        1.26880
O         -0.37550        1.49260       -0.71820
H          2.82870       -0.62820        0.42900
H          1.86690        0.62640        1.22970
H          2.60770        0.93380       -0.37910
H          1.07150       -0.38280       -1.53710
H          0.79940       -1.52680       -0.13010
H         -0.99680       -1.02570       -1.88190
H         -2.36700       -0.08050       -1.12300
H         -1.67630       -1.52290       -0.32000
H         -1.59300        0.98730        1.46330
H         -1.24640       -0.79940        1.51210
H         -0.01200        0.38090        1.98520
H         -1.18140        2.01740       -0.51000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

