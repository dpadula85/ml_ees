%nproc=16
%mem=2GB
%chk=mol_634_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.79010        0.03770        0.53780
C          0.93460        0.03250       -0.69900
C         -0.55900        0.05480       -0.35460
C         -0.90670        1.27980        0.43100
C         -0.95820       -1.21910        0.35710
O         -1.22940        0.09790       -1.57690
H          2.17190       -1.01760        0.65900
H          2.61760        0.75490        0.36190
H          1.21370        0.36100        1.42850
H          1.10780        0.94570       -1.32760
H          1.12840       -0.83580       -1.36060
H         -0.06250        1.91450        0.69580
H         -1.37180        0.94860        1.39950
H         -1.72230        1.87310       -0.07150
H         -1.67910       -0.90340        1.16610
H         -1.52320       -1.91590       -0.29400
H         -0.08580       -1.75750        0.76770
H         -0.86600       -0.65120       -2.12010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

