%nproc=16
%mem=2GB
%chk=mol_634_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.84420       -0.01560        0.48790
C          0.94410       -0.19840       -0.70060
C         -0.50650        0.04010       -0.37990
C         -0.77990        1.42060        0.12370
C         -1.05990       -0.97540        0.58690
O         -1.21270       -0.11330       -1.59260
H          1.98920        1.05930        0.74170
H          2.85050       -0.41450        0.21190
H          1.50780       -0.55670        1.37240
H          1.25970        0.42510       -1.53140
H          1.03900       -1.26550       -1.01730
H         -0.90550        1.41460        1.23240
H          0.00680        2.14910       -0.15300
H         -1.74250        1.79030       -0.28530
H         -1.32780       -0.50100        1.55810
H         -0.35170       -1.79300        0.79500
H         -2.00960       -1.42460        0.20700
H         -1.54510       -1.04110       -1.65680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

