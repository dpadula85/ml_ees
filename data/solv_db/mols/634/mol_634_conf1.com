%nproc=16
%mem=2GB
%chk=mol_634_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.81760       -0.02700        0.53730
C          0.93600       -0.31560       -0.63580
C         -0.51100       -0.00630       -0.39490
C         -0.75050        1.44480       -0.06110
C         -1.10520       -0.83700        0.71720
O         -1.21900       -0.28970       -1.55890
H          2.20410       -0.95920        1.03130
H          2.72480        0.57000        0.27700
H          1.31040        0.53820        1.34830
H          1.25070        0.22210       -1.55030
H          1.00730       -1.40540       -0.84940
H         -0.58860        1.68980        0.99570
H         -0.16980        2.05550       -0.76720
H         -1.83010        1.69660       -0.26880
H         -0.90160       -0.37090        1.71140
H         -2.20480       -0.87520        0.53130
H         -0.74980       -1.87560        0.69520
H         -1.22040       -1.25520       -1.75840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

