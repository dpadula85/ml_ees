%nproc=16
%mem=2GB
%chk=mol_634_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04150       -0.05160        0.44060
C          0.80790       -0.74620       -0.04950
C         -0.44290        0.06610        0.11780
C         -0.43120        1.36450       -0.60280
C         -1.60370       -0.80380       -0.37520
O         -0.62890        0.24300        1.49630
H          2.49350        0.49460       -0.40880
H          1.86580        0.58280        1.32160
H          2.77270       -0.84730        0.73210
H          0.69260       -1.69290        0.51710
H          0.97820       -1.05150       -1.10980
H         -1.15420        1.33310       -1.44970
H          0.55450        1.71660       -0.92700
H         -0.84750        2.15120        0.09250
H         -2.49050       -0.15070       -0.50640
H         -1.77290       -1.62320        0.35780
H         -1.25310       -1.21360       -1.36480
H         -1.58190        0.22890        1.71820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_634_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

