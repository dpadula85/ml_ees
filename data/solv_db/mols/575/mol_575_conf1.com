%nproc=16
%mem=2GB
%chk=mol_575_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04910       -0.03640       -0.25760
C         -1.39350       -1.14180        0.24450
C         -0.06190       -1.05550        0.61200
C          0.69680        0.08870        0.38920
C         -0.02830        1.24140        0.13350
C         -1.39040        1.18020       -0.11720
C          2.11760       -0.01830       -0.09440
F          2.26320        0.36920       -1.40290
F          2.98040        0.68250        0.71750
F          2.49830       -1.35480       -0.00900
H         -2.75890       -0.18040       -1.08150
H         -1.89920       -2.09930        0.13370
H          0.46590       -1.91410        0.96510
H          0.49820        2.18240        0.18460
H         -1.93910        2.05620       -0.41750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

