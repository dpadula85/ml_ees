%nproc=16
%mem=2GB
%chk=mol_575_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.01570        0.06050        0.05830
C          1.34080        1.23450       -0.01820
C         -0.06250        1.19980       -0.22600
C         -0.71460        0.00500       -0.31920
C          0.02050       -1.12800       -0.43040
C          1.42410       -1.12340       -0.21430
C         -2.11600       -0.10860        0.23010
F         -2.97790       -0.78930       -0.54090
F         -2.55420        1.15360        0.58620
F         -2.02650       -0.78460        1.44230
H          2.88790        0.05950        0.75900
H          1.89060        2.16430       -0.10110
H         -0.61580        2.11700       -0.29800
H         -0.45930       -2.01510       -0.85130
H          1.94720       -2.04510       -0.07640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

