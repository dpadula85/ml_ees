%nproc=16
%mem=2GB
%chk=mol_575_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04240        0.23820        0.23070
C         -1.23800        1.36370        0.10160
C          0.12630        1.18850       -0.04370
C          0.69890       -0.05960       -0.03640
C         -0.15220       -1.13920       -0.16330
C         -1.51290       -1.02650        0.01690
C          2.16240       -0.29500        0.02190
F          2.59720       -0.81170        1.23020
F          2.54140       -1.22560       -0.94770
F          2.75500        0.93570       -0.24630
H         -2.95590        0.34110        0.84160
H         -1.72610        2.30960       -0.14850
H          0.76780        2.06340        0.03570
H          0.19970       -2.06340       -0.66170
H         -2.22130       -1.81930       -0.23100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

