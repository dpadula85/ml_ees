%nproc=16
%mem=2GB
%chk=mol_575_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.07080        0.05060       -0.01560
C          1.37300       -1.08290        0.20310
C          0.07290       -1.15250        0.31750
C         -0.72160        0.01140        0.22760
C         -0.01360        1.25160        0.10610
C          1.31790        1.28950        0.03330
C         -2.07240       -0.17230       -0.37010
F         -2.33310       -1.29830       -1.03000
F         -3.01160       -0.24530        0.81830
F         -2.42140        0.88530       -1.10650
H          3.07310        0.19960       -0.20130
H          1.94290       -2.03040        0.22930
H         -0.45570       -2.05920        0.63550
H         -0.66280        2.11620        0.08330
H          1.84170        2.23650        0.06960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

