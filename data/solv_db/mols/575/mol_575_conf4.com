%nproc=16
%mem=2GB
%chk=mol_575_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.97850       -0.38090        0.24990
C         -1.11940       -1.41420       -0.07570
C          0.21170       -1.11730       -0.33000
C          0.65650        0.17390       -0.18320
C         -0.21600        1.22340       -0.01730
C         -1.55640        0.92800        0.21260
C          2.12000        0.37370        0.04480
F          2.74750        0.33320       -1.15060
F          2.59820       -0.71460        0.81180
F          2.32550        1.55840        0.73590
H         -2.99520       -0.62270        0.49140
H         -1.46050       -2.44220        0.02860
H          0.80140       -1.86240       -0.86370
H          0.13190        2.22720       -0.18460
H         -2.26670        1.73640        0.23030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_575_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

