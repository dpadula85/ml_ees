%nproc=16
%mem=2GB
%chk=mol_334_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19470       -0.02240       -0.82770
C          1.50850       -0.67790        0.35130
C          0.01650       -0.59510        0.26620
C         -0.47710        0.83620        0.22640
C         -1.94520        0.91660        0.14260
C         -2.71440       -0.16870        0.10800
H          1.66720       -0.22910       -1.78270
H          3.22530       -0.41230       -0.95930
H          2.21610        1.08480       -0.69760
H          1.81500       -1.73880        0.37260
H          1.91330       -0.17240        1.26300
H         -0.42520       -1.09290        1.13550
H         -0.29040       -1.08190       -0.68240
H         -0.06160        1.38160       -0.65340
H         -0.10930        1.39150        1.12110
H         -2.44400        1.87510        0.10670
H         -2.29500       -1.17910        0.14010
H         -3.79430       -0.11520        0.04610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

