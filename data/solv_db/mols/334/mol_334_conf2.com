%nproc=16
%mem=2GB
%chk=mol_334_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.12410       -0.83590        0.50270
C         -1.74100        0.51770        0.01400
C         -0.30000        0.86010        0.26960
C          0.65820       -0.09260       -0.39470
C          2.04650        0.34010       -0.07570
C          2.88950       -0.45470        0.56920
H         -3.22390       -0.82740        0.70330
H         -1.95180       -1.56670       -0.30510
H         -1.63730       -1.10940        1.44920
H         -1.92530        0.58400       -1.08870
H         -2.38070        1.26540        0.48850
H         -0.07600        1.88330       -0.08810
H         -0.09060        0.82200        1.36290
H          0.48310       -1.14060       -0.15260
H          0.52490        0.01720       -1.50100
H          2.37230        1.32840       -0.37930
H          2.59390       -1.44940        0.88650
H          3.88210       -0.14170        0.79560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

