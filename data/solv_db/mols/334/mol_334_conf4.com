%nproc=16
%mem=2GB
%chk=mol_334_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.64240        0.10520       -0.24580
C         -1.39620       -0.65100        0.07700
C         -0.13310        0.16380       -0.13300
C          1.03110       -0.71760        0.22790
C          2.33460       -0.04080        0.06690
C          2.48960        1.20650       -0.34860
H         -3.48080       -0.38490        0.31670
H         -2.57260        1.18540       -0.01490
H         -2.92250        0.02420       -1.31440
H         -1.33290       -1.55050       -0.54390
H         -1.42560       -0.97180        1.13290
H         -0.15360        1.10960        0.43920
H         -0.07810        0.40470       -1.21400
H          1.02200       -1.56710       -0.51280
H          0.90880       -1.20000        1.21800
H          3.22430       -0.60510        0.30580
H          3.47070        1.63600       -0.44260
H          1.65660        1.85330       -0.61240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

