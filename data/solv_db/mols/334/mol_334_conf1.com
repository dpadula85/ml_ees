%nproc=16
%mem=2GB
%chk=mol_334_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.42220        0.39730        0.22340
C          1.02190        0.42480       -0.29300
C          0.39550       -0.93990       -0.10340
C         -1.02970       -0.97300       -0.61030
C         -1.82280        0.00920        0.12460
C         -2.42230        0.99560       -0.51090
H          2.64200       -0.57610        0.70840
H          3.19070        0.53950       -0.56150
H          2.53640        1.22840        0.94760
H          0.97830        0.68910       -1.37870
H          0.37710        1.13430        0.30540
H          1.00660       -1.69140       -0.65370
H          0.39020       -1.24210        0.95280
H         -1.00370       -0.71030       -1.67360
H         -1.43800       -1.99890       -0.43220
H         -1.91000       -0.08560        1.19570
H         -2.32410        1.07500       -1.58440
H         -3.01050        1.72410        0.03690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

