%nproc=16
%mem=2GB
%chk=mol_334_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.80450       -0.01020        0.05440
C         -1.45790        0.61310       -0.06900
C         -0.33130       -0.40730       -0.06040
C          0.96170        0.38790       -0.19310
C          2.14460       -0.48760       -0.19830
C          3.07080       -0.35530        0.72870
H         -3.15090        0.04680        1.09900
H         -2.86210       -1.04940       -0.32750
H         -3.52470        0.57660       -0.56790
H         -1.28530        1.27740        0.80320
H         -1.32440        1.21470       -0.98870
H         -0.26940       -0.95080        0.89720
H         -0.41950       -1.12540       -0.87330
H          0.96920        0.94000       -1.17450
H          1.06630        1.14400        0.58020
H          2.25000       -1.23530       -0.96100
H          3.96810       -0.97430        0.77610
H          2.99930        0.39520        1.52350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_334_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

