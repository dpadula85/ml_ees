%nproc=16
%mem=2GB
%chk=mol_447_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.14490        0.54400        1.17250
C          3.24270       -0.03130       -0.17700
C          2.18510        0.05410       -0.98210
C          0.92450        0.70800       -0.55680
C         -0.18380       -0.33480       -0.63390
C         -1.48490        0.30350       -0.20520
C         -1.72020        1.44630       -1.18140
C         -1.34530        0.95400        1.14110
C         -2.15530        1.90600        1.55890
O         -2.58200       -0.53700       -0.30450
C         -2.76160       -1.72960        0.33620
O         -1.86850       -2.13340        1.09470
C         -3.97660       -2.55940        0.16320
C          4.50360       -0.68700       -0.60990
H          3.09480        1.66060        1.11530
H          4.04670        0.30930        1.76090
H          2.26750        0.10910        1.69660
H          2.21240       -0.35620       -1.98520
H          0.98900        1.12050        0.47050
H          0.71960        1.53930       -1.26490
H         -0.25990       -0.68570       -1.68360
H          0.11790       -1.18150       -0.02490
H         -1.18740        2.35770       -0.81220
H         -2.78050        1.64280       -1.35020
H         -1.22720        1.21800       -2.17130
H         -0.55310        0.64100        1.80880
H         -2.97870        2.28100        0.95150
H         -1.99340        2.32900        2.54330
H         -4.24250       -3.12810        1.08200
H         -3.78590       -3.27650       -0.65670
H         -4.85180       -1.93900       -0.10590
H          4.54330       -1.72710       -0.23120
H          4.57200       -0.70420       -1.72030
H          5.37440       -0.11360       -0.23830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_447_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_447_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_447_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

