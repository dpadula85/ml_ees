%nproc=16
%mem=2GB
%chk=mol_447_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.43510       -0.20790        0.78040
C         -3.18000       -0.38510        0.05530
C         -2.04270       -0.76330        0.62980
C         -0.79270       -0.93600       -0.11460
C          0.27430       -0.00510        0.42780
C          1.57090       -0.17070       -0.32970
C          1.41920        0.13730       -1.79320
C          1.97060       -1.59740       -0.21700
C          2.07660       -2.17890        0.95290
O          2.60510        0.55550        0.27940
C          2.62530        1.91060        0.45690
O          1.65590        2.58350        0.04850
C          3.76280        2.60670        1.11940
C         -3.17730       -0.12450       -1.42450
H         -4.53590        0.86850        1.04950
H         -4.53850       -0.81740        1.68360
H         -5.27860       -0.43720        0.10870
H         -2.05330       -0.94940        1.69830
H         -0.95520       -0.62880       -1.17620
H         -0.43910       -1.99360       -0.13570
H          0.42740       -0.21860        1.50280
H         -0.15110        1.01320        0.35230
H          0.66920        0.90360       -2.01630
H          2.38700        0.47090       -2.17630
H          1.12530       -0.78570       -2.34230
H          2.18310       -2.15360       -1.10840
H          1.87000       -1.61080        1.85950
H          2.36990       -3.22790        1.03820
H          3.42270        3.03520        2.09710
H          4.08840        3.42380        0.45210
H          4.59880        1.89430        1.28160
H         -2.54110        0.75890       -1.61090
H         -2.80510       -1.04650       -1.92770
H         -4.17680        0.07640       -1.80620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_447_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_447_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_447_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

