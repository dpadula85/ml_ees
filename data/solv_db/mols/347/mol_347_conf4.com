%nproc=16
%mem=2GB
%chk=mol_347_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49180        1.67800        0.08810
C         -1.26520        0.84570        0.04110
C         -0.02360        1.42660        0.17650
C          1.10590        0.61450        0.12760
C          2.37450        1.12610        0.25540
C          3.46300        0.28850        0.20120
C          3.34670       -1.07190        0.02080
C          2.08090       -1.59300       -0.10820
C          0.96850       -0.74350       -0.05360
C         -0.28920       -1.33460       -0.19110
C         -1.39730       -0.52650       -0.14160
C         -2.75670       -1.10340       -0.28230
H         -3.19910        1.15130        0.78360
H         -2.99540        1.77400       -0.87370
H         -2.29050        2.66030        0.58990
H          0.10610        2.48410        0.31880
H          2.48510        2.18740        0.39700
H          4.47480        0.67610        0.30060
H          4.21420       -1.73880       -0.02230
H          1.90620       -2.65590       -0.25320
H         -0.41970       -2.39300       -0.33330
H         -3.48980       -0.32920       -0.59550
H         -3.13120       -1.55300        0.66590
H         -2.77640       -1.86970       -1.11180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

