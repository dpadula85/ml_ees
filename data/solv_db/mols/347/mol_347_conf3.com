%nproc=16
%mem=2GB
%chk=mol_347_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.37920        1.86680       -0.37500
C         -1.24300        0.91660       -0.18090
C          0.07160        1.31500       -0.31850
C          1.13700        0.45420       -0.14380
C          2.45200        0.86080       -0.28310
C          3.51260       -0.00410       -0.10700
C          3.28520       -1.32530        0.21960
C          1.97600       -1.72820        0.35790
C          0.90930       -0.86760        0.18300
C         -0.40200       -1.27510        0.32270
C         -1.46540       -0.40590        0.14590
C         -2.87420       -0.84170        0.29550
H         -3.21440        1.42460       -0.92590
H         -2.68470        2.23400        0.64520
H         -1.98060        2.75920       -0.91730
H          0.26430        2.33270       -0.57060
H          2.62350        1.89580       -0.53890
H          4.53630        0.30610       -0.21360
H          4.07850       -2.04370        0.36840
H          1.75510       -2.75470        0.61290
H         -0.57580       -2.31580        0.57970
H         -2.97870       -1.84220        0.72190
H         -3.31110       -0.85250       -0.73840
H         -3.49210       -0.10910        0.86010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

