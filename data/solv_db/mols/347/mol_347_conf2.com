%nproc=16
%mem=2GB
%chk=mol_347_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.57400        1.35060       -0.82700
C         -1.30850        0.67560       -0.41540
C         -0.08350        1.22810       -0.72860
C          1.06020        0.55960       -0.32220
C          2.32100        1.06610       -0.60730
C          3.45780        0.39910       -0.20120
C          3.40410       -0.78760        0.49860
C          2.15280       -1.29280        0.78300
C          1.01340       -0.62770        0.37810
C         -0.24410       -1.16020        0.67910
C         -1.39520       -0.51040        0.28360
C         -2.74190       -1.07160        0.60120
H         -2.91470        0.87020       -1.74390
H         -3.29930        1.25400        0.02520
H         -2.43250        2.45220       -0.95550
H         -0.02420        2.16010       -1.27810
H          2.34270        2.00200       -1.15930
H          4.43440        0.80090       -0.42750
H          4.30910       -1.30440        0.81380
H          2.05370       -2.21930        1.32870
H         -0.33380       -2.09030        1.22710
H         -2.58400       -2.17420        0.76100
H         -3.15860       -0.64500        1.52270
H         -3.45500       -0.93490       -0.23600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

