%nproc=16
%mem=2GB
%chk=mol_347_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.42970        1.52700       -0.89110
C         -1.22960        0.77280       -0.45480
C          0.06330        1.19440       -0.73750
C          1.12370        0.41590       -0.28350
C          2.39540        0.86160       -0.58050
C          3.47760        0.12060       -0.14980
C          3.28910       -1.04290        0.56360
C          2.01770       -1.46520        0.84630
C          0.92260       -0.74820        0.43050
C         -0.37900       -1.17000        0.71350
C         -1.42050       -0.38890        0.25760
C         -2.79940       -0.83210        0.55510
H         -3.01910        0.94390       -1.64500
H         -3.04750        1.87730       -0.04610
H         -2.11220        2.45500       -1.43940
H          0.21040        2.10580       -1.29630
H          2.52190        1.78690       -1.14720
H          4.47830        0.47970       -0.38860
H          4.13430       -1.61930        0.89850
H          1.80220       -2.38310        1.41040
H         -0.54480       -2.08090        1.27240
H         -3.56280       -0.11160        0.20950
H         -3.00560       -1.86970        0.22070
H         -2.88630       -0.82890        1.68170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

