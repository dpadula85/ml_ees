%nproc=16
%mem=2GB
%chk=mol_347_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73400       -1.19710        0.30490
C         -1.41940       -0.54490        0.15260
C         -0.26410       -1.31720        0.12740
C          0.99320       -0.76660       -0.01230
C          2.14580       -1.53910       -0.03740
C          3.39650       -0.98330       -0.17710
C          3.45490        0.40010       -0.29320
C          2.31310        1.17170       -0.26890
C          1.06820        0.60650       -0.12880
C         -0.09160        1.37460       -0.10290
C         -1.34300        0.80730        0.03770
C         -2.54770        1.63860        0.06170
H         -3.52380       -0.76260       -0.33350
H         -2.65300       -2.27590       -0.02180
H         -3.02720       -1.25240        1.38180
H         -0.29840       -2.40520        0.21780
H          2.04010       -2.60540        0.05660
H          4.27060       -1.61600       -0.19230
H          4.44920        0.81850       -0.40240
H          2.37140        2.24440       -0.36000
H         -0.00850        2.45810       -0.19650
H         -2.74540        2.06930       -0.96180
H         -2.41610        2.55140        0.71430
H         -3.43080        1.12510        0.43420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_347_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

