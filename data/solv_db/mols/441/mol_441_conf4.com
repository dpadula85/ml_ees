%nproc=16
%mem=2GB
%chk=mol_441_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07960        1.08830        0.41560
C         -0.09680        0.11570       -0.18370
C         -0.85570       -1.17060       -0.45970
O          0.95500       -0.20200        0.68450
C          2.30710       -0.00830        0.35840
O          2.51900        0.47700       -0.78230
H         -0.62490        2.09350        0.37850
H         -1.40700        0.77940        1.42070
H         -1.98200        1.11280       -0.22470
H          0.22770        0.53360       -1.15160
H         -1.45070       -1.12610       -1.38590
H         -1.51060       -1.45370        0.41170
H         -0.10150       -1.97340       -0.53900
H          3.09990       -0.26650        1.05760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

