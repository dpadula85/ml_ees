%nproc=16
%mem=2GB
%chk=mol_441_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12490        0.84620        0.66210
C         -0.10120       -0.15980        0.17530
C         -0.79630       -0.97580       -0.91660
O          0.98920        0.56840       -0.35360
C          2.30320        0.36520        0.10170
O          2.42830       -0.50520        1.00560
H         -0.58620        1.74690        1.04050
H         -1.80120        1.10690       -0.20250
H         -1.78010        0.40770        1.44640
H          0.19000       -0.87150        0.95050
H         -1.89210       -0.90070       -0.85140
H         -0.48650       -0.50960       -1.88100
H         -0.47810       -2.04420       -0.87060
H          3.13590        0.92550       -0.30640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

