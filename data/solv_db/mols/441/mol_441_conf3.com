%nproc=16
%mem=2GB
%chk=mol_441_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.05380        1.06250       -0.12350
C         -0.10200       -0.11010        0.13330
C         -0.70910       -1.31140       -0.54260
O          1.14030        0.25340       -0.36770
C          2.22300        0.37270        0.48400
O          2.00320        0.13580        1.69710
H         -0.55780        2.03330        0.08940
H         -1.35370        1.03130       -1.18050
H         -1.93790        1.00370        0.53930
H         -0.13420       -0.28480        1.22310
H         -0.41330       -1.41400       -1.59920
H         -0.49000       -2.22510        0.04180
H         -1.81700       -1.19840       -0.53660
H          3.20240        0.65120        0.14200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

