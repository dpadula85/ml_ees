%nproc=16
%mem=2GB
%chk=mol_441_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.93750        1.17510        0.26020
C         -0.10630       -0.08560        0.10230
C         -0.96070       -1.08090       -0.61240
O          1.12380        0.18000       -0.46770
C          2.35130       -0.02370        0.11560
O          2.31690       -0.49740        1.27770
H         -0.52080        1.94340       -0.44910
H         -0.91430        1.53040        1.30390
H         -1.99890        1.02120       -0.01430
H          0.08250       -0.50080        1.13570
H         -0.69630       -2.12300       -0.44770
H         -1.03730       -0.77740       -1.66710
H         -2.00170       -0.95160       -0.19030
H          3.29920        0.19030       -0.34690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

