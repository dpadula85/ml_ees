%nproc=16
%mem=2GB
%chk=mol_441_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38290       -0.79500       -0.18060
C         -0.17380       -0.00840        0.25580
C         -0.41250        1.48550        0.00500
O          0.91870       -0.36370       -0.58260
C          2.12900       -0.77840       -0.05140
O          2.22730       -0.82950        1.19960
H         -2.16180       -0.87900        0.58610
H         -1.09210       -1.84230       -0.45030
H         -1.76580       -0.30300       -1.12140
H          0.05470       -0.10070        1.32350
H          0.49320        2.02270        0.36450
H         -1.34060        1.82700        0.48740
H         -0.41350        1.59930       -1.09880
H          2.92000       -1.03450       -0.73670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_441_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

