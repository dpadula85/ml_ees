%nproc=16
%mem=2GB
%chk=mol_301_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.34320        0.10680       -0.01680
C          0.61120        1.27890       -0.02590
C         -0.76600        1.14170       -0.00820
C         -1.36960       -0.09810        0.01710
C         -0.61920       -1.24580        0.02560
C          0.76610       -1.13450        0.00820
Cl         1.71070       -2.61980        0.01960
Cl        -1.35420       -2.85450        0.05810
Cl        -3.10370       -0.23210        0.03810
Cl        -1.72570        2.62290       -0.01910
Cl         1.39970        2.84560       -0.05830
Cl         3.10740        0.18900       -0.03840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_301_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_301_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_301_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

