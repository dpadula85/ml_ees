%nproc=16
%mem=2GB
%chk=mol_602_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.96150       -0.95580        0.24220
N          0.00070       -0.11340       -0.39410
C          0.36260        1.26720       -0.29050
C         -1.35920       -0.38930       -0.03820
H          0.61710       -1.97450        0.43270
H          1.90140       -0.94090       -0.37440
H          1.25470       -0.47040        1.20890
H          1.14200        1.42640        0.48560
H         -0.47250        1.94410       -0.04890
H          0.85070        1.56480       -1.25750
H         -1.89570       -0.66680       -0.98560
H         -1.49520       -1.20870        0.67490
H         -1.86800        0.51720        0.34490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

