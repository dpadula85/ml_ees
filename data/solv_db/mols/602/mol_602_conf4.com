%nproc=16
%mem=2GB
%chk=mol_602_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.57460        1.27450       -0.00010
N         -0.02870       -0.00940        0.29540
C         -0.79380       -1.12080       -0.15020
C          1.37560       -0.12690        0.17860
H         -0.08150        2.01290        0.68480
H         -1.64970        1.27290        0.30980
H         -0.52560        1.57740       -1.04880
H         -1.82740       -0.77590       -0.40610
H         -0.87310       -1.87030        0.67940
H         -0.39010       -1.65970       -1.01990
H          1.64820       -1.07000       -0.35490
H          1.88120        0.68700       -0.36600
H          1.83940       -0.19170        1.19800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

