%nproc=16
%mem=2GB
%chk=mol_602_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.48030        1.32250       -0.13390
N         -0.04830        0.06450        0.38530
C         -0.85720       -1.05300        0.01080
C          1.33440       -0.22280        0.20180
H         -0.10880        2.15880        0.51080
H         -0.23480        1.49700       -1.19270
H         -1.59830        1.34480       -0.06770
H         -0.29320       -1.70710       -0.70840
H         -1.11280       -1.61630        0.94390
H         -1.77360       -0.74710       -0.52630
H          1.65330       -0.87850        1.06440
H          2.00110        0.65140        0.23670
H          1.51860       -0.81410       -0.72480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

