%nproc=16
%mem=2GB
%chk=mol_602_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.16910       -1.35170       -0.22380
N          0.03870        0.05890       -0.26490
C          1.12580        0.80580        0.26600
C         -1.25580        0.58160       -0.02820
H         -0.35290       -1.78130       -1.12410
H         -0.33130       -1.83830        0.64760
H          1.22680       -1.62870       -0.33120
H          1.17490        0.84330        1.36420
H          1.05620        1.84760       -0.14740
H          2.07010        0.39290       -0.18390
H         -1.99110       -0.26110        0.00560
H         -1.37970        1.12300        0.92950
H         -1.55090        1.20790       -0.90950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

