%nproc=16
%mem=2GB
%chk=mol_602_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.33620       -0.32260        0.24210
N          0.05230       -0.03060       -0.26330
C         -0.96560       -0.99230       -0.00220
C         -0.35210        1.32270       -0.25770
H          1.56530        0.16890        1.22340
H          1.42020       -1.43700        0.36310
H          2.11350       -0.04680       -0.52020
H         -0.74340       -1.92920       -0.57620
H         -1.12300       -1.23040        1.06240
H         -1.92110       -0.59430       -0.43480
H         -0.08780        1.86320        0.66580
H         -1.45070        1.37220       -0.40210
H          0.15630        1.85620       -1.10030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_602_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

