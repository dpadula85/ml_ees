%nproc=16
%mem=2GB
%chk=mol_217_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.64010       -0.78570       -0.25050
O          1.80570        0.34580       -0.42290
C          0.45880        0.17550       -0.13570
C         -0.07610       -1.01820        0.29730
C         -1.42740       -1.15020        0.57450
C         -2.30360       -0.08890        0.42940
C         -1.77270        1.10230       -0.00210
C         -0.43500        1.23910       -0.27820
O          0.10470        2.43960       -0.71390
H          3.70230       -0.55950       -0.43870
H          2.59770       -1.15700        0.81140
H          2.22840       -1.58370       -0.88890
H          0.59580       -1.86400        0.41790
H         -1.84380       -2.08650        0.91360
H         -3.36400       -0.21050        0.65190
H         -2.42790        1.94210       -0.12320
H         -0.48310        3.25990       -0.84190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

