%nproc=16
%mem=2GB
%chk=mol_217_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.54150       -0.94710       -0.05040
O          1.73770        0.19860       -0.25820
C          0.37900        0.13150       -0.08130
C         -0.31040       -0.99910        0.29740
C         -1.69130       -0.99990        0.45930
C         -2.40010        0.14780        0.23990
C         -1.74380        1.30220       -0.14130
C         -0.37680        1.27210       -0.29350
O          0.26930        2.43930       -0.67690
H          2.48570       -1.15590        1.05500
H          2.06170       -1.75430       -0.64570
H          3.58900       -0.75860       -0.36360
H          0.24470       -1.91040        0.47320
H         -2.21420       -1.89130        0.75530
H         -3.47860        0.18910        0.35570
H         -2.34480        2.19950       -0.30730
H          1.25150        2.53670       -0.81770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

