%nproc=16
%mem=2GB
%chk=mol_217_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.12500        1.65940        0.23400
O         -1.49760        0.67760       -0.53330
C         -0.30370        0.06810       -0.20620
C          0.37780        0.39980        0.94550
C          1.57290       -0.20870        1.27510
C          2.09270       -1.17100        0.42780
C          1.41730       -1.51740       -0.73870
C          0.21840       -0.89220       -1.04830
O         -0.44470       -1.25840       -2.23030
H         -3.20240        1.68800       -0.05670
H         -2.08040        1.47290        1.31720
H         -1.68990        2.67760        0.05850
H          0.00690        1.15230        1.64380
H          2.13210        0.02040        2.15980
H          3.02960       -1.66150        0.66730
H          1.79960       -2.26190       -1.41420
H         -1.30360       -0.84520       -2.50120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

