%nproc=16
%mem=2GB
%chk=mol_217_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.63830       -0.46730        0.34260
O          1.64900        0.49240        0.11530
C          0.29610        0.21720        0.00990
C         -0.21280       -1.05990        0.12210
C         -1.55670       -1.34370        0.01870
C         -2.40130       -0.27470       -0.20770
C         -1.89900        1.01110       -0.32180
C         -0.54990        1.28290       -0.21630
O         -0.09050        2.59210       -0.33790
H          2.79260       -0.74130        1.38800
H          2.36190       -1.40960       -0.20370
H          3.62600       -0.12310       -0.07420
H          0.44860       -1.90540        0.30070
H         -1.95620       -2.35170        0.10760
H         -3.46920       -0.49250       -0.29090
H         -2.59370        1.82390       -0.49940
H          0.91670        2.74940       -0.25310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

