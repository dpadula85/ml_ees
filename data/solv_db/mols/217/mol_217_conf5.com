%nproc=16
%mem=2GB
%chk=mol_217_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.59640       -0.73030        0.00610
O          1.70900        0.34050       -0.11540
C          0.31330        0.18290       -0.02850
C         -0.26510       -1.04190        0.18250
C         -1.63070       -1.18430        0.26580
C         -2.42880       -0.05830        0.13200
C         -1.85210        1.18260       -0.08140
C         -0.48100        1.30050       -0.16120
O          0.11750        2.53470       -0.37450
H          3.59760       -0.36460       -0.34930
H          2.24960       -1.54070       -0.68300
H          2.71640       -1.09140        1.04880
H          0.37300       -1.90120        0.28330
H         -2.12220       -2.14630        0.43320
H         -3.50030       -0.18690        0.19980
H         -2.49950        2.03790       -0.18100
H          1.10680        2.66680       -0.43920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_217_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

