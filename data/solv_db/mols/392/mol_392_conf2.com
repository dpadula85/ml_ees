%nproc=16
%mem=2GB
%chk=mol_392_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.74880        0.05380       -0.16320
C         -0.76630       -0.01800       -0.05880
Cl        -1.21250       -1.71670       -0.40950
Cl        -1.34560        0.51650        1.50570
Cl         1.19570        1.74760        0.18670
Cl         1.52690       -1.03080        0.99150
H          1.08000       -0.14190       -1.20150
H         -1.22710        0.58950       -0.85080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

