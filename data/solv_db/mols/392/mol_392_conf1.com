%nproc=16
%mem=2GB
%chk=mol_392_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.74790        0.27690       -0.09700
C          0.67810       -0.26100       -0.22140
Cl         1.85860        1.00560       -0.47610
Cl         1.03560       -1.06350        1.32600
Cl        -0.75590        1.37610        1.29420
Cl        -1.76650       -1.16390        0.20190
H         -1.06460        0.82880       -0.98930
H          0.76260       -0.99890       -1.03840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

