%nproc=16
%mem=2GB
%chk=mol_392_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.69880       -0.31840       -0.02810
C          0.67850        0.22120       -0.24370
Cl         1.00010        1.46210        0.99710
Cl         1.91370       -1.05090       -0.13420
Cl        -0.76350       -1.03170        1.60780
Cl        -1.84260        1.04070       -0.17780
H         -0.99550       -1.05410       -0.79860
H          0.70810        0.73120       -1.22260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

