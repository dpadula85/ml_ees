%nproc=16
%mem=2GB
%chk=mol_392_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76520        0.02340       -0.06000
C          0.69070       -0.10000        0.31060
Cl         1.68180       -0.71570       -1.02360
Cl         1.28950        1.53080        0.69270
Cl        -1.48060       -1.52800       -0.53310
Cl        -0.93400        1.19460       -1.37720
H         -1.30580        0.39120        0.81940
H          0.82360       -0.79630        1.17120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_392_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

