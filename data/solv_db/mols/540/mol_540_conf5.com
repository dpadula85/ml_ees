%nproc=16
%mem=2GB
%chk=mol_540_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.06170       -0.38560       -0.04060
C          0.07410        0.62690       -0.19700
N          1.28500       -0.06650        0.15860
O          2.17410       -0.15950       -0.69690
O          1.44640       -0.60600        1.40250
H         -1.93890       -0.10880       -0.65230
H         -0.68560       -1.40460       -0.32190
H         -1.30270       -0.39500        1.03820
H         -0.11500        1.47920        0.51950
H          0.12420        1.01990       -1.21010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

