%nproc=16
%mem=2GB
%chk=mol_540_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.01760       -0.40610       -0.08540
C          0.07150        0.67430       -0.04300
N          1.30210       -0.04110        0.09870
O          2.16440        0.07710       -0.76110
O          1.48020       -0.83030        1.18090
H         -1.93390       -0.07260        0.44730
H         -1.26680       -0.67860       -1.12480
H         -0.68320       -1.33190        0.42830
H          0.05160        1.28820       -0.96690
H         -0.16830        1.32090        0.82610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

