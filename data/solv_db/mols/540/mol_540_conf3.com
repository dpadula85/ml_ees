%nproc=16
%mem=2GB
%chk=mol_540_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.05860        0.15660        0.11560
C          0.13020       -0.48150       -0.55560
N          1.31840        0.29410       -0.49670
O          2.30980       -0.20520        0.07240
O          1.36030        1.56140       -1.05540
H         -1.49320        0.98740       -0.48770
H         -1.83460       -0.65060        0.20910
H         -0.85040        0.48950        1.14990
H          0.27590       -1.46870       -0.08280
H         -0.15790       -0.68300       -1.60590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

