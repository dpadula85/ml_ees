%nproc=16
%mem=2GB
%chk=mol_540_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15210        0.10010       -0.12440
C          0.22620       -0.52570       -0.05620
N          1.25350        0.39850       -0.43610
O          1.97060        0.10000       -1.41490
O          1.48060        1.58750        0.22620
H         -1.18300        0.86830       -0.90860
H         -1.87240       -0.72490       -0.26340
H         -1.35620        0.53240        0.87300
H          0.22700       -1.39280       -0.74030
H          0.40590       -0.94330        0.97110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

