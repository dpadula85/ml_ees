%nproc=16
%mem=2GB
%chk=mol_540_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.05580       -0.37860       -0.08700
C          0.06780        0.64180       -0.01740
N          1.31080       -0.08220        0.10950
O          2.00380        0.12910        1.11830
O          1.77510       -0.98580       -0.81070
H         -0.67950       -1.40550        0.08950
H         -1.83080       -0.18200        0.71020
H         -1.56750       -0.33970       -1.06750
H          0.06640        1.30700       -0.90490
H         -0.09030        1.29580        0.86010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_540_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

