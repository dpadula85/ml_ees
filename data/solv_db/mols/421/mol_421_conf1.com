%nproc=16
%mem=2GB
%chk=mol_421_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.71200       -0.04970        0.06710
C          0.57600        0.22640       -0.05000
Cl         1.72440       -1.09060       -0.18990
H         -1.41210        0.75130        0.15230
H         -1.06760       -1.07490        0.07790
H          0.89140        1.23760       -0.05730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_421_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_421_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_421_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

