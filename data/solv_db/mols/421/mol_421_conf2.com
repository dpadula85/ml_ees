%nproc=16
%mem=2GB
%chk=mol_421_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.71220       -0.08320       -0.05940
C          0.56260        0.25310        0.01470
Cl         1.78670       -0.94910        0.34820
H         -1.00510       -1.11910        0.08840
H         -1.50560        0.62310       -0.26310
H          0.87350        1.27520       -0.12890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_421_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_421_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_421_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

