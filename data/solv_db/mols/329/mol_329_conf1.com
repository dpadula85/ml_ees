%nproc=16
%mem=2GB
%chk=mol_329_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.54900       -0.01280       -0.02630
S          1.11360       -0.73380       -0.07160
H         -1.30910       -0.77860       -0.24110
H         -0.58040        0.85370       -0.69930
H         -0.71000        0.36720        1.00300
H          2.03500        0.30440        0.03540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

