%nproc=16
%mem=2GB
%chk=mol_329_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.52250       -0.00120        0.00470
S          1.15290        0.64010       -0.08580
H         -0.73910       -0.80180       -0.71520
H         -0.81080       -0.34040        1.02180
H         -1.21360        0.84120       -0.25150
H          2.13300       -0.33800        0.02600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

