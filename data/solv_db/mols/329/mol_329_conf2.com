%nproc=16
%mem=2GB
%chk=mol_329_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.54020        0.00640        0.00520
S          1.11640       -0.67760       -0.08900
H         -0.85900        0.01000        1.05670
H         -0.60680        1.00590       -0.44120
H         -1.21730       -0.65890       -0.56450
H          2.10680        0.31420        0.03280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

