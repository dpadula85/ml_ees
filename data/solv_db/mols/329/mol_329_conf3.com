%nproc=16
%mem=2GB
%chk=mol_329_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.52130        0.00270       -0.00140
S          1.15240       -0.61440       -0.20170
H         -0.74510        0.89530       -0.58900
H         -0.79920        0.18600        1.05510
H         -1.21450       -0.79960       -0.36750
H          2.12770        0.33000        0.10450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_329_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

