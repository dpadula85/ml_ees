%nproc=16
%mem=2GB
%chk=mol_479_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99090       -1.34310       -0.33330
N          2.14240       -0.18900       -0.12150
C          2.55250        1.06770        0.14640
N          1.44450        1.82100        0.26930
C          0.37000        1.03390        0.07870
C          0.80330       -0.24370       -0.16990
C         -0.16920       -1.20200       -0.39250
O          0.23790       -2.37450       -0.62040
N         -1.48180       -0.88690       -0.36480
C         -1.86410        0.37900       -0.11710
O         -3.09650        0.65250       -0.09560
N         -0.93190        1.35200        0.10740
C         -1.36160        2.70440        0.37140
C         -2.43440       -1.94420       -0.60680
H          4.05550       -1.10020       -0.14250
H          2.91770       -1.58570       -1.42150
H          2.65420       -2.18550        0.29890
H          3.57460        1.44150        0.25120
H         -1.95530        2.79890        1.30070
H         -1.94510        3.12600       -0.49480
H         -0.47430        3.35010        0.46750
H         -3.42530       -1.72640       -0.11370
H         -2.58600       -2.03160       -1.69710
H         -2.01780       -2.91440       -0.25860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

