%nproc=16
%mem=2GB
%chk=mol_479_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.14130        0.84490       -0.32050
N          2.12280       -0.14720       -0.02940
C          2.36020       -1.44570        0.24350
N          1.17230       -2.02700        0.45460
C          0.21400       -1.10630        0.31540
C          0.80810        0.10730        0.00430
C         -0.00770        1.20570       -0.18430
O          0.51620        2.30230       -0.46460
N         -1.34280        1.13620       -0.07710
C         -1.86260       -0.07270        0.22780
O         -3.11140       -0.17400        0.33620
N         -1.13470       -1.16900        0.42200
C         -1.70020       -2.46830        0.75020
C         -2.20630        2.29830       -0.27670
H          4.09820        0.51940        0.16850
H          3.31050        0.79500       -1.42660
H          2.88630        1.84460        0.03470
H          3.32740       -1.96350        0.29240
H         -2.77450       -2.40810        1.00430
H         -1.12930       -2.83070        1.64170
H         -1.46390       -3.14950       -0.07300
H         -3.00870        2.34710        0.48640
H         -1.59850        3.22820       -0.13200
H         -2.61650        2.33290       -1.30950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

