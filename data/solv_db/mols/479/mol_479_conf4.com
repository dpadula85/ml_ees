%nproc=16
%mem=2GB
%chk=mol_479_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.11050        1.03000        0.08600
N          2.17450       -0.08110        0.12640
C          2.39420       -1.39030        0.20430
N          1.21860       -2.06650        0.21490
C          0.21550       -1.16140        0.14160
C          0.83910        0.06920        0.08750
C          0.01880        1.17660        0.00720
O          0.55680        2.32420       -0.04420
N         -1.33460        1.06350       -0.01730
C         -1.85230       -0.16210        0.03890
O         -3.11610       -0.26630        0.01590
N         -1.13350       -1.29420        0.11820
C         -1.75080       -2.60900        0.17680
C         -2.13720        2.27090       -0.10270
H          3.04520        1.49730       -0.91380
H          4.14760        0.69220        0.25160
H          2.82210        1.82040        0.83120
H          3.38070       -1.82760        0.25140
H         -1.09250       -3.25560        0.80250
H         -1.81840       -3.01650       -0.83140
H         -2.75770       -2.55290        0.66940
H         -1.55850        3.00490       -0.69980
H         -3.10550        2.06440       -0.59950
H         -2.26650        2.66980        0.92480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

