%nproc=16
%mem=2GB
%chk=mol_479_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.22070       -0.75550       -0.01190
N          2.14220        0.21360        0.02210
C          2.25900        1.55410        0.06930
N          1.03170        2.11950        0.08740
C          0.11550        1.16190        0.05260
C          0.82770       -0.04030        0.01150
C          0.11800       -1.20620       -0.02990
O          0.75670       -2.28730       -0.06670
N         -1.22000       -1.16060       -0.02990
C         -1.87730        0.01130        0.01020
O         -3.15410       -0.03360        0.00710
N         -1.23000        1.18970        0.05210
C         -1.96950        2.43720        0.09480
C         -1.94740       -2.40840       -0.07450
H          4.17540       -0.28460       -0.36850
H          3.02290       -1.58020       -0.72710
H          3.42510       -1.14500        0.98940
H          3.22710        2.05290        0.08800
H         -2.15100        2.73500       -0.97930
H         -1.32050        3.25420        0.51080
H         -2.93350        2.35310        0.61090
H         -1.31460       -3.22330       -0.44860
H         -2.81850       -2.26400       -0.77220
H         -2.38570       -2.69330        0.90220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

