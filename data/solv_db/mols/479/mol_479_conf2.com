%nproc=16
%mem=2GB
%chk=mol_479_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.11840        0.96720       -0.08620
N         -2.15120       -0.13820       -0.10870
C         -2.38870       -1.45530       -0.12580
N         -1.20070       -2.08160       -0.14300
C         -0.19050       -1.17050       -0.13700
C         -0.80170        0.06290       -0.11520
C         -0.03000        1.21990       -0.10400
O         -0.60870        2.32560       -0.08420
N          1.29920        1.10320       -0.11470
C          1.87200       -0.11480       -0.13600
O          3.14550       -0.16590       -0.14540
N          1.14640       -1.24520       -0.14730
C          1.75190       -2.55240       -0.17020
C          2.11720        2.30460       -0.10320
H         -3.02900        1.48040        0.91580
H         -4.14010        0.53740       -0.15820
H         -2.91950        1.64050       -0.92420
H         -3.39170       -1.86420       -0.12460
H          1.21080       -3.16970        0.58260
H          2.83860       -2.52930        0.01210
H          1.52450       -3.01260       -1.15610
H          1.63090        3.12470       -0.68270
H          3.11910        2.09930       -0.53400
H          2.31420        2.63420        0.95740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_479_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

