%nproc=16
%mem=2GB
%chk=mol_271_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.31760       -0.36160        0.02950
C          0.86550       -0.04420       -0.02020
C         -0.09690       -1.02620       -0.09400
C         -1.44780       -0.74740       -0.14080
C         -1.81900        0.58440       -0.11060
C         -0.87710        1.59860       -0.03650
C          0.47600        1.27830        0.00900
Cl         1.64000        2.59880        0.10280
O         -2.40110       -1.75370       -0.21510
H          2.91840        0.42650       -0.50670
H          2.60910       -0.42910        1.10120
H          2.43690       -1.35220       -0.48770
H          0.25030       -2.07020       -0.11540
H         -2.88020        0.82240       -0.14670
H         -1.19750        2.63560       -0.01420
H         -2.79400       -2.15980        0.64550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

