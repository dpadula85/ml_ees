%nproc=16
%mem=2GB
%chk=mol_271_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24770       -0.72750        0.09900
C         -0.86390       -0.17490        0.00100
C          0.24930       -0.99250       -0.09560
C          1.53340       -0.50020       -0.18730
C          1.74440        0.86250       -0.18460
C          0.64750        1.69380       -0.08900
C         -0.65210        1.18980        0.00360
Cl        -2.02860        2.29630        0.12310
O          2.64340       -1.33200       -0.28390
H         -2.92140       -0.32770       -0.67320
H         -2.17890       -1.83690        0.03870
H         -2.65950       -0.48310        1.10060
H          0.07990       -2.06730       -0.09710
H          2.76510        1.25690       -0.25710
H          0.81740        2.75630       -0.08750
H          3.07170       -1.61340        0.58930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

