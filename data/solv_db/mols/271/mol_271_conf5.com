%nproc=16
%mem=2GB
%chk=mol_271_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.27150       -0.65170       -0.08820
C          0.87350       -0.14260       -0.05190
C         -0.21840       -0.97320       -0.29320
C         -1.51170       -0.49930       -0.25900
C         -1.78280        0.82230        0.01650
C         -0.71710        1.65730        0.25730
C          0.56500        1.17260        0.22010
Cl         1.95020        2.23170        0.52780
O         -2.59860       -1.32940       -0.49990
H          2.81300       -0.16130        0.77260
H          2.29600       -1.74190        0.11470
H          2.76810       -0.45970       -1.04990
H         -0.00130       -2.01360       -0.50980
H         -2.80690        1.18410        0.04070
H         -0.91140        2.69350        0.47420
H         -2.98900       -1.78870        0.32810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

