%nproc=16
%mem=2GB
%chk=mol_271_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.26240       -0.53340        0.00060
C          0.85870       -0.07900       -0.02130
C         -0.16180       -0.99560       -0.17530
C         -1.50280       -0.61720       -0.20220
C         -1.76040        0.73310       -0.06580
C         -0.76400        1.67840        0.09010
C          0.55430        1.26060        0.11150
Cl         1.81560        2.48330        0.31180
O         -2.51480       -1.56140       -0.35870
H          2.94960        0.21210        0.42580
H          2.32050       -1.47090        0.60100
H          2.61750       -0.81960       -1.02980
H          0.07390       -2.06060       -0.28120
H         -2.82150        1.01970       -0.08830
H         -1.02550        2.72940        0.19320
H         -2.90170       -1.97910        0.48860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

