%nproc=16
%mem=2GB
%chk=mol_271_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.19010       -0.87200        0.09430
C         -0.84730       -0.23900        0.00880
C          0.31480       -0.99200       -0.18050
C          1.54280       -0.37440       -0.25530
C          1.70140        0.98840       -0.15130
C          0.55640        1.73930        0.03590
C         -0.67360        1.12530        0.11120
Cl        -2.11420        2.10300        0.34920
O          2.68710       -1.13120       -0.44300
H         -2.85020       -0.27870        0.79020
H         -2.69950       -0.91590       -0.88250
H         -2.11500       -1.92360        0.47280
H          0.17480       -2.05160       -0.25960
H          2.68180        1.45570       -0.21350
H          0.67890        2.82270        0.11900
H          3.15200       -1.45590        0.40420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_271_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

