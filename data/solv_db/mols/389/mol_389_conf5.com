%nproc=16
%mem=2GB
%chk=mol_389_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.72730       -0.03340       -0.50390
C         -3.12510        1.08150        0.08390
C         -1.87650        0.96290        0.65800
C         -1.17830       -0.22650        0.68110
C         -1.78340       -1.32050        0.09740
C         -3.04210       -1.22620       -0.48800
O          0.08100       -0.35980        1.25640
C          1.25950       -0.16710        0.56910
C          1.94560       -1.14860       -0.10630
C          3.12070       -0.93330       -0.78570
C          3.64330        0.36420       -0.78550
C          2.99010        1.37950       -0.12220
C          1.80480        1.10010        0.54760
H         -4.70600        0.07050       -0.95090
H         -3.66530        2.00760        0.06750
H         -1.44070        1.84910        1.10450
H         -1.27660       -2.26740        0.09190
H         -3.52330       -2.08780       -0.94990
H          1.53730       -2.14480       -0.10250
H          3.65350       -1.71630       -1.31530
H          4.56550        0.55390       -1.31390
H          3.43950        2.36190       -0.15530
H          1.30400        1.90070        1.06180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

