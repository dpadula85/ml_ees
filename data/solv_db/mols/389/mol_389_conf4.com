%nproc=16
%mem=2GB
%chk=mol_389_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.52750        0.32250        0.81010
C         -3.65000       -0.26410       -0.43350
C         -2.48770       -0.56840       -1.10710
C         -1.22780       -0.31960       -0.60800
C         -1.15760        0.27170        0.64690
C         -2.29280        0.59440        1.35890
O         -0.06890       -0.64020       -1.31630
C          1.16400       -0.35800       -0.74560
C          1.83430       -1.26070        0.05520
C          3.06030       -0.97550        0.62050
C          3.60690        0.26000        0.35770
C          2.94040        1.15550       -0.43750
C          1.71830        0.88000       -1.00510
H         -4.45080        0.56530        1.34750
H         -4.63890       -0.47410       -0.85640
H         -2.56250       -1.02780       -2.08140
H         -0.16470        0.47370        1.05270
H         -2.19610        1.05350        2.33220
H          1.39190       -2.23190        0.25440
H          3.56470       -1.70520        1.24640
H          4.56580        0.50180        0.79110
H          3.36890        2.14340       -0.65240
H          1.20990        1.60360       -1.63040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

