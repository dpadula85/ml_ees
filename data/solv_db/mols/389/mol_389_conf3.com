%nproc=16
%mem=2GB
%chk=mol_389_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.74480       -0.46100       -0.41470
C          2.56990       -0.95380       -0.94430
C          1.32540       -0.52850       -0.50160
C          1.21730        0.41680        0.49930
C          2.39500        0.90690        1.02600
C          3.63540        0.48350        0.58530
O          0.00010        0.88100        0.98320
C         -1.21920        0.46320        0.53280
C         -1.92630       -0.60450        1.05420
C         -3.14760       -1.00190        0.58320
C         -3.69800       -0.30070       -0.46370
C         -3.02810        0.77990       -1.02300
C         -1.81750        1.13500       -0.51930
H          4.70410       -0.80910       -0.77720
H          2.65770       -1.69850       -1.73270
H          0.40140       -0.92850       -0.93120
H          2.30900        1.65470        1.81770
H          4.55190        0.88610        1.01780
H         -1.46210       -1.13250        1.87680
H         -3.71170       -1.84430        0.99010
H         -4.67880       -0.62320       -0.83870
H         -3.52140        1.29040       -1.84920
H         -1.30130        1.98900       -0.97070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

