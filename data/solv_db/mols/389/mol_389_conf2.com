%nproc=16
%mem=2GB
%chk=mol_389_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.68230       -0.04220       -0.90260
C          2.99830       -1.21760       -0.66810
C          1.80240       -1.24270        0.04210
C          1.25980       -0.07490        0.53870
C          1.94100        1.10170        0.30600
C          3.13840        1.12470       -0.40540
O          0.08300       -0.11860        1.23480
C         -1.17960       -0.00090        0.67220
C         -1.84320       -1.13340        0.24660
C         -3.10490       -1.07320       -0.32190
C         -3.74670        0.13770       -0.48270
C         -3.09430        1.27490       -0.06160
C         -1.82640        1.20690        0.50880
H          4.61560       -0.05010       -1.46110
H          3.42340       -2.14430       -1.05890
H          1.28090       -2.18400        0.21430
H          1.50830        2.01360        0.69890
H          3.65700        2.06090       -0.57660
H         -1.33650       -2.09620        0.37280
H         -3.55890       -2.01910       -0.63180
H         -4.73540        0.15500       -0.93140
H         -3.61770        2.21130       -0.19830
H         -1.34670        2.11030        0.82570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

