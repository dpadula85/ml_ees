%nproc=16
%mem=2GB
%chk=mol_389_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.73120       -0.23500        0.90140
C         -2.55670       -0.35590        1.60300
C         -1.33700       -0.19970        1.00060
C         -1.24350        0.08450       -0.33190
C         -2.43840        0.20750       -1.04570
C         -3.66980        0.05140       -0.44480
O         -0.08260        0.25950       -1.02940
C          1.18720        0.19150       -0.56720
C          1.82260        1.30320       -0.08330
C          3.13060        1.21070        0.38790
C          3.79540        0.00560        0.37210
C          3.17130       -1.11380       -0.10920
C          1.86730       -0.99850       -0.57390
H         -4.71280       -0.35000        1.33560
H         -2.64250       -0.58160        2.65990
H         -0.41580       -0.29730        1.56410
H         -2.41270        0.43170       -2.10170
H         -4.59660        0.15060       -1.01640
H          1.34610        2.25130       -0.05340
H          3.65210        2.08170        0.77590
H          4.81590       -0.09940        0.73240
H          3.65570       -2.08860       -0.14210
H          1.39550       -1.90930       -0.95200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_389_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

