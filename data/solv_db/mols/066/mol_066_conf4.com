%nproc=16
%mem=2GB
%chk=mol_066_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.21830        0.10220       -0.01200
C         -0.72740        0.03100       -0.01130
C          0.04630        1.17330        0.01630
C          1.42580        1.12360        0.01710
C          2.09240       -0.08540       -0.01000
C          1.32230       -1.22670       -0.03750
C         -0.06570       -1.17950       -0.03840
H         -2.54100        0.45650        1.00850
H         -2.61900       -0.91800       -0.10650
H         -2.57060        0.80820       -0.77300
H         -0.50410        2.11710        0.03730
H          2.01200        2.03520        0.03910
H          3.17170       -0.17450       -0.01060
H          1.84560       -2.18360       -0.05890
H         -0.67010       -2.07960       -0.06010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

