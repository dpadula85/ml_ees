%nproc=16
%mem=2GB
%chk=mol_066_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21920       -0.15100        0.01230
C          0.72650       -0.02240       -0.00410
C         -0.04990       -1.15220        0.13480
C         -1.44330       -1.05180        0.12210
C         -2.08380        0.16020       -0.02730
C         -1.29290        1.28480       -0.16560
C          0.09100        1.18840       -0.15340
H          2.70570        0.68610       -0.50970
H          2.49490       -0.21090        1.09440
H          2.44730       -1.14380       -0.41650
H          0.44620       -2.11190        0.25320
H         -2.01720       -1.96630        0.23470
H         -3.16560        0.18390       -0.03040
H         -1.78280        2.24100       -0.28350
H          0.70470        2.06580       -0.26110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

