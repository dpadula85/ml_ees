%nproc=16
%mem=2GB
%chk=mol_066_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.14910       -0.26390       -0.10520
C          0.68440       -0.10180        0.09130
C          0.14580        1.15610        0.23810
C         -1.19780        1.36640        0.42220
C         -2.01400        0.25790        0.45700
C         -1.48390       -1.00940        0.31060
C         -0.13120       -1.20740        0.12620
H          2.35950       -0.86770       -1.03160
H          2.62090        0.73670       -0.25570
H          2.56910       -0.81680        0.75080
H          0.80410        2.01000        0.20760
H         -1.58190        2.38370        0.53440
H         -3.06730        0.41140        0.60050
H         -2.15010       -1.87550        0.34160
H          0.29340       -2.17970        0.01160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

