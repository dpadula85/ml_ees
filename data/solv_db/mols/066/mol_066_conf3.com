%nproc=16
%mem=2GB
%chk=mol_066_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.20820        0.03800       -0.00220
C         -0.73000        0.00730        0.00490
C         -0.03770       -1.15850        0.22140
C          1.34360       -1.21440        0.23300
C          2.06790       -0.05770        0.01960
C          1.39360        1.14210       -0.20310
C          0.01290        1.15160       -0.20620
H         -2.50500        0.76500       -0.80930
H         -2.64450        0.43610        0.93340
H         -2.65300       -0.94410       -0.26270
H         -0.58700       -2.07430        0.39030
H          1.88660       -2.14410        0.40570
H          3.16200       -0.09530        0.02760
H          1.99170        2.03990       -0.36870
H         -0.49270        2.10840       -0.38370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

