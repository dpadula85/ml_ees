%nproc=16
%mem=2GB
%chk=mol_066_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.20840       -0.11310       -0.03630
C          0.72320       -0.03020       -0.01560
C          0.06730        1.16260        0.21380
C         -1.32010        1.22940        0.23130
C         -2.09300        0.10610        0.01990
C         -1.44490       -1.08740       -0.20970
C         -0.05580       -1.14890       -0.22620
H          2.65230        0.42600        0.84870
H          2.65350        0.35480       -0.92200
H          2.53590       -1.15860        0.06360
H          0.66100        2.06030        0.38270
H         -1.85770        2.15100        0.40870
H         -3.17840        0.11940        0.02620
H         -2.02610       -1.99070       -0.37970
H          0.47450       -2.08070       -0.40540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_066_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

