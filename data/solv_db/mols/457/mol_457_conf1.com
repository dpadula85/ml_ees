%nproc=16
%mem=2GB
%chk=mol_457_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.99720       -0.95340        0.15490
C         -0.10950       -0.03620       -0.37220
C          0.40560        1.36350       -0.29270
C         -1.31860       -0.28780        0.48870
Cl        -0.38830       -0.49150       -2.06060
H          0.54520       -1.94120        0.38920
H          1.31760       -0.51840        1.12770
H          1.79370       -0.99320       -0.59670
H         -0.21800        1.98320       -0.99140
H          1.47680        1.33880       -0.62930
H          0.39020        1.78650        0.72380
H         -1.02430       -0.48820        1.54760
H         -2.09260        0.48000        0.41450
H         -1.77500       -1.24200        0.09640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

