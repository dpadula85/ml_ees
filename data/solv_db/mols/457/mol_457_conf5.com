%nproc=16
%mem=2GB
%chk=mol_457_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.02710       -1.00740        0.11230
C         -0.04070       -0.01240       -0.36040
C         -1.40860       -0.40320        0.10230
C          0.38900        1.35130        0.13810
Cl        -0.00760       -0.09700       -2.14490
H          0.77700       -2.03500       -0.15700
H          1.97840       -0.64250       -0.37190
H          1.13620       -0.82490        1.19740
H         -1.99190        0.50470        0.41930
H         -2.03050       -0.85980       -0.71370
H         -1.37350       -1.07640        0.97100
H          0.48780        2.01430       -0.73560
H          1.37910        1.32760        0.64290
H         -0.32190        1.76070        0.90020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

