%nproc=16
%mem=2GB
%chk=mol_457_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.72480       -1.17110        0.31800
C          0.05360        0.12840        0.36290
C          1.45390       -0.03280       -0.16220
C         -0.70450        1.13390       -0.50500
Cl         0.14420        0.75670        2.01190
H         -0.09520       -2.02670       -0.01710
H         -1.60630       -1.10880       -0.36580
H         -1.14980       -1.39730        1.32190
H          2.12000       -0.36770        0.65610
H          1.83710        0.93350       -0.57230
H          1.41050       -0.80950       -0.96330
H         -0.41830        2.17150       -0.28400
H         -1.78880        0.96630       -0.25390
H         -0.53170        0.82360       -1.54730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

