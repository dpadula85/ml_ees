%nproc=16
%mem=2GB
%chk=mol_457_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.46170       -0.06500       -0.13740
C          0.03920       -0.00700        0.30490
C         -0.65450        1.22160       -0.26520
C         -0.77770       -1.20130       -0.05320
Cl        -0.03540        0.17950        2.09320
H          2.13580        0.12050        0.74380
H          1.74950       -1.05590       -0.53870
H          1.71010        0.76930       -0.84910
H         -1.69580        1.21080        0.14850
H         -0.13540        2.09320        0.22520
H         -0.62300        1.26470       -1.35710
H         -1.68300       -1.18920        0.62380
H         -1.20280       -1.17000       -1.08180
H         -0.28860       -2.17140        0.14300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

