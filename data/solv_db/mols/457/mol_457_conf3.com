%nproc=16
%mem=2GB
%chk=mol_457_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.32170       -0.56870        0.18330
C          0.07060       -0.00360        0.33280
C          0.17040        1.43250       -0.07640
C          1.05400       -0.84620       -0.45430
Cl         0.54360       -0.09920        2.05810
H         -2.08120        0.21380       -0.03320
H         -1.63270       -1.09480        1.12290
H         -1.38210       -1.34630       -0.61840
H         -0.74690        1.84460       -0.52140
H          0.43270        2.11460        0.77940
H          1.01700        1.59640       -0.80350
H          0.57350       -1.80900       -0.73400
H          1.35590       -0.35170       -1.39110
H          1.94690       -1.08250        0.15580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_457_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

