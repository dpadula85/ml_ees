%nproc=16
%mem=2GB
%chk=mol_037_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.34360        0.00840       -0.00620
O          0.94460       -0.27140       -0.38850
H         -0.71900        0.98930       -0.36290
H         -0.99980       -0.77040       -0.48810
H         -0.52960       -0.05440        1.07400
H          1.64740        0.09850        0.17160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

