%nproc=16
%mem=2GB
%chk=mol_037_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.35110        0.02080       -0.03420
O          0.97330        0.19110       -0.36370
H         -0.80530       -0.72770       -0.69510
H         -0.89670        0.97350       -0.19170
H         -0.53320       -0.28820        1.00360
H          1.61310       -0.16950        0.28120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

