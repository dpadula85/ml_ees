%nproc=16
%mem=2GB
%chk=mol_037_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.38370        0.00090        0.00680
O          0.92930       -0.14850        0.48720
H         -1.02830       -0.28250        0.88420
H         -0.54950        1.04460       -0.30870
H         -0.54820       -0.68340       -0.84250
H          1.58030        0.06890       -0.22710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

