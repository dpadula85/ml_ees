%nproc=16
%mem=2GB
%chk=mol_037_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.35280        0.01050       -0.04480
O          0.97650       -0.03250       -0.41530
H         -0.51440       -0.00080        1.04600
H         -0.82900        0.93720       -0.45140
H         -0.88950       -0.86660       -0.47970
H          1.60920       -0.04780        0.34510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_037_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

