%nproc=16
%mem=2GB
%chk=mol_185_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.04330        0.50610        0.11200
C          2.57280        0.32380        0.08050
C          1.90840       -0.21250        1.16190
C          0.54240       -0.38240        1.13470
O         -0.00780       -0.86520        2.12030
N         -0.19430       -0.04260        0.08040
C          0.47160        0.48470       -0.98110
O         -0.11570        0.84380       -2.04090
N          1.80620        0.65870       -0.97370
C         -1.60130       -0.16820       -0.05550
C         -2.26680        1.09950       -0.59560
C         -2.34950       -0.52850        1.19860
C         -1.93970       -1.31900       -1.01890
Cl         2.86430       -0.66990        2.57180
H          4.46190        0.54960       -0.92390
H          4.56390       -0.33700        0.61620
H          4.32460        1.43290        0.66830
H          2.32010        1.06900       -1.79970
H         -3.22560        1.32890       -0.05670
H         -2.54530        0.93070       -1.66740
H         -1.64070        1.98600       -0.48300
H         -3.44600       -0.41860        0.92250
H         -2.22000        0.19010        2.02590
H         -2.26360       -1.59110        1.48720
H         -3.04270       -1.47280       -1.00100
H         -1.53840       -1.16830       -2.00940
H         -1.48220       -2.22770       -0.57350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

