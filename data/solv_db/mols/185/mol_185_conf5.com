%nproc=16
%mem=2GB
%chk=mol_185_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.03850        0.41190        0.32730
C          2.56440        0.27390        0.21960
C          1.97270       -0.31230       -0.87810
C          0.59520       -0.45550       -0.99890
O          0.15000       -0.98960       -2.05480
N         -0.18350       -0.00470        0.00780
C          0.37930        0.58230        1.08650
O         -0.35960        1.03630        2.02380
N          1.70760        0.70080        1.16430
C         -1.60130       -0.15130       -0.07450
C         -2.04080       -1.33580       -0.90940
C         -2.15500        1.08630       -0.77480
C         -2.30510       -0.33630        1.25740
Cl         3.00890       -0.89340       -2.16010
H          4.56150       -0.55210        0.05570
H          4.45060        1.19690       -0.32450
H          4.33360        0.60240        1.38010
H          2.14260        1.15140        2.00390
H         -2.23590       -1.08760       -1.97140
H         -3.07490       -1.67880       -0.55890
H         -1.42380       -2.24100       -0.74630
H         -1.54860        1.23070       -1.67870
H         -3.22990        0.98990       -0.97520
H         -2.03260        1.97500       -0.10880
H         -1.76130       -0.91230        1.99740
H         -3.26390       -0.93630        1.10540
H         -2.68860        0.64910        1.58500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

