%nproc=16
%mem=2GB
%chk=mol_185_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.06000        0.34560       -0.26010
C          2.57660        0.22750       -0.21140
C          1.91890       -0.12750        0.94700
C          0.54000       -0.23560        0.98650
O          0.04890       -0.56750        2.09710
N         -0.21310        0.00470       -0.11480
C          0.45050        0.34600       -1.22250
O         -0.17910        0.57560       -2.25690
N          1.80200        0.45900       -1.28780
C         -1.62310       -0.12880        0.01950
C         -2.04430        0.69270        1.24630
C         -2.45150        0.34840       -1.12080
C         -2.00220       -1.57660        0.31150
Cl         2.89170       -0.43660        2.36820
H          4.48130       -0.47300        0.35590
H          4.34170        1.34540        0.08360
H          4.38170        0.15770       -1.30210
H          2.27520        0.73160       -2.18730
H         -1.28880        1.42280        1.53790
H         -2.34000        0.02380        2.09260
H         -2.98000        1.28180        1.03750
H         -2.45260       -0.31590       -2.00870
H         -3.52430        0.30510       -0.75860
H         -2.30180        1.41930       -1.35810
H         -1.14160       -2.17430        0.65700
H         -2.87540       -1.62680        1.01010
H         -2.35050       -2.02450       -0.66150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

