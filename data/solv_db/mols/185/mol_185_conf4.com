%nproc=16
%mem=2GB
%chk=mol_185_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.08730        0.21320       -0.17970
C          2.60360        0.14490       -0.11480
C          1.91740       -0.81750        0.58770
C          0.53530       -0.83660        0.61560
O          0.02070       -1.76370        1.29310
N         -0.20200        0.09510       -0.05100
C          0.49980        1.01530       -0.72220
O         -0.12450        1.89140       -1.34700
N          1.84800        1.05700       -0.76600
C         -1.61490       -0.00620        0.03310
C         -2.01920       -1.45870       -0.25920
C         -2.39970        0.85210       -0.89590
C         -2.13830        0.28830        1.44600
Cl         2.86630       -2.02110        1.44820
H          4.49020       -0.81980       -0.09770
H          4.41840        0.84660        0.69000
H          4.42360        0.69530       -1.12430
H          2.31220        1.81170       -1.31570
H         -1.22060       -1.99290       -0.81860
H         -2.91310       -1.50090       -0.93920
H         -2.34380       -1.98740        0.65830
H         -2.45810        1.92810       -0.59070
H         -3.47610        0.50860       -0.79480
H         -2.18230        0.70940       -1.96820
H         -1.42980       -0.00410        2.22280
H         -3.12280       -0.22730        1.50570
H         -2.37770        1.37910        1.46500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

