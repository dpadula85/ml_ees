%nproc=16
%mem=2GB
%chk=mol_185_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.04130       -0.26500        0.41970
C          2.56750       -0.17290        0.25790
C          2.00930        1.02980       -0.14790
C          0.63990        1.11970       -0.29930
O          0.17310        2.20290       -0.66240
N         -0.19590        0.09870       -0.07410
C          0.38270       -1.06380        0.32100
O         -0.27830       -2.10920        0.56760
N          1.72070       -1.17730        0.47670
C         -1.60520        0.12060       -0.20680
C         -2.24660        1.43880       -0.39730
C         -2.29980       -0.56330        0.97860
C         -1.96000       -0.72670       -1.43820
Cl         3.06240        2.39210       -0.45410
H          4.38110        0.46920        1.18100
H          4.37690       -1.28400        0.59510
H          4.47480        0.10440       -0.55300
H          2.15940       -2.09410        0.78700
H         -2.04720        2.18810        0.38980
H         -3.36560        1.26300       -0.32640
H         -2.13450        1.86390       -1.43700
H         -1.65430       -0.54940        1.88440
H         -2.67360       -1.56550        0.71510
H         -3.22070        0.01400        1.28740
H         -1.96730       -1.80470       -1.19990
H         -3.00680       -0.45760       -1.69060
H         -1.33340       -0.47150       -2.30020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_185_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

