%nproc=16
%mem=2GB
%chk=mol_072_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.55150       -0.37020       -0.21550
C          1.12360       -0.55920        0.23430
C          0.21700        0.39260       -0.50430
C         -1.19810        0.27080       -0.12090
C         -2.09500       -0.04860       -1.03890
C         -1.70210        0.48760        1.26230
H          2.79670       -0.97290       -1.12410
H          3.24750       -0.71850        0.58040
H          2.79120        0.68250       -0.42450
H          0.85250       -1.61520        0.03480
H          1.07880       -0.40890        1.32960
H          0.51610        1.44160       -0.37940
H          0.31790        0.15870       -1.58150
H         -3.13650       -0.14560       -0.79230
H         -1.75010       -0.21110       -2.05180
H         -2.36100       -0.38430        1.50990
H         -2.33080        1.40130        1.27380
H         -0.91930        0.59950        2.00800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

