%nproc=16
%mem=2GB
%chk=mol_072_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.50830       -0.56590        0.03710
C          1.03530       -0.53800        0.34690
C          0.30980        0.48390       -0.51070
C         -1.13860        0.51650       -0.21130
C         -1.69310        1.62420        0.21660
C         -1.95730       -0.70390       -0.39690
H          3.09030       -0.53490        0.97750
H          2.80270       -1.42630       -0.60010
H          2.75770        0.35430       -0.53070
H          0.84710       -0.31500        1.41830
H          0.63160       -1.55780        0.16920
H          0.81080        1.44100       -0.38860
H          0.44400        0.17180       -1.56930
H         -2.74420        1.65210        0.43510
H         -1.10180        2.50790        0.35120
H         -1.91970       -1.36050        0.51910
H         -3.03890       -0.48340       -0.53930
H         -1.64400       -1.26620       -1.30650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

