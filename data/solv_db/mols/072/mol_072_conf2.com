%nproc=16
%mem=2GB
%chk=mol_072_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.49730        0.42530       -0.34040
C          1.03610        0.29830       -0.78610
C          0.27860       -0.21050        0.41610
C         -1.15390       -0.39710        0.17950
C         -1.71220       -1.57420        0.31720
C         -1.98980        0.76430       -0.22280
H          3.15030        0.24270       -1.22750
H          2.68500        1.44900        0.04400
H          2.71440       -0.35290        0.43250
H          0.70950        1.30530       -1.10820
H          1.03140       -0.42100       -1.62070
H          0.76370       -1.18590        0.69530
H          0.50170        0.47120        1.26810
H         -2.76040       -1.71170        0.14430
H         -1.11820       -2.41780        0.60720
H         -3.03050        0.68850        0.18680
H         -1.57960        1.73410        0.16480
H         -2.02330        0.89230       -1.33620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

