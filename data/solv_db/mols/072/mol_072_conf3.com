%nproc=16
%mem=2GB
%chk=mol_072_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.57570       -0.25170        0.20360
C          1.26830       -0.15710       -0.56110
C          0.16490        0.02140        0.47420
C         -1.16310        0.12530       -0.17030
C         -1.33840        0.06760       -1.47040
C         -2.34660        0.30390        0.70910
H          3.05630       -1.22620       -0.06160
H          3.22190        0.61820       -0.04750
H          2.42400       -0.23470        1.30360
H          1.04030       -1.07460       -1.11840
H          1.26470        0.68510       -1.28300
H          0.16130       -0.83650        1.16510
H          0.39530        0.97800        1.00240
H         -2.31730        0.14700       -1.89760
H         -0.48230       -0.06120       -2.10790
H         -2.65350       -0.73200        1.02470
H         -2.08870        0.89970        1.61750
H         -3.18300        0.72780        0.14860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

