%nproc=16
%mem=2GB
%chk=mol_072_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53960        0.07510       -0.28260
C          1.18770       -0.01980        0.43420
C          0.13760        0.18030       -0.66460
C         -1.22480        0.11930       -0.15120
C         -2.04410        1.14540       -0.27260
C         -1.71600       -1.10490        0.52040
H          2.85010        1.13600       -0.31250
H          3.31410       -0.50200        0.24540
H          2.44980       -0.30730       -1.33050
H          1.13350        0.77840        1.19780
H          1.08610       -0.98730        0.92810
H          0.39250        1.15130       -1.13650
H          0.35280       -0.64320       -1.40590
H         -3.06510        1.16030        0.08770
H         -1.71960        2.07740       -0.76610
H         -0.90590       -1.80140        0.81460
H         -2.38010       -0.84490        1.38140
H         -2.38830       -1.61270       -0.23030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_072_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

