%nproc=16
%mem=2GB
%chk=mol_178_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.11030       -0.07450        0.01480
C          0.23110        0.53710       -0.19660
C          1.28690       -0.19930        0.11100
H         -1.10300       -1.06430       -0.50540
H         -1.92950        0.51130       -0.41110
H         -1.20660       -0.26880        1.10520
H          0.33600        1.53130       -0.58160
H          1.22840       -1.20450        0.50110
H          2.26710        0.23160       -0.03730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

