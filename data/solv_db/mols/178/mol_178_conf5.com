%nproc=16
%mem=2GB
%chk=mol_178_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.04330       -0.09400       -0.20310
C          0.16930       -0.22910        0.62540
C          1.32000        0.30710        0.22250
H         -1.95020       -0.37860        0.39800
H         -1.21100        0.92310       -0.59520
H         -1.04780       -0.80820       -1.06580
H          0.14130       -0.76950        1.57880
H          1.40300        0.84150       -0.69390
H          2.21870        0.20770        0.83520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

