%nproc=16
%mem=2GB
%chk=mol_178_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.09240        0.15830       -0.06740
C         -0.20260       -0.37840        0.45060
C         -1.30440       -0.01560       -0.18580
H          1.07020        1.25280        0.06130
H          1.96330       -0.21780        0.49400
H          1.19280       -0.03230       -1.15680
H         -0.27470       -1.02860        1.29480
H         -1.29500        0.64290       -1.05000
H         -2.24200       -0.38120        0.15930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

