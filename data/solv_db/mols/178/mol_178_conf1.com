%nproc=16
%mem=2GB
%chk=mol_178_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.10090        0.04840       -0.03740
C          0.28330        0.30500        0.40280
C          1.30870       -0.26570       -0.19600
H         -1.60330        1.02000       -0.21590
H         -1.67840       -0.51740        0.73570
H         -1.15280       -0.51450       -0.97810
H          0.50650        0.96380        1.22280
H          1.09330       -0.93670       -1.03180
H          2.34360       -0.10290        0.09800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

