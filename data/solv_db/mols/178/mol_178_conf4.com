%nproc=16
%mem=2GB
%chk=mol_178_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.04320       -0.18920       -0.28740
C          0.15840       -0.07410        0.57190
C          1.28300        0.30380        0.01540
H         -1.13730        0.73330       -0.91050
H         -0.89730       -1.10300       -0.91380
H         -1.98360       -0.29640        0.31410
H          0.14640       -0.28710        1.63050
H          2.17470        0.39580        0.61990
H          1.29870        0.51700       -1.04010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_178_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

