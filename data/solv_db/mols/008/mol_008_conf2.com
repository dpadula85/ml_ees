%nproc=16
%mem=2GB
%chk=mol_008_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.56680        1.02140        0.10150
C          1.27060        0.34880        0.59560
C          1.66400       -1.06660        0.91010
C          0.28930        0.50170       -0.50790
C         -1.06510       -0.05120       -0.31220
C         -1.18020       -1.51020       -0.06600
C         -1.88520        0.32760       -1.55160
C         -1.75200        0.66660        0.84830
H          3.24750        1.15310        0.95390
H          2.33110        1.96900       -0.42620
H          3.00330        0.34410       -0.66390
H          0.91420        0.82290        1.52890
H          0.99020       -1.60500        1.57300
H          1.91910       -1.59950       -0.03480
H          2.63970       -0.99460        1.47790
H          0.17100        1.59000       -0.75180
H          0.72400        0.04180       -1.43380
H         -2.13850       -1.85260       -0.54660
H         -0.37620       -2.14140       -0.43750
H         -1.33030       -1.71160        1.01600
H         -2.66050       -0.44260       -1.75500
H         -2.35960        1.31980       -1.42760
H         -1.21610        0.40790       -2.45190
H         -1.44730        1.71440        0.92800
H         -1.46020        0.12510        1.76490
H         -2.85940        0.62110        0.66880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

