%nproc=16
%mem=2GB
%chk=mol_008_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.50030        0.28490       -1.18540
C          1.37590       -0.31620       -0.35190
C          1.82430       -0.38960        1.04860
C          0.14180        0.43890       -0.64520
C         -1.10430        0.03450        0.05490
C         -1.54270       -1.37450       -0.19850
C         -1.08610        0.36440        1.53640
C         -2.22960        0.92660       -0.50830
H          3.34100       -0.41330       -1.22850
H          2.14540        0.54180       -2.20050
H          2.87070        1.21050       -0.66350
H          1.26730       -1.37270       -0.73990
H          1.47150       -1.35500        1.50470
H          1.61520        0.48160        1.67840
H          2.95000       -0.50390        1.05730
H          0.31380        1.51510       -0.43200
H         -0.03620        0.37320       -1.74060
H         -1.26960       -2.04510        0.66710
H         -2.66150       -1.44800       -0.24850
H         -1.17180       -1.76570       -1.15510
H         -2.13950        0.49700        1.92420
H         -0.61700        1.36220        1.65720
H         -0.64220       -0.42170        2.15180
H         -2.38430        0.57440       -1.54420
H         -3.13380        0.86120        0.11250
H         -1.79850        1.93920       -0.55080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

