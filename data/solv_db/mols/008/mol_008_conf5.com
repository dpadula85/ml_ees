%nproc=16
%mem=2GB
%chk=mol_008_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.56110       -1.13030        0.02790
C          1.42670       -0.14460        0.41530
C          1.85440        1.18430       -0.13920
C          0.19440       -0.68930       -0.16060
C         -1.09870       -0.03360        0.01750
C         -1.20830        1.36080       -0.53190
C         -1.62380        0.01030        1.42780
C         -2.13760       -0.85800       -0.77180
H          2.48300       -1.28410       -1.05070
H          3.54470       -0.68070        0.26910
H          2.42150       -2.05150        0.62500
H          1.46970       -0.08910        1.51450
H          1.65370        2.04040        0.55880
H          1.38410        1.44630       -1.10070
H          2.97850        1.21890       -0.26310
H          0.11400       -1.80140        0.07810
H          0.37810       -0.73670       -1.28740
H         -0.91100        1.41170       -1.61790
H         -0.64240        2.09260        0.06880
H         -2.29090        1.68230       -0.55920
H         -1.41460       -0.87360        2.01920
H         -1.25680        0.96670        1.88020
H         -2.72640        0.12550        1.35870
H         -3.15680       -0.71850       -0.31760
H         -2.15470       -0.53680       -1.82920
H         -1.84200       -1.91160       -0.63170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

