%nproc=16
%mem=2GB
%chk=mol_008_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.79840        1.21100       -0.42130
C          1.44950       -0.23140       -0.30730
C          2.59070       -0.84290        0.54500
C          0.20350       -0.53480        0.40040
C         -1.10260       -0.07930       -0.02660
C         -1.25050        1.41420       -0.07350
C         -1.62050       -0.60670       -1.33660
C         -2.14650       -0.54170        1.01790
H          1.19750        1.83640       -1.06480
H          1.76370        1.64900        0.61790
H          2.89680        1.24340       -0.71720
H          1.57380       -0.78130       -1.27810
H          2.29840       -0.81970        1.62970
H          2.75080       -1.89420        0.29190
H          3.50830       -0.26260        0.45110
H          0.17440       -1.67380        0.51860
H          0.38170       -0.23520        1.49000
H         -1.13610        1.75940       -1.14210
H         -0.55950        1.98950        0.53440
H         -2.32770        1.70130        0.17770
H         -0.85720       -0.84540       -2.06860
H         -2.31300        0.13090       -1.83770
H         -2.24060       -1.51950       -1.19360
H         -1.90550       -1.61110        1.19190
H         -1.97080       -0.02440        1.98080
H         -3.15700       -0.43080        0.62000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

