%nproc=16
%mem=2GB
%chk=mol_008_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.85530       -1.16750        0.59100
C          1.55040        0.28940        0.30130
C          1.99600        0.57520       -1.08800
C          0.20810        0.71910        0.69550
C         -0.99320        0.09640        0.13000
C         -2.19910        0.80890        0.77790
C         -1.21490       -1.35290        0.38230
C         -1.14690        0.38270       -1.36540
H          1.61710       -1.80890       -0.30840
H          2.96730       -1.32320        0.71590
H          1.40170       -1.54060        1.50730
H          2.26880        0.85420        0.97650
H          1.54280        1.41750       -1.58480
H          1.86160       -0.33030       -1.73190
H          3.11300        0.75430       -1.10950
H          0.14190        0.61520        1.82170
H          0.10820        1.83350        0.54830
H         -2.20400        0.65650        1.86850
H         -3.14230        0.46570        0.33020
H         -2.06450        1.88390        0.53440
H         -0.76390       -1.97580       -0.43660
H         -2.32400       -1.55090        0.25880
H         -0.97910       -1.69460        1.38350
H         -2.21750        0.30720       -1.67200
H         -0.52800       -0.34800       -1.92480
H         -0.85470        1.43320       -1.60170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_008_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

