%nproc=16
%mem=2GB
%chk=mol_121_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.99970       -0.17840        0.02150
C          0.25280        0.68300        0.10940
N          1.39760       -0.18830       -0.13720
H         -1.29570       -0.46510        1.05190
H         -0.77880       -1.14370       -0.50160
H         -1.80480        0.30410       -0.55410
H          0.37610        1.04860        1.15600
H          0.22410        1.55130       -0.54910
H          1.38600       -0.59070       -1.10030
H          1.24240       -1.02090        0.50340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

