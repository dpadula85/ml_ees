%nproc=16
%mem=2GB
%chk=mol_121_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.08740       -0.09780        0.08250
C          0.28710        0.55550        0.07490
N          1.24620       -0.52380        0.03580
H         -1.87260        0.60870       -0.21510
H         -1.10780       -1.01820       -0.54960
H         -1.22440       -0.47540        1.13600
H          0.34280        1.14490        1.02540
H          0.42370        1.25440       -0.75350
H          2.15220       -0.16470       -0.29590
H          0.84010       -1.28360       -0.54040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

