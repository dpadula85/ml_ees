%nproc=16
%mem=2GB
%chk=mol_121_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.03050        0.32160        0.19620
C         -0.16360       -0.56470       -0.07520
N         -1.41640        0.09750        0.05310
H          1.09760        0.57850        1.26240
H          1.92870       -0.30440       -0.08030
H          1.07780        1.20480       -0.45390
H         -0.10840       -1.39080        0.68420
H         -0.04270       -0.94700       -1.11260
H         -2.03480       -0.09720       -0.77680
H         -1.36870        1.10160        0.30290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

