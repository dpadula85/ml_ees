%nproc=16
%mem=2GB
%chk=mol_121_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.10420       -0.29040       -0.14670
C         -0.12400        0.50040        0.20380
N         -1.28350       -0.38260        0.17310
H          1.86250        0.31250       -0.68150
H          0.78250       -1.12910       -0.81170
H          1.53510       -0.68300        0.79690
H         -0.09110        0.91050        1.24640
H         -0.23900        1.35640       -0.50540
H         -2.09770        0.20140        0.48580
H         -1.44900       -0.79600       -0.76080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

