%nproc=16
%mem=2GB
%chk=mol_121_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.10520       -0.30890       -0.24000
C         -0.12440        0.44100        0.17580
N         -1.33330       -0.34140        0.07630
H          1.92070        0.44170       -0.36770
H          0.97970       -0.91860       -1.14540
H          1.45170       -0.98970        0.57010
H         -0.20730        1.41150       -0.34160
H          0.00260        0.67490        1.27240
H         -2.00040       -0.11320        0.83890
H         -1.79440       -0.29720       -0.83890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_121_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

