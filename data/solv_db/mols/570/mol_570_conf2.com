%nproc=16
%mem=2GB
%chk=mol_570_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.12460        0.44760        0.29560
C          1.14160       -0.03680       -0.72570
C          0.48400       -1.33400       -0.37450
C         -0.70300       -0.93380        0.46430
C         -0.76180        0.55880        0.23320
O          0.06920        0.84360       -0.83310
C         -2.19320        0.95180       -0.08670
H          1.63240        1.03370        1.10830
H          2.81540        1.17540       -0.20650
H          2.76280       -0.35800        0.70350
H          1.68040       -0.17300       -1.67760
H          0.12100       -1.78780       -1.33180
H          1.13540       -2.06580        0.09880
H         -0.55950       -1.13790        1.54110
H         -1.65030       -1.41440        0.12950
H         -0.45900        1.10110        1.16240
H         -2.44690        0.72340       -1.15270
H         -2.91200        0.37500        0.52380
H         -2.28110        2.03120        0.12810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

