%nproc=16
%mem=2GB
%chk=mol_570_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24230        0.91680       -0.15120
C          0.85690        0.41630       -0.41660
C          0.79720       -1.10190       -0.28060
C         -0.70530       -1.31620       -0.15000
C         -1.17290        0.02090        0.35380
O         -0.05290        0.80160        0.57550
C         -2.08820        0.67890       -0.64060
H          2.96350        0.32230       -0.71970
H          2.28430        1.99370       -0.38000
H          2.46360        0.77130        0.92000
H          0.52460        0.72390       -1.41880
H          1.32410       -1.41420        0.63740
H          1.17910       -1.57130       -1.19900
H         -1.10100       -1.57740       -1.14170
H         -0.85040       -2.12700        0.57530
H         -1.69630       -0.12950        1.30950
H         -2.73360        1.38250       -0.08010
H         -2.69660       -0.07780       -1.16890
H         -1.53850        1.28710       -1.38780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

