%nproc=16
%mem=2GB
%chk=mol_570_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.31500        0.34630       -0.11880
C          1.03830       -0.26900       -0.68320
C          0.50250       -1.36130        0.21190
C         -0.98700       -0.98880        0.21550
C         -0.83230        0.51040        0.26340
O          0.09410        0.73270       -0.73180
C         -2.11670        1.24310        0.01620
H          2.69990        1.03260       -0.89840
H          3.06010       -0.41570        0.14640
H          2.03760        0.92330        0.77320
H          1.25420       -0.67200       -1.71560
H          0.68100       -2.36780       -0.14980
H          0.86020       -1.18610        1.26160
H         -1.52510       -1.41440        1.05090
H         -1.36990       -1.27170       -0.78390
H         -0.43460        0.82590        1.25170
H         -2.52700        1.03910       -0.98410
H         -1.89910        2.32450        0.07400
H         -2.85130        0.96890        0.80070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

