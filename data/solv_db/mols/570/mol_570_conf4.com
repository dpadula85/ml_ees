%nproc=16
%mem=2GB
%chk=mol_570_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.37900        0.58930        0.33880
C          0.98210        0.34620       -0.05180
C          0.72380       -1.09000       -0.52980
C         -0.79200       -0.93000       -0.70750
C         -1.09490       -0.18650        0.58490
O          0.06580        0.44560        0.96970
C         -2.21630        0.78690        0.33280
H          2.40820        1.17550        1.28300
H          2.98050       -0.33570        0.40110
H          2.85660        1.24960       -0.43120
H          0.61110        0.99580       -0.88610
H          1.23360       -1.32230       -1.45500
H          0.94730       -1.73970        0.32640
H         -0.92370       -0.21510       -1.52740
H         -1.30920       -1.87180       -0.79900
H         -1.40640       -0.93020        1.36720
H         -1.79700        1.75290       -0.04340
H         -2.86980        0.32600       -0.45150
H         -2.77890        0.95350        1.27890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

