%nproc=16
%mem=2GB
%chk=mol_570_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32920        0.81410        0.13950
C         -0.94380        0.29370        0.31270
C         -0.80420       -1.14960       -0.14880
C          0.72800       -1.25040       -0.10840
C          1.09640        0.13370       -0.59040
O         -0.01080        0.93230       -0.47650
C          2.23020        0.74270        0.17490
H         -2.53530        0.89200       -0.94530
H         -3.04150        0.16170        0.66600
H         -2.38630        1.80780        0.61920
H         -0.59940        0.31310        1.37300
H         -1.28510       -1.86540        0.51540
H         -1.09250       -1.24370       -1.21860
H          1.10790       -2.05240       -0.73170
H          0.98920       -1.33320        0.97080
H          1.35560        0.01490       -1.68260
H          2.90260       -0.00310        0.60880
H          1.81320        1.35570        1.00220
H          2.80510        1.43600       -0.48030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_570_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

