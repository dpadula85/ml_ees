%nproc=16
%mem=2GB
%chk=mol_206_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.43190       -1.67090       -0.58350
O         -1.15240       -1.34940       -0.48430
C         -0.67160       -0.15390       -0.06930
C          0.84330       -0.23110       -0.18820
C          1.48640       -1.27850       -0.78300
C          2.88510       -1.38230       -0.73050
C          3.62880       -0.43350       -0.08080
C          2.98920        0.61920        0.51790
C          1.61470        0.69430        0.44990
O         -0.96040        0.23210        1.15210
C         -2.09560        0.46440        1.76220
O         -1.13930        0.75350       -1.06450
C         -0.78300        2.04710       -0.87120
H         -3.02300       -1.66810        0.32660
H         -2.47570       -2.76110       -0.93990
H         -2.97480       -1.14690       -1.42730
H          0.94300       -2.04740       -1.30010
H          3.38330       -2.21990       -1.20760
H          4.73010       -0.53340       -0.05010
H          3.57960        1.38170        1.03940
H          1.16500        1.55680        0.95630
H         -1.83310        0.87930        2.81480
H         -2.67140       -0.44550        2.06470
H         -2.75830        1.23230        1.34360
H         -1.13830        2.49050        0.09990
H         -1.39580        2.64820       -1.62140
H          0.25630        2.32230       -1.12570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

