%nproc=16
%mem=2GB
%chk=mol_206_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.59840       -1.95600        0.22110
O         -1.61950       -0.62430        0.51550
C         -0.80900        0.20290       -0.19530
C          0.63890        0.03920        0.00320
C          1.25210       -0.67100        0.99730
C          2.61220       -0.83860        1.02940
C          3.44140       -0.29860        0.06900
C          2.81420        0.42080       -0.92530
C          1.45430        0.60340       -0.98540
O         -1.06610        0.15730       -1.57350
C         -2.37350        0.53300       -1.86190
O         -1.11510        1.51980        0.26440
C         -0.86480        1.60410        1.61870
H         -0.61430       -2.38990        0.35480
H         -2.38460       -2.47120        0.84950
H         -1.96840       -2.14920       -0.82840
H          0.61800       -1.02430        1.81530
H          3.01120       -1.42620        1.86050
H          4.50920       -0.45480        0.13140
H          3.48680        0.84930       -1.68470
H          1.05210        1.17350       -1.78810
H         -2.48220        0.46660       -2.96590
H         -3.13110       -0.06130       -1.33560
H         -2.45650        1.62050       -1.62140
H         -1.15050        2.66120        1.91240
H          0.22440        1.57060        1.88240
H         -1.48100        0.94310        2.24060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

