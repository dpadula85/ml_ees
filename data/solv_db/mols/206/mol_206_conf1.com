%nproc=16
%mem=2GB
%chk=mol_206_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11570       -1.79070        1.28680
O         -0.88120       -1.46320        0.79090
C         -0.73860       -0.10290        0.50000
C          0.64130        0.08970       -0.01140
C          1.56210       -0.92580        0.17770
C          2.80350       -0.81320       -0.37210
C          3.21150        0.27070       -1.12200
C          2.26600        1.26850       -1.29370
C          1.00650        1.19200       -0.75710
O         -1.71860        0.18010       -0.46170
C         -1.59120       -0.55350       -1.61100
O         -0.91910        0.61520        1.65270
C         -0.82250        1.96720        1.58770
H         -2.94020       -1.55420        0.59780
H         -2.13510       -2.88990        1.43740
H         -2.33840       -1.34130        2.27350
H          1.27110       -1.80610        0.77040
H          3.50230       -1.63030       -0.20280
H          4.21170        0.33140       -1.54780
H          2.57190        2.14320       -1.89000
H          0.28220        1.97870       -0.93420
H         -0.63450       -0.37950       -2.10450
H         -1.69310       -1.66740       -1.44120
H         -2.46290       -0.31650       -2.27650
H         -1.04170        2.34720        2.64350
H          0.19890        2.37960        1.41520
H         -1.49620        2.47100        0.89250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

