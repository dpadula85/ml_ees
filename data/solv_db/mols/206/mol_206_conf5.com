%nproc=16
%mem=2GB
%chk=mol_206_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.44550       -1.64540       -1.19320
O         -1.59360       -0.76920       -0.17420
C         -0.79290        0.31360       -0.09090
C          0.65290        0.06940        0.11400
C          1.18180       -1.15350        0.48270
C          2.54120       -1.36720        0.56650
C          3.43520       -0.35420        0.28630
C          2.94090        0.87410       -0.07840
C          1.59840        1.05790       -0.15550
O         -0.93780        1.09620       -1.23730
C         -2.22120        1.55250       -1.42880
O         -1.27510        1.09300        1.00230
C         -1.23560        0.38810        2.18260
H         -0.50150       -2.15690       -1.32500
H         -1.69390       -1.15610       -2.16610
H         -2.21540       -2.46240       -1.04870
H          0.53650       -1.96200        0.78420
H          2.91670       -2.35840        0.86040
H          4.51500       -0.48360        0.34100
H          3.66070        1.67710       -0.29840
H          1.22510        2.03050       -0.44360
H         -2.20930        2.15720       -2.36670
H         -2.55980        2.25210       -0.63670
H         -2.97600        0.76650       -1.55580
H         -0.18500        0.09830        2.39310
H         -1.80530       -0.56330        2.14760
H         -1.55650        1.00580        3.03880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

