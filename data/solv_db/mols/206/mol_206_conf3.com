%nproc=16
%mem=2GB
%chk=mol_206_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.65870        1.83810       -1.18890
O         -1.65780        1.03100       -0.15670
C         -1.11610       -0.14560        0.02910
C          0.37490       -0.16070        0.21150
C          0.95800       -1.39640        0.32520
C          2.31650       -1.54080        0.47600
C          3.12290       -0.42270        0.51490
C          2.52500        0.81400        0.40270
C          1.16940        0.94270        0.25640
O         -1.41120       -1.23970       -0.73400
C         -1.19380       -1.28080       -2.05200
O         -1.56560       -0.58420        1.34020
C         -1.19100        0.25780        2.33290
H         -2.15400        1.43310       -2.08940
H         -0.68660        2.23810       -1.55620
H         -2.26060        2.78530       -0.97060
H          0.34380       -2.31080        0.29790
H          2.74560       -2.53530        0.56370
H          4.21180       -0.53190        0.63270
H          3.19020        1.69230        0.43630
H          0.72480        1.92510        0.27410
H         -1.49470       -2.34880       -2.36880
H         -1.84060       -0.67490       -2.71060
H         -0.12170       -1.23360       -2.36880
H         -0.10510        0.40150        2.47740
H         -1.53450       -0.20150        3.30690
H         -1.69100        1.24880        2.31800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_206_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

