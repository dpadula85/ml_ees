%nproc=16
%mem=2GB
%chk=mol_252_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.23970        1.31430       -0.01520
C          1.26920        0.39230       -0.00890
C          0.90370       -0.93810        0.00700
O          1.83380       -1.78140        0.01270
N         -0.40530       -1.32430        0.01590
C         -1.36660       -0.38280        0.00910
O         -2.55760       -0.76830        0.01770
N         -1.05370        0.91620       -0.00620
Br         3.10730        0.93160       -0.02150
H          0.51870        2.37220       -0.02770
H         -0.66790       -2.33760        0.02790
H         -1.82130        1.60590       -0.01090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_252_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_252_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_252_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

