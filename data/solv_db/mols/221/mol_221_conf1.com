%nproc=16
%mem=2GB
%chk=mol_221_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.04560        1.29750        0.73820
C         -2.36170       -0.00390        0.36950
C         -1.68800        0.08810       -0.98240
C         -1.02100       -1.20390       -1.33040
C          0.02120       -1.54980       -0.30880
O          1.03000       -0.56800       -0.20940
C          2.05980       -0.75830        0.69760
O          2.06470       -1.79780        1.40350
C          3.17970        0.18970        0.90400
C          3.09330        1.40060        0.02130
H         -2.57230        1.77510        1.59920
H         -4.12740        1.14630        0.92590
H         -2.96380        1.97220       -0.13200
H         -3.06350       -0.84500        0.39990
H         -1.55740       -0.17680        1.11870
H         -0.94340        0.89660       -0.98150
H         -2.42870        0.35940       -1.78340
H         -0.61070       -1.22390       -2.35470
H         -1.77160       -2.04900       -1.30110
H          0.53290       -2.48130       -0.68970
H         -0.44980       -1.81460        0.64410
H          3.18080        0.46200        1.99000
H          4.12270       -0.33330        0.69870
H          4.01150        1.51810       -0.62170
H          2.23240        1.40410       -0.65940
H          3.07590        2.29590        0.67670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

