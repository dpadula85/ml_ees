%nproc=16
%mem=2GB
%chk=mol_221_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.39700        0.35740        0.42830
C          2.23140        0.77670       -0.42400
C          1.70400       -0.39850       -1.15860
C          1.21280       -1.53100       -0.32910
C          0.09200       -1.20510        0.60290
O         -1.04350       -0.73090       -0.13080
C         -2.21700       -0.35500        0.50310
O         -2.26050       -0.44060        1.75470
C         -3.42280        0.14280       -0.19710
C         -3.43170        1.65800       -0.11420
H          4.22820        0.04070       -0.26880
H          3.19450       -0.49380        1.08090
H          3.82040        1.23530        0.99150
H          1.47220        1.35580        0.13270
H          2.63930        1.48720       -1.18940
H          0.90870       -0.03940       -1.85560
H          2.52520       -0.76470       -1.82910
H          0.86670       -2.33850       -1.00840
H          2.05160       -1.96250        0.25750
H          0.31580       -0.47650        1.37340
H         -0.25420       -2.12960        1.10490
H         -3.48240       -0.17250       -1.24840
H         -4.36640       -0.19220        0.29910
H         -2.43280        2.06620       -0.45480
H         -4.18970        2.10270       -0.77470
H         -3.55860        2.00800        0.92460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

