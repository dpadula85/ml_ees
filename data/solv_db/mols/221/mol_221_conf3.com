%nproc=16
%mem=2GB
%chk=mol_221_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.37230        0.40000        0.74500
C          2.91180        0.28020        1.15530
C          2.12930        0.03880       -0.09620
C          0.62900       -0.10870        0.16200
C         -0.00700       -0.33360       -1.16380
O         -1.38010       -0.51630       -1.17200
C         -2.34870        0.35770       -0.71880
O         -1.93250        1.44650       -0.23640
C         -3.80170        0.05760       -0.79110
C         -4.41920       -0.37340        0.48930
H          5.07020        0.01640        1.51750
H          4.57010        1.42620        0.41320
H          4.52700       -0.25860       -0.14490
H          2.54400        1.25100        1.58580
H          2.75550       -0.48350        1.92580
H          2.36190        0.80120       -0.83910
H          2.46220       -0.93980       -0.51270
H          0.47990       -0.93370        0.88240
H          0.33290        0.87380        0.62430
H          0.47650       -1.23570       -1.61580
H          0.32840        0.50480       -1.83610
H         -4.02320       -0.69410       -1.58610
H         -4.29670        1.00370       -1.14840
H         -3.83460       -1.22300        0.90690
H         -4.43970        0.50310        1.18860
H         -5.46760       -0.68070        0.26530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

