%nproc=16
%mem=2GB
%chk=mol_221_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.37250        0.09180        0.47020
C         -3.03390        0.28520       -0.25700
C         -2.04490       -0.68850        0.33180
C         -0.67930       -0.58640       -0.31700
C         -0.09030        0.78710       -0.14970
O          1.16740        0.93090       -0.73520
C          2.31820        0.26330       -0.44020
O          2.28050       -0.59320        0.46830
C          3.61240        0.51100       -1.15240
C          4.64400       -0.41110       -0.54950
H         -4.45450       -1.01640        0.61480
H         -5.20220        0.41620       -0.16750
H         -4.28870        0.62880        1.42200
H         -3.25390        0.06160       -1.32100
H         -2.70800        1.33590       -0.20650
H         -1.99610       -0.60090        1.43450
H         -2.42750       -1.71180        0.11950
H          0.00320       -1.31200        0.16170
H         -0.76890       -0.88310       -1.37680
H         -0.08880        1.04060        0.94550
H         -0.74150        1.56750       -0.64780
H          3.93430        1.56360       -1.04850
H          3.49200        0.20730       -2.21360
H          4.46930       -0.48630        0.55540
H          4.56500       -1.41790       -1.00310
H          5.66480        0.01710       -0.67250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

