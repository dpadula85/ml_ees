%nproc=16
%mem=2GB
%chk=mol_221_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.76310       -0.30490       -0.47600
C         -2.91990       -0.73150        0.68220
C         -1.44690       -0.62960        0.21390
C         -1.21780        0.78080       -0.15540
C          0.08980        1.16990       -0.69720
O          1.25740        0.98840       -0.00590
C          1.94850       -0.12850        0.30820
O          1.51840       -1.24370       -0.08090
C          3.23920       -0.08230        1.12070
C          4.40530       -0.14780        0.12010
H         -4.77950       -0.77650       -0.38120
H         -3.95930        0.78600       -0.45380
H         -3.31110       -0.54210       -1.46180
H         -3.11290       -1.75500        1.03150
H         -2.98710       -0.03340        1.54790
H         -1.31590       -1.32850       -0.60810
H         -0.88690       -0.90170        1.15520
H         -1.53740        1.44770        0.72100
H         -1.99760        1.03100       -0.95720
H          0.13780        0.72390       -1.74500
H          0.04940        2.30210       -0.90750
H          3.23910        0.85790        1.67000
H          3.33430       -0.97050        1.74880
H          4.34550       -1.12170       -0.41190
H          4.33070        0.71930       -0.59920
H          5.33990       -0.10920        0.71130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_221_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

