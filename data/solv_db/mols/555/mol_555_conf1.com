%nproc=16
%mem=2GB
%chk=mol_555_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.44470       -0.32930       -0.02420
S          0.01200        0.48880       -0.76440
O          0.10170        1.98340       -0.67790
C         -1.43460       -0.06640        0.14300
H          1.22020       -1.43130       -0.08370
H          2.37260       -0.11990       -0.56150
H          1.51210        0.01450        1.02320
H         -1.87640       -0.90580       -0.44380
H         -2.15720        0.77200        0.22780
H         -1.19500       -0.40610        1.16150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

