%nproc=16
%mem=2GB
%chk=mol_555_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.42620        0.26130        0.07580
S          0.05980        0.06030       -0.92810
O          0.03200       -1.37940       -1.48780
C          1.46970        0.06840        0.21590
H         -2.18130        0.82790       -0.53420
H         -1.23990        0.81790        1.00300
H         -1.92100       -0.72370        0.26690
H          2.40280       -0.24860       -0.31240
H          1.58140        1.03480        0.72170
H          1.22260       -0.71890        0.97930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

