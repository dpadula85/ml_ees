%nproc=16
%mem=2GB
%chk=mol_555_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.44260        0.08750       -0.14450
S         -0.00760       -0.00510        0.88510
O         -0.03410       -1.24700        1.73980
C         -1.45540        0.15650       -0.14900
H          2.11680        0.89360        0.21170
H          1.21120        0.34010       -1.20780
H          1.92730       -0.90240       -0.15830
H         -2.36270       -0.30280        0.29980
H         -1.22950       -0.27460       -1.15290
H         -1.60860        1.25410       -0.32390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

