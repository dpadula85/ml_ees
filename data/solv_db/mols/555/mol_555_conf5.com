%nproc=16
%mem=2GB
%chk=mol_555_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45370        0.11390       -0.27840
S         -0.05330        0.14460        0.83870
O         -0.18370       -0.86090        1.95420
C          1.47480        0.01090       -0.06730
H         -1.49080        1.12330       -0.76410
H         -1.24900       -0.59830       -1.10860
H         -2.40630       -0.06120        0.23330
H          2.19990        0.72820        0.39250
H          1.28640        0.42340       -1.09550
H          1.87570       -1.02390       -0.10480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

