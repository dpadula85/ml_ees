%nproc=16
%mem=2GB
%chk=mol_555_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.50400        0.07550       -0.15360
S          0.01180       -0.83750        0.07380
O          0.05620       -1.36870        1.50800
C          1.47300        0.15020       -0.18290
H         -1.29650        1.04010       -0.65640
H         -1.99090        0.26930        0.83830
H         -2.25880       -0.47490       -0.75480
H          1.24840        1.21440       -0.42570
H          2.13260       -0.26980       -0.97750
H          2.12820        0.20140        0.73080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_555_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

