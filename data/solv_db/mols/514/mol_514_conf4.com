%nproc=16
%mem=2GB
%chk=mol_514_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.62970       -1.14660        0.02850
C         -1.17910        0.26350       -0.16540
C          0.31720        0.44150       -0.00140
C          0.62680        1.90950       -0.22420
O          1.03240       -0.34150       -0.93100
N          1.89800       -1.34710       -0.57770
O          1.45910       -2.36110       -0.00660
O          3.23870       -1.24100       -0.84930
H         -1.51680       -1.76010       -0.88770
H         -2.73720       -1.11210        0.23010
H         -1.19410       -1.64410        0.92040
H         -1.70370        0.92910        0.52650
H         -1.39900        0.53850       -1.23560
H          0.58200        0.09860        1.00870
H          1.60980        2.00280       -0.75840
H          0.75060        2.41760        0.75290
H         -0.15500        2.35240       -0.84950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

