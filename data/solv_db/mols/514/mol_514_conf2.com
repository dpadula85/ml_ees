%nproc=16
%mem=2GB
%chk=mol_514_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.78630        0.58180       -0.41050
C          1.03530        0.31030        0.85760
C         -0.33790       -0.27280        0.61250
C         -0.25640       -1.58150       -0.13070
O         -1.16030        0.61970       -0.08600
N         -2.30990        1.10580        0.45440
O         -2.25050        2.07330        1.21350
O         -3.53600        0.55570        0.18330
H          2.71990        1.09600       -0.15460
H          2.01320       -0.33980       -0.91840
H          1.18560        1.25580       -1.03140
H          1.63890       -0.39630        1.44980
H          0.94000        1.25290        1.40750
H         -0.77190       -0.49260        1.62040
H         -0.70780       -2.42380        0.44360
H         -0.77960       -1.48320       -1.12370
H          0.79140       -1.86110       -0.32320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

