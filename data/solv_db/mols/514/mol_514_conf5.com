%nproc=16
%mem=2GB
%chk=mol_514_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.79890        0.74900        0.03200
C         -0.83900        0.15810       -0.97170
C          0.37870       -0.35800       -0.23500
C         -0.01580       -1.41710        0.76010
O          1.04340        0.66320        0.45870
N          2.34620        0.99420        0.19470
O          2.99490        1.69980        0.96700
O          2.97470        0.54210       -0.95500
H         -2.77780        0.25340       -0.08530
H         -1.44580        0.59260        1.07280
H         -1.87340        1.85530       -0.11280
H         -1.30210       -0.69360       -1.50280
H         -0.53540        0.94040       -1.68930
H          1.06730       -0.80050       -0.97450
H          0.89660       -2.02880        0.99670
H         -0.35850       -1.00700        1.72180
H         -0.75500       -2.14300        0.32260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

