%nproc=16
%mem=2GB
%chk=mol_514_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.10050        0.02070       -0.18790
C         -0.92110       -0.01690        0.74910
C          0.37440        0.01680       -0.02060
C          0.46740       -1.16890       -0.94670
O          1.49830        0.15230        0.78340
N          2.37580        1.18660        0.78650
O          2.71640        1.81530        1.79640
O          2.92660        1.56130       -0.42990
H         -2.70360        0.93020        0.05820
H         -2.75970       -0.87200       -0.09160
H         -1.72310        0.11350       -1.21440
H         -0.96430        0.79490        1.48600
H         -0.99290       -0.99520        1.30370
H          0.32630        0.93560       -0.67670
H          1.07490       -0.92660       -1.84700
H         -0.54420       -1.55140       -1.20000
H          0.94900       -1.99640       -0.34850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

