%nproc=16
%mem=2GB
%chk=mol_514_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.96800       -0.21330        0.07410
C          0.86010        0.01200       -0.92480
C         -0.47270        0.23460       -0.23020
C         -0.44290        1.42750        0.67380
O         -0.86250       -0.91250        0.48550
N         -2.00990       -1.59440        0.16070
O         -2.54930       -1.50110       -0.97870
O         -2.60330       -2.41060        1.09820
H          2.39850       -1.23020        0.00330
H          2.75100        0.56040       -0.04470
H          1.52800       -0.11710        1.08760
H          1.09230        0.91480       -1.53260
H          0.79700       -0.90200       -1.54920
H         -1.22150        0.43380       -1.02300
H         -1.18200        2.21260        0.38220
H          0.55360        1.91070        0.56950
H         -0.60450        1.17470        1.74830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_514_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

