%nproc=16
%mem=2GB
%chk=mol_362_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.34860        0.24360       -0.83840
C          1.78990       -0.85720       -0.39610
C          0.44240       -0.89290        0.18720
C         -0.22020        0.45880        0.22120
C         -1.56970        0.27060        0.83330
C         -2.66600        0.57830        0.15500
H          1.85520        1.22420       -0.80840
H          3.34450        0.25340       -1.26650
H          2.34380       -1.78530       -0.46170
H          0.48070       -1.27850        1.22360
H         -0.19500       -1.58350       -0.40810
H          0.31610        1.17940        0.84640
H         -0.34850        0.89730       -0.78550
H         -1.66620       -0.12040        1.84060
H         -3.63930        0.44070        0.60140
H         -2.61630        0.97160       -0.85330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

