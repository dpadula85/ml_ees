%nproc=16
%mem=2GB
%chk=mol_362_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.36490        0.58280        0.14870
C         -1.91680       -0.59480        0.49420
C         -0.46860       -0.89130        0.60700
C          0.31250        0.35730        0.26480
C          1.78180        0.12120        0.36060
C          2.60050        0.26850       -0.66220
H         -1.67000        1.36970       -0.06300
H         -3.41420        0.77280        0.07410
H         -2.61130       -1.39160        0.70870
H         -0.28770       -1.15880        1.68250
H         -0.21140       -1.71920       -0.07160
H          0.09560        0.63410       -0.76970
H          0.07830        1.18110        0.96940
H          2.18730       -0.18330        1.31130
H          3.65040        0.08330       -0.53040
H          2.23850        0.56820       -1.61780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

