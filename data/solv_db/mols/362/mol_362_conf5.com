%nproc=16
%mem=2GB
%chk=mol_362_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11470        1.05100       -0.03930
C          1.63920       -0.17760       -0.14970
C          0.54710       -0.65140        0.74190
C         -0.67660       -1.06350       -0.05280
C         -1.14360        0.13300       -0.81960
C         -2.33090        0.66150       -0.67430
H          1.72820        1.75750        0.70250
H          2.89830        1.38960       -0.67960
H          2.05840       -0.83090       -0.89940
H          0.22780        0.15050        1.43960
H          0.89940       -1.48850        1.38710
H         -0.39830       -1.82500       -0.81420
H         -1.44650       -1.46090        0.61300
H         -0.44710        0.56060       -1.51940
H         -3.06560        0.26880        0.01560
H         -2.60440        1.52510       -1.25960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

