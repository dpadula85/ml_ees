%nproc=16
%mem=2GB
%chk=mol_362_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.74050        0.10780        0.14640
C          1.49060        0.49560        0.00360
C          0.55190       -0.27940       -0.81910
C         -0.65040       -0.78080       -0.01010
C         -1.42250        0.31600        0.58080
C         -2.69470        0.50010        0.28740
H          3.16310       -0.77940       -0.31300
H          3.41350        0.69380        0.75730
H          1.15840        1.39130        0.49720
H          0.11920        0.28710       -1.66650
H          1.04180       -1.19770       -1.22240
H         -0.27260       -1.48930        0.75720
H         -1.24630       -1.38690       -0.72060
H         -0.95090        0.99570        1.27260
H         -3.27830        1.31070        0.71970
H         -3.16330       -0.18470       -0.40860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

