%nproc=16
%mem=2GB
%chk=mol_362_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.02590       -0.66380        0.85230
C          1.49120        0.44320        0.37250
C          0.76990        0.42240       -0.91080
C         -0.67800        0.87210       -0.67200
C         -1.26130       -0.10180        0.29610
C         -2.31210       -0.84590        0.01950
H          1.92250       -1.57510        0.28720
H          2.56680       -0.71540        1.78630
H          1.57550        1.38210        0.91640
H          0.75210       -0.61180       -1.30710
H          1.26570        1.07650       -1.65880
H         -1.19670        0.89650       -1.63050
H         -0.69980        1.87080       -0.21480
H         -0.76020       -0.16420        1.26290
H         -2.77970       -0.75570       -0.94600
H         -2.68190       -1.52990        0.76560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_362_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

