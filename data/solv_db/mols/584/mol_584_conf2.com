%nproc=16
%mem=2GB
%chk=mol_584_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07390        0.35370        0.06030
C         -0.59920        0.11700        0.00950
C         -0.07110       -1.11660        0.33180
N          1.26420       -1.36130        0.29490
C          2.12330       -0.39060       -0.06320
C          1.59060        0.84100       -0.38450
N          0.26390        1.08530       -0.34820
H         -2.34060        0.94990       -0.83140
H         -2.30650        0.98130        0.96900
H         -2.56170       -0.61730        0.11310
H         -0.78020       -1.88360        0.61720
H          3.19110       -0.57050       -0.09750
H          2.30040        1.61180       -0.67110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

