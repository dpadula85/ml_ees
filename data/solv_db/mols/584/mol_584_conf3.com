%nproc=16
%mem=2GB
%chk=mol_584_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05590       -0.38320       -0.14460
C          0.58190       -0.13640       -0.06370
C          0.10330        0.99430        0.57330
N         -1.21580        1.25860        0.67130
C         -2.11420        0.41160        0.14100
C         -1.62710       -0.71580       -0.49380
N         -0.31050       -0.98060       -0.59210
H          2.23860       -1.47250        0.03760
H          2.59550        0.14380        0.66870
H          2.47150       -0.03130       -1.09980
H          0.77990        1.70440        1.01520
H         -3.18990        0.60760        0.21010
H         -2.36910       -1.40030       -0.92330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

