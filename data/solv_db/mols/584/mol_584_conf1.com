%nproc=16
%mem=2GB
%chk=mol_584_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.05120       -0.12400        0.40960
C         -0.57600       -0.05770        0.13350
C         -0.12570        0.74980       -0.89180
N          1.19000        0.83850       -1.17300
C          2.09640        0.13960       -0.45850
C          1.63170       -0.67020        0.57150
N          0.31560       -0.76460        0.85970
H         -2.15670       -0.57250        1.41510
H         -2.43510        0.91610        0.48510
H         -2.56260       -0.72010       -0.37430
H         -0.88230        1.30680       -1.45700
H          3.16620        0.18520       -0.65570
H          2.38970       -1.22690        1.13570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

