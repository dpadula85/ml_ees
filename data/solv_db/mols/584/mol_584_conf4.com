%nproc=16
%mem=2GB
%chk=mol_584_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05680        0.03340        0.36720
C          0.59280        0.01230        0.11780
C          0.04880       -0.78480       -0.87060
N         -1.28150       -0.80350       -1.09680
C         -2.13960       -0.05650       -0.38120
C         -1.59240        0.73730        0.60390
N         -0.26700        0.76030        0.83430
H          2.59790       -0.34190       -0.51560
H          2.35630        1.10200        0.49430
H          2.32310       -0.55530        1.24990
H          0.73580       -1.39600       -1.45860
H         -3.20990       -0.06220       -0.55240
H         -2.22120        1.35480        1.20800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_584_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

