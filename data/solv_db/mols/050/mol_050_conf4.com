%nproc=16
%mem=2GB
%chk=mol_050_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.57560        0.36550        0.44260
C          3.91010       -0.71520       -0.35000
O          2.49960       -0.68940       -0.24830
C          1.73760        0.39660       -0.63810
O          2.37480        1.40090       -1.10560
C          0.25410        0.43330       -0.53520
C         -0.19570       -0.89000        0.04970
C         -1.67280       -0.94990        0.19220
O         -2.15170       -2.01050        0.67060
O         -2.50580        0.09890       -0.17680
C         -3.89520       -0.08820        0.01160
C         -4.69110        1.09950       -0.41790
H          4.72890        1.26330       -0.22360
H          5.57160        0.04350        0.86210
H          3.89910        0.68860        1.26110
H          4.22730       -1.71020        0.07160
H          4.23510       -0.73960       -1.42070
H         -0.22240        0.50000       -1.55180
H         -0.10740        1.29400        0.06140
H          0.34330       -1.06920        0.98200
H          0.10650       -1.67980       -0.67810
H         -4.07200       -0.27750        1.10710
H         -4.27250       -0.98080       -0.51960
H         -5.69440        0.75590       -0.74420
H         -4.79860        1.77230        0.46600
H         -4.18420        1.68780       -1.20740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

