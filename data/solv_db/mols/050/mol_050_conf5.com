%nproc=16
%mem=2GB
%chk=mol_050_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.16060       -0.14930        1.27950
C          3.68880        1.08020        0.56140
O          2.85630        0.80890       -0.53390
C          1.65130        0.15570       -0.48180
O          1.22990       -0.24560        0.62980
C          0.85160       -0.07930       -1.70140
C         -0.45790       -0.74540       -1.48310
C         -1.38140        0.01350       -0.63650
O         -1.10280        1.13970       -0.18410
O         -2.62530       -0.52720       -0.31400
C         -3.49520        0.20400        0.49060
C         -4.79300       -0.56650        0.71450
H          3.52890       -1.00980        1.00580
H          4.21330        0.01960        2.37890
H          5.19450       -0.43660        0.93110
H          3.21400        1.78130        1.29100
H          4.60810        1.58270        0.14120
H          1.48620       -0.68280       -2.38020
H          0.66350        0.88660       -2.21290
H         -0.36140       -1.81200       -1.16630
H         -0.94370       -0.81690       -2.50110
H         -3.06270        0.43750        1.47710
H         -3.78880        1.16010       -0.02330
H         -5.44160        0.08350        1.33590
H         -4.63180       -1.49060        1.27770
H         -5.26110       -0.79120       -0.26900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

