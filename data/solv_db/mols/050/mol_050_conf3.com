%nproc=16
%mem=2GB
%chk=mol_050_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.89150       -0.40580        0.42270
C         -3.99840        0.66330       -0.09820
O         -2.64540        0.52890        0.25400
C         -1.85170       -0.56660       -0.08530
O         -2.40390       -1.47070       -0.74330
C         -0.43740       -0.58900        0.34350
C          0.48710       -0.19700       -0.81630
C          1.90800       -0.22580       -0.36530
O          2.50530       -1.33080       -0.25540
O          2.61090        0.92290       -0.05600
C          3.95850        0.87630        0.37200
C          4.89100        0.26290       -0.62420
H         -5.87900        0.05570        0.66690
H         -4.54400       -0.89980        1.34870
H         -5.13420       -1.15800       -0.38120
H         -4.14640        0.72550       -1.21230
H         -4.33300        1.64190        0.31090
H         -0.19480       -1.58010        0.72810
H         -0.27940        0.15690        1.14250
H          0.37490       -0.88300       -1.67000
H          0.21260        0.85020       -1.05870
H          4.29660        1.91270        0.67010
H          4.04910        0.29190        1.33470
H          5.60460       -0.43740       -0.12010
H          5.46990        1.10470       -1.08130
H          4.37040       -0.24970       -1.43970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

