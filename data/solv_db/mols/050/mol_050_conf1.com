%nproc=16
%mem=2GB
%chk=mol_050_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.58640        0.62580       -0.62180
C          3.98640       -0.57370        0.08380
O          2.63610       -0.66850       -0.31240
C          1.75460       -1.62670        0.16590
O          2.14330       -2.45720        0.99440
C          0.35110       -1.61660       -0.31280
C         -0.37550       -0.40890        0.23710
C         -1.78520       -0.36450       -0.21320
O         -2.21940       -1.30550       -0.92140
O         -2.63890        0.66590        0.10140
C         -3.97350        0.78100       -0.29400
C         -4.47780        2.10230        0.29980
H          3.77870        1.41500       -0.60670
H          4.73640        0.33830       -1.68260
H          5.49120        0.94760       -0.10740
H          4.53670       -1.50610       -0.15880
H          4.01330       -0.43960        1.17590
H         -0.18340       -2.50390        0.10480
H          0.25460       -1.69530       -1.40430
H          0.13020        0.47050       -0.26360
H         -0.22650       -0.28790        1.30400
H         -4.64260       -0.00730        0.13520
H         -4.13670        0.77130       -1.37240
H         -4.50060        2.05550        1.41390
H         -5.46450        2.36570       -0.12610
H         -3.77430        2.92260        0.03870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

