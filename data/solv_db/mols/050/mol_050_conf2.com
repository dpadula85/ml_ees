%nproc=16
%mem=2GB
%chk=mol_050_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.16960        1.17740       -0.06540
C         -4.14520       -0.04230        0.84580
O         -2.82150       -0.47990        0.94140
C         -2.07730       -0.88380       -0.14010
O         -2.58910       -0.86480       -1.27310
C         -0.67070       -1.33120        0.07840
C          0.22510       -0.19000       -0.28490
C          1.66380       -0.51770       -0.10610
O          1.93290       -1.67410        0.31020
O          2.66350        0.40230       -0.38300
C          3.99600       -0.02610       -0.17490
C          4.99470        1.03080       -0.50180
H         -4.46880        0.79860       -1.06830
H         -3.17440        1.64380       -0.06530
H         -4.94020        1.87290        0.31580
H         -4.86070       -0.80930        0.50250
H         -4.46500        0.31040        1.87070
H         -0.56110       -1.65240        1.11910
H         -0.50990       -2.23020       -0.55760
H         -0.01830        0.20490       -1.27410
H          0.01640        0.62550        0.46870
H          4.07670       -0.29190        0.90070
H          4.16240       -0.95010       -0.76340
H          4.48260        2.00160       -0.65160
H          5.69640        1.16470        0.35020
H          5.56140        0.71080       -1.41220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_050_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

