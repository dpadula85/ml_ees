%nproc=16
%mem=2GB
%chk=mol_223_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95610       -1.35800        0.12620
O          0.74660       -1.23350       -0.48880
C          0.04360       -0.06110       -0.21250
C         -0.08070        0.11320        1.27040
Cl        -0.89730       -1.24660        2.07070
O          0.85110        1.04720       -0.63730
C          0.20560        2.24640       -0.39380
O         -1.07090        0.01350       -0.96040
C         -2.03480       -0.93830       -0.87580
H          1.97050       -1.35470        1.21710
H          2.70830       -0.60750       -0.22180
H          2.36580       -2.35710       -0.18570
H         -0.70030        1.02870        1.48010
H          0.92420        0.31110        1.68760
H         -0.79380        2.31400       -0.91310
H          0.17190        2.57730        0.63910
H          0.80670        3.03500       -0.96570
H         -2.84430       -0.65840       -1.63440
H         -2.58280       -0.91070        0.11740
H         -1.74560       -1.96030       -1.11910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

