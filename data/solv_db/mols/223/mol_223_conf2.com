%nproc=16
%mem=2GB
%chk=mol_223_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.19810        2.22940       -0.50860
O          0.57070        1.13330       -0.75920
C          0.03970       -0.07690       -0.22010
C         -0.03370        0.02790        1.27450
Cl        -0.59690       -1.43460        2.09670
O          0.95130       -1.05890       -0.57990
C          2.22720       -0.88470       -0.11270
O         -1.17860       -0.24070       -0.78850
C         -2.00510       -1.25120       -0.55760
H         -1.23740        2.17060       -0.90050
H         -0.20720        2.52220        0.55490
H          0.28410        3.09350       -1.04710
H          0.95010        0.31230        1.74840
H         -0.70760        0.88820        1.57020
H          2.35170       -0.86140        0.97060
H          2.88640       -1.73200       -0.46510
H          2.72930        0.02260       -0.53330
H         -2.43820       -1.42200        0.42610
H         -1.53430       -2.25120       -0.86090
H         -2.85340       -1.18650       -1.30820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

