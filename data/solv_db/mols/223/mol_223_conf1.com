%nproc=16
%mem=2GB
%chk=mol_223_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.25980       -0.05160        0.73160
O          1.03420       -0.58050        0.79280
C         -0.03340       -0.01990        0.17700
C          0.00220        0.11310       -1.30500
Cl         1.30160        1.15240       -1.92020
O         -0.17290        1.27330        0.72780
C         -1.23660        1.91770        0.15990
O         -1.17610       -0.68660        0.63310
C         -1.14720       -2.04220        0.34620
H          2.38630        0.98830        1.08420
H          2.94380       -0.62770        1.45980
H          2.75400       -0.15680       -0.24540
H         -0.93160        0.45910       -1.78190
H          0.19140       -0.90740       -1.78730
H         -1.34130        2.92590        0.66930
H         -2.17170        1.38600        0.28870
H         -1.10450        2.16090       -0.92790
H         -0.28050       -2.55820        0.80970
H         -1.22650       -2.26070       -0.72970
H         -2.05090       -2.48500        0.81760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

