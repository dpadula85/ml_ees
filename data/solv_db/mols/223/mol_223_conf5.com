%nproc=16
%mem=2GB
%chk=mol_223_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.03580        1.89510       -1.08690
O         -0.18370        0.59420       -0.86770
C          0.01960       -0.01250        0.30190
C         -0.62320        0.47540        1.54260
Cl        -0.16660        2.13140        1.98380
O         -0.31210       -1.38760        0.18860
C         -1.61110       -1.61290       -0.16630
O          1.41380       -0.06970        0.64080
C          2.11020       -0.77220       -0.33190
H         -0.52310        2.64660       -0.53340
H          1.14900        2.09580       -1.05030
H         -0.23240        2.11270       -2.17020
H         -0.23420       -0.16760        2.38960
H         -1.72030        0.31820        1.58930
H         -1.76890       -2.72990       -0.23360
H         -1.90330       -1.21590       -1.15590
H         -2.37840       -1.28910        0.56380
H          1.65250       -1.80410       -0.34830
H          2.13500       -0.29600       -1.30880
H          3.14130       -0.91180        0.05270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

