%nproc=16
%mem=2GB
%chk=mol_223_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.14100       -0.85570        0.16960
O          1.50380        0.09960       -0.58950
C          0.18580        0.32470       -0.30890
C         -0.08290        1.71470       -1.02720
Cl         0.24260        1.53870       -2.76290
O          0.13980        0.71140        1.04490
C         -1.11680        0.98380        1.50860
O         -0.77520       -0.45570       -0.76160
C         -0.95140       -1.75300       -0.55580
H          2.11890       -0.59640        1.27320
H          1.76280       -1.87290        0.10110
H          3.24140       -0.79610       -0.08340
H          0.52480        2.49240       -0.54940
H         -1.15110        1.97060       -0.92870
H         -1.75170        0.08050        1.40060
H         -1.64310        1.81460        1.02430
H         -1.12470        1.19610        2.61580
H         -0.18250       -2.46480       -0.92110
H         -1.87120       -2.08090       -1.14190
H         -1.21030       -2.05160        0.49230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_223_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

