%nproc=16
%mem=2GB
%chk=mol_283_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.90980        0.30750       -0.26370
C          2.26120       -0.39040       -0.48570
Cl         2.97430       -0.59390        1.13840
O          0.18930       -0.55560        0.54530
C         -1.05190       -0.11670        0.88610
C         -1.92980        0.10790       -0.30790
Cl        -3.52160        0.66220        0.27630
H          1.05860        1.24880        0.27500
H          0.36890        0.40870       -1.22510
H          2.02610       -1.37880       -0.91130
H          2.93590        0.21090       -1.10270
H         -0.99080        0.82830        1.49140
H         -1.58870       -0.83520        1.54700
H         -1.54730        0.90940       -0.99440
H         -2.09400       -0.81310       -0.86900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

