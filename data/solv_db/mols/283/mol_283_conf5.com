%nproc=16
%mem=2GB
%chk=mol_283_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07090        0.40770        0.34830
C         -2.42670       -0.08160       -0.16990
Cl        -3.70010        0.82490        0.65920
O         -0.10120       -0.34270       -0.34080
C          1.13280        0.09850        0.12670
C          2.29130       -0.61560       -0.52100
Cl         3.78340        0.08150        0.20220
H         -1.00270        1.48990        0.09570
H         -0.99980        0.28050        1.44030
H         -2.56220       -1.16040        0.06020
H         -2.44460        0.09910       -1.26760
H          1.26070        1.18090       -0.03860
H          1.18250       -0.16510        1.22360
H          2.35550       -0.40050       -1.60760
H          2.30210       -1.69690       -0.31920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

