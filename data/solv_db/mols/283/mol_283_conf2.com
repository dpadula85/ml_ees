%nproc=16
%mem=2GB
%chk=mol_283_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.09300       -0.66700       -0.79830
C          1.94660        0.45650       -0.29820
Cl         3.62850       -0.13180       -0.25870
O         -0.24270       -0.37820       -0.88990
C         -0.86240       -0.02220        0.26720
C         -2.33080        0.24920       -0.08200
Cl        -3.26860        0.72400        1.32300
H          1.25770       -1.53710       -0.13940
H          1.45240       -0.96440       -1.80490
H          1.69450        0.69330        0.77400
H          1.93400        1.35500       -0.93650
H         -0.41180        0.78510        0.83920
H         -0.89430       -0.91620        0.95360
H         -2.69710       -0.68480       -0.57120
H         -2.29890        1.03870       -0.87700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

