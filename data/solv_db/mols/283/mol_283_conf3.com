%nproc=16
%mem=2GB
%chk=mol_283_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.86920       -0.17430        0.00690
C         -2.23030        0.48900       -0.07570
Cl        -3.28120       -0.60800       -1.01360
O          0.02990        0.58840        0.71380
C          1.25410       -0.01740        0.79420
C          1.88660       -0.27300       -0.54680
Cl         2.11050        1.26660       -1.41310
H         -1.01340       -1.19330        0.40450
H         -0.51100       -0.20010       -1.06540
H         -2.69890        0.64210        0.90080
H         -2.12020        1.46740       -0.61130
H          1.23900       -0.96150        1.38440
H          1.96100        0.67080        1.34370
H          2.90900       -0.67340       -0.36120
H          1.33430       -1.02330       -1.10660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

