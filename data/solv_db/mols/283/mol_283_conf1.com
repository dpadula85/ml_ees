%nproc=16
%mem=2GB
%chk=mol_283_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.09190        0.11900       -0.51840
C         -2.46530        0.21300        0.12470
Cl        -2.68220       -1.28050        1.09000
O         -0.09020        0.02230        0.44670
C          1.11800       -0.05810       -0.24390
C          2.30550       -0.16850        0.67860
Cl         3.75920       -0.26120       -0.37030
H         -0.95900        1.04890       -1.13730
H         -1.04990       -0.79230       -1.12550
H         -2.52670        1.05160        0.84390
H         -3.22280        0.28630       -0.66820
H          1.22480        0.92000       -0.78030
H          1.08260       -0.88560       -0.98710
H          2.34440        0.78380        1.26530
H          2.25360       -0.99860        1.38190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_283_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

