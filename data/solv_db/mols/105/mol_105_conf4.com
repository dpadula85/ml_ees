%nproc=16
%mem=2GB
%chk=mol_105_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.83510        0.16500       -0.98870
C          1.02680       -0.76730       -0.11600
C          0.14810        0.06670        0.77690
C         -0.65420       -0.85430        1.64300
C         -0.68060        1.04340       -0.00130
C         -1.59970        0.40490       -0.99160
H          2.19910        0.98640       -0.33690
H          1.26630        0.59130       -1.81310
H          2.70160       -0.38660       -1.41650
H          1.71110       -1.34770        0.53490
H          0.40870       -1.47060       -0.72180
H          0.81240        0.65770        1.44620
H         -1.47180       -0.35450        2.18500
H          0.06200       -1.22650        2.43430
H         -0.99770       -1.76540        1.10770
H         -1.24850        1.72840        0.65000
H          0.03010        1.68620       -0.57520
H         -2.01040        1.21780       -1.64010
H         -1.06160       -0.28850       -1.66730
H         -2.47680       -0.08660       -0.50930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

