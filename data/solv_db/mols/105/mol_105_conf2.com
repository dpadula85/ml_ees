%nproc=16
%mem=2GB
%chk=mol_105_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.80550        0.66130        0.30510
C          0.95810        0.25820       -0.87460
C         -0.21280       -0.59300       -0.47240
C          0.25750       -1.86630        0.20260
C         -1.14480        0.08450        0.48470
C         -1.74570        1.35350       -0.07020
H          1.20510        0.94210        1.18290
H          2.44660        1.51600       -0.05410
H          2.42890       -0.20700        0.59080
H          1.59810       -0.37320       -1.54060
H          0.69380        1.14690       -1.44050
H         -0.77640       -0.89020       -1.36560
H          0.44620       -1.69200        1.28880
H          1.10170       -2.34360       -0.31330
H         -0.62400       -2.56650        0.17460
H         -0.68360        0.23670        1.49180
H         -1.99690       -0.61260        0.65960
H         -2.44040        1.03250       -0.89310
H         -1.01500        2.06620       -0.44410
H         -2.30190        1.84640        0.75680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

