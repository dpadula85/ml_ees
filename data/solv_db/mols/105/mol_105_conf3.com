%nproc=16
%mem=2GB
%chk=mol_105_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.77390        1.02660        0.02530
C          1.20700       -0.28120       -0.40000
C         -0.07470       -0.70470        0.23020
C         -0.46220       -2.09110       -0.34160
C         -1.26270        0.15360       -0.01200
C         -1.18190        1.57270        0.46190
H          2.91530        0.98150        0.08380
H          1.40100        1.32290        1.00820
H          1.61500        1.86110       -0.69630
H          1.16770       -0.30510       -1.51540
H          1.99510       -1.05430       -0.13900
H          0.07870       -0.82450        1.31690
H         -0.96580       -2.68630        0.45300
H          0.47040       -2.57840       -0.61740
H         -1.14640       -1.95160       -1.21390
H         -2.10780       -0.30430        0.59010
H         -1.63180        0.09370       -1.06160
H         -0.89350        1.56210        1.53050
H         -2.25720        1.94150        0.47350
H         -0.64010        2.26570       -0.17630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

