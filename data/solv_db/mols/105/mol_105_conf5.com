%nproc=16
%mem=2GB
%chk=mol_105_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.22700        0.53780       -0.16600
C          0.76410        0.54900       -0.54400
C         -0.04860       -0.21280        0.49460
C          0.48900       -1.63770        0.51220
C         -1.48210       -0.30520        0.04930
C         -2.13160        1.04320       -0.13500
H          2.78440       -0.23480       -0.73840
H          2.65280        1.51890       -0.43520
H          2.30610        0.34340        0.92340
H          0.68560        0.08400       -1.54110
H          0.34910        1.56780       -0.61860
H          0.01870        0.23760        1.48520
H         -0.05810       -2.24060        1.25930
H          0.39200       -2.03260       -0.51600
H          1.57610       -1.55190        0.73000
H         -2.04350       -0.93150        0.76720
H         -1.46510       -0.80460       -0.95900
H         -2.10210        1.34880       -1.20180
H         -1.69280        1.81800        0.53360
H         -3.22090        0.90330        0.10030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

