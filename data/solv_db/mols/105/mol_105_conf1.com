%nproc=16
%mem=2GB
%chk=mol_105_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.19490       -0.65400        0.23770
C          0.85730       -0.58830       -0.45510
C         -0.09860        0.22100        0.37260
C          0.47180        1.61520        0.53100
C         -1.45960        0.34660       -0.22820
C         -2.15130       -0.97450       -0.43740
H          2.87070       -1.37520       -0.29850
H          2.66850        0.35890        0.22060
H          2.04500       -0.94890        1.29760
H          0.51980       -1.59600       -0.68280
H          1.02590       -0.06210       -1.43400
H         -0.14990       -0.22390        1.39810
H          1.06690        1.91210       -0.36520
H         -0.36320        2.36640        0.62790
H          1.06450        1.67610        1.46280
H         -2.08180        0.95170        0.44910
H         -1.38540        0.90510       -1.18060
H         -1.68700       -1.80250        0.11520
H         -3.22330       -0.92660       -0.10550
H         -2.18490       -1.20130       -1.52520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_105_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

