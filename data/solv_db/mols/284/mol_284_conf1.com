%nproc=16
%mem=2GB
%chk=mol_284_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.77820       -0.43040       -0.10900
C         -3.07620        0.79100        0.43290
O         -1.68200        0.58480        0.57790
C         -0.87570        0.26400       -0.51420
O         -1.45050        0.17000       -1.61880
C          0.56180        0.05440       -0.34710
C          1.16060        0.16660        0.88680
C          2.52320       -0.03030        1.05500
C          3.32710       -0.34620       -0.01210
C          2.72790       -0.45900       -1.25070
C          1.36570       -0.26220       -1.41840
O          4.70630       -0.54830        0.14130
H         -3.06660       -1.24320       -0.38580
H         -4.42010       -0.17590       -0.97780
H         -4.45190       -0.87900        0.66300
H         -3.46690        1.00150        1.45500
H         -3.31600        1.68750       -0.17240
H          0.59920        0.41180        1.77570
H          2.99020        0.05800        2.02170
H          3.38430       -0.71250       -2.09490
H          0.93130       -0.35900       -2.40390
H          5.30660        0.25630        0.04280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

