%nproc=16
%mem=2GB
%chk=mol_284_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.02040        0.72620        0.70280
C         -3.12380       -0.35790        0.19480
O         -1.74680        0.04610        0.23460
C         -0.72720       -0.77640       -0.18050
O         -1.02730       -1.91600       -0.60590
C          0.68750       -0.40190       -0.15570
C          1.09750        0.83110        0.29710
C          2.43560        1.12150        0.29530
C          3.38050        0.22760       -0.14150
C          2.97170       -1.00620       -0.59460
C          1.64230       -1.29660       -0.59360
O          4.74260        0.57480       -0.12280
H         -4.44020        1.36250       -0.08960
H         -4.86280        0.22570        1.25650
H         -3.44950        1.38900        1.39840
H         -3.43170       -0.65720       -0.80940
H         -3.19070       -1.23760        0.85960
H          0.34070        1.53880        0.64360
H          2.74090        2.11370        0.66180
H          3.70920       -1.71720       -0.94070
H          1.29990       -2.28370       -0.95490
H          4.97210        1.49360        0.22120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

