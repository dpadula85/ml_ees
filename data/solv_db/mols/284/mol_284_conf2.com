%nproc=16
%mem=2GB
%chk=mol_284_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.67270        0.74870        0.64740
C         -3.00860        0.86900       -0.68970
O         -1.62040        0.77050       -0.65180
C         -0.90280       -0.31120       -0.19420
O         -1.56190       -1.29690        0.22650
C          0.55690       -0.35740       -0.17870
C          1.30020        0.71140       -0.63490
C          2.68760        0.64750       -0.61140
C          3.30910       -0.48570       -0.13150
C          2.59370       -1.56770        0.33030
C          1.20710       -1.46700        0.29110
O          4.70840       -0.55460       -0.10590
H         -3.27390        1.44250        1.40280
H         -4.75160        1.04640        0.49950
H         -3.72450       -0.30390        1.01710
H         -3.48080        0.14460       -1.37250
H         -3.26450        1.88200       -1.09470
H          0.76750        1.57130       -0.99890
H          3.26810        1.49340       -0.97260
H          3.07370       -2.46980        0.71240
H          0.63340       -2.29550        0.64450
H          5.15580       -0.21770        0.74650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

