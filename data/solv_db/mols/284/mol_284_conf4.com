%nproc=16
%mem=2GB
%chk=mol_284_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.70080        0.09590       -0.58150
C          2.94620       -1.20170       -0.53670
O          1.56800       -0.90940       -0.51260
C          0.93590       -0.17430        0.46110
O          1.62920        0.27420        1.40290
C         -0.50240        0.06940        0.38810
C         -1.11270        0.81030        1.37760
C         -2.48450        1.05400        1.32610
C         -3.28210        0.57750        0.30630
C         -2.64730       -0.16070       -0.67110
C         -1.29020       -0.40870       -0.63100
O         -4.64080        0.84710        0.30050
H          4.64800       -0.06980       -1.13350
H          3.06370        0.86060       -1.02740
H          3.90590        0.42430        0.47080
H          3.26710       -1.86550        0.28990
H          3.15310       -1.75340       -1.48770
H         -0.49430        1.19270        2.18930
H         -2.96770        1.63900        2.10710
H         -3.26530       -0.54350       -1.48260
H         -0.82080       -0.98950       -1.41000
H         -5.30970        0.23170        0.74760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

