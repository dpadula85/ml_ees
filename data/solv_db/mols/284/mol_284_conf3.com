%nproc=16
%mem=2GB
%chk=mol_284_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61300        0.46200        0.58620
C         -3.09320       -0.90750        0.21530
O         -1.69090       -0.89930        0.39710
C         -0.92970        0.00080       -0.33290
O         -1.47890        0.79220       -1.13950
C          0.52540       -0.00490       -0.13070
C          1.28340        0.88870       -0.85510
C          2.66410        0.90680       -0.68470
C          3.26770        0.04310        0.19670
C          2.49610       -0.84330        0.91260
C          1.12290       -0.87260        0.75350
O          4.65950        0.03030        0.39730
H         -3.77280        1.10640       -0.30450
H         -2.87620        0.95730        1.24550
H         -4.56300        0.34900        1.16800
H         -3.29060       -1.08570       -0.86460
H         -3.52870       -1.69560        0.86460
H          0.81710        1.58280       -1.56100
H          3.27780        1.61110       -1.25350
H          2.96480       -1.53300        1.61450
H          0.51800       -1.57500        1.32150
H          5.24020        0.68660       -0.13150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_284_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

