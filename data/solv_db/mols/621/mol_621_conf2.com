%nproc=16
%mem=2GB
%chk=mol_621_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.02520        0.59740       -0.23290
C          1.15830       -0.36290        0.13620
Cl         2.71080        0.46710       -0.01330
C         -1.31600       -0.09380       -0.11160
Cl        -1.60800       -0.66920        1.53910
H          0.23650        0.91560       -1.27220
H          0.05020        1.44570        0.46890
H          1.00340       -0.65210        1.18470
H          1.15770       -1.25420       -0.50200
H         -2.10120        0.59290       -0.42530
H         -1.31680       -0.98650       -0.77180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

