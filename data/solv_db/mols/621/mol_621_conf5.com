%nproc=16
%mem=2GB
%chk=mol_621_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.03220        0.32710       -0.55990
C          1.23180       -0.41640       -0.14130
Cl         1.81510        0.14540        1.41310
C         -1.11400        0.08140        0.50070
Cl        -2.59050        0.92760        0.04860
H         -0.37980       -0.01270       -1.55570
H          0.16440        1.41240       -0.65680
H          0.99730       -1.50550       -0.12760
H          1.98960       -0.28570       -0.94230
H         -1.34230       -1.00600        0.51500
H         -0.73930        0.33250        1.50620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

