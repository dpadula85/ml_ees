%nproc=16
%mem=2GB
%chk=mol_621_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.04290       -0.54520       -0.31620
C         -1.32440       -0.00480        0.12430
Cl        -1.51970        1.65090       -0.49650
C          1.15610        0.33030        0.23050
Cl         2.68980       -0.37710       -0.33330
H          0.19370       -1.55630        0.09330
H          0.04820       -0.48880       -1.41210
H         -1.42440       -0.02670        1.22420
H         -2.08980       -0.62580       -0.38530
H          1.17920        0.26070        1.34820
H          1.04840        1.38290       -0.07710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

