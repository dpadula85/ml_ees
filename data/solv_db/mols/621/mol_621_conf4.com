%nproc=16
%mem=2GB
%chk=mol_621_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.14280        0.44550       -0.45660
C          1.24500        0.24250        0.08690
Cl         1.81560       -1.40370       -0.19450
C         -1.15650       -0.47690        0.17110
Cl        -1.26940       -0.24840        1.92580
H         -0.46270        1.48630       -0.30640
H         -0.11420        0.21220       -1.53760
H          1.91670        0.92590       -0.47340
H          1.33670        0.52610        1.15520
H         -1.01590       -1.53210       -0.11950
H         -2.15240       -0.17750       -0.25090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

