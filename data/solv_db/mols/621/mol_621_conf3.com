%nproc=16
%mem=2GB
%chk=mol_621_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.06390       -0.23340       -0.70740
C         -1.22790       -0.02380        0.07500
Cl        -1.28040        1.69970        0.50860
C          1.19570        0.15890        0.21250
Cl         1.09370       -0.90820        1.63590
H          0.01950        0.35370       -1.61920
H          0.21650       -1.30770       -0.96200
H         -2.09720       -0.34690       -0.51340
H         -1.24260       -0.60110        1.01780
H          1.06770        1.20070        0.60100
H          2.19110        0.00820       -0.24880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_621_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

