%nproc=16
%mem=2GB
%chk=mol_615_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.39230       -0.29100       -0.17930
C         -0.86230        1.01700        0.35770
N          0.48320        1.27860        0.05180
C          1.31280        0.19570       -0.31630
C          0.83480       -1.16900       -0.07940
N         -0.50560       -1.31150        0.38910
H         -1.36360       -0.27680       -1.27230
H         -2.40450       -0.45370        0.24690
H         -1.53390        1.83960        0.00460
H         -1.01620        0.97950        1.47030
H          0.62620        2.12410       -0.57100
H          1.55000        0.30540       -1.41660
H          2.31550        0.36320        0.18050
H          0.95770       -1.82810       -0.99000
H          1.48990       -1.67130        0.69310
H         -0.49180       -1.10180        1.43090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

