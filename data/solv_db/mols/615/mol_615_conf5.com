%nproc=16
%mem=2GB
%chk=mol_615_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.76620       -1.10340        0.09320
C         -0.76380       -1.22100        0.15660
N         -1.25010        0.00310        0.75160
C         -0.90780        1.11140       -0.09750
C          0.61160        1.18860       -0.27500
N          1.03430       -0.06550       -0.86210
H          1.22130       -2.06380       -0.11890
H          1.06230       -0.74840        1.11460
H         -1.15550       -1.22610       -0.89910
H         -1.07050       -2.12840        0.68210
H         -0.85690        0.06710        1.70230
H         -1.28440        2.07470        0.25090
H         -1.30150        0.89140       -1.11800
H          0.87760        2.06650       -0.87620
H          1.04520        1.21220        0.75420
H          1.97200       -0.05840       -1.25880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

