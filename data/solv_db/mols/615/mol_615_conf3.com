%nproc=16
%mem=2GB
%chk=mol_615_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.34330        0.23780       -0.54400
C          0.26760        1.28710       -0.24600
N         -0.97870        0.65980       -0.71370
C         -1.31050       -0.31370        0.30140
C         -0.24580       -1.36490        0.30360
N          1.08340       -0.88740        0.28170
H          2.33220        0.66720       -0.28510
H          1.31720        0.03390       -1.63120
H          0.46440        2.24050       -0.73350
H          0.25040        1.40120        0.87140
H         -1.71270        1.33830       -0.91670
H         -1.24960        0.24000        1.28490
H         -2.29020       -0.79050        0.15500
H         -0.42170       -2.02800       -0.58070
H         -0.37910       -1.97010        1.24860
H          1.52990       -0.75110        1.20430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

