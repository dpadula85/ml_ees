%nproc=16
%mem=2GB
%chk=mol_615_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.70610        1.23390       -0.08640
C          1.33900       -0.11850       -0.08690
N          0.47860       -1.14890       -0.66120
C         -0.75010       -1.12320        0.14650
C         -1.44650        0.16960       -0.29450
N         -0.67800        1.25560        0.23480
H          0.94340        1.78710       -1.04300
H          1.24090        1.84140        0.70280
H          2.31980       -0.06260       -0.59200
H          1.47270       -0.41570        0.99390
H          0.93180       -2.05910       -0.55330
H         -0.45760       -0.94470        1.20680
H         -1.36780       -2.00420        0.00240
H         -1.45840        0.13690       -1.40410
H         -2.43720        0.17230        0.16660
H         -0.83680        1.28010        1.26750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

