%nproc=16
%mem=2GB
%chk=mol_615_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.07450       -0.82270        0.14380
C          1.26430        0.62460       -0.10970
N          0.08940        1.32610       -0.54400
C         -1.07080        0.80370        0.16220
C         -1.29020       -0.61010       -0.26820
N         -0.11540       -1.39500       -0.39620
H          1.94400       -1.36610       -0.33220
H          1.19690       -1.03770        1.24100
H          2.06010        0.81710       -0.88580
H          1.62730        1.13680        0.82510
H          0.16530        2.33780       -0.24250
H         -0.87160        0.81470        1.24820
H         -1.97300        1.39770       -0.12130
H         -2.01460       -1.06600        0.45750
H         -1.82630       -0.63150       -1.25980
H         -0.25980       -2.32950        0.08200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_615_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

