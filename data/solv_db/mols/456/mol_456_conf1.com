%nproc=16
%mem=2GB
%chk=mol_456_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.50990       -0.53710        0.16780
C          2.36720        0.82100       -0.02040
C          1.07700        1.28270       -0.14920
C         -0.04290        0.47220       -0.09990
C          0.12630       -0.88380        0.08930
C          1.42550       -1.38480        0.22390
C         -0.96430       -1.69480        0.14020
C         -2.22840       -1.16340        0.00340
C         -2.39370        0.17990       -0.18390
C         -1.30200        1.01520       -0.23800
O         -1.53260        2.36090       -0.42900
H          3.49420       -0.96520        0.27620
H          3.22940        1.47970       -0.06300
H          0.96870        2.34590       -0.29640
H          1.49890       -2.45410        0.36990
H         -0.84880       -2.76060        0.28810
H         -3.11310       -1.78550        0.03990
H         -3.39850        0.58010       -0.28960
H         -0.87280        3.09190       -0.49180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_456_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_456_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_456_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

