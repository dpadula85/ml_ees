%nproc=16
%mem=2GB
%chk=mol_456_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.62090        0.14570        0.04400
C          2.27520       -1.18400        0.01750
C          0.94510       -1.52100       -0.00920
C         -0.02840       -0.51500       -0.00890
C          0.32180        0.81760        0.01770
C          1.66170        1.15830        0.04450
C         -0.61600        1.82450        0.01890
C         -1.95800        1.49820       -0.00730
C         -2.32870        0.16230       -0.03430
C         -1.37120       -0.83090       -0.03500
O         -1.69540       -2.18940       -0.06170
H          3.66160        0.40040        0.06450
H          3.02240       -1.95840        0.01790
H          0.64880       -2.55660       -0.03050
H          1.96190        2.20610        0.06610
H         -0.35660        2.87760        0.03970
H         -2.72030        2.25870       -0.00750
H         -3.38470       -0.11400       -0.05510
H         -2.66000       -2.47990       -0.08150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_456_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_456_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_456_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

