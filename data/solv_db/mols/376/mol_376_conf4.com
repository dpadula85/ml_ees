%nproc=16
%mem=2GB
%chk=mol_376_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.34360        0.20110       -0.02030
C         -2.01390       -0.15100       -0.62370
S         -0.90750        1.28730       -0.69370
S          0.90510        0.97570        0.31700
C          1.96040       -0.27090       -0.46190
C          3.36430       -0.08100        0.08620
H         -3.16740        0.48760        1.02600
H         -3.99620       -0.68080       -0.14980
H         -3.71570        1.10340       -0.55950
H         -2.07450       -0.53550       -1.65430
H         -1.51480       -0.91040        0.00750
H          1.92990       -0.16380       -1.56660
H          1.60220       -1.27400       -0.18470
H          3.64780        0.96340       -0.17920
H          3.27790       -0.11610        1.19450
H          4.04600       -0.83500       -0.33010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

