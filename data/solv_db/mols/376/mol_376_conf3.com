%nproc=16
%mem=2GB
%chk=mol_376_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.52540       -0.46840        0.27620
C         -2.24230        0.97260       -0.10790
S         -0.71310        0.98400       -1.04220
S          0.96960        1.37670        0.16100
C          1.71840       -0.14890        0.73820
C          2.97560       -0.45900       -0.06580
H         -1.66590       -0.94790        0.79070
H         -2.66660       -1.02530       -0.68510
H         -3.47470       -0.55970        0.82840
H         -2.25900        1.65630        0.75640
H         -3.06280        1.26760       -0.81150
H          0.99830       -1.00160        0.67790
H          2.04420       -0.00840        1.79810
H          3.90130       -0.29340        0.54110
H          3.05230        0.17890       -0.95540
H          2.95000       -1.52340       -0.34580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

