%nproc=16
%mem=2GB
%chk=mol_376_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09080        0.35380       -0.09490
C          1.85470       -0.97390        0.54680
S          0.27160       -1.66880        0.48430
S         -0.88470       -0.34030        1.67190
C         -1.49400        0.76590        0.46380
C         -2.44520        0.02600       -0.47530
H          1.96810        0.32370       -1.19510
H          3.18580        0.58240        0.07440
H          1.53290        1.16750        0.40410
H          2.60630       -1.68210        0.08400
H          2.21940       -0.87690        1.61330
H         -0.78800        1.31050       -0.16830
H         -2.14300        1.56390        0.93790
H         -1.86250       -0.26390       -1.38040
H         -2.76590       -0.91360        0.03530
H         -3.34630        0.62590       -0.71580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

