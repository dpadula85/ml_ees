%nproc=16
%mem=2GB
%chk=mol_376_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.05840        0.17900        0.17650
C         -1.73360       -0.53410        0.31350
S         -0.44830        0.63610        0.86770
S          0.63770        1.18110       -0.89320
C          2.06790        0.04990       -0.96940
C          2.75640        0.09450        0.37570
H         -2.95780        1.05530       -0.48320
H         -3.77400       -0.58230       -0.22700
H         -3.42700        0.42890        1.19230
H         -1.84970       -1.30500        1.09640
H         -1.45730       -1.00490       -0.65240
H          1.75570       -0.98370       -1.21810
H          2.76800        0.45920       -1.73240
H          2.26760       -0.59130        1.10990
H          3.81380       -0.20600        0.26820
H          2.63870        1.12340        0.77550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

