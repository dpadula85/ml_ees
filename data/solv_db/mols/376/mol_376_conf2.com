%nproc=16
%mem=2GB
%chk=mol_376_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.03310        0.16740        0.17840
C         -2.08400       -0.11410       -0.97340
S         -0.64280        0.97170       -0.94890
S          1.07890       -0.28320       -1.12670
C          1.82630       -0.39680        0.51420
C          3.20740        0.23980        0.43160
H         -2.62390       -0.31280        1.10040
H         -4.04320       -0.22750       -0.07770
H         -3.12050        1.24150        0.40350
H         -2.60590        0.01790       -1.95560
H         -1.81400       -1.19500       -0.87050
H          1.21310        0.10700        1.29940
H          1.88690       -1.46970        0.82680
H          3.97720       -0.41400        0.89470
H          3.24050        1.23300        0.92800
H          3.53710        0.37910       -0.62420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_376_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

