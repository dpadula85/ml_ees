%nproc=16
%mem=2GB
%chk=mol_590_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.00260        0.81910        0.37470
C         -1.63070       -0.60990        0.03650
C         -0.26420       -0.72200       -0.58710
C          0.84820       -0.38220        0.37730
O          1.66410        0.64120       -0.12370
N          3.02260        0.69450       -0.02910
O          3.75670       -0.30870       -0.19980
O          3.69270        1.90620        0.00780
H         -2.95840        0.75760        0.95000
H         -2.26990        1.34680       -0.57250
H         -1.19620        1.33100        0.91760
H         -1.76550       -1.30320        0.87910
H         -2.35790       -0.93150       -0.75090
H         -0.18440       -1.76530       -0.97860
H         -0.20610       -0.04380       -1.44960
H          0.36700       -0.13930        1.34040
H          1.48450       -1.29050        0.52690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

