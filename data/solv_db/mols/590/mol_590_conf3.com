%nproc=16
%mem=2GB
%chk=mol_590_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.46640       -0.85800       -0.04970
C         -1.07780       -0.42290       -0.51700
C         -0.44440        0.31480        0.63340
C          0.94050        0.80540        0.30990
O          1.82190       -0.24600       -0.01980
N          3.11210        0.07180       -0.33390
O          3.89370        0.30060        0.60020
O          3.59560        0.15370       -1.60810
H         -2.91620        0.00840        0.51680
H         -2.40480       -1.76580        0.57670
H         -3.13700       -0.99620       -0.92570
H         -1.21690        0.22900       -1.38510
H         -0.53070       -1.35770       -0.74880
H         -0.39780       -0.30740        1.53140
H         -1.07100        1.20910        0.83360
H          1.32420        1.30430        1.23800
H          0.97500        1.55670       -0.48700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

