%nproc=16
%mem=2GB
%chk=mol_590_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49540        0.25340        0.57220
C         -1.05820       -0.17920        0.50120
C         -0.42220        0.26330       -0.82310
C          1.02840       -0.19780       -0.84490
O          1.70230        0.38080        0.22790
N          2.99800        0.21960        0.55080
O          3.39000        0.53670        1.67640
O          3.88770       -0.28830       -0.34370
H         -2.70250        0.82100        1.50250
H         -3.14190       -0.65890        0.50090
H         -2.69030        0.91500       -0.29630
H         -0.97360       -1.28100        0.56520
H         -0.51280        0.23390        1.36750
H         -0.51030        1.34440       -0.96860
H         -1.01970       -0.23840       -1.61260
H          1.07890       -1.29710       -0.74100
H          1.44170        0.07300       -1.83430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

