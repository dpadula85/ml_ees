%nproc=16
%mem=2GB
%chk=mol_590_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.60260        0.23890       -0.06260
C         -1.19100       -0.11930       -0.55060
C         -0.30510       -0.07400        0.66750
C          1.13010       -0.40340        0.36620
O          1.66600        0.51480       -0.57020
N          2.96550        0.39600       -0.98500
O          3.51930        1.41110       -1.48180
O          3.65300       -0.78280       -0.86950
H         -2.55790        1.33000        0.18100
H         -2.75690       -0.38220        0.85470
H         -3.30490        0.00780       -0.86780
H         -1.28390       -1.13510       -0.97240
H         -0.85940        0.55010       -1.35920
H         -0.71880       -0.78700        1.40370
H         -0.34140        0.93790        1.11790
H          1.70080       -0.24820        1.30960
H          1.28720       -1.45450        0.07950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

