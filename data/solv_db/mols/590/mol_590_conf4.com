%nproc=16
%mem=2GB
%chk=mol_590_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.13080        0.59210       -0.51730
C         -1.57890       -0.72680       -0.08310
C         -0.25360       -0.63160        0.61580
C          0.82250       -0.01950       -0.23280
O          2.01140       -0.00110        0.57140
N          3.18570        0.49030        0.11250
O          4.22280       -0.16310        0.16180
O          3.21850        1.74890       -0.42080
H         -1.37600        1.27570       -0.96020
H         -2.66600        1.04370        0.33870
H         -2.91140        0.36610       -1.30360
H         -1.56060       -1.46350       -0.92060
H         -2.29510       -1.15720        0.66640
H         -0.38460       -0.06050        1.54300
H          0.06430       -1.65980        0.88080
H          0.60330        1.01080       -0.54440
H          1.02860       -0.64440       -1.12540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_590_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

