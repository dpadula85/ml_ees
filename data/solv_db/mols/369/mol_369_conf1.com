%nproc=16
%mem=2GB
%chk=mol_369_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.03920        0.45670        0.16090
O         -1.04720       -0.37750        0.64750
S         -0.09720       -0.87210       -0.65080
O         -1.03380       -1.09000       -1.83390
O          0.53470       -2.19680       -0.33080
O          0.98170        0.27580       -1.14150
C          2.11420        0.38080       -0.38130
H         -2.54630       -0.08290       -0.66000
H         -1.53680        1.35660       -0.27400
H         -2.74920        0.79750        0.92850
H          2.13420       -0.22460        0.52950
H          3.04890        0.12490       -0.95270
H          2.23610        1.45170       -0.10430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

