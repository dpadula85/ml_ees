%nproc=16
%mem=2GB
%chk=mol_369_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.45440        0.05250       -0.29280
O          1.35990       -0.73000       -0.61700
S         -0.00320       -0.37800        0.24740
O          0.14040        0.85130        1.10220
O         -0.22870       -1.52000        1.22900
O         -1.28910       -0.30650       -0.78480
C         -2.43520        0.23390       -0.24930
H          2.32260        1.12080       -0.48780
H          3.29680       -0.29060       -0.93820
H          2.75680       -0.18650        0.74610
H         -2.86910       -0.41070        0.55800
H         -2.31690        1.28980        0.08830
H         -3.18870        0.27390       -1.07330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

