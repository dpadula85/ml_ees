%nproc=16
%mem=2GB
%chk=mol_369_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44870       -0.09870       -0.10160
O         -1.40600       -0.29610       -1.01590
S          0.03060       -0.43140       -0.17230
O         -0.18040       -0.73050        1.28190
O          0.75580       -1.62480       -0.77490
O          0.97650        0.93770       -0.27730
C          2.31220        0.64820       -0.50940
H         -2.16530       -0.54780        0.86370
H         -2.58290        1.01250       -0.04210
H         -3.35830       -0.59630       -0.48230
H          2.98990        1.19940        0.18490
H          2.54000       -0.42000       -0.39890
H          2.53640        0.94770       -1.54830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

