%nproc=16
%mem=2GB
%chk=mol_369_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.44580        0.13540        0.41310
O          1.14690        0.15670        0.86820
S          0.02200       -0.13270       -0.34860
O          0.67770        0.25260       -1.65630
O         -0.27640       -1.59410       -0.41350
O         -1.33220        0.81170       -0.17020
C         -2.48890        0.10550        0.04830
H          3.07580       -0.35250        1.19730
H          2.57270       -0.45500       -0.52380
H          2.77210        1.19670        0.25570
H         -2.59830       -0.26690        1.09440
H         -3.34100        0.83470       -0.08010
H         -2.67610       -0.69210       -0.68450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

