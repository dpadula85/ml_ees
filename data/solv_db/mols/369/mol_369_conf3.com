%nproc=16
%mem=2GB
%chk=mol_369_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.97370        0.18840       -0.46170
O          1.64920       -0.19930        0.81800
S          0.02020       -0.47850        1.04000
O         -0.32160       -1.82130        0.50800
O         -0.22160       -0.51980        2.53940
O         -0.91060        0.74590        0.42240
C         -1.92230        0.31960       -0.41530
H          2.19490        1.28640       -0.54400
H          1.19650       -0.01600       -1.21250
H          2.95220       -0.30700       -0.70980
H         -2.71820        1.11510       -0.39030
H         -2.35790       -0.65020       -0.13060
H         -1.53450        0.33660       -1.46350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_369_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

