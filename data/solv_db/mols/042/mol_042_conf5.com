%nproc=16
%mem=2GB
%chk=mol_042_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.42510       -0.32340       -0.03570
S         -0.00220        0.74780        0.09840
O         -0.05500        1.45900        1.39400
O          0.03130        1.76830       -1.00430
C         -1.43130       -0.30530       -0.04160
H          1.16240       -1.26250       -0.59040
H          2.25760        0.18040       -0.53890
H          1.71270       -0.57590        1.00510
H         -1.52710       -0.53730       -1.13350
H         -1.24420       -1.28480        0.45260
H         -2.32920        0.13380        0.39430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

