%nproc=16
%mem=2GB
%chk=mol_042_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43040        0.02130       -0.42420
S         -0.02300       -0.17360        0.62520
O         -0.12270        0.89990        1.69810
O         -0.10530       -1.53480        1.25100
C          1.50080        0.07800       -0.21800
H         -1.34200        0.89500       -1.11020
H         -1.70580       -0.91170       -0.95820
H         -2.30710        0.24660        0.24070
H          1.42030        0.95610       -0.86970
H          2.27350        0.31610        0.56550
H          1.84180       -0.79280       -0.80020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

