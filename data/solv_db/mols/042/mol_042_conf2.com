%nproc=16
%mem=2GB
%chk=mol_042_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45980        0.38420       -0.00520
S         -0.02950       -0.63950        0.13740
O         -0.09300       -1.79060       -0.82900
O         -0.01480       -1.18760        1.55100
C          1.47320        0.22340       -0.14040
H         -2.24470       -0.21190       -0.53870
H         -1.89040        0.66750        0.98760
H         -1.29850        1.30160       -0.58300
H          1.31650        1.26500       -0.47520
H          2.13670       -0.27880       -0.89480
H          2.10430        0.26660        0.79040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

