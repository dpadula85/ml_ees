%nproc=16
%mem=2GB
%chk=mol_042_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.39560       -0.23360        0.31540
S         -0.01920        0.75940       -0.09130
O          0.20640        1.46450       -1.39510
O         -0.32190        1.73160        1.02560
C         -1.38970       -0.38570       -0.25940
H          2.05070        0.25860        1.04690
H          1.10240       -1.21310        0.77230
H          1.90420       -0.47280       -0.64710
H         -1.02500       -1.37440       -0.62660
H         -1.75120       -0.56680        0.77770
H         -2.15230        0.03220       -0.91850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

