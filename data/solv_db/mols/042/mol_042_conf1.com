%nproc=16
%mem=2GB
%chk=mol_042_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.42570        0.16640       -0.41260
S         -0.02150        0.06380        0.68070
O         -0.24730       -1.12670        1.59520
O          0.03800        1.26060        1.58450
C          1.46680       -0.18860       -0.22290
H         -2.07850        1.02880       -0.12450
H         -1.05130        0.28650       -1.44900
H         -2.05090       -0.75680       -0.36710
H          1.20280       -0.33100       -1.30190
H          2.01240       -1.07700        0.16090
H          2.15520        0.67390       -0.14330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_042_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

