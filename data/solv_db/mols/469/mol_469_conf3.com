%nproc=16
%mem=2GB
%chk=mol_469_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88640       -0.74590       -0.16470
C         -1.02740        0.39120       -0.65100
C         -0.09170        0.90990        0.39820
C          0.87540       -0.09680        0.92580
C          1.78660       -0.68770       -0.09860
O          0.61950        1.99160       -0.11900
H         -1.34590       -1.70420       -0.09660
H         -2.70170       -0.90370       -0.91650
H         -2.37910       -0.46590        0.77780
H         -0.50280        0.12110       -1.58740
H         -1.71560        1.22190       -0.91760
H         -0.69240        1.27480        1.25820
H          0.37230       -0.93390        1.45580
H          1.51070        0.40360        1.68740
H          1.34150       -1.51700       -0.67560
H          2.65520       -1.11550        0.46090
H          2.19740        0.11430       -0.74220
H          0.98450        1.74200       -0.99470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

