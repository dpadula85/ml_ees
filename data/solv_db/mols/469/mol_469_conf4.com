%nproc=16
%mem=2GB
%chk=mol_469_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53300       -0.18570       -0.16090
C          1.25650        0.57750       -0.15330
C          0.02600       -0.27960       -0.01350
C         -1.17190        0.64700       -0.02390
C         -2.46980       -0.11220        0.11150
O          0.05640       -1.10980        1.08970
H          2.81430       -0.46570       -1.19980
H          3.33920        0.51250        0.21220
H          2.55550       -1.06320        0.50940
H          1.16660        1.09380       -1.14690
H          1.22100        1.39930        0.60170
H         -0.06450       -0.89930       -0.95100
H         -1.18950        1.14680       -1.01110
H         -1.06520        1.40090        0.75550
H         -3.27660        0.65990        0.18030
H         -2.63220       -0.76180       -0.76830
H         -2.51810       -0.70170        1.03430
H         -0.58060       -1.85870        0.93410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

