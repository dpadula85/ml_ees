%nproc=16
%mem=2GB
%chk=mol_469_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93500        0.87690       -0.42130
C         -1.36560       -0.51220       -0.25410
C         -0.06070       -0.51460        0.49430
C          0.95530        0.31300       -0.25270
C          2.28110        0.35620        0.43440
O          0.40370       -1.83810        0.49910
H         -3.03550        0.84400       -0.37690
H         -1.55980        1.55980        0.36610
H         -1.57810        1.29180       -1.38920
H         -2.07270       -1.14970        0.33400
H         -1.26910       -0.96470       -1.25500
H         -0.24270       -0.12310        1.50540
H          1.10110       -0.15380       -1.25020
H          0.55750        1.32290       -0.38270
H          3.07910       -0.22080       -0.10570
H          2.69420        1.40130        0.49620
H          2.27790       -0.02220        1.46640
H         -0.23070       -2.46690        0.09180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

