%nproc=16
%mem=2GB
%chk=mol_469_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.26430        0.31810       -0.01590
C          0.85400        0.59370       -0.43720
C          0.02340       -0.63820       -0.11660
C         -1.43720       -0.42120       -0.52470
C         -2.06480        0.72530        0.17430
O          0.45020       -1.73690       -0.85820
H          2.94010        0.37190       -0.88050
H          2.38110       -0.64090        0.52790
H          2.61310        1.10950        0.69710
H          0.46990        1.49230        0.07850
H          0.82470        0.82150       -1.51910
H          0.08360       -0.87470        0.94380
H         -1.51980       -0.39510       -1.62750
H         -1.96740       -1.35810       -0.19590
H         -1.57890        0.91250        1.14660
H         -3.15200        0.51100        0.39490
H         -2.02280        1.65540       -0.45580
H          0.83850       -2.44610       -0.31360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

