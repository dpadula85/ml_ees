%nproc=16
%mem=2GB
%chk=mol_469_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30530        0.49050       -0.07970
C          0.80620        0.62470       -0.19050
C          0.09750       -0.69920       -0.01590
C         -1.38740       -0.44930       -0.14470
C         -1.90630        0.51160        0.88170
O          0.47570       -1.62080       -0.97980
H          2.55510        0.07210        0.92360
H          2.72590        1.51730       -0.10330
H          2.74170       -0.09630       -0.91010
H          0.58340        1.01270       -1.20500
H          0.47460        1.29820        0.61910
H          0.31650       -1.15170        0.97210
H         -1.55310       -0.00460       -1.16630
H         -1.93170       -1.41440       -0.15000
H         -1.18040        0.73920        1.68700
H         -2.81710        0.10340        1.40320
H         -2.22900        1.48700        0.45480
H         -0.07690       -2.42030       -0.88860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_469_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

