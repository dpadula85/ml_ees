%nproc=16
%mem=2GB
%chk=mol_551_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.06090       -0.42240       -0.71790
C          1.43430        0.29040        0.42580
C          0.03900       -0.14370        0.74920
C         -0.94360        0.03730       -0.36200
C         -2.29240       -0.45980        0.16380
I         -3.80170       -0.26490       -1.31440
H          2.75500       -1.24300       -0.37040
H          1.37890       -0.85110       -1.44840
H          2.74130        0.30900       -1.24150
H          1.47900        1.39920        0.30520
H          2.04660        0.06660        1.33790
H         -0.00440       -1.17510        1.09870
H         -0.31200        0.49950        1.59740
H         -0.70710       -0.51940       -1.26990
H         -1.09990        1.13190       -0.55500
H         -2.57890        0.15110        1.05750
H         -2.19500       -1.50380        0.54390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

