%nproc=16
%mem=2GB
%chk=mol_551_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.48750        0.45740       -0.32460
C         -1.27940       -0.41120       -0.09340
C          0.01540        0.36600       -0.23030
C          1.13120       -0.63090        0.02720
C          2.49400        0.00440       -0.07570
I          2.87680        0.83550       -1.96980
H         -2.79800        0.96840        0.59520
H         -3.35760       -0.13560       -0.67540
H         -2.22040        1.16780       -1.14390
H         -1.26100       -0.87400        0.90910
H         -1.25590       -1.23540       -0.84030
H          0.05710        1.21950        0.46650
H          0.14680        0.72550       -1.27660
H          1.05750       -1.44780       -0.68960
H          1.06310       -1.07190        1.04410
H          3.23060       -0.76370        0.23870
H          2.58710        0.82580        0.69360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

