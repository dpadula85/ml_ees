%nproc=16
%mem=2GB
%chk=mol_551_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.35170        0.21070        0.25420
C          0.94380       -0.18880        0.71480
C          0.07440       -0.13780       -0.51800
C         -1.35350       -0.50080       -0.27470
C         -1.95180        0.44340        0.71870
I         -3.99370        0.00030        1.12460
H          2.93960        0.53210        1.14040
H          2.85130       -0.65700       -0.20340
H          2.30860        1.03770       -0.47850
H          0.60700        0.53320        1.48720
H          0.96850       -1.22070        1.12220
H          0.16450        0.91570       -0.90730
H          0.55380       -0.76530       -1.30530
H         -1.39190       -1.51870        0.17100
H         -1.86850       -0.48420       -1.25760
H         -1.79920        1.49460        0.41910
H         -1.40480        0.30560        1.68000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

