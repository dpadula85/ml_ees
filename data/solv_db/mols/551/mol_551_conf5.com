%nproc=16
%mem=2GB
%chk=mol_551_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.12870        0.03640        0.44460
C         -1.27900       -0.49570       -0.67880
C          0.08970        0.12240       -0.71950
C          0.86000       -0.13870        0.57280
C          2.22390        0.54320        0.39080
I          3.10250       -0.40090       -1.31580
H         -3.15160        0.26960        0.08160
H         -1.68270        0.93930        0.92000
H         -2.25360       -0.70270        1.26880
H         -1.13990       -1.58620       -0.49790
H         -1.76800       -0.35250       -1.63880
H          0.66210       -0.42970       -1.51280
H          0.10890        1.17330       -0.99900
H          0.33400        0.28680        1.42800
H          1.09110       -1.21580        0.68970
H          2.13340        1.61680        0.24760
H          2.79800        0.33440        1.31860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

