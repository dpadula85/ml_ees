%nproc=16
%mem=2GB
%chk=mol_551_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24640        0.72470       -0.21440
C         -0.87790        0.60540        0.49310
C         -0.20640       -0.62740        0.00600
C          1.11210       -0.97850        0.54430
C          2.25570       -0.07080        0.38840
I          2.17400        1.83570        1.21880
H         -2.71360        1.68500        0.14670
H         -2.09450        0.84250       -1.30490
H         -2.88300       -0.13200       -0.00740
H         -0.39220        1.55570        0.16410
H         -1.08830        0.55580        1.56570
H         -0.89660       -1.47820        0.31020
H         -0.24340       -0.67290       -1.10620
H          1.41690       -1.94070        0.02190
H          0.98100       -1.33060        1.61260
H          2.55900        0.03280       -0.69370
H          3.14350       -0.60660        0.85130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_551_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

