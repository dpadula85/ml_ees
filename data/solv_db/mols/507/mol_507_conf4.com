%nproc=16
%mem=2GB
%chk=mol_507_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.57980        0.40450       -0.38490
N          1.66290       -0.20010        0.56250
C          0.25510       -0.16010        0.32270
C         -0.25210        0.45190       -0.80250
C         -1.61760        0.49440       -1.04610
C         -2.52380       -0.07280       -0.17440
C         -2.01020       -0.68390        0.95040
C         -0.65300       -0.72560        1.19140
H          3.62260        0.00820       -0.23970
H          2.65470        1.49580       -0.24380
H          2.30410        0.11490       -1.42690
H          2.07070       -0.65190        1.40260
H          0.43370        0.91850       -1.52310
H         -2.00130        0.98090       -1.93830
H         -3.58760       -0.02680       -0.38670
H         -2.70730       -1.14170        1.65860
H         -0.23070       -1.20620        2.07820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

