%nproc=16
%mem=2GB
%chk=mol_507_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.56890       -0.55820        0.03240
N          1.65460        0.39600       -0.53790
C          0.26620        0.28360       -0.24990
C         -0.20220       -0.72250        0.56330
C         -1.54690       -0.85150        0.85680
C         -2.47710        0.03220        0.34020
C         -1.99480        1.03450       -0.47230
C         -0.65910        1.17770       -0.77490
H          2.14080       -1.58210       -0.18250
H          2.61260       -0.40530        1.13580
H          3.59100       -0.47070       -0.35050
H          2.04080        1.14190       -1.14630
H          0.48710       -1.45620        1.00480
H         -1.90860       -1.64640        1.49810
H         -3.52850       -0.08710        0.58320
H         -2.71040        1.73870       -0.88790
H         -0.33450        1.97530       -1.41270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

