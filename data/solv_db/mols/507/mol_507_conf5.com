%nproc=16
%mem=2GB
%chk=mol_507_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65270        0.24490        0.09580
N          1.58380       -0.70930        0.27420
C          0.20540       -0.35220        0.10470
C         -0.79060       -1.28510        0.27910
C         -2.10210       -0.91110        0.11110
C         -2.49440        0.36470       -0.22820
C         -1.47450        1.27880       -0.39660
C         -0.15600        0.93170       -0.23470
H          2.74940        0.93770        0.95760
H          2.42600        0.80010       -0.84500
H          3.60870       -0.26910       -0.06040
H          1.84280       -1.68050        0.53620
H         -0.51060       -2.30150        0.54760
H         -2.89750       -1.65550        0.25040
H         -3.54010        0.62810       -0.35360
H         -1.74830        2.30730       -0.66760
H          0.64510        1.67100       -0.37060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

