%nproc=16
%mem=2GB
%chk=mol_507_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.67170        0.45650        0.27360
N          1.61810       -0.50080        0.36840
C          0.24240       -0.26050        0.15000
C         -0.26320        0.97600       -0.19360
C         -1.62890        1.16490       -0.39970
C         -2.50230        0.11130       -0.26240
C         -2.02040       -1.13180        0.07990
C         -0.67730       -1.28270        0.27460
H          3.29210        0.48430        1.20900
H          3.37670        0.09560       -0.51520
H          2.28290        1.43660       -0.00210
H          1.90820       -1.49190        0.63040
H          0.36100        1.82500       -0.31330
H         -2.03100        2.13190       -0.66930
H         -3.57760        0.25080       -0.42260
H         -2.72690       -1.97790        0.18950
H         -0.32550       -2.28740        0.54680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

