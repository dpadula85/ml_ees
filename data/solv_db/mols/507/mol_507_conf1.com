%nproc=16
%mem=2GB
%chk=mol_507_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.58690       -0.72370       -0.26340
N         -1.68440        0.42860       -0.18170
C         -0.29650        0.28450       -0.03140
C          0.48450        1.41740        0.03870
C          1.86090        1.32290        0.18630
C          2.47730        0.09180        0.26600
C          1.71070       -1.05650        0.19760
C          0.34470       -0.93130        0.05080
H         -2.45080       -1.13750       -1.29510
H         -3.62400       -0.44820       -0.05800
H         -2.18250       -1.46820        0.46560
H         -2.14390        1.35970       -0.24360
H          0.05230        2.42440       -0.01870
H          2.49040        2.22460        0.24270
H          3.56010        0.06790        0.38180
H          2.22270       -2.00450        0.26290
H         -0.23470       -1.85190       -0.00060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_507_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

