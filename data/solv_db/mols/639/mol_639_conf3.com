%nproc=16
%mem=2GB
%chk=mol_639_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.78820        0.00400        0.07490
C         -0.69550       -0.21460       -0.05470
Br        -1.61660        1.47200       -0.33510
H          1.33350       -0.20710       -0.88460
H          1.25290       -0.61680        0.87030
H          0.94590        1.09730        0.24020
H         -1.06310       -0.58590        0.93890
H         -0.94520       -0.94900       -0.85000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

