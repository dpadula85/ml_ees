%nproc=16
%mem=2GB
%chk=mol_639_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.78650        0.10020       -0.07060
C          0.67770       -0.32890        0.04740
Br         1.69090        1.33600        0.13870
H         -0.93880        0.83050        0.77230
H         -1.47860       -0.73840        0.02610
H         -0.93940        0.61660       -1.04870
H          0.99170       -0.90400       -0.85230
H          0.78300       -0.91200        0.98710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

