%nproc=16
%mem=2GB
%chk=mol_639_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79230        0.04450       -0.08100
C          0.70320       -0.24940        0.05250
Br         1.63930        1.42100       -0.02700
H         -1.39750       -0.84840        0.13450
H         -1.05250        0.42150       -1.07970
H         -0.97550        0.85220        0.67910
H          1.05940       -0.92930       -0.74430
H          0.81590       -0.71210        1.06590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

