%nproc=16
%mem=2GB
%chk=mol_639_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.80560        0.03280        0.03620
C          0.69050       -0.27230        0.01610
Br         1.54950        1.43200       -0.38940
H         -0.96130        0.75090        0.85690
H         -1.03550        0.56110       -0.91450
H         -1.41890       -0.86550        0.14360
H          0.93880       -1.02740       -0.77050
H          1.04250       -0.61160        1.02160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

