%nproc=16
%mem=2GB
%chk=mol_639_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76670        0.12910        0.00620
C          0.68580       -0.24440        0.02270
Br         1.85230        1.23340       -0.33130
H         -0.99870        1.08900        0.46920
H         -1.16690        0.15260       -1.03170
H         -1.34610       -0.66770        0.52930
H          0.82240       -1.07440       -0.71180
H          0.91790       -0.61770        1.04730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_639_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

