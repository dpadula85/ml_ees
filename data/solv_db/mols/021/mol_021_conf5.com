%nproc=16
%mem=2GB
%chk=mol_021_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77280        0.01960       -0.05040
C          0.72480       -0.01620        0.16280
Cl         1.61840       -0.32830       -1.30780
Cl         1.06490       -1.25010        1.39470
Cl        -1.20210        1.30220       -1.20930
H         -1.30210        0.24490        0.88620
H         -1.14430       -0.98490       -0.39100
H          1.01320        1.01270        0.51490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

