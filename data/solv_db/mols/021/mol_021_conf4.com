%nproc=16
%mem=2GB
%chk=mol_021_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.71490       -0.26870       -0.04870
C          0.71430        0.19120        0.13310
Cl         1.82600       -1.18860        0.10750
Cl         1.09930        1.38600       -1.12310
Cl        -1.86120        1.06440       -0.03090
H         -0.98640       -0.97060        0.78270
H         -0.86680       -0.83700       -0.97730
H          0.78950        0.62330        1.15670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

