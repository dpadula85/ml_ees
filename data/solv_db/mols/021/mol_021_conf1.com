%nproc=16
%mem=2GB
%chk=mol_021_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77060       -0.08020       -0.08920
C          0.73730       -0.08180        0.12640
Cl         1.36810        1.32750       -0.74440
Cl         1.47390       -1.59260       -0.42990
Cl        -1.39160        1.44710        0.57390
H         -1.04970       -0.20320       -1.13840
H         -1.25630       -0.89660        0.48160
H          0.88900        0.07980        1.22000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

