%nproc=16
%mem=2GB
%chk=mol_021_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.79740        0.08160       -0.02800
C         -0.72200        0.07320        0.14500
Cl        -1.33950        1.62900       -0.43560
Cl        -1.33300       -1.28230       -0.83930
Cl         1.38230       -1.49860        0.56640
H          0.96030        0.17740       -1.12780
H          1.26870        0.91760        0.52080
H         -1.01420       -0.09800        1.19840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

