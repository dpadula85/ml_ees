%nproc=16
%mem=2GB
%chk=mol_021_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.75200       -0.16570       -0.14740
C         -0.69730        0.01880        0.27660
Cl        -1.26080        1.47560       -0.57820
Cl        -1.55930       -1.43490       -0.25220
Cl         1.64670        1.28290        0.33970
H          0.72260       -0.26550       -1.27040
H          1.18050       -1.10070        0.26440
H         -0.78440        0.18960        1.36750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_021_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

