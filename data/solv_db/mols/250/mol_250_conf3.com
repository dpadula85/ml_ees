%nproc=16
%mem=2GB
%chk=mol_250_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.33470       -0.72740       -0.71210
C         -1.21720        0.21970       -0.34560
C         -1.71380        1.11610        0.74890
C         -0.03000       -0.56860        0.03300
O         -0.10000       -1.78360        0.00420
C          1.25890        0.04940        0.45280
C          1.78830        0.92700       -0.69080
C          2.28040       -1.04400        0.64050
H         -2.84550       -1.11570        0.19340
H         -1.89690       -1.63200       -1.22620
H         -3.05510       -0.28810       -1.42530
H         -1.01090        0.85680       -1.23110
H         -1.67830        0.62960        1.73690
H         -1.23770        2.10820        0.75950
H         -2.80440        1.30590        0.54780
H          1.18060        0.62150        1.38530
H          2.62160        1.52510       -0.23140
H          0.98320        1.58340       -1.09260
H          2.22330        0.25770       -1.44520
H          3.31120       -0.66480        0.72180
H          2.26110       -1.66230       -0.30060
H          2.01570       -1.71380        1.47710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

