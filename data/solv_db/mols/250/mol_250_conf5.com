%nproc=16
%mem=2GB
%chk=mol_250_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.95210        1.05740        0.66190
C          1.37720        0.23360       -0.46180
C          1.69680       -1.23570       -0.34690
C         -0.03710        0.46600       -0.71410
O         -0.33880        0.81950       -1.87240
C         -1.16500        0.33830        0.21020
C         -2.49740        0.72680       -0.45360
C         -1.30270       -1.08770        0.67210
H          2.80850        0.55080        1.15630
H          2.33840        2.03480        0.30450
H          1.22300        1.27860        1.46140
H          1.95090        0.58160       -1.38220
H          0.93780       -1.90500       -0.76150
H          1.88830       -1.55510        0.71070
H          2.65680       -1.48800       -0.88370
H         -1.09960        1.01210        1.09580
H         -3.22850        0.83900        0.37440
H         -2.82100       -0.06270       -1.15930
H         -2.34340        1.70650       -0.95580
H         -2.21690       -1.19630        1.30640
H         -1.36240       -1.74670       -0.22210
H         -0.41680       -1.36780        1.25950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

