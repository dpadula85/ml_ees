%nproc=16
%mem=2GB
%chk=mol_250_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.73240        1.14310       -0.20970
C         -1.40060       -0.15500        0.49630
C         -1.92860       -1.28480       -0.33180
C          0.00070       -0.23660        0.85010
O          0.27170       -0.32270        2.06420
C          1.17320       -0.23170       -0.03800
C          1.32710        1.02980       -0.85690
C          2.45230       -0.54070        0.72360
H         -1.30560        1.17120       -1.23010
H         -2.85240        1.19700       -0.38310
H         -1.40810        2.03900        0.34330
H         -2.00420       -0.09890        1.43870
H         -1.17890       -1.81650       -0.92160
H         -2.39520       -2.03820        0.35290
H         -2.71420       -0.92840       -1.03260
H          1.11860       -1.05560       -0.81330
H          2.42780        1.24620       -1.01960
H          0.86210        1.87460       -0.33350
H          0.95880        0.89920       -1.91610
H          2.21670       -1.15390        1.63730
H          3.15940       -1.15490        0.13670
H          2.95170        0.41780        1.04300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

