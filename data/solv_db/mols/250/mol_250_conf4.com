%nproc=16
%mem=2GB
%chk=mol_250_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24190       -1.25730        0.43920
C          1.25330       -0.29380       -0.17320
C          1.79700        1.09950        0.03510
C         -0.03210       -0.39780        0.56260
O         -0.10350       -1.18650        1.48650
C         -1.23180        0.39470        0.24450
C         -1.75850        0.19000       -1.13660
C         -2.30130        0.11010        1.26120
H          2.11600       -1.29380        1.56110
H          2.13680       -2.28460        0.04660
H          3.29330       -0.96140        0.27930
H          1.13750       -0.54680       -1.24690
H          2.26620        1.16000        1.04670
H          2.60980        1.33950       -0.69840
H          1.00070        1.86380       -0.06200
H         -0.95360        1.46880        0.36730
H         -1.96250       -0.89370       -1.29070
H         -1.11210        0.60940       -1.92520
H         -2.73910        0.70510       -1.20610
H         -3.27960        0.54480        0.92730
H         -2.00090        0.61850        2.20170
H         -2.37760       -0.98840        1.37640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

