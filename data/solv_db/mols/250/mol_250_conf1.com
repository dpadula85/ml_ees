%nproc=16
%mem=2GB
%chk=mol_250_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.16470       -1.39050        0.34470
C         -1.17790       -0.30860        0.70710
C         -1.88100        1.03020        0.45540
C          0.01310       -0.44500       -0.15700
O          0.01000       -1.34650       -0.96860
C          1.18760        0.47910       -0.05390
C          1.82670        0.45630        1.29270
C          2.21060        0.07060       -1.09140
H         -1.74670       -2.39770        0.51480
H         -2.32880       -1.31020       -0.75780
H         -3.13680       -1.25550        0.85120
H         -0.93420       -0.42900        1.78390
H         -1.33550        1.61370       -0.32800
H         -1.95020        1.63470        1.36870
H         -2.90520        0.89360        0.06820
H          0.89970        1.52360       -0.28940
H          2.95140        0.40950        1.19920
H          1.56140       -0.44580        1.88970
H          1.62480        1.39940        1.84240
H          3.10380        0.69850       -1.05730
H          2.46040       -0.99440       -0.89070
H          1.71160        0.11390       -2.10360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_250_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

