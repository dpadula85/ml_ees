%nproc=16
%mem=2GB
%chk=mol_159_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.33840        1.12930        0.00790
C          1.16720        0.02440       -0.00030
C          0.35830       -1.11750       -0.00810
N         -0.92180       -0.70070       -0.00460
C         -0.95700        0.65750        0.00510
H          0.62960        2.17080        0.01520
H          2.24950        0.02260       -0.00060
H          0.72940       -2.13350       -0.01540
H         -1.77030       -1.33180       -0.00890
H         -1.82350        1.27880        0.00970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_159_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_159_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_159_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

