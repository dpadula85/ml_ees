%nproc=16
%mem=2GB
%chk=mol_528_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.49720       -0.16950       -0.04760
C          0.41050        0.58580       -0.00460
C         -0.89800       -0.02010       -0.41470
O         -1.72380        0.08600        0.73120
H          2.42830        0.28410        0.25100
H          1.48880       -1.19840       -0.36270
H          0.43390        1.62820        0.31710
H         -0.77620       -1.09710       -0.59090
H         -1.35410        0.52820       -1.25760
H         -1.50660       -0.62720        1.37870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

