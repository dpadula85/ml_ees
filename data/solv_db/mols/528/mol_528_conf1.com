%nproc=16
%mem=2GB
%chk=mol_528_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.27800        0.70870        0.12930
C          0.70650       -0.46430        0.08140
C         -0.74290       -0.55720       -0.13250
O         -1.25450        0.71240       -0.24960
H          0.69270        1.63370        0.01210
H          2.32620        0.80440        0.28330
H          1.29160       -1.35150        0.19770
H         -0.89990       -1.17920       -1.04030
H         -1.21160       -1.11820        0.70480
H         -2.18610        0.81130        0.01380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

