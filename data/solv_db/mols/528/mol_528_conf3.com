%nproc=16
%mem=2GB
%chk=mol_528_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.61050        0.53820       -0.25830
C         -0.32240        0.80360       -0.44390
C          0.70640        0.29650        0.46640
O          1.64970       -0.51940       -0.14170
H         -1.92230       -0.06960        0.58690
H         -2.39110        0.89980       -0.91490
H         -0.04640        1.40840       -1.29050
H          0.21850       -0.28250        1.27540
H          1.18900        1.16870        0.95830
H          2.52910       -0.07740       -0.23780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

