%nproc=16
%mem=2GB
%chk=mol_528_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43400       -0.29750       -0.56060
C         -0.26520       -0.57600        0.00650
C          0.58790        0.53270        0.44680
O          1.83120        0.56140       -0.16660
H         -1.77550        0.72890       -0.70750
H         -2.11840       -1.06440       -0.91130
H         -0.00290       -1.60690        0.11460
H          0.68130        0.44830        1.56680
H          0.10230        1.50490        0.26830
H          2.39330       -0.23140        0.08060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

