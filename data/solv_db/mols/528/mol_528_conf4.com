%nproc=16
%mem=2GB
%chk=mol_528_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.49670        0.00280        0.14240
C          0.35230        0.50080       -0.25340
C         -0.86440       -0.33150       -0.37330
O         -1.89500        0.14120        0.46170
H          1.55620       -1.04420        0.38850
H          2.39630        0.58620        0.23810
H          0.28740        1.54380       -0.49990
H         -0.61670       -1.36770       -0.03500
H         -1.26420       -0.35250       -1.41570
H         -1.44870        0.32110        1.34660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_528_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

