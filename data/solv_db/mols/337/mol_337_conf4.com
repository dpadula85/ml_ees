%nproc=16
%mem=2GB
%chk=mol_337_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.24250       -0.22120       -0.12240
C          1.03540        0.58660        0.19280
O          1.23100        1.71270        0.61660
C         -0.34340        0.12850        0.03130
C         -1.32160        1.20640        0.44680
C         -0.69040       -0.27110       -1.38120
C         -0.58660       -1.06630        0.93630
H          3.04530        0.45510       -0.48980
H          2.05580       -0.97430       -0.91520
H          2.60240       -0.67530        0.82340
H         -2.35500        0.85720        0.20630
H         -1.26430        1.41160        1.52440
H         -1.14700        2.11480       -0.17560
H         -0.10860       -1.14050       -1.73670
H         -1.75600       -0.60470       -1.36180
H         -0.62280        0.59960       -2.06830
H         -0.69420       -1.99940        0.32780
H          0.24400       -1.14960        1.66370
H         -1.56650       -0.97020        1.48170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

