%nproc=16
%mem=2GB
%chk=mol_337_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.00160       -0.87330        0.20780
C          1.10420        0.31670        0.22340
O          1.59710        1.40140        0.48490
C         -0.33750        0.25490       -0.05830
C         -0.95140       -0.68010        0.98750
C         -0.65340       -0.32890       -1.41220
C         -0.91650        1.64300        0.06310
H          2.78360       -0.70760        0.97460
H          2.50770       -1.01900       -0.75850
H          1.41970       -1.78670        0.52790
H         -0.61200       -1.70190        0.72630
H         -2.04700       -0.66590        0.96510
H         -0.51830       -0.42340        1.97930
H         -0.67590       -1.42930       -1.30620
H         -1.68580        0.00330       -1.66900
H          0.03350        0.01770       -2.20140
H         -1.96160        1.69130       -0.29430
H         -0.80450        1.97500        1.11200
H         -0.28350        2.31280       -0.55200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

