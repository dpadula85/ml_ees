%nproc=16
%mem=2GB
%chk=mol_337_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.20980        0.16810        0.05860
C          0.97620       -0.33040       -0.61120
O          1.10520       -0.94050       -1.65740
C         -0.36110       -0.11590       -0.06420
C         -0.57750       -0.70270        1.29710
C         -0.59740        1.39920        0.00220
C         -1.40380       -0.66460       -1.01110
H          3.03280        0.25130       -0.67740
H          2.03740        1.16240        0.47530
H          2.54870       -0.57620        0.81690
H         -1.10450       -1.68420        1.27630
H         -1.23890        0.00230        1.87110
H          0.33130       -0.83720        1.88370
H         -1.62330        1.58300        0.42590
H         -0.46850        1.87450       -0.97390
H          0.12870        1.84670        0.71460
H         -2.43710       -0.42930       -0.69110
H         -1.29860       -1.76890       -1.11860
H         -1.25920       -0.23760       -2.01690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

