%nproc=16
%mem=2GB
%chk=mol_337_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11340       -0.37780       -0.05170
C          0.76390       -0.81600        0.36360
O          0.64480       -1.87410        0.93320
C         -0.43340        0.02560        0.08880
C         -1.67570       -0.63190        0.60390
C         -0.57030        0.11470       -1.41760
C         -0.19100        1.38950        0.67320
H          2.51530       -1.16870       -0.72920
H          2.80940       -0.31940        0.80630
H          2.11840        0.59440       -0.56660
H         -2.55960       -0.32670       -0.01720
H         -1.86580       -0.48420        1.67140
H         -1.55940       -1.73690        0.40560
H          0.44180        0.31210       -1.81830
H         -0.90850       -0.89130       -1.75390
H         -1.30450        0.89710       -1.68720
H          0.70400        1.41960        1.33680
H         -0.00160        2.12730       -0.13080
H         -1.04120        1.74670        1.28960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

