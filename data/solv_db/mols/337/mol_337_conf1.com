%nproc=16
%mem=2GB
%chk=mol_337_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.09750        0.80960       -0.11320
C          1.02130        0.07160        0.59250
O          1.31250       -0.35640        1.70430
C         -0.31870       -0.16980        0.06480
C         -0.32830       -0.95400       -1.22690
C         -1.14560       -0.95730        1.06060
C         -1.05320        1.15740       -0.13770
H          3.05400        0.26890        0.06860
H          2.10980        1.84660        0.28180
H          1.94560        0.84660       -1.20930
H          0.28050       -0.50960       -2.01450
H          0.06150       -1.95520       -1.00380
H         -1.37970       -0.96490       -1.58680
H         -0.43560       -1.68660        1.53780
H         -1.54090       -0.33080        1.87850
H         -1.95420       -1.54570        0.55190
H         -2.05110        0.98670       -0.55380
H         -0.48310        1.85100       -0.77180
H         -1.19230        1.59200        0.87710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_337_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

