%nproc=16
%mem=2GB
%chk=mol_350_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.53960       -0.60540        1.49000
C         -0.11300        0.07020        0.30930
O          0.52120       -0.21230       -0.87300
C          1.86610        0.12570       -0.88450
O         -1.44550       -0.31470        0.18720
C         -1.56520       -1.67430       -0.07970
O         -0.10990        1.45170        0.51010
C         -0.75960        2.12960       -0.50150
H          0.99030       -1.59120        1.19410
H         -0.23010       -0.86930        2.25680
H          1.27610        0.04550        2.00250
H          2.06310        1.18690       -0.71510
H          2.25190       -0.11840       -1.90660
H          2.45060       -0.50470       -0.18390
H         -2.64420       -1.90340       -0.16250
H         -1.07500       -1.88150       -1.05100
H         -1.12030       -2.31010        0.69120
H         -0.71140        3.21220       -0.26600
H         -0.33940        1.94460       -1.50370
H         -1.84530        1.81900       -0.51360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

