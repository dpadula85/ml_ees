%nproc=16
%mem=2GB
%chk=mol_350_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.47630        0.40820        0.89820
C         -0.26190        0.09950        0.01280
O          0.13530        1.24510       -0.67460
C          0.45940        2.29730        0.13570
O         -0.54750       -0.84640       -0.93400
C         -0.95980       -2.05660       -0.48720
O          0.73540       -0.34010        0.89020
C          1.91390       -0.64980        0.23370
H         -2.30860        0.83070        0.29980
H         -1.75680       -0.53420        1.41620
H         -1.19080        1.11830        1.68690
H          0.75860        3.14200       -0.55090
H          1.30910        2.12200        0.80060
H         -0.42480        2.72390        0.69410
H         -1.14760       -2.70650       -1.37990
H         -0.19880       -2.54560        0.14380
H         -1.90450       -2.06940        0.08600
H          1.82350       -1.44230       -0.51940
H          2.43040        0.24590       -0.18720
H          2.61180       -1.04200        1.01550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

