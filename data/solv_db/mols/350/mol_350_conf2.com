%nproc=16
%mem=2GB
%chk=mol_350_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.54760        0.84660        1.34550
C         -0.18840        0.04460        0.26250
O         -1.24310        0.88750       -0.15810
C         -0.71900        2.02810       -0.75860
O         -0.65310       -1.09710        0.86260
C         -1.45410       -1.87690        0.04770
O          0.61820       -0.15550       -0.82800
C          1.70640       -0.96110       -0.59790
H          1.27150        1.51920        0.88250
H         -0.23060        1.35480        1.95170
H          1.08700        0.12200        2.00040
H         -0.11070        1.67370       -1.64100
H         -1.55640        2.62040       -1.16370
H         -0.12510        2.64930       -0.08760
H         -1.77360       -2.76100        0.61460
H         -2.37190       -1.31230       -0.22190
H         -0.88860       -2.19180       -0.82800
H          1.43370       -1.96140       -0.26860
H          2.28860       -1.00690       -1.55390
H          2.36150       -0.42200        0.13950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

