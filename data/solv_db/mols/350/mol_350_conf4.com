%nproc=16
%mem=2GB
%chk=mol_350_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.31330       -0.22410       -1.13640
C          0.08490        0.02190        0.30070
O          1.38300        0.51220        0.38530
C          2.31930       -0.37360       -0.14010
O         -0.06800       -1.17450        0.99350
C         -1.37470       -1.60240        0.86780
O         -0.74570        0.95840        0.92070
C         -0.67120        2.20240        0.29630
H          0.40190        0.37190       -1.76250
H         -1.34420        0.06970       -1.36460
H         -0.18360       -1.28280       -1.44250
H          2.31710       -1.35730        0.39090
H          2.26680       -0.46840       -1.23190
H          3.32350        0.07020        0.08860
H         -1.51230       -2.57030        1.42420
H         -1.68900       -1.80640       -0.16680
H         -2.10470       -0.85700        1.27970
H          0.33910        2.60870        0.31610
H         -1.44560        2.85380        0.74610
H         -0.98340        2.04760       -0.76530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

