%nproc=16
%mem=2GB
%chk=mol_350_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.33240       -0.16290        1.63620
C         -0.18360        0.11580        0.22900
O         -0.32720        1.49490        0.12710
C         -0.83870        1.87340       -1.11100
O          0.67540       -0.39650       -0.73050
C          1.95480        0.12870       -0.65030
O         -1.40690       -0.49020        0.09120
C         -1.37190       -1.86650        0.17750
H          0.59220       -1.20830        1.79860
H          1.20640        0.52210        1.82700
H         -0.46850        0.20520        2.33450
H         -0.92360        2.97460       -1.11590
H         -0.15020        1.49040       -1.88440
H         -1.84550        1.46190       -1.29640
H          1.97710        1.22490       -0.78230
H          2.44650       -0.23460        0.25560
H          2.52440       -0.29740       -1.51370
H         -0.73330       -2.33890       -0.59580
H         -1.06740       -2.22570        1.18920
H         -2.39260       -2.27100        0.01430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_350_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

