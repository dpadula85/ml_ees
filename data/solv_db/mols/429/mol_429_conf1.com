%nproc=16
%mem=2GB
%chk=mol_429_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.58440       -1.06730        0.38750
C         -2.97410       -0.96840        0.24580
C         -3.54590        0.20490       -0.20660
C         -2.71890        1.25160       -0.50600
C         -1.33030        1.15700       -0.37050
C         -0.75710       -0.01070        0.07820
C          0.69250       -0.11320        0.14210
C          1.36300       -0.81520       -0.86620
C          2.73630       -0.92020       -0.91600
C          3.50730       -0.31910        0.05840
C          2.86520        0.37260        1.06070
C          1.47550        0.47580        1.10430
Cl         0.66630        1.37490        2.39030
Cl         3.82770        1.10920        2.29810
Cl         5.26930       -0.43700       -0.03390
Cl         3.56480       -1.80230       -2.19250
Cl        -0.33180        2.55100       -0.79460
Cl        -3.47900        2.74010       -1.06560
Cl        -5.30030        0.31250       -0.41140
H         -1.09920       -1.97590        0.75100
H         -3.59360       -1.82190        0.48220
H          0.74680       -1.29850       -1.63530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

