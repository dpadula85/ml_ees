%nproc=16
%mem=2GB
%chk=mol_429_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43610       -1.14920        0.30570
C         -2.81010       -1.22410        0.28990
C         -3.55360       -0.15740       -0.16680
C         -2.87710        0.96370       -0.59850
C         -1.48340        1.02820       -0.57690
C         -0.73690       -0.03890       -0.12000
C          0.72460        0.01510       -0.01390
C          1.34620        1.06000        0.64790
C          2.71170        1.08640        0.82850
C          3.52360        0.07500        0.35740
C          2.90360       -0.95890       -0.29980
C          1.53420       -0.99420       -0.48600
Cl         0.78120       -2.30510       -1.37580
Cl         3.89850       -2.25880       -0.91010
Cl         5.26320        0.07770        0.57250
Cl         3.50640        2.40580        1.67090
Cl        -0.67150        2.47540       -1.14360
Cl        -3.81720        2.30980       -1.19360
Cl        -5.32440       -0.21080       -0.16870
H         -0.88150       -1.99380        0.70110
H         -3.36300       -2.09770        0.62690
H          0.76140        1.89180        1.05300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

