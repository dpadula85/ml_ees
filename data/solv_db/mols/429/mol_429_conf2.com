%nproc=16
%mem=2GB
%chk=mol_429_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.47590       -0.96800       -0.76870
C          2.85790       -1.03760       -0.66870
C          3.53490       -0.32780        0.29710
C          2.80240        0.45250        1.16230
C          1.42120        0.52030        1.06140
C          0.74010       -0.18720        0.09630
C         -0.69930        0.00030       -0.00340
C         -1.26090        1.28320       -0.03130
C         -2.61640        1.49890       -0.12400
C         -3.47210        0.42580       -0.19650
C         -2.96970       -0.85140       -0.17810
C         -1.60260       -1.04070       -0.08620
Cl        -1.00840       -2.67850       -0.09350
Cl        -4.04960       -2.24400       -0.22480
Cl        -5.18700        0.70670       -0.28550
Cl        -3.27400        3.11580       -0.16140
Cl         0.59250        1.48240        2.26220
Cl         3.69460        1.34810        2.39310
Cl         5.29070       -0.43930        0.41670
H          0.96990       -1.53810       -1.55450
H          3.37830       -1.66970       -1.39610
H         -0.61850        2.14800       -0.02450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

