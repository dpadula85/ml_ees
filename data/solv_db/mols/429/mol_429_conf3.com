%nproc=16
%mem=2GB
%chk=mol_429_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38630       -1.27910        0.07630
C         -2.75610       -1.39960        0.13350
C         -3.55370       -0.27770        0.10040
C         -2.90900        0.94190        0.00270
C         -1.53090        1.07320       -0.05140
C         -0.73940       -0.06500       -0.01060
C          0.71830        0.02060       -0.06280
C          1.37200        0.77880       -0.99170
C          2.76760        0.86870       -1.03700
C          3.53100        0.17530       -0.13970
C          2.89250       -0.60100        0.79830
C          1.51890       -0.67740        0.84380
Cl         0.73690       -1.58220        2.14860
Cl         3.79780       -1.53510        1.97540
Cl         5.28290        0.27180       -0.15670
Cl         3.53450        1.90960       -2.22780
Cl        -0.78780        2.66090       -0.07600
Cl        -3.94370        2.37370       -0.08220
Cl        -5.31180       -0.37880        0.25730
H         -0.77230       -2.18530        0.04190
H         -3.21640       -2.37050        0.21420
H          0.75520        1.27720       -1.75660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_429_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

