%nproc=16
%mem=2GB
%chk=mol_262_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.56340       -0.55470       -1.27080
C         -1.63330       -0.39180        0.25020
C         -3.05240        0.05520        0.54940
C         -0.65130        0.65540        0.70230
C          0.73560        0.35700        0.37740
C          1.24140        0.79610       -0.83080
C          2.54350        0.55370       -1.21120
C          3.39960       -0.14720       -0.38430
C          2.90660       -0.59090        0.82450
C          1.60150       -0.34480        1.19950
H         -2.38940       -1.27800       -1.52590
H         -1.81520        0.40310       -1.76530
H         -0.60810       -0.99020       -1.58870
H         -1.50470       -1.35810        0.73650
H         -3.25440       -0.03620        1.63740
H         -3.71890       -0.66000        0.01070
H         -3.25870        1.06630        0.15480
H         -0.94100        1.65770        0.34030
H         -0.73570        0.70390        1.82110
H          0.59750        1.35780       -1.52170
H          2.88990        0.92280       -2.17400
H          4.43730       -0.34700       -0.67400
H          3.57720       -1.14410        1.48090
H          1.19620       -0.68580        2.14640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

