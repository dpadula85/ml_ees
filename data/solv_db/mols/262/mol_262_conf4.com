%nproc=16
%mem=2GB
%chk=mol_262_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.09810       -1.03690        0.36490
C         -1.86120        0.21960       -0.39180
C         -1.80810        1.37120        0.61380
C         -0.67740        0.20370       -1.28890
C          0.61000       -0.03300       -0.62170
C          1.34770        1.03440       -0.14740
C          2.56690        0.87040        0.47520
C          3.10740       -0.37650        0.65280
C          2.38840       -1.45820        0.18700
C          1.17010       -1.28040       -0.43390
H         -2.67870       -1.77230       -0.26630
H         -2.78260       -0.84390        1.23950
H         -1.21750       -1.55620        0.73480
H         -2.75540        0.43730       -1.02880
H         -1.15240        1.06390        1.45150
H         -2.83440        1.58060        1.00550
H         -1.44700        2.26700        0.09680
H         -0.66170        1.18820       -1.84180
H         -0.86580       -0.55350       -2.08380
H          0.95100        2.04070       -0.27020
H          3.14270        1.72680        0.84760
H          4.08010       -0.51060        1.14940
H          2.83780       -2.43470        0.33990
H          0.63830       -2.14760       -0.78410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

