%nproc=16
%mem=2GB
%chk=mol_262_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.68180        1.35870        0.14890
C         -1.65530       -0.11710        0.44600
C         -3.02180       -0.65600        0.05940
C         -0.63010       -0.86990       -0.33090
C          0.75050       -0.43990       -0.11520
C          1.55700       -0.98000        0.86760
C          2.86180       -0.56750        1.05840
C          3.41700        0.40720        0.27040
C          2.61070        0.95630       -0.72230
C          1.31350        0.52980       -0.89430
H         -2.65680        1.82040        0.41580
H         -1.48850        1.51220       -0.93360
H         -0.92430        1.87580        0.77810
H         -1.52930       -0.33210        1.52240
H         -3.28850       -0.36090       -0.99140
H         -3.81850       -0.21080        0.69590
H         -3.06460       -1.75950        0.09530
H         -0.90360       -0.92740       -1.42030
H         -0.69510       -1.94270        0.01520
H          1.18510       -1.75820        1.53160
H          3.47530       -1.01240        1.84320
H          4.44440        0.75500        0.39570
H          3.01380        1.74000       -1.37700
H          0.72920        0.97900       -1.67400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

