%nproc=16
%mem=2GB
%chk=mol_262_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11160       -0.84430        0.62280
C          1.82990        0.45440       -0.05500
C          1.85920        0.32550       -1.57690
C          0.62220        1.17740        0.42340
C         -0.65060        0.49990        0.29980
C         -1.13250       -0.29240        1.31400
C         -2.33840       -0.96420        1.22390
C         -3.11260       -0.85460        0.08360
C         -2.64220       -0.05670       -0.95360
C         -1.42730        0.60740       -0.83820
H          1.71930       -1.65790       -0.05430
H          3.21870       -1.06700        0.68110
H          1.73760       -0.93040        1.63930
H          2.70800        1.11990        0.17030
H          2.92650        0.17090       -1.87210
H          1.55990        1.30350       -1.99500
H          1.29550       -0.52650       -1.94410
H          0.56310        2.13570       -0.15630
H          0.80930        1.49490        1.47580
H         -0.51730       -0.35390        2.19790
H         -2.69280       -1.58400        2.04360
H         -4.05600       -1.37510        0.00270
H         -3.25950        0.00970       -1.82100
H         -1.13160        1.20790       -1.68750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

