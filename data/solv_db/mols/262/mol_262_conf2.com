%nproc=16
%mem=2GB
%chk=mol_262_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.73030        1.10610       -0.25060
C          1.62730        0.00710        0.78720
C          2.96500       -0.73300        0.75560
C          0.56780       -0.98880        0.45060
C         -0.78250       -0.44810        0.34080
C         -1.67820       -0.34950        1.37830
C         -2.94590        0.17900        1.18440
C         -3.35040        0.62230       -0.05170
C         -2.45930        0.52950       -1.10610
C         -1.20110        0.00110       -0.89690
H          2.59170        1.75640        0.07480
H          2.07810        0.62880       -1.19260
H          0.81110        1.68540       -0.36500
H          1.54560        0.42520        1.80410
H          3.69710       -0.09890        1.28710
H          3.28610       -0.81360       -0.30960
H          2.81690       -1.71800        1.22900
H          0.57440       -1.77100        1.23690
H          0.84860       -1.47890       -0.50610
H         -1.39200       -0.68460        2.34390
H         -3.66050        0.26140        1.99450
H         -4.32940        1.04200       -0.25020
H         -2.78650        0.88240       -2.08210
H         -0.55410       -0.04240       -1.76410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_262_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

