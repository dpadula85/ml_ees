%nproc=16
%mem=2GB
%chk=mol_157_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.89940        0.15860       -0.16150
C          0.55790        0.03960        0.56680
C         -0.48960       -0.12700       -0.51050
C         -1.88240       -0.25780        0.07890
Br        -2.30480        1.32160        1.09400
H          1.95130       -0.73480       -0.82140
H          2.74200        0.23960        0.53320
H          1.81480        1.04080       -0.83070
H          0.65790       -0.86250        1.19270
H          0.37740        0.91910        1.21300
H         -0.51850        0.78400       -1.14230
H         -0.21610       -1.01110       -1.10620
H         -2.58380       -0.32600       -0.77970
H         -2.00550       -1.18420        0.67360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

