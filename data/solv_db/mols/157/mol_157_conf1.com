%nproc=16
%mem=2GB
%chk=mol_157_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.85100       -0.09730        0.32910
C          0.64470        0.12760       -0.58890
C         -0.57770       -0.01620        0.25520
C         -1.86130        0.17580       -0.49650
Br        -2.10240       -1.06840       -1.92630
H          1.76060       -1.10610        0.78180
H          2.80140       -0.00780       -0.22000
H          1.84430        0.67960        1.13870
H          0.66140       -0.71750       -1.31460
H          0.77460        1.10660       -1.05610
H         -0.54850        0.74020        1.06400
H         -0.58400       -1.03250        0.68520
H         -1.97250        1.21220       -0.89330
H         -2.69150        0.00390        0.24220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

