%nproc=16
%mem=2GB
%chk=mol_157_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.92610        0.02890       -0.17710
C          0.52320       -0.50170       -0.09580
C         -0.49890        0.59210        0.11070
C         -1.86600       -0.00720        0.18170
Br        -2.35600       -0.96700       -1.41730
H          1.93720        1.06970       -0.52370
H          2.48990       -0.06000        0.77240
H          2.51590       -0.55680       -0.93150
H          0.29820       -0.95110       -1.10610
H          0.43370       -1.32150        0.64780
H         -0.33140        1.19690        1.00050
H         -0.46080        1.25130       -0.77270
H         -2.01970       -0.62770        1.09850
H         -2.59140        0.85420        0.27360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

