%nproc=16
%mem=2GB
%chk=mol_157_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.47640        0.13340        0.44800
C         -0.31750       -0.84630        0.44620
C          0.64570       -0.52620       -0.65420
C          1.18400        0.87180       -0.45200
Br         2.10400        1.03940        1.24180
H         -1.83200        0.22010       -0.61850
H         -2.31450       -0.30550        1.06400
H         -1.17110        1.12810        0.83840
H         -0.77390       -1.86300        0.26530
H          0.21710       -0.81880        1.42600
H          1.43870       -1.27700       -0.65800
H          0.09350       -0.51880       -1.61430
H          0.34940        1.59100       -0.44040
H          1.85300        1.17180       -1.29220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

