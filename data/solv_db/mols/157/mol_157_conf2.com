%nproc=16
%mem=2GB
%chk=mol_157_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.93720        0.08240        0.09010
C          0.59110       -0.22890       -0.58210
C         -0.47080        0.17520        0.42130
C         -1.86160       -0.08950       -0.14010
Br        -3.21380        0.42630        1.14060
H          2.78470       -0.28110       -0.52530
H          1.95690        1.20690        0.14240
H          1.92310       -0.34550        1.10400
H          0.49880        0.30110       -1.53770
H          0.58150       -1.31230       -0.78660
H         -0.36320        1.21900        0.73700
H         -0.33880       -0.47650        1.30790
H         -2.04150        0.50120       -1.05810
H         -1.98350       -1.17840       -0.31340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_157_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

