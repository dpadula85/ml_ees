%nproc=16
%mem=2GB
%chk=mol_006_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.32970        1.39710       -0.04050
C          1.00320        1.04100       -0.06590
C          1.32630       -0.31500       -0.02670
C          0.32790       -1.26190        0.03570
C         -1.01320       -0.91740        0.06180
C         -1.31670        0.43370        0.02220
Cl        -2.27750       -2.14670        0.14160
Cl         2.97960       -0.81500       -0.05540
H         -0.65060        2.42120       -0.06800
H          1.77560        1.80060       -0.11550
H          0.54240       -2.33080        0.06750
H         -2.36710        0.69320        0.04310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_006_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_006_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_006_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

