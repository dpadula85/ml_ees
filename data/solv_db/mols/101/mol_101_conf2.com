%nproc=16
%mem=2GB
%chk=mol_101_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33280       -0.45610        0.14450
C          0.85610       -0.57240        0.46800
C          0.19720        0.74020        0.14950
C         -1.27290        0.73720        0.43320
C         -2.04040       -0.28560       -0.34170
Cl        -3.75390       -0.17650        0.09080
H          2.59240        0.54960       -0.26660
H          2.68240       -1.20380       -0.56470
H          2.92120       -0.53440        1.09610
H          0.73190       -0.87460        1.51400
H          0.43970       -1.40030       -0.15690
H          0.66160        1.52130        0.77820
H          0.36740        0.96930       -0.91910
H         -1.46130        0.67310        1.53250
H         -1.66540        1.74190        0.13080
H         -1.69210       -1.32270       -0.09730
H         -1.89670       -0.10630       -1.40860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

