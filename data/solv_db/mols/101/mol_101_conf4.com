%nproc=16
%mem=2GB
%chk=mol_101_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24610        0.19260       -0.61620
C         -0.97990        0.38720        0.22200
C         -0.03770       -0.72830       -0.17950
C          1.26570       -0.68280        0.55650
C          2.02620        0.60190        0.32680
Cl         2.41200        0.84830       -1.38560
H         -3.11470        0.69940       -0.15840
H         -2.05460        0.62040       -1.61410
H         -2.41510       -0.91540       -0.70640
H         -0.58210        1.38620        0.05200
H         -1.26890        0.30990        1.28990
H         -0.54180       -1.67780        0.10110
H          0.09610       -0.75660       -1.27850
H          1.89870       -1.50170        0.15640
H          1.07000       -0.79520        1.63410
H          1.48480        1.48460        0.72280
H          2.98740        0.52720        0.87700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

