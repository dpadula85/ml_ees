%nproc=16
%mem=2GB
%chk=mol_101_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.49660        0.10810       -0.59740
C         -1.27650       -0.17890        0.21380
C          0.01010        0.15240       -0.50930
C          1.14460       -0.18680        0.42490
C          2.49630        0.09480       -0.16920
Cl         3.75770       -0.33220        0.99350
H         -2.92690        1.10130       -0.37530
H         -3.28240       -0.63650       -0.28920
H         -2.35190        0.03310       -1.67800
H         -1.31660        0.47750        1.12550
H         -1.23250       -1.22080        0.52740
H          0.04910        1.18050       -0.88630
H          0.05720       -0.52220       -1.41320
H          1.05390       -1.26400        0.67100
H          1.03310        0.44140        1.33130
H          2.59690        1.18140       -0.37800
H          2.68450       -0.42910       -1.11140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

