%nproc=16
%mem=2GB
%chk=mol_101_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43300       -0.39430        0.15140
C          1.25040        0.37590       -0.44400
C         -0.00050       -0.17750        0.18680
C         -1.22340        0.52780       -0.34510
C         -2.46840       -0.02690        0.28610
Cl        -2.67000       -1.75790       -0.03610
H          2.97120       -0.88720       -0.67750
H          3.07140        0.29630        0.73290
H          2.09900       -1.18300        0.84770
H          1.21590        0.24060       -1.54430
H          1.36900        1.47260       -0.23560
H          0.07100       -0.04000        1.29380
H         -0.01630       -1.26330       -0.04170
H         -1.14840        1.59300       -0.03370
H         -1.20510        0.47080       -1.43410
H         -2.40450        0.19020        1.38190
H         -3.34440        0.56290       -0.08830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

