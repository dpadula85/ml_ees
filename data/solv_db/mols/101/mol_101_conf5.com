%nproc=16
%mem=2GB
%chk=mol_101_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26200       -0.56120        0.31340
C         -0.91930        0.15280        0.41010
C         -0.08840       -0.09860       -0.83090
C          1.22630        0.62020       -0.70330
C          2.03260        0.17850        0.47480
Cl         2.43540       -1.54030        0.43010
H         -2.96840        0.02010        0.94190
H         -2.20240       -1.57050        0.75010
H         -2.63730       -0.57550       -0.70960
H         -0.38760       -0.32080        1.26940
H         -1.08250        1.23000        0.53360
H         -0.63690        0.17160       -1.75270
H          0.13700       -1.18740       -0.84010
H          1.02730        1.71870       -0.56080
H          1.85110        0.54720       -1.60280
H          2.96490        0.81580        0.47230
H          1.51040        0.39950        1.40460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_101_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

