%nproc=16
%mem=2GB
%chk=mol_339_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.65160       -0.69070       -0.04200
C          3.63360        0.67890        0.11360
C          2.45570        1.37940        0.19650
C          1.26860        0.71850        0.12490
C          1.24490       -0.64800       -0.03030
C          2.44120       -1.35140       -0.11360
O          0.02010       -1.34430       -0.10590
C         -1.19040       -0.67550       -0.02660
C         -2.37490       -1.39650       -0.10520
C         -3.58600       -0.73740       -0.02710
C         -3.57860        0.64430        0.12970
C         -2.39310        1.34390        0.20590
C         -1.18230        0.69290        0.12870
O          0.03290        1.35570        0.20060
Cl        -5.14570        1.42490        0.22170
H          4.58260       -1.26540       -0.11000
H          4.59150        1.19410        0.16940
H          2.45610        2.47020        0.32050
H          2.41650       -2.44000       -0.23710
H         -2.39100       -2.46850       -0.22680
H         -4.51510       -1.29490       -0.08770
H         -2.43820        2.40990        0.32720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_339_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_339_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_339_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

