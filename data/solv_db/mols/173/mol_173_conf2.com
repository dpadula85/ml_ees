%nproc=16
%mem=2GB
%chk=mol_173_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.26860        1.21110       -0.33910
C          3.48960       -0.14190       -0.42330
C          2.48700       -1.01430       -0.06970
C          1.26920       -0.56760        0.36570
C          1.05680        0.79390        0.44680
C          2.04960        1.68360        0.09680
C         -0.25730        1.31090        0.91640
C         -1.29580        0.44030        0.25970
C         -2.42200        1.03210       -0.24530
C         -3.38020        0.25600       -0.84800
C         -3.21760       -1.09870       -0.94670
C         -2.06000       -1.68120       -0.42410
C         -1.08940       -0.92020        0.18300
C          0.15900       -1.46960        0.75850
H          4.04750        1.92140       -0.61360
H          4.44400       -0.54080       -0.76470
H          2.62430       -2.09860       -0.12300
H          1.90040        2.77340        0.15440
H         -0.40700        2.35380        0.59230
H         -0.35990        1.29580        2.01280
H         -2.55250        2.09730       -0.16900
H         -4.28000        0.72650       -1.25160
H         -3.97930       -1.69910       -1.42350
H         -1.89100       -2.74420       -0.48000
H          0.05510       -1.40160        1.87200
H          0.34080       -2.51810        0.46310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

