%nproc=16
%mem=2GB
%chk=mol_173_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.49280       -0.51270       -0.05370
C         -3.39170        0.55330       -0.92930
C         -2.18590        1.22120       -1.00230
C         -1.10750        0.82700       -0.21380
C         -1.25220       -0.24150        0.64590
C         -2.46240       -0.93750        0.74110
C         -0.10090       -0.64840        1.47510
C          1.13240       -0.45640        0.65900
C          2.17110       -1.37600        0.77860
C          3.34190       -1.26700        0.05790
C          3.46660       -0.19860       -0.80780
C          2.44750        0.72410       -0.94030
C          1.27890        0.60380       -0.21240
C          0.16550        1.59000       -0.34020
H         -4.43250       -1.05050        0.01800
H         -4.23560        0.85290       -1.53880
H         -2.08130        2.06560       -1.68570
H         -2.52280       -1.76540        1.42880
H         -0.02490        0.04410        2.33950
H         -0.20020       -1.70890        1.78020
H          2.04680       -2.20950        1.46880
H          4.14370       -2.00800        0.17340
H          4.37770       -0.09000       -1.38650
H          2.50800        1.57540       -1.60900
H          0.21880        2.08550       -1.33400
H          0.19180        2.32760        0.48750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

