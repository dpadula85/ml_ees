%nproc=16
%mem=2GB
%chk=mol_173_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.43980        0.39840        0.68700
C          3.28850       -0.98080        0.61470
C          2.15140       -1.56330        0.10510
C          1.14920       -0.73200       -0.33950
C          1.23930        0.64370       -0.29420
C          2.39880        1.17810        0.22510
C          0.11250        1.51610       -0.78770
C         -1.13370        0.82220       -0.30300
C         -2.20000        1.52420        0.20200
C         -3.34080        0.88290        0.65070
C         -3.43210       -0.48590        0.60010
C         -2.37820       -1.22230        0.09800
C         -1.23590       -0.55840       -0.35010
C         -0.08840       -1.32510       -0.90070
H          4.34990        0.82130        1.09460
H          4.08320       -1.65410        0.96720
H          2.07260       -2.65330        0.06610
H          2.48510        2.26340        0.26740
H          0.18910        2.51040       -0.35200
H          0.15000        1.47180       -1.87870
H         -2.15770        2.61290        0.25510
H         -4.16580        1.49410        1.04600
H         -4.32080       -0.99910        0.94890
H         -2.42360       -2.30960        0.04620
H         -0.07780       -1.24950       -2.01770
H         -0.15470       -2.40620       -0.65090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

