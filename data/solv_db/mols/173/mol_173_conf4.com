%nproc=16
%mem=2GB
%chk=mol_173_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.58220        0.01800       -0.76850
C          3.55740       -0.45430        0.53200
C          2.40300       -0.47930        1.26920
C          1.24210       -0.02780        0.71240
C          1.21200        0.45220       -0.57850
C          2.38490        0.46210       -1.28470
C         -0.03730        0.94000       -1.20580
C         -1.23340        0.37710       -0.58920
C         -2.40390        0.31890       -1.32190
C         -3.55260       -0.21780       -0.77410
C         -3.55540       -0.70890        0.52080
C         -2.39090       -0.65440        1.25940
C         -1.23010       -0.10360        0.68450
C          0.00680       -0.04370        1.49610
H          4.51030        0.02510       -1.32530
H          4.46460       -0.82670        1.02060
H          2.38420       -0.85530        2.30450
H          2.36460        0.84200       -2.30750
H          0.00280        2.04080       -1.23430
H         -0.00690        0.57140       -2.27290
H         -2.39970        0.70740       -2.34470
H         -4.49140       -0.27090       -1.34480
H         -4.46410       -1.13810        0.96580
H         -2.34570       -1.02700        2.27760
H          0.05290       -0.87070        2.23320
H         -0.05630        0.92340        2.07630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

