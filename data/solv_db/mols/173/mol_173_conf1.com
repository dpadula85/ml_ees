%nproc=16
%mem=2GB
%chk=mol_173_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.42320        0.99830       -0.39720
C         -3.45320       -0.31610       -0.79220
C         -2.37190       -1.12930       -0.58050
C         -1.24780       -0.65880        0.02260
C         -1.18590        0.65120        0.42810
C         -2.28030        1.45780        0.20980
C          0.03680        1.21370        1.06880
C          1.23890        0.51710        0.49850
C          2.43410        1.18540        0.34490
C          3.53680        0.56500       -0.20360
C          3.48990       -0.74820       -0.62090
C          2.30030       -1.42690       -0.47160
C          1.17900       -0.79510        0.08740
C         -0.09270       -1.53090        0.27640
H         -4.27650        1.64510       -0.56220
H         -4.36560       -0.69000       -1.27940
H         -2.46650       -2.16810       -0.92220
H         -2.28010        2.50170        0.51370
H          0.06310        2.29790        0.80670
H         -0.01200        1.09420        2.16950
H          2.46320        2.23410        0.68090
H          4.47230        1.09190       -0.32260
H          4.34840       -1.25320       -1.05590
H          2.19210       -2.46830       -0.78010
H         -0.12990       -2.36580       -0.45280
H         -0.16940       -1.90290        1.33380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_173_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

