%nproc=16
%mem=2GB
%chk=mol_527_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.85460       -1.03580        0.21000
C         -1.30860       -0.26050       -0.97260
C          0.02320        0.24540       -0.67200
C          0.06880        0.29310        0.85310
C         -1.29320       -0.06650        1.23480
C         -2.13110        1.14260        0.75160
C         -2.21190        0.97770       -0.77660
O         -3.27620        0.99120        0.04940
C          1.27480       -0.46880        1.19480
C          2.35520        0.53910        0.78460
C          2.32890        0.55600       -0.54510
C          1.27630       -0.44370       -1.01130
C          1.63930       -1.42800        0.08690
Cl         3.46530       -1.49980        0.03260
Cl         1.22070       -3.02600        0.03780
Cl         1.50030       -0.99930       -2.61360
Cl         3.30450        1.52600       -1.60480
Cl         3.33830        1.41320        1.91860
Cl         1.45690       -1.05490        2.79620
H         -2.96170       -0.95400        0.16410
H         -1.67380       -2.04420        0.31270
H         -1.48400       -0.74770       -1.91790
H          0.12700        1.29410       -1.09650
H          0.19350        1.38730        1.14550
H         -1.50310       -0.32350        2.25580
H         -1.85340        2.13400        1.14130
H         -2.02140        1.85290       -1.39850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

