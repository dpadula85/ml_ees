%nproc=16
%mem=2GB
%chk=mol_527_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.77500       -0.96080       -0.39430
C         -1.51120        0.49480       -0.41260
C         -0.12200        0.78960        0.03870
C          0.31560       -0.56540        0.55150
C         -0.94560       -1.23090        0.83780
C         -1.70820       -0.26970        1.77610
C         -2.30690        0.81560        0.87500
O         -3.03410       -0.09050        1.54700
C          1.28690       -0.98310       -0.47200
C          2.51770       -0.25440        0.10140
C          2.24260        1.03170       -0.13010
C          0.92990        1.13640       -0.90330
C          1.24910       -0.14630       -1.69370
Cl         0.41580       -0.41280       -3.10530
Cl         2.96290        0.14290       -2.26210
Cl         0.78810        2.54670       -1.85410
Cl         3.24220        2.36680        0.38030
Cl         3.90390       -0.99000        0.85860
Cl         1.55680       -2.66640       -0.68370
H         -1.53830       -1.64260       -1.11410
H         -2.83680       -1.13220       -0.14400
H         -1.87010        1.04720       -1.26380
H         -0.06630        1.58230        0.85130
H          0.75140       -0.35950        1.58590
H         -0.95550       -2.24510        1.17770
H         -1.17140        0.14720        2.63060
H         -2.32160        1.84850        1.22120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

