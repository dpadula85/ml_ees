%nproc=16
%mem=2GB
%chk=mol_527_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67340       -0.81400        1.03690
C         -1.20450       -1.16180       -0.35630
C          0.02150       -0.42310       -0.67230
C         -0.02310        0.78290        0.26560
C         -1.32640        0.65820        0.90280
C         -2.31580        0.92950       -0.25710
C         -2.29580       -0.35780       -1.09780
O         -3.38880        0.13480       -0.48280
C          1.27190        0.73610        0.95830
C          2.19710        1.20390       -0.16570
C          2.23710        0.19820       -1.03530
C          1.37310       -0.94060       -0.46760
C          1.84220       -0.66420        0.94790
Cl         1.67960       -1.75450        2.18250
Cl         3.65690       -0.51220        0.78370
Cl         1.77940       -2.47960       -1.10090
Cl         3.07180        0.09240       -2.54230
Cl         3.01910        2.73170       -0.27640
Cl         1.45630        1.61590        2.41700
H         -2.77410       -0.94340        1.05150
H         -1.34370       -1.33140        1.86370
H         -1.24380       -2.21450       -0.58360
H         -0.02370       -0.06950       -1.75110
H         -0.08510        1.71630       -0.38640
H         -1.54820        1.24740        1.77410
H         -2.16820        1.86560       -0.82510
H         -2.19130       -0.24600       -2.18320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

