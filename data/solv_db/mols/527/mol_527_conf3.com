%nproc=16
%mem=2GB
%chk=mol_527_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.78220        0.23810        1.33550
C         -1.29130       -1.03580        0.68070
C          0.03660       -0.80730        0.11870
C          0.01770        0.66770       -0.24160
C         -1.28380        1.12760        0.21660
C         -2.26280        0.58690       -0.85220
C         -2.22670       -0.92670       -0.54800
O         -3.33470       -0.16750       -0.56620
C          1.31130        1.15010        0.26510
C          2.29610        0.46010       -0.65360
C          2.31390       -0.82840       -0.33090
C          1.30200       -0.98760        0.81720
C          1.74200        0.31770        1.46220
Cl         3.56810        0.34660        1.40300
Cl         1.41870        0.72710        3.02630
Cl         1.55480       -2.40020        1.75340
Cl         3.23230       -2.15480       -0.98380
Cl         3.22020        1.23650       -1.89260
Cl         1.54620        2.84030        0.41940
H         -1.55160        0.44420        2.31550
H         -2.88840        0.22030        1.31150
H         -1.47360       -1.93210        1.24690
H          0.08390       -1.39430       -0.85310
H          0.05420        0.79070       -1.37020
H         -1.43300        2.17640        0.41400
H         -2.11250        0.91330       -1.88100
H         -2.05710       -1.60900       -1.38350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

