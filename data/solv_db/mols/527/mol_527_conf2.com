%nproc=16
%mem=2GB
%chk=mol_527_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.81320       -0.19550        1.09990
C          1.26870        1.08120        0.49230
C         -0.06880        0.81450       -0.02000
C          0.00760       -0.62710       -0.49150
C          1.33450       -1.06990       -0.03700
C          2.27060       -0.47690       -1.10520
C          2.14910        1.02240       -0.78170
O          3.30950        0.32440       -0.81520
C         -1.25040       -1.19190       -0.00960
C         -2.28810       -0.40420       -0.83310
C         -2.34350        0.82820       -0.33720
C         -1.30300        0.85900        0.78290
C         -1.68480       -0.51830        1.27530
Cl        -1.30830       -1.11320        2.77420
Cl        -3.50790       -0.65670        1.22770
Cl        -1.51370        2.14630        1.89100
Cl        -3.35290        2.16060       -0.81070
Cl        -3.22510       -0.99660       -2.16760
Cl        -1.48320       -2.88840       -0.08120
H          1.60300       -0.42590        2.08010
H          2.91940       -0.12170        1.06130
H          1.44900        1.96910        1.06740
H         -0.16930        1.45520       -0.95880
H         -0.04560       -0.68600       -1.62580
H          1.48790       -2.12550        0.11090
H          2.12380       -0.80060       -2.14250
H          1.80830        1.63750       -1.64560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_527_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

