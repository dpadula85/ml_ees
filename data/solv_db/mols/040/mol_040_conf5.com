%nproc=16
%mem=2GB
%chk=mol_040_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.07430       -0.34930        0.71230
C          0.62160        0.74880       -0.21950
C         -0.76850        0.44990       -0.63930
C         -1.47040       -0.60260       -0.25150
H          2.11550       -0.18950        1.06620
H          0.34960       -0.38380        1.55390
H          0.96850       -1.31120        0.16240
H          0.62490        1.73910        0.27510
H          1.24180        0.79390       -1.14380
H         -1.22630        1.17580       -1.32600
H         -2.48320       -0.75200       -0.60280
H         -1.04780       -1.31910        0.41320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

