%nproc=16
%mem=2GB
%chk=mol_040_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.53280        0.38840        0.01180
C         -0.29880       -0.08400       -0.72420
C          0.70190       -0.59910        0.24360
C          1.87810       -0.03620        0.30700
H         -2.46500        0.20640       -0.57180
H         -1.59780       -0.14130        0.97900
H         -1.44230        1.47270        0.25160
H         -0.53600       -0.92970       -1.42060
H          0.09830        0.74210       -1.34670
H          0.45860       -1.42170        0.87920
H          2.11320        0.78570       -0.33250
H          2.62270       -0.38320        0.99380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

