%nproc=16
%mem=2GB
%chk=mol_040_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15210        0.70160        0.20530
C         -0.58290       -0.63260        0.57040
C          0.86920       -0.74530        0.33720
C          1.62190        0.22430       -0.14770
H         -0.56180        1.55240        0.58610
H         -1.22440        0.83690       -0.90740
H         -2.20620        0.72650        0.57350
H         -1.16550       -1.40550        0.00510
H         -0.79670       -0.82990        1.64210
H          1.33260       -1.69380        0.58620
H          2.68350        0.08340       -0.29750
H          1.18250        1.18210       -0.40450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

