%nproc=16
%mem=2GB
%chk=mol_040_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.51030       -0.44730        0.12350
C         -0.42750        0.60420        0.25590
C          0.73970        0.15380       -0.55640
C          1.93260       -0.06690       -0.03500
H         -1.52620       -0.86250       -0.89550
H         -1.21300       -1.29150        0.79490
H         -2.49210       -0.10490        0.48620
H         -0.09590        0.72220        1.29210
H         -0.75270        1.57920       -0.20770
H          0.56020        0.01490       -1.60650
H          2.06470        0.08850        1.03720
H          2.72050       -0.38970       -0.68860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

