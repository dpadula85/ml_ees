%nproc=16
%mem=2GB
%chk=mol_040_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.50360       -0.38110        0.09370
C          0.35510        0.33560       -0.57640
C         -0.73210        0.47030        0.42720
C         -1.90260       -0.08500        0.16410
H          1.38470       -0.45520        1.19370
H          1.55710       -1.43480       -0.27510
H          2.48970        0.09150       -0.12750
H          0.67780        1.33440       -0.93340
H          0.03100       -0.25980       -1.45020
H         -0.58900        1.00460        1.35520
H         -2.05970       -0.61880       -0.75240
H         -2.71560       -0.00160        0.88130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_040_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

