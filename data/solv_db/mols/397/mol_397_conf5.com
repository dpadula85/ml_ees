%nproc=16
%mem=2GB
%chk=mol_397_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.54500       -0.43280        0.07460
C          0.66550        0.67560       -0.39770
C         -0.69260        0.61190        0.29150
C         -1.40780       -0.67710        0.01930
Cl        -1.67070        2.04060       -0.10610
H          2.60650       -0.14680       -0.05210
H          1.33840       -1.34720       -0.50110
H          1.31570       -0.63470        1.14340
H          0.52140        0.57860       -1.50540
H          1.09960        1.66490       -0.21530
H         -0.48140        0.66390        1.38500
H         -1.23800       -1.44710        0.80050
H         -2.49980       -0.45910        0.01990
H         -1.10180       -1.09060       -0.95640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

