%nproc=16
%mem=2GB
%chk=mol_397_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87220        0.08720        0.14950
C         -0.55500        0.02890       -0.59280
C          0.55080        0.01870        0.44210
C          1.91900       -0.03920       -0.21350
Cl         0.33290       -1.29750        1.61760
H         -1.73680       -0.02540        1.23310
H         -2.35390        1.07890       -0.00840
H         -2.57280       -0.70030       -0.18050
H         -0.54470       -0.88910       -1.21160
H         -0.38100        0.90240       -1.23880
H          0.47910        0.97650        0.99780
H          1.93230        0.65200       -1.06000
H          2.64450        0.29370        0.56180
H          2.15800       -1.08680       -0.49620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

