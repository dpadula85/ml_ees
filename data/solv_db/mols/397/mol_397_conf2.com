%nproc=16
%mem=2GB
%chk=mol_397_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.38880        0.71190       -0.08100
C          0.59610       -0.37950       -0.74120
C         -0.53600       -0.88090        0.13050
C         -1.47450        0.24040        0.43900
Cl         0.06400       -1.60850        1.63090
H          1.42200        0.61810        1.03110
H          0.99830        1.71970       -0.30030
H          2.43100        0.68480       -0.43350
H          0.10150        0.04200       -1.66100
H          1.23450       -1.21120       -1.10380
H         -1.07540       -1.65660       -0.44500
H         -1.06650        1.20010        0.02970
H         -1.61890        0.40570        1.53940
H         -2.46490        0.11390       -0.03490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

