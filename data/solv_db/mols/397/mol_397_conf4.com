%nproc=16
%mem=2GB
%chk=mol_397_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.42870        0.53160        0.46330
C          0.75520       -0.21210       -0.65030
C         -0.59530       -0.74770       -0.27190
C         -1.56920        0.29360        0.13660
Cl        -0.41050       -1.97680        0.99190
H          1.11420        1.60630        0.44760
H          2.51630        0.53250        0.25580
H          1.27390        0.11030        1.45300
H          1.41450       -1.05760       -0.93320
H          0.68780        0.46680       -1.52310
H         -1.00370       -1.25580       -1.18630
H         -2.21750        0.64540       -0.70360
H         -2.27820       -0.12080        0.90640
H         -1.11610        1.18440        0.61390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

