%nproc=16
%mem=2GB
%chk=mol_397_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.55880       -0.24760       -0.43690
C         -0.66690       -0.00820        0.73290
C          0.65180        0.59490        0.36770
C          1.46370       -0.24820       -0.57080
Cl         1.58200        0.89740        1.86170
H         -2.51670       -0.67160       -0.06420
H         -1.72580        0.71140       -0.97550
H         -1.14660       -0.96340       -1.14990
H         -1.16440        0.67180        1.44810
H         -0.51010       -0.98120        1.24820
H          0.44890        1.58050       -0.12940
H          0.89280       -0.94110       -1.18220
H          2.17070       -0.84510        0.05370
H          2.07940        0.45060       -1.20330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_397_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

