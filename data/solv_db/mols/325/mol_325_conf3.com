%nproc=16
%mem=2GB
%chk=mol_325_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.65240       -0.07250        0.10220
C          1.32290       -0.07660       -0.63690
C          0.28880        0.45530        0.32770
C         -1.09520        0.50540       -0.28570
C         -2.01780        1.05980        0.80470
C         -1.57820       -0.87580       -0.61160
O          1.04940       -1.40520       -0.94970
H          2.47900       -0.60700        1.05490
H          3.42390       -0.62380       -0.44580
H          2.98980        0.93210        0.33890
H          1.36700        0.51410       -1.55680
H          0.54250        1.48680        0.62590
H          0.21760       -0.15730        1.23940
H         -1.13520        1.14450       -1.19240
H         -3.07610        0.91390        0.56620
H         -1.74340        2.08830        1.08960
H         -1.80860        0.42070        1.70710
H         -1.16660       -1.60960        0.11000
H         -2.69520       -0.88800       -0.47500
H         -1.30860       -1.21010       -1.63940
H          1.29140       -1.99490       -0.17320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

