%nproc=16
%mem=2GB
%chk=mol_325_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.76680        1.15630        0.69530
C          1.30310        0.64100       -0.65840
C          0.40520       -0.53950       -0.43890
C         -0.81000       -0.28000        0.35730
C         -1.75920        0.72160       -0.16220
C         -1.55670       -1.60840        0.50460
O          2.46640        0.28220       -1.33690
H          2.28210        0.32640        1.19650
H          0.90100        1.44980        1.25890
H          2.46560        1.99370        0.60040
H          0.83480        1.48030       -1.16170
H          0.13860       -0.93690       -1.44870
H          0.98390       -1.34970        0.05130
H         -0.53420        0.00760        1.40630
H         -1.77720        1.64780        0.48510
H         -1.57080        1.05980       -1.19880
H         -2.81170        0.35520       -0.14710
H         -1.74640       -1.95890       -0.53850
H         -2.49960       -1.46960        1.06770
H         -0.86780       -2.29970        1.03090
H          2.38630       -0.67880       -1.56270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

