%nproc=16
%mem=2GB
%chk=mol_325_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.87650        0.72930        0.58660
C          1.45690       -0.04000       -0.62000
C          0.16180       -0.76290       -0.50790
C         -1.05470        0.05290       -0.26220
C         -2.30050       -0.86420       -0.20580
C         -1.09340        0.80190        1.04450
O          2.48900       -0.89350       -1.07220
H          3.01560        0.70460        0.70570
H          1.45080        0.38610        1.52350
H          1.69360        1.83100        0.44880
H          1.34500        0.68640       -1.49300
H         -0.01940       -1.30690       -1.48370
H          0.21870       -1.54470        0.31540
H         -1.19120        0.75320       -1.09370
H         -3.13770       -0.24850       -0.54950
H         -2.40570       -1.22020        0.83640
H         -2.09910       -1.68940       -0.92050
H         -0.55320        1.74270        1.06470
H         -2.18280        1.08030        1.18950
H         -0.86650        0.10690        1.89910
H          3.19630       -0.30510       -1.40570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

