%nproc=16
%mem=2GB
%chk=mol_325_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.61720        0.21870        0.43580
C          1.27220        0.57570       -0.11650
C          0.28730       -0.50670        0.20890
C         -1.09500       -0.15880       -0.34840
C         -1.58060        1.14070        0.25830
C         -2.06550       -1.27050       -0.03280
O          1.39550        0.69070       -1.49740
H          2.72410        0.58930        1.44220
H          3.38300        0.69520       -0.20440
H          2.77370       -0.86070        0.40820
H          0.89290        1.53190        0.28260
H          0.19350       -0.62540        1.30560
H          0.57970       -1.46890       -0.22340
H         -1.01240       -0.09830       -1.45210
H         -2.65490        1.08430        0.56050
H         -1.49020        1.98960       -0.46160
H         -0.96640        1.35940        1.17770
H         -2.25440       -1.92030       -0.92760
H         -3.03980       -0.85000        0.28400
H         -1.67360       -1.93610        0.75380
H          1.71370       -0.17970       -1.85360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

