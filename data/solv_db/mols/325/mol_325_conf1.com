%nproc=16
%mem=2GB
%chk=mol_325_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.04110       -0.21240        0.19430
C          1.18970        0.69710       -0.63480
C         -0.08720       -0.04780       -0.98600
C         -0.81520       -0.43090        0.28580
C         -2.07990       -1.16810       -0.03430
C         -1.10110        0.82890        1.07950
O          1.84180        1.11000       -1.79390
H          2.08870        0.08030        1.27020
H          3.08990       -0.17200       -0.19020
H          1.71110       -1.25610        0.09740
H          0.90220        1.56340       -0.00770
H          0.13020       -0.99130       -1.52850
H         -0.76140        0.55220       -1.61710
H         -0.16800       -1.12290        0.88340
H         -1.83420       -1.99040       -0.74460
H         -2.50300       -1.60040        0.90040
H         -2.86880       -0.49000       -0.43340
H         -2.12200        0.71110        1.49710
H         -0.34950        1.03810        1.84060
H         -1.12200        1.68800        0.36630
H          2.81770        1.21320       -1.64430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_325_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

