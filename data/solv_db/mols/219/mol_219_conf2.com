%nproc=16
%mem=2GB
%chk=mol_219_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.21310       -0.06860        0.03930
C         -2.70970       -0.06540        0.34260
C         -2.05480        0.80820       -0.68620
C         -0.57730        0.86820       -0.43260
C          0.01390       -0.49500       -0.51740
C          1.50360       -0.38410       -0.27620
C          2.38120       -0.18320       -1.31940
C          3.73900       -0.08470       -1.07830
C          4.21480       -0.18920        0.22110
C          3.36190       -0.39050        1.28830
C          2.01500       -0.48220        0.99610
H         -4.44390       -0.97940       -0.54850
H         -4.81010       -0.11100        0.97880
H         -4.47460        0.85940       -0.52360
H         -2.60410        0.30690        1.36770
H         -2.34970       -1.12050        0.27430
H         -2.23130        0.41150       -1.72810
H         -2.43140        1.85660       -0.61590
H         -0.06040        1.60390       -1.08680
H         -0.46900        1.25110        0.62500
H         -0.14660       -1.01270       -1.45900
H         -0.37220       -1.14950        0.31290
H          1.95860       -0.10770       -2.32170
H          4.40450        0.07280       -1.91240
H          5.28540       -0.10580        0.36040
H          3.75030       -0.46950        2.29390
H          1.31990       -0.63940        1.80620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

