%nproc=16
%mem=2GB
%chk=mol_219_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.16980        1.27810       -1.27450
C          2.86120        0.24710       -0.20010
C          1.44330       -0.23650       -0.41800
C          1.02280       -1.25760        0.58110
C         -0.37990       -1.72760        0.33540
C         -1.41760       -0.69980        0.39280
C         -1.81420       -0.01420       -0.76130
C         -2.78490        0.95910       -0.75430
C         -3.39860        1.27970        0.43420
C         -3.01710        0.60780        1.59430
C         -2.05750       -0.35030        1.57110
H          2.89760        2.27490       -0.83920
H          4.24520        1.24150       -1.57740
H          2.52510        1.06340       -2.15610
H          3.54060       -0.60580       -0.34800
H          3.03040        0.65370        0.79460
H          0.72010        0.62140       -0.40070
H          1.37710       -0.67510       -1.43300
H          1.19790       -0.94810        1.63060
H          1.66970       -2.17690        0.45490
H         -0.63160       -2.61860        0.98240
H         -0.38940       -2.13290       -0.71970
H         -1.31910       -0.27590       -1.70770
H         -3.08330        1.48120       -1.64710
H         -4.17110        2.04550        0.47450
H         -3.49890        0.85970        2.52820
H         -1.73750       -0.89370        2.46320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

