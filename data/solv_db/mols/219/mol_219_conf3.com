%nproc=16
%mem=2GB
%chk=mol_219_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.88260        1.27540        0.78830
C         -2.60230       -0.10500        1.31010
C         -1.27050       -0.65370        0.81460
C         -1.25650       -0.73070       -0.70160
C          0.03100       -1.26370       -1.22180
C          1.21380       -0.47040       -0.86300
C          1.70260        0.57170       -1.63450
C          2.81640        1.26720       -1.24170
C          3.50220        0.97750       -0.08020
C          3.01900       -0.05990        0.69190
C          1.89690       -0.76070        0.29620
H         -3.52410        1.85960        1.47310
H         -3.32920        1.27080       -0.22340
H         -1.88720        1.79190        0.70440
H         -3.38060       -0.78330        0.87310
H         -2.69750       -0.19810        2.40180
H         -1.11280       -1.63310        1.26610
H         -0.50380        0.06670        1.13820
H         -2.05180       -1.48070       -0.96710
H         -1.57720        0.21500       -1.15620
H         -0.05840       -1.41110       -2.32210
H          0.17040       -2.28770       -0.79280
H          1.15060        0.78330       -2.54180
H          3.18020        2.08040       -1.86410
H          4.37860        1.55450        0.19070
H          3.54830       -0.29700        1.60540
H          1.52470       -1.57880        0.91520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

