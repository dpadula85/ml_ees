%nproc=16
%mem=2GB
%chk=mol_219_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.44490        0.55150        0.49690
C          2.19330       -0.07790        1.07050
C          1.52950       -1.02930        0.14140
C          1.07000       -0.48070       -1.15960
C          0.05730        0.58680       -1.12920
C         -1.23700        0.28870       -0.53700
C         -2.31800       -0.24730       -1.23530
C         -3.53930       -0.44660       -0.66690
C         -3.75170       -0.12040        0.63770
C         -2.72460        0.40880        1.36320
C         -1.48040        0.61060        0.77580
H          4.04730        0.89150        1.36570
H          3.23340        1.45800       -0.10310
H          4.06250       -0.16580       -0.06230
H          2.55690       -0.69830        1.94400
H          1.57470        0.69950        1.50350
H          2.28450       -1.83820       -0.08430
H          0.73650       -1.55730        0.71420
H          1.98760       -0.03230       -1.65000
H          0.82540       -1.34380       -1.81460
H         -0.13010        1.01560       -2.15560
H          0.50400        1.45970       -0.56470
H         -2.17220       -0.50990       -2.26020
H         -4.34260       -0.87280       -1.27900
H         -4.74090       -0.28280        1.09850
H         -2.91750        0.65970        2.39590
H         -0.75360        1.07300        1.44150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

