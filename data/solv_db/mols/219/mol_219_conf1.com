%nproc=16
%mem=2GB
%chk=mol_219_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.13410        0.93200        0.49700
C          3.18590        0.01210       -0.72330
C          2.15760       -1.09070       -0.43630
C          0.84540       -0.39640       -0.28060
C         -0.30340       -1.33300        0.01040
C         -1.49860       -0.47380        0.12950
C         -2.22400       -0.21570       -0.99690
C         -3.35000        0.58030       -0.94530
C         -3.74120        1.11830        0.26290
C         -3.01120        0.85980        1.40230
C         -1.88820        0.05980        1.32320
H          2.82920        0.39280        1.40140
H          4.12200        1.41130        0.61430
H          2.36610        1.74060        0.33850
H          4.18820       -0.38110       -0.86680
H          2.77780        0.58650       -1.57580
H          2.46160       -1.54670        0.53460
H          2.18150       -1.83260       -1.23520
H          0.91170        0.30560        0.56680
H          0.61480        0.15270       -1.21360
H         -0.43370       -2.09870       -0.78570
H         -0.13180       -1.89650        0.94860
H         -1.96300       -0.61470       -1.98520
H         -3.94400        0.80530       -1.80620
H         -4.62690        1.75480        0.35020
H         -3.33210        1.29410        2.36210
H         -1.32790       -0.12620        2.25460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_219_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

