%nproc=16
%mem=2GB
%chk=mol_577_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.60860       -0.63940       -0.34040
C          3.61940        0.68820        0.03270
C          2.42810        1.33370        0.26740
C          1.24170        0.61440        0.11770
C          1.24580       -0.70990       -0.25520
C          2.44290       -1.34860       -0.48820
O          0.02610       -1.39350       -0.39340
C         -1.13700       -0.68700       -0.14280
C         -2.37580       -1.32280       -0.26660
C         -3.56770       -0.68110       -0.03290
C         -3.59180        0.64510        0.34140
C         -2.38240        1.28410        0.46740
C         -1.16320        0.63800        0.23130
O          0.04020        1.31100        0.36720
Cl        -2.37120        2.95840        0.93770
Cl        -5.10170        1.48360        0.64460
Cl        -2.36730       -3.00360       -0.74060
H          4.54380       -1.18660       -0.53600
H          4.58850        1.20030        0.13380
H          2.40820        2.38220        0.56340
H          2.38930       -2.38800       -0.77840
H         -4.52440       -1.17850       -0.13010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_577_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_577_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_577_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

