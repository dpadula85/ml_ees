%nproc=16
%mem=2GB
%chk=mol_273_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.31730       -2.08360        0.80910
C          0.06920       -0.79420        0.04840
O          1.10540        0.09930        0.41730
C          1.99440        0.56360       -0.54260
O          1.82680        0.15450       -1.72000
C          3.10730        1.51400       -0.18530
O         -1.14640       -0.18720        0.43300
C         -2.16670        0.12200       -0.41900
O         -2.05540       -0.13380       -1.62910
C         -3.38710        0.76580        0.16880
H          1.40260       -2.34410        0.64280
H          0.21860       -1.91040        1.88880
H         -0.29520       -2.89540        0.41540
H          0.12760       -1.00400       -1.03020
H          3.26740        2.16760       -1.07070
H          2.74850        2.21080        0.62570
H          3.98280        0.94100        0.11740
H         -4.04110        0.00940        0.64630
H         -3.96490        1.32260       -0.58850
H         -3.11120        1.48210        0.97220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

