%nproc=16
%mem=2GB
%chk=mol_273_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.00340       -0.95230       -2.09390
C         -0.00110       -0.41290       -0.69400
O          1.05260        0.50240       -0.46960
C          2.06820        0.27950        0.44660
O          2.02370       -0.79320        1.09190
C          3.14180        1.28950        0.62630
O         -1.21990        0.15910       -0.32900
C         -1.99340       -0.32590        0.71630
O         -1.54370       -1.32630        1.33900
C         -3.29830        0.30330        1.09080
H          1.05110       -1.22200       -2.36710
H         -0.56670       -1.90770       -2.14010
H         -0.39290       -0.24380       -2.84150
H          0.18560       -1.27830       -0.00370
H          4.05230        1.06160        0.04540
H          2.79660        2.31270        0.38650
H          3.45050        1.27680        1.70260
H         -4.00470       -0.52860        1.32650
H         -3.69390        0.85720        0.21800
H         -3.10450        0.94900        1.97140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

