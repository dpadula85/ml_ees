%nproc=16
%mem=2GB
%chk=mol_273_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.14330       -2.23280       -0.08470
C          0.04760       -0.75030        0.12770
O         -0.89260       -0.21800       -0.81060
C         -2.05890        0.37050       -0.34140
O         -2.27740        0.43010        0.89700
C         -3.09350        0.95010       -1.23720
O          1.28920       -0.10620        0.04950
C          1.85900        0.56470        1.10400
O          1.24340        0.61330        2.21230
C          3.19010        1.22780        0.94080
H          0.98930       -2.69800        0.42540
H          0.15580       -2.45040       -1.16430
H         -0.79080       -2.68130        0.33030
H         -0.33570       -0.59640        1.17720
H         -3.43490        1.90050       -0.74270
H         -3.92180        0.23360       -1.34250
H         -2.66910        1.25150       -2.21810
H          3.51090        1.72190        1.86100
H          3.09180        2.00620        0.15900
H          3.95400        0.46340        0.69000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

