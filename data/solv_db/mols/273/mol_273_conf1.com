%nproc=16
%mem=2GB
%chk=mol_273_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.16360       -2.18470        0.31800
C          0.11830       -0.72070       -0.04680
O          1.29070       -0.03010        0.25770
C          2.03260        0.60170       -0.70850
O          1.62880        0.54480       -1.88990
C          3.30160        1.35210       -0.39650
O         -1.01990       -0.17260        0.62280
C         -2.13900        0.27550       -0.02990
O         -2.16850        0.20160       -1.28520
C         -3.32230        0.84530        0.66580
H          0.68080       -2.76520       -0.49360
H          0.56090       -2.38620        1.31300
H         -0.88790       -2.55950        0.30430
H         -0.12530       -0.68960       -1.14930
H          4.08960        0.62720       -0.18420
H          3.16000        2.04340        0.45460
H          3.56340        1.97840       -1.27310
H         -3.82540        1.55640       -0.02150
H         -4.07200        0.05240        0.89850
H         -3.02990        1.42990        1.56400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

