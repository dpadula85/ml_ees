%nproc=16
%mem=2GB
%chk=mol_273_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.13730       -2.05080       -1.07130
C         -0.06140       -0.82740       -0.18460
O         -1.16180        0.05660       -0.42760
C         -2.04670        0.42710        0.55270
O         -1.86700       -0.04130        1.71170
C         -3.19160        1.35270        0.28020
O          1.10430       -0.06860       -0.53620
C          2.10180        0.17270        0.40300
O          1.92620       -0.30340        1.55540
C          3.33090        0.96510        0.05570
H         -1.14700       -2.47070       -1.13640
H          0.52740       -2.81010       -0.64520
H          0.26320       -1.73840       -2.06540
H         -0.02960       -1.15320        0.85880
H         -2.89400        2.41890        0.48420
H         -3.52350        1.31800       -0.76370
H         -4.04030        1.14080        0.93420
H          3.57470        0.88990       -1.00860
H          3.14470        2.06030        0.25930
H          4.12710        0.66200        0.74370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_273_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

