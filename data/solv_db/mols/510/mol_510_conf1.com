%nproc=16
%mem=2GB
%chk=mol_510_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29100        0.63180       -0.02540
C         -0.13190        1.37840       -0.01380
C          1.12320        0.80350        0.01040
C          1.17560       -0.57670        0.02300
C          0.02850       -1.34290        0.01180
C         -1.22110       -0.75230       -0.01250
Cl        -2.68680       -1.73650       -0.02630
Cl         2.76160       -1.31220        0.05430
Cl         2.57830        1.79020        0.02440
H         -2.26720        1.06410       -0.04420
H         -0.18140        2.47370       -0.02380
H          0.11230       -2.42100        0.02220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_510_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_510_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_510_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

