%nproc=16
%mem=2GB
%chk=mol_489_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.90270        0.71700       -0.07930
O          2.14900       -0.46910       -0.21060
C          0.78910       -0.53380       -0.16980
C          0.17810       -1.76140       -0.31180
C         -1.17930       -1.91950       -0.28360
C         -1.99100       -0.81260       -0.10600
C         -1.42140        0.42820        0.03930
C         -0.04380        0.55930        0.00670
N         -2.22880        1.58580        0.22390
H          3.96670        0.51460       -0.28020
H          2.80180        1.19610        0.90830
H          2.57040        1.44140       -0.86120
H          0.79380       -2.63890       -0.45130
H         -1.62950       -2.89850       -0.39830
H         -3.07410       -0.90310       -0.07870
H          0.44220        1.52750        0.11830
H         -2.91450        1.58060        0.99850
H         -2.11140        2.38640       -0.40830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

