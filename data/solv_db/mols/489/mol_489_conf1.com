%nproc=16
%mem=2GB
%chk=mol_489_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.08590       -0.22490       -0.11510
O          1.89100       -0.87570       -0.43720
C          0.67720       -0.28950       -0.14440
C          0.65130        0.94480        0.46930
C         -0.57760        1.50190        0.74770
C         -1.74060        0.83850        0.41910
C         -1.72730       -0.39570       -0.19450
C         -0.50370       -0.96060       -0.47670
N         -2.96000       -1.04100       -0.51430
H          3.31590        0.58760       -0.83190
H          3.92210       -0.97120       -0.24660
H          3.04810        0.13440        0.93740
H          1.54290        1.49740        0.74310
H         -0.61440        2.46440        1.22620
H         -2.72880        1.26650        0.63320
H         -0.47790       -1.93140       -0.95930
H         -3.42610       -1.66450        0.17800
H         -3.37790       -0.88090       -1.43400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

