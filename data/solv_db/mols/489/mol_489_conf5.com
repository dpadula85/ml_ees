%nproc=16
%mem=2GB
%chk=mol_489_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.93160        0.10470        0.21340
O         -1.99550       -0.94510        0.27450
C         -0.62570       -0.73970        0.14210
C          0.20650       -1.83780        0.21730
C          1.56050       -1.62010        0.08460
C          2.09220       -0.36220       -0.11680
C          1.25860        0.74450       -0.19300
C         -0.10330        0.52790       -0.05980
N          1.80470        2.03210       -0.39990
H         -3.92650       -0.32330       -0.04350
H         -2.67180        0.87900       -0.51470
H         -3.05370        0.61080        1.21280
H         -0.23260       -2.81440        0.37540
H          2.20220       -2.48220        0.14470
H          3.16200       -0.19240       -0.22130
H         -0.73580        1.39230       -0.12090
H          1.51600        2.55950       -1.24200
H          2.47390        2.46630        0.24710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

