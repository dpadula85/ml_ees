%nproc=16
%mem=2GB
%chk=mol_489_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.10310       -0.51200       -0.35610
O          1.82940       -1.04600       -0.31170
C          0.65550       -0.33120       -0.10500
C          0.65750        1.03510        0.08240
C         -0.51780        1.75100        0.28940
C         -1.72510        1.07530        0.30820
C         -1.75930       -0.30300        0.12210
C         -0.56610       -0.96980       -0.07980
N         -3.01070       -1.00780        0.14100
H          3.22150        0.25500        0.46460
H          3.27610       -0.06590       -1.34610
H          3.86060       -1.29520       -0.15020
H          1.60900        1.55740        0.06630
H         -0.52080        2.82850        0.43760
H         -2.66030        1.62520        0.47010
H         -0.57500       -2.03320       -0.22510
H         -3.04920       -1.84810        0.76270
H         -3.82860       -0.71530       -0.41790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

