%nproc=16
%mem=2GB
%chk=mol_489_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.02680       -0.83620       -0.41510
O          1.75020       -1.16180        0.02380
C          0.63980       -0.37880       -0.01240
C          0.67100        0.89680       -0.51990
C         -0.48450        1.69020       -0.54900
C         -1.66630        1.18940       -0.06460
C         -1.71530       -0.06820        0.44020
C         -0.56580       -0.83790        0.46150
N         -2.95220       -0.58330        0.94350
H          3.63680       -1.74160       -0.61780
H          2.91550       -0.33730       -1.42250
H          3.58730       -0.18340        0.28250
H          1.59680        1.29330       -0.90110
H         -0.43040        2.67980       -0.94990
H         -2.59650        1.77280       -0.06630
H         -0.62680       -1.81860        0.86090
H         -3.43390       -0.18540        1.77810
H         -3.35250       -1.38960        0.42440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_489_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

