%nproc=16
%mem=2GB
%chk=mol_487_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.48190       -0.01720        0.00800
C          0.97680        0.01880       -0.02230
N          2.12760        0.03030       -0.04110
H         -0.91560       -0.54480       -0.87430
H         -0.81490       -0.51790        0.95910
H         -0.89200        1.03080       -0.02930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_487_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_487_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_487_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

