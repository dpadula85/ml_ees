%nproc=16
%mem=2GB
%chk=mol_371_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.92540       -0.53150        0.18660
C          0.75250        0.12320       -0.47160
C          0.12480        1.07550        0.53580
C         -1.35980        0.87500        0.43020
C         -1.52620       -0.54540       -0.04240
C         -0.31700       -0.82490       -0.90010
H          2.85030       -0.13430       -0.30040
H          2.02460       -0.24270        1.26470
H          1.88450       -1.63890        0.17760
H          1.03520        0.75300       -1.35230
H          0.45570        0.81740        1.57060
H          0.42050        2.12800        0.35930
H         -1.80880        0.95020        1.43270
H         -1.80960        1.59210       -0.28330
H         -1.50680       -1.26180        0.79990
H         -2.46180       -0.58730       -0.63490
H         -0.62950       -0.65960       -1.96410
H         -0.05390       -1.88790       -0.80840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

