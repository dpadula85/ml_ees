%nproc=16
%mem=2GB
%chk=mol_371_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.08680        0.21580       -0.21620
C          0.64500       -0.04650       -0.35600
C          0.06970       -1.01790        0.64240
C         -1.40750       -0.92570        0.22970
C         -1.60800        0.54900       -0.08060
C         -0.23760        1.16920       -0.13920
H          2.65600       -0.57130       -0.78940
H          2.32270        1.16890       -0.76800
H          2.47000        0.21930        0.80100
H          0.38830       -0.43640       -1.36590
H          0.21730       -0.58210        1.65940
H          0.45810       -2.03620        0.55090
H         -1.46250       -1.48380       -0.74690
H         -2.07900       -1.31010        0.98770
H         -2.15640        0.65330       -1.04070
H         -2.22450        0.94900        0.76750
H         -0.05840        1.64700        0.85110
H         -0.08010        1.83870       -0.98670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

