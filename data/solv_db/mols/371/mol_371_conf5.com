%nproc=16
%mem=2GB
%chk=mol_371_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.08150        0.07420        0.26470
C          0.71550       -0.04410       -0.31980
C         -0.12570        1.18040       -0.18880
C         -1.53020        0.63760       -0.40890
C         -1.45890       -0.84420       -0.09210
C         -0.07930       -1.03350        0.54390
H          1.98900        0.03140        1.37650
H          2.62820        0.98760       -0.07510
H          2.64990       -0.84290       -0.01600
H          0.69810       -0.36340       -1.37910
H         -0.10130        1.59910        0.84990
H          0.07920        1.96320       -0.91930
H         -2.27130        1.10110        0.26700
H         -1.79960        0.78830       -1.48580
H         -2.20140       -1.15780        0.65730
H         -1.51090       -1.40980       -1.04060
H          0.31350       -2.03590        0.41210
H         -0.07630       -0.63120        1.55410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

