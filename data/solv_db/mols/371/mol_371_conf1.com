%nproc=16
%mem=2GB
%chk=mol_371_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05170       -0.51870        0.14140
C          0.65460       -0.10020        0.38640
C          0.18350        1.00190       -0.57300
C         -1.24290        1.12710       -0.02100
C         -1.65770       -0.32620        0.05810
C         -0.38160       -1.13990        0.05470
H          2.61180        0.37890       -0.25570
H          2.16480       -1.38410       -0.51840
H          2.53460       -0.70060        1.14730
H          0.44950        0.28790        1.40270
H          0.75200        1.91160       -0.48350
H          0.07310        0.59180       -1.59010
H         -1.21910        1.59290        0.99320
H         -1.88000        1.73640       -0.65650
H         -2.17610       -0.48430        1.02330
H         -2.29630       -0.52820       -0.82140
H         -0.39910       -1.99530        0.72630
H         -0.22280       -1.45090       -1.01390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

