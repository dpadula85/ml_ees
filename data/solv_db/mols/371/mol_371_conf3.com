%nproc=16
%mem=2GB
%chk=mol_371_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.99380       -0.68670        0.10390
C          0.65630       -0.20820       -0.33350
C         -0.48800       -1.06810        0.20110
C         -1.68970       -0.20240       -0.13410
C         -1.17770        1.22710       -0.17750
C          0.27690        1.13810        0.30050
H          2.41610       -1.30870       -0.74000
H          1.93470       -1.40220        0.96260
H          2.74070        0.11340        0.26750
H          0.55450       -0.07570       -1.41670
H         -0.42640       -1.11970        1.30250
H         -0.49400       -2.03190       -0.30640
H         -2.48680       -0.27150        0.63530
H         -2.06010       -0.52560       -1.14030
H         -1.14180        1.54090       -1.25970
H         -1.77030        1.92050        0.41520
H          0.27390        1.00200        1.39090
H          0.88780        1.95870       -0.07100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_371_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

