%nproc=16
%mem=2GB
%chk=mol_227_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.89380       -0.96940        0.25210
C          0.49190       -1.30690       -0.21310
C          1.48810       -0.20790        0.06100
C          0.90820        1.14090       -0.08220
O          1.57730        2.02650       -0.55430
C         -0.48010        1.41710        0.34560
C         -1.33980        0.34610       -0.33480
H         -0.88590       -0.87570        1.36340
H         -1.57290       -1.76020       -0.11150
H          0.82810       -2.20210        0.32340
H          0.46190       -1.52070       -1.30640
H          2.33650       -0.38400       -0.66020
H          1.95730       -0.34280        1.07610
H         -0.57450        1.30860        1.46570
H         -0.84230        2.40110        0.03150
H         -1.05360        0.38730       -1.42350
H         -2.40640        0.54200       -0.23290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

