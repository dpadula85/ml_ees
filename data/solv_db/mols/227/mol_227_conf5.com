%nproc=16
%mem=2GB
%chk=mol_227_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.06370        1.26340        0.45320
C          1.23160        0.64790       -0.26000
C          1.27170       -0.85780       -0.08880
C         -0.05190       -1.39150       -0.44570
O         -0.14670       -2.28290       -1.25790
C         -1.27050       -0.84830        0.17940
C         -1.25350        0.65670        0.10470
H          0.25220        1.28000        1.55460
H          0.01290        2.33730        0.10380
H          2.15470        1.09520        0.10950
H          1.08030        0.83870       -1.35850
H          1.61220       -1.15190        0.92300
H          2.06170       -1.22430       -0.80730
H         -1.29920       -1.16600        1.23740
H         -2.19680       -1.17270       -0.36420
H         -1.51740        0.95150       -0.91560
H         -2.00500        1.02450        0.83260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

