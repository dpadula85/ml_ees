%nproc=16
%mem=2GB
%chk=mol_227_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.21670       -1.25320       -0.36190
C         -1.34010       -0.39350        0.17490
C         -1.07280        1.02500       -0.33220
C          0.28310        1.39030        0.11990
O          0.44330        2.45520        0.66630
C          1.43780        0.49160       -0.07900
C          1.01520       -0.88120        0.44460
H         -0.44210       -2.33300       -0.30370
H         -0.05480       -0.92260       -1.41950
H         -2.33370       -0.74750       -0.10850
H         -1.28770       -0.32790        1.28660
H         -1.84320        1.68490        0.11880
H         -1.16410        0.98640       -1.44530
H          1.70450        0.34130       -1.14020
H          2.32130        0.81370        0.48980
H          1.81810       -1.62220        0.37330
H          0.73180       -0.70720        1.51630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

