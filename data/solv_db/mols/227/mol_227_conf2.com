%nproc=16
%mem=2GB
%chk=mol_227_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.35010        1.26170        0.12380
C          1.39330        0.21300        0.01130
C          0.90300       -1.14430        0.45240
C         -0.36680       -1.41770       -0.25530
O         -0.46210       -2.40980       -0.93920
C         -1.44840       -0.42740       -0.06400
C         -0.95970        0.93520       -0.49340
H          0.17260        1.55430        1.19070
H          0.74480        2.18470       -0.39470
H          1.75130        0.12290       -1.02690
H          2.23010        0.48540        0.71060
H          1.65910       -1.91210        0.32800
H          0.65180       -1.05430        1.54190
H         -1.62300       -0.35890        1.04980
H         -2.39570       -0.70900       -0.51250
H         -0.88990        1.00220       -1.59680
H         -1.71050        1.67420       -0.12590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

