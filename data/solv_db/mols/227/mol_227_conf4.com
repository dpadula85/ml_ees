%nproc=16
%mem=2GB
%chk=mol_227_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.98890        0.85960        0.12470
C         -1.28070       -0.58010        0.22960
C         -0.13010       -1.50550        0.19780
C          1.10250       -0.73530        0.56920
O          1.77200       -0.98980        1.54680
C          1.43240        0.37370       -0.37250
C          0.26810        1.27130       -0.52330
H         -1.06200        1.34380        1.14340
H         -1.84060        1.33930       -0.44660
H         -2.01360       -0.90910       -0.56510
H         -1.83860       -0.77280        1.19430
H          0.01800       -1.85790       -0.85030
H         -0.19290       -2.35570        0.91280
H          1.68930       -0.12940       -1.35090
H          2.38260        0.89050       -0.07720
H          0.56230        2.28170       -0.10780
H          0.12050        1.47550       -1.62510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_227_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

