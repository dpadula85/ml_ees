%nproc=16
%mem=2GB
%chk=mol_019_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.82490        0.16340       -0.39670
C         -2.71020        0.16880        0.62420
C         -1.58590       -0.75520        0.21020
C         -0.98040       -0.36470       -1.10490
O         -0.44430        0.90530       -1.10120
C          0.55720        1.08460       -0.17820
C          1.73190        0.15510       -0.40100
C          2.81410        0.38220        0.63270
C          3.93600       -0.58970        0.32360
H         -3.90760       -0.87100       -0.78210
H         -3.49580        0.83770       -1.22320
H         -4.79440        0.45710        0.03580
H         -3.14740       -0.19290        1.57560
H         -2.30800        1.18940        0.79610
H         -0.85060       -0.79890        1.03010
H         -2.02250       -1.76810        0.10220
H         -0.26340       -1.15840       -1.39810
H         -1.80180       -0.40310       -1.86840
H          0.17550        0.99080        0.83890
H          0.95210        2.11650       -0.28660
H          2.20310        0.31750       -1.39130
H          1.44390       -0.92020       -0.31480
H          3.20520        1.42580        0.59220
H          2.45630        0.16070        1.65630
H          4.54130       -0.81260        1.20490
H          3.49480       -1.52480       -0.06910
H          4.62610       -0.19550       -0.46210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

