%nproc=16
%mem=2GB
%chk=mol_019_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.21830        0.30780        0.41790
C         -3.29390       -0.55600       -0.40710
C         -1.89390        0.03490       -0.43460
C         -1.31830        0.13940        0.95160
O         -0.05190        0.68460        0.98340
C          0.83700       -0.06640        0.26250
C          2.25140        0.52180        0.31750
C          3.14650       -0.39680       -0.51810
C          4.56240        0.07920       -0.53900
H         -4.12540        1.35280        0.05000
H         -4.01100        0.22440        1.49560
H         -5.25670       -0.05100        0.19800
H         -3.30320       -1.60510       -0.10650
H         -3.66840       -0.50780       -1.45570
H         -1.95080        1.04920       -0.86840
H         -1.28710       -0.67400       -1.04800
H         -1.19390       -0.92280        1.32240
H         -2.01680        0.62650        1.66340
H          0.88120       -1.10730        0.64080
H          0.59460       -0.06700       -0.83990
H          2.62000        0.61510        1.34700
H          2.27300        1.50540       -0.19470
H          3.03760       -1.40540       -0.07800
H          2.68340       -0.42260       -1.52710
H          5.24270       -0.60980        0.02310
H          4.87920        0.15490       -1.60490
H          4.58050        1.09590       -0.08100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

