%nproc=16
%mem=2GB
%chk=mol_019_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.43940        0.77930       -0.04820
C         -2.99940        0.75790        0.33000
C         -2.30750       -0.54900        0.00800
C         -0.84350       -0.36000        0.47440
O         -0.10890       -1.47980        0.23660
C          1.18890       -1.40890        0.66150
C          1.93110       -0.30960       -0.01170
C          3.39270       -0.25970        0.45200
C          4.03580        0.88470       -0.29370
H         -4.62430        1.57790       -0.82510
H         -5.05520        1.08600        0.82200
H         -4.81100       -0.15350       -0.49620
H         -2.88670        0.90040        1.43520
H         -2.44640        1.55230       -0.22060
H         -2.28380       -0.76850       -1.06720
H         -2.70200       -1.40340        0.56880
H         -0.88000       -0.01140        1.51610
H         -0.42830        0.49680       -0.13730
H          1.21170       -1.32850        1.77650
H          1.70460       -2.38190        0.42080
H          1.95190       -0.46990       -1.10440
H          1.47220        0.69890        0.20690
H          3.45350       -0.13990        1.55360
H          3.85680       -1.24220        0.21780
H          3.35630        1.36690       -1.02040
H          4.90480        0.46810       -0.84770
H          4.35610        1.69700        0.40670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

