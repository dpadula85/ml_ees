%nproc=16
%mem=2GB
%chk=mol_019_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.88870        0.18460        0.18380
C         -3.10490        1.13260       -0.67610
C         -1.83600        0.51610       -1.19420
C         -0.93310        0.09400       -0.06190
O          0.24990       -0.48060       -0.52040
C          0.98720       -0.82300        0.61780
C          2.30160       -1.45950        0.26900
C          3.18340       -0.55490       -0.55460
C          3.49140        0.72110        0.19340
H         -4.84990        0.66240        0.44570
H         -4.10400       -0.76560       -0.35460
H         -3.35520       -0.08950        1.10610
H         -2.88350        2.03470       -0.04400
H         -3.75970        1.48300       -1.49060
H         -1.31080        1.29550       -1.78950
H         -2.08970       -0.29820       -1.87880
H         -0.76880        0.96700        0.59240
H         -1.47080       -0.66040        0.57220
H          1.10990        0.03620        1.29180
H          0.37880       -1.57530        1.16040
H          2.83360       -1.65680        1.22230
H          2.12960       -2.43700       -0.21260
H          4.15290       -1.08640       -0.68700
H          2.75390       -0.31590       -1.53910
H          4.08610        0.46080        1.11750
H          2.55890        1.24740        0.52090
H          4.13840        1.36790       -0.41120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

