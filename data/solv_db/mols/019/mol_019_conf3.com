%nproc=16
%mem=2GB
%chk=mol_019_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.87370        0.63700       -0.47820
C         -3.28860        1.13070        0.80150
C         -1.79920        1.16120        0.82690
C         -1.20060       -0.22880        0.60890
O          0.19650       -0.12060        0.64560
C          0.81950       -1.32600        0.45800
C          2.31020       -1.10480        0.51440
C          2.76500       -0.13540       -0.56500
C          4.25160        0.05440       -0.47190
H         -4.63620        1.33730       -0.89770
H         -3.12020        0.45290       -1.26990
H         -4.40000       -0.35200       -0.33840
H         -3.65950        2.15170        0.98390
H         -3.64440        0.50580        1.64280
H         -1.34160        1.86900        0.13450
H         -1.48760        1.45040        1.86180
H         -1.55440       -0.93730        1.38210
H         -1.56870       -0.56370       -0.38340
H          0.60590       -1.75640       -0.56180
H          0.56660       -2.07230        1.23970
H          2.88970       -2.04480        0.44370
H          2.55520       -0.60200        1.48930
H          2.49860       -0.57430       -1.56170
H          2.29880        0.86220       -0.43360
H          4.72960       -0.72820       -1.09640
H          4.51600        1.06260       -0.79000
H          4.57150       -0.12840        0.57510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_019_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

