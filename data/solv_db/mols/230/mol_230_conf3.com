%nproc=16
%mem=2GB
%chk=mol_230_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.21950        0.51790       -0.50790
C         -2.77380        0.67380       -0.01880
C         -2.26570       -0.73120        0.24290
C         -0.84420       -0.73410        0.73380
C          0.09260       -0.09140       -0.29810
C          1.47900       -0.15400        0.30700
C          2.51620        0.45370       -0.61690
C          3.85380        0.35430        0.04800
C          4.86660       -0.29540       -0.46350
H         -4.86500        1.31820       -0.10080
H         -4.63900       -0.43910       -0.12780
H         -4.24310        0.45720       -1.60430
H         -2.73070        1.31100        0.87210
H         -2.22880        1.14570       -0.86150
H         -2.87740       -1.23670        1.01640
H         -2.31550       -1.31020       -0.68180
H         -0.71910       -0.24890        1.72310
H         -0.54950       -1.79490        0.87010
H          0.01910       -0.58360       -1.26260
H         -0.19950        0.98620       -0.32140
H          1.43190        0.49520        1.22990
H          1.76970       -1.15070        0.64520
H          2.30340        1.51520       -0.81680
H          2.56430       -0.17480       -1.52910
H          3.95350        0.86060        1.00520
H          5.81120       -0.33250        0.06440
H          4.80960       -0.81140       -1.41080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

