%nproc=16
%mem=2GB
%chk=mol_230_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.91550        0.21780       -0.64360
C         -3.07330       -0.53130        0.35980
C         -1.68010       -0.80370       -0.16180
C         -0.99840        0.51320       -0.46890
C          0.40390        0.32410       -0.99890
C          1.24620       -0.39530        0.00490
C          2.66280       -0.61590       -0.46110
C          3.38370        0.62400       -0.75340
C          4.49150        0.91130       -0.08150
H         -4.00440       -0.36450       -1.59740
H         -4.94660        0.26660       -0.21400
H         -3.58980        1.25050       -0.81130
H         -3.58170       -1.49340        0.56790
H         -3.05300        0.06400        1.29520
H         -1.15220       -1.35830        0.66110
H         -1.76210       -1.44670       -1.03970
H         -1.60600        1.02120       -1.23710
H         -0.94390        1.10090        0.47930
H          0.30970       -0.27790       -1.94620
H          0.80220        1.29380       -1.29170
H          0.81410       -1.42730        0.14540
H          1.24490        0.14660        0.94980
H          3.18720       -1.15660        0.34910
H          2.71570       -1.26540       -1.35400
H          3.07550        1.35280       -1.50070
H          4.88770        0.23960        0.69540
H          5.08180        1.80970       -0.24040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

