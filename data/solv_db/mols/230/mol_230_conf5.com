%nproc=16
%mem=2GB
%chk=mol_230_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.32840        0.65680       -0.31390
C         -2.91730       -0.79760       -0.50000
C         -1.91500       -1.23240        0.52760
C         -0.64710       -0.41920        0.48470
C          0.04290       -0.52210       -0.85920
C          1.30530        0.28940       -0.90110
C          2.31410       -0.15920        0.14400
C          3.50640        0.69310        0.01410
C          3.91000        1.43090        1.01800
H         -2.86400        1.31490       -1.08670
H         -4.40860        0.79680       -0.26160
H         -2.90520        0.97550        0.65980
H         -3.85250       -1.38450       -0.37070
H         -2.52500       -0.95310       -1.50560
H         -2.37660       -1.10150        1.53060
H         -1.69440       -2.29410        0.40520
H          0.03770       -0.79170        1.28980
H         -0.89320        0.63730        0.69560
H         -0.68440       -0.10030       -1.60310
H          0.26400       -1.56300       -1.13350
H          1.12960        1.37070       -0.73320
H          1.75780        0.20400       -1.90850
H          1.91930       -0.05230        1.16710
H          2.60540       -1.21860       -0.05940
H          4.04830        0.71140       -0.90570
H          3.37880        1.43180        1.96340
H          4.79200        2.07700        0.95870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

