%nproc=16
%mem=2GB
%chk=mol_230_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.13100        0.45510       -0.61690
C          2.88360       -0.92110       -0.02810
C          2.28880       -0.73240        1.34700
C          0.99870        0.02280        1.28880
C         -0.07270       -0.65590        0.45950
C         -1.29120        0.23380        0.51360
C         -2.45080       -0.30830       -0.26860
C         -3.57340        0.65970       -0.12980
C         -4.11740        1.26700       -1.18120
H          3.75090        0.37210       -1.53680
H          2.13950        0.88650       -0.86790
H          3.63040        1.07960        0.15940
H          3.81580       -1.52590        0.00760
H          2.15980       -1.44840       -0.67950
H          2.96970       -0.17270        2.01160
H          2.11930       -1.71900        1.84410
H          1.16770        1.05530        0.91760
H          0.62960        0.10620        2.35190
H         -0.25210       -1.68430        0.76490
H          0.28610       -0.66820       -0.60770
H         -0.99650        1.21240        0.07840
H         -1.59070        0.32130        1.56450
H         -2.74190       -1.30120        0.14710
H         -2.22910       -0.46110       -1.33880
H         -3.97750        0.89290        0.84950
H         -3.74490        1.06350       -2.17870
H         -4.93280        1.97020       -1.08170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

