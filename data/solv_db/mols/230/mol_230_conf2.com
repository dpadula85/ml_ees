%nproc=16
%mem=2GB
%chk=mol_230_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.88190       -0.20160       -0.09560
C          2.75450       -1.21110       -0.04070
C          1.45320       -0.50350        0.28880
C          1.09110        0.52830       -0.72070
C         -0.19470        1.19620       -0.32550
C         -1.35510        0.23310       -0.23330
C         -2.60740        1.00150        0.15440
C         -3.78620        0.11800        0.26090
C         -3.69240       -1.16860        0.02950
H          4.01960        0.19230       -1.13080
H          3.55870        0.68500        0.51640
H          4.83480       -0.61500        0.24570
H          2.95480       -1.90870        0.80330
H          2.68820       -1.79540       -0.96800
H          1.57250       -0.01110        1.27460
H          0.62970       -1.23740        0.38580
H          0.90130        0.05400       -1.72200
H          1.86500        1.33310       -0.81370
H         -0.48510        1.95830       -1.09360
H         -0.06970        1.77100        0.61690
H         -1.21270       -0.52760        0.58900
H         -1.51630       -0.25530       -1.19280
H         -2.47000        1.51720        1.13180
H         -2.83030        1.75330       -0.64510
H         -4.73320        0.55100        0.53420
H         -2.73400       -1.58910       -0.24510
H         -4.51820       -1.86790        0.09180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_230_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

