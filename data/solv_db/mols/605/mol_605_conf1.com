%nproc=16
%mem=2GB
%chk=mol_605_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.62810       -1.21170       -0.10190
C          0.74380       -1.30670        0.03150
C          1.56280       -0.17950        0.08620
C          1.00350        1.07560        0.00630
C         -0.38000        1.18280       -0.12850
C         -1.17120        0.05080       -0.18050
O         -2.54650        0.17500       -0.31470
Cl         2.03680        2.47690        0.07590
Cl         3.29690       -0.30170        0.25510
H         -1.23810       -2.11870       -0.14130
H          1.23070       -2.26550        0.09810
H         -0.76710        2.18320       -0.18700
H         -3.14360        0.23960        0.50080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

