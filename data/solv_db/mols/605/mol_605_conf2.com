%nproc=16
%mem=2GB
%chk=mol_605_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.99270        0.86330       -0.27380
C          0.20300        1.53740       -0.20650
C          1.37490        0.85830        0.06470
C          1.27610       -0.50650        0.25960
C          0.07260       -1.17790        0.19070
C         -1.09350       -0.49510       -0.08020
O         -2.33650       -1.09290       -0.16600
Cl         2.71580       -1.42590        0.60600
Cl         2.90160        1.70630        0.15280
H         -1.89520        1.44210       -0.49070
H          0.27660        2.61630       -0.36160
H         -0.00950       -2.24550        0.34240
H         -2.49330       -2.07990       -0.03740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

