%nproc=16
%mem=2GB
%chk=mol_605_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.57100       -1.19080       -0.03030
C         -0.79330       -1.28790        0.18480
C         -1.59830       -0.16790        0.18760
C         -0.99150        1.05180       -0.03210
C          0.37140        1.13960       -0.24600
C          1.18830        0.02470       -0.25110
O          2.55650        0.13190       -0.46780
Cl        -1.96150        2.52260       -0.04130
Cl        -3.32560       -0.25340        0.45610
H          1.19520       -2.08330       -0.03000
H         -1.26180       -2.24030        0.35560
H          0.87630        2.09030       -0.42190
H          3.17320        0.26270        0.33650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_605_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

