%nproc=16
%mem=2GB
%chk=mol_455_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.50800        0.16520       -0.12760
C          2.95550       -1.08550       -0.16520
C          1.58110       -1.30240       -0.12240
C          0.71700       -0.23370       -0.03870
C          1.28840        1.05050       -0.00020
C          2.65400        1.24070       -0.04390
C          0.42670        2.12730        0.08390
C         -0.93450        1.95510        0.12830
C         -1.49640        0.69640        0.09070
C         -0.64530       -0.39170        0.00650
C         -1.18570       -1.65170       -0.03210
C         -2.57750       -1.83020        0.01310
C         -3.41170       -0.73970        0.09670
C         -2.89000        0.54290        0.13720
H          4.59220        0.33110       -0.16160
H          3.56620       -1.97560       -0.23070
H          1.18770       -2.28460       -0.15370
H          3.06610        2.24320       -0.01270
H          0.86540        3.11900        0.11370
H         -1.61090        2.79080        0.19380
H         -0.63070       -2.53480       -0.09510
H         -3.02430       -2.80940       -0.01580
H         -4.49610       -0.84730        0.13340
H         -3.50540        1.42450        0.20250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_455_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_455_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_455_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

