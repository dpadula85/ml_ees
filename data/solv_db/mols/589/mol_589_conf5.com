%nproc=16
%mem=2GB
%chk=mol_589_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.94580       -0.01150        0.00270
O          0.61750       -0.01590       -0.48770
C         -0.46250       -0.00010        0.38270
O         -0.17850        0.01800        1.61700
C         -1.87820       -0.00420       -0.10030
F         -2.14120        1.09210       -0.88320
F         -2.15870       -1.14640       -0.79970
F         -2.69490        0.06040        1.00320
H          2.66470        0.37960       -0.74850
H          2.01350        0.64580        0.91690
H          2.27240       -1.01770        0.32820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

