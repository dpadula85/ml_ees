%nproc=16
%mem=2GB
%chk=mol_589_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93060       -0.20380        0.23040
O         -0.56220       -0.30530        0.49310
C          0.44390        0.20460       -0.30290
O          0.12420        0.80310       -1.33660
C          1.88900        0.06420        0.03050
F          2.14860       -1.29380        0.08680
F          2.21400        0.68470        1.21680
F          2.66500        0.63080       -0.96130
H         -2.18600       -0.51450       -0.80190
H         -2.34730        0.80700        0.40260
H         -2.45850       -0.87700        0.94240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

