%nproc=16
%mem=2GB
%chk=mol_589_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.87540        0.24050       -0.53120
O          0.52790        0.54570       -0.22020
C         -0.45150       -0.40670       -0.04120
O         -0.13190       -1.61740       -0.16060
C         -1.85020        0.02790        0.28640
F         -2.69120       -1.03740        0.43010
F         -2.26040        0.81330       -0.79810
F         -1.78190        0.83780        1.38390
H          2.16870       -0.77050       -0.16210
H          2.10000        0.37660       -1.60740
H          2.49520        0.99040        0.00990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

