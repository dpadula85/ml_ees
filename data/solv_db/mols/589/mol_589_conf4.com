%nproc=16
%mem=2GB
%chk=mol_589_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.91370        0.15530        0.15170
O         -0.55290        0.48250        0.35890
C          0.44430       -0.28760       -0.21550
O          0.10210       -1.26030       -0.90810
C          1.87090        0.06860        0.01140
F          2.04920        1.35630       -0.48850
F          2.67100       -0.79600       -0.67200
F          2.12470        0.07340        1.38220
H         -2.45670        0.50430        1.06610
H         -1.99980       -0.93840        0.06600
H         -2.33910        0.64190       -0.75240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

