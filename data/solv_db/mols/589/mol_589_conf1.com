%nproc=16
%mem=2GB
%chk=mol_589_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.85980       -0.18440       -0.54760
O         -0.50140       -0.57510       -0.41380
C          0.41940        0.25890        0.20380
O         -0.00420        1.35960        0.63150
C          1.83890       -0.07650        0.37800
F          1.90430       -1.23500        1.13110
F          2.45930        0.90820        1.08420
F          2.49440       -0.28060       -0.81790
H         -2.00470        0.59940       -1.31360
H         -2.51290       -1.03980       -0.75520
H         -2.23320        0.26510        0.41860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_589_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

