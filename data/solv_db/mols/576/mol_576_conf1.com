%nproc=16
%mem=2GB
%chk=mol_576_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.56030       -0.15250       -0.05450
C         -2.91800        0.17080       -1.23000
C         -1.53540        0.18360       -1.19630
C         -0.79180       -0.10180       -0.06820
C         -1.47180       -0.42500        1.09780
C         -2.84360       -0.44580        1.08880
Cl        -3.64380       -0.86450        2.60450
C          0.66310       -0.01870       -0.03690
C          1.42910       -1.02770        0.49960
C          2.82230       -0.91180        0.57580
C          3.41450        0.25030        0.09510
C          2.66340        1.27630       -0.44650
C          1.29450        1.11240       -0.49710
Cl         3.44980        2.72540       -1.04160
Cl         5.16890        0.36570        0.20360
Cl         3.76140       -2.21110        1.26230
Cl        -0.64200        0.50670       -2.69080
H         -4.62830       -0.18130       -0.00940
H         -3.42830        0.40750       -2.14860
H         -0.91510       -0.65730        2.00250
H          0.99250       -1.93810        0.87860
H          0.71890        1.93690       -0.88870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

