%nproc=16
%mem=2GB
%chk=mol_576_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.57630        0.33310       -0.08120
C          3.00640       -0.77550        0.50050
C          1.63640       -0.90910        0.55730
C          0.79050        0.04190        0.04230
C          1.40820        1.15550       -0.54030
C          2.75930        1.31010       -0.60730
Cl         3.48260        2.72610       -1.34880
C         -0.65970       -0.04900        0.02890
C         -1.26410       -1.21320       -0.37690
C         -2.65770       -1.35350       -0.37880
C         -3.45110       -0.28660        0.03960
C         -2.85350        0.88620        0.44720
C         -1.49430        0.99140        0.43810
Cl        -3.88070        2.20220        0.96360
Cl        -5.20020       -0.49600        0.02520
Cl        -3.41320       -2.84580       -0.89770
Cl         0.93910       -2.30260        1.34620
H          4.64190        0.41060       -0.11150
H          3.61530       -1.55710        0.92100
H          0.75570        1.91590       -0.95000
H         -0.72800       -2.08810       -0.74130
H         -1.00900        1.90320        0.75430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

