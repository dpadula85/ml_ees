%nproc=16
%mem=2GB
%chk=mol_576_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.59440       -0.31370       -0.28620
C         -2.85990        0.15700       -1.34150
C         -1.47110        0.26980       -1.25070
C         -0.77990       -0.07840       -0.11190
C         -1.55130       -0.55780        0.95180
C         -2.92250       -0.67010        0.86080
Cl        -3.79610       -1.28710        2.26360
C          0.66310        0.07630        0.04800
C          1.31350        1.23930       -0.29770
C          2.70420        1.35720       -0.17680
C          3.46590        0.30880        0.29580
C          2.81520       -0.85520        0.64410
C          1.44410       -0.95900        0.51930
Cl         3.75990       -2.20050        1.24810
Cl         5.21540        0.46270        0.44670
Cl         3.51660        2.85200       -0.62720
Cl        -0.52770        0.80730       -2.62820
H         -4.66840       -0.42230       -0.29890
H         -3.35600        0.44530       -2.25610
H         -1.00260       -0.83250        1.84430
H          0.72980        2.07580       -0.63680
H          0.90240       -1.87490        0.78980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_576_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

