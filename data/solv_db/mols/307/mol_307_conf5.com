%nproc=16
%mem=2GB
%chk=mol_307_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.46410       -0.74430        0.45840
C         -1.13220       -0.03720        0.58120
C         -1.18780        1.36280        0.03380
C         -0.03430       -0.79190       -0.14900
C          1.25690        0.00460        0.03930
O          2.26520       -0.68870       -0.63060
C          3.55840       -0.20400       -0.63650
O          3.77360        0.87050       -0.02030
H         -2.35450       -1.79140        0.11850
H         -2.91650       -0.78060        1.46600
H         -3.10150       -0.14290       -0.20530
H         -0.81870        0.02530        1.64360
H         -2.20860        1.79840        0.21900
H         -1.02310        1.40690       -1.06060
H         -0.46050        1.97280        0.58790
H          0.08150       -1.82010        0.20070
H         -0.22630       -0.82500       -1.24440
H          1.48950        0.06700        1.14360
H          1.12470        1.04170       -0.29040
H          4.37850       -0.72390       -1.15400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

