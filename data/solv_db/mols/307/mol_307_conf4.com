%nproc=16
%mem=2GB
%chk=mol_307_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.64940       -1.08590       -0.98300
C         -0.77090       -0.36830        0.04960
C         -1.67490        0.58180        0.79880
C          0.28170        0.37180       -0.73990
C          1.23300        1.13270        0.11010
O          1.93660        0.32300        1.00700
C          2.77380       -0.70150        0.64840
O          2.97050       -0.98700       -0.54970
H         -2.22210       -1.90510       -0.52500
H         -2.33660       -0.35560       -1.44440
H         -1.02170       -1.48920       -1.80390
H         -0.37650       -1.12250        0.72790
H         -1.26540        1.60420        0.66900
H         -1.78930        0.30110        1.86840
H         -2.70690        0.61610        0.37280
H         -0.28790        1.09300       -1.40070
H          0.81890       -0.28150       -1.43870
H          0.77500        2.01790        0.60750
H          2.00670        1.56820       -0.59010
H          3.30530       -1.31320        1.38320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

