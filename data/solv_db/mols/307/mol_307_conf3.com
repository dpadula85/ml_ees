%nproc=16
%mem=2GB
%chk=mol_307_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.01170        1.42920        0.39000
C         -0.98790        0.09310       -0.29050
C         -2.41510       -0.41250       -0.34210
C         -0.10280       -0.90940        0.39910
C          1.32330       -0.49880        0.48880
O          1.90400       -0.29320       -0.80790
C          3.22720        0.09350       -0.92240
O          3.88620        0.25410        0.13250
H         -1.92860        1.96080        0.02080
H         -0.14020        2.04510        0.16780
H         -1.18610        1.26140        1.46990
H         -0.66700        0.25100       -1.33900
H         -2.57870       -0.98090        0.60960
H         -3.15340        0.41530       -0.31940
H         -2.60990       -1.07990       -1.19150
H         -0.18670       -1.87570       -0.10760
H         -0.49800       -1.04310        1.42600
H          1.89470       -1.34420        0.92740
H          1.50180        0.36870        1.14840
H          3.72870        0.26580       -1.86010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

