%nproc=16
%mem=2GB
%chk=mol_307_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23580        0.17950       -1.48910
C         -1.09410        0.58710       -0.06220
C         -2.00970       -0.25710        0.77420
C          0.33330        0.55510        0.43710
C          0.95400       -0.78990        0.35200
O          2.26120       -0.76640        0.82870
C          3.27020       -0.04820        0.26840
O          3.06690        0.64750       -0.74460
H         -1.90560        0.91230       -2.02090
H         -1.77350       -0.80530       -1.58670
H         -0.30520        0.10200       -2.04910
H         -1.43340        1.65520        0.05550
H         -1.54650       -0.47140        1.74820
H         -2.26320       -1.20910        0.24500
H         -2.99860        0.26580        0.89860
H          0.29420        0.84870        1.50470
H          0.88840        1.31360       -0.09180
H          0.31860       -1.49900        0.96880
H          0.87980       -1.18610       -0.69580
H          4.29900       -0.03440        0.65920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

