%nproc=16
%mem=2GB
%chk=mol_307_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.76330        1.52960        0.16160
C         -1.24310        0.12550        0.33650
C         -1.82200       -0.46190       -0.93460
C         -0.22390       -0.79390        0.95100
C          1.03460       -0.91650        0.15320
O          1.67040        0.34790        0.00570
C          2.85800        0.46450       -0.70250
O          3.33520       -0.59080       -1.19930
H         -1.66870        2.20130        0.20460
H         -0.22840        1.70210       -0.79080
H         -0.13230        1.79760        1.03370
H         -2.10230        0.16540        1.06440
H         -2.93850       -0.36390       -0.96520
H         -1.59980       -1.56550       -1.00320
H         -1.45460        0.06060       -1.84050
H         -0.69610       -1.79200        1.05380
H          0.03300       -0.45380        1.97420
H          1.73040       -1.56380        0.73010
H          0.89240       -1.32030       -0.85190
H          3.31910        1.42780       -0.80060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_307_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

