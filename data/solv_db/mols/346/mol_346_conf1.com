%nproc=16
%mem=2GB
%chk=mol_346_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.15910       -0.99310        0.15480
C         -1.78070        0.47050        0.18220
C         -2.64320        1.19900       -0.79110
C         -0.29990        0.62060       -0.09050
C          0.45330       -0.11950        0.96990
O          1.83940       -0.07190        0.85170
C          2.59020       -0.52260       -0.20340
O          1.98460       -1.03660       -1.17720
C          4.08010       -0.43630       -0.25780
H         -1.53370       -1.57420       -0.56450
H         -3.23120       -1.04930       -0.11670
H         -2.00180       -1.36810        1.20700
H         -1.97220        0.90710        1.19860
H         -2.78050        2.28160       -0.52390
H         -2.28620        1.08700       -1.83250
H         -3.67950        0.75700       -0.70450
H         -0.10550        0.26240       -1.11780
H         -0.06820        1.69960       -0.08140
H          0.18410        0.34290        1.94880
H          0.05420       -1.16650        1.03090
H          4.42430       -0.56890        0.79900
H          4.51500       -1.27560       -0.82420
H          4.41660        0.55480       -0.61240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

