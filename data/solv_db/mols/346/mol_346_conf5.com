%nproc=16
%mem=2GB
%chk=mol_346_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.73110        1.11650        0.16110
C         -1.73440        0.17380       -0.47770
C         -2.19390       -1.27090       -0.36480
C         -0.39120        0.25350        0.26250
C          0.54110       -0.70320       -0.43300
O          1.82620       -0.77920        0.09300
C          2.66760        0.31020        0.11640
O          2.25930        1.39890       -0.35510
C          4.06050        0.21260        0.69590
H         -3.48010        0.56490        0.74350
H         -3.24210        1.69750       -0.61580
H         -2.14750        1.84130        0.78430
H         -1.62520        0.43410       -1.54920
H         -1.76140       -1.77820        0.50710
H         -1.82530       -1.79890       -1.27450
H         -3.30420       -1.28290       -0.31440
H         -0.04560        1.28330        0.33260
H         -0.63360       -0.14680        1.29440
H          0.02390       -1.69900       -0.44900
H          0.58480       -0.41360       -1.52700
H          4.71280        0.70430       -0.06820
H          4.06480        0.71800        1.66650
H          4.37450       -0.83630        0.77140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

