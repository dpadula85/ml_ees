%nproc=16
%mem=2GB
%chk=mol_346_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.68710        1.07340        0.06890
C         -1.82850       -0.17140        0.25760
C         -2.07780       -1.03750       -0.95600
C         -0.39720        0.28390        0.35350
C          0.56790       -0.83540        0.54160
O          1.90670       -0.50020        0.64040
C          2.60110        0.12570       -0.36590
O          1.97900        0.39600       -1.41690
C          4.05970        0.47530       -0.21180
H         -2.67020        1.39340       -1.01440
H         -2.30950        1.91960        0.66280
H         -3.73550        0.84270        0.29530
H         -2.15120       -0.69410        1.18010
H         -1.85950       -0.49940       -1.89260
H         -1.52820       -1.97620       -0.95760
H         -3.16540       -1.29880       -0.94110
H         -0.34390        0.91610        1.28650
H         -0.17270        0.97380       -0.47340
H          0.30540       -1.36280        1.50830
H          0.34550       -1.58830       -0.25260
H          4.62790       -0.46250       -0.34140
H          4.19080        0.83080        0.80980
H          4.34280        1.19590       -0.99070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

