%nproc=16
%mem=2GB
%chk=mol_346_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.80660        1.21550        0.19620
C         -1.28670       -0.07540       -0.40630
C         -2.58640       -0.81770       -0.84060
C         -0.60920       -0.97250        0.53160
C          0.61040       -0.57370        1.25760
O          1.77670       -0.30760        0.61510
C          2.28850        0.57750       -0.24780
O          1.55240        1.50550       -0.65470
C          3.72230        0.49820       -0.74920
H         -1.02120        1.83560        0.64040
H         -2.26210        1.79510       -0.63400
H         -2.62840        0.97090        0.89970
H         -0.77500        0.11880       -1.35500
H         -2.32750       -1.63280       -1.52310
H         -3.02020       -1.19320        0.10740
H         -3.27320       -0.07490       -1.25530
H         -0.38220       -1.92750       -0.06490
H         -1.38120       -1.36260        1.28720
H          0.85290       -1.42810        1.99240
H          0.34490        0.24450        2.02000
H          4.33650       -0.11410       -0.06490
H          3.76530        0.16660       -1.78480
H          4.10990        1.55200       -0.66680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

