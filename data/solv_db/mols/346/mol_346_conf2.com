%nproc=16
%mem=2GB
%chk=mol_346_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88240        1.01660       -0.80480
C         -1.19860       -0.23940       -0.30790
C         -2.21590       -1.35760       -0.35290
C         -0.61580       -0.04820        1.07450
C          0.38350        1.05670        1.00900
O          1.45390        0.87080        0.14270
C          2.34770       -0.15990        0.29250
O          2.16390       -0.93990        1.25640
C          3.50590       -0.35570       -0.65620
H         -1.13960        1.77840       -1.15240
H         -2.56360        0.70750       -1.63230
H         -2.48580        1.49040       -0.01800
H         -0.37210       -0.52600       -0.97930
H         -1.73990       -2.34580       -0.38840
H         -2.80670       -1.22880       -1.29090
H         -2.92220       -1.29790        0.52190
H         -1.44660        0.24870        1.76280
H         -0.24680       -0.99870        1.44340
H          0.68240        1.30790        2.05120
H         -0.13690        1.99950        0.65660
H          3.86350        0.67860       -0.88950
H          3.07280       -0.82480       -1.55640
H          4.29910       -0.93310       -0.18200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_346_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

