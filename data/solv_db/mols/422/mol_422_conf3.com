%nproc=16
%mem=2GB
%chk=mol_422_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.73840       -0.05540        0.03360
C         -0.32080        0.28570        0.32830
C          0.66850       -0.27770       -0.33640
C          2.05740        0.03280       -0.07350
O          2.34400        0.86550        0.81540
H         -1.78860       -0.98450       -0.55860
H         -2.15710        0.74930       -0.58370
H         -2.30290       -0.20210        0.99420
H         -0.05800        1.00370        1.09400
H          0.42740       -0.99540       -1.10250
H          2.86850       -0.42190       -0.61070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

