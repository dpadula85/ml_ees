%nproc=16
%mem=2GB
%chk=mol_422_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.73730       -0.03180       -0.00590
C         -0.31550        0.40420        0.10340
C          0.67680       -0.42180       -0.10930
C          2.03230        0.06000        0.01190
O          3.00150       -0.70500       -0.18520
H         -1.98610       -0.06670       -1.08540
H         -2.37980        0.71080        0.50860
H         -1.87610       -1.01160        0.49590
H         -0.09200        1.43010        0.36510
H          0.45380       -1.45070       -0.37230
H          2.22230        1.08230        0.27310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

