%nproc=16
%mem=2GB
%chk=mol_422_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.70860        0.30110       -0.24220
C         -0.29560       -0.03020       -0.56550
C          0.66360       -0.05070        0.34880
C          2.03530       -0.37820        0.00220
O          2.89260       -0.37840        0.91970
H         -1.81770        0.84860        0.69410
H         -2.34330       -0.60480       -0.27650
H         -2.05970        0.98250       -1.06740
H         -0.01480       -0.26770       -1.58270
H          0.39520        0.18300        1.35740
H          2.25290       -0.60500       -1.02280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

