%nproc=16
%mem=2GB
%chk=mol_422_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69890       -0.11760        0.00540
C         -0.25960       -0.38170       -0.16140
C          0.66830        0.48580        0.19810
C          2.05750        0.15910        0.00480
O          2.37670       -0.92890       -0.49550
H         -1.92020        0.42270        0.96590
H         -2.28450       -1.06210        0.02140
H         -2.10890        0.46470       -0.86930
H          0.02940       -1.32830       -0.59710
H          0.33200        1.42800        0.63340
H          2.80830        0.85820        0.29440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_422_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

