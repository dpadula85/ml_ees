%nproc=16
%mem=2GB
%chk=mol_327_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45370       -1.57030        0.03600
C         -1.45670       -0.12620        0.53620
C         -0.78440        0.76370       -0.48280
C         -0.82250       -0.10140        1.84970
N          0.45540       -0.25820        1.95110
O          1.17060       -0.44500        0.79280
C          2.45580        0.08240        0.62980
O          2.90870        0.73240        1.60340
N          3.20520       -0.09910       -0.55230
C          4.52820        0.47810       -0.64510
S         -3.19190        0.35660        0.69630
C         -4.03240        0.43100       -0.89070
H         -0.46850       -1.86800       -0.37070
H         -2.25030       -1.62670       -0.75360
H         -1.67660       -2.19890        0.91020
H         -0.25180        1.56920        0.03030
H         -1.58200        1.22570       -1.11660
H         -0.16830        0.16720       -1.18060
H         -1.45240        0.04450        2.73630
H          2.86500       -0.63180       -1.37480
H          4.52660        1.45080       -1.14440
H          5.24010       -0.17610       -1.20780
H          4.94620        0.52550        0.40180
H         -5.06510        0.02440       -0.75260
H         -4.12900        1.47410       -1.26780
H         -3.51620       -0.22400       -1.61660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

