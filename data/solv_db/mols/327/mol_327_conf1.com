%nproc=16
%mem=2GB
%chk=mol_327_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.09150       -0.72200        0.19890
C         -1.56130       -0.65590        0.08200
C         -1.09520       -2.10180        0.29830
C         -1.20130       -0.14870       -1.24600
N         -0.07080        0.44220       -1.38890
O          0.75080        0.57370       -0.29460
C          2.12310        0.37610       -0.36070
O          2.57860        0.06510       -1.49540
N          2.99570        0.50320        0.74250
C          4.40850        0.27750        0.57390
S         -0.98440        0.41150        1.41070
C         -1.57230        2.08850        1.10730
H         -3.36330       -1.01510        1.21950
H         -3.43330       -1.55560       -0.46750
H         -3.56240        0.22690       -0.11000
H         -1.54950       -2.68050       -0.53950
H          0.00110       -2.17690        0.33510
H         -1.57780       -2.42610        1.25790
H         -1.88880       -0.27820       -2.09710
H          2.63460        0.75540        1.67950
H          4.94050        0.14720        1.54720
H          4.92820        1.13170        0.07590
H          4.62710       -0.61110       -0.05590
H         -2.09090        2.43600        2.03600
H         -0.69480        2.78370        0.99830
H         -2.25050        2.15340        0.23380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

