%nproc=16
%mem=2GB
%chk=mol_327_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.77710       -1.03060        0.56080
C         -1.52060       -0.30000        0.05190
C         -0.80620       -1.24080       -0.82340
C         -0.87030        0.27680        1.19380
N          0.40120        0.36890        1.29330
O          1.13390       -0.11500        0.24860
C          2.50870        0.08660        0.26980
O          2.98650        0.71140        1.26380
N          3.36430       -0.37570       -0.75780
C          4.77990       -0.12330       -0.65430
S         -2.18040        1.04970       -1.00080
C         -3.07170        2.17300        0.12430
H         -3.52930       -1.04940       -0.23880
H         -3.13540       -0.48400        1.47140
H         -2.49350       -2.04340        0.91710
H         -0.22610       -0.76620       -1.66960
H         -0.24480       -2.05450       -0.32090
H         -1.60500       -1.81000       -1.40040
H         -1.48270        0.65660        2.00570
H          2.99580       -0.89260       -1.58020
H          4.97960        0.87060       -1.14410
H          5.31960       -0.89120       -1.24690
H          5.14140       -0.11140        0.39850
H         -3.60810        1.51640        0.84290
H         -2.29420        2.75940        0.65810
H         -3.76550        2.81870       -0.46290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

