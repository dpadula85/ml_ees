%nproc=16
%mem=2GB
%chk=mol_327_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.42460       -1.42170        0.22890
C          1.40670       -0.33350        0.00600
C          1.66400       -0.26850       -1.50170
C          1.10470        0.95450        0.58130
N         -0.08820        1.42110        0.56400
O         -1.04020        0.63080       -0.02810
C         -2.38130        0.74090        0.31410
O         -2.65370        1.60730        1.19540
N         -3.40640       -0.04370       -0.25950
C         -4.77650        0.14680        0.16530
S          3.00310       -0.88960        0.72830
C          4.23000        0.39210        0.48080
H          0.98480       -2.37810        0.31130
H         -0.16650       -1.33940        1.16280
H         -0.23640       -1.59340       -0.67220
H          0.67710       -0.40950       -1.99450
H          2.16980        0.66560       -1.78180
H          2.25710       -1.18240       -1.74140
H          1.88630        1.55450        1.02340
H         -3.19860       -0.75460       -0.98000
H         -5.26140        1.01650       -0.31890
H         -5.38330       -0.75550       -0.11820
H         -4.78770        0.27110        1.27860
H          5.15340        0.19950        1.04350
H          4.52870        0.35960       -0.60680
H          3.88970        1.40960        0.68280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

