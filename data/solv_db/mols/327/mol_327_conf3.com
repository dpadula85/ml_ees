%nproc=16
%mem=2GB
%chk=mol_327_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.59030       -0.71050        1.37370
C         -1.48370       -0.78910        0.31390
C         -2.14730       -1.19810       -0.98210
C         -0.56640       -1.86520        0.70740
N          0.68960       -1.65210        0.72840
O          1.14090       -0.40370        0.37590
C          2.48860       -0.26180        0.11630
O          3.24660       -1.26300        0.20930
N          3.09370        0.96490       -0.25710
C          4.49790        1.10600       -0.52580
S         -0.71920        0.82680        0.23010
C         -1.94110        2.01290       -0.41580
H         -2.92720       -1.73850        1.65160
H         -2.11840       -0.18400        2.24000
H         -3.41040       -0.08270        1.00160
H         -2.32060       -0.35550       -1.64890
H         -1.47700       -1.91110       -1.54530
H         -3.06190       -1.78560       -0.74250
H         -0.95490       -2.82270        0.97450
H          2.48930        1.80860       -0.34080
H          4.95730        1.84630        0.16610
H          5.03380        0.13190       -0.38650
H          4.71770        1.46760       -1.55330
H         -2.40210        1.66210       -1.36050
H         -2.75240        2.19460        0.30540
H         -1.48240        3.00180       -0.63560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_327_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

