%nproc=16
%mem=2GB
%chk=mol_482_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.74620       -0.18250        0.22020
C          2.28140       -0.54450       -0.08640
O          1.82150        0.44230       -0.92300
C          0.51430        0.31180       -1.35290
C         -0.38500        0.31930       -0.13820
O         -1.75960        0.19260       -0.52620
C         -2.68490        0.18720        0.51070
O         -2.32870        0.28830        1.71900
C         -4.14470        0.06270        0.26180
H          3.71530        0.66500        0.92640
H          4.18540        0.20410       -0.71870
H          4.28870       -1.02740        0.65890
H          1.76500       -0.57040        0.91520
H          2.20290       -1.56590       -0.48380
H          0.39940       -0.62070       -1.94790
H          0.28010        1.18300       -2.01940
H         -0.19540        1.23590        0.42510
H         -0.16970       -0.61940        0.44360
H         -4.49390       -0.82980        0.82570
H         -4.66480        0.97230        0.62860
H         -4.37330       -0.10390       -0.81110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

