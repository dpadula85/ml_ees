%nproc=16
%mem=2GB
%chk=mol_482_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.66660        0.89680        0.42130
C         -2.33090        0.35470       -0.11050
O         -1.84520       -0.54080        0.80600
C         -0.65890       -1.14540        0.50860
C          0.42750       -0.14020        0.35630
O          1.68190       -0.75090        0.04700
C          2.75900        0.13400       -0.11240
O          2.60520        1.37670        0.01350
C          4.12760       -0.34820       -0.43600
H         -3.54240        1.19240        1.47690
H         -4.00070        1.75980       -0.17650
H         -4.45580        0.12690        0.34560
H         -1.65080        1.20890       -0.31170
H         -2.52340       -0.15260       -1.09460
H         -0.37190       -1.79890        1.37360
H         -0.75420       -1.80060       -0.40470
H          0.25650        0.58920       -0.47560
H          0.56660        0.38980        1.31790
H          4.66510       -0.41030        0.55180
H          4.10300       -1.33570       -0.94240
H          4.60830        0.39450       -1.08220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

