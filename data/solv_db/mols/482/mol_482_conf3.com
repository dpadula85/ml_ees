%nproc=16
%mem=2GB
%chk=mol_482_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.16980        1.15710       -0.11140
C         -2.30550       -0.19530       -0.78620
O         -1.06110       -0.81580       -0.69450
C         -0.68610       -0.97770        0.64960
C          0.66370       -1.65300        0.63010
O          1.60540       -0.89430       -0.04070
C          2.00410        0.35310        0.34940
O          1.53000        0.89580        1.36820
C          3.02700        1.05200       -0.48020
H         -2.42950        1.02480        0.95210
H         -2.81100        1.89510       -0.63120
H         -1.12050        1.53410       -0.17420
H         -2.55190       -0.05520       -1.83970
H         -3.02790       -0.85460       -0.24960
H         -0.67500       -0.02530        1.20100
H         -1.43950       -1.60060        1.19270
H          0.98570       -1.94510        1.66070
H          0.61090       -2.64190        0.08860
H          3.04030        0.63920       -1.49580
H          2.79250        2.15030       -0.48820
H          4.01820        0.95740        0.01770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

