%nproc=16
%mem=2GB
%chk=mol_482_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.77350       -0.65650       -0.05240
C          2.40950        0.04930        0.07130
O          1.53200       -0.83720        0.65060
C          0.27960       -0.29610        0.84580
C         -0.35580        0.13500       -0.42650
O         -1.65750        0.69030       -0.24340
C         -2.73900        0.05250        0.27490
O         -2.62690       -1.14380        0.63870
C         -4.08050        0.67540        0.45010
H          4.59580        0.07010        0.04090
H          3.81580       -1.44520        0.72360
H          3.82440       -1.11220       -1.05950
H          2.05690        0.29280       -0.97430
H          2.51150        1.00990        0.61480
H          0.38090        0.63970        1.47690
H         -0.33210       -1.05200        1.40890
H          0.23220        1.00250       -0.85930
H         -0.36000       -0.68310       -1.17140
H         -4.47740        0.55870        1.47190
H         -4.75150        0.26350       -0.33350
H         -4.03140        1.78660        0.26750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

