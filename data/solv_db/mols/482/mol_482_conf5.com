%nproc=16
%mem=2GB
%chk=mol_482_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.78780        0.47800        0.16050
C         -2.39090        0.21740       -0.38470
O         -1.63790       -0.35230        0.59900
C         -0.34670       -0.66690        0.21900
C          0.39960        0.55070       -0.20610
O          1.73120        0.31140       -0.60200
C          2.72520       -0.21250        0.19540
O          2.43610       -0.50460        1.37980
C          4.10930       -0.43830       -0.29890
H         -3.74870        1.44940        0.68870
H         -4.02890       -0.37060        0.84750
H         -4.51870        0.55990       -0.64860
H         -2.00230        1.22610       -0.71950
H         -2.39680       -0.43740       -1.26980
H         -0.44980       -1.36200       -0.67260
H          0.17340       -1.29070        0.97390
H         -0.09250        0.96110       -1.14170
H          0.34300        1.38120        0.52580
H          4.21280       -1.43870       -0.79060
H          4.42520        0.38170       -0.99560
H          4.84530       -0.44300        0.53490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_482_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

