%nproc=16
%mem=2GB
%chk=mol_209_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.90080       -0.99030        0.01700
C          1.29990        0.32900       -0.00510
C          0.35310        1.33220       -0.02240
O          0.69360        2.55350       -0.04300
N         -0.93600        0.94850       -0.01640
C         -1.28840       -0.34600        0.00550
O         -2.52330       -0.65880        0.01040
N         -0.40010       -1.36030        0.02290
F          2.61430        0.64100       -0.00990
H          1.66410       -1.75910        0.03020
H         -1.67910        1.68280       -0.02900
H         -0.69890       -2.37250        0.03990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_209_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_209_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_209_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

