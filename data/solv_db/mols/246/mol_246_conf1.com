%nproc=16
%mem=2GB
%chk=mol_246_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.56250       -0.71040        0.76930
C          2.34620       -1.34570        0.87930
C          1.19530       -0.67660        0.44410
C          1.27280        0.59500       -0.08540
C          2.50990        1.20640       -0.18240
C          3.66070        0.55940        0.24330
Cl         5.19960        1.38830        0.09330
O          0.11000        1.25050       -0.51630
C         -1.13790        0.65250       -0.42640
C         -1.19670       -0.61810        0.10520
C         -2.44690       -1.20940        0.19190
C         -3.61260       -0.60030       -0.21970
C         -3.52630        0.67200       -0.74840
C         -2.28960        1.28340       -0.84540
Cl        -2.21690        2.90100       -1.52240
Cl        -4.96610        1.52830       -1.30070
Cl        -5.16000       -1.41090       -0.07880
Cl        -2.51560       -2.81970        0.86630
O         -0.05520       -1.28360        0.53750
H          4.45270       -1.22120        1.10310
H          2.24710       -2.34420        1.29020
H          2.56690        2.20310       -0.59780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_246_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_246_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_246_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

