%nproc=16
%mem=2GB
%chk=mol_626_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04030       -0.26670       -0.03650
C         -0.52980       -0.24520       -0.03310
C          0.29720        0.83950        0.05580
C          1.61810        0.61170        0.03870
C          2.03640       -0.68420       -0.06600
S          0.56970       -1.54790       -0.13760
H         -2.40290        0.23690        0.90520
H         -2.35590       -1.32510        0.06990
H         -2.45520        0.17470       -0.94320
H         -0.16660        1.85380        0.13760
H          2.36950        1.42160        0.10540
H          3.05980       -1.06930       -0.09630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

