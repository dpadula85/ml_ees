%nproc=16
%mem=2GB
%chk=mol_626_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.05740        0.16240        0.05040
C         -0.54880        0.21790        0.00790
C          0.33790       -0.82890        0.01440
C          1.64050       -0.52860       -0.02980
C          1.98620        0.79220       -0.07780
S          0.47450        1.58250       -0.05970
H         -2.32980        0.27100        1.10310
H         -2.43750        1.04370       -0.48710
H         -2.43080       -0.75050       -0.43080
H         -0.07600       -1.86170        0.05590
H          2.43070       -1.30490       -0.02860
H          3.01040        1.20490       -0.11780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

