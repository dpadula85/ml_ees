%nproc=16
%mem=2GB
%chk=mol_626_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04820       -0.24250        0.04060
C         -0.53840       -0.24470        0.02110
C          0.30130        0.83340       -0.09430
C          1.61470        0.58900       -0.08780
C          2.01800       -0.71120        0.03270
S          0.54320       -1.55960        0.13410
H         -2.36440        0.77810        0.26200
H         -2.46200       -0.67080       -0.88740
H         -2.33810       -0.91200        0.88420
H         -0.15120        1.84690       -0.18650
H          2.37420        1.39070       -0.17530
H          3.05090       -1.09740        0.05650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

