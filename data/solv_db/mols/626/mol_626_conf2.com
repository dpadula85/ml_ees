%nproc=16
%mem=2GB
%chk=mol_626_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.04480       -0.22880       -0.01700
C         -0.55100       -0.21600       -0.02290
C          0.30810        0.84310        0.07690
C          1.61920        0.57860        0.04460
C          1.99580       -0.73460       -0.08620
S          0.49860       -1.55430       -0.15940
H         -2.38970        0.80420       -0.08080
H         -2.38450       -0.87680       -0.85600
H         -2.41220       -0.69540        0.93580
H         -0.08290        1.88250        0.18080
H          2.42290        1.34450        0.11580
H          3.02050       -1.14670       -0.13160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_626_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

