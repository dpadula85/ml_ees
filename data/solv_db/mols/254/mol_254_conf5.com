%nproc=16
%mem=2GB
%chk=mol_254_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.39390        0.30380        0.79930
C          1.57830       -0.99090        0.86710
C          0.84420       -1.24580       -0.40060
C         -0.15310       -0.25510       -0.83600
C         -1.34570        0.00880       -0.01540
C         -2.15910        1.08140       -0.78510
C         -1.19460        0.51380        1.36460
H          1.74100        1.09970        0.38390
H          2.75200        0.51600        1.80710
H          3.25860        0.08040        0.15390
H          2.35300       -1.79260        1.03190
H          0.99440       -0.95060        1.77840
H          0.42070       -2.28140       -0.37020
H          1.64400       -1.34120       -1.20560
H          0.35260        0.72370       -1.13540
H         -0.54750       -0.62280       -1.84180
H         -1.98430       -0.91590       -0.01050
H         -2.72770        1.71320       -0.07320
H         -1.48810        1.74250       -1.35430
H         -2.91320        0.56970       -1.43940
H         -1.20900       -0.29220        2.15890
H         -2.17450        1.06110        1.60430
H         -0.43590        1.27420        1.51430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

