%nproc=16
%mem=2GB
%chk=mol_254_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.39150        0.29520       -0.02920
C          2.12390       -0.49630        0.18650
C          0.89250        0.31020       -0.25270
C         -0.29800       -0.55710        0.00100
C         -1.60920        0.05250       -0.36400
C         -2.69610       -0.95760       -0.04250
C         -1.93370        1.32420        0.31740
H          4.17490        0.05360        0.71940
H          3.81290       -0.04960       -1.01620
H          3.23920        1.38090       -0.00660
H          2.07080       -0.75200        1.26540
H          2.16320       -1.45310       -0.37270
H          1.00140        0.42510       -1.36100
H          0.86840        1.29040        0.21940
H         -0.28630       -0.82650        1.08750
H         -0.14960       -1.50830       -0.54890
H         -1.61250        0.23070       -1.46620
H         -3.29140       -1.20370       -0.95760
H         -3.36700       -0.60290        0.77520
H         -2.27340       -1.92090        0.31960
H         -2.96890        1.61630        0.04170
H         -1.28450        2.17680        0.04970
H         -1.96810        1.17210        1.43480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

