%nproc=16
%mem=2GB
%chk=mol_254_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.95700        0.35110       -0.01280
C          2.13220       -0.84330       -0.39380
C          0.70260       -0.49180       -0.71670
C         -0.01850        0.15010        0.43070
C         -1.43440        0.44920       -0.02760
C         -2.11100       -0.84360       -0.42370
C         -2.22810        1.19910        0.98760
H          2.64660        1.27630       -0.51170
H          4.00580        0.14770       -0.31580
H          2.98390        0.44410        1.10590
H          2.23910       -1.60840        0.37710
H          2.57150       -1.26490       -1.33500
H          0.18040       -1.45360       -0.93370
H          0.67390        0.17500       -1.57920
H         -0.13260       -0.59140        1.26800
H          0.47990        1.05060        0.81190
H         -1.34630        1.06270       -0.96540
H         -3.21890       -0.70110       -0.32830
H         -1.90470       -1.11870       -1.48030
H         -1.77210       -1.62710        0.28500
H         -3.20630        0.70110        1.18640
H         -1.72450        1.31190        1.96700
H         -2.47540        2.22530        0.60460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

