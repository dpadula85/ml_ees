%nproc=16
%mem=2GB
%chk=mol_254_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.95840        0.35340       -0.07900
C          2.15090       -0.82080        0.38250
C          0.74700       -0.83100       -0.17400
C         -0.03840        0.38960        0.22000
C         -1.41810        0.27020       -0.38990
C         -2.27000        1.48830       -0.02340
C         -2.11960       -0.99880        0.05910
H          4.00840        0.03670       -0.25300
H          2.99500        1.14210        0.69870
H          2.60300        0.78050       -1.03510
H          2.64710       -1.74610       -0.01550
H          2.18610       -0.89310        1.47200
H          0.80520       -0.89870       -1.26580
H          0.23440       -1.71030        0.29740
H          0.39390        1.33150       -0.11620
H         -0.16470        0.45540        1.32020
H         -1.38440        0.23870       -1.49430
H         -1.91060        2.37250       -0.58730
H         -3.30280        1.25120       -0.34480
H         -2.19350        1.70930        1.05210
H         -3.18580       -0.91930       -0.23280
H         -2.07690       -1.14540        1.15240
H         -1.66450       -1.85590       -0.43620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

