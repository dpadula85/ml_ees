%nproc=16
%mem=2GB
%chk=mol_254_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.14810        0.44560       -0.14280
C         -1.65150        0.18730       -0.45010
C         -1.01560       -0.54490        0.69530
C          0.40320       -0.88270        0.50000
C          1.39170        0.19680        0.29150
C          2.76250       -0.52280        0.15860
C          1.27100        0.96450       -0.99970
H         -3.62220       -0.51020        0.15350
H         -3.15720        1.20660        0.65590
H         -3.55850        0.88390       -1.06420
H         -1.55170       -0.40640       -1.37440
H         -1.26160        1.23890       -0.56570
H         -1.59330       -1.52150        0.77340
H         -1.20750       -0.00860        1.67360
H          0.78660       -1.47800        1.39100
H          0.53790       -1.62380       -0.34160
H          1.50490        0.88560        1.15290
H          3.48380        0.16460       -0.32940
H          2.58320       -1.35160       -0.55050
H          3.12600       -0.85410        1.13110
H          0.66640        0.43390       -1.73660
H          0.96320        2.01220       -0.79230
H          2.28670        1.08470       -1.48290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_254_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

