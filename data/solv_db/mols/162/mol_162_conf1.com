%nproc=16
%mem=2GB
%chk=mol_162_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.93560       -1.66540        0.05810
C         -1.86100       -0.68740       -0.21020
C         -1.41910        0.60980       -0.30100
C         -0.07800        0.93690       -0.12870
C          0.85570       -0.04040        0.14080
C          0.41340       -1.34220        0.23220
C          2.27110        0.28190        0.32550
O          2.70640        1.44440        0.25040
O          0.38640        2.24170       -0.21690
H         -1.26780       -2.68360        0.13190
H         -2.91100       -0.90050       -0.35020
H         -2.12480        1.39680       -0.51080
H          1.14350       -2.13410        0.44660
H          2.98650       -0.50110        0.53630
H         -0.16590        3.04310       -0.40400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

