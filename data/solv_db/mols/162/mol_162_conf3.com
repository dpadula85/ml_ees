%nproc=16
%mem=2GB
%chk=mol_162_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.69070        0.86550        0.06670
C         -1.96290       -0.48130        0.18340
C         -0.92260       -1.40520        0.17030
C          0.37490       -0.95510        0.03960
C          0.61180        0.39310       -0.07450
C         -0.40350        1.33500       -0.06460
C          1.97040        0.89340       -0.21340
O          2.23620        2.12010       -0.32100
O          1.38410       -1.89080        0.02970
H         -2.49740        1.57150        0.07770
H         -2.98270       -0.86400        0.28820
H         -1.10530       -2.46220        0.25950
H         -0.19170        2.39860       -0.15660
H          2.81780        0.22310       -0.23000
H          2.36160       -1.74180       -0.05520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

