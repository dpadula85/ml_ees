%nproc=16
%mem=2GB
%chk=mol_162_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.54030       -1.11870       -0.00500
C         -1.92020        0.20950        0.10350
C         -1.00840        1.23140        0.13110
C          0.33150        0.90830        0.04670
C          0.75600       -0.40320       -0.06270
C         -0.19340       -1.40890       -0.08730
C          2.17950       -0.68850       -0.14830
O          2.58320       -1.87160       -0.24800
O          1.31020        1.90000        0.06910
H         -2.25690       -1.92740       -0.02690
H         -2.99410        0.39570        0.16590
H         -1.31680        2.26580        0.21620
H          0.14700       -2.45200       -0.17440
H          2.91060        0.09830       -0.12850
H          1.01200        2.86140        0.14860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_162_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

