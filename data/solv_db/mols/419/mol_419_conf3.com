%nproc=16
%mem=2GB
%chk=mol_419_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.55900        0.55890        0.08500
C         -2.22600       -0.11070        0.38710
C         -2.31030       -1.51770       -0.12950
C         -1.15630        0.69640       -0.26440
O          0.13930        0.17330       -0.05560
C          1.21000        0.86520       -0.63070
O          0.96720        1.90460       -1.29580
C          2.60660        0.43120       -0.49180
C          2.83010       -0.96570       -1.06130
C          3.08780        0.45370        0.93830
H         -4.36780       -0.16720       -0.08460
H         -3.49200        1.18310       -0.84310
H         -3.83590        1.27450        0.89660
H         -2.11430       -0.12930        1.48140
H         -1.32940       -1.93710       -0.42430
H         -2.72730       -2.15810        0.69770
H         -3.03950       -1.62160       -0.96260
H         -1.33930        0.78200       -1.35260
H         -1.16450        1.71220        0.16970
H          3.27360        1.10040       -1.06880
H          3.94440       -1.11940       -1.09440
H          2.40760       -1.05970       -2.06930
H          2.42390       -1.75250       -0.42410
H          4.20530        0.41830        0.89510
H          2.75490       -0.42370        1.51590
H          2.81110        1.40910        1.38790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

