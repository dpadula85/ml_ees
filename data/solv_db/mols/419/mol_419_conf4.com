%nproc=16
%mem=2GB
%chk=mol_419_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.39020       -0.85390       -0.45040
C         -2.00340       -0.21300       -0.33010
C         -2.17260        1.07900        0.43300
C         -1.12170       -1.16810        0.42570
O          0.19120       -0.72080        0.62390
C          1.04800       -0.46250       -0.40450
O          0.64960       -0.63530       -1.58560
C          2.43980        0.01780       -0.24010
C          2.55280        1.32900        0.44860
C          3.16490       -0.05430       -1.56270
H         -3.74960       -1.19030        0.55470
H         -3.34670       -1.77260       -1.07690
H         -4.09840       -0.14270       -0.92510
H         -1.66210       -0.02990       -1.36150
H         -3.18730        1.48590        0.33580
H         -1.95400        0.84930        1.50550
H         -1.47460        1.83470        0.01270
H         -1.11020       -2.11320       -0.20060
H         -1.64060       -1.39400        1.38850
H          2.94260       -0.75530        0.41470
H          3.33670        1.92440       -0.09110
H          2.92250        1.24930        1.49930
H          1.63220        1.94420        0.40850
H          4.26830       -0.12870       -1.43620
H          2.83120       -0.96220       -2.09770
H          2.93160        0.88310       -2.12040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

