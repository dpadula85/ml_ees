%nproc=16
%mem=2GB
%chk=mol_419_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48610        0.80550       -0.35930
C         -2.14130        0.02640       -0.38280
C         -2.14510       -0.99870        0.68250
C         -1.07260        1.05040       -0.39570
O          0.24050        0.70380       -0.43110
C          1.10230        0.09010        0.39160
O          0.70940       -0.29570        1.50930
C          2.54720       -0.17090        0.04850
C          3.25420       -0.87020        1.16040
C          2.60080       -0.94130       -1.25600
H         -3.45900        1.42560       -1.27280
H         -3.46680        1.47140        0.51350
H         -4.33180        0.10170       -0.37710
H         -2.21000       -0.53760       -1.36310
H         -1.43650       -1.83420        0.50960
H         -2.07570       -0.64800        1.71650
H         -3.19220       -1.47440        0.64680
H         -1.29020        1.84560        0.41320
H         -1.25990        1.68680       -1.33000
H          3.00040        0.82800       -0.11110
H          2.51340       -1.49610        1.73810
H          3.64360       -0.10050        1.88640
H          4.08610       -1.49650        0.82870
H          2.66270       -2.03320       -1.10500
H          1.70630       -0.64080       -1.84710
H          3.50020       -0.63070       -1.81390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

