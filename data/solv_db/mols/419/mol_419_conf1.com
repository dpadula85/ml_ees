%nproc=16
%mem=2GB
%chk=mol_419_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.39410        0.38800       -0.90890
C          2.11740       -0.27940       -0.32470
C          2.37320       -0.36500        1.13520
C          1.00720        0.57130       -0.79850
O         -0.28880        0.29530       -0.50200
C         -1.03670        0.24680        0.61700
O         -0.47460        0.51900        1.70120
C         -2.47890       -0.10420        0.68850
C         -2.75290       -1.49600        0.18050
C         -3.20070        0.85670       -0.26670
H          4.28100       -0.18960       -0.65940
H          3.18990        0.49590       -1.98130
H          3.40560        1.39400       -0.45440
H          2.02220       -1.29650       -0.74960
H          2.37250        0.61750        1.64380
H          3.45950       -0.70120        1.23830
H          1.81940       -1.16120        1.67490
H          1.08730        0.57920       -1.93750
H          1.31840        1.63170       -0.55560
H         -2.86060        0.10350        1.68060
H         -2.18530       -1.63930       -0.75860
H         -2.57820       -2.26500        0.93470
H         -3.83100       -1.51700       -0.11160
H         -3.27190        0.42010       -1.28540
H         -2.66900        1.83090       -0.30610
H         -4.21870        1.06450        0.10550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

