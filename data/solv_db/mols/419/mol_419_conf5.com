%nproc=16
%mem=2GB
%chk=mol_419_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48730       -1.13290        0.40920
C         -2.41030       -0.07450        0.45180
C         -2.64240        0.93350       -0.62800
C         -1.08410       -0.78000        0.33730
O          0.00230        0.10310        0.36480
C          1.28350       -0.41640        0.26760
O          1.43380       -1.63900        0.15960
C          2.49500        0.46380        0.28970
C          2.47720        1.45550       -0.85540
C          3.74960       -0.36990        0.30590
H         -3.16070       -1.96410        1.06710
H         -4.40380       -0.68310        0.80990
H         -3.58230       -1.52480       -0.61880
H         -2.49100        0.42300        1.43800
H         -3.52970        1.54780       -0.31640
H         -2.95090        0.45390       -1.60180
H         -1.82130        1.63090       -0.78210
H         -1.06100       -1.47210       -0.52560
H         -1.00520       -1.44140        1.24500
H          2.47720        1.06840        1.22660
H          1.46660        1.88390       -0.92760
H          2.76670        0.97410       -1.81580
H          3.21660        2.24900       -0.62540
H          4.29270       -0.26880        1.28040
H          4.42430        0.01560       -0.49920
H          3.54460       -1.43560        0.06190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_419_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

