%nproc=16
%mem=2GB
%chk=mol_437_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.44480        0.48970        0.01030
C          1.39810       -0.52730       -0.23030
O          1.75390       -1.67360       -0.67070
C         -0.01030       -0.29430        0.00650
C         -0.42660        0.92800        0.48030
C         -1.79500        1.13490        0.70350
N         -2.70480        0.17650        0.46830
C         -2.31460       -1.02160        0.00590
C         -0.95490       -1.26170       -0.22800
H          3.41120        0.01250        0.34670
H          2.69250        0.99630       -0.94820
H          2.18360        1.20170        0.82090
H          0.22440        1.75670        0.69850
H         -2.14440        2.10180        1.08060
H         -3.06890       -1.77490       -0.17450
H         -0.68910       -2.24440       -0.60120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

