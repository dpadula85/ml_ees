%nproc=16
%mem=2GB
%chk=mol_437_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44390       -0.30040       -0.13310
C         -1.35540        0.35470        0.63890
O         -1.70320        1.05340        1.62770
C          0.02890        0.18250        0.25310
C          0.32950       -0.59040       -0.83110
C          1.68200       -0.72280       -1.15950
N          2.64920       -0.11970       -0.44510
C          2.38180        0.63750        0.61390
C          1.03250        0.78480        0.96220
H         -2.23030       -0.27190       -1.24260
H         -2.52110       -1.36100        0.17120
H         -3.39030        0.25530       -0.01200
H         -0.37970       -1.10340       -1.45140
H          1.94640       -1.32850       -2.01280
H          3.16090        1.13250        1.20130
H          0.81270        1.39720        1.81920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

