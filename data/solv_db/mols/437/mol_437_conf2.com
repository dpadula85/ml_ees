%nproc=16
%mem=2GB
%chk=mol_437_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.29530       -0.78000       -0.11810
C          1.43310        0.33110       -0.55020
O          1.85840        1.31540       -1.22860
C          0.03480        0.30190       -0.18560
C         -0.44220       -0.75460        0.54780
C         -1.78300       -0.82760        0.92310
N         -2.59410        0.16430        0.54680
C         -2.13360        1.20300       -0.17210
C         -0.82380        1.31520       -0.56240
H          3.30770       -0.75130       -0.56480
H          2.33620       -0.73880        1.00970
H          1.77420       -1.72580       -0.39520
H          0.18190       -1.57960        0.87320
H         -2.16600       -1.65000        1.49710
H         -2.81340        2.01200       -0.47500
H         -0.46540        2.16480       -1.14590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

