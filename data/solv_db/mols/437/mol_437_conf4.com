%nproc=16
%mem=2GB
%chk=mol_437_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43170        0.52740        0.30970
C          1.42240       -0.50620        0.10410
O          1.84980       -1.70520        0.03490
C          0.01110       -0.28320       -0.02410
C         -0.88080       -1.33110       -0.21860
C         -2.23120       -1.11680       -0.34140
N         -2.72920        0.12130       -0.27610
C         -1.90500        1.19280       -0.08650
C         -0.56310        0.97310        0.03480
H          2.96690        0.38670        1.29090
H          3.21330        0.44950       -0.50230
H          2.08050        1.55550        0.29360
H         -0.47140       -2.33820       -0.27120
H         -2.89760       -1.93810       -0.49040
H         -2.36320        2.17030       -0.04190
H          0.06600        1.84220        0.18470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

