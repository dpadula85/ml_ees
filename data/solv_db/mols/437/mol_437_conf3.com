%nproc=16
%mem=2GB
%chk=mol_437_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44320        0.17510        0.09610
C         -1.34690       -0.80400       -0.12100
O         -1.58140       -2.01560       -0.34860
C          0.03450       -0.33980       -0.06710
C          1.10520       -1.19520       -0.25960
C          2.40520       -0.72880       -0.20340
N          2.63480        0.56700        0.04030
C          1.63250        1.45720        0.23770
C          0.33040        0.98460        0.18040
H         -3.42700       -0.30620        0.08010
H         -2.30820        0.70070        1.08390
H         -2.43100        0.98850       -0.67630
H          0.87300       -2.23940       -0.45470
H          3.19600       -1.44730       -0.36220
H          1.81470        2.50490        0.43500
H         -0.48860        1.69840        0.33920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_437_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

