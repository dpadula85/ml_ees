%nproc=16
%mem=2GB
%chk=mol_165_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.93240       -0.68700       -0.02120
C         -0.57360       -0.12100       -0.38540
C         -0.73160        1.39500       -0.06220
C          0.54300       -0.72150        0.45150
O          1.65410       -0.83120       -0.45350
N          2.88410       -0.30630       -0.14800
O          3.24040       -0.08280        1.01940
O          3.42950        0.56030       -1.10140
H         -1.87940       -1.80620        0.01090
H         -2.31220       -0.27420        0.91820
H         -2.61160       -0.33620       -0.82640
H         -0.40220       -0.28550       -1.43910
H         -1.22390        1.84050       -0.96370
H         -1.44450        1.57940        0.77280
H          0.21850        1.91970        0.10060
H          0.87470       -0.11750        1.29590
H          0.26720       -1.72540        0.83170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

