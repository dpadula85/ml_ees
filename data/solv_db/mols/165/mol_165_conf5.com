%nproc=16
%mem=2GB
%chk=mol_165_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.37470        1.12190        0.41960
C         -0.82840        0.15920       -0.58010
C         -1.21750       -1.24730       -0.14460
C          0.67730        0.19980       -0.69830
O          1.30500       -0.10700        0.51940
N          2.65150       -0.12590        0.62960
O          3.35900        0.81000        0.18010
O          3.35250       -1.15750        1.23530
H         -0.83480        1.07210        1.38790
H         -1.40780        2.17710        0.03030
H         -2.43430        0.82950        0.62390
H         -1.32110        0.34180       -1.55510
H         -0.92830       -1.94440       -0.97270
H         -2.31630       -1.21890        0.00190
H         -0.67250       -1.53090        0.79500
H          1.03920        1.19870       -1.01550
H          0.95120       -0.57800       -1.44730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

