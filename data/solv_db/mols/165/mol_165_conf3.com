%nproc=16
%mem=2GB
%chk=mol_165_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.94920       -1.67460        0.23870
C         -0.60200       -0.35420       -0.43290
C         -1.67600        0.64290       -0.05330
C          0.76190        0.04820        0.04420
O          1.18460        1.29010       -0.49440
N          2.46400        1.69140       -0.15100
O          3.37700        1.26780       -0.90340
O          2.76510        1.61740        1.19970
H         -1.95050       -1.63330        0.73700
H         -0.99730       -2.46190       -0.52910
H         -0.23070       -1.91000        1.06380
H         -0.57260       -0.43270       -1.52990
H         -1.37360        1.67860       -0.25530
H         -2.55860        0.38390       -0.69170
H         -1.97520        0.43720        0.98130
H          0.86070        0.12610        1.13440
H          1.47250       -0.71660       -0.35810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

