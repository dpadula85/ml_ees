%nproc=16
%mem=2GB
%chk=mol_165_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.46650       -0.82370        0.81020
C         -0.49000        0.14580        0.25430
C         -1.21450        1.28210       -0.42320
C          0.60500       -0.39510       -0.59720
O          1.60000       -1.05340        0.15850
N          2.88110       -0.47790        0.04790
O          3.29400       -0.17990       -1.06010
O          3.11680        0.44450        1.07980
H         -2.52750       -0.41330        0.70910
H         -1.41850       -1.73330        0.17260
H         -1.32380       -0.99420        1.88220
H          0.06170        0.64800        1.15970
H         -2.11690        0.86490       -0.93860
H         -0.50430        1.78860       -1.09750
H         -1.60020        1.93560        0.38800
H          0.11090       -1.29670       -1.16740
H          0.99260        0.25820       -1.37830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

