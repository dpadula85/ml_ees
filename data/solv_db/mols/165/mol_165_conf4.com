%nproc=16
%mem=2GB
%chk=mol_165_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87370       -0.57300       -0.55840
C         -0.69570        0.34860       -0.27140
C         -0.81080        0.83590        1.14530
C          0.57100       -0.40830       -0.54520
O          1.70430        0.44300       -0.67300
N          2.94130        0.00070       -0.23640
O          3.65190        0.88070        0.31300
O          3.05020       -1.27030        0.30380
H         -2.65290        0.04780       -1.04200
H         -1.58460       -1.43150       -1.18210
H         -2.24100       -0.97200        0.42140
H         -0.68860        1.23360       -0.94220
H         -0.10700        0.34470        1.83330
H         -0.69000        1.94620        1.20380
H         -1.84130        0.63850        1.53640
H          0.81190       -1.16870        0.23910
H          0.45480       -0.89600       -1.54530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_165_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

