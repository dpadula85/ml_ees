%nproc=16
%mem=2GB
%chk=mol_355_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.51910        0.10330       -0.23120
C         -2.81530        1.25790        0.03360
C         -1.43970        1.21060        0.05160
C         -0.75960        0.03300       -0.18990
C         -1.47920       -1.11220       -0.45300
C         -2.85300       -1.08720       -0.47580
Cl        -0.65730       -2.62050       -0.76300
C          0.69600       -0.02480       -0.17240
C          1.38330       -0.30370        0.99090
C          2.76120       -0.34710        0.95520
C          3.44920       -0.11810       -0.21370
C          2.78790        0.16260       -1.39040
C          1.40180        0.20240       -1.33630
Cl         0.54710        0.56160       -2.84100
Cl         5.21470       -0.17530       -0.25160
Cl         0.50430       -0.59850        2.49480
Cl        -0.53200        2.68350        0.38950
H         -4.60170        0.16140       -0.24080
H         -3.28230        2.21310        0.23130
H         -3.41820       -1.98270       -0.68160
H          3.29640       -0.56760        1.87610
H          3.31520        0.34830       -2.33310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_355_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_355_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_355_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

