%nproc=16
%mem=2GB
%chk=mol_355_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.51610       -0.31650       -0.09680
C         -2.77200       -0.42500       -1.24560
C         -1.40460       -0.31760       -1.21300
C         -0.75620       -0.09670       -0.00980
C         -1.51860        0.00960        1.13330
C         -2.90000       -0.09700        1.11350
Cl        -0.66820        0.28810        2.63380
C          0.69610        0.01830        0.02970
C          1.53320       -1.06900        0.21410
C          2.90550       -0.89890        0.24260
C          3.44640        0.36850        0.08550
C          2.63460        1.46930       -0.09980
C          1.26140        1.26240       -0.12320
Cl         0.20520        2.63640       -0.35600
Cl         5.17980        0.56800        0.12370
Cl         0.79240       -2.65830        0.40850
Cl        -0.40890       -0.45000       -2.65700
H         -4.58730       -0.40780       -0.17190
H         -3.22590       -0.59800       -2.21660
H         -3.47520       -0.00710        2.04390
H          3.55690       -1.75030        0.38700
H          3.02160        2.47180       -0.22570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_355_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_355_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_355_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

