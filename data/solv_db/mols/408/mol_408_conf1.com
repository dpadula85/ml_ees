%nproc=16
%mem=2GB
%chk=mol_408_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.40780        0.61090        0.49300
C          2.62510       -0.54400        0.00750
O          3.23560       -1.59650       -0.30040
C          1.16970       -0.51470       -0.13040
C          0.50600        0.64470        0.20670
C         -0.87070        0.65140        0.07520
C         -1.57570       -0.44550       -0.37350
C         -0.91770       -1.61140       -0.71370
C          0.45510       -1.60580       -0.57810
O         -2.96700       -0.44410       -0.50820
C         -3.64880        0.75500       -0.15880
H          2.89270        1.17630        1.30610
H          4.39030        0.24030        0.84630
H          3.56150        1.34010       -0.35380
H          1.03460        1.53400        0.56180
H         -1.44710        1.54340        0.32830
H         -1.41750       -2.50090       -1.07060
H          1.00900       -2.51940       -0.84100
H         -3.32460        1.58480       -0.82240
H         -3.38410        1.05190        0.88550
H         -4.73410        0.64940       -0.21370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

