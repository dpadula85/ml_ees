%nproc=16
%mem=2GB
%chk=mol_408_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.41510        0.51620        0.66830
C         -2.66160        0.13860       -0.55110
O         -3.29260       -0.05700       -1.59560
C         -1.20910        0.00090       -0.51980
C         -0.47500        0.21100        0.62460
C          0.89370        0.08660        0.67570
C          1.57780       -0.26520       -0.46770
C          0.84950       -0.48150       -1.63440
C         -0.53160       -0.35180       -1.66870
O          2.96400       -0.39930       -0.44890
C          3.66520       -0.17610        0.73490
H         -4.48020        0.19000        0.61600
H         -3.37040        1.60230        0.88620
H         -2.97750       -0.01430        1.54090
H         -1.00070        0.48860        1.53150
H          1.47810        0.25010        1.57100
H          1.39520       -0.76010       -2.53770
H         -1.04340       -0.53430       -2.61180
H          4.75090       -0.22330        0.50840
H          3.42560        0.78380        1.23250
H          3.45720       -1.00520        1.44570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

