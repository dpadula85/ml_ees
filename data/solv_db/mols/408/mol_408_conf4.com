%nproc=16
%mem=2GB
%chk=mol_408_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.61360       -0.01030       -0.53630
C         -2.60590        0.48560        0.42630
O         -2.93170        1.12230        1.45390
C         -1.20220        0.22760        0.17370
C         -0.24630        0.68200        1.06000
C          1.07940        0.42200        0.79380
C          1.48150       -0.27670       -0.32830
C          0.51700       -0.72980       -1.21340
C         -0.81370       -0.47930       -0.96390
O          2.80170       -0.54920       -0.61610
C          3.89510       -0.17320        0.15610
H         -3.43740       -1.06860       -0.83360
H         -3.51440        0.58980       -1.47340
H         -4.61470        0.11240       -0.06990
H         -0.52880        1.24260        1.96560
H          1.89020        0.75270        1.45370
H          0.78680       -1.28880       -2.11780
H         -1.58710       -0.83450       -1.66000
H          4.13750        0.89940       -0.06040
H          4.75400       -0.82610       -0.12310
H          3.75270       -0.29980        1.24430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

