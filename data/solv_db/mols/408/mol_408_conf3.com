%nproc=16
%mem=2GB
%chk=mol_408_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.65260        0.23170        0.22370
C         -2.61190       -0.58200       -0.44370
O         -2.89430       -1.52630       -1.19620
C         -1.20800       -0.26820       -0.21290
C         -0.84840        0.77240        0.61570
C          0.47730        1.03430        0.80600
C          1.50610        0.32380        0.22150
C          1.13070       -0.71560       -0.60590
C         -0.19820       -1.00930       -0.82160
O          2.79530        0.68070        0.48850
C          3.95560        0.10150        0.00720
H         -4.66860       -0.10250       -0.10070
H         -3.60830        0.04110        1.33000
H         -3.51620        1.31960        0.05350
H         -1.64680        1.35260        1.09230
H          0.73190        1.85930        1.46340
H          1.87580       -1.29960       -1.08510
H         -0.51830       -1.81610       -1.46340
H          4.47840        0.84360       -0.66090
H          4.62590       -0.11700        0.89010
H          3.79470       -0.80040       -0.60160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

