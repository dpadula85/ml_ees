%nproc=16
%mem=2GB
%chk=mol_408_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.37010        0.80960        0.00590
C         -2.68750       -0.50380       -0.11390
O         -3.29890       -1.57650       -0.24730
C         -1.21920       -0.54440       -0.07360
C         -0.52550       -1.73130       -0.17950
C          0.85450       -1.70490       -0.13470
C          1.56750       -0.53480        0.01220
C          0.88340        0.65670        0.11890
C         -0.48850        0.62310        0.07360
O          2.95620       -0.51790        0.05630
C          3.65940        0.69940        0.20790
H         -3.15720        1.20990        1.02710
H         -2.90780        1.49060       -0.75310
H         -4.46070        0.66950       -0.16660
H         -1.05660       -2.66600       -0.29560
H          1.42580       -2.63060       -0.21640
H          1.44150        1.59080        0.23580
H         -1.03640        1.55900        0.15700
H          4.74470        0.49970        0.21040
H          3.30590        1.23790        1.11320
H          3.36950        1.36400       -0.65630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_408_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

