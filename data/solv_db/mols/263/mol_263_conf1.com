%nproc=16
%mem=2GB
%chk=mol_263_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.23190        0.33640       -0.02660
C          0.74570        0.22630       -0.00460
C          0.12620       -1.00420       -0.09220
C         -1.26220       -1.13870       -0.07420
C         -2.06080       -0.01770        0.03430
C         -1.41200        1.19770        0.12010
N         -0.06080        1.29760        0.10000
H          2.65490       -0.57340       -0.49330
H          2.64400        0.37310        1.02210
H          2.55720        1.27580       -0.52360
H          0.75300       -1.89200       -0.17810
H         -1.73570       -2.10370       -0.14310
H         -3.14100       -0.07020        0.05260
H         -2.04030        2.09290        0.20650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

