%nproc=16
%mem=2GB
%chk=mol_263_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.22790        0.35270        0.03920
C          0.74330        0.22270        0.00860
C          0.13680       -1.01750       -0.01620
C         -1.24340       -1.17180       -0.04470
C         -2.06120       -0.05390       -0.04880
C         -1.42730        1.17380       -0.02350
N         -0.07820        1.28990        0.00390
H          2.72720       -0.33440       -0.67370
H          2.48730        1.42080       -0.17080
H          2.58660        0.18560        1.09840
H          0.79230       -1.87730       -0.01210
H         -1.70790       -2.15010       -0.06400
H         -3.14440       -0.11800       -0.07030
H         -2.03900        2.07750       -0.02600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

