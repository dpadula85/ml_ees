%nproc=16
%mem=2GB
%chk=mol_263_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.23550       -0.16790        0.15440
C         -0.73940       -0.15270        0.04040
C         -0.02740        1.02310        0.06170
C          1.34640        1.03510       -0.04320
C          2.00390       -0.16140       -0.17130
C          1.30740       -1.32890       -0.19330
N         -0.03000       -1.28350       -0.08750
H         -2.59730       -1.13700        0.54120
H         -2.64820        0.10240       -0.82120
H         -2.54710        0.59550        0.92460
H         -0.59000        1.95950        0.16560
H          1.86980        1.96470       -0.02330
H          3.07120       -0.15480       -0.25300
H          1.81630       -2.29400       -0.29500

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

