%nproc=16
%mem=2GB
%chk=mol_263_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21710       -0.15800        0.00300
C          0.73200       -0.19980       -0.04820
C          0.04460        0.92960        0.32910
C         -1.31100        0.96430        0.30780
C         -2.02070       -0.14630       -0.09750
C         -1.31290       -1.26630       -0.47110
N          0.02670       -1.26360       -0.43710
H          2.66860       -1.13120       -0.25830
H          2.61490        0.58510       -0.71840
H          2.56080        0.19160        1.00850
H          0.57780        1.83550        0.65860
H         -1.82010        1.87040        0.61160
H         -3.09740       -0.05900       -0.09360
H         -1.88040       -2.15230       -0.79440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_263_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

