%nproc=16
%mem=2GB
%chk=mol_518_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.77660       -0.29830       -0.46680
C         -1.74810        0.06500        0.58140
C         -0.39170        0.22790       -0.05260
O         -0.24740        0.06460       -1.27100
O          0.72400        0.56150        0.69890
C          2.01830        0.72450        0.14260
C          2.51810       -0.52770       -0.53140
H         -3.77680       -0.17680        0.01790
H         -2.63980       -1.36160       -0.79990
H         -2.69060        0.39180       -1.35150
H         -2.08540        1.05450        1.00600
H         -1.75050       -0.72600        1.34300
H          2.07590        1.61280       -0.52140
H          2.69810        0.91190        1.01180
H          3.02130       -0.19850       -1.48790
H          3.31220       -1.04470        0.07520
H          1.73900       -1.28090       -0.70540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

