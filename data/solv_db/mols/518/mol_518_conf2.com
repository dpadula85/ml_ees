%nproc=16
%mem=2GB
%chk=mol_518_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.03120        0.14020        0.13410
C          1.79470       -0.66040       -0.15310
C          0.58620        0.14060        0.15290
O          0.73860        1.30560        0.58340
O         -0.70350       -0.32980       -0.01170
C         -1.85200        0.44080        0.28260
C         -3.12520       -0.28350        0.02380
H          3.92980       -0.42860       -0.20330
H          2.98280        1.13250       -0.32630
H          3.08150        0.27820        1.23890
H          1.81550       -1.61750        0.40470
H          1.84140       -0.94400       -1.23780
H         -1.82240        1.32940       -0.39430
H         -1.77760        0.85580        1.31370
H         -3.91810        0.19080        0.66710
H         -3.49760       -0.18970       -1.01170
H         -3.10530       -1.36040        0.27370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

