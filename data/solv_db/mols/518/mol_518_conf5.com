%nproc=16
%mem=2GB
%chk=mol_518_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.35960       -0.09240       -0.84550
C          1.92690       -0.33920        0.57210
C          0.45890       -0.42760        0.75300
O         -0.11060       -1.55110        0.81300
O         -0.33290        0.69890        0.85060
C         -1.71580        0.73930        1.01540
C         -2.51380        0.06440       -0.06910
H          2.57610       -1.01810       -1.41850
H          1.67010        0.56050       -1.42010
H          3.34820        0.44590       -0.77250
H          2.40620        0.35750        1.30030
H          2.36380       -1.34420        0.84020
H         -2.05590        0.38640        2.02580
H         -2.01410        1.82250        0.99180
H         -2.08710       -0.86510       -0.45930
H         -2.75900        0.74330       -0.91730
H         -3.52060       -0.18110        0.38770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

