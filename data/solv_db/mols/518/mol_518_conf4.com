%nproc=16
%mem=2GB
%chk=mol_518_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.78780        0.14810        0.92480
C          1.89180        0.17870       -0.26750
C          0.58050       -0.45200       -0.08530
O          0.44890       -1.63780        0.32000
O         -0.63190        0.22270       -0.32790
C         -1.85640       -0.49780       -0.09480
C         -2.96330        0.49210       -0.37940
H          3.74610        0.64790        0.59610
H          2.35500        0.82890        1.69160
H          2.96670       -0.86340        1.27740
H          2.38080       -0.41800       -1.09580
H          1.84640        1.20200       -0.66020
H         -1.93120       -1.35390       -0.82520
H         -1.91140       -0.86590        0.91940
H         -3.74520        0.40170        0.40540
H         -2.60830        1.56120       -0.28320
H         -3.35650        0.40530       -1.40470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

