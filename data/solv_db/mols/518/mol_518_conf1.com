%nproc=16
%mem=2GB
%chk=mol_518_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.75780        0.63640       -0.33240
C         -1.75950       -0.13200        0.48490
C         -0.41100       -0.15510       -0.14160
O         -0.14890        0.40900       -1.23310
O          0.59100       -0.82320        0.50290
C          1.91930       -0.94320        0.05170
C          2.60530        0.38780       -0.10810
H         -3.34880       -0.01740       -1.00880
H         -2.24940        1.39870       -0.94730
H         -3.50490        1.09750        0.36800
H         -2.14360       -1.18350        0.58230
H         -1.65210        0.23760        1.52150
H          1.95190       -1.53350       -0.86580
H          2.47460       -1.49480        0.86590
H          2.20820        1.12230        0.61650
H          3.69080        0.22840        0.06350
H          2.53480        0.76500       -1.16800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_518_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

