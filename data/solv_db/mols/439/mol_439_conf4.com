%nproc=16
%mem=2GB
%chk=mol_439_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.86750        0.50970       -0.30780
C          1.71420       -0.41810       -0.47740
C          0.42620        0.10190        0.11990
C         -0.64250       -0.92700       -0.12300
C         -1.98260       -0.48380        0.44910
C         -2.46060        0.79740       -0.15980
H          3.33490        0.80500       -1.28770
H          3.69290       -0.02920        0.23470
H          2.61340        1.46470        0.20160
H          1.93470       -1.41020       -0.05810
H          1.53030       -0.54160       -1.57210
H          0.51280        0.29640        1.19630
H          0.14450        1.00990       -0.45290
H         -0.77340       -1.13610       -1.20260
H         -0.39640       -1.89690        0.35750
H         -2.74560       -1.26470        0.25470
H         -1.88770       -0.34990        1.52940
H         -3.58610        0.85050       -0.17100
H         -2.16630        0.91690       -1.22970
H         -2.13030        1.70510        0.38890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

