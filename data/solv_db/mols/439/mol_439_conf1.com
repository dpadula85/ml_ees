%nproc=16
%mem=2GB
%chk=mol_439_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.57300        0.52310       -0.61810
C          1.10670        0.57860       -0.16310
C          0.67990       -0.73770        0.40310
C         -0.69510       -0.84340        0.93240
C         -1.84170       -0.60060        0.03490
C         -1.98590        0.74490       -0.57070
H          2.75690        1.22380       -1.45910
H          2.84020       -0.48200       -0.93750
H          3.23490        0.82370        0.22080
H          1.02540        1.40790        0.55140
H          0.57040        0.77530       -1.14090
H          1.37460       -0.98180        1.26430
H          0.87840       -1.57090       -0.33450
H         -0.79870       -0.10050        1.78360
H         -0.87740       -1.85310        1.42940
H         -1.86440       -1.37590       -0.77720
H         -2.77560       -0.81240        0.64890
H         -1.40160        1.55640       -0.11660
H         -3.07700        1.02030       -0.56010
H         -1.72290        0.70420       -1.66130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

