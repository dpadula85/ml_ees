%nproc=16
%mem=2GB
%chk=mol_439_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.78330        0.36410        0.58890
C         -1.73260        0.24950       -0.45850
C         -0.38910       -0.16920        0.09580
C          0.64230       -0.27050       -1.01070
C          1.94450       -0.69430       -0.34460
C          2.37960        0.31390        0.68350
H         -3.55800        1.09220        0.21420
H         -3.24150       -0.62220        0.81270
H         -2.38220        0.79440        1.52720
H         -1.57450        1.25550       -0.92910
H         -2.00750       -0.44090       -1.26430
H         -0.03750        0.49010        0.90990
H         -0.47930       -1.19680        0.53050
H          0.36060       -1.05200       -1.74220
H          0.76990        0.67350       -1.56770
H          2.74600       -0.81520       -1.10190
H          1.83600       -1.67820        0.15130
H          2.21560       -0.08890        1.72430
H          3.46190        0.52080        0.56700
H          1.82900        1.27410        0.61380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

