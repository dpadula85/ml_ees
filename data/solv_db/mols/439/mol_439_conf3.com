%nproc=16
%mem=2GB
%chk=mol_439_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.07970       -0.00480       -0.11410
C          1.78460       -0.77850       -0.39880
C          0.66560        0.21900       -0.21910
C         -0.68850       -0.38010       -0.46320
C         -1.71170        0.72840       -0.24860
C         -3.08170        0.12560       -0.49470
H          3.04410        0.20050        0.98950
H          3.04960        0.94970       -0.67930
H          3.94240       -0.62030       -0.36870
H          1.64280       -1.60740        0.29040
H          1.75970       -1.12400       -1.45020
H          0.79020        1.05240       -0.95080
H          0.69530        0.66530        0.79670
H         -0.75380       -0.70240       -1.52560
H         -0.90940       -1.21380        0.22540
H         -1.63090        1.10730        0.79180
H         -1.47340        1.52720       -0.97960
H         -3.81830        0.89080       -0.76370
H         -3.37020       -0.36770        0.47730
H         -3.01630       -0.66720       -1.26750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

