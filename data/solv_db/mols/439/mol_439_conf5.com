%nproc=16
%mem=2GB
%chk=mol_439_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.12290        0.08750        0.16590
C          1.81290       -0.66240        0.27840
C          0.66570        0.28010       -0.01810
C         -0.67160       -0.40010        0.07600
C         -1.77000        0.58910       -0.22950
C         -3.13680        0.00640       -0.16110
H          3.22760        0.86840        0.94680
H          3.98420       -0.61140        0.13020
H          3.10310        0.57390       -0.85320
H          1.69210       -1.18210        1.23330
H          1.86050       -1.47030       -0.50650
H          0.71100        1.10560        0.72050
H          0.74270        0.71900       -1.02680
H         -0.71860       -1.27260       -0.60710
H         -0.83350       -0.80510        1.10020
H         -1.66680        1.43190        0.50820
H         -1.56590        1.02610       -1.24330
H         -3.83240        0.66170       -0.76270
H         -3.21610       -1.03470       -0.46350
H         -3.51080        0.08900        0.89700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_439_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

