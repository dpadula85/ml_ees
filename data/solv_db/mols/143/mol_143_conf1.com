%nproc=16
%mem=2GB
%chk=mol_143_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01780        1.65870        0.14320
C          1.17060        0.91280        0.00060
C          1.15570       -0.46200       -0.17240
C         -0.08860       -1.05690       -0.19540
C         -1.25980       -0.33760       -0.05540
C         -1.21020        1.03020        0.11530
O         -2.51960       -0.96330       -0.08160
C          2.37850       -1.22720       -0.32030
N          3.36500       -1.83030       -0.42570
H          0.02630        2.73140        0.27810
H          2.14120        1.40440        0.02200
H         -0.11950       -2.14630       -0.33140
H         -2.13610        1.57980        0.22400
H         -2.92140       -1.29380        0.79900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_143_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_143_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_143_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

