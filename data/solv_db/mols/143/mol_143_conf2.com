%nproc=16
%mem=2GB
%chk=mol_143_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.71090       -1.47300        0.34330
C          0.65660       -1.30000        0.26010
C          1.23870       -0.10020       -0.07550
C          0.39660        0.96050       -0.33540
C         -0.98900        0.83100       -0.26330
C         -1.51630       -0.39480        0.07770
O         -1.85040        1.88260       -0.52030
C          2.66120        0.07770       -0.16160
N          3.82420        0.22820       -0.21240
H         -1.10180       -2.45530        0.61570
H          1.31620       -2.15290        0.46960
H          0.79520        1.93460       -0.60590
H         -2.60320       -0.50960        0.13780
H         -2.11720        2.47130        0.27030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_143_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_143_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_143_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

