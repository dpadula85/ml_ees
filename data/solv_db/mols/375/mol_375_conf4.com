%nproc=16
%mem=2GB
%chk=mol_375_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.31920       -0.35050        0.11180
C          1.82720       -0.43740       -0.25100
C          1.27190        0.93590       -0.03680
O         -0.08540        0.94620       -0.34890
C         -1.06400        0.22370        0.25880
O         -0.79050       -0.54610        1.20320
C         -2.45630        0.38050       -0.23470
C         -3.35940       -0.50930        0.58220
H          3.66100        0.62960       -0.26510
H          3.79710       -1.19110       -0.46790
H          3.44630       -0.47870        1.19820
H          1.79600       -0.64860       -1.35810
H          1.33540       -1.23410        0.29400
H          1.76850        1.62510       -0.78090
H          1.47240        1.29680        1.00110
H         -2.71480        1.44340       -0.14560
H         -2.44270        0.10120       -1.31010
H         -3.98410        0.13890        1.25660
H         -2.80270       -1.21890        1.22810
H         -3.99520       -1.10650       -0.08020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

