%nproc=16
%mem=2GB
%chk=mol_375_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47680        0.10530       -1.24020
C         -2.08950       -0.53360        0.09870
C         -1.34890        0.43630        0.94850
O         -0.16180        0.94060        0.42890
C          0.90400        0.12900        0.10610
O          0.75660       -1.10650        0.30510
C          2.18590        0.65760       -0.45450
C          3.10900       -0.51910       -0.68000
H         -2.48240        1.19230       -1.11170
H         -3.49030       -0.20570       -1.55980
H         -1.72720       -0.16390       -2.01780
H         -1.63250       -1.52480       -0.02450
H         -3.08700       -0.72400        0.59710
H         -1.20430       -0.05770        1.95430
H         -2.03170        1.31830        1.11770
H          2.02330        1.12650       -1.44600
H          2.67680        1.36610        0.22990
H          2.67830       -1.20840       -1.43670
H          4.08380       -0.14800       -1.08000
H          3.31460       -1.08040        0.26100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

