%nproc=16
%mem=2GB
%chk=mol_375_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.46530       -0.90410        0.23760
C         -2.09650       -0.61850        0.81340
C         -1.17960       -0.06090       -0.26090
O          0.10370        0.21550        0.25310
C          1.14000        0.72300       -0.49150
O          0.92410        0.95350       -1.71300
C          2.48030        1.01250        0.04650
C          3.47030       -0.09550       -0.23110
H         -3.62220       -2.00480        0.13610
H         -4.28550       -0.51160        0.87860
H         -3.54350       -0.37770       -0.73170
H         -2.17550        0.11340        1.64070
H         -1.65930       -1.54600        1.24360
H         -1.18120       -0.69200       -1.15500
H         -1.60110        0.93370       -0.57810
H          2.92040        1.92980       -0.44680
H          2.49260        1.22600        1.13600
H          3.81170       -0.11740       -1.28370
H          4.38030        0.10150        0.38530
H          3.08620       -1.08920        0.12100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

