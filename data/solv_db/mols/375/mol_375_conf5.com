%nproc=16
%mem=2GB
%chk=mol_375_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.98450        0.61350       -0.15560
C         -2.35870       -0.50970       -0.96290
C         -1.10110       -0.99320       -0.26770
O         -0.15250        0.06820       -0.13070
C          1.06230       -0.26330        0.46030
O          1.34730       -1.42000        0.86040
C          2.12880        0.77130        0.63570
C          3.42280        0.29260       -0.03640
H         -2.16770        1.20950        0.34890
H         -3.49630        1.36670       -0.81240
H         -3.73090        0.25970        0.55120
H         -3.08310       -1.35470       -0.93350
H         -2.19290       -0.11920       -1.95780
H         -0.61790       -1.81980       -0.81340
H         -1.39040       -1.27360        0.77250
H          1.86570        1.75350        0.21540
H          2.39220        0.88970        1.72950
H          3.61760       -0.74550        0.29060
H          4.25270        0.96140        0.24190
H          3.18660        0.31290       -1.12450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

