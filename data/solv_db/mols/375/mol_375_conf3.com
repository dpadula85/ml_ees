%nproc=16
%mem=2GB
%chk=mol_375_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.06500        0.68260        0.20770
C         -2.52870       -0.70290        0.09000
C         -1.13720       -0.77930       -0.44440
O         -0.19370       -0.09350        0.35190
C          1.14010       -0.13060       -0.10350
O          1.40880       -0.75400       -1.16290
C          2.22820        0.53570        0.61930
C          3.52720        0.32060       -0.10560
H         -3.43590        0.80370        1.26580
H         -3.93660        0.86130       -0.48300
H         -2.32810        1.47520       -0.04180
H         -2.54490       -1.26190        1.05750
H         -3.18660       -1.27060       -0.59400
H         -1.10420       -0.44110       -1.50390
H         -0.83350       -1.86340       -0.40610
H          2.01970        1.63680        0.58860
H          2.33630        0.19010        1.66900
H          3.34850       -0.03060       -1.15830
H          4.11350        1.26510       -0.21640
H          4.17200       -0.44310        0.37000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_375_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

