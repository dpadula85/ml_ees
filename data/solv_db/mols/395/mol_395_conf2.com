%nproc=16
%mem=2GB
%chk=mol_395_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.31090       -0.81470        0.56340
C         -3.18620       -0.46900       -0.92530
C         -2.52270        0.88480       -0.99480
O         -1.26840        0.85250       -0.38770
C         -0.25840        0.06540       -0.85990
O         -0.41530       -0.65810       -1.86370
C          1.06400        0.02420       -0.21830
C          1.29310        0.80260        0.89330
C          2.52730        0.79250        1.52810
C          3.54770        0.00560        1.06120
C          3.31310       -0.77100       -0.05090
C          2.08920       -0.76490       -0.68530
O          4.79440       -0.03570        1.66280
H         -4.20910       -1.45000        0.69240
H         -2.40140       -1.39160        0.88510
H         -3.42310        0.10110        1.16060
H         -4.23410       -0.35870       -1.34580
H         -2.69070       -1.26270       -1.49720
H         -3.18670        1.67550       -0.56430
H         -2.35490        1.16980       -2.07700
H          0.51890        1.43240        1.28940
H          2.72370        1.40710        2.41400
H          4.10900       -1.41390       -0.45180
H          1.92630       -1.39400       -1.57180
H          5.55520       -0.60000        1.34330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

