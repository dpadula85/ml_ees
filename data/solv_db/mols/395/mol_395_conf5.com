%nproc=16
%mem=2GB
%chk=mol_395_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.71270       -0.51010        0.11510
C         -3.25260       -0.80410        0.12440
C         -2.40430        0.45040       -0.07360
O         -1.05490        0.02400       -0.04300
C          0.02310        0.87110       -0.18800
O         -0.21280        2.08940       -0.35650
C          1.38680        0.33710       -0.14230
C          1.62680       -1.00030        0.04310
C          2.93320       -1.45600        0.07890
C          4.02100       -0.60500       -0.06680
C          3.75390        0.74630       -0.25360
C          2.45310        1.20020       -0.28930
O          5.30780       -1.08520       -0.02700
H         -4.95760        0.53770       -0.15560
H         -5.28360       -1.19850       -0.55090
H         -5.08760       -0.64150        1.16120
H         -2.99490       -1.52560       -0.67460
H         -2.91710       -1.24560        1.08230
H         -2.67150        1.00090       -0.98000
H         -2.65430        1.10760        0.78920
H          0.79410       -1.65990        0.15580
H          3.16670       -2.50500        0.22350
H          4.59880        1.43380       -0.37060
H          2.30840        2.25890       -0.43710
H          5.83010       -1.15210        0.83550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

