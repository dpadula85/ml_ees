%nproc=16
%mem=2GB
%chk=mol_395_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.18670        0.92790       -0.90370
C         -3.33920       -0.48520       -0.37270
C         -2.64440       -0.51940        0.98240
O         -1.29060       -0.20180        0.92560
C         -0.31890       -0.87480        0.22160
O         -0.65480       -1.87630       -0.44210
C          1.05160       -0.40780        0.26970
C          2.01150       -1.09080       -0.43910
C          3.33350       -0.68270       -0.42380
C          3.69600        0.42280        0.31080
C          2.76060        1.12700        1.02990
C          1.44720        0.69470        0.99500
O          5.02070        0.81200        0.31160
H         -2.10450        1.16850       -0.90840
H         -3.67870        1.01710       -1.87210
H         -3.66140        1.59120       -0.12630
H         -4.41030       -0.70220       -0.27960
H         -2.91030       -1.17880       -1.09170
H         -2.82920       -1.54610        1.41460
H         -3.12990        0.17680        1.69640
H          1.75840       -1.97520       -1.03470
H          4.05570       -1.26330       -1.00610
H          3.01770        2.00550        1.62020
H          0.68280        1.23830        1.55930
H          5.32320        1.62250        0.84220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

