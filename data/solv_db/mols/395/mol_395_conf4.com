%nproc=16
%mem=2GB
%chk=mol_395_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.55010        0.88630        0.50220
C         -3.38130        0.17450       -0.78220
C         -2.42050       -0.95970       -0.85240
O         -1.10020       -0.75040       -0.61520
C         -0.36350       -0.39300        0.44950
O         -0.89440       -0.18350        1.54370
C          1.10270       -0.23350        0.36980
C          1.84870        0.13380        1.46920
C          3.20980        0.27100        1.35200
C          3.84010        0.04280        0.13530
C          3.07740       -0.32460       -0.95680
C          1.71200       -0.46040       -0.83160
O          5.19800        0.16880       -0.02540
H         -3.67220        0.23470        1.38170
H         -4.54010        1.46890        0.48560
H         -2.79560        1.68440        0.60430
H         -3.06410        0.90060       -1.59860
H         -4.41070       -0.13570       -1.15720
H         -2.50190       -1.35140       -1.92330
H         -2.85300       -1.82510       -0.25900
H          1.33980        0.30880        2.41530
H          3.83710        0.55680        2.18470
H          3.58050       -0.50180       -1.90750
H          1.13560       -0.74590       -1.68760
H          5.66620        1.03360       -0.29640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

