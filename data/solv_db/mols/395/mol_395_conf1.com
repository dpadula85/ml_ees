%nproc=16
%mem=2GB
%chk=mol_395_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.62150       -0.20010        0.11460
C          3.13260       -0.54320        0.26520
C          2.42200        0.74470        0.50940
O          1.04240        0.66080        0.67840
C          0.12560        0.18380       -0.23070
O          0.58990       -0.22010       -1.33750
C         -1.30190        0.12350        0.01110
C         -1.87290        0.54450        1.18860
C         -3.25760        0.47000        1.38370
C         -4.05390       -0.03490        0.37350
C         -3.50260       -0.45850       -0.80500
C         -2.13530       -0.37100       -0.96380
O         -5.44060       -0.13780        0.50000
H          4.78280        0.06250       -0.96210
H          4.83470        0.71610        0.69380
H          5.25090       -1.05550        0.43780
H          2.79530       -1.04340       -0.65680
H          2.99720       -1.26520        1.09480
H          2.83570        1.19340        1.45860
H          2.72590        1.45460       -0.29440
H         -1.27660        0.94340        1.99570
H         -3.73770        0.78670        2.28320
H         -4.08450       -0.86250       -1.62810
H         -1.68180       -0.70280       -1.89320
H         -5.81080       -0.98880        0.87840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_395_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

