%nproc=16
%mem=2GB
%chk=mol_082_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.62080       -0.04170       -0.03690
C          0.84910        0.08040        0.07290
O          1.55100       -0.90700        0.10870
H         -0.89140       -0.88430       -0.71290
H         -1.08980        0.86930       -0.51150
H         -1.12040       -0.16180        0.95680
H          1.32220        1.04500        0.12290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

