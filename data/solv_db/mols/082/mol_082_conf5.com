%nproc=16
%mem=2GB
%chk=mol_082_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.65290       -0.04500        0.01020
C          0.83280       -0.06910       -0.02380
O          1.38160        1.02580        0.09490
H         -1.10800       -1.02450       -0.23240
H         -0.95040        0.75060       -0.73460
H         -0.93700        0.30840        1.02650
H          1.43380       -0.94610       -0.14080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

