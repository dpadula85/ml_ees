%nproc=16
%mem=2GB
%chk=mol_082_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.61560       -0.02240        0.05960
C          0.84610        0.00080       -0.06340
O          1.45570        1.04240       -0.11860
H         -1.11480        0.90120       -0.29890
H         -1.01350       -0.84840       -0.58260
H         -0.96410       -0.16160        1.11090
H          1.40630       -0.91210       -0.10710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

