%nproc=16
%mem=2GB
%chk=mol_082_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.61660        0.00730       -0.12100
C          0.84280        0.02850        0.12360
O          1.50210       -0.98790        0.15590
H         -1.16820       -0.11480        0.84800
H         -0.92770       -0.86010       -0.73670
H         -0.99080        0.96820       -0.55000
H          1.35840        0.95880        0.28030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

