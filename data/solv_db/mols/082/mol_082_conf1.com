%nproc=16
%mem=2GB
%chk=mol_082_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.65310       -0.04040       -0.01150
C          0.83740       -0.05360       -0.01570
O          1.38970        1.03860       -0.00770
H         -0.94970        0.34160        1.00030
H         -1.09480       -1.03640       -0.15300
H         -0.95530        0.69570       -0.78710
H          1.42580       -0.94560       -0.02520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_082_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

