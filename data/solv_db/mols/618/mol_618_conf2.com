%nproc=16
%mem=2GB
%chk=mol_618_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.38170       -0.34020       -0.63500
C         -2.98470        0.96020       -0.97610
C         -1.66480        1.33490       -0.85840
C         -0.74780        0.41460       -0.40240
C         -1.11910       -0.86600       -0.06290
C         -2.46260       -1.25050       -0.18170
C          0.04070       -1.65650        0.39530
C          1.17660       -0.70860        0.29770
C          0.69060        0.48900       -0.16870
C          1.60510        1.53770       -0.33800
C          2.93760        1.38840       -0.05290
C          3.38560        0.17230        0.41260
C          2.53670       -0.89410        0.59920
H         -4.42220       -0.63540       -0.72790
H         -3.75570        1.63380       -1.32800
H         -1.36710        2.33050       -1.12070
H         -2.79230       -2.24710        0.07520
H          0.27900       -2.49550       -0.30640
H         -0.08410       -2.04390        1.42720
H          1.18220        2.47370       -0.71000
H          3.61330        2.21960       -0.19590
H          4.42950        0.03020        0.64380
H          2.90520       -1.84690        0.96710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_618_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_618_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_618_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

