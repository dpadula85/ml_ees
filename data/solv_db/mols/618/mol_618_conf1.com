%nproc=16
%mem=2GB
%chk=mol_618_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.43130        0.18770        0.09930
C         -3.03150       -1.13050        0.05240
C         -1.69270       -1.44760        0.02590
C         -0.74290       -0.41650        0.04710
C         -1.14560        0.88730        0.09360
C         -2.50790        1.19990        0.12030
C          0.06490        1.73660        0.10560
C          1.18300        0.77970        0.06240
C          0.70970       -0.51170        0.02670
C          1.59340       -1.58240       -0.01770
C          2.95550       -1.29180       -0.02430
C          3.42770       -0.00570        0.01130
C          2.55050        1.03290        0.05470
H         -4.48520        0.45690        0.12090
H         -3.78620       -1.90800        0.03690
H         -1.34380       -2.47190       -0.01100
H         -2.79470        2.24240        0.15700
H          0.08400        2.45170       -0.74420
H          0.14050        2.26570        1.08630
H          1.22130       -2.60530       -0.04610
H          3.64750       -2.11350       -0.05860
H          4.48920        0.19780        0.00540
H          2.89490        2.04640        0.08310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_618_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_618_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_618_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

