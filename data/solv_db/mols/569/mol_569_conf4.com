%nproc=16
%mem=2GB
%chk=mol_569_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.82440       -0.01200        0.61070
C         -0.31920        0.20240        0.69680
C          0.25140       -0.13730       -0.65230
C          1.75000        0.03460       -0.70330
I          2.77420       -1.17650        0.67460
H         -2.29610        0.09970        1.58690
H         -1.95250       -1.06560        0.23930
H         -2.22260        0.67910       -0.16880
H         -0.15830        1.26550        0.92300
H          0.08500       -0.39340        1.51330
H         -0.17080        0.59390       -1.38760
H          0.00550       -1.17040       -0.94690
H          2.01730        1.11040       -0.64940
H          2.06040       -0.29140       -1.73640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

