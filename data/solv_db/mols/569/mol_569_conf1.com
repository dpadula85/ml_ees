%nproc=16
%mem=2GB
%chk=mol_569_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.94760        0.12080       -0.02500
C         -0.62050       -0.63810       -0.08970
C          0.47210        0.38430        0.05340
C          1.84810       -0.20330        0.01010
I          3.24210        1.41010        0.22940
H         -2.80350       -0.50710       -0.25960
H         -1.83380        0.95590       -0.77000
H         -1.99490        0.61040        0.96090
H         -0.50380       -1.16420       -1.06010
H         -0.53470       -1.38510        0.69870
H          0.31850        0.88930        1.03180
H          0.37730        1.17740       -0.71680
H          1.98170       -0.75110       -0.91710
H          1.99890       -0.89950        0.85410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

