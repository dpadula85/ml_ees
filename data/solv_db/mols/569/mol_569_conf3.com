%nproc=16
%mem=2GB
%chk=mol_569_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.90560       -0.21050       -0.05850
C         -0.50070        0.06430       -0.54760
C          0.45350        0.05420        0.61320
C          1.87930        0.32410        0.18580
I          2.51360       -1.18110       -1.20380
H         -2.60260       -0.38840       -0.90070
H         -1.88590       -1.07440        0.65490
H         -2.28450        0.71450        0.46360
H         -0.25280       -0.76550       -1.26930
H         -0.52400        1.01680       -1.08750
H          0.13720        0.79530        1.35000
H          0.42610       -0.96040        1.05570
H          2.54850        0.31590        1.06720
H          1.99780        1.29520       -0.32300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

