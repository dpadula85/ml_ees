%nproc=16
%mem=2GB
%chk=mol_569_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.43300       -0.25220        0.75760
C         -0.59270       -0.79140       -0.35130
C          0.44850        0.18020       -0.85050
C          1.41070        0.58250        0.24730
I          2.86130        1.94420       -0.47390
H         -1.40050       -0.91310        1.67160
H         -2.51250       -0.24740        0.43510
H         -1.20820        0.80510        1.00920
H         -0.09360       -1.72860       -0.03000
H         -1.22900       -1.04790       -1.23660
H         -0.02290        1.10370       -1.22010
H          0.99240       -0.32800       -1.67390
H          1.95880       -0.31900        0.63640
H          0.82070        1.01190        1.07910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

