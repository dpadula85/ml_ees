%nproc=16
%mem=2GB
%chk=mol_569_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.47330        0.38500        0.63820
C          0.79100       -0.33190       -0.44610
C         -0.60400       -0.76970       -0.21570
C         -1.59960        0.30140        0.07840
I         -1.75610        1.72000       -1.50480
H          0.84570        0.71720        1.48000
H          2.30900       -0.20960        1.10660
H          1.98420        1.31110        0.25560
H          1.38970       -1.22820       -0.71890
H          0.79970        0.30650       -1.37840
H         -0.95130       -1.32160       -1.11220
H         -0.63580       -1.49640        0.62230
H         -2.59670       -0.18710        0.14270
H         -1.44910        0.80330        1.05230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_569_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

