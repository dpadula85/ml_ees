%nproc=16
%mem=2GB
%chk=mol_341_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.95700        3.21400        0.21250
C         -3.52680        1.95350        0.95790
O         -2.32030        1.59180        0.38770
P         -2.25020       -0.05240        0.10700
S         -2.61510       -0.93020        1.50990
O         -3.30590       -0.46050       -1.20680
C         -4.20290       -1.43270       -0.81200
C         -3.95760       -2.74540       -1.52810
O         -0.70350       -0.47490       -0.43350
C          0.50400       -0.05170        0.10490
C          0.82830       -0.49800        1.36230
C          2.11930       -0.31550        1.83370
N          3.06210        0.10580        0.95900
C          2.78430        0.37830       -0.33600
N          1.48210        0.39050       -0.70900
C          3.87030        0.18250       -1.38230
C          4.97710       -0.68490       -0.83240
C          4.40900        1.50840       -1.82170
C          2.61330       -1.03800        3.05020
H         -3.13820        3.63630       -0.40970
H         -4.37180        3.99000        0.84920
H         -4.76850        2.90950       -0.53490
H         -3.38110        2.33620        2.02580
H         -4.27860        1.19770        1.01540
H         -4.32760       -1.59770        0.23650
H         -5.22250       -1.08540       -1.20520
H         -3.13980       -2.68570       -2.28110
H         -4.83860       -3.14620       -2.04730
H         -3.68990       -3.55030       -0.76180
H          0.24930       -1.32500        1.81470
H          3.44750       -0.33400       -2.27240
H          5.56870       -0.16320       -0.05860
H          5.61780       -0.97900       -1.67470
H          4.57580       -1.59730       -0.36130
H          3.54720        2.19470       -1.94030
H          5.04990        1.96330       -1.03460
H          4.97240        1.34670       -2.76480
H          1.82970       -1.18840        3.80060
H          2.97370       -2.05220        2.74400
H          3.51400       -0.51050        3.43690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_341_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_341_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_341_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

