%nproc=16
%mem=2GB
%chk=mol_004_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.15240       -0.20730        0.14010
C         -1.38910       -1.10890       -0.59340
C         -0.04760       -0.88510       -0.86510
C          0.55680        0.26120       -0.39930
C         -0.21490        1.14660        0.32750
C         -1.54700        0.93980        0.60600
O          1.90870        0.51790       -0.65580
C          2.85460        0.05420        0.26140
O          2.46150       -0.57080        1.27400
H         -3.20630       -0.42130        0.33200
H         -1.83000       -2.02340       -0.97690
H          0.55010       -1.58610       -1.43700
H          0.28300        2.03720        0.68110
H         -2.15080        1.63120        1.17390
H          3.92330        0.21460        0.13130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_004_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_004_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_004_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

