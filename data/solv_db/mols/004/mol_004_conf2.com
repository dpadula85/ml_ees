%nproc=16
%mem=2GB
%chk=mol_004_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07070        0.57410        0.22780
C         -1.21510        1.41190       -0.46430
C          0.05910        0.98970       -0.79400
C          0.45980       -0.28160       -0.42000
C         -0.39300       -1.12230        0.27220
C         -1.66060       -0.69140        0.59580
O          1.73990       -0.76090       -0.72840
C          2.80930       -0.59360        0.10850
O          2.60130        0.02380        1.19880
H         -3.05590        0.87500        0.49320
H         -1.52480        2.42490       -0.76660
H          0.76310        1.60430       -1.33160
H         -0.03150       -2.12260        0.54760
H         -2.29750       -1.38810        1.14180
H          3.81670       -0.94310       -0.08080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_004_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_004_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_004_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

