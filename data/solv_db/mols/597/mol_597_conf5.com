%nproc=16
%mem=2GB
%chk=mol_597_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.18640        0.40660        0.30490
C         -0.27240       -0.36980       -0.61670
C          1.08780       -0.23550       -0.10400
C          2.20970       -0.11540        0.33890
H         -2.22260        0.07040        0.08380
H         -0.86570        0.18280        1.33860
H         -1.02220        1.47970        0.12180
H         -0.53240       -1.44930       -0.57050
H         -0.39300        0.03560       -1.62940
H          3.19730       -0.00530        0.73250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

