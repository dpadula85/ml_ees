%nproc=16
%mem=2GB
%chk=mol_597_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.20230       -0.49980       -0.08970
C         -0.29850        0.69160        0.15910
C          1.09320        0.24370        0.04700
C          2.24910       -0.14120       -0.04850
H         -2.20530       -0.38200        0.33690
H         -1.26040       -0.62860       -1.19090
H         -0.67610       -1.39430        0.30360
H         -0.49970        1.07030        1.18380
H         -0.45860        1.50210       -0.57850
H          3.25870       -0.46170       -0.12280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

