%nproc=16
%mem=2GB
%chk=mol_597_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24780        0.29480       -0.34150
C         -0.16510       -0.71270       -0.02010
C          1.14960       -0.04100       -0.02340
C          2.22720        0.50390       -0.01790
H         -1.35330        0.28690       -1.45600
H         -2.21040        0.01200        0.11810
H         -1.00860        1.31070        0.03550
H         -0.37930       -1.12170        1.00640
H         -0.18290       -1.51920       -0.78410
H          3.17070        0.98630       -0.02960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

