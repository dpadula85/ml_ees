%nproc=16
%mem=2GB
%chk=mol_597_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21610       -0.38350       -0.28850
C         -0.19870        0.73920       -0.13870
C          1.08920        0.12400        0.21790
C          2.14770       -0.38420        0.51470
H         -1.66240       -0.59990        0.71650
H         -0.64950       -1.26490       -0.66880
H         -1.97980       -0.16020       -1.05500
H         -0.48970        1.38490        0.71580
H         -0.12480        1.36270       -1.04370
H          3.08410       -0.81810        0.78550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

