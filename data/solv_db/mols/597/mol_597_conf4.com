%nproc=16
%mem=2GB
%chk=mol_597_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23190        0.42860       -0.23250
C         -0.25440       -0.54360        0.40690
C          1.12660       -0.19310        0.06040
C          2.27520        0.08440       -0.22360
H         -0.69810        0.99590       -1.04690
H         -2.06040       -0.10210       -0.74110
H         -1.57180        1.12790        0.54090
H         -0.47930       -1.54450        0.01000
H         -0.39260       -0.56790        1.52390
H          3.28670        0.31430       -0.47550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_597_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

