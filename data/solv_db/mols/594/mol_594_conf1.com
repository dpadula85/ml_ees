%nproc=16
%mem=2GB
%chk=mol_594_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31120       -0.00580        0.38530
C         -0.83800        0.01660        0.25750
C         -0.28770       -1.14770       -0.57190
C          1.14990       -1.29740       -0.08420
C          1.71940       -0.02030        0.43550
C          1.21130        1.21200       -0.21890
C         -0.28100        1.25940       -0.38410
H         -2.85030        0.79860       -0.11080
H         -2.65030        0.00160        1.46270
H         -2.69140       -0.99260       -0.00360
H         -0.39380       -0.01990        1.29420
H         -0.24900       -0.86740       -1.65850
H         -0.89610       -2.02670       -0.38230
H          1.78800       -1.81280       -0.81320
H          1.07230       -2.00120        0.79700
H          1.63030        0.08550        1.54680
H          2.83180       -0.05860        0.24800
H          1.76510        1.39260       -1.16240
H          1.48400        2.08140        0.44600
H         -0.47150        1.21830       -1.49740
H         -0.73190        2.18450        0.01450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

