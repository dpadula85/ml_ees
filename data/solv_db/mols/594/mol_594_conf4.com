%nproc=16
%mem=2GB
%chk=mol_594_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.37150       -0.09990        0.28350
C         -0.97450        0.02390       -0.23480
C         -0.20650       -1.19650        0.11350
C          1.24810       -1.14810       -0.15250
C          1.91160        0.16160        0.03870
C          1.05920        1.29530       -0.49960
C         -0.24100        1.23100        0.31420
H         -3.09930       -0.21340       -0.56090
H         -2.66430        0.76080        0.93530
H         -2.51720       -1.01140        0.91750
H         -1.02420        0.18450       -1.34950
H         -0.43400       -1.44430        1.19040
H         -0.63550       -2.04790       -0.49350
H          1.73750       -1.88230        0.55430
H          1.50810       -1.55520       -1.17220
H          2.84910        0.16980       -0.57550
H          2.21440        0.34300        1.09100
H          0.85000        1.03340       -1.57400
H          1.53880        2.26580       -0.44740
H          0.05900        0.97290        1.36810
H         -0.80780        2.15710        0.25350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

