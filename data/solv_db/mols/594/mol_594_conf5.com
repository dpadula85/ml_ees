%nproc=16
%mem=2GB
%chk=mol_594_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.30360        0.04070        0.42630
C          0.80540       -0.05770        0.54760
C          0.22110        1.25290        0.13730
C         -1.07950        1.16940       -0.57430
C         -1.88090        0.01500       -0.05120
C         -1.17930       -1.25650       -0.41800
C          0.29320       -1.23330       -0.22320
H          2.62750        1.03720        0.00310
H          2.75400       -0.76720       -0.16730
H          2.81430        0.00140        1.42850
H          0.58530       -0.26760        1.63540
H          0.10060        1.86460        1.07700
H          0.95130        1.81350       -0.51380
H         -1.00120        1.13560       -1.66970
H         -1.65320        2.11210       -0.33570
H         -2.02910        0.12450        1.03450
H         -2.88760        0.00230       -0.53350
H         -1.68080       -2.07880        0.17080
H         -1.42830       -1.47170       -1.48970
H          0.80060       -1.28450       -1.20600
H          0.56300       -2.15170        0.37870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

