%nproc=16
%mem=2GB
%chk=mol_594_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.34060       -0.45320       -0.17170
C          0.95790       -0.14120        0.26750
C          0.00080       -1.19900       -0.21300
C         -1.41230       -0.97470        0.23760
C         -1.86250        0.41610       -0.17750
C         -0.89460        1.39960        0.47940
C          0.44170        1.19150       -0.25030
H          3.04050       -0.52030        0.70860
H          2.37250       -1.48730       -0.61730
H          2.77320        0.25270       -0.89270
H          0.91830       -0.08250        1.38630
H          0.38250       -2.17120        0.12380
H          0.00130       -1.15050       -1.32600
H         -2.05300       -1.68610       -0.34450
H         -1.56830       -1.14650        1.31870
H         -1.79900        0.46190       -1.28730
H         -2.86350        0.66230        0.18450
H         -1.23680        2.43180        0.42580
H         -0.80300        1.05300        1.53870
H          1.11840        2.02440       -0.05460
H          0.14520        1.11900       -1.33580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

