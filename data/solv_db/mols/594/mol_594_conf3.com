%nproc=16
%mem=2GB
%chk=mol_594_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.73940       -0.96610        0.20150
C          0.80560       -0.27470       -0.75160
C         -0.51250       -0.97220       -0.89980
C         -1.34940       -0.78890        0.37070
C         -1.62530        0.71750        0.41390
C         -0.27590        1.33820        0.78640
C          0.68860        1.16200       -0.35730
H          2.03960       -1.95620       -0.23880
H          2.67940       -0.36950        0.22930
H          1.34970       -1.07510        1.21580
H          1.30420       -0.32130       -1.76530
H         -1.07790       -0.44200       -1.69910
H         -0.36550       -2.03220       -1.07270
H         -0.70750       -1.05520        1.21630
H         -2.25340       -1.38470        0.36080
H         -2.41820        0.98190        1.10560
H         -1.89700        1.04940       -0.62310
H          0.11720        0.71960        1.63350
H         -0.39590        2.37960        1.07500
H          1.70240        1.47850        0.02060
H          0.45240        1.81130       -1.22190

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_594_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

