%nproc=16
%mem=2GB
%chk=mol_497_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.75930       -0.05740       -0.15560
C          2.26220       -0.16250       -0.04460
C          1.69180       -0.00220        1.13130
C          0.25000       -0.10720        1.23570
O         -0.24450       -0.35790        2.35520
C         -0.52890        0.09290       -0.01350
C          0.05450       -0.84680       -1.01680
C          1.48770       -0.42940       -1.24890
C         -1.95990       -0.05850        0.28230
C         -2.90540        0.10550       -0.85420
C         -2.33220        0.91850        1.37720
H          4.02170        0.21610       -1.17870
H          4.24400       -0.99130        0.17480
H          4.11970        0.78110        0.50450
H          2.28090        0.20460        2.01390
H         -0.30980        1.13920       -0.36740
H          0.00540       -1.89880       -0.67670
H         -0.44020       -0.81340       -2.00640
H          2.02440       -1.22200       -1.83010
H          1.50590        0.50940       -1.87400
H         -2.13110       -1.08650        0.72010
H         -3.19360        1.18180       -1.01930
H         -3.86230       -0.42210       -0.65140
H         -2.53980       -0.25660       -1.82450
H         -1.48130        1.62770        1.59380
H         -3.19850        1.52000        1.03690
H         -2.57990        0.41550        2.33660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

