%nproc=16
%mem=2GB
%chk=mol_497_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.70950       -0.33580       -0.05180
C          2.23290       -0.23430       -0.03360
C          1.50400       -1.30430        0.25140
C          0.04970       -1.18500        0.27150
O         -0.60660       -2.15840       -0.18820
C         -0.51760        0.04000        0.82320
C          0.34800        1.25340        0.58430
C          1.48970        1.00560       -0.34790
C         -1.95670        0.29290        0.47440
C         -2.16040        0.39920       -1.03370
C         -2.91430       -0.71750        0.99560
H          4.12540       -0.39770        0.97870
H          4.09050        0.61350       -0.48190
H          4.06440       -1.18920       -0.65710
H          2.02170       -2.24150        0.44070
H         -0.51500       -0.09760        1.94920
H         -0.22960        2.10930        0.17490
H          0.80250        1.65920        1.53490
H          1.04950        0.91040       -1.38320
H          2.16860        1.85280       -0.38190
H         -2.27840        1.28340        0.86560
H         -1.99900        1.45960       -1.36680
H         -1.55460       -0.31810       -1.59140
H         -3.23820        0.18910       -1.22640
H         -2.78760       -0.95170        2.06900
H         -2.95590       -1.65100        0.36800
H         -3.94230       -0.28630        0.90610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

