%nproc=16
%mem=2GB
%chk=mol_497_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.64520        0.26110        0.21150
C          2.20150        0.26930       -0.14140
C          1.49190        1.36650       -0.12820
C          0.07510        1.36200       -0.47660
O         -0.59120        2.42990       -0.58270
C         -0.65640        0.09240       -0.72480
C          0.11040       -1.07850       -0.17320
C          1.56420       -1.00160       -0.51490
C         -2.02540        0.23010       -0.14770
C         -2.88260       -0.99450       -0.32950
C         -1.94120        0.63120        1.30060
H          4.02220       -0.75330        0.35130
H          3.80760        0.87820        1.11860
H          4.25170        0.72360       -0.60730
H          1.93830        2.30310        0.14130
H         -0.75510       -0.05040       -1.82320
H         -0.28630       -2.01860       -0.62570
H          0.00920       -1.19900        0.91940
H          2.08070       -1.85940       -0.01930
H          1.71380       -1.14940       -1.61860
H         -2.53460        1.06120       -0.68320
H         -3.16760       -1.09680       -1.40790
H         -2.44660       -1.90200        0.09170
H         -3.83700       -0.80270        0.21070
H         -0.94280        0.41840        1.71910
H         -2.09220        1.73770        1.42220
H         -2.75260        0.14150        1.84610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

