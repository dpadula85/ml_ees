%nproc=16
%mem=2GB
%chk=mol_497_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.70420        0.03500        0.18300
C          2.22580        0.06360        0.02780
C          1.73240        0.87170       -0.88650
C          0.28930        0.93720       -1.07850
O         -0.21780        1.93980       -1.63620
C         -0.49500       -0.22340       -0.58430
C         -0.03050       -0.69720        0.75590
C          1.43410       -0.80920        0.90360
C         -1.96160        0.11630       -0.55800
C         -2.25980        1.28580        0.33990
C         -2.82740       -1.03750       -0.17230
H          4.15680        1.03760        0.04180
H          4.20100       -0.66330       -0.50290
H          3.99410       -0.27480        1.22620
H          2.38960        1.49880       -1.50880
H         -0.33750       -1.04370       -1.30760
H         -0.48690       -1.71920        0.91160
H         -0.43810       -0.08820        1.60430
H          1.76910       -1.86750        0.69530
H          1.77260       -0.62080        1.95450
H         -2.23630        0.40460       -1.60920
H         -2.68470        2.13980       -0.24150
H         -1.37100        1.59620        0.93080
H         -3.06700        1.02510        1.08150
H         -2.66870       -1.93960       -0.78130
H         -2.70130       -1.24990        0.92300
H         -3.88550       -0.71720       -0.28460

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

