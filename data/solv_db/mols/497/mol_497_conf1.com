%nproc=16
%mem=2GB
%chk=mol_497_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.65080       -0.75380        0.23600
C          2.21010       -0.53510        0.41050
C          1.34220       -1.51970        0.40390
C         -0.09930       -1.27650        0.58100
O         -0.83400       -2.26260        0.83170
C         -0.65940        0.09730        0.46480
C          0.41440        0.97290       -0.14820
C          1.72190        0.86520        0.60240
C         -1.83080        0.13830       -0.50500
C         -2.95800       -0.75350       -0.08950
C         -2.22590        1.55880       -0.74730
H          3.87220       -1.65360       -0.37520
H          4.12880        0.14820       -0.19560
H          4.12080       -0.90090        1.23340
H          1.71400       -2.51500        0.26480
H         -0.95580        0.49980        1.45940
H          0.04550        2.01750       -0.19570
H          0.56540        0.60150       -1.18460
H          2.42060        1.59070        0.14210
H          1.56310        1.09960        1.67080
H         -1.41900       -0.25490       -1.47790
H         -2.82700       -1.76470       -0.53080
H         -3.09610       -0.82960        0.99920
H         -3.88810       -0.34640       -0.58590
H         -1.60540        1.99020       -1.58540
H         -3.27170        1.56520       -1.12960
H         -2.09950        2.22100        0.13320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_497_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

