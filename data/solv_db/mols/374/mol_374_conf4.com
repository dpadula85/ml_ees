%nproc=16
%mem=2GB
%chk=mol_374_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.79410        0.28780        0.05900
C          3.32880       -1.11880       -0.09290
C          1.95590       -1.29840       -0.63230
C          0.80640       -0.79170        0.13170
C          0.70240        0.65030        0.43780
C         -0.57350        0.99590        1.19950
C         -1.80890        0.65950        0.47360
C         -2.45970       -0.54440        0.61950
C         -3.61720       -0.85930       -0.06060
C         -4.12960        0.08800       -0.92060
C         -3.50730        1.30540       -1.09410
C         -2.35000        1.60170       -0.40470
H          3.84180        0.57830        1.11800
H          3.17780        0.98030       -0.57250
H          4.81710        0.36480       -0.42290
H          3.37480       -1.59250        0.92520
H          4.08710       -1.70940       -0.69650
H          1.93470       -0.93250       -1.70820
H          1.81520       -2.42480       -0.77410
H          0.68560       -1.38960        1.08120
H         -0.13030       -1.05630       -0.46720
H          0.72670        1.30550       -0.45010
H          1.53670        0.99520        1.10640
H         -0.50220        0.48320        2.17660
H         -0.54960        2.11410        1.33290
H         -2.05920       -1.27460        1.28630
H         -4.11180       -1.83260        0.08030
H         -5.02780       -0.16190       -1.44570
H         -3.92460        2.03790       -1.77450
H         -1.83330        2.53860       -0.51100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_374_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_374_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_374_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

