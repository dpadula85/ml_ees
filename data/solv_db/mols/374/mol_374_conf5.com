%nproc=16
%mem=2GB
%chk=mol_374_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.81200        0.70960        1.19520
C          2.99480       -0.11520        0.19350
C          1.65690       -0.48640        0.79320
C          0.86100       -1.30490       -0.18210
C          0.61820       -0.51420       -1.43210
C         -0.19160        0.73960       -1.18450
C         -1.54510        0.42860       -0.66290
C         -2.55330        0.18690       -1.60460
C         -3.87870       -0.02070       -1.22910
C         -4.20750        0.01270        0.10770
C         -3.22880        0.25020        1.04030
C         -1.89690        0.46020        0.65790
H          3.54640        0.32280        2.19970
H          4.87500        0.53750        0.93000
H          3.58860        1.76860        1.06040
H          2.89350        0.53440       -0.69370
H          3.52000       -1.04100       -0.07380
H          1.18810        0.44190        1.11900
H          1.80050       -1.14500        1.69150
H         -0.11740       -1.61430        0.24710
H          1.41120       -2.23160       -0.47930
H          1.54880       -0.22040       -1.95800
H          0.06500       -1.19820       -2.12630
H         -0.26100        1.32260       -2.15060
H          0.30990        1.39670       -0.43940
H         -2.28960        0.16160       -2.65660
H         -4.65070       -0.20690       -1.96710
H         -5.21620       -0.14320        0.44440
H         -3.49180        0.27530        2.08510
H         -1.16140        0.69270        1.40380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_374_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_374_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_374_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

