%nproc=16
%mem=2GB
%chk=mol_335_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.21910       -0.19230        0.11330
C          1.39870       -1.30060       -0.09070
C          0.03430       -1.16980       -0.24140
C         -0.52170        0.09450       -0.18710
C          0.29240        1.20550        0.01640
C          1.66060        1.06860        0.16690
C         -1.98670        0.31250       -0.34160
O         -2.64640        0.26490        0.88810
H          3.28450       -0.34300        0.22630
H          1.85290       -2.27490       -0.12930
H         -0.63280       -2.00340       -0.40070
H         -0.17350        2.19510        0.05510
H          2.28790        1.96000        0.32720
H         -2.21200        1.30740       -0.79320
H         -2.36310       -0.52910       -0.96210
H         -2.49430       -0.59550        1.35280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

