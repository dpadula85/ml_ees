%nproc=16
%mem=2GB
%chk=mol_335_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24020        0.33670        0.06930
C         -1.63140       -0.85550        0.46180
C         -0.24910       -0.99210        0.35450
C          0.52800        0.03720       -0.13550
C         -0.12400        1.20320       -0.51280
C         -1.48070        1.37230       -0.42110
C          2.01100       -0.09780       -0.25480
O          2.36390       -1.36130        0.19520
H         -3.30220        0.40200        0.16740
H         -2.17660       -1.69010        0.84930
H          0.22890       -1.93010        0.66360
H          0.48490        1.99420       -0.89080
H         -1.97700        2.29170       -0.72050
H          2.48220        0.65510        0.38610
H          2.31930       -0.03070       -1.32060
H          2.76310       -1.33470        1.10880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

