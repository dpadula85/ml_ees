%nproc=16
%mem=2GB
%chk=mol_335_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24500        0.18820       -0.04880
C         -1.64050       -1.04700       -0.18580
C         -0.27350       -1.20180       -0.08530
C          0.56120       -0.13510        0.15660
C         -0.04380        1.09620        0.29300
C         -1.40870        1.25320        0.19300
C          2.03200       -0.30680        0.26390
O          2.56080       -0.15100       -1.01640
H         -3.31970        0.28330       -0.13200
H         -2.26480       -1.92340       -0.37900
H          0.19840       -2.18770       -0.19560
H          0.60880        1.95180        0.48540
H         -1.87080        2.22140        0.30150
H          2.43140        0.46230        0.95640
H          2.32000       -1.28750        0.69160
H          2.35420        0.78380       -1.29850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

