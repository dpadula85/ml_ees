%nproc=16
%mem=2GB
%chk=mol_335_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26190        0.29620       -0.01080
C         -1.37680        1.35380        0.02730
C         -0.01390        1.14970        0.10340
C          0.52100       -0.11960        0.14460
C         -0.36230       -1.17870        0.10660
C         -1.72540       -0.97610        0.03050
C          1.98460       -0.36940        0.22700
O          2.55700       -0.46550       -1.04960
H         -3.33970        0.45530       -0.07110
H         -1.79440        2.35020       -0.00500
H          0.69160        1.96990        0.13410
H          0.05990       -2.19440        0.13930
H         -2.41160       -1.81950        0.00120
H          2.49730        0.43200        0.79030
H          2.16120       -1.36580        0.71650
H          2.81330        0.48190       -1.28420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

