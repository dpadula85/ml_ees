%nproc=16
%mem=2GB
%chk=mol_335_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.27300        0.24450       -0.42480
C         -1.72900       -0.98310       -0.11220
C         -0.37990       -1.21440        0.02310
C          0.48100       -0.14280       -0.16780
C         -0.02360        1.11080       -0.48380
C         -1.38660        1.28340       -0.60670
C          1.95370       -0.28360       -0.03810
O          2.27790       -0.00460        1.28450
H         -3.34450        0.37340       -0.51980
H         -2.39410       -1.82620        0.03870
H          0.05850       -2.16800        0.26690
H          0.63650        1.95320       -0.63500
H         -1.73720        2.27510       -0.85380
H          2.24180       -1.32800       -0.29380
H          2.41010        0.43000       -0.75250
H          3.20850        0.28040        1.35290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_335_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

