%nproc=16
%mem=2GB
%chk=mol_104_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.79930       -0.03810       -0.02460
C         -0.71740       -0.19790       -0.00460
Cl        -1.38160        1.46590        0.10970
H          1.32330       -0.96300       -0.24890
H          1.06460        0.41550        0.95650
H          1.03700        0.73410       -0.78670
H         -1.03300       -0.74230        0.92080
H         -1.09230       -0.67430       -0.92220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

