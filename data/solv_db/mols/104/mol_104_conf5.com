%nproc=16
%mem=2GB
%chk=mol_104_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79340        0.02040       -0.05500
C          0.70780       -0.18220        0.08920
Cl         1.55740        1.36170        0.23060
H         -1.29310        0.24070        0.88520
H         -1.24700       -0.88580       -0.49150
H         -0.91230        0.91040       -0.71980
H          1.03010       -0.63510       -0.88930
H          0.95060       -0.83030        0.95070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

