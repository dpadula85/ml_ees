%nproc=16
%mem=2GB
%chk=mol_104_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79250        0.01950       -0.06450
C          0.72340       -0.14730        0.05450
Cl         1.48190        1.42020       -0.26880
H         -1.06960        0.63280       -0.93650
H         -1.15070        0.52970        0.86260
H         -1.18860       -1.00980       -0.15100
H          0.89720       -0.49630        1.10280
H          1.09900       -0.94880       -0.59910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

