%nproc=16
%mem=2GB
%chk=mol_104_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77000        0.18640        0.02690
C          0.65740       -0.25580       -0.03600
Cl         1.81200        1.07730       -0.02250
H         -1.16750        0.00040        1.05930
H         -0.92380        1.24790       -0.25330
H         -1.36960       -0.49400       -0.63430
H          0.89710       -0.91510        0.82220
H          0.86440       -0.84710       -0.96230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

