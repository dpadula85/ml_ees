%nproc=16
%mem=2GB
%chk=mol_104_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.77460        0.09760       -0.03480
C          0.71360       -0.23230        0.02230
Cl         1.63920        1.26470        0.11460
H         -0.92710        1.19890       -0.14040
H         -1.29090       -0.18020        0.91600
H         -1.23110       -0.40860       -0.91560
H          0.85230       -0.89280        0.90230
H          1.01850       -0.84720       -0.86450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_104_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

