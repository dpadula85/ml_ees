%nproc=16
%mem=2GB
%chk=mol_426_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.19330        1.87380        1.63410
C         -2.09100        0.87930        0.52420
C         -2.46860       -0.52360        0.94660
C         -1.41340       -1.34120        0.29600
C         -1.30130       -2.70670        0.11580
C         -0.20340       -3.20450       -0.53050
C          0.79930       -2.36550       -1.00860
C          0.69820       -1.00720       -0.83500
C         -0.40450       -0.50570       -0.18570
O         -0.73310        0.80560        0.10870
O          1.67750       -0.14460       -1.29810
C          2.74520        0.20240       -0.49330
O          2.79060       -0.28950        0.65890
N          3.72170        1.08980       -1.01520
C          4.83810        1.44610       -0.15940
C         -2.97240        1.23160       -0.66070
H         -3.16870        2.41260        1.52940
H         -1.40530        2.66040        1.55030
H         -2.08360        1.41470        2.64900
H         -3.47440       -0.81150        0.57110
H         -2.35590       -0.61700        2.02740
H         -2.04930       -3.38800        0.46760
H         -0.10870       -4.26980       -0.67510
H          1.66780       -2.76610       -1.52040
H          3.67200        1.48450       -1.95920
H          5.59330        0.63930       -0.28320
H          4.46360        1.47560        0.87420
H          5.26960        2.43630       -0.44350
H         -2.54540        0.71620       -1.54320
H         -2.96920        2.33100       -0.81290
H         -3.99540        0.84180       -0.52960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_426_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_426_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_426_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

