%nproc=16
%mem=2GB
%chk=mol_017_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.14940        1.30860       -0.72420
C         -0.04710       -0.04500       -0.04090
C         -0.73980        0.19950        1.26700
C          1.35740       -0.54190        0.26840
C         -0.79620       -0.99910       -0.91090
H          0.64180        1.14090       -1.69320
H         -0.76310        1.90180       -0.75690
H          0.87530        1.85850       -0.05520
H         -0.23850       -0.38730        2.08220
H         -1.80510       -0.09640        1.23070
H         -0.71940        1.28750        1.53450
H          1.92660       -0.72730       -0.64220
H          1.81410        0.25910        0.91600
H          1.24430       -1.42450        0.93000
H         -0.18460       -1.29300       -1.78100
H         -1.70860       -0.53450       -1.28870
H         -1.00670       -1.90680       -0.33560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

