%nproc=16
%mem=2GB
%chk=mol_017_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.32030        1.48700        0.00500
C          0.00340        0.02280       -0.03370
C         -0.87440       -0.37060       -1.18870
C          1.31900       -0.73860       -0.13600
C         -0.73860       -0.32350        1.24500
H         -0.60590        2.09400       -0.01750
H          0.77810        1.69020        1.01270
H          1.07500        1.80640       -0.73230
H         -0.63840       -1.45430       -1.39810
H         -0.76310        0.25580       -2.07700
H         -1.94980       -0.38900       -0.86370
H          2.19540       -0.07960        0.01990
H          1.34680       -1.56950        0.59180
H          1.42850       -1.16410       -1.16510
H         -0.94840        0.59030        1.86110
H         -1.74700       -0.75870        1.03490
H         -0.20100       -1.09840        1.84160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

