%nproc=16
%mem=2GB
%chk=mol_017_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.23730        1.47380        0.33610
C          0.04990        0.02570       -0.06410
C         -0.75590       -0.59820        1.08510
C          1.34260       -0.73140       -0.13740
C         -0.79660       -0.12620       -1.30130
H         -0.71700        1.99430        0.12430
H          0.37070        1.54960        1.45130
H          1.10530        1.94980       -0.11370
H         -1.76010       -0.09730        1.02210
H         -0.80140       -1.67450        0.87530
H         -0.21000       -0.33830        2.00570
H          1.69470       -0.92300       -1.15970
H          2.13660       -0.21410        0.45050
H          1.25210       -1.74750        0.34500
H         -1.85140       -0.42200       -1.06550
H         -0.87550        0.82760       -1.88400
H         -0.42120       -0.94820       -1.96970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

