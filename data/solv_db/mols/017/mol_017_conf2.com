%nproc=16
%mem=2GB
%chk=mol_017_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.00750        1.49050       -0.32850
C          0.02570        0.00270        0.01710
C         -1.06410       -0.63530       -0.83310
C          1.38280       -0.56360       -0.30700
C         -0.25640       -0.24300        1.46850
H         -1.07450        1.79130       -0.23050
H          0.28590        1.54080       -1.41460
H          0.67790        2.08030        0.29340
H         -0.72990       -1.60580       -1.25000
H         -1.94990       -0.81370       -0.20410
H         -1.39040        0.02990       -1.65860
H          2.13140        0.21950       -0.01320
H          1.47160       -0.86400       -1.35920
H          1.53190       -1.45330        0.34430
H         -0.65720        0.69650        1.90180
H         -1.02240       -1.05580        1.55040
H          0.63000       -0.61710        2.02330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

