%nproc=16
%mem=2GB
%chk=mol_017_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.46040       -0.37790       -0.33600
C          0.03340       -0.04640       -0.04750
C         -0.38930        1.29200       -0.64020
C         -0.92820       -1.07910       -0.55310
C         -0.13980        0.13730        1.46490
H          1.97370       -0.69020        0.62100
H          1.98510        0.50620       -0.72030
H          1.57830       -1.27150       -1.00460
H          0.26400        2.06900       -0.17490
H         -0.39430        1.30140       -1.72490
H         -1.41540        1.50260       -0.22130
H         -1.02040       -1.00730       -1.65460
H         -0.64040       -2.11800       -0.28930
H         -1.93010       -0.91050       -0.12250
H          0.29790       -0.68790        2.03490
H         -1.20580        0.32570        1.65960
H          0.47100        1.05470        1.70870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_017_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

