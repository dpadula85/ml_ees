%nproc=16
%mem=2GB
%chk=mol_175_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.36340       -0.16680        0.06150
C          0.06190        0.17250       -0.06660
C          0.43870        1.36960       -0.45120
C          1.05570       -0.90870        0.26140
H         -1.54010       -1.25730        0.16310
H         -1.95050        0.23090       -0.79900
H         -1.75490        0.35250        0.95910
H         -0.29360        2.11860       -0.67790
H          1.48990        1.58360       -0.53450
H          2.06870       -0.65600       -0.08340
H          0.71390       -1.86260       -0.19520
H          1.07350       -0.97630        1.36250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

