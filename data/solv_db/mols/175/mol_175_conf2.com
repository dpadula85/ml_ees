%nproc=16
%mem=2GB
%chk=mol_175_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23290       -0.63240       -0.27370
C         -0.01100        0.15860       -0.02000
C         -0.04980        1.46550       -0.14560
C          1.27300       -0.49450        0.37710
H         -0.99940       -1.63360       -0.71600
H         -1.94950       -0.10940       -0.92730
H         -1.71810       -0.81140        0.70760
H         -0.97910        1.93010       -0.43260
H          0.84820        2.03470        0.04170
H          1.43750       -0.29710        1.45460
H          2.14440       -0.04590       -0.18660
H          1.23650       -1.56460        0.12070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

