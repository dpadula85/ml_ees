%nproc=16
%mem=2GB
%chk=mol_175_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.35670       -0.41750        0.03280
C         -0.00490        0.13550        0.33520
C          0.13090        1.38020        0.72850
C          1.23400       -0.68670        0.20700
H         -1.73570        0.03560       -0.91660
H         -1.35340       -1.50200       -0.09090
H         -2.00090       -0.16320        0.90540
H          1.10810        1.76770        0.94330
H         -0.75190        1.99010        0.82730
H          1.93310       -0.11200       -0.46860
H          1.73940       -0.78060        1.18080
H          1.05810       -1.64720       -0.30770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

