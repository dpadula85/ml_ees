%nproc=16
%mem=2GB
%chk=mol_175_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.29690       -0.45040       -0.10340
C          0.04460        0.20090       -0.03910
C          0.18460        1.47750       -0.32440
C          1.18700       -0.67610        0.36170
H         -1.52530       -0.63370       -1.17550
H         -1.32210       -1.40570        0.46040
H         -2.08110        0.22770        0.30080
H          1.18830        1.91170       -0.26350
H         -0.67320        2.05950       -0.60680
H          1.08930       -0.79810        1.47960
H          1.04800       -1.64880       -0.15420
H          2.15680       -0.26450        0.06450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

