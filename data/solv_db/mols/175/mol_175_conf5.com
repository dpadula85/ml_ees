%nproc=16
%mem=2GB
%chk=mol_175_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.42700       -0.00690        0.05410
C         -0.04560        0.16070       -0.05540
C         -0.61850        1.29470       -0.43160
C         -0.90510       -0.99130        0.27110
H          1.97990        0.68490       -0.61140
H          1.70760        0.21450        1.11020
H          1.67670       -1.06800       -0.13360
H         -0.06400        2.19650       -0.69160
H         -1.69510        1.38420       -0.50110
H         -1.46770       -1.26280       -0.65090
H         -1.64090       -0.72410        1.05230
H         -0.35420       -1.88250        0.58780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_175_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

