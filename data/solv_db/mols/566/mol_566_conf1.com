%nproc=16
%mem=2GB
%chk=mol_566_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.40730        0.85380        0.23990
C         -0.75780       -0.22640       -0.57930
C          0.20550       -1.05900        0.22850
O          1.26050       -0.37550        0.80050
C          2.20430        0.32000        0.07060
O          2.07920        0.31640       -1.17400
H         -0.73440        1.23070        1.05050
H         -2.35720        0.48630        0.70700
H         -1.69420        1.68880       -0.42880
H         -1.60730       -0.87250       -0.94400
H         -0.33780        0.22730       -1.48060
H          0.52480       -1.96250       -0.34150
H         -0.41100       -1.48260        1.07750
H          3.03270        0.85530        0.56870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

