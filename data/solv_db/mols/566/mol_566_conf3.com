%nproc=16
%mem=2GB
%chk=mol_566_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.11520        0.02960       -0.03840
C         -0.77990        0.53920        0.52080
C          0.35590       -0.17210       -0.20640
O          1.57320        0.31460        0.32580
C          2.77030       -0.18000       -0.17270
O          2.72460       -1.04250       -1.07910
H         -2.01540       -0.21490       -1.11230
H         -2.40400       -0.88800        0.51630
H         -2.85000        0.83340        0.15960
H         -0.79610        0.26640        1.59520
H         -0.70060        1.63240        0.45980
H          0.27180       -0.06570       -1.29400
H          0.28550       -1.25040        0.08510
H          3.67980        0.19820        0.24030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

