%nproc=16
%mem=2GB
%chk=mol_566_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.78370        0.58370        0.14410
C         -1.03200       -0.67750       -0.13620
C          0.43440       -0.59230        0.24960
O          1.09040        0.44110       -0.46240
C          2.42190        0.66970       -0.24640
O          3.05850       -0.03530        0.58060
H         -1.40040        1.15980        1.00640
H         -1.85290        1.24700       -0.73010
H         -2.84680        0.29240        0.38450
H         -1.06990       -0.87150       -1.23000
H         -1.43980       -1.55530        0.44150
H          0.55480       -0.51580        1.33870
H          0.87510       -1.58860       -0.04620
H          2.99030        1.44250       -0.75430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

