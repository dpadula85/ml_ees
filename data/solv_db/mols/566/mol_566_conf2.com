%nproc=16
%mem=2GB
%chk=mol_566_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.07120       -0.14520        0.25370
C         -0.85290       -0.48340       -0.56570
C          0.39710        0.18980        0.02080
O          1.49360       -0.16380       -0.78570
C          2.75460        0.31540       -0.47400
O          2.84980        1.05680        0.53630
H         -2.82100        0.35840       -0.40920
H         -1.81270        0.50150        1.10510
H         -2.57860       -1.06510        0.63990
H         -0.72690       -1.58040       -0.57260
H         -0.97560       -0.13850       -1.62000
H          0.24060        1.28250        0.07540
H          0.47630       -0.18470        1.05540
H          3.62690        0.05680       -1.08380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_566_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

