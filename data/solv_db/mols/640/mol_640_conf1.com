%nproc=16
%mem=2GB
%chk=mol_640_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.98840        1.32430       -0.51940
C          1.65310       -0.14240       -0.52560
C          2.01670       -0.85160        0.74080
S         -0.04970       -0.38270       -1.04010
C         -1.23990       -0.15400        0.29320
C         -2.18240       -1.35260        0.22120
C         -2.10030        1.06730        0.09670
H          1.87200        1.67920       -1.57590
H          3.02620        1.55020       -0.24280
H          1.22870        1.88200        0.05780
H          2.30010       -0.59340       -1.32160
H          1.70070       -1.90120        0.64240
H          1.58900       -0.41640        1.65420
H          3.13390       -0.83800        0.81120
H         -0.76820       -0.11740        1.28810
H         -1.56040       -2.25250        0.37850
H         -3.02850       -1.26210        0.92290
H         -2.50900       -1.38690       -0.84980
H         -3.05100        0.80390       -0.43170
H         -2.41790        1.48640        1.08110
H         -1.60150        1.85800       -0.49270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

