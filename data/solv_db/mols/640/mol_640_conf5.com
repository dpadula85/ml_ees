%nproc=16
%mem=2GB
%chk=mol_640_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.47870        0.81870        0.86380
C          1.51750        0.22210       -0.16160
C          2.16720       -1.08260       -0.60950
S         -0.02290       -0.04050        0.71080
C         -1.48510       -0.03990       -0.31610
C         -2.40730       -1.12930        0.22160
C         -2.26800        1.26170       -0.19040
H          3.47030        1.00500        0.43110
H          1.97880        1.75170        1.22650
H          2.52570        0.14930        1.75830
H          1.45590        0.88470       -1.03260
H          3.13670       -0.81270       -1.08260
H          2.38220       -1.74530        0.24120
H          1.55730       -1.55480       -1.40720
H         -1.30290       -0.20140       -1.38810
H         -2.18470       -2.07060       -0.30450
H         -3.46650       -0.83070        0.19850
H         -2.17350       -1.32560        1.29930
H         -1.64090        2.13260       -0.50650
H         -3.16590        1.16980       -0.81870
H         -2.55280        1.43770        0.86670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

