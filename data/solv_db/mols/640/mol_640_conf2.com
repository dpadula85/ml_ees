%nproc=16
%mem=2GB
%chk=mol_640_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.11610       -1.31950       -0.23580
C          1.44990       -0.01840        0.12420
C          2.46630        1.10460       -0.02280
S          0.03460        0.33720       -0.97130
C         -1.50600        0.26370       -0.04870
C         -2.02730       -1.14240       -0.07470
C         -2.56010        1.20840       -0.54530
H          2.77760       -1.58560        0.61760
H          2.70890       -1.26390       -1.16220
H          1.39100       -2.14760       -0.36900
H          1.13470       -0.07950        1.18940
H          2.00630        1.97750       -0.50140
H          2.86730        1.38560        0.97500
H          3.32320        0.74140       -0.61610
H         -1.26820        0.56880        1.00190
H         -1.50370       -1.69210        0.74830
H         -1.75050       -1.61990       -1.04050
H         -3.11870       -1.09680        0.09140
H         -3.37450        1.20150        0.22300
H         -2.17020        2.24830       -0.59430
H         -2.99690        0.92850       -1.51540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

