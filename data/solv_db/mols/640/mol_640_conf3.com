%nproc=16
%mem=2GB
%chk=mol_640_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24190        0.90890       -0.90030
C         -1.50840        0.03440        0.07500
C         -2.37820       -1.16260        0.44350
S          0.04560       -0.60470       -0.55110
C          1.48870        0.32080        0.00090
C          2.59170        0.11680       -1.01260
C          2.01910       -0.14900        1.33710
H         -1.60450        1.38450       -1.65880
H         -2.75630        1.74560       -0.36380
H         -3.04680        0.33880       -1.41180
H         -1.28100        0.66110        0.97490
H         -3.17520       -1.32810       -0.30380
H         -1.78860       -2.08560        0.57190
H         -2.80800       -0.93850        1.45170
H          1.23760        1.39550        0.11720
H          3.50140       -0.34910       -0.57330
H          2.27230       -0.59000       -1.81910
H          2.87050        1.08880       -1.44100
H          2.44390        0.71990        1.90180
H          2.86850       -0.86360        1.20630
H          1.24970       -0.64410        1.95530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

