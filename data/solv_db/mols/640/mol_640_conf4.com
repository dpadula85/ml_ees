%nproc=16
%mem=2GB
%chk=mol_640_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.54510        0.94580        0.38010
C         -1.25200        0.15310        0.24860
C         -1.61420       -1.26260       -0.14380
S         -0.19290        0.80940       -1.07050
C          1.53510        0.39080       -0.79740
C          2.13240        1.15090        0.38140
C          1.78200       -1.06580       -0.56000
H         -2.45100        1.97860        0.02290
H         -3.37140        0.39200       -0.13300
H         -2.80350        0.92300        1.46930
H         -0.67800        0.21660        1.18500
H         -1.39040       -1.47370       -1.20610
H         -1.05000       -1.94470        0.50850
H         -2.70410       -1.43350       -0.00150
H          2.10980        0.69940       -1.68390
H          3.05000        0.60920        0.72490
H          2.38450        2.18570        0.06730
H          1.42430        1.11600        1.22630
H          0.97860       -1.73150       -0.90090
H          1.93090       -1.25180        0.52970
H          2.72500       -1.40700       -1.05490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_640_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

