%nproc=16
%mem=2GB
%chk=mol_352_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.71490       -0.00130       -0.13930
C         -1.22940        0.00230       -0.06680
C         -0.47920       -1.07850       -0.48390
C          0.89990       -1.07670       -0.41680
C          1.59260        0.01050        0.07250
C          0.84610        1.09220        0.49010
C         -0.53270        1.08860        0.42230
N          3.01650        0.00610        0.13900
H         -3.11050       -0.32290        0.85840
H         -3.12830        0.98810       -0.41740
H         -3.06120       -0.72280       -0.88020
H         -0.99610       -1.94900       -0.87380
H          1.50750       -1.92810       -0.74440
H          1.40000        1.95040        0.87670
H         -1.10710        1.94760        0.75450
H          3.63020        0.16150       -0.67170
H          3.46650       -0.16810        1.08090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

