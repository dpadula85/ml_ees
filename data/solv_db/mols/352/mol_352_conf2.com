%nproc=16
%mem=2GB
%chk=mol_352_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.71110       -0.01670        0.02750
C          1.22260        0.00020        0.00680
C          0.50690        0.89820       -0.75880
C         -0.87090        0.89440       -0.76180
C         -1.60990       -0.00480       -0.00190
C         -0.88570       -0.90040        0.76160
C          0.48730       -0.89180        0.76060
N         -3.02840        0.00630       -0.01740
H          3.10030       -1.05740       -0.07950
H          3.09940        0.54890       -0.85290
H          3.12270        0.44700        0.94740
H          1.05630        1.61450       -1.36430
H         -1.45170        1.59540       -1.35970
H         -1.43690       -1.60150        1.35410
H          1.07390       -1.59890        1.36350
H         -3.50460        0.84380        0.35480
H         -3.59240       -0.77710       -0.38010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

