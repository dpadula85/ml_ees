%nproc=16
%mem=2GB
%chk=mol_352_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.69950        0.05320        0.30830
C         -1.21900        0.01350        0.13760
C         -0.44820        1.12520        0.41140
C          0.92400        1.10990        0.26000
C          1.58480       -0.02040       -0.17270
C          0.82460       -1.13970       -0.45010
C         -0.55380       -1.11620       -0.29530
N          2.99650       -0.04620       -0.33140
H         -2.97470       -0.02690        1.39930
H         -3.14500        0.99340       -0.05350
H         -3.12980       -0.77660       -0.27950
H         -0.94740        2.02790        0.75400
H          1.53450        1.98540        0.47520
H          1.34910       -2.03530       -0.79300
H         -1.14750       -2.00600       -0.51670
H          3.68530        0.26380        0.38240
H          3.36620       -0.40470       -1.23600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

