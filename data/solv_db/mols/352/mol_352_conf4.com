%nproc=16
%mem=2GB
%chk=mol_352_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70380        0.17300       -0.08430
C         -1.21430        0.09140       -0.03140
C         -0.43930        1.12220        0.45910
C          0.93700        1.02210        0.49650
C          1.60750       -0.09980        0.05150
C          0.82190       -1.13240       -0.44050
C         -0.55290       -1.03130       -0.47740
N          3.01250       -0.22930        0.07860
H         -3.16770       -0.38620        0.76150
H         -3.10720       -0.25140       -1.02560
H         -2.95750        1.24700       -0.00320
H         -0.94130        2.01560        0.81560
H          1.50770        1.84490        0.88510
H          1.30410       -2.02870       -0.79930
H         -1.18960       -1.83460       -0.86130
H          3.48530       -1.15420        0.16300
H          3.59750        0.63160        0.01210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

