%nproc=16
%mem=2GB
%chk=mol_352_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70860        0.21900       -0.01810
C         -1.22430        0.10340       -0.01450
C         -0.60750       -1.11660       -0.19680
C          0.76880       -1.22980       -0.19450
C          1.59270       -0.13560       -0.01090
C          0.97530        1.08740        0.17190
C         -0.40170        1.19560        0.16880
N          3.00540       -0.24730       -0.00780
H         -2.96950        1.28730       -0.15770
H         -3.10340       -0.08150        0.98550
H         -3.16930       -0.44360       -0.76590
H         -1.24970       -1.99680       -0.34430
H          1.25800       -2.18610       -0.33730
H          1.62390        1.96000        0.31820
H         -0.86380        2.16320        0.31380
H          3.60180       -0.29460       -0.84830
H          3.47180       -0.28410        0.93780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_352_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

