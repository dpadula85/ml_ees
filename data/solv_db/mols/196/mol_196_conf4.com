%nproc=16
%mem=2GB
%chk=mol_196_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.89530        0.15180       -0.00710
C         -1.40800        0.07340        0.00670
C         -0.65930        0.64760       -1.00110
C          0.71940        0.59310       -1.01770
C          1.41700       -0.04660       -0.01350
C          0.66460       -0.61880        0.99120
C         -0.71430       -0.56760        1.01330
C          2.88960       -0.13540        0.01860
H         -3.29740       -0.56230       -0.78340
H         -3.28940       -0.18680        0.96420
H         -3.25370        1.16850       -0.29750
H         -1.23700        1.14810       -1.78470
H          1.27380        1.05060       -1.81700
H          1.21020       -1.12120        1.78010
H         -1.29380       -1.02410        1.81280
H          3.26510       -1.04140       -0.53890
H          3.20640       -0.25750        1.07970
H          3.40220        0.72860       -0.40560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

