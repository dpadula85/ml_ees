%nproc=16
%mem=2GB
%chk=mol_196_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.91120       -0.01390       -0.09850
C          1.41440       -0.01430       -0.05540
C          0.67070       -0.97400       -0.71210
C         -0.72290       -0.95800       -0.66030
C         -1.40030        0.01120        0.04420
C         -0.66510        0.97460        0.70360
C          0.71410        0.94760        0.64460
C         -2.89420        0.04930        0.11450
H          3.34510       -0.12180        0.91430
H          3.31450        0.87320       -0.61690
H          3.23110       -0.90330       -0.67250
H          1.18060       -1.74710       -1.27300
H         -1.32130       -1.70370       -1.17000
H         -1.21570        1.73590        1.25750
H          1.29340        1.70690        1.16440
H         -3.26980        1.07710       -0.07240
H         -3.25510       -0.30560        1.12220
H         -3.33080       -0.63420       -0.63410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

