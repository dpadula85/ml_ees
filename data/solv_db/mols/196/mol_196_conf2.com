%nproc=16
%mem=2GB
%chk=mol_196_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.89590       -0.03620        0.01010
C          1.41220        0.00770       -0.00270
C          0.70190        1.19110       -0.03400
C         -0.68590        1.18140       -0.04420
C         -1.41650        0.01200       -0.02410
C         -0.69590       -1.16510        0.00710
C          0.68460       -1.16410        0.01750
C         -2.91120        0.02080       -0.03620
H          3.29720        0.97880        0.28520
H          3.27950       -0.70770        0.83120
H          3.33520       -0.37450       -0.93060
H          1.24310        2.13130       -0.05060
H         -1.25950        2.11170       -0.06870
H         -1.22530       -2.11310        0.02400
H          1.26530       -2.09740        0.04210
H         -3.31590       -0.83140        0.57990
H         -3.31300        0.93890        0.46860
H         -3.29170       -0.08410       -1.07450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

