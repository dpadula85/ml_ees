%nproc=16
%mem=2GB
%chk=mol_196_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.89210        0.23160        0.19100
C         -1.41390        0.10360        0.10730
C         -0.71750       -0.88980        0.76620
C          0.66040       -0.99850        0.68030
C          1.38370       -0.10220       -0.07810
C          0.70330        0.89780       -0.74370
C         -0.66790        0.99000       -0.64640
C          2.88040       -0.19210       -0.19270
H         -3.30800       -0.70170        0.63490
H         -3.34930        0.29140       -0.83370
H         -3.13610        1.09890        0.81900
H         -1.26370       -1.61410        1.37450
H          1.22740       -1.76700        1.18630
H          1.30180        1.59060       -1.33460
H         -1.19890        1.77680       -1.17070
H          3.38090        0.57010        0.44050
H          3.21480       -1.23280        0.06800
H          3.19470       -0.05250       -1.26830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

