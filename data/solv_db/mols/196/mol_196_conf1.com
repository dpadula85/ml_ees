%nproc=16
%mem=2GB
%chk=mol_196_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.85020        0.19240        0.36660
C          1.37430        0.03900        0.27200
C          0.64410        1.20300        0.12940
C         -0.73400        1.09980        0.03810
C         -1.36090       -0.12150        0.08730
C         -0.63050       -1.27880        0.22940
C          0.75970       -1.17920        0.32190
C         -2.83440       -0.17540       -0.01460
H          3.37400       -0.77310        0.46930
H          3.02480        0.84840        1.26840
H          3.20320        0.74490       -0.51090
H          1.09300        2.18240        0.08610
H         -1.35550        1.97290       -0.07420
H         -1.11350       -2.23320        0.26830
H          1.37520       -2.06570        0.43480
H         -3.16790        0.55740       -0.77300
H         -3.21270       -1.17520       -0.30200
H         -3.28910        0.16160        0.95290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_196_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

