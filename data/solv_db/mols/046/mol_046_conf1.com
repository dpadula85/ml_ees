%nproc=16
%mem=2GB
%chk=mol_046_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.31370        0.15380       -0.15620
C         -1.85390       -1.14730       -0.13920
C         -0.48730       -1.32970       -0.04850
C          0.37980       -0.25780        0.02190
C         -0.08460        1.04290        0.00460
C         -1.44880        1.22230       -0.08590
C          1.82090       -0.48580        0.11760
O          2.25870       -1.66850        0.13490
N          2.76860        0.55220        0.19330
H         -3.38370        0.28300       -0.22720
H         -2.52680       -2.00110       -0.19400
H         -0.11830       -2.36340       -0.03480
H          0.59150        1.89570        0.05960
H         -1.84740        2.24630       -0.10170
H          3.58900        0.45800        0.82190
H          2.65610        1.39960       -0.36640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

