%nproc=16
%mem=2GB
%chk=mol_046_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32420        0.25760       -0.19200
C         -1.89360       -1.01400        0.07150
C         -0.53640       -1.29250        0.20650
C          0.42400       -0.30040        0.08030
C         -0.04210        0.96970       -0.18510
C         -1.38560        1.25980       -0.32140
C          1.83060       -0.60890        0.22430
O          2.23990       -1.76020        0.46410
N          2.77400        0.43210        0.08830
H         -3.37200        0.50240       -0.30150
H         -2.62560       -1.80810        0.17480
H         -0.19950       -2.30070        0.41470
H          0.66560        1.77100       -0.29130
H         -1.75370        2.26440       -0.53140
H          2.54310        1.34970        0.52410
H          3.65570        0.27810       -0.42580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

