%nproc=16
%mem=2GB
%chk=mol_046_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.35070        0.21150        0.24330
C          1.46280        1.24810        0.47290
C          0.08850        0.99580        0.47790
C         -0.40640       -0.26900        0.25820
C          0.48570       -1.28430        0.03240
C          1.86800       -1.06210        0.02140
C         -1.83880       -0.52990        0.26360
O         -2.30370       -1.69940        0.06080
N         -2.79720        0.47580        0.48970
H          3.43660        0.36950        0.23160
H          1.84770        2.23120        0.64370
H         -0.62380        1.79060        0.65520
H          0.13870       -2.29670       -0.14540
H          2.57930       -1.85640       -0.15570
H         -2.83470        0.95680        1.39470
H         -3.45350        0.71850       -0.26030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

