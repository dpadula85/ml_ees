%nproc=16
%mem=2GB
%chk=mol_046_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.27720        0.60830        0.17960
C          1.21760        1.48660        0.07790
C         -0.08480        0.99470       -0.02160
C         -0.34380       -0.35140       -0.02110
C          0.70400       -1.24020        0.07990
C          2.00510       -0.74290        0.17920
C         -1.71400       -0.85310       -0.12600
O         -1.95030       -2.08070       -0.12540
N         -2.81390        0.02860       -0.23120
H          3.30590        0.95670        0.25890
H          1.41680        2.55020        0.07700
H         -0.91680        1.67400       -0.10130
H          0.50300       -2.31030        0.08070
H          2.82490       -1.43920        0.25820
H         -2.87130        0.65950       -1.04740
H         -3.55930        0.05950        0.48270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_046_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

