%nproc=16
%mem=2GB
%chk=mol_496_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.65350       -0.21560        0.15220
C         -0.62000        0.02930       -0.06220
Cl        -1.08690        1.53860       -0.80880
Cl        -1.77520       -1.18770        0.42090
Cl         1.87820        0.98120       -0.31560
H          0.95040       -1.14590        0.61350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_496_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_496_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_496_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

