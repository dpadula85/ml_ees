%nproc=16
%mem=2GB
%chk=mol_496_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.60310       -0.39180        0.13620
C          0.56820        0.16020       -0.05500
Cl         2.08630       -0.68470        0.24550
Cl         0.65190        1.80880       -0.63390
Cl        -2.05260        0.51300       -0.18520
H         -0.65070       -1.40540        0.49230

--Link1--
%nproc=16
%mem=2GB
%chk=mol_496_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_496_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_496_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

