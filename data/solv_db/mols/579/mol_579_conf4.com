%nproc=16
%mem=2GB
%chk=mol_579_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.32930       -0.99310        0.22590
C         -0.96920       -0.32390       -0.24950
C         -0.67600        1.08840        0.24000
O          0.68210        1.23800        0.28630
C          1.29110        0.11980       -0.21970
H          0.52030       -1.93550       -0.27610
H          0.29200       -1.04430        1.33210
H         -0.94780       -0.34260       -1.35020
H         -1.84980       -0.76510        0.21050
H         -1.20270        1.85070       -0.33620
H         -1.04740        1.11360        1.30430
H          2.29110       -0.07530        0.15910
H          1.28710        0.06930       -1.32650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

