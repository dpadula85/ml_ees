%nproc=16
%mem=2GB
%chk=mol_579_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.05670        0.12550       -0.05580
C         -0.16840        1.04680       -0.05850
C         -1.17060        0.09420        0.57190
O         -0.84580       -1.17350        0.17830
C          0.35310       -1.17130       -0.47380
H          1.82130        0.43620       -0.75520
H          1.37500       -0.01960        0.98350
H         -0.01500        1.94410        0.54150
H         -0.39420        1.26090       -1.11340
H         -0.95280        0.16160        1.68020
H         -2.21220        0.39110        0.38180
H          0.16260       -1.04550       -1.57860
H          0.99040       -2.05050       -0.30200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

