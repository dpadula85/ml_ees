%nproc=16
%mem=2GB
%chk=mol_579_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.83870       -0.40020        0.47440
C          0.23030       -0.96320       -0.47420
C          1.29620        0.08500       -0.15720
O          0.56830        1.25470       -0.37910
C         -0.69870        1.06430        0.10690
H         -1.82390       -0.81270        0.27820
H         -0.52760       -0.53280        1.52870
H          0.52320       -1.96860       -0.23380
H         -0.12190       -0.76470       -1.48450
H          2.14310       -0.01240       -0.83710
H          1.56620        0.07270        0.91810
H         -0.89900        1.73230        0.97970
H         -1.41750        1.24550       -0.72030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

