%nproc=16
%mem=2GB
%chk=mol_579_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.49850       -0.95600        0.23810
C         -0.67830       -0.49880       -0.63800
C         -0.95280        0.86340       -0.05590
O          0.03080        1.12910        0.87630
C          1.15610        0.38270        0.47190
H          1.12500       -1.68900       -0.25330
H          0.03310       -1.29410        1.18940
H         -0.26960       -0.41150       -1.66620
H         -1.47920       -1.21680       -0.51790
H         -1.92620        0.85700        0.48830
H         -0.91530        1.63980       -0.84370
H          1.92820        0.38310        1.22420
H          1.44970        0.81100       -0.51320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

