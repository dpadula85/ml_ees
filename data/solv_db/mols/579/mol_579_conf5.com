%nproc=16
%mem=2GB
%chk=mol_579_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.55050        0.89210       -0.02840
C          0.94870        0.56170        0.03390
C          0.87160       -0.90700       -0.39590
O         -0.20090       -1.38990        0.31550
C         -1.14220       -0.41180        0.44820
H         -0.81110        1.75230        0.58630
H         -0.80680        0.98910       -1.10110
H          1.51780        1.17380       -0.66170
H          1.29460        0.61800        1.09060
H          0.62410       -0.89270       -1.49590
H          1.79140       -1.45310       -0.18550
H         -1.47070       -0.24230        1.50080
H         -2.06590       -0.69020       -0.10690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_579_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

