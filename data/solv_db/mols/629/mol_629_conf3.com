%nproc=16
%mem=2GB
%chk=mol_629_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.80440       -0.30940       -0.24900
C         -0.85040        0.84900       -0.12010
C          0.23730        0.59520        0.88620
C          1.07790       -0.60970        0.52880
N          1.71840       -0.45390       -0.74190
H         -1.63390       -1.06610        0.55520
H         -1.74430       -0.80990       -1.22940
H         -2.87180        0.01250       -0.12950
H         -0.40520        1.09520       -1.10810
H         -1.42770        1.72730        0.23420
H         -0.18970        0.49360        1.89010
H          0.90930        1.47880        0.87280
H          1.85950       -0.69680        1.31370
H          0.50680       -1.54430        0.51650
H          2.39520       -1.24030       -0.87590
H          2.22310        0.47890       -0.82370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

