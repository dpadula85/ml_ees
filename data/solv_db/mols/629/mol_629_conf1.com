%nproc=16
%mem=2GB
%chk=mol_629_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.18030       -0.54320        0.13420
C          0.83320        0.09230        0.33670
C         -0.08380       -0.06860       -0.83850
C         -1.39320        0.61520       -0.49560
N         -2.01000        0.02870        0.68060
H          2.21240       -1.28720       -0.66540
H          2.47800       -1.04410        1.08230
H          2.96820        0.23750       -0.03870
H          0.99490        1.16900        0.54190
H          0.32160       -0.33880        1.22390
H         -0.32950       -1.14690       -0.98790
H          0.29370        0.34390       -1.77440
H         -2.08460        0.65160       -1.36500
H         -1.21240        1.69680       -0.22820
H         -2.27300       -0.97330        0.53090
H         -2.89600        0.56720        0.84020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

