%nproc=16
%mem=2GB
%chk=mol_629_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.85870       -0.21260       -0.41640
C         -1.06020       -0.01260        0.85470
C          0.30350        0.59160        0.54750
C          1.03040       -0.35210       -0.36000
N          2.32870        0.16020       -0.68960
H         -2.92360       -0.37140       -0.07960
H         -1.55180       -1.12050       -0.97090
H         -1.79560        0.66880       -1.08510
H         -0.99130       -0.98360        1.36080
H         -1.64310        0.67180        1.51880
H          0.83820        0.75210        1.49620
H          0.09920        1.57260        0.05090
H          0.39000       -0.53800       -1.24400
H          1.17220       -1.30540        0.18810
H          2.58800        1.02990       -0.20250
H          3.07390       -0.55110       -0.75270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

