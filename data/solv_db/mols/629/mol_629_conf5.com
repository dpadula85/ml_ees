%nproc=16
%mem=2GB
%chk=mol_629_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.87230       -0.02940       -0.77690
C          1.05310       -0.63760        0.34240
C         -0.28070        0.07410        0.39400
C         -1.05680       -0.05340       -0.89010
N         -2.30370        0.66380       -0.71570
H          1.41160       -0.25920       -1.74350
H          1.88310        1.07500       -0.57990
H          2.91360       -0.42460       -0.77550
H          1.57910       -0.37390        1.29560
H          0.96370       -1.71660        0.24540
H         -0.90660       -0.28740        1.23360
H         -0.11200        1.15650        0.56700
H         -1.22180       -1.10130       -1.19060
H         -0.50920        0.42830       -1.73150
H         -3.07960        0.02620       -0.39300
H         -2.20610        1.45940       -0.05660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

