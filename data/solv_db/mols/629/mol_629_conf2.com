%nproc=16
%mem=2GB
%chk=mol_629_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.29250        0.38980        0.20140
C         -1.08000       -0.34180       -0.34190
C          0.15730        0.30890        0.20290
C          1.38210       -0.42500       -0.34190
N          2.60440        0.14250        0.13700
H         -3.11320        0.24530       -0.54020
H         -2.08230        1.47820        0.35970
H         -2.61430       -0.06710        1.14910
H         -1.10360       -1.40160        0.03630
H         -1.04660       -0.35910       -1.43060
H          0.13040        0.27020        1.29740
H          0.18480        1.36880       -0.14580
H          1.30130       -0.43430       -1.44930
H          1.27240       -1.47920       -0.01830
H          3.03000        0.85690       -0.46340
H          3.26980       -0.55240        0.53890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_629_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

