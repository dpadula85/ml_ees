%nproc=16
%mem=2GB
%chk=mol_237_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.88510        0.11490        0.09250
N         -0.42570        0.03490        0.01630
C          0.42670        1.07850        0.11340
C          1.72620        0.57830       -0.01020
C          1.63930       -0.78140       -0.18290
C          0.29400       -1.07890       -0.16080
H         -2.17500        1.04200        0.62720
H         -2.28540       -0.73260        0.71150
H         -2.33250        0.03710       -0.91270
H          0.15380        2.12140        0.26120
H          2.62140        1.15870        0.02470
H          2.42510       -1.50850       -0.31280
H         -0.18290       -2.06440       -0.26740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

