%nproc=16
%mem=2GB
%chk=mol_237_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.86670       -0.03270       -0.06200
N         -0.41890       -0.00360       -0.02670
C          0.33650       -1.09630        0.10370
C          1.64030       -0.65840        0.09460
C          1.64870        0.71390       -0.04340
C          0.33220        1.10920       -0.11860
H         -2.27510        0.99100       -0.25920
H         -2.18650       -0.32880        0.97810
H         -2.27730       -0.79830       -0.72860
H          0.01260       -2.13380        0.19950
H          2.54810       -1.26820        0.18060
H          2.52440        1.36640       -0.08580
H         -0.01830        2.13940       -0.23200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

