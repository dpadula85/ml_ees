%nproc=16
%mem=2GB
%chk=mol_237_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.91020       -0.01120        0.01940
N          0.44620        0.00330       -0.00340
C         -0.35710       -1.06620       -0.06200
C         -1.68380       -0.69890       -0.06740
C         -1.68220        0.68810       -0.00790
C         -0.36060        1.08420        0.03010
H          2.20050        0.83950        0.67770
H          2.30460       -0.92420        0.50940
H          2.35240        0.07510       -0.97230
H         -0.00780       -2.08880       -0.10030
H         -2.57810       -1.30980       -0.10790
H         -2.56310        1.31500        0.00500
H          0.01870        2.09390        0.07950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

