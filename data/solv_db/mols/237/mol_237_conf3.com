%nproc=16
%mem=2GB
%chk=mol_237_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.84850       -0.03650       -0.06740
N         -0.40020        0.00130       -0.01510
C          0.33750        1.10210        0.17120
C          1.65600        0.69090        0.15320
C          1.66610       -0.66640       -0.04680
C          0.36660       -1.09120       -0.15150
H         -2.27910        0.98050        0.13360
H         -2.26470       -0.71550        0.71510
H         -2.22620       -0.36320       -1.05870
H         -0.04740        2.12030        0.30900
H          2.48380        1.38610        0.28260
H          2.53840       -1.28060       -0.10930
H          0.01750       -2.12780       -0.31590

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

