%nproc=16
%mem=2GB
%chk=mol_237_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.87920        0.09800        0.00910
N         -0.42720        0.02660        0.00110
C          0.28950       -1.10230       -0.14410
C          1.62690       -0.76380       -0.09970
C          1.66730        0.59390        0.07550
C          0.37240        1.09470        0.13930
H         -2.18410        0.83150        0.80410
H         -2.29020       -0.90550        0.25380
H         -2.19100        0.41810       -0.99610
H         -0.09730       -2.11030       -0.27430
H          2.42470       -1.49270       -0.19330
H          2.57710        1.16640        0.14970
H          0.11130        2.14550        0.27470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_237_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

