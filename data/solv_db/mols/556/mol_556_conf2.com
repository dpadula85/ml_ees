%nproc=16
%mem=2GB
%chk=mol_556_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.52260        1.11270       -0.23650
C         -1.06230       -0.26510        0.18920
C         -2.29560       -1.17020        0.01130
C         -0.00610       -0.82000       -0.70430
O          1.17210       -0.08490       -0.80290
C          1.97070        0.14740        0.29570
O          1.61610       -0.32390        1.40380
C          3.24500        0.94760        0.18690
H         -2.25150        1.46910        0.51830
H         -2.08810        1.01520       -1.20080
H         -0.69980        1.82710       -0.33660
H         -0.79050       -0.24340        1.25920
H         -3.18270       -0.63480        0.43810
H         -2.42360       -1.32200       -1.08050
H         -2.12910       -2.15130        0.46950
H          0.18250       -1.87230       -0.34910
H         -0.47100       -0.91070       -1.72600
H          3.09630        1.98010        0.49960
H          3.70060        0.82790       -0.80790
H          3.93970        0.47180        0.92560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

