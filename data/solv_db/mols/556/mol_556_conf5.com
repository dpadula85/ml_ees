%nproc=16
%mem=2GB
%chk=mol_556_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.40850       -0.91740       -0.24450
C         -1.11310       -0.10940       -0.17400
C         -1.39870        1.17790        0.55440
C         -0.07230       -0.94140        0.48560
O          1.18190       -0.37200        0.64340
C          1.96880        0.04360       -0.39300
O          1.57200       -0.08070       -1.57610
C          3.31470        0.65740       -0.22590
H         -2.38630       -1.74520        0.48050
H         -3.26760       -0.23490       -0.10900
H         -2.52550       -1.37280       -1.25750
H         -0.82100        0.10860       -1.20760
H         -0.95180        2.04040        0.00460
H         -2.49570        1.35560        0.58720
H         -1.06480        1.14790        1.62190
H         -0.45620       -1.18710        1.51510
H         -0.04050       -1.92030       -0.07200
H          3.41770        1.47060       -0.97080
H          3.48560        1.00980        0.81120
H          4.06130       -0.13070       -0.47330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

