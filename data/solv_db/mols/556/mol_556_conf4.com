%nproc=16
%mem=2GB
%chk=mol_556_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.75160       -1.03080        0.36340
C         -1.34930        0.40750        0.53930
C         -2.35830        1.35760       -0.10370
C         -0.02870        0.63520       -0.20160
O          0.95010       -0.22480        0.35340
C          2.22680       -0.20240       -0.15050
O          2.48170        0.59110       -1.09800
C          3.32620       -1.06350        0.36040
H         -0.84710       -1.66100        0.14640
H         -2.18030       -1.47030        1.30210
H         -2.47630       -1.18810       -0.45150
H         -1.25870        0.71370        1.58700
H         -3.32220        1.34980        0.42670
H         -2.45490        1.14890       -1.19410
H         -1.89480        2.38010       -0.02080
H         -0.13200        0.45000       -1.27020
H          0.33330        1.67230       -0.05160
H          2.92900       -2.09770        0.54940
H          4.12540       -1.13770       -0.40460
H          3.68160       -0.62990        1.32040

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

