%nproc=16
%mem=2GB
%chk=mol_556_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.49050        1.32200        0.34180
C         -1.34240       -0.15800        0.54670
C         -2.54450       -0.93630        0.08100
C         -0.16100       -0.62240       -0.28380
O          0.96960        0.07860        0.17930
C          2.17960       -0.16210       -0.41320
O          2.25000       -0.99260       -1.34440
C          3.44140        0.51750       -0.00920
H         -0.53250        1.79500        0.63340
H         -2.33700        1.75790        0.88390
H         -1.57570        1.48970       -0.76600
H         -1.19750       -0.34640        1.60990
H         -3.41940       -0.83180        0.75280
H         -2.25030       -2.02340        0.13270
H         -2.81830       -0.75340       -0.96460
H         -0.00840       -1.72660       -0.18570
H         -0.36450       -0.44220       -1.35390
H          3.75350        1.13680       -0.88640
H          4.17740       -0.29270        0.21130
H          3.27040        1.19040        0.83440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

