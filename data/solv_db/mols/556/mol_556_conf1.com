%nproc=16
%mem=2GB
%chk=mol_556_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.86410       -0.74090       -1.03380
C         -1.53540       -0.38810        0.37750
C         -1.82770        1.08920        0.64710
C         -0.11570       -0.69660        0.76830
O          0.80660        0.02570       -0.05680
C          2.15580       -0.11170        0.13250
O          2.54170       -0.88970        1.05030
C          3.19330        0.59030       -0.66630
H         -1.02420       -1.05130       -1.64780
H         -2.35470        0.14190       -1.53560
H         -2.64160       -1.54060       -1.05790
H         -2.19140       -0.94960        1.07780
H         -1.42700        1.72340       -0.17730
H         -2.93710        1.17380        0.68040
H         -1.35670        1.37220        1.58830
H          0.10170       -1.75700        0.63510
H          0.02230       -0.34000        1.81020
H          3.69200        1.40310       -0.07050
H          3.97770       -0.12930       -0.96130
H          2.78430        1.07520       -1.56010

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_556_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

