%nproc=16
%mem=2GB
%chk=mol_030_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.36850        0.67580       -0.20110
C          2.91510        0.41930       -0.61390
C          2.23080       -0.44870        0.40130
C          0.82080       -0.74270       -0.00990
C          0.01040        0.49270       -0.13830
C         -1.40840        0.20270       -0.52280
C         -2.07130       -0.63750        0.53470
C         -3.47190       -0.84880        0.08550
O         -3.77320       -1.89310       -0.41690
C         -4.46170        0.23130        0.24190
H          4.97750        0.65110       -1.13570
H          4.40530        1.71730        0.17140
H          4.68570       -0.06020        0.55050
H          2.36890        1.37170       -0.69480
H          2.91530       -0.14060       -1.58150
H          2.81690       -1.40250        0.54900
H          2.26150        0.04860        1.41310
H          0.34540       -1.49260        0.66530
H          0.79460       -1.25340       -1.01960
H          0.46260        1.21590       -0.87350
H          0.07750        1.03130        0.83730
H         -1.92830        1.20200       -0.61310
H         -1.51210       -0.25110       -1.52130
H         -2.12990       -0.13100        1.51990
H         -1.57020       -1.62900        0.65370
H         -4.04440        1.10360        0.81420
H         -5.33950       -0.11610        0.79340
H         -4.74560        0.68370       -0.75060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

