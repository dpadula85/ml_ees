%nproc=16
%mem=2GB
%chk=mol_030_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.49910       -0.43320       -0.32320
C          3.16200       -0.79470        0.28300
C          2.13150        0.13750       -0.30930
C          0.74120       -0.14510        0.23940
C         -0.17870        0.84940       -0.43130
C         -1.61010        0.74410       -0.01920
C         -2.14880       -0.60710       -0.34660
C         -3.58620       -0.63470        0.08930
O         -4.19000       -1.68060       -0.10720
C         -4.23960        0.53250        0.72070
H          5.32410       -1.03790        0.06740
H          4.64040        0.66240       -0.15170
H          4.42280       -0.56380       -1.43720
H          2.94830       -1.84780        0.03260
H          3.23050       -0.62310        1.37430
H          2.38920        1.17370       -0.06880
H          2.08740       -0.00500       -1.39700
H          0.42960       -1.17930       -0.01170
H          0.69780       -0.05180        1.32880
H         -0.08690        0.72830       -1.53400
H          0.15190        1.89760       -0.18290
H         -1.70710        1.00130        1.04680
H         -2.15020        1.55990       -0.58150
H         -1.56600       -1.38980        0.13920
H         -2.15520       -0.81260       -1.43500
H         -3.85310        0.62400        1.76160
H         -4.04300        1.48970        0.17130
H         -5.34100        0.40620        0.69510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

