%nproc=16
%mem=2GB
%chk=mol_030_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.65120       -0.20780        1.14960
C         -3.42390        0.53570       -0.12290
C         -1.95240        0.58680       -0.50980
C         -1.39300       -0.79580       -0.70030
C          0.05770       -0.81450       -1.09760
C          0.89340       -0.15050       -0.03550
C          2.36470       -0.17720       -0.42350
C          3.20620        0.47240        0.60970
O          2.68340        0.93450        1.60180
C          4.68140        0.58270        0.48000
H         -4.25460       -1.10820        0.97350
H         -2.68930       -0.50240        1.65370
H         -4.17320        0.43170        1.90620
H         -3.99050        0.05910       -0.93650
H         -3.73610        1.59030        0.02300
H         -1.42960        1.07650        0.34530
H         -1.87600        1.15150       -1.44410
H         -1.55470       -1.41600        0.22070
H         -2.03780       -1.28780       -1.47290
H          0.19280       -0.30940       -2.08700
H          0.34830       -1.89110       -1.15820
H          0.53930        0.90910        0.06580
H          0.73150       -0.67220        0.92450
H          2.52550        0.35120       -1.37120
H          2.65400       -1.23170       -0.56850
H          5.05470        1.53890        0.94770
H          5.01240        0.58610       -0.57850
H          5.21700       -0.24180        0.99060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

