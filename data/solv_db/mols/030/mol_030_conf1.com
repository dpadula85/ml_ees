%nproc=16
%mem=2GB
%chk=mol_030_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.25200        0.41760        0.31830
C         -3.29330       -0.73400        0.32320
C         -1.86420       -0.32240        0.48610
C         -1.38060        0.58710       -0.60180
C          0.05930        0.97970       -0.40470
C          0.99660       -0.18790       -0.39010
C          2.39980        0.32750       -0.18740
C          3.39140       -0.79400       -0.16290
O          3.06750       -1.96180       -0.29330
C          4.81850       -0.48670        0.02570
H         -4.28520        0.95320       -0.65140
H         -5.27490        0.00040        0.47730
H         -4.05610        1.08280        1.17780
H         -3.54980       -1.35430        1.23200
H         -3.46740       -1.40060       -0.53590
H         -1.24550       -1.25400        0.42940
H         -1.69230        0.13440        1.47220
H         -1.97210        1.52510       -0.64580
H         -1.46340        0.11130       -1.58910
H          0.20300        1.51470        0.58060
H          0.39710        1.70850       -1.16970
H          0.75550       -0.83120        0.47850
H          0.97430       -0.72390       -1.35110
H          2.45520        0.87090        0.78500
H          2.63050        1.06010       -0.98660
H          5.01920        0.51520        0.42700
H          5.31430       -1.22690        0.71090
H          5.31460       -0.51080       -0.98710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

