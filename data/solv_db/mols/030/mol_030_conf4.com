%nproc=16
%mem=2GB
%chk=mol_030_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.44610        0.62330        1.01620
C         -3.23750       -0.72160        0.36470
C         -2.21780       -0.61220       -0.72810
C         -0.86610       -0.13940       -0.22420
C          0.08430       -0.06370       -1.40170
C          1.45890        0.39610       -1.02770
C          2.12720       -0.50800       -0.02770
C          3.47740       -0.06120        0.33790
O          4.41860       -0.28540       -0.40230
C          3.73490        0.66960        1.60660
H         -2.90990        1.44560        0.48260
H         -3.11210        0.62540        2.08420
H         -4.54550        0.87680        1.03650
H         -2.89590       -1.49320        1.09360
H         -4.18100       -1.08770       -0.12960
H         -2.59770        0.11300       -1.48160
H         -2.13160       -1.61170       -1.20220
H         -0.50730       -0.92600        0.47940
H         -0.97910        0.83630        0.28770
H         -0.34720        0.62170       -2.17470
H          0.16450       -1.10020       -1.81070
H          1.32720        1.41100       -0.55390
H          2.04630        0.51020       -1.93990
H          1.46860       -0.64380        0.85370
H          2.17050       -1.52960       -0.51030
H          3.71770       -0.01290        2.49920
H          4.75110        1.14080        1.59070
H          3.02760        1.52680        1.73130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_030_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

