%nproc=16
%mem=2GB
%chk=mol_391_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.68690        0.28320       -0.13610
C         -1.22160        0.11760       -0.06390
C         -0.40360        1.22130       -0.08570
C          0.97560        1.05880       -0.01710
C          1.54460       -0.19570        0.07300
C          0.71510       -1.29670        0.09420
C         -0.64330       -1.12070        0.02580
C          2.99110       -0.35460        0.14450
O          3.71740        0.64940        0.12380
H         -3.09160        0.61720        0.86180
H         -2.88810        1.11900       -0.83840
H         -3.17730       -0.65070       -0.41740
H         -0.79920        2.23900       -0.15560
H          1.64980        1.91290       -0.03230
H          1.18930       -2.28060        0.16650
H         -1.29910       -1.98120        0.04210
H          3.42790       -1.33810        0.21480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

