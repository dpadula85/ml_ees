%nproc=16
%mem=2GB
%chk=mol_391_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.69660       -0.28530        0.01500
C         -1.20130       -0.17430       -0.00390
C         -0.38310       -1.25570       -0.19120
C          0.97920       -1.05480       -0.19300
C          1.57520        0.18910       -0.01460
C          0.71210        1.26570        0.17310
C         -0.65510        1.08410        0.17800
C          3.01560        0.36040       -0.02300
O          3.78860       -0.60030       -0.19090
H         -3.11570        0.56360        0.63260
H         -3.01180       -1.21180        0.54600
H         -3.11300       -0.24070       -1.01070
H         -0.82590       -2.24570       -0.33390
H          1.63250       -1.89470       -0.33890
H          1.18100        2.23310        0.31190
H         -1.30640        1.92820        0.32440
H          3.42480        1.33920        0.11890

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

