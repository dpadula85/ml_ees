%nproc=16
%mem=2GB
%chk=mol_391_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70840        0.03680        0.32850
C         -1.22650        0.02100        0.14830
C         -0.63170        0.69210       -0.90150
C          0.74510        0.66700       -1.05370
C          1.57290       -0.01410       -0.18410
C          0.97910       -0.68470        0.86500
C         -0.39750       -0.65860        1.01580
C          3.02590       -0.01420       -0.38290
O          3.80640       -0.61460        0.37690
H         -3.00570        0.70080        1.15280
H         -3.09530       -0.97700        0.58930
H         -3.19850        0.29040       -0.64210
H         -1.24000        1.24820       -1.62100
H          1.23100        1.18630       -1.86900
H          1.59570       -1.22830        1.56520
H         -0.88910       -1.17890        1.83380
H          3.43670        0.52760       -1.22130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

