%nproc=16
%mem=2GB
%chk=mol_391_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70920       -0.25430        0.01730
C         -1.22800       -0.12260        0.00620
C         -0.59660        0.91180        0.66750
C          0.77670        1.04800        0.66710
C          1.58580        0.15130        0.00180
C          0.95300       -0.87980       -0.65720
C         -0.41740       -1.01720       -0.65790
C          3.04440        0.28650       -0.00550
O          3.75110       -0.54700       -0.62060
H         -3.10250        0.37190        0.86650
H         -3.05440       -1.29190        0.10040
H         -3.15280        0.18470       -0.92360
H         -1.25590        1.61710        1.19240
H          1.24020        1.87910        1.20170
H          1.56600       -1.60680       -1.19380
H         -0.92440       -1.83090       -1.17760
H          3.52410        1.10010        0.51530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

