%nproc=16
%mem=2GB
%chk=mol_391_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.71930        0.09580       -0.04330
C          1.22510        0.03330        0.00120
C          0.55820       -0.63100        1.00960
C         -0.83240       -0.66350        1.01060
C         -1.59090       -0.05360        0.03600
C         -0.91340        0.60690       -0.96610
C          0.47270        0.64760       -0.98000
C         -3.05860       -0.10920        0.07110
O         -3.73900        0.44830       -0.81940
H          3.08710       -0.01670       -1.10050
H          3.18360       -0.73520        0.51630
H          3.11550        1.04720        0.32610
H          1.14250       -1.12050        1.79070
H         -1.33540       -1.19030        1.81110
H         -1.46830        1.09750       -1.74820
H          1.02120        1.17000       -1.77210
H         -3.58710       -0.62660        0.85700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_391_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

