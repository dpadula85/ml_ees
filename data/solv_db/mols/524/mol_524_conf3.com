%nproc=16
%mem=2GB
%chk=mol_524_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.08910        0.11620       -0.31530
C          0.11260       -0.21330        0.51180
O         -0.04900       -0.68720        1.66820
N          1.41010        0.00570       -0.00090
H         -1.72960       -0.78910       -0.35610
H         -1.63000        0.96970        0.18180
H         -0.75430        0.42540       -1.33060
H          1.81180        0.95240        0.08140
H          1.91750       -0.77980       -0.44030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

