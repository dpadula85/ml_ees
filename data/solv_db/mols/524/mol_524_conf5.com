%nproc=16
%mem=2GB
%chk=mol_524_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12290        0.04200       -0.09120
C          0.22760       -0.49590        0.12750
O          0.37360       -1.68010        0.49240
N          1.36490        0.31800       -0.07230
H         -1.46960        0.68570        0.76020
H         -1.09420        0.62590       -1.04530
H         -1.89260       -0.76100       -0.17370
H          1.47160        1.24260        0.40250
H          2.14170        0.02270       -0.70390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

