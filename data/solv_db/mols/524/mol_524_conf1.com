%nproc=16
%mem=2GB
%chk=mol_524_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.08790       -0.29650        0.15750
C         -0.13580        0.51760        0.10300
O         -0.08610        1.72550        0.40700
N         -1.39840       -0.01790       -0.28770
H          1.95240        0.36300       -0.03100
H          1.16250       -0.75000        1.15610
H          1.10090       -1.07350       -0.65480
H         -1.48610       -1.04100       -0.32070
H         -2.19720        0.57270       -0.52940

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

