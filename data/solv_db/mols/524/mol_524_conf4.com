%nproc=16
%mem=2GB
%chk=mol_524_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.13230       -0.01040       -0.26710
C          0.16710       -0.21180        0.45230
O          0.07900       -0.77680        1.58100
N          1.41010        0.18450       -0.05770
H         -1.51410       -1.01630       -0.53850
H         -1.81600        0.48270        0.45710
H         -0.98870        0.65270       -1.14840
H          2.05180        0.74700        0.53400
H          1.74320       -0.05170       -1.01270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

