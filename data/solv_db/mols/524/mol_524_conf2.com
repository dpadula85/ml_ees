%nproc=16
%mem=2GB
%chk=mol_524_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.14100       -0.03800       -0.05210
C          0.23180       -0.28750        0.46030
O          0.32140       -1.12180        1.41690
N          1.38720        0.34290       -0.05660
H         -1.56500       -0.98320       -0.48270
H         -1.80090        0.28420        0.76560
H         -1.12140        0.68890       -0.90160
H          2.09590       -0.24770       -0.54010
H          1.59190        1.36210        0.00630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_524_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

