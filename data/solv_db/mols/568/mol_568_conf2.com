%nproc=16
%mem=2GB
%chk=mol_568_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.94120       -0.66810        0.79340
C         -0.31730        0.36340       -0.07060
C         -1.06020        1.41790       -0.35920
C          1.03890        0.23770       -0.58230
C          1.78800       -0.80580       -0.30280
H         -0.55500       -1.67430        0.63840
H         -2.04950       -0.71110        0.57730
H         -0.88040       -0.40890        1.87540
H         -0.63380        2.18910       -0.98770
H         -2.06910        1.53840        0.00510
H          1.42830        1.02680       -1.20570
H          2.79310       -0.87760       -0.69430
H          1.45840       -1.62740        0.31300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

