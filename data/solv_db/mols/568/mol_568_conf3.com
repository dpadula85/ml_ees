%nproc=16
%mem=2GB
%chk=mol_568_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.15610       -0.16760        0.75790
C         -0.17820        0.46340       -0.17650
C         -0.43890        1.64250       -0.74450
C          1.06450       -0.22320       -0.46160
C          1.30770       -1.38760        0.10730
H         -1.57270       -1.07320        0.26040
H         -1.96750        0.57390        0.98790
H         -0.63510       -0.50830        1.68150
H          0.28730        2.07540       -1.41670
H         -1.36840        2.15090       -0.52890
H          1.76160        0.23090       -1.13070
H          2.24730       -1.89430       -0.11580
H          0.64850       -1.88270        0.77970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

