%nproc=16
%mem=2GB
%chk=mol_568_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.69550        1.09140        0.33750
C         -0.44120       -0.30880       -0.15460
C         -1.42420       -1.19140       -0.23770
C          0.91090       -0.66220       -0.53470
C          1.86770        0.24070       -0.44000
H         -0.27190        1.82720       -0.38990
H         -0.23640        1.25700        1.33310
H         -1.79660        1.30420        0.34800
H         -2.44860       -0.99320        0.02820
H         -1.17910       -2.17550       -0.59660
H          1.15540       -1.65250       -0.89560
H          1.66000        1.24190       -0.08290
H          2.89960        0.02130       -0.71400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

