%nproc=16
%mem=2GB
%chk=mol_568_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.07930       -0.69940        0.57750
C         -0.34520        0.34040       -0.19110
C         -1.03030        1.29500       -0.76350
C          1.11130        0.29570       -0.30410
C          1.82910       -0.65210        0.26050
H         -0.66040       -1.70680        0.31950
H         -2.12970       -0.71260        0.30540
H         -0.96350       -0.46250        1.64390
H         -0.51880        2.05110       -1.32040
H         -2.09170        1.32150       -0.67740
H          1.63610        1.05240       -0.86250
H          1.34050       -1.42770        0.82820
H          2.90190       -0.69500        0.18420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

