%nproc=16
%mem=2GB
%chk=mol_568_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.84020       -0.01370       -1.09540
C         -0.39260       -0.09820        0.30410
C         -1.26900       -0.35600        1.23920
C          1.00050        0.10140        0.64680
C          1.91490        0.36580       -0.28910
H         -1.95400        0.14400       -1.16840
H         -0.35780        0.81500       -1.66200
H         -0.67890       -1.00230       -1.61430
H         -0.94080       -0.41780        2.26410
H         -2.31280       -0.50820        1.00750
H          1.27200        0.02760        1.69590
H          2.94250        0.51000       -0.00980
H          1.61610        0.43250       -1.31850

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_568_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

