%nproc=16
%mem=2GB
%chk=mol_247_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.47150       -0.25250       -0.02270
C         -0.97840        1.04660       -0.03070
C          0.37730        1.28810       -0.00990
C          1.22980        0.20360        0.01900
C          0.72520       -1.08310        0.02660
C         -0.61560       -1.34120        0.00630
Cl         1.85770       -2.41730        0.06340
Cl         2.97190        0.48770        0.04620
Cl         1.02140        2.93540       -0.01940
H         -2.53150       -0.42000       -0.03930
H         -1.63450        1.91730       -0.05330
H         -0.95170       -2.36470        0.01370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_247_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_247_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_247_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

