%nproc=16
%mem=2GB
%chk=mol_069_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79070        0.23950       -0.02940
C          0.69390       -0.03970       -0.06510
Cl         1.64370        1.46990       -0.13110
Cl         1.09280       -0.82770        1.50240
Cl         1.16690       -1.14200       -1.33360
Cl        -1.62360       -1.32610        0.03450
H         -1.13470        0.84750       -0.88180
H         -1.04840        0.77860        0.90410

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

