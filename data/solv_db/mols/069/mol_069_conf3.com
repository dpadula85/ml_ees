%nproc=16
%mem=2GB
%chk=mol_069_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.82360        0.04910       -0.00370
C          0.68370       -0.09960       -0.07370
Cl         1.17270       -0.76540       -1.64970
Cl         1.26220       -1.19100        1.19310
Cl         1.50400        1.46700        0.10330
Cl        -1.39050        0.69690        1.51200
H         -1.10910        0.75590       -0.82620
H         -1.29940       -0.91300       -0.25520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

