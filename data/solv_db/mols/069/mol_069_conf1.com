%nproc=16
%mem=2GB
%chk=mol_069_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.79100       -0.02720       -0.14200
C          0.71050       -0.09120        0.04070
Cl         1.27630       -1.60200       -0.68700
Cl         1.52310        1.27040       -0.73910
Cl         1.07420       -0.01520        1.78100
Cl        -1.47470        1.46110        0.55650
H         -1.06840       -0.12540       -1.20470
H         -1.25000       -0.87050        0.39470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

