%nproc=16
%mem=2GB
%chk=mol_069_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.75810        0.29370        0.06620
C          0.70800       -0.04850       -0.12190
Cl         0.81280       -1.22720       -1.45160
Cl         1.70290        1.37420       -0.42790
Cl         1.21380       -0.90470        1.37040
Cl        -1.60980       -1.21930        0.46640
H         -1.20360        0.76110       -0.83410
H         -0.86600        0.97070        0.93250

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_069_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

