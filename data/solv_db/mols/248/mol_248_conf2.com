%nproc=16
%mem=2GB
%chk=mol_248_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.87230       -0.14410        0.14750
C          0.50440        0.31850       -0.22170
O          0.30070        1.29670       -0.94800
O         -0.55850       -0.39410        0.28280
C         -1.89990       -0.09900        0.03770
H          2.09840       -0.99370       -0.55090
H          2.63110        0.65070        0.05840
H          1.92470       -0.53560        1.16860
H         -2.34820        0.22200        1.01890
H         -2.47520       -0.99860       -0.26070
H         -2.05000        0.67730       -0.73270

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

