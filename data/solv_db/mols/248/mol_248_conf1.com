%nproc=16
%mem=2GB
%chk=mol_248_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.88590        0.00600       -0.06450
C          0.41750       -0.30410       -0.17440
O          0.08040       -1.50010       -0.25060
O         -0.53460        0.70460       -0.19090
C         -1.88750        0.29610       -0.29770
H          2.10410        1.05760       -0.28960
H          2.22660       -0.22640        0.96660
H          2.37430       -0.63610       -0.83220
H         -2.25260       -0.16530        0.66710
H         -1.94630       -0.43170       -1.14050
H         -2.46790        1.19930       -0.56060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

