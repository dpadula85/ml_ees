%nproc=16
%mem=2GB
%chk=mol_248_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.89690       -0.08550        0.00280
C         -0.48310        0.34190        0.18370
O         -0.20810        1.41620        0.75430
O          0.53260       -0.44710       -0.27830
C          1.90620       -0.15040       -0.16640
H         -2.53740        0.78540       -0.21140
H         -1.96590       -0.87560       -0.79190
H         -2.23350       -0.60110        0.93770
H          2.13630        0.92340       -0.31220
H          2.42110       -0.79780       -0.93370
H          2.32850       -0.50930        0.81540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

