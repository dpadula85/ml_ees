%nproc=16
%mem=2GB
%chk=mol_248_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.90660        0.22870        0.18780
C          0.51450       -0.31340        0.03540
O          0.28010       -1.53740       -0.06660
O         -0.58420        0.53010        0.00090
C         -1.89380        0.07020       -0.13910
H          2.08570        0.90780       -0.68900
H          1.93790        0.80340        1.13690
H          2.61790       -0.59190        0.13160
H         -2.19060       -0.69790        0.60280
H         -2.10990       -0.37010       -1.13670
H         -2.56430        0.97070       -0.06390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

