%nproc=16
%mem=2GB
%chk=mol_248_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.89420       -0.05260       -0.21750
C         -0.42680       -0.09450       -0.01880
O         -0.00260       -0.45060        1.10260
O          0.49160        0.23430       -0.99240
C          1.86380        0.14770       -0.66010
H         -2.18620        0.53850       -1.08420
H         -2.21730       -1.12580       -0.34530
H         -2.32560        0.36840        0.72160
H          2.19940        1.08250       -0.12340
H          2.51380        0.04760       -1.55170
H          1.98400       -0.69550        0.05120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_248_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

