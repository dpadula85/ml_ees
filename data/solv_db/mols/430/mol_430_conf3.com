%nproc=16
%mem=2GB
%chk=mol_430_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99560        1.33770        0.44540
O          2.41130        0.52130       -0.53570
C          1.29290       -0.25420       -0.35920
C          1.36040       -1.56100        0.07630
C          0.20400       -2.32700        0.24690
C         -1.02920       -1.73910       -0.03430
C         -1.11580       -0.42160       -0.47470
C          0.05640        0.29240       -0.62700
O         -0.00820        1.60680       -1.06490
O         -2.35080        0.14960       -0.75050
C         -3.13860        0.83530        0.17300
Cl        -2.49620       -2.66480        0.16720
Cl         2.95350       -2.22780        0.40930
H          2.58490        2.36760        0.36930
H          4.08690        1.42540        0.17730
H          2.93390        0.91420        1.46190
H          0.27300       -3.35710        0.59120
H         -0.87240        2.06610       -1.27750
H         -4.15460        0.94630       -0.25510
H         -2.75680        1.83930        0.40840
H         -3.23010        0.25040        1.10990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

