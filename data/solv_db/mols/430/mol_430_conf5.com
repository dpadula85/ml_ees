%nproc=16
%mem=2GB
%chk=mol_430_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99040        1.08570        0.40880
O          2.39110        0.18270       -0.47250
C          1.18000       -0.44410       -0.27580
C          1.12710       -1.65940        0.36850
C         -0.10260       -2.24000        0.53830
C         -1.27180       -1.63400        0.08000
C         -1.21800       -0.41620       -0.56550
C          0.01870        0.17090       -0.73800
O          0.12300        1.39920       -1.38330
O         -2.31740        0.23640       -1.04060
C         -3.10900        1.15750       -0.36900
Cl        -2.82770       -2.41920        0.32170
Cl         2.59950       -2.44860        0.96000
H          2.85070        2.14620        0.10590
H          2.64810        0.98180        1.46230
H          4.10300        0.89990        0.36420
H         -0.14220       -3.21070        1.05320
H          0.98920        1.85750       -1.52940
H         -2.76700        1.43490        0.63480
H         -3.09960        2.11590       -0.96680
H         -4.16550        0.80380       -0.39990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

