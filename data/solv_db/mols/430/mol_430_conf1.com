%nproc=16
%mem=2GB
%chk=mol_430_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.09490        1.10550        0.09370
O         -2.48200        0.04940       -0.58440
C         -1.24840       -0.45450       -0.23500
C         -1.23090       -1.49550        0.69150
C          0.00010       -2.00320        1.04420
C          1.16800       -1.51940        0.51850
C          1.14360       -0.48740       -0.39940
C         -0.07650        0.03910       -0.76980
O         -0.14300        1.07940       -1.69130
O          2.33450       -0.00570       -0.92530
C          3.04830        1.06790       -0.29330
Cl         2.72170       -2.18600        0.98650
Cl        -2.72860       -2.12170        1.37150
H         -2.38040        1.53070        0.81500
H         -4.01620        0.79550        0.64420
H         -3.40250        1.90670       -0.60380
H          0.01270       -2.81100        1.76350
H          0.66110        1.48700       -2.11950
H          4.11580        0.79380       -0.14720
H          2.63180        1.22970        0.74080
H          2.96580        1.99950       -0.90030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

