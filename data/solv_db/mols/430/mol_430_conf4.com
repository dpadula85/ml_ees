%nproc=16
%mem=2GB
%chk=mol_430_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.89710       -0.00300        1.36930
O         -2.27220       -0.74770        0.36920
C         -1.15450       -0.37580       -0.33650
C         -1.25760        0.32970       -1.50910
C         -0.12460        0.71060       -2.23100
C          1.10190        0.34850       -1.71720
C          1.25420       -0.36320       -0.53790
C          0.07960       -0.71800        0.14340
O          0.15180       -1.42630        1.32290
O          2.48260       -0.72480       -0.02500
C          3.13400        0.14930        0.85630
Cl         2.56220        0.81220       -2.60280
Cl        -2.82320        0.77960       -2.14480
H         -3.24060       -0.72910        2.16030
H         -3.82270        0.50550        1.02350
H         -2.25960        0.73320        1.87130
H         -0.19990        1.26630       -3.15480
H         -0.68870       -1.68210        1.81380
H          2.63250        1.12200        0.97920
H          3.17610       -0.33420        1.84690
H          4.16580        0.34720        0.50300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

