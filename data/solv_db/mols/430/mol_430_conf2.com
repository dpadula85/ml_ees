%nproc=16
%mem=2GB
%chk=mol_430_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.00920       -1.12390       -0.42220
O          2.39530       -0.25790        0.50930
C          1.19130        0.38680        0.28010
C          1.19340        1.63430       -0.30970
C         -0.02250        2.24120       -0.52030
C         -1.21340        1.61130       -0.14770
C         -1.18380        0.37350        0.43560
C          0.02500       -0.24980        0.65430
O          0.00410       -1.50110        1.24710
O         -2.36450       -0.22990        0.79510
C         -3.13610       -1.07110       -0.00360
Cl        -2.68830        2.48550       -0.46820
Cl         2.70880        2.39970       -0.76660
H          2.28900       -1.24430       -1.26130
H          3.20330       -2.12960        0.01550
H          3.95760       -0.65150       -0.72470
H         -0.02730        3.23060       -0.98760
H          0.83050       -2.02660        1.44360
H         -4.19370       -0.69400        0.03880
H         -2.77570       -1.06540       -1.04400
H         -3.20230       -2.11780        0.40030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_430_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

