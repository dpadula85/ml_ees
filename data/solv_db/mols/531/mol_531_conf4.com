%nproc=16
%mem=2GB
%chk=mol_531_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.29810        0.74910        0.19260
C          3.00170        0.00850        0.23250
C          1.89820        0.62650        0.62870
C          0.58490       -0.03580        0.69380
C         -0.35790        0.72780       -0.24020
C         -1.72020        0.10790       -0.22580
C         -2.80180        0.78800        0.15130
C         -4.16430        0.16720        0.16560
O         -4.95710        0.92780       -0.71370
C         -1.89450       -1.29270       -0.63630
C          3.01110       -1.42190       -0.18470
H          4.06130        1.75920       -0.23660
H          4.72420        0.85790        1.21220
H          5.04690        0.25260       -0.42740
H          1.94900        1.66780        0.92140
H          0.13370        0.08250        1.71640
H          0.57620       -1.07340        0.35210
H         -0.37640        1.79320       -0.00410
H          0.00830        0.61970       -1.29360
H         -2.70520        1.81180        0.45570
H         -4.15580       -0.87520       -0.15390
H         -4.60850        0.21200        1.17670
H         -4.88160        1.88280       -0.55770
H         -2.18920       -1.87460        0.27060
H         -0.92990       -1.71310       -0.99040
H         -2.63650       -1.42830       -1.45090
H          2.68130       -1.45640       -1.24250
H          4.04050       -1.82510       -0.11900
H          2.36340       -2.04580        0.46780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

