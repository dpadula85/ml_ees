%nproc=16
%mem=2GB
%chk=mol_531_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.24740        1.76160       -0.22950
C         -2.29820        0.43490        0.45870
C         -1.72370       -0.61800       -0.07890
C         -0.98250       -0.52730       -1.34280
C          0.33170       -1.17960       -1.41310
C          1.43280       -0.77650       -0.55620
C          1.45160        0.18140        0.33110
C          2.65350        0.43230        1.14720
O          3.17020        1.71130        0.97350
C          2.74010       -1.54450       -0.69480
C         -3.01100        0.32750        1.76520
H         -1.23930        2.21680       -0.18090
H         -2.52930        1.67000       -1.28680
H         -2.98270        2.47350        0.21900
H         -1.80840       -1.56920        0.43390
H         -1.65740       -0.95220       -2.14970
H         -0.83560        0.57550       -1.55580
H          0.17650       -2.30380       -1.27690
H          0.65810       -1.13380       -2.51580
H          0.66620        0.91060        0.39380
H          2.33230        0.37550        2.21730
H          3.46920       -0.28110        0.96790
H          2.68800        2.22130        0.27410
H          2.97330       -2.10140        0.23100
H          3.57000       -0.87360       -0.96610
H          2.62800       -2.32050       -1.47830
H         -4.09030        0.07510        1.58460
H         -2.99340        1.26590        2.33710
H         -2.54200       -0.45140        2.39130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

