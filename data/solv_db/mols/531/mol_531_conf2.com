%nproc=16
%mem=2GB
%chk=mol_531_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.38260       -0.03070        0.22530
C         -2.91430        0.30950        0.15510
C         -2.00490       -0.49590        0.68290
C         -0.57880       -0.13130        0.59490
C          0.21240       -1.18070       -0.15200
C          1.64400       -0.73470       -0.19040
C          2.06390        0.39670        0.36170
C          3.50500        0.76680        0.27600
O          4.10420        0.90260        1.50870
C          2.68630       -1.58200       -0.87860
C         -2.53860        1.57150       -0.52260
H         -4.91140        0.26110       -0.69930
H         -4.45070       -1.12310        0.42280
H         -4.80570        0.53150        1.07630
H         -2.29850       -1.41130        1.17380
H         -0.48660        0.82270        0.05710
H         -0.17940       -0.08660        1.62940
H          0.10750       -2.17620        0.28000
H         -0.18850       -1.19730       -1.20420
H          1.35680        1.03830        0.87700
H          3.51810        1.79430       -0.19440
H          4.07190        0.12440       -0.39610
H          4.95020        1.39710        1.37490
H          2.19730       -2.52890       -1.19180
H          3.48610       -1.87230       -0.16760
H          3.07990       -1.09500       -1.77340
H         -3.43290        2.13460       -0.83510
H         -1.98260        2.19630        0.23660
H         -1.82830        1.39860       -1.36880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

