%nproc=16
%mem=2GB
%chk=mol_531_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.45880       -0.68890        0.27830
C          2.64830        0.44980       -0.22800
C          1.37900        0.54100        0.14120
C          0.80010       -0.46240        1.02730
C         -0.33710       -1.26190        0.48760
C         -1.49760       -0.42120        0.09990
C         -2.67470       -0.58400        0.68880
C         -3.81320        0.26830        0.30460
O         -4.89350       -0.45420       -0.19320
C         -1.46020        0.46800       -1.10680
C          3.29290        1.45930       -1.14430
H          3.59360       -0.62730        1.38100
H          4.46680       -0.70130       -0.22130
H          2.96390       -1.61970       -0.05350
H          0.85950        1.43540       -0.20620
H          1.61920       -1.13040        1.39720
H          0.48330        0.08870        1.96430
H         -0.68440       -1.94970        1.30070
H         -0.04800       -1.91130       -0.36780
H         -2.83810       -1.33250        1.45750
H         -3.57610        1.06250       -0.42650
H         -4.17040        0.83010        1.21530
H         -5.64320       -0.52840        0.43140
H         -1.47240        1.50890       -0.73450
H         -2.32200        0.27030       -1.78640
H         -0.53850        0.23700       -1.69720
H          2.64980        2.34300       -1.26690
H          4.23070        1.78260       -0.66410
H          3.52350        0.92830       -2.07840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

