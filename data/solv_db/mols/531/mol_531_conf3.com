%nproc=16
%mem=2GB
%chk=mol_531_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99380       -1.70540        0.51600
C          2.50880       -0.36820        0.10020
C          1.28910        0.01290        0.38150
C          0.78170        1.34070       -0.02180
C         -0.41750        1.24160       -0.93010
C         -1.55760        0.53160       -0.29020
C         -2.05030       -0.57910       -0.82320
C         -3.18840       -1.33590       -0.23300
O         -4.21860       -1.37950       -1.16200
C         -2.18340        1.03460        0.96060
C          3.42390        0.53170       -0.63820
H          4.06070       -1.72910        0.75170
H          2.81390       -2.43210       -0.31570
H          2.36730       -2.04880        1.37690
H          0.63370       -0.64920        0.91880
H          0.57990        1.92830        0.89330
H          1.58120        1.84440       -0.60190
H         -0.09180        0.66220       -1.82370
H         -0.78290        2.23480       -1.25720
H         -1.61150       -0.96020       -1.72870
H         -3.52070       -0.92670        0.73260
H         -2.88370       -2.41110       -0.06630
H         -4.52210       -0.47870       -1.43210
H         -3.25730        1.21660        0.76350
H         -2.11590        0.22500        1.73970
H         -1.70200        1.96920        1.31600
H          3.08310        0.64040       -1.67900
H          4.46280        0.10390       -0.67950
H          3.52350        1.48620       -0.09160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_531_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

