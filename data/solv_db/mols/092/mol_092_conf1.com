%nproc=16
%mem=2GB
%chk=mol_092_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.64840        0.11160        0.09730
C         -1.05050       -1.14190       -0.01110
C          0.32220       -1.19650       -0.15130
C          1.08880       -0.05460       -0.18450
C          0.51580        1.18740       -0.07910
C         -0.85850        1.25010        0.06160
O          2.46890       -0.19330       -0.32880
H         -2.74050        0.20180        0.21090
H         -1.65740       -2.03420        0.01570
H          0.81390       -2.17090       -0.23840
H          1.10440        2.10860       -0.10300
H         -1.34030        2.20360        0.14670
H          2.98160       -0.27150        0.56400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_092_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_092_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_092_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

