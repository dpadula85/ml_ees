%nproc=16
%mem=2GB
%chk=mol_092_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67020       -0.11390        0.10500
C         -1.04980        1.12330        0.14820
C          0.32050        1.20090        0.00140
C          1.11000        0.08280       -0.18920
C          0.46820       -1.13930       -0.22860
C         -0.91120       -1.24880       -0.08370
O          2.50290        0.17430       -0.33710
H         -2.73920       -0.15790        0.22100
H         -1.63860        2.03200        0.29720
H          0.82930        2.16480        0.03180
H          1.08700       -2.01780       -0.37800
H         -1.35850       -2.22020       -0.12170
H          3.04970        0.11970        0.53370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_092_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_092_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_092_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

