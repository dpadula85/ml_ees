%nproc=16
%mem=2GB
%chk=mol_260_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.15460        0.47080        0.00970
O          2.25720       -0.25940       -0.80570
C          0.89990       -0.23280       -0.50070
C          0.43470        0.49240        0.57810
C         -0.90940        0.52680        0.89110
C         -1.84930       -0.15850        0.14520
C         -1.37560       -0.88090       -0.93130
C         -0.03260       -0.92470       -1.25710
N         -3.22940       -0.10110        0.49670
H          4.19380        0.35870       -0.29680
H          2.84350        1.54990        0.02930
H          3.00030        0.10600        1.05930
H          1.18410        1.02930        1.15970
H         -1.24420        1.10560        1.74520
H         -2.12060       -1.41940       -1.51640
H          0.32580       -1.49460       -2.10330
H         -3.56220       -0.39220        1.45280
H         -3.97060        0.22420       -0.15580

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

