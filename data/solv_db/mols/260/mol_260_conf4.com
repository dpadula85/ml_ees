%nproc=16
%mem=2GB
%chk=mol_260_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.16330       -0.54550        0.04320
O          2.28740        0.45150        0.53210
C          0.90270        0.35370        0.31420
C          0.03970        1.31880        0.78590
C         -1.31430        1.19890        0.55850
C         -1.87240        0.13070       -0.13830
C         -0.99510       -0.82960       -0.60540
C          0.36180       -0.71060       -0.37820
N         -3.27040        0.05840       -0.34280
H          2.80600       -1.56900        0.33330
H          3.25150       -0.54320       -1.05540
H          4.16590       -0.35330        0.47390
H          0.43360        2.16710        1.33340
H         -2.02350        1.95230        0.92280
H         -1.40770       -1.66750       -1.14890
H          1.07230       -1.45810       -0.73860
H         -3.62150       -0.25980       -1.27050
H         -3.97920        0.30510        0.38080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

