%nproc=16
%mem=2GB
%chk=mol_260_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.22660       -0.25070        0.00130
O         -2.21260        0.63600        0.40130
C         -0.87240        0.42020        0.24200
C          0.03730        1.36080        0.67280
C          1.38640        1.12390        0.50280
C          1.88910       -0.02900       -0.08840
C          0.95440       -0.96110       -0.51400
C         -0.40230       -0.73290       -0.34800
N          3.28450       -0.23630       -0.24560
H         -2.87210       -1.27370        0.18610
H         -3.51060       -0.09940       -1.07400
H         -4.15410       -0.08180        0.59110
H         -0.31080        2.28110        1.14260
H          2.13350        1.86010        0.83870
H          1.33640       -1.86190       -0.97540
H         -1.05680       -1.51830       -0.71180
H          3.62150       -0.85990       -0.99940
H          3.97520        0.22300        0.37780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

