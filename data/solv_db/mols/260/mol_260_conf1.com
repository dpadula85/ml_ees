%nproc=16
%mem=2GB
%chk=mol_260_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.23600       -0.05300        0.03300
O         -2.18470       -0.10090       -0.88140
C         -0.84780       -0.05650       -0.59020
C         -0.40490        0.04540        0.70840
C          0.96750        0.08910        0.97950
C          1.88380        0.03000       -0.05890
C          1.41670       -0.07310       -1.36950
C          0.05830       -0.11560       -1.62650
N          3.27390        0.07490        0.22400
H         -2.93420        0.63250        0.87280
H         -3.50150       -1.06090        0.38110
H         -4.15580        0.44010       -0.39510
H         -1.08640        0.09360        1.54620
H          1.31410        0.16810        1.98610
H          2.12300       -0.11960       -2.18260
H         -0.24670       -0.19660       -2.67230
H          3.88210        0.61810       -0.42110
H          3.67850       -0.41550        1.04560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

