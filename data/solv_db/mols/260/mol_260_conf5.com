%nproc=16
%mem=2GB
%chk=mol_260_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.17690        0.15650       -0.02670
O          2.20200       -0.82460        0.15600
C          0.84740       -0.53040        0.07390
C          0.42250        0.74570       -0.19170
C         -0.90940        1.05150       -0.27580
C         -1.85040        0.06920       -0.09230
C         -1.44440       -1.21060        0.17380
C         -0.10080       -1.51500        0.25770
N         -3.21360        0.44750       -0.19140
H          3.31850        0.35650       -1.12160
H          2.94790        1.11630        0.48620
H          4.13420       -0.20430        0.44380
H          1.14110        1.56780       -0.34630
H         -1.23280        2.09430       -0.49260
H         -2.21510       -1.96450        0.31370
H          0.18720       -2.52600        0.46740
H         -3.70420        0.79440        0.64460
H         -3.70700        0.37550       -1.09170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_260_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

