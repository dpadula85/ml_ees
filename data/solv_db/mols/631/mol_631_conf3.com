%nproc=16
%mem=2GB
%chk=mol_631_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.13230        1.01080        0.21510
C         -0.97230        0.22440       -0.32330
C          0.34810        0.99990       -0.45290
C          1.25010        0.76780        0.77490
O          2.47290        0.43170        0.05050
N          3.21840       -0.68660        0.37870
O          4.42450       -0.71130        0.19350
O          2.56160       -1.94980        0.18280
O         -0.79590       -0.85480        0.70270
N         -1.30850       -2.10340        0.42540
O         -0.84890       -2.86790       -0.44730
O         -2.62840       -2.31940        0.78490
H         -2.86270        1.13330       -0.68630
H         -1.87350        2.03590        0.47550
H         -2.71370        0.52760        0.98790
H         -1.22050       -0.30480       -1.23100
H         -0.03130        2.07190       -0.50450
H          0.76410        0.76080       -1.40500
H          1.44090        1.78050        1.22530
H          0.90750        0.05340        1.44680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

