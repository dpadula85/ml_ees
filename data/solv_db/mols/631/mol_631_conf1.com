%nproc=16
%mem=2GB
%chk=mol_631_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.72780        1.34420       -0.63500
C         -0.93910        0.05250       -0.35180
C          0.49630        0.32670       -0.43460
C          1.29850       -0.90240        0.01420
O          2.38630       -0.37020        0.74290
N          3.68300       -0.49150        0.45300
O          4.40520       -1.42570        0.24590
O          4.31740        0.79690        0.43300
O         -1.29540       -0.28320        1.01060
N         -2.61440       -0.61420        1.18290
O         -3.37070        0.21120        1.74960
O         -3.19290       -1.52640        0.35780
H         -1.84240        1.90550        0.32070
H         -1.24600        1.95450       -1.41410
H         -2.74380        1.02590       -0.92500
H         -1.29470       -0.71530       -1.01910
H          0.81730        1.26940       -0.00720
H          0.69830        0.43290       -1.58000
H          0.61090       -1.47840        0.68390
H          1.55400       -1.51240       -0.82750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

