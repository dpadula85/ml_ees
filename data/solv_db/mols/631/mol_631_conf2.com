%nproc=16
%mem=2GB
%chk=mol_631_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.61530       -1.21410       -1.39730
C         -1.06770       -0.83850        0.00620
C          0.15930        0.03340       -0.31080
C          1.14790        0.17240        0.84660
O          2.13810        1.06240        0.16290
N          3.41100        0.44710        0.06940
O          3.39270       -0.46330       -0.92520
O          4.54830        0.96560        0.40640
O         -2.08870        0.06450        0.37600
N         -2.29490        1.07700        1.19830
O         -2.16380        1.22190        2.41050
O         -2.63730        2.29530        0.53080
H         -2.58640       -1.62240       -1.31750
H         -1.36440       -0.41540       -2.06400
H         -0.90430       -2.07300       -1.68710
H         -0.75690       -1.57660        0.62190
H          0.64100       -0.22630       -1.22220
H         -0.25700        1.08830       -0.41330
H          1.57560       -0.77060        1.08090
H          0.72280        0.77240        1.62730

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

