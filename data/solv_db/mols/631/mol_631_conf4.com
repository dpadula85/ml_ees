%nproc=16
%mem=2GB
%chk=mol_631_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.17830       -1.42500       -0.42810
C         -1.18170       -0.44930        0.15390
C          0.21100       -0.79590       -0.36770
C          1.23990        0.16740        0.19900
O          2.51040       -0.18790       -0.30750
N          3.65130        0.49040        0.01640
O          3.65870        1.68290        0.33190
O          4.83940       -0.21580       -0.01900
O         -1.56380        0.83990       -0.21510
N         -1.85630        1.85530        0.65140
O         -2.68970        2.72540        0.32600
O         -1.26260        1.95200        1.87360
H         -1.64260       -2.38220       -0.57070
H         -2.45640       -1.03710       -1.42820
H         -3.03490       -1.48280        0.24830
H         -1.15600       -0.51040        1.24500
H          0.25950       -0.75860       -1.47330
H          0.45040       -1.83180       -0.05520
H          1.00110        1.20510       -0.10810
H          1.20060        0.15850        1.30780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

