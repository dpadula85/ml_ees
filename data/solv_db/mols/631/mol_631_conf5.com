%nproc=16
%mem=2GB
%chk=mol_631_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.08850       -0.96240       -1.17120
C         -1.10680        0.03790       -0.04170
C          0.19410        0.83940       -0.07340
C          1.32850       -0.17580        0.09890
O          2.55050        0.51790        0.07600
N          3.72460       -0.17940        0.21310
O          4.74770        0.46260        0.51110
O          3.73890       -1.53710        0.02200
O         -2.23820        0.86500       -0.08600
N         -3.18880        0.93400        0.89630
O         -2.81730        0.79600        2.07860
O         -4.51120        1.14510        0.63780
H         -0.48000       -0.51140       -1.99430
H         -0.62420       -1.89130       -0.80370
H         -2.09250       -1.12960       -1.59310
H         -1.14240       -0.52180        0.91100
H          0.21620        1.56850        0.75320
H          0.30560        1.35790       -1.04680
H          1.24410       -0.91870       -0.73590
H          1.23970       -0.69660        1.06280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_631_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

