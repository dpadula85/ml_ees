%nproc=16
%mem=2GB
%chk=mol_516_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.64190       -0.19430        0.28970
C         -3.92860        0.03420       -1.01590
C         -2.47000        0.41600       -0.76690
C         -1.84600       -0.73200       -0.02110
O         -0.50460       -0.55850        0.29470
C          0.52220       -0.36740       -0.59270
O          0.25420       -0.34220       -1.81910
C          1.87040       -0.20330       -0.07350
C          2.93240       -0.00890       -0.92720
C          4.24030        0.15210       -0.46650
C          4.50390        0.11900        0.89180
C          3.45060       -0.07440        1.75040
C          2.16910       -0.23110        1.27940
O          5.76890        0.27050        1.41700
H         -4.39720       -1.20250        0.70690
H         -4.22360        0.55450        1.02490
H         -5.72870       -0.05510        0.22170
H         -4.00090       -0.86310       -1.66120
H         -4.45750        0.84720       -1.54180
H         -1.99790        0.64960       -1.72280
H         -2.45710        1.33650       -0.14690
H         -2.05550       -1.69200       -0.53470
H         -2.40610       -0.80450        0.95730
H          2.78490        0.02590       -2.00310
H          5.06170        0.30450       -1.16220
H          3.64170       -0.10200        2.80360
H          1.31480       -0.38620        1.94290
H          6.60060        0.41610        0.87520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

