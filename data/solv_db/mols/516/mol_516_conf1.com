%nproc=16
%mem=2GB
%chk=mol_516_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.77490       -0.74930       -1.06070
C          4.05610        0.09650       -0.04020
C          2.56140       -0.21960       -0.13760
C          1.77930        0.59820        0.86310
O          0.41050        0.37740        0.85170
C         -0.46800        0.57000       -0.18960
O         -0.00630        0.99730       -1.27300
C         -1.89490        0.29380       -0.06310
C         -2.75040        0.49680       -1.12470
C         -4.11790        0.23550       -1.01390
C         -4.66230       -0.23550        0.16270
C         -3.80050       -0.43570        1.21880
C         -2.44990       -0.17700        1.10750
O         -6.03360       -0.49850        0.27740
H          5.05210       -1.72380       -0.62440
H          5.70200       -0.25830       -1.42540
H          4.15460       -0.94740       -1.94800
H          4.18540        1.18520       -0.21120
H          4.41840       -0.14750        0.96940
H          2.27140        0.11530       -1.15160
H          2.36260       -1.28510       -0.04220
H          2.06250        1.67210        0.80740
H          2.15180        0.26550        1.87720
H         -2.34100        0.86540       -2.05400
H         -4.79350        0.39480       -1.84920
H         -4.20910       -0.80470        2.14990
H         -1.74880       -0.32940        1.93450
H         -6.66670       -0.35200       -0.49680

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

