%nproc=16
%mem=2GB
%chk=mol_516_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.22880        1.36150        0.59450
C         -3.08160        0.72420       -0.20190
C         -2.95540       -0.69680        0.29980
C         -1.87330       -1.46490       -0.37930
O         -0.63610       -0.79240       -0.14110
C          0.56170       -1.27850       -0.65450
O          0.54150       -2.32700       -1.33710
C          1.81880       -0.59470       -0.41230
C          1.83560        0.55390        0.33630
C          3.03840        1.19510        0.55940
C          4.19480        0.68570        0.03740
C          4.15590       -0.46760       -0.71060
C          2.96910       -1.12050       -0.94390
O          5.38200        1.36140        0.28560
H         -4.21900        0.90310        1.60270
H         -5.16410        1.11490        0.07600
H         -4.04840        2.44940        0.70710
H         -3.25500        0.79690       -1.27300
H         -2.18510        1.32420        0.05750
H         -2.70720       -0.63270        1.37850
H         -3.90830       -1.24550        0.19090
H         -2.05790       -1.64480       -1.44190
H         -1.78660       -2.44820        0.12790
H          0.92590        0.97260        0.75850
H          3.00710        2.10360        1.15860
H          5.08330       -0.86850       -1.12410
H          2.95150       -2.04000       -1.54240
H          5.64130        2.07570       -0.39300

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

