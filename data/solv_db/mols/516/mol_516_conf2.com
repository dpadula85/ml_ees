%nproc=16
%mem=2GB
%chk=mol_516_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.28520        1.53530       -0.44130
C         -3.06980        1.00200        0.28330
C         -3.06780       -0.51180        0.24270
C         -1.88830       -1.10210        0.94380
O         -0.65740       -0.66490        0.34670
C          0.56230       -1.07780        0.82260
O          0.58520       -1.85880        1.80720
C          1.83280       -0.65010        0.23580
C          1.89370        0.20480       -0.84510
C          3.09140        0.60880       -1.39970
C          4.29350        0.15870       -0.87670
C          4.23480       -0.69450        0.20210
C          3.03120       -1.09090        0.74720
O          5.47960        0.58370       -1.45730
H         -3.99020        2.00630       -1.38620
H         -4.85460        2.26510        0.20540
H         -4.97500        0.68280       -0.70670
H         -3.17030        1.29970        1.35180
H         -2.18110        1.45150       -0.16130
H         -3.17710       -0.85840       -0.80120
H         -3.97840       -0.84560        0.77930
H         -1.87940       -0.69230        1.98260
H         -1.96290       -2.19170        0.91010
H          0.94840        0.54770       -1.24260
H          3.10230        1.28310       -2.25060
H          5.17280       -1.04490        0.60960
H          2.97860       -1.77160        1.60770
H          5.93080        1.42570       -1.12320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

