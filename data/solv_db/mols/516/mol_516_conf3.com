%nproc=16
%mem=2GB
%chk=mol_516_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.19980        1.36340        0.07140
C          3.01830        0.73080       -0.62690
C          3.03910       -0.75060       -0.33250
C          1.86600       -1.40270       -1.02140
O          0.68260       -0.79130       -0.49540
C         -0.58220       -1.15110       -0.90800
O         -0.67620       -2.05180       -1.77940
C         -1.80210       -0.54520       -0.38920
C         -1.77560        0.44810        0.56780
C         -2.95590        1.01740        1.05340
C         -4.18390        0.59910        0.58630
C         -4.21310       -0.39170       -0.36840
C         -3.03890       -0.94800       -0.84180
O         -5.38940        1.11550        1.01990
H          5.11350        1.30230       -0.57840
H          4.37860        0.80600        1.01340
H          3.97400        2.42320        0.30990
H          3.02530        0.93190       -1.71230
H          2.08200        1.12670       -0.14670
H          2.94630       -0.87110        0.76800
H          3.96200       -1.23390       -0.70370
H          1.87520       -2.48880       -0.84350
H          1.92600       -1.20160       -2.10680
H         -0.85460        0.80090        0.95570
H         -2.95070        1.79640        1.80320
H         -5.15540       -0.75220       -0.76490
H         -3.08000       -1.72620       -1.59250
H         -5.43090        1.84460        1.71920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_516_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

