%nproc=16
%mem=2GB
%chk=mol_308_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35010        1.00620       -0.09490
C         -1.96850       -0.41490       -0.32690
C         -0.66060       -0.81340        0.29160
C          0.50770       -0.00790       -0.22450
C          1.74150       -0.54300        0.50530
C          2.95090        0.16060        0.09100
C          3.95020        0.75480       -0.24460
H         -2.80110        1.46070       -1.00890
H         -1.55080        1.66220        0.23850
H         -3.15880        1.06920        0.68880
H         -2.74300       -1.07720        0.12040
H         -1.96120       -0.67840       -1.41960
H         -0.46980       -1.86990        0.01210
H         -0.74880       -0.77800        1.38760
H          0.38280        1.08110       -0.05810
H          0.65860       -0.22570       -1.29110
H          1.82640       -1.61350        0.27840
H          1.56210       -0.45770        1.59540
H          4.83250        1.28490       -0.54050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

