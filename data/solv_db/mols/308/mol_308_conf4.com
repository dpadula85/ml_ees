%nproc=16
%mem=2GB
%chk=mol_308_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67320        1.02630        0.31880
C         -0.70790        0.86540       -0.82960
C         -0.41040       -0.54470       -1.16150
C          0.18650       -1.36590       -0.09330
C          1.48470       -1.03720        0.48900
C          1.68650        0.18580        1.18320
C          1.88070        1.20250        1.79440
H         -1.45950        0.34740        1.16150
H         -2.71220        0.85760       -0.05870
H         -1.62270        2.08470        0.65490
H         -1.25820        1.34260       -1.71210
H          0.13990        1.52970       -0.67300
H         -1.42020       -1.02620       -1.38750
H          0.16100       -0.63650       -2.12690
H          0.25420       -2.45360       -0.44650
H         -0.61100       -1.45470        0.72120
H          1.74260       -1.89060        1.20340
H          2.30470       -1.13580       -0.29230
H          2.03430        2.10310        2.32790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

