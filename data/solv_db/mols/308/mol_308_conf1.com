%nproc=16
%mem=2GB
%chk=mol_308_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.86510        1.08870        0.23720
C         -1.81500       -0.39730        0.24790
C         -0.79280       -1.07250       -0.55990
C          0.63920       -0.87880       -0.23160
C          1.11270        0.50950       -0.31740
C          2.59250        0.49160       -0.02410
C          3.77070        0.45640        0.21170
H         -1.30320        1.55250       -0.59520
H         -2.93830        1.43920        0.16370
H         -1.48540        1.48090        1.19450
H         -2.82630       -0.78730       -0.08450
H         -1.70440       -0.73110        1.33100
H         -1.01860       -2.19920       -0.63420
H         -0.99980       -0.77120       -1.64930
H          1.29660       -1.48630       -0.91560
H          0.89040       -1.26860        0.79920
H          0.70720        1.13220        0.54040
H          0.92980        1.01240       -1.26870
H          4.80950        0.42890        0.40830

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

