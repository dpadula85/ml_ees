%nproc=16
%mem=2GB
%chk=mol_308_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.99090       -0.20950        1.02040
C         -1.51820       -0.82670       -0.27640
C         -0.80820        0.15450       -1.15760
C          0.41740        0.74580       -0.50770
C          1.40130       -0.38070       -0.16820
C          2.58200        0.25610        0.46550
C          3.55260        0.76510        0.96780
H         -1.14700        0.13270        1.65050
H         -2.67570        0.63180        0.80610
H         -2.60180       -0.98530        1.54140
H         -0.90320       -1.72190       -0.06330
H         -2.44850       -1.13410       -0.83110
H         -0.47770       -0.38610       -2.06340
H         -1.52400        0.94380       -1.43670
H          0.17970        1.33630        0.39980
H          0.90490        1.41480       -1.24330
H          0.91370       -1.04760        0.56890
H          1.73290       -0.90100       -1.07260
H          4.41070        1.21180        1.40000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

