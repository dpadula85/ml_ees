%nproc=16
%mem=2GB
%chk=mol_308_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26370        0.37810       -0.23860
C         -1.71170       -0.67160        0.69550
C         -0.22710       -0.46850        0.92180
C          0.51200       -0.56040       -0.39040
C          1.98240       -0.37370       -0.27330
C          2.33080        0.92620        0.27990
C          2.59690        1.99790        0.73400
H         -2.11270        0.01140       -1.28540
H         -3.32150        0.52240       -0.00160
H         -1.71980        1.35510       -0.07950
H         -2.28040       -0.63900        1.62660
H         -1.85190       -1.65960        0.21100
H          0.09130       -1.27560        1.63360
H         -0.06930        0.48150        1.44520
H          0.06130        0.18830       -1.06910
H          0.30020       -1.55470       -0.82990
H          2.41350       -0.41850       -1.29580
H          2.44920       -1.20780        0.28970
H          2.82040        2.96850        1.13530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_308_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

