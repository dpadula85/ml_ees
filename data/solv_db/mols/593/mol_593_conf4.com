%nproc=16
%mem=2GB
%chk=mol_593_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.46680       -0.18130       -0.03440
S          0.02330       -0.55870        0.95030
C          1.44920        0.15890        0.06690
H         -1.83870        0.85190        0.17800
H         -2.21890       -0.94890        0.24270
H         -1.21090       -0.21250       -1.12620
H          1.87420        0.92870        0.75030
H          2.23280       -0.61760       -0.11590
H          1.15580        0.57940       -0.91170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

