%nproc=16
%mem=2GB
%chk=mol_593_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.47550       -0.02670        0.02210
S         -0.01030       -0.32880       -1.03640
C          1.45660        0.00520       -0.01150
H         -2.28460       -0.63490       -0.40810
H         -1.21010       -0.28960        1.05900
H         -1.74510        1.04220       -0.09420
H          1.95070        0.89380       -0.44300
H          2.11830       -0.88170       -0.12090
H          1.19990        0.22050        1.03310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

