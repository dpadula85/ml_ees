%nproc=16
%mem=2GB
%chk=mol_593_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.46490       -0.04300        0.38710
S         -0.05940       -0.41310        1.26850
C         -1.47270        0.00530        0.23270
H          2.14380       -0.90760        0.44940
H          1.96490        0.79240        0.92460
H          1.31120        0.24860       -0.66100
H         -2.02780        0.85580        0.68620
H         -1.18260        0.33010       -0.78110
H         -2.14240       -0.86840        0.19660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

