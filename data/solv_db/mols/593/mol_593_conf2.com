%nproc=16
%mem=2GB
%chk=mol_593_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45790        0.14830        0.15260
S          0.03980       -0.46550        0.98220
C          1.44990       -0.17930       -0.10150
H         -1.84410        1.00450        0.73200
H         -2.19760       -0.69100        0.15920
H         -1.24020        0.46090       -0.89890
H          1.97450       -1.11180       -0.39250
H          2.13150        0.52350        0.41360
H          1.14400        0.31050       -1.04670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_593_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

