%nproc=16
%mem=2GB
%chk=mol_544_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.51780        0.04760        0.78690
C         -0.45790       -0.29910       -0.23680
C          0.89280       -0.33020        0.34040
O          1.21240       -1.05040        1.32220
N          1.91360        0.49850       -0.19100
H         -2.18980       -0.81880        0.90110
H         -1.11500        0.36200        1.75540
H         -2.15710        0.89810        0.41350
H         -0.62970       -1.34750       -0.64230
H         -0.55140        0.38690       -1.08830
H          1.95510        0.81490       -1.18020
H          2.64480        0.83810        0.44560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

