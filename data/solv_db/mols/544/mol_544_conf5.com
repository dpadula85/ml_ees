%nproc=16
%mem=2GB
%chk=mol_544_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.67300        0.16830        0.33040
C         -0.28670        0.50950       -0.12320
C          0.68480       -0.46340        0.41530
O          0.25240       -1.39460        1.13970
N          2.07850       -0.34780        0.12000
H         -2.06930       -0.61420       -0.35000
H         -2.33940        1.03890        0.41160
H         -1.56960       -0.33190        1.33200
H         -0.28930        0.56280       -1.22420
H          0.00260        1.52750        0.26500
H          2.65910       -1.21230        0.10980
H          2.54990        0.55730       -0.08700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

