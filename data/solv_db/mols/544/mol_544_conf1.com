%nproc=16
%mem=2GB
%chk=mol_544_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.50150        0.21230        0.28300
C          0.41280       -0.48690       -0.49860
C         -0.82850       -0.44420        0.30520
O         -1.01010       -1.16320        1.30390
N         -1.85820        0.45690       -0.07670
H          1.40810        0.04070        1.37020
H          1.39040        1.30710        0.12260
H          2.49490       -0.06380       -0.12580
H          0.29400        0.05070       -1.46320
H          0.66520       -1.53280       -0.75260
H         -2.85020        0.29290        0.14270
H         -1.61990        1.33020       -0.61070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

