%nproc=16
%mem=2GB
%chk=mol_544_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.38420       -0.38030       -0.25370
C         -0.36290        0.19130        0.71470
C          0.75190        0.80610       -0.05100
O          0.90580        2.05880       -0.08770
N          1.72810       -0.10140       -0.58050
H         -0.96620       -1.31140       -0.65590
H         -1.63000        0.31210       -1.06950
H         -2.27200       -0.66240        0.36630
H         -0.77600        0.92710        1.40830
H         -0.00060       -0.69440        1.29570
H          2.59010       -0.18650        0.01950
H          1.41600       -0.95910       -1.10620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

