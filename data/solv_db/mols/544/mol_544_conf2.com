%nproc=16
%mem=2GB
%chk=mol_544_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.51050        0.36750       -0.01190
C          0.34500       -0.58080       -0.20710
C         -0.79280       -0.18550        0.64150
O         -0.66080       -0.16100        1.88700
N         -2.01160        0.22280        0.00580
H          1.17360        1.35150       -0.38630
H          2.37030        0.02980       -0.63340
H          1.77930        0.49620        1.05270
H          0.06790       -0.53200       -1.29030
H          0.60710       -1.63170        0.01650
H         -2.44630       -0.53040       -0.59070
H         -1.94210        1.15370       -0.48380

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_544_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

