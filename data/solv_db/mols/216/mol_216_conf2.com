%nproc=16
%mem=2GB
%chk=mol_216_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.27990       -0.32860        0.49520
C          1.81420       -0.25540        0.19330
C          1.24440       -1.03930       -0.78950
C         -0.10170       -0.99320       -1.08930
C         -0.94400       -0.13420       -0.39260
C         -0.36340        0.64310        0.58600
C          0.98190        0.59770        0.88630
C         -2.37070       -0.07330       -0.69710
O         -2.86940       -0.78390       -1.58300
C         -3.21460        0.86370        0.08230
H          3.45330        0.24920        1.43790
H          3.90190        0.01140       -0.33170
H          3.58400       -1.37720        0.73550
H          1.90750       -1.70800       -1.33010
H         -0.52890       -1.61570       -1.86290
H         -1.01980        1.32100        1.13810
H          1.43610        1.21790        1.66500
H         -4.20690        1.02400       -0.38190
H         -2.69790        1.85820        0.10950
H         -3.28580        0.52260        1.12910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

