%nproc=16
%mem=2GB
%chk=mol_216_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.34130        0.10180       -0.00430
C         -1.86250       -0.07100        0.05390
C         -1.27210       -1.16260        0.66460
C          0.10730       -1.24980        0.67610
C          0.91080       -0.29560        0.10520
C          0.33280        0.78700       -0.50050
C         -1.04470        0.87780       -0.51420
C          2.35890       -0.41410        0.13640
O          2.86910       -1.43150        0.71140
C          3.26520        0.56610       -0.45360
H         -3.60770        0.91100        0.69990
H         -3.86310       -0.83300        0.27020
H         -3.61730        0.46340       -1.02940
H         -1.90470       -1.92140        1.11850
H          0.54170       -2.12850        1.16890
H          0.95090        1.56210       -0.96090
H         -1.49760        1.75370       -1.00480
H          2.88470        1.06690       -1.36320
H          3.58690        1.36440        0.26910
H          4.20270        0.05330       -0.75120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

