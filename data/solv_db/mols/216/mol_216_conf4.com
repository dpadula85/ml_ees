%nproc=16
%mem=2GB
%chk=mol_216_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.33070        0.22340       -0.07680
C          1.85070        0.03370       -0.14000
C          1.26220       -0.85140       -1.01770
C         -0.10160       -0.99950       -1.04830
C         -0.95340       -0.29230       -0.22520
C         -0.35750        0.60190        0.66140
C          1.01360        0.74930        0.69050
C         -2.39180       -0.46300       -0.27180
O         -2.89590       -1.27250       -1.07640
C         -3.27360        0.31120        0.62370
H          3.87860       -0.62440       -0.49650
H          3.63620        1.18250       -0.53780
H          3.57100        0.31690        1.01340
H          1.88370       -1.42500       -1.67880
H         -0.56050       -1.69620       -1.73970
H         -1.00370        1.15810        1.30720
H          1.48400        1.44950        1.38420
H         -2.90340        1.37190        0.59700
H         -3.13460       -0.01690        1.67390
H         -4.33460        0.24270        0.35780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

