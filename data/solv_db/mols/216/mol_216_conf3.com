%nproc=16
%mem=2GB
%chk=mol_216_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.32180       -0.01220        0.15720
C          1.83650       -0.06740        0.16440
C          1.05160        0.78090       -0.59750
C         -0.32550        0.68820       -0.55490
C         -0.96690       -0.24450        0.24110
C         -0.15290       -1.08360        0.99590
C          1.22180       -0.99610        0.95770
C         -2.41510       -0.34100        0.28550
O         -2.96570       -1.18710        1.00850
C         -3.22180        0.57590       -0.53650
H          3.67310       -0.69580       -0.65520
H          3.75830       -0.27860        1.13890
H          3.71150        0.99220       -0.10380
H          1.56790        1.50950       -1.21890
H         -0.90400        1.36890       -1.16490
H         -0.64990       -1.81660        1.62140
H          1.79190       -1.68640        1.57600
H         -2.88370        0.54670       -1.60900
H         -3.15530        1.63540       -0.16800
H         -4.29350        0.31160       -0.55870

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

