%nproc=16
%mem=2GB
%chk=mol_216_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.33000       -0.23090        0.08200
C         -1.83760       -0.21250        0.14210
C         -1.14690       -1.19420        0.82620
C          0.23150       -1.19510        0.89360
C          0.97910       -0.21290        0.27870
C          0.28440        0.76420       -0.40260
C         -1.09400        0.77170       -0.47430
C          2.44650       -0.21150        0.34850
O          3.06240       -1.09420        0.96300
C          3.21520        0.85370       -0.32000
H         -3.70860       -0.67190       -0.84000
H         -3.75810        0.78960        0.16200
H         -3.70950       -0.80050        0.94350
H         -1.74300       -1.97800        1.31680
H          0.74640       -1.98610        1.44300
H          0.85970        1.55920       -0.90220
H         -1.63860        1.53760       -1.00810
H          2.73100        1.85840       -0.14800
H          3.16980        0.70700       -1.42400
H          4.24050        0.94650        0.06660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_216_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

