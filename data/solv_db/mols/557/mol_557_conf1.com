%nproc=16
%mem=2GB
%chk=mol_557_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.78400        0.60040       -0.44230
C         -2.30090        0.29180        0.96320
C         -1.11440       -0.66690        0.79480
C         -0.02820        0.01730       -0.02990
C          1.10080       -0.94720       -0.16740
C          2.26200       -0.42120       -0.96080
C          2.88960        0.80490       -0.38990
H         -2.15850        1.35310       -0.92130
H         -3.85140        0.93690       -0.32490
H         -2.83680       -0.36900       -0.98780
H         -2.01820        1.17650        1.53320
H         -3.10820       -0.21990        1.50950
H         -1.44400       -1.60160        0.29950
H         -0.71420       -0.90960        1.79700
H         -0.49100        0.18420       -1.04200
H          0.22640        0.99520        0.36730
H          0.71740       -1.84270       -0.70580
H          1.42510       -1.31200        0.82930
H          3.03950       -1.22910       -0.96250
H          1.96370       -0.30710       -2.02660
H          3.84650        0.60980        0.14570
H          2.24700        1.35300        0.32180
H          3.13190        1.50340       -1.22080

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

