%nproc=16
%mem=2GB
%chk=mol_557_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.16280        0.59850       -0.03590
C         -1.72610        0.22850        0.39890
C         -1.19440       -0.84890       -0.48070
C          0.13860       -1.40210       -0.24310
C          1.36220       -0.60320       -0.33790
C          1.58900        0.55970        0.54130
C          3.01600        1.06980        0.16330
H         -3.15490        1.43200       -0.75330
H         -3.66400       -0.26540       -0.50630
H         -3.76150        0.91270        0.84120
H         -1.81360       -0.05500        1.45530
H         -1.20550        1.19910        0.35480
H         -1.29240       -0.56560       -1.57060
H         -1.95940       -1.69660       -0.39410
H          0.29730       -2.27480       -0.98240
H          0.16700       -2.00520        0.74670
H          1.53380       -0.27750       -1.40550
H          2.23040       -1.31280       -0.15590
H          1.61380        0.34610        1.61810
H          0.96200        1.45370        0.29630
H          3.14600        0.95500       -0.92730
H          3.76450        0.41830        0.65810
H          3.11400        2.13360        0.42470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

