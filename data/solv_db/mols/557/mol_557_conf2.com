%nproc=16
%mem=2GB
%chk=mol_557_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.21850        0.21690       -0.43900
C          2.10540       -0.38160        0.37270
C          0.78190       -0.33510       -0.34370
C         -0.32860       -0.93170        0.46480
C         -1.65190       -0.90090       -0.21690
C         -2.13370        0.47660       -0.56170
C         -2.29090        1.35610        0.66280
H          3.91360       -0.57070       -0.80830
H          3.82140        0.92090        0.15770
H          2.82660        0.78630       -1.32310
H          2.06740        0.14790        1.34970
H          2.39280       -1.43380        0.61640
H          0.90090       -0.94490       -1.27510
H          0.58260        0.73240       -0.59000
H         -0.06790       -2.00230        0.64020
H         -0.35450       -0.47460        1.48920
H         -1.56500       -1.46900       -1.16110
H         -2.41580       -1.36780        0.43290
H         -3.17460        0.35390       -0.97690
H         -1.54230        0.96980       -1.33470
H         -2.57350        2.37500        0.28500
H         -3.15120        1.02180        1.28500
H         -1.36090        1.45500        1.23560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

