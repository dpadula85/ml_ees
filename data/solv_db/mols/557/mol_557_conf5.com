%nproc=16
%mem=2GB
%chk=mol_557_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.54260        0.23380       -0.07340
C          2.20020        0.22240        0.62710
C          1.10190       -0.20800       -0.35380
C         -0.21370       -0.20420        0.38890
C         -1.29270       -0.62460       -0.56490
C         -2.65860       -0.65740        0.07000
C         -2.94310        0.75810        0.56020
H          3.68250        1.19850       -0.61770
H          3.61970       -0.58290       -0.82090
H          4.36460        0.16240        0.66700
H          1.93850        1.24910        0.93250
H          2.24260       -0.44760        1.49950
H          1.33170       -1.20280       -0.76000
H          1.08130        0.56610       -1.14740
H         -0.39500        0.83170        0.74800
H         -0.18480       -0.84130        1.29270
H         -1.33990        0.17360       -1.36240
H         -1.04310       -1.56190       -1.07080
H         -3.38370       -0.89030       -0.75440
H         -2.75970       -1.40600        0.85740
H         -4.01610        0.92810        0.71320
H         -2.49130        1.44450       -0.19030
H         -2.38390        0.85890        1.53440

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

