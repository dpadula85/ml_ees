%nproc=16
%mem=2GB
%chk=mol_557_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.19870        0.68210        0.43780
C         -2.11780       -0.10590       -0.26200
C         -0.83620       -0.14070        0.56830
C          0.15140       -0.93650       -0.21110
C          1.49060       -1.10280        0.41660
C          2.23200        0.16340        0.68860
C          2.48250        0.94490       -0.59210
H         -3.51650        0.12660        1.31980
H         -4.03570        0.78540       -0.28720
H         -2.87130        1.71970        0.67400
H         -1.89360        0.37890       -1.22910
H         -2.47570       -1.14200       -0.38070
H         -1.09160       -0.65730        1.51560
H         -0.54970        0.87610        0.80860
H          0.29720       -0.51860       -1.22840
H         -0.27940       -1.94990       -0.35860
H          1.43600       -1.68880        1.35750
H          2.11400       -1.70810       -0.27720
H          1.76820        0.83180        1.41820
H          3.23470       -0.11160        1.07870
H          3.11310        0.39830       -1.29570
H          3.02510        1.87120       -0.29580
H          1.52160        1.28370       -1.03600

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_557_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

