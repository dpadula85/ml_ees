%nproc=16
%mem=2GB
%chk=mol_305_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26100       -0.04010        0.67000
C         -1.45820       -0.29230       -0.59820
C         -0.03570        0.02510       -0.36840
C          0.81720       -0.95540        0.08570
C          2.15710       -0.65030        0.30060
C          2.62370        0.61280        0.06280
N          1.78530        1.55360       -0.37740
C          0.48450        1.28970       -0.59470
H         -2.46910       -0.98150        1.22380
H         -1.70190        0.70340        1.25540
H         -3.25540        0.39200        0.39850
H         -1.52870       -1.37560       -0.81870
H         -1.88730        0.25930       -1.46370
H          0.44150       -1.93590        0.26730
H          2.82860       -1.43490        0.66150
H          3.68080        0.78850        0.24990
H         -0.22140        2.04150       -0.95450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

