%nproc=16
%mem=2GB
%chk=mol_305_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.32190        0.22150        0.54470
C         -1.44380        0.19570       -0.68590
C         -0.02720       -0.09780       -0.35910
C          0.85750        0.92940       -0.07700
C          2.18080        0.64540        0.22820
C          2.65590       -0.64810        0.26130
N          1.77440       -1.62630       -0.01690
C          0.48440       -1.37970       -0.31640
H         -3.37510       -0.04020        0.30530
H         -2.37250        1.24360        1.00470
H         -1.94300       -0.44080        1.35380
H         -1.49470        1.20310       -1.14340
H         -1.79930       -0.55950       -1.40470
H          0.48700        1.95750       -0.10160
H          2.85440        1.46120        0.44510
H          3.70270       -0.88580        0.50210
H         -0.21940       -2.17930       -0.54000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

