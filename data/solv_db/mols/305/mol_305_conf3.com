%nproc=16
%mem=2GB
%chk=mol_305_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.33740       -0.51640       -0.13090
C          1.45240        0.67730        0.11380
C          0.00730        0.35430       -0.03550
C         -0.74160       -0.07750        1.04060
C         -2.09690       -0.37730        0.88880
C         -2.72670       -0.25260       -0.33340
N         -1.97870        0.16980       -1.36970
C         -0.65930        0.46640       -1.24240
H          1.76010       -1.45670       -0.12160
H          2.84350       -0.44980       -1.12060
H          3.13560       -0.55230        0.66150
H          1.68030        1.10970        1.11230
H          1.76560        1.46870       -0.62510
H         -0.24370       -0.17430        1.99740
H         -2.66810       -0.71300        1.73870
H         -3.77860       -0.47950       -0.47860
H         -0.08860        0.80310       -2.09540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

