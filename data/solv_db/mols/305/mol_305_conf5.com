%nproc=16
%mem=2GB
%chk=mol_305_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.28850        0.54710       -0.13860
C          1.44980       -0.56240        0.50190
C          0.01840       -0.35000        0.13790
C         -0.84560        0.40160        0.88540
C         -2.17360        0.59840        0.54690
C         -2.60710       -0.01690       -0.61800
N         -1.77180       -0.76920       -1.38040
C         -0.46890       -0.94020       -1.01410
H          3.34520        0.36540        0.05930
H          2.14480        0.55220       -1.25250
H          1.95690        1.51870        0.23260
H          1.60470       -0.48410        1.57700
H          1.82940       -1.50820        0.05110
H         -0.46400        0.86070        1.78280
H         -2.85590        1.19390        1.14040
H         -3.64350        0.12190       -0.90500
H          0.19280       -1.52900       -1.60660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

