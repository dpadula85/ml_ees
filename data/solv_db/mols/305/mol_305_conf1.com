%nproc=16
%mem=2GB
%chk=mol_305_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.23300        0.78100        0.38880
C         -1.50700       -0.29560       -0.35870
C         -0.05410       -0.28000       -0.18620
C          0.82540        0.41390       -0.98650
C          2.18010        0.35970       -0.72900
C          2.70930       -0.37640        0.32020
N          1.81850       -1.04240        1.08200
C          0.49520       -1.00640        0.85310
H         -3.32560        0.49120        0.38250
H         -1.94440        0.78860        1.45900
H         -2.18770        1.76860       -0.08270
H         -1.81780       -0.24110       -1.43790
H         -1.88790       -1.27530       -0.00150
H          0.41520        0.98460       -1.80010
H          2.87490        0.90390       -1.35610
H          3.78530       -0.40930        0.51100
H         -0.14640       -1.56500        1.50640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_305_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

