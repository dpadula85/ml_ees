%nproc=16
%mem=2GB
%chk=mol_191_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.06070        0.27250        0.57990
C          1.63540       -0.03470        0.26640
C          1.26020       -1.32550       -0.00040
C         -0.04650       -1.61160       -0.28910
C         -1.00090       -0.61550       -0.31650
C         -0.64370        0.66880       -0.05410
C          0.67210        0.96130        0.23730
C         -1.62820        1.75360       -0.07380
C         -2.39300       -1.01510       -0.63850
H          3.09430        1.28790        0.98800
H          3.61220        0.21530       -0.39780
H          3.48270       -0.45440        1.28540
H          1.97280       -2.13260        0.01050
H         -0.31650       -2.64440       -0.49690
H          0.98350        1.98210        0.45130
H         -2.65160        1.43970        0.20270
H         -1.30070        2.52280        0.65590
H         -1.60510        2.27110       -1.07410
H         -2.35240       -1.85760       -1.38950
H         -3.02530       -0.19200       -0.96600
H         -2.81010       -1.49170        0.28330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

