%nproc=16
%mem=2GB
%chk=mol_191_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.10950        0.04070        0.20990
C          1.65100       -0.15050        0.07580
C          1.17140       -1.39930       -0.21700
C         -0.18780       -1.55410       -0.33700
C         -1.08700       -0.51420       -0.17680
C         -0.59610        0.72700        0.11520
C          0.77210        0.89950        0.23950
C         -1.51040        1.85140        0.29170
C         -2.51530       -0.82390       -0.33440
H          3.62680       -0.60230       -0.53720
H          3.46620       -0.24560        1.22590
H          3.36070        1.09090       -0.05120
H          1.84390       -2.25880       -0.35390
H         -0.53710       -2.56970       -0.57250
H          1.20120        1.87800        0.47240
H         -0.98230        2.63080        0.88800
H         -2.43160        1.62010        0.85400
H         -1.74560        2.34870       -0.68770
H         -3.20410       -0.08500        0.04430
H         -2.70300       -1.77200        0.24770
H         -2.70250       -1.11170       -1.39650

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

