%nproc=16
%mem=2GB
%chk=mol_191_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.15360       -0.07110        0.03490
C          1.67010        0.17960        0.03080
C          1.15180        1.45480        0.10810
C         -0.22290        1.64520        0.10120
C         -1.09170        0.57660        0.01800
C         -0.54720       -0.68580       -0.05800
C          0.82460       -0.89850       -0.05270
C         -1.47780       -1.83540       -0.14780
C         -2.57300        0.80030        0.01200
H          3.34650       -0.64660        0.98050
H          3.47530       -0.66400       -0.82990
H          3.70500        0.88190        0.03820
H          1.85260        2.26900        0.17200
H         -0.60150        2.67360        0.16410
H          1.23030       -1.90740       -0.11390
H         -0.92090       -2.69770       -0.59170
H         -1.82240       -2.04650        0.88640
H         -2.33050       -1.64180       -0.83390
H         -2.84710        1.59830        0.75470
H         -3.08980       -0.13400        0.32170
H         -2.88510        1.14930       -0.99470

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

