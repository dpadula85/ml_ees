%nproc=16
%mem=2GB
%chk=mol_191_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.15660        0.05330        0.35300
C          1.68160       -0.14680        0.23990
C          1.10310       -1.36560        0.51290
C         -0.25790       -1.57840        0.41630
C         -1.10940       -0.56430        0.03650
C         -0.53650        0.65810       -0.23810
C          0.83240        0.87260       -0.14100
C         -1.45100        1.75440       -0.64850
C         -2.57690       -0.74820       -0.08070
H          3.50290        0.83880       -0.35520
H          3.45260        0.29680        1.39920
H          3.72140       -0.86680        0.08320
H          1.76140       -2.16620        0.81100
H         -0.68140       -2.55290        0.63900
H          1.27400        1.85250       -0.36310
H         -2.24750        1.93400        0.09380
H         -0.85860        2.69130       -0.70420
H         -1.92000        1.58030       -1.63280
H         -2.77960       -1.58610       -0.81080
H         -3.03070       -1.10110        0.87490
H         -3.03650        0.14430       -0.48540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

