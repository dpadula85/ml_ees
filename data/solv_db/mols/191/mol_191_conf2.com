%nproc=16
%mem=2GB
%chk=mol_191_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.09580       -0.62360       -0.07950
C          1.68260       -0.14650       -0.06460
C          1.36780        1.19300       -0.17150
C          0.06410        1.64780       -0.15890
C         -0.99260        0.76720       -0.03710
C         -0.66840       -0.56890        0.06910
C          0.63020       -1.03000        0.05720
C         -1.76380       -1.55390        0.20250
C         -2.40350        1.23760       -0.02180
H          3.71390        0.22580       -0.48590
H          3.22420       -1.54320       -0.67420
H          3.41740       -0.75070        0.97980
H          2.20720        1.87290       -0.26650
H         -0.17090        2.70720       -0.24390
H          0.86220       -2.09680        0.14300
H         -1.52440       -2.39980       -0.48460
H         -2.74790       -1.15820       -0.03970
H         -1.71120       -1.98690        1.23700
H         -2.38920        2.34070       -0.20910
H         -2.85380        1.13780        1.00630
H         -3.03980        0.72860       -0.75760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_191_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

