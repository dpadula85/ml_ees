%nproc=16
%mem=2GB
%chk=mol_296_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.67240        1.87650        0.11760
C         -0.28680        0.61550        0.83520
C          1.19150        0.60130        1.00760
C          1.94320        0.17340       -0.22760
C          1.50680       -1.27310       -0.49640
C          0.05250       -1.17850       -0.89330
C         -0.80920       -0.63300        0.21780
C         -2.21210       -0.54330       -0.27730
H         -1.73550        2.16420        0.28490
H         -0.07710        2.70450        0.59350
H         -0.39360        1.87920       -0.94820
H         -0.73800        0.69030        1.86590
H          1.48040       -0.11710        1.82520
H          1.55530        1.61500        1.29640
H          1.62180        0.74530       -1.12320
H          3.01640        0.22630       -0.03780
H          1.54870       -1.81750        0.46930
H          2.07150       -1.73800       -1.29820
H         -0.29140       -2.24120       -1.05970
H         -0.10120       -0.66490       -1.84690
H         -0.79600       -1.42580        1.02260
H         -2.27910       -0.74890       -1.38650
H         -2.70610        0.41440       -0.11920
H         -2.88950       -1.32460        0.17860

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

