%nproc=16
%mem=2GB
%chk=mol_296_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.97050       -1.09450        0.24640
C          0.57710       -0.63150        0.43520
C         -0.33480       -1.29250       -0.60640
C         -1.79130       -1.11860       -0.22020
C         -2.06680        0.32430        0.11610
C         -0.90830        1.18750       -0.29170
C          0.36080        0.82560        0.46430
C          1.47030        1.65840       -0.10220
H          2.67140       -0.39140       -0.17820
H          2.42140       -1.46630        1.20770
H          1.97160       -2.00080       -0.42050
H          0.21500       -1.05510        1.41910
H         -0.11210       -2.38640       -0.52440
H         -0.16100       -0.88950       -1.61060
H         -2.04660       -1.78430        0.62650
H         -2.39520       -1.45210       -1.08160
H         -2.94490        0.64680       -0.46870
H         -2.31720        0.48610        1.18040
H         -0.75410        1.15790       -1.38160
H         -1.15080        2.24140        0.03080
H          0.19060        1.19760        1.51750
H          2.21830        1.87920        0.69530
H          1.89830        1.29170       -1.03180
H          1.01770        2.66630       -0.34330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

