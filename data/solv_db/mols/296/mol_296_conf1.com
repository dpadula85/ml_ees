%nproc=16
%mem=2GB
%chk=mol_296_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.36790        1.60360       -0.69930
C         -0.52350        0.89040        0.36730
C          0.88370        1.29400        0.11800
C          1.93050        0.31840        0.49730
C          1.58000       -1.06230       -0.01260
C          0.33700       -1.45210        0.73830
C         -0.82940       -0.53940        0.47060
C         -1.53280       -1.11910       -0.75510
H         -1.12240        2.67590       -0.59770
H         -2.43440        1.48620       -0.53270
H         -1.02570        1.30470       -1.71690
H         -0.85190        1.40920        1.31890
H          1.04080        1.55170       -0.96990
H          1.12940        2.26260        0.64760
H          2.88800        0.62520        0.02400
H          2.11920        0.23040        1.57580
H          1.44420       -1.07190       -1.10240
H          2.37940       -1.75920        0.27760
H          0.07780       -2.48720        0.42910
H          0.58250       -1.49390        1.81320
H         -1.54890       -0.73530        1.32270
H         -2.09610       -0.38510       -1.31510
H         -2.25100       -1.92900       -0.45790
H         -0.80830       -1.61790       -1.44100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

