%nproc=16
%mem=2GB
%chk=mol_296_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.62760       -1.24750        0.15800
C         -0.62630       -0.46190       -0.70000
C          0.57650       -1.30430       -0.86930
C          1.74200       -0.96010       -0.01880
C          1.98380        0.54600        0.00390
C          0.74630        1.11700        0.69700
C         -0.41090        0.91530       -0.21170
C         -1.64200        1.47300        0.50480
H         -2.16790       -1.98840       -0.49850
H         -1.11560       -1.84070        0.91940
H         -2.37270       -0.60400        0.62240
H         -1.13560       -0.39030       -1.70780
H          0.33290       -2.39140       -0.68160
H          0.92200       -1.29410       -1.94220
H          1.54280       -1.24030        1.04950
H          2.64110       -1.47000       -0.37630
H          2.11550        0.96930       -0.99570
H          2.89700        0.75030        0.57960
H          0.90600        2.20380        0.82880
H          0.61740        0.61260        1.66300
H         -0.26770        1.59970       -1.09850
H         -1.43490        2.57690        0.60890
H         -1.66470        1.07910        1.53250
H         -2.55740        1.34990       -0.06720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

