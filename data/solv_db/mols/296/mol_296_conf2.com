%nproc=16
%mem=2GB
%chk=mol_296_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.73660        1.54770        0.01560
C         -0.48790        0.83440        0.39660
C          0.59630        1.18980       -0.59430
C          1.89630        0.62370       -0.01320
C          1.71970       -0.89370       -0.08040
C          0.63900       -1.30220        0.88440
C         -0.65800       -0.64960        0.54210
C         -1.27290       -1.30800       -0.65420
H         -2.36900        0.97650       -0.67710
H         -2.32440        1.79430        0.92900
H         -1.44010        2.52570       -0.44960
H         -0.16530        1.22270        1.39420
H          0.63560        2.26910       -0.73540
H          0.44730        0.64620       -1.56360
H          2.77280        0.91130       -0.60030
H          1.94480        0.97440        1.04990
H          1.35070       -1.10830       -1.09810
H          2.65890       -1.42100        0.06470
H          0.90440       -1.12930        1.93960
H          0.49740       -2.41060        0.74410
H         -1.34760       -0.80730        1.41740
H         -1.08510       -0.71380       -1.56300
H         -2.36230       -1.45590       -0.55760
H         -0.81390       -2.31600       -0.79090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_296_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

