%nproc=16
%mem=2GB
%chk=mol_573_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.22520       -1.17780        0.14080
C         -1.58120       -0.93800        0.13970
N         -2.07530        0.30860        0.00500
C         -1.26050        1.38030       -0.13510
C          0.08090        1.15330       -0.13520
C          0.64410       -0.09740       -0.00140
C          2.08850       -0.28400       -0.00810
O          2.64540       -1.39320        0.10940
H          0.19430       -2.16950        0.24750
H         -2.25620       -1.77280        0.24960
H         -1.69860        2.38580       -0.24280
H          0.73930        2.01880       -0.24860
H          2.70450        0.58600       -0.12090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_573_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_573_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_573_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

