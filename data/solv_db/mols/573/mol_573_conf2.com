%nproc=16
%mem=2GB
%chk=mol_573_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.21940        1.15710       -0.21130
C         -1.10580        1.49770       -0.12020
N         -2.03020        0.55750        0.13380
C         -1.65150       -0.72340        0.29920
C         -0.34130       -1.13160        0.22060
C          0.61500       -0.16930       -0.04040
C          2.03050       -0.50900       -0.14170
O          2.36580       -1.70030        0.01400
H          1.00300        1.87820       -0.41430
H         -1.37220        2.53590       -0.25870
H         -2.41570       -1.46060        0.50270
H         -0.07720       -2.16900        0.35920
H          2.76030        0.23680       -0.34290

--Link1--
%nproc=16
%mem=2GB
%chk=mol_573_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_573_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_573_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

