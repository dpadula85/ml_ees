%nproc=16
%mem=2GB
%chk=mol_277_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.26760        0.11230        0.01050
C         -0.77460        0.05670        0.00460
C         -0.12840       -1.16080       -0.11620
C          1.27020       -1.24190       -0.12420
C          2.03980       -0.10580       -0.01150
C          1.38820        1.10660        0.10880
C          0.01090        1.18400        0.11640
Br         3.93950       -0.25520       -0.02620
H         -2.67950        0.29710       -1.00610
H         -2.71460       -0.80530        0.40310
H         -2.63700        0.94840        0.65820
H         -0.73090       -2.07160       -0.20650
H          1.75760       -2.20500       -0.21960
H          2.01140        2.00360        0.19770
H         -0.48500        2.13700        0.21100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

