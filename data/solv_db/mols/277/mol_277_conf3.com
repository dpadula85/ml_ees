%nproc=16
%mem=2GB
%chk=mol_277_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.27130       -0.06570       -0.16360
C          0.78040       -0.02730       -0.06040
C          0.03610       -1.17950        0.05130
C         -1.34030       -1.16030        0.14760
C         -2.03070        0.03520        0.13510
C         -1.28210        1.19040        0.02300
C          0.09340        1.17430       -0.07340
Br        -3.94010        0.10770        0.26580
H          2.68710       -0.42900        0.81680
H          2.63620        0.96840       -0.27420
H          2.62320       -0.65790       -1.01850
H          0.55250       -2.11990        0.06290
H         -1.93010       -2.08260        0.23670
H         -1.81860        2.14500        0.01210
H          0.66180        2.10130       -0.16120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

