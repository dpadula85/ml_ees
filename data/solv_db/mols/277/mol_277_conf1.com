%nproc=16
%mem=2GB
%chk=mol_277_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25720       -0.17330       -0.04620
C         -0.78840       -0.02690       -0.02100
C         -0.14190        1.19150       -0.08800
C          1.24310        1.26210       -0.05930
C          2.04430        0.14340        0.03580
C          1.39070       -1.06890        0.10230
C          0.01070       -1.14880        0.07440
Br         3.95640        0.27150        0.07290
H         -2.73380        0.79720        0.18960
H         -2.62970       -0.89970        0.72670
H         -2.60620       -0.58610       -1.01660
H         -0.75550        2.08230       -0.16320
H          1.75850        2.22700       -0.11220
H          1.99730       -1.95160        0.17670
H         -0.48840       -2.11950        0.12810

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

