%nproc=16
%mem=2GB
%chk=mol_277_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.25470        0.14030        0.00840
C         -0.77220        0.10140        0.00730
C         -0.16730       -1.12990       -0.13680
C          1.22320       -1.25110       -0.14810
C          2.02880       -0.14570       -0.01570
C          1.41410        1.08230        0.12800
C          0.03090        1.20400        0.13930
Br         3.92600       -0.24180       -0.02230
H         -2.66030        1.13950       -0.22100
H         -2.67930       -0.16350        0.99440
H         -2.61220       -0.62830       -0.71440
H         -0.75640       -2.04620       -0.24620
H          1.71350       -2.21550       -0.26110
H          2.02610        1.98690        0.23600
H         -0.46010        2.16770        0.25220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

