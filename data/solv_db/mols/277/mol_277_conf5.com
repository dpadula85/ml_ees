%nproc=16
%mem=2GB
%chk=mol_277_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.26320       -0.18560        0.03390
C          0.77830       -0.05480        0.01460
C         -0.02790       -1.17020       -0.06870
C         -1.40820       -1.08400       -0.08880
C         -2.03250        0.14310       -0.02500
C         -1.22600        1.26260        0.05860
C          0.15230        1.17540        0.07860
Br        -3.94390        0.29780       -0.05060
H          2.66660       -0.53480       -0.94210
H          2.68870        0.84470        0.18030
H          2.60780       -0.87080        0.83010
H          0.45340       -2.13590       -0.11910
H         -2.03350       -1.98360       -0.15550
H         -1.72080        2.22830        0.10870
H          0.78250        2.06770        0.14490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_277_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

