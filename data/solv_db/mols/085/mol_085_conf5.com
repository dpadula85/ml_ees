%nproc=16
%mem=2GB
%chk=mol_085_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.20200       -0.21890        0.01120
C         -0.00050        0.73220       -0.01000
C         -1.22400       -0.16160       -0.14490
Cl        -1.18820       -1.22930        1.30150
H          1.12510       -0.89330        0.88210
H          2.13140        0.37110       -0.01860
H          1.12690       -0.85610       -0.88950
H         -0.07740        1.26420        0.95730
H          0.10630        1.43770       -0.84580
H         -1.04310       -0.84290       -1.02270
H         -2.15830        0.39690       -0.22060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

