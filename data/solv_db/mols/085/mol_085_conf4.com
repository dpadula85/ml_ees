%nproc=16
%mem=2GB
%chk=mol_085_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24970        0.22050       -0.11520
C         -0.02420       -0.61270        0.10220
C          1.25330        0.17560       -0.04610
Cl         1.36450        1.50880        1.09650
H         -1.27380        1.13400        0.49470
H         -1.34150        0.41410       -1.20940
H         -2.12650       -0.39980        0.20140
H         -0.00910       -1.42090       -0.65820
H         -0.03540       -1.10880        1.09420
H          1.33150        0.58840       -1.07240
H          2.11090       -0.49920        0.11220

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

