%nproc=16
%mem=2GB
%chk=mol_085_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23550        0.16350        0.13360
C          0.01580       -0.63550       -0.13770
C          1.25380        0.22540        0.02770
Cl         1.26960        1.60070       -1.06710
H         -2.08370       -0.56030        0.15720
H         -1.19090        0.71720        1.07220
H         -1.44680        0.89780       -0.67970
H         -0.07450       -1.07380       -1.13680
H          0.07370       -1.40740        0.67320
H          1.28510        0.51380        1.10270
H          2.13330       -0.44140       -0.14550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

