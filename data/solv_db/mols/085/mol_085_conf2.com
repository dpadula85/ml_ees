%nproc=16
%mem=2GB
%chk=mol_085_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.21210       -0.28050        0.33930
C         -0.11180        0.53860       -0.33370
C          1.18040       -0.20720       -0.08830
Cl         2.57840        0.60750       -0.80690
H         -1.97430        0.41650        0.76560
H         -0.83300       -0.92000        1.13370
H         -1.65890       -0.91760       -0.46700
H         -0.09320        1.51690        0.18790
H         -0.32190        0.71630       -1.38740
H          1.34320       -0.19480        1.02610
H          1.10330       -1.27590       -0.36930

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

