%nproc=16
%mem=2GB
%chk=mol_085_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.24890        0.15540        0.35660
C         -0.08220       -0.57930       -0.31240
C          1.15480        0.27930       -0.04620
Cl         2.58540       -0.47490       -0.77910
H         -1.19380        0.00610        1.46220
H         -2.22010       -0.18500       -0.04410
H         -1.10150        1.22240        0.07910
H          0.08380       -1.56690        0.13360
H         -0.30350       -0.58020       -1.38310
H          1.29890        0.43410        1.04160
H          1.02720        1.28890       -0.50840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_085_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

