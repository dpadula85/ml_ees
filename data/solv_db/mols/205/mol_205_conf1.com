%nproc=16
%mem=2GB
%chk=mol_205_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.26910        1.23670       -0.10400
C         -0.95080        0.16200       -0.80930
C         -0.72840       -1.16470       -0.20540
S          0.96350       -0.98170        0.42670
C          0.87750        0.78970        0.69880
O          1.17900       -1.84300        1.86190
O          2.10240       -1.39340       -0.76310
H          0.11370        1.98020       -0.86640
H         -0.94950        1.86250        0.54600
H         -2.05790        0.39020       -0.83980
H         -0.66130        0.10270       -1.90050
H         -0.77240       -1.97310       -0.96120
H         -1.37290       -1.38570        0.68610
H          1.84830        1.29400        0.42680
H          0.67790        0.92370        1.80330

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

