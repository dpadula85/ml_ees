%nproc=16
%mem=2GB
%chk=mol_205_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.00230       -0.76470       -0.08990
C         -1.03200        0.64670        0.26180
C          0.23160        1.34970       -0.13910
S          1.40830        0.01100        0.25910
C          0.34470       -1.25170       -0.46180
O          1.49740       -0.10550        1.92690
O          2.86140        0.19180       -0.51930
H         -1.77390       -0.96490       -0.89130
H         -1.35880       -1.39630        0.77610
H         -1.91910        1.21650       -0.12320
H         -1.10780        0.76000        1.38430
H          0.43520        2.23320        0.46080
H          0.29030        1.50400       -1.23080
H          0.50150       -1.19860       -1.56410
H          0.62350       -2.23120       -0.04950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

