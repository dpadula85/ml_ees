%nproc=16
%mem=2GB
%chk=mol_205_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.17490        0.20250       -0.35890
C         -0.69220       -1.04330        0.27870
C          0.79660       -1.06460        0.30230
S          1.28120        0.60390       -0.23890
C         -0.33260        1.35560        0.14920
O          2.51770        1.23610        0.66770
O          1.53660        0.63260       -1.89990
H         -2.23900        0.34120       -0.14970
H         -0.95350        0.12400       -1.45690
H         -1.00860       -1.90580       -0.38130
H         -1.15390       -1.26060        1.26260
H          1.26190       -1.83770       -0.31370
H          1.11160       -1.13260        1.38300
H         -0.51770        2.23940       -0.47060
H         -0.43310        1.50920        1.22660

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

