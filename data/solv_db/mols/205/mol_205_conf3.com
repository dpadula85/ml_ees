%nproc=16
%mem=2GB
%chk=mol_205_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.23410       -0.16180       -0.15620
C         -0.22580       -1.18220       -0.38490
C          1.15470       -0.71720       -0.19600
S          0.99460        1.04490        0.13050
C         -0.74170        0.96600        0.66060
O          1.17430        1.91660       -1.29610
O          2.00550        1.55690        1.37280
H         -2.15710       -0.61410        0.30770
H         -1.61360        0.26120       -1.13270
H         -0.45530       -2.09150        0.24640
H         -0.32440       -1.57400       -1.44120
H          1.56720       -1.18690        0.74360
H          1.83060       -0.86810       -1.05910
H         -1.26710        1.90520        0.44830
H         -0.70790        0.74490        1.75640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

