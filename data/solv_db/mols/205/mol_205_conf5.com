%nproc=16
%mem=2GB
%chk=mol_205_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.67810       -0.84120       -0.64670
C          0.58000       -1.13580        0.01990
C          1.26710        0.06260        0.52980
S          0.11300        1.41350        0.26600
C         -1.32310        0.38820       -0.14840
O         -0.15460        2.31240        1.66890
O          0.60360        2.35940       -1.04490
H         -0.58150       -0.78100       -1.76630
H         -1.39660       -1.69830       -0.48610
H          0.44040       -1.88310        0.83010
H          1.27060       -1.64430       -0.71700
H          1.46750       -0.02600        1.63740
H          2.25570        0.27940        0.07070
H         -1.88240        0.93850       -0.94180
H         -1.98150        0.25580        0.72820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_205_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

