%nproc=16
%mem=2GB
%chk=mol_039_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.06940       -0.90560       -0.22410
C          1.29810        0.49960        0.34350
O          0.46940        1.34980       -0.32010
C         -0.86580        1.09600       -0.22890
C         -1.26970       -0.32670       -0.26180
N         -0.26870       -1.25960        0.16320
H          1.77860       -1.56980        0.27900
H          1.12460       -0.91870       -1.32170
H          1.06040        0.44240        1.43160
H          2.34030        0.82440        0.19780
H         -1.27880        1.55380        0.70680
H         -1.35740        1.60990       -1.10830
H         -1.64510       -0.62730       -1.28300
H         -2.16690       -0.49650        0.40560
H         -0.28850       -1.27170        1.22070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

