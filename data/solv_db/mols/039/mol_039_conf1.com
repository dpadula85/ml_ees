%nproc=16
%mem=2GB
%chk=mol_039_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.65200        1.00700       -0.54250
C         -1.41930       -0.22320       -0.19310
O         -0.79820       -1.07940        0.66670
C          0.52510       -1.32360        0.41930
C          1.28830       -0.03370        0.43570
N          0.76390        0.85130       -0.56240
H         -0.96350        1.32790       -1.57950
H         -0.93170        1.88070        0.10760
H         -1.79950       -0.71770       -1.13220
H         -2.36470        0.13200        0.31620
H          0.91330       -1.93630        1.27960
H          0.72960       -1.92650       -0.48250
H          2.36520       -0.20110        0.19630
H          1.15820        0.43530        1.43980
H          1.18510        1.80740       -0.36900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

