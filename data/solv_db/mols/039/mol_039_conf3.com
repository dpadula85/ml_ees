%nproc=16
%mem=2GB
%chk=mol_039_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.12710       -0.60070       -0.24340
C         -1.12710        0.69220        0.52580
O          0.12490        1.03640        1.01710
C          1.02050        0.95810       -0.05170
C          1.28010       -0.53280       -0.25980
N          0.10320       -1.30940       -0.05660
H         -1.33090       -0.44680       -1.33340
H         -1.95240       -1.28330        0.10060
H         -1.79950        0.58030        1.38280
H         -1.43800        1.49110       -0.17310
H          0.51940        1.35330       -0.96590
H          1.96140        1.50530        0.16640
H          2.04900       -0.86520        0.45690
H          1.59330       -0.63280       -1.32330
H          0.12300       -1.94560        0.75740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

