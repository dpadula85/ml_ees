%nproc=16
%mem=2GB
%chk=mol_039_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.27520       -0.28710       -0.12500
C         -0.93620        1.13560        0.10540
O          0.33020        1.46700        0.33830
C          1.33290        0.59130        0.25770
C          1.03110       -0.81380       -0.10020
N         -0.22860       -1.03310       -0.73440
H         -1.64840       -0.74930        0.83660
H         -2.17110       -0.35880       -0.81060
H         -1.58090        1.50170        0.96010
H         -1.32230        1.72910       -0.77580
H          1.88850        0.57680        1.24300
H          2.10560        0.99330       -0.46450
H          1.82150       -1.21730       -0.80040
H          1.11390       -1.49270        0.79880
H         -0.46090       -2.04270       -0.72900

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

