%nproc=16
%mem=2GB
%chk=mol_039_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.34290        0.07420        0.02400
C         -0.53910        1.31850       -0.03170
O          0.76510        1.22820       -0.28730
C          1.41380        0.05970       -0.25560
C          0.62500       -1.18540       -0.11880
N         -0.63390       -1.06250        0.52690
H         -2.21960        0.23960        0.71950
H         -1.83400       -0.13310       -0.96820
H         -1.02190        1.99240       -0.80080
H         -0.69020        1.88160        0.93600
H          2.02110       -0.03610       -1.20610
H          2.20590        0.10460        0.54950
H          0.46840       -1.67610       -1.11860
H          1.23220       -1.92690        0.47760
H         -0.44980       -0.87870        1.55370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_039_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

