%nproc=16
%mem=2GB
%chk=mol_090_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.93610        0.27310       -0.08790
C          1.67400       -0.44620        0.15410
O          1.55720       -1.08580        1.18530
C          0.51020       -0.45450       -0.77560
C          0.02020        0.95170       -0.97020
C         -1.04690        1.36530       -0.01490
C         -2.19980        0.40180       -0.06670
C         -1.69450       -0.94850        0.34280
C         -0.51310       -1.44130       -0.39250
H          2.83000        0.98950       -0.94080
H          3.19580        0.85530        0.82890
H          3.79680       -0.40000       -0.30330
H          0.93460       -0.76540       -1.77690
H          0.89620        1.62510       -0.80820
H         -0.28630        1.13510       -2.03920
H         -0.63120        1.33680        1.01020
H         -1.40410        2.36180       -0.29500
H         -2.94480        0.70750        0.70080
H         -2.72300        0.38390       -1.03230
H         -2.53360       -1.66330        0.21240
H         -1.49810       -0.90920        1.44870
H         -0.01790       -2.21630        0.26820
H         -0.85780       -2.05620       -1.27710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

