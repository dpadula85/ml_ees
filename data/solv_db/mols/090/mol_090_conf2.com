%nproc=16
%mem=2GB
%chk=mol_090_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.89740       -0.82510       -0.27090
C          1.86630        0.24640       -0.35230
O          2.14790        1.22390       -1.02740
C          0.55450        0.11330        0.36580
C         -0.16210       -1.08040       -0.11750
C         -1.63280       -1.01530       -0.20730
C         -2.19660        0.17820        0.56580
C         -1.55970        1.37750       -0.15210
C         -0.13950        1.41080        0.31920
H          2.90860       -1.44550       -1.20910
H          3.93510       -0.38630       -0.22290
H          2.78360       -1.46710        0.61530
H          0.86000       -0.07860        1.43900
H          0.24300       -1.35140       -1.13510
H          0.09300       -1.98530        0.51220
H         -1.95670       -0.87760       -1.25050
H         -2.14750       -1.92500        0.16660
H         -1.80410        0.09700        1.60150
H         -3.28710        0.20940        0.50380
H         -2.04300        2.31890        0.11860
H         -1.59420        1.18090       -1.24760
H         -0.16380        1.92670        1.32500
H          0.39790        2.15460       -0.33980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

