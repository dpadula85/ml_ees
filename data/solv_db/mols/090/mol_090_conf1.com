%nproc=16
%mem=2GB
%chk=mol_090_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.03210        0.50670       -0.08920
C          1.89510       -0.33090        0.37550
O          2.08630       -1.07630        1.31720
C          0.55180       -0.30260       -0.24670
C         -0.35350       -1.26070        0.45840
C         -1.76060       -1.22710       -0.05220
C         -2.28020        0.19690        0.13890
C         -1.42440        1.06360       -0.79290
C         -0.03370        1.09330       -0.25640
H          3.96470       -0.09260       -0.21340
H          2.83210        0.96760       -1.08210
H          3.20330        1.37420        0.60380
H          0.68840       -0.66940       -1.30060
H         -0.31760       -1.11540        1.55600
H          0.03300       -2.29970        0.27870
H         -2.35940       -1.95000        0.53470
H         -1.76860       -1.41980       -1.13280
H         -2.01050        0.51910        1.17620
H         -3.34120        0.26250       -0.08400
H         -1.39950        0.49580       -1.76260
H         -1.85890        2.05040       -0.94040
H          0.58480        1.72180       -0.93960
H          0.03660        1.49270        0.76170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

