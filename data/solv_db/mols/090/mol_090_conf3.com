%nproc=16
%mem=2GB
%chk=mol_090_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.03310       -0.26460        0.08140
C         -1.83530        0.49480        0.56890
O         -1.97370        1.21620        1.54270
C         -0.53770        0.33610       -0.15250
C         -0.14680       -1.09390       -0.09390
C          1.32340       -1.35060       -0.07720
C          2.16510       -0.21020       -0.53490
C          1.84510        1.10400        0.06910
C          0.42140        1.29410        0.45830
H         -2.67980       -1.12740       -0.48180
H         -3.69150       -0.57180        0.93590
H         -3.59760        0.47510       -0.56070
H         -0.69380        0.62530       -1.22890
H         -0.57300       -1.58800        0.82930
H         -0.65650       -1.62010       -0.94020
H          1.68510       -1.73650        0.91130
H          1.51760       -2.20140       -0.79060
H          2.15230       -0.12730       -1.65340
H          3.22910       -0.46110       -0.26360
H          2.17010        1.90100       -0.66320
H          2.49130        1.28130        0.97400
H          0.31190        1.29690        1.56610
H          0.10640        2.32830        0.13800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

