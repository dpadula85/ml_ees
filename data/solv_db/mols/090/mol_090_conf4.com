%nproc=16
%mem=2GB
%chk=mol_090_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.99930       -0.48920        0.08300
C          1.87470        0.33760       -0.45490
O          2.02720        0.99600       -1.45570
C          0.55720        0.36660        0.22340
C          0.03050       -1.05160        0.23000
C         -1.41400       -1.07640        0.68040
C         -2.19280       -0.23860       -0.32320
C         -1.78150        1.20720       -0.04520
C         -0.33430        1.32750       -0.47830
H          3.22020       -0.08490        1.11190
H          3.86240       -0.43140       -0.58910
H          2.60470       -1.51830        0.21620
H          0.68020        0.64910        1.29490
H          0.66960       -1.62160        0.93550
H          0.06530       -1.52370       -0.77340
H         -1.49080       -0.57630        1.67880
H         -1.78430       -2.09430        0.76160
H         -1.85350       -0.44870       -1.36630
H         -3.26890       -0.38720       -0.25250
H         -1.82210        1.34870        1.03440
H         -2.41500        1.86320       -0.64150
H         -0.28630        1.09540       -1.57360
H          0.05220        2.35060       -0.29630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_090_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

