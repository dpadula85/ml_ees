%nproc=16
%mem=2GB
%chk=mol_405_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48640       -0.32110       -0.30650
C         -2.77060       -1.21380        0.46030
C         -1.40650       -1.06030        0.56140
C         -0.72540       -0.04580       -0.07580
C         -1.45510        0.84000       -0.83970
C         -2.84240        0.70870       -0.96070
Cl        -0.59670        2.13230       -1.65350
C          0.71900        0.07130        0.06470
C          1.29900        0.81570        1.07020
C          2.66640        0.94890        1.23380
C          3.46380        0.29000        0.32690
C          2.93970       -0.46700       -0.69640
C          1.54980       -0.57430       -0.82460
Cl         0.90930       -1.53600       -2.12560
Cl         0.23490        1.62770        2.18910
Cl        -0.48040       -2.18140        1.52820
H         -4.55520       -0.39620       -0.41870
H         -3.30980       -2.00930        0.95920
H         -3.41570        1.41960       -1.57180
H          3.11220        1.53930        2.03140
H          4.53330        0.37090        0.42300
H          3.61670       -0.95920       -1.37480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_405_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_405_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_405_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

