%nproc=16
%mem=2GB
%chk=mol_405_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.46930       -0.04110        0.22830
C         -2.82680       -0.68390       -0.83530
C         -1.44920       -0.66740       -0.91150
C         -0.71190       -0.01090        0.07150
C         -1.34340        0.62720        1.12700
C         -2.72100        0.61190        1.20500
Cl        -0.36580        1.43670        2.33410
C          0.72640        0.00580       -0.00820
C          1.34250        1.04530       -0.69800
C          2.72050        1.06060       -0.77390
C          3.43460        0.04360       -0.16230
C          2.82550       -0.98850        0.52240
C          1.44710       -1.00330        0.59810
Cl         0.60120       -2.29920        1.46200
Cl         0.42260        2.32550       -1.46670
Cl        -0.67930       -1.48320       -2.25840
H         -4.53890       -0.05600        0.28390
H         -3.37450       -1.18910       -1.59330
H         -3.17440        1.11810        2.03860
H          3.21410        1.87580       -1.31550
H          4.53280        0.05500       -0.22230
H          3.38730       -1.78290        0.99990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_405_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_405_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_405_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

