%nproc=16
%mem=2GB
%chk=mol_176_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.00360       -0.02420        0.13410
Cl         1.47930       -0.76860       -0.50230
Cl        -1.45880       -0.85310       -0.43320
Cl        -0.07870        1.65710       -0.44430
H          0.06180       -0.01120        1.24560

--Link1--
%nproc=16
%mem=2GB
%chk=mol_176_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_176_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_176_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

