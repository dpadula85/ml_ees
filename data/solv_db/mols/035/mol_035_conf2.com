%nproc=16
%mem=2GB
%chk=mol_035_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.61380       -0.24940        0.05360
C         -3.25650        0.28810       -0.31850
C         -2.13190       -0.49020        0.32140
C         -0.83230        0.16840       -0.15000
C          0.28120       -0.61040        0.51030
C          1.60290       -0.07420        0.13640
O          1.96980        0.02650       -1.05690
O          2.51970        0.37290        1.10200
C          3.76850        0.89700        0.75470
C          4.65440       -0.05600        0.02830
H         -5.37030        0.48490       -0.30510
H         -4.78210       -1.18250       -0.52080
H         -4.71810       -0.40200        1.14660
H         -3.14160        0.24560       -1.41330
H         -3.16300        1.34690        0.03140
H         -2.18110       -0.47130        1.42790
H         -2.10720       -1.53310       -0.10320
H         -0.78500        0.18560       -1.25400
H         -0.78870        1.21920        0.24910
H          0.24780       -1.67920        0.16930
H          0.12340       -0.58940        1.58760
H          4.27970        1.20990        1.69500
H          3.61080        1.79990        0.11110
H          5.71350        0.14700        0.34350
H          4.66510        0.06410       -1.08720
H          4.43500       -1.11830        0.31240

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

