%nproc=16
%mem=2GB
%chk=mol_035_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.36560        1.38720       -0.28410
C         -2.58000        0.09790       -0.35770
C         -1.79300       -0.09330        0.93300
C         -1.00290       -1.36020        0.90290
C         -0.00360       -1.47750       -0.18510
C          1.06640       -0.45810       -0.18500
O          1.14810        0.43570        0.69660
O          2.05400       -0.41980       -1.15900
C          3.09660        0.49470       -1.24090
C          4.03860        0.51810       -0.08350
H         -3.31830        1.93780       -1.24680
H         -4.45530        1.14230       -0.13590
H         -2.98550        2.07430        0.49060
H         -3.26000       -0.75580       -0.45740
H         -1.89290        0.15420       -1.22160
H         -2.59380       -0.13740        1.73130
H         -1.20440        0.82200        1.09640
H         -0.56930       -1.54700        1.92300
H         -1.73680       -2.20820        0.77850
H          0.41080       -2.51460       -0.19640
H         -0.52500       -1.37040       -1.18170
H          2.74280        1.54970       -1.40960
H          3.69820        0.25390       -2.16630
H          4.71490       -0.38210       -0.16140
H          3.58580        0.46450        0.90560
H          4.73040        1.39220       -0.18530

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

