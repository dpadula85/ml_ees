%nproc=16
%mem=2GB
%chk=mol_035_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.02770       -0.07560        0.03630
C         -2.93730       -0.14600       -0.98640
C         -1.62560        0.42740       -0.49070
C         -1.10140       -0.27340        0.70520
C          0.19040        0.27420        1.22520
C          1.32470        0.17910        0.29120
O          1.19690       -0.39800       -0.80440
O          2.57460        0.72130        0.57910
C          3.66430        0.64370       -0.29670
C          4.09370       -0.74810       -0.63790
H         -3.90540       -0.89160        0.80480
H         -4.98100       -0.32990       -0.48980
H         -4.15670        0.90950        0.50360
H         -2.82480       -1.18020       -1.31020
H         -3.24470        0.48350       -1.85260
H         -1.77130        1.52100       -0.24630
H         -0.94870        0.42200       -1.35490
H         -0.88410       -1.34560        0.40390
H         -1.84020       -0.38160        1.52520
H         -0.00400        1.35540        1.50760
H          0.41280       -0.22150        2.19990
H          3.47850        1.17890       -1.27130
H          4.53770        1.18900        0.15150
H          3.61070       -1.12820       -1.54740
H          3.96900       -1.46550        0.19560
H          5.19960       -0.71990       -0.84710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

