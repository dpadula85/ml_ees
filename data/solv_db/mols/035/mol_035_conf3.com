%nproc=16
%mem=2GB
%chk=mol_035_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.42920       -0.19770       -0.43000
C         -3.02300       -0.74230       -0.50930
C         -2.08500        0.36010       -0.06230
C         -0.63360       -0.10370       -0.11060
C          0.21190        1.06200        0.35090
C          1.65820        0.73020        0.34720
O          2.51470        1.57940        0.70140
O          2.14680       -0.50830       -0.03820
C          3.49010       -0.87250       -0.06090
C          4.34400       -0.02780       -0.96670
H         -4.52710        0.59610        0.36140
H         -5.17450       -0.96840       -0.16590
H         -4.69580        0.31610       -1.39420
H         -2.92300       -1.61070        0.17720
H         -2.77810       -1.07480       -1.53040
H         -2.33180        0.66570        0.96360
H         -2.18730        1.22060       -0.74660
H         -0.50310       -0.95580        0.58090
H         -0.37000       -0.41470       -1.14330
H          0.04880        1.88870       -0.36340
H         -0.15520        1.36970        1.34940
H          3.95570       -0.82310        0.95450
H          3.55050       -1.91710       -0.43970
H          4.54160        0.98660       -0.56970
H          3.99730       -0.02960       -2.01640
H          5.35680       -0.52850       -0.99510

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

