%nproc=16
%mem=2GB
%chk=mol_035_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.52610       -0.34580        0.43760
C         -2.39040        0.61200        0.76150
C         -1.67600        1.04720       -0.51210
C         -1.10720       -0.16470       -1.21300
C         -0.12310       -0.93420       -0.39950
C          1.06960       -0.20900        0.01550
O          1.36170        0.98190       -0.24880
O          2.01330       -0.89930        0.79980
C          3.19470       -0.27130        1.24270
C          4.03220        0.20040        0.08300
H         -4.25060       -0.33180        1.28900
H         -4.01480        0.00450       -0.49640
H         -3.10580       -1.33640        0.26080
H         -2.84380        1.49820        1.24620
H         -1.64370        0.15340        1.41830
H         -0.93230        1.80610       -0.25610
H         -2.48270        1.46020       -1.16430
H         -1.98840       -0.79220       -1.49430
H         -0.69040        0.20400       -2.17440
H          0.20650       -1.80710       -1.02470
H         -0.64600       -1.36920        0.48610
H          3.77300       -1.02830        1.81150
H          2.97240        0.56100        1.95380
H          3.57870       -0.09780       -0.88360
H          5.06710       -0.23080        0.10290
H          4.15210        1.28880        0.13370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_035_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

