%nproc=16
%mem=2GB
%chk=mol_199_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.44240       -1.26030        0.46860
O         -2.07880       -1.53220        0.29920
C         -1.10260       -0.59720        0.03070
C         -1.44960        0.71200       -0.26490
C         -0.41260        1.65410       -0.33460
C          0.91520        1.17780       -0.22960
C          1.22060       -0.09940        0.08800
C          0.18460       -1.00720        0.17180
O          0.43000       -2.37720        0.30060
O          2.48990       -0.42250        0.41300
C          3.64420       -0.78050       -0.23960
Cl         2.17650        2.37280       -0.64500
C         -0.71740        3.08540       -0.23750
O         -1.54540        3.65140       -0.89340
H         -4.04680       -2.22560        0.43570
H         -3.82580       -0.40100       -0.03730
H         -3.49930       -1.00240        1.64990
H         -2.37390        0.95190       -0.64450
H          1.44640       -2.72770        0.38750
H          3.54660       -1.75290       -0.78860
H          4.40900       -0.98400        0.56140
H          4.10320       -0.04180       -0.91710
H         -0.07160        3.60650        0.42540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

