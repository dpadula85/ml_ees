%nproc=16
%mem=2GB
%chk=mol_199_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48020       -0.03850       -0.24750
O         -2.45160       -0.76380       -0.96210
C         -1.19620       -0.11280       -0.88160
C         -1.08590        1.22280       -0.50100
C          0.05020        1.74240        0.06860
C          1.06610        0.90150        0.40740
C          0.99510       -0.48080        0.08130
C         -0.10240       -0.87740       -0.60480
O         -0.21340       -2.15110       -1.11360
O          1.88420       -1.31470        0.59380
C          2.88060       -2.09630        0.87070
Cl         2.69570        1.42700        0.83270
C          0.46200        3.13240       -0.09230
O         -0.22120        3.96340       -0.74890
H         -4.47060       -0.57350       -0.32840
H         -3.11630       -0.46590        0.88410
H         -3.35280        0.98780       -0.03320
H         -1.80400        2.00920       -0.88090
H          0.48470       -2.87640       -0.81480
H          3.86290       -1.92770        0.33060
H          3.14550       -2.04490        1.91870
H          2.66800       -3.20330        0.75110
H          1.29940        3.54060        0.47020

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

