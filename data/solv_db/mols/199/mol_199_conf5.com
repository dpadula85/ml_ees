%nproc=16
%mem=2GB
%chk=mol_199_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.52160       -0.73090        0.47510
O          2.29590       -0.99870       -0.05670
C          1.15370       -0.25010       -0.17390
C          1.00780        1.05260        0.23960
C         -0.17890        1.72000        0.19190
C         -1.33030        1.11210       -0.25730
C         -1.23700       -0.18720       -0.69510
C         -0.01710       -0.82820       -0.67830
O          0.11070       -2.13530       -1.19760
O         -2.40080       -0.88860       -1.08210
C         -3.04250       -1.67970       -0.05020
Cl        -2.88210        1.94640       -0.26440
C         -0.20760        3.19300        0.47410
O         -0.00490        3.58450        1.63020
H          3.81660       -1.54120        1.23950
H          4.31570       -0.71510       -0.27630
H          3.58170        0.23530        1.06070
H          1.91920        1.51770        0.66200
H         -0.67270       -2.63830       -1.57840
H         -4.10640       -1.82170       -0.22520
H         -2.44800       -2.64800       -0.03310
H         -2.80670       -1.18240        0.93750
H         -0.38810        3.88390       -0.34200

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

