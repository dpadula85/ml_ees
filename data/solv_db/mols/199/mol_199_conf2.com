%nproc=16
%mem=2GB
%chk=mol_199_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.54150       -3.28660       -0.04650
O         -1.69520       -1.87660       -0.46320
C         -0.51160       -1.04960       -0.41690
C          0.75320       -1.41100       -0.17290
C          1.71240       -0.48620        0.22090
C          1.24440        0.83690        0.47680
C         -0.06690        1.26420        0.19330
C         -0.89230        0.30500       -0.30390
O         -2.13970        0.60190       -0.66410
O         -0.44350        2.48570        0.43710
C         -0.63770        3.59560       -0.34470
Cl         2.47600        1.84220        1.22880
C          3.00770       -0.87600        0.63540
O          3.99200       -0.90470       -0.19090
H         -2.59110       -3.58010        0.31570
H         -0.88650       -3.34170        0.89050
H         -1.14260       -3.96070       -0.80900
H          1.01670       -2.45360       -0.35280
H         -2.53490        1.52990       -0.66340
H         -0.41540        4.52270        0.22990
H         -1.73350        3.77670       -0.62770
H         -0.09520        3.72310       -1.26570
H          3.12530       -1.25710        1.69320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

