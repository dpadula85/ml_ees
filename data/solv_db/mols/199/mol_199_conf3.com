%nproc=16
%mem=2GB
%chk=mol_199_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.59290       -0.83160        0.39740
O         -2.22630       -1.09480        0.33090
C         -1.16070       -0.28520        0.08200
C         -1.16520        1.04440       -0.15720
C         -0.00250        1.79370       -0.03630
C          1.22570        1.19450        0.35790
C          1.23580       -0.15940        0.55220
C          0.03000       -0.86700        0.33500
O          0.33080       -2.26420        0.22750
O          2.31410       -0.86220        0.86250
C          3.30020       -1.60700        0.29050
Cl         2.69110        2.06910       -0.09180
C         -0.04560        3.23170       -0.26870
O         -0.98210        3.96860        0.14690
H         -3.79650       -0.20060       -0.49960
H         -3.95150       -0.37230        1.35000
H         -4.12560       -1.79810        0.31230
H         -2.12060        1.60920       -0.18370
H          0.42910       -2.48800       -0.78010
H          3.91980       -2.07680        1.14550
H          3.97070       -1.11640       -0.41860
H          2.95170       -2.58430       -0.21050
H          0.77070        3.69660       -0.82420

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_199_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

