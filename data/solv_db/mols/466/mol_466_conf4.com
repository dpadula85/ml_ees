%nproc=16
%mem=2GB
%chk=mol_466_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.40470       -0.15420        0.02210
C          2.24700       -0.89710       -0.59740
C          1.04140        0.02090       -0.77950
C          0.59470        0.56970        0.58220
C         -0.58040        1.46590        0.41320
C         -1.79120        0.86770       -0.18270
C         -2.41290       -0.27280        0.52520
N         -3.58720       -0.68080       -0.26210
H          3.46770       -0.38090        1.12020
H          4.39060       -0.47980       -0.39110
H          3.29230        0.93400       -0.16820
H          2.56660       -1.17340       -1.63090
H          1.97800       -1.81460       -0.04170
H          0.23680       -0.50420       -1.29900
H          1.42110        0.90380       -1.33740
H          0.45960       -0.28490        1.23360
H          1.46130        1.15490        0.95460
H         -0.82530        1.90440        1.40920
H         -0.26510        2.32070       -0.22320
H         -1.56870        0.53690       -1.23710
H         -2.55580        1.66610       -0.29450
H         -2.83490        0.02500        1.52110
H         -1.80260       -1.17930        0.60400
H         -4.32970        0.07870       -0.22680
H         -4.00800       -1.47480        0.28620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

