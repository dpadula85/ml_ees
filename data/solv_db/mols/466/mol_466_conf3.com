%nproc=16
%mem=2GB
%chk=mol_466_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.07390        0.57110       -0.19340
C         -2.02840        0.08050        0.75350
C         -1.33260       -1.16460        0.26840
C         -0.63170       -0.98910       -1.03690
C          0.43810        0.06800       -1.01290
C          1.51220       -0.25850        0.00430
C          2.52910        0.86500       -0.05780
N          3.61090        0.65240        0.88340
H         -3.49010       -0.29040       -0.75210
H         -2.65830        1.32050       -0.91550
H         -3.91090        1.11010        0.32260
H         -1.33850        0.90660        0.97400
H         -2.52950       -0.17750        1.71280
H         -2.11940       -1.93610        0.12470
H         -0.64880       -1.56830        1.04140
H         -1.34980       -0.80420       -1.85620
H         -0.13030       -1.95100       -1.27820
H          0.05250        1.07060       -0.75740
H          0.89520        0.14490       -2.01430
H          0.99970       -0.21050        1.00850
H          1.97480       -1.24700       -0.17030
H          2.87200        0.93140       -1.09630
H          2.00430        1.80100        0.22310
H          4.34340        1.38340        0.64910
H          4.01000       -0.30830        0.81490

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

