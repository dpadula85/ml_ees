%nproc=16
%mem=2GB
%chk=mol_466_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.08930       -0.31190       -0.24620
C         -2.89110        0.56200        0.05340
C         -1.65480       -0.28530       -0.15630
C         -0.43880        0.57430        0.13950
C          0.76990       -0.31040       -0.08230
C          2.06210        0.42480        0.17910
C          3.19740       -0.56260       -0.07550
N          4.44030        0.17420        0.18460
H         -4.38340       -0.12230       -1.29240
H         -4.89510       -0.01450        0.44820
H         -3.86120       -1.39030       -0.15800
H         -2.89960        0.96290        1.07620
H         -2.81640        1.42550       -0.63860
H         -1.63810       -0.66460       -1.19520
H         -1.64850       -1.16370        0.53500
H         -0.42960        1.43430       -0.54780
H         -0.51370        0.88930        1.20590
H          0.64380       -1.18600        0.58910
H          0.70610       -0.67050       -1.12990
H          2.12770        1.26120       -0.54190
H          2.06990        0.81410        1.19720
H          3.15780       -0.88960       -1.12900
H          3.16770       -1.39670        0.65110
H          5.24540       -0.32950       -0.26680
H          4.57160        0.29660        1.20070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

