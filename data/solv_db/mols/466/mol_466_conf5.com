%nproc=16
%mem=2GB
%chk=mol_466_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.89460        1.41350        0.05730
C          2.01450        0.24800        0.45270
C          1.48050       -0.47280       -0.74190
C          0.60270       -1.63750       -0.38410
C         -0.59980       -1.21980        0.42840
C         -1.45290       -0.23600       -0.33440
C         -2.67320        0.17630        0.50110
N         -3.45080        1.10860       -0.26290
H          2.39080        2.39310        0.19080
H          3.87800        1.39930        0.57660
H          3.12040        1.35420       -1.04490
H          2.58470       -0.37610        1.14090
H          1.15960        0.69830        1.02290
H          0.94130        0.22550       -1.40940
H          2.34240       -0.86490       -1.35210
H          1.16370       -2.44840        0.12980
H          0.18180       -2.11280       -1.31870
H         -1.19020       -2.13830        0.64620
H         -0.25880       -0.72250        1.35880
H         -1.81280       -0.66840       -1.26940
H         -0.82760        0.66520       -0.52600
H         -2.33100        0.61500        1.47350
H         -3.28570       -0.71240        0.72460
H         -2.98700        1.47940       -1.10240
H         -3.88490        1.83340        0.34320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

