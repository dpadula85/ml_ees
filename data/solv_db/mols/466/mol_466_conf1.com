%nproc=16
%mem=2GB
%chk=mol_466_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.83640        0.40490       -0.41240
C          2.44370        0.42620        0.19450
C          1.58530       -0.63300       -0.45920
C          0.20020       -0.66230        0.10450
C         -0.55180        0.63920       -0.06670
C         -1.93680        0.54200        0.51880
C         -2.78380       -0.52460       -0.11340
N         -4.07800       -0.48240        0.55790
H          4.46300       -0.26220        0.23310
H          3.80950        0.05830       -1.46980
H          4.21950        1.44540       -0.42090
H          1.99100        1.43690       -0.02880
H          2.48290        0.28110        1.28460
H          1.59130       -0.52900       -1.56120
H          2.06050       -1.61710       -0.20360
H          0.31630       -0.83240        1.21210
H         -0.39540       -1.51790       -0.24940
H         -0.64830        0.87570       -1.14930
H          0.01410        1.46970        0.38470
H         -2.41000        1.54380        0.47900
H         -1.84160        0.28320        1.61420
H         -2.39580       -1.54590        0.07620
H         -2.87220       -0.37460       -1.19490
H         -4.28110        0.53030        0.72570
H         -4.81890       -0.95520       -0.00050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_466_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

