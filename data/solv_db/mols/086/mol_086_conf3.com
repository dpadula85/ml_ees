%nproc=16
%mem=2GB
%chk=mol_086_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.53190       -1.31960        0.13300
C          1.23330       -0.61320        0.05860
C          0.06480       -1.38150       -0.01400
C         -1.14840       -0.74820       -0.08380
C         -1.18030        0.61970       -0.08020
C         -0.04030        1.41270       -0.00910
C          1.17320        0.76500        0.06060
C         -0.12910        2.89740       -0.00810
C         -2.40180       -1.57410       -0.16190
H          3.34410       -0.79180       -0.38880
H          2.86050       -1.47740        1.19520
H          2.43440       -2.31990       -0.32700
H          0.08260       -2.45530       -0.01720
H         -2.14310        1.09830       -0.13570
H          2.09420        1.32860        0.11750
H          0.54850        3.30890        0.78580
H         -1.16130        3.20560        0.32880
H          0.04670        3.34500       -0.98910
H         -2.42530       -2.27110        0.71110
H         -2.48950       -2.11030       -1.10740
H         -3.29500       -0.91890       -0.06840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

