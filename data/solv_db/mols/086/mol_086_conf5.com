%nproc=16
%mem=2GB
%chk=mol_086_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.78990       -0.62850       -0.40090
C          1.34000       -0.27590       -0.19640
C          0.37660       -1.26530       -0.21350
C         -0.98070       -0.98140       -0.02780
C         -1.35730        0.32420        0.17700
C         -0.39990        1.30550        0.19370
C          0.94590        1.02490        0.00980
C         -0.83010        2.70690        0.41630
C         -1.96180       -2.09620       -0.05860
H          3.37770        0.22380       -0.73840
H          2.83790       -1.50810       -1.08600
H          3.17100       -1.00700        0.59010
H          0.72200       -2.27970       -0.37850
H         -2.38120        0.60070        0.32480
H          1.71040        1.79200        0.02120
H         -1.52790        2.71070        1.30380
H         -1.36620        3.13210       -0.44850
H          0.03180        3.35910        0.68210
H         -1.75000       -2.75220        0.82140
H         -1.76560       -2.66770       -1.00160
H         -2.98270       -1.71780        0.00990

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

