%nproc=16
%mem=2GB
%chk=mol_086_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.89600       -0.09480        0.16860
C          1.39670       -0.03650        0.07730
C          0.73300        1.17450        0.04540
C         -0.63190        1.21800       -0.03780
C         -1.37560        0.04880       -0.09170
C         -0.74110       -1.18180       -0.06180
C          0.63450       -1.18290        0.02240
C         -1.53810       -2.45610       -0.11930
C         -1.36390        2.50080       -0.07330
H          3.25060        0.93470        0.37510
H          3.21320       -0.80640        0.97860
H          3.35100       -0.39220       -0.79690
H          1.31780        2.10530        0.08800
H         -2.44740        0.06370       -0.15720
H          1.15480       -2.13290        0.04750
H         -2.22590       -2.41010        0.77270
H         -2.06550       -2.57620       -1.06480
H         -0.83540       -3.29660        0.02920
H         -1.94970        2.56830       -1.01730
H         -2.10160        2.58930        0.75990
H         -0.67150        3.36300        0.05550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

