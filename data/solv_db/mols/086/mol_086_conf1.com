%nproc=16
%mem=2GB
%chk=mol_086_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.48440        2.82560        0.10110
C         -0.19190        1.36440        0.05610
C          1.07480        0.84840        0.03690
C          1.32300       -0.50560       -0.00440
C          0.21920       -1.34180       -0.02620
C         -1.07010       -0.85610       -0.00790
C         -1.27490        0.51480        0.03380
C         -2.24370       -1.79900       -0.03280
C          2.70820       -1.02120       -0.02370
H         -1.22140        3.07730       -0.71710
H          0.44580        3.37300       -0.17090
H         -0.88440        3.10560        1.08950
H          1.91520        1.53440        0.05500
H          0.41000       -2.42140       -0.05910
H         -2.29530        0.87670        0.04760
H         -2.10660       -2.50830       -0.87280
H         -3.16080       -1.20410       -0.26280
H         -2.41190       -2.28150        0.92980
H          3.31310       -0.71240        0.83510
H          3.25960       -0.72860       -0.95930
H          2.67640       -2.14000       -0.04800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

