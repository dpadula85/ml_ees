%nproc=16
%mem=2GB
%chk=mol_086_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.00560       -2.04690        0.07080
C         -0.94890       -0.98980        0.03210
C          0.38690       -1.29750        0.03970
C          1.35970       -0.32440        0.00400
C          1.00090        1.00750       -0.04110
C         -0.33930        1.35310       -0.05000
C         -1.29830        0.35780       -0.01360
C         -0.78600        2.75600       -0.09740
C          2.78490       -0.74400        0.01530
H         -2.92220       -1.62130       -0.42450
H         -2.30330       -2.29070        1.10860
H         -1.68510       -2.95420       -0.48200
H          0.63480       -2.36770        0.07600
H          1.75880        1.80160       -0.07020
H         -2.35190        0.61400       -0.02010
H         -1.81160        2.81260       -0.49900
H         -0.74520        3.18520        0.93440
H         -0.06680        3.32230       -0.74790
H          3.42250        0.00640       -0.49460
H          2.85150       -1.73490       -0.44420
H          3.06410       -0.84520        1.10400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_086_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

