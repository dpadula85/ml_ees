%nproc=16
%mem=2GB
%chk=mol_414_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.52770        0.09390        0.08700
C         -2.76290        1.23920        0.02510
C         -1.39180        1.20460       -0.00610
C         -0.74190       -0.00610        0.02250
C         -1.49880       -1.14890        0.08530
C         -2.87410       -1.11680        0.11780
C          0.71630       -0.04050       -0.05690
C          1.45330       -0.77390        0.86520
C          2.83620       -0.78650        0.86440
C          3.54580       -0.06370       -0.06190
C          2.82980        0.66570       -0.98190
C          1.43880        0.67870       -0.98400
Cl         0.55340        1.55650       -2.21970
Cl         3.73090       -1.73180        2.05390
H         -4.60790        0.18500        0.10910
H         -3.23830        2.21400       -0.00130
H         -0.85020        2.13150       -0.01870
H         -0.99360       -2.11160        0.11010
H         -3.49110       -2.00780        0.16740
H          0.87800       -1.34650        1.59920
H          4.62390       -0.07310       -0.06100
H          3.37200        1.23790       -1.71520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

