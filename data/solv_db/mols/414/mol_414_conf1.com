%nproc=16
%mem=2GB
%chk=mol_414_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.51890       -0.00640       -0.07380
C         -2.81110        1.16770        0.05470
C         -1.43330        1.14470        0.07690
C         -0.74690       -0.04720       -0.02640
C         -1.44980       -1.22890       -0.15670
C         -2.81590       -1.17790       -0.17690
C          0.72200       -0.03190        0.04500
C          1.48260       -0.68810       -0.91790
C          2.85560       -0.64730       -0.91380
C          3.53540        0.05680        0.06050
C          2.80770        0.71600        1.02920
C          1.41230        0.66330        1.01160
Cl         0.48960        1.44180        2.29280
Cl         3.77530       -1.49960       -2.16130
H         -4.59480        0.02470       -0.09010
H         -3.35460        2.09960        0.13700
H         -0.95560        2.11950        0.14710
H         -0.88940       -2.12940       -0.23570
H         -3.38290       -2.09580       -0.27840
H          0.92730       -1.23360       -1.67160
H          4.62710        0.07700        0.04400
H          3.31830        1.27480        1.80370

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

