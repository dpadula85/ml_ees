%nproc=16
%mem=2GB
%chk=mol_414_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.48650        0.02910        0.41690
C         -2.85030       -1.13130        0.02600
C         -1.48670       -1.11320       -0.15700
C         -0.71830        0.04220        0.03920
C         -1.42260        1.15900        0.42980
C         -2.78700        1.19350        0.62580
C          0.72000        0.00180       -0.12800
C          1.46280       -0.98660        0.47620
C          2.85020       -1.00820        0.36040
C          3.53490       -0.05180       -0.35650
C          2.78630        0.93310       -0.95920
C          1.40720        0.94790       -0.84070
Cl         0.50100        2.18890       -1.67380
Cl         3.79300       -2.27940        1.13890
H         -4.56030        0.05720        0.57140
H         -3.43920       -2.04290       -0.13120
H         -0.96720       -2.00930       -0.46270
H         -0.87350        2.06460        0.62150
H         -3.29860        2.11530        0.93720
H          0.93630       -1.74360        1.04200
H          4.61650       -0.07750       -0.43950
H          3.28220        1.71110       -1.53670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_414_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

