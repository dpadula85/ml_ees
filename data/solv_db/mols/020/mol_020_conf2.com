%nproc=16
%mem=2GB
%chk=mol_020_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.03050       -2.73120       -0.67460
C         -1.03760       -2.07830        0.16630
O         -0.55460       -1.10250        1.01650
P          0.12440        0.21160        0.23480
O          0.70060        1.12400        1.30830
O          1.39770       -0.21010       -0.77830
C          2.41800        0.73030       -0.72320
C          3.62640        0.23310        0.04120
O         -1.04720        1.14370       -0.57320
C         -2.10140        1.32910        0.33460
C         -3.14290        2.24960       -0.24670
H          0.02840       -2.26120       -1.69090
H         -0.27130       -3.79440       -0.85610
H          1.02850       -2.70910       -0.19940
H         -1.83450       -1.73780       -0.50970
H         -1.45450       -2.88120        0.82640
H          2.01560        1.60880       -0.18700
H          2.75240        1.06810       -1.71510
H          4.21280       -0.51300       -0.53620
H          3.35960       -0.23420        1.00250
H          4.30440        1.11600        0.15700
H         -1.76730        1.71570        1.30590
H         -2.54780        0.33030        0.48560
H         -2.69990        3.22240       -0.55360
H         -3.93540        2.37450        0.52160
H         -3.60500        1.79590       -1.15350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

