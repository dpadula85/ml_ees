%nproc=16
%mem=2GB
%chk=mol_020_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.47590       -1.63970       -0.00750
C          2.09480       -1.42820       -0.62450
O          1.54710       -0.39240        0.09790
P         -0.06550       -0.09540       -0.16730
O         -0.40330        0.13100       -1.63070
O         -0.42360        1.37920        0.60660
C          0.07480        2.37990       -0.24210
C          0.08160        3.66990        0.56910
O         -1.09130       -1.25490        0.50500
C         -2.40790       -0.83170        0.26280
C         -3.25030       -2.08110        0.11180
H          3.92370       -2.59990       -0.30170
H          3.30590       -1.61680        1.09930
H          4.12010       -0.76610       -0.23160
H          2.26260       -1.09610       -1.68110
H          1.56260       -2.38370       -0.55070
H         -0.60160        2.43590       -1.11280
H          1.10490        2.06260       -0.52160
H         -0.90500        3.69880        1.09820
H          0.22980        4.54110       -0.09160
H          0.92080        3.55290        1.29670
H         -2.79500       -0.19320        1.08280
H         -2.43820       -0.22700       -0.67830
H         -4.28320       -1.95010        0.46590
H         -2.81080       -2.94950        0.66190
H         -3.22910       -2.34530       -0.96710

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

