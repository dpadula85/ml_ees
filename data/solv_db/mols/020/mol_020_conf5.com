%nproc=16
%mem=2GB
%chk=mol_020_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.87220        3.40670       -0.20570
C          0.18370        2.11540        0.13450
O          0.02930        1.27040       -0.95070
P          0.01910       -0.31880       -0.34230
O         -0.11200       -0.31720        1.17470
O         -1.42450       -1.02430       -0.90620
C         -2.44170       -0.40720       -0.16060
C         -3.81470       -0.82740       -0.59620
O          1.37590       -1.17300       -0.83430
C          1.99770       -1.68160        0.31220
C          3.23440       -2.47350        0.00200
H          1.08140        3.92730        0.76200
H          0.21500        4.08970       -0.77340
H          1.85910        3.25620       -0.68200
H          0.78190        1.57760        0.89880
H         -0.82270        2.35810        0.51370
H         -2.31510        0.67830       -0.31760
H         -2.30830       -0.61090        0.93070
H         -4.15190       -1.61910        0.11970
H         -4.51300        0.04750       -0.59900
H         -3.75310       -1.25590       -1.60620
H          2.20110       -0.79490        0.96020
H          1.21450       -2.27980        0.83350
H          4.10070       -1.79380       -0.03760
H          3.09100       -2.94990       -0.99450
H          3.39990       -3.20000        0.81430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

