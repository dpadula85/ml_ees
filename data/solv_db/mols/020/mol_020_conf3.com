%nproc=16
%mem=2GB
%chk=mol_020_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.50460       -0.12830       -0.18920
C          2.34680        0.49500       -0.95760
O          1.40810       -0.47950       -1.26590
P          0.00210       -0.26400       -0.33080
O          0.30410       -0.30200        1.13670
O         -1.09540       -1.48730       -0.77090
C         -0.97020       -2.60850        0.04650
C         -2.15400       -2.69700        0.97130
O         -0.68940        1.19130       -0.82480
C         -0.76550        2.12910        0.18970
C         -2.00810        2.96700       -0.08600
H          4.05880        0.72420        0.26710
H          3.12350       -0.83470        0.56190
H          4.14710       -0.61630       -0.96470
H          2.73140        0.88990       -1.92950
H          1.94480        1.29590       -0.30280
H         -0.03020       -2.56610        0.64050
H         -0.95580       -3.49060       -0.64250
H         -3.09080       -2.79470        0.40110
H         -2.06620       -3.59090        1.62230
H         -2.12810       -1.81980        1.64000
H          0.11020        2.80260        0.06140
H         -0.84380        1.69090        1.19410
H         -2.86370        2.53350        0.46380
H         -1.77550        4.01220        0.24140
H         -2.24480        2.94820       -1.17320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

