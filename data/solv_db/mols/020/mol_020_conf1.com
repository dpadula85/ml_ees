%nproc=16
%mem=2GB
%chk=mol_020_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.21520       -1.61000        0.72050
C          2.32750       -0.39800        0.60050
O          1.00020       -0.76240        0.87290
P          0.05990        0.05330       -0.28770
O          0.22460       -0.71330       -1.58270
O          0.67080        1.60580       -0.39120
C         -0.21700        2.53790       -0.88950
C         -1.06970        3.06410        0.23740
O         -1.55930       -0.01650        0.13820
C         -2.21290       -1.06650       -0.47420
C         -2.11020       -2.36810        0.28730
H          2.66180       -2.47530        0.31990
H          4.11530       -1.46720        0.07750
H          3.50160       -1.74450        1.77580
H          2.44370       -0.02310       -0.44760
H          2.65860        0.42540        1.26110
H         -0.79310        2.20070       -1.76030
H          0.40270        3.40030       -1.24540
H         -0.96170        4.16880        0.37680
H         -0.80000        2.61720        1.22430
H         -2.12160        2.86570       -0.02300
H         -1.74030       -1.23650       -1.45630
H         -3.29080       -0.88410       -0.64070
H         -2.59110       -2.29200        1.27440
H         -1.09140       -2.77620        0.32330
H         -2.72280       -3.10520       -0.29150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_020_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

