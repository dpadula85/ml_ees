%nproc=16
%mem=2GB
%chk=mol_261_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.07040       -0.98340       -0.04810
C         -0.20900       -1.48690       -0.07550
C         -1.25260       -0.56960       -0.03080
C         -1.01970        0.77710        0.03770
C          0.27780        1.24040        0.06310
C          1.33140        0.36200        0.02040
Cl         2.97550        1.00470        0.05560
Cl         0.56560        2.99180        0.15220
Cl        -2.32430        1.95780        0.09520
Cl        -2.92680       -1.10160       -0.06030
H          1.92750       -1.65080       -0.08040
H         -0.41570       -2.54150       -0.12910

--Link1--
%nproc=16
%mem=2GB
%chk=mol_261_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_261_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_261_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

