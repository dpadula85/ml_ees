%nproc=16
%mem=2GB
%chk=mol_022_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.04330        0.23890       -0.04740
O         -2.63460        0.43010       -0.05200
C         -1.72170       -0.59560        0.01080
O         -2.16250       -1.77170        0.07620
C         -0.27220       -0.34980        0.00330
C          0.21820        0.93590       -0.06810
C          1.54740        1.21920       -0.07890
C          2.46250        0.19530       -0.01680
C          2.00460       -1.11840        0.05640
C          0.63860       -1.38140        0.06590
N          3.84130        0.50820       -0.02960
O          4.19080        1.69830       -0.09630
O          4.76260       -0.48340        0.03040
H         -4.46690        0.77450        0.82820
H         -4.33630       -0.81070       -0.12080
H         -4.42230        0.79040       -0.94780
H         -0.53130        1.72740       -0.11630
H          1.86930        2.26180       -0.13710
H          2.75420       -1.87870        0.10290
H          0.30160       -2.39030        0.12210

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

