%nproc=16
%mem=2GB
%chk=mol_022_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.01400        0.22930        0.18740
O          2.62590        0.37930        0.17760
C          1.71470       -0.66060        0.27820
O          2.17670       -1.82270        0.38590
C          0.28010       -0.38330        0.25660
C         -0.66620       -1.37940        0.35310
C         -2.03010       -1.06650        0.32800
C         -2.45200        0.23830        0.20670
C         -1.51020        1.21340        0.11210
C         -0.18210        0.91020        0.13650
N         -3.85500        0.57190        0.17960
O         -4.22180        1.75780        0.06910
O         -4.75820       -0.44290        0.27770
H          4.45190        1.24890        0.37390
H          4.35200       -0.38820        1.05180
H          4.44250       -0.11660       -0.76900
H         -0.36950       -2.40940        0.44890
H         -2.79380       -1.83270        0.40240
H         -1.80080        2.25540        0.01500
H          0.58190        1.69790        0.06030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

