%nproc=16
%mem=2GB
%chk=mol_022_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.04780        0.36180        0.13400
O          2.66130        0.26660        0.38310
C          1.70290        0.07470       -0.58080
O          2.08990       -0.02350       -1.77510
C          0.27370       -0.01650       -0.26110
C         -0.17010        0.08870        1.04080
C         -1.50560        0.00770        1.37780
C         -2.46140       -0.18500        0.40430
C         -2.02860       -0.29210       -0.90620
C         -0.68330       -0.20910       -1.23270
N         -3.83720       -0.26950        0.74190
O         -4.17170       -0.16650        1.93630
O         -4.81470       -0.46040       -0.19630
H          4.43550       -0.50010       -0.41040
H          4.55500        0.38250        1.14090
H          4.28930        1.34140       -0.32620
H          0.57150        0.24040        1.81390
H         -1.84170        0.09190        2.40450
H         -2.76340       -0.44130       -1.66450
H         -0.34900       -0.29160       -2.24350

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

