%nproc=16
%mem=2GB
%chk=mol_022_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.01370        0.23430       -0.01980
O          2.62980        0.33990       -0.15960
C          1.70760       -0.50970        0.40310
O          2.12270       -1.46340        1.10240
C          0.26730       -0.28960        0.17900
C         -0.14840        0.75550       -0.58540
C         -1.50090        0.99220       -0.81730
C         -2.44660        0.16470       -0.27150
C         -2.00800       -0.90470        0.51140
C         -0.66310       -1.12130        0.72860
N         -3.82740        0.37620       -0.48650
O         -4.19330        1.33170       -1.18500
O         -4.73660       -0.46350        0.06990
H          4.35350        1.14380        0.55570
H          4.49980        0.29860       -1.01380
H          4.33580       -0.64560        0.56710
H          0.58890        1.41710       -1.02270
H         -1.83150        1.83200       -1.43110
H         -2.78920       -1.54080        0.92840
H         -0.37430       -1.94740        1.33120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

