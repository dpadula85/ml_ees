%nproc=16
%mem=2GB
%chk=mol_022_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.04750        0.33350        0.03010
O         -2.64130        0.49620        0.04210
C         -1.73450       -0.50160       -0.17940
O         -2.16000       -1.66050       -0.41370
C         -0.29190       -0.30650       -0.16120
C          0.27640        0.93070        0.08720
C          1.61400        1.14760        0.11150
C          2.48110        0.11370       -0.11680
C          1.94590       -1.14220       -0.36950
C          0.59320       -1.33990       -0.38970
N          3.87000        0.33120       -0.09300
O          4.65980       -0.60290       -0.29920
O          4.35940        1.57490        0.15790
H         -4.29880       -0.46200       -0.71350
H         -4.48050        0.16640        1.01850
H         -4.45460        1.29020       -0.41010
H         -0.45160        1.73840        0.26640
H          1.95160        2.16920        0.31850
H          2.63680       -1.95300       -0.54870
H          0.17250       -2.32340       -0.58760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_022_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

