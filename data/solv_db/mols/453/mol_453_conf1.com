%nproc=16
%mem=2GB
%chk=mol_453_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.51610        0.07890        0.38480
C          0.73360        1.32780        0.19330
C         -0.30240        1.12090       -0.87240
C         -1.22150       -0.01660       -0.51740
C         -0.53050       -1.27800       -0.16400
C          0.70950       -1.14400        0.63360
O         -2.11330        0.44230        0.44040
H          2.15770       -0.07340       -0.51720
H          2.22720        0.19180        1.25380
H          0.29330        1.72920        1.12000
H          1.44100        2.11770       -0.17290
H          0.15230        1.03160       -1.87020
H         -0.93840        2.04700       -0.88460
H         -1.84070       -0.21570       -1.44070
H         -0.35330       -1.87590       -1.10010
H         -1.25520       -1.90880        0.42990
H          1.36480       -2.03050        0.38100
H          0.49840       -1.28370        1.73440
H         -2.53850       -0.26080        0.96840

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

