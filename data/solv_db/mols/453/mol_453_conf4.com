%nproc=16
%mem=2GB
%chk=mol_453_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.51410       -0.54090       -0.02050
C         -1.24910        0.90670        0.18490
C          0.12450        1.15660        0.81110
C          1.10120        0.65060       -0.26220
C          0.97260       -0.86650       -0.18520
C         -0.38510       -1.33870       -0.55290
O          2.38530        1.05790       -0.03430
H         -2.36390       -0.62200       -0.76050
H         -1.84170       -0.96460        0.97320
H         -1.23220        1.48910       -0.77750
H         -2.00830        1.34080        0.85540
H          0.24470        2.21780        1.01400
H          0.15930        0.52070        1.71570
H          0.71870        1.04650       -1.21780
H          1.13460       -1.11380        0.90450
H          1.73890       -1.31290       -0.81920
H         -0.49060       -2.38670       -0.14100
H         -0.46020       -1.49130       -1.66990
H          2.96540        0.25060       -0.01780

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

