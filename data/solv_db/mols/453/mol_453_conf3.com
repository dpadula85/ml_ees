%nproc=16
%mem=2GB
%chk=mol_453_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.47050       -0.52930        0.05490
C          1.11320        0.94640       -0.01470
C         -0.11490        1.03670       -0.92080
C         -1.25170        0.37270       -0.14510
C         -0.91460       -1.07340        0.07740
C          0.38760       -1.27260        0.81400
O         -1.48490        1.07600        1.00750
H          2.47560       -0.70520        0.46950
H          1.40170       -0.90500       -1.00250
H          1.94690        1.50980       -0.46030
H          0.86500        1.26630        1.02200
H         -0.33940        2.07800       -1.17320
H          0.05000        0.38410       -1.80870
H         -2.13440        0.40640       -0.81310
H         -0.75810       -1.52080       -0.94620
H         -1.75770       -1.62250        0.51330
H          0.34700       -1.01360        1.87270
H          0.63210       -2.36840        0.71270
H         -1.93380        1.93440        0.74070

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

