%nproc=16
%mem=2GB
%chk=mol_453_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.49190       -0.02920        0.46250
C          0.94640       -1.10350       -0.48980
C         -0.49890       -1.27310       -0.04620
C         -1.25900       -0.04660       -0.36450
C         -0.61610        1.23790       -0.05790
C          0.85760        1.25380        0.05110
O         -2.48620       -0.11880        0.33620
H          2.58420        0.05410        0.29200
H          1.25770       -0.33740        1.49100
H          0.98890       -0.64520       -1.50110
H          1.52290       -2.02400       -0.42390
H         -0.97690       -2.15460       -0.47130
H         -0.46160       -1.38750        1.07200
H         -1.56340       -0.10320       -1.45000
H         -0.90730        1.97880       -0.86080
H         -1.05870        1.68640        0.88010
H          1.16800        2.03170        0.80820
H          1.30080        1.53230       -0.95070
H         -2.29030       -0.55170        1.22310

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

