%nproc=16
%mem=2GB
%chk=mol_453_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.58960       -0.11070        0.23150
C          0.90760        1.24280        0.00950
C         -0.46330        1.09390        0.67330
C         -1.22420        0.08780       -0.19130
C         -0.55170       -1.24610        0.01370
C          0.83860       -1.13420       -0.60790
O         -2.56080        0.08990        0.13230
H          1.40000       -0.41460        1.29910
H          2.65750       -0.07990        0.01830
H          1.48930        2.06680        0.43030
H          0.72650        1.35480       -1.08970
H         -0.26750        0.70600        1.68440
H         -1.00640        2.03900        0.72960
H         -1.01710        0.38860       -1.24550
H         -1.11220       -2.09460       -0.38890
H         -0.44230       -1.37800        1.11790
H          1.32980       -2.11750       -0.46790
H          0.76690       -0.85750       -1.66130
H         -3.06020        0.36360       -0.68720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_453_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

