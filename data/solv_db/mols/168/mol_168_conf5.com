%nproc=16
%mem=2GB
%chk=mol_168_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.47000       -0.01200        0.14660
C         -0.98200        0.00390        0.09500
C         -0.31490        1.11590       -0.36330
C          1.07470        1.12100       -0.40770
C          1.83330        0.03980       -0.00500
C          1.16070       -1.07590        0.45480
C         -0.23770       -1.07450        0.49700
O          3.22230        0.09370       -0.06860
H         -2.85030       -0.60230        1.01620
H         -2.87690       -0.48780       -0.79280
H         -2.89800        0.99510        0.17890
H         -0.90380        1.97530       -0.68390
H          1.58790        2.00660       -0.77210
H          1.72920       -1.94340        0.77920
H         -0.77080       -1.93880        0.85370
H          3.69620       -0.21640       -0.92800

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

