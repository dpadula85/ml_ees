%nproc=16
%mem=2GB
%chk=mol_168_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.47110       -0.26120       -0.01200
C          0.99590       -0.07060       -0.00720
C          0.26630       -0.29650        1.14120
C         -1.10130       -0.12520        1.16450
C         -1.80690        0.27800        0.04890
C         -1.07590        0.50400       -1.10080
C          0.29530        0.33270       -1.12560
O         -3.19000        0.44870        0.08150
H          2.83640       -0.17490       -1.06510
H          2.93730        0.54610        0.59120
H          2.76590       -1.26540        0.35160
H          0.84000       -0.61560        2.01740
H         -1.64760       -0.30710        2.06960
H         -1.62670        0.82100       -1.97930
H          0.87320        0.51170       -2.03550
H         -3.83280       -0.32590       -0.14060

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

