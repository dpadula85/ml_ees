%nproc=16
%mem=2GB
%chk=mol_168_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.48630       -0.08200        0.00900
C         -1.00000       -0.04810        0.02700
C         -0.25490       -1.06810       -0.52810
C          1.12440       -1.03940       -0.51290
C          1.83050        0.00120        0.05310
C          1.08280        1.02070        0.60790
C         -0.29870        0.99610        0.59490
O          3.21770        0.05340        0.08110
H         -2.88640       -0.88400        0.66960
H         -2.86640       -0.23550       -1.00640
H         -2.85070        0.88000        0.45120
H         -0.81020       -1.88930       -0.97460
H          1.70990       -1.84330       -0.95030
H          1.59280        1.86710        1.06820
H         -0.89680        1.80570        1.03560
H          3.79230        0.46560       -0.62520

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

