%nproc=16
%mem=2GB
%chk=mol_168_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.43270       -0.38560       -0.28570
C          0.98240       -0.14100       -0.07940
C          0.20340       -0.93920        0.73340
C         -1.14320       -0.69240        0.91000
C         -1.77890        0.36060        0.28600
C         -0.99940        1.15800       -0.52650
C          0.34790        0.91400       -0.70530
O         -3.14090        0.60490        0.46850
H          2.77430        0.02260       -1.25930
H          3.05220       -0.00110        0.55470
H          2.65350       -1.48740       -0.33600
H          0.69230       -1.77650        1.23380
H         -1.75380       -1.31830        1.54730
H         -1.48630        1.98480       -1.01960
H          0.96920        1.54110       -1.34660
H         -3.80550        0.15560       -0.17540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

