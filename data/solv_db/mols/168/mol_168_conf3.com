%nproc=16
%mem=2GB
%chk=mol_168_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.48500       -0.01380        0.21020
C          0.99890       -0.00320        0.05290
C          0.27040       -1.16800       -0.06720
C         -1.10130       -1.16470       -0.21270
C         -1.82030        0.01360       -0.24580
C         -1.08600        1.17520       -0.12520
C          0.28390        1.17400        0.02020
O         -3.20450        0.02840       -0.39220
H          2.78220        0.40970        1.20810
H          2.84310       -1.04790        0.08830
H          2.92990        0.58110       -0.60300
H          0.84150       -2.10910       -0.04110
H         -1.66510       -2.08580       -0.30640
H         -1.64100        2.11320       -0.14970
H          0.86540        2.09650        0.11580
H         -3.78210        0.00080        0.44790

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_168_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

