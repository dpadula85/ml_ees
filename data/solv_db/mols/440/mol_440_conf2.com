%nproc=16
%mem=2GB
%chk=mol_440_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.00650        0.07570       -0.57340
C         -2.08460       -0.66480       -1.24560
C         -0.76960       -0.67570       -0.82280
C         -0.33110        0.04100        0.27030
C         -1.27960        0.78790        0.94220
C         -2.61520        0.81570        0.53500
O          0.95430        0.06300        0.72680
C          2.05370       -0.61010        0.20260
C          3.13040        0.43260       -0.13150
F          2.66700        1.34520       -1.05480
F          4.24250       -0.22260       -0.64760
F          2.65250       -1.46500        1.12670
H         -4.03760        0.07010       -0.92370
H         -2.37080       -1.24950       -2.11420
H         -0.07850       -1.26500       -1.37110
H         -1.03140        1.38150        1.80920
H         -3.37000        1.38330        1.03450
H          1.84200       -1.23060       -0.68820
H          3.43280        0.98730        0.77260

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

