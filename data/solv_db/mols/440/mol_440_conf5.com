%nproc=16
%mem=2GB
%chk=mol_440_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.98520       -0.59240       -0.23950
C         -2.69210        0.76520       -0.15870
C         -1.39890        1.19110        0.07570
C         -0.36750        0.29500        0.23550
C         -0.64060       -1.04260        0.15840
C         -1.95060       -1.47180       -0.07890
O          0.88180        0.74900        0.46300
C          2.07790        0.08660        0.65650
C          3.07500        0.43170       -0.44570
F          3.27380        1.80750       -0.38920
F          4.26730       -0.19830       -0.21360
F          2.67360        0.34610        1.87460
H         -3.99050       -0.93780       -0.42210
H         -3.52160        1.45540       -0.28840
H         -1.21010        2.26510        0.13210
H          0.12450       -1.80540        0.27450
H         -2.14860       -2.52410       -0.13640
H          1.92990       -1.00300        0.60630
H          2.60190        0.18280       -1.39970

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

