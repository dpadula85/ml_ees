%nproc=16
%mem=2GB
%chk=mol_440_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.92010       -0.21080        0.81910
C         -2.03980        0.78200        1.13250
C         -0.75720        0.83380        0.57530
C         -0.34510       -0.13140       -0.31760
C         -1.25970       -1.13000       -0.61930
C         -2.52120       -1.17690       -0.06820
O          0.87430       -0.18820       -0.92430
C          1.96400        0.64940       -0.82080
C          3.09690       -0.14150       -0.15440
F          2.70820       -0.56810        1.10020
F          4.24990        0.62720       -0.07280
F          2.42170        1.13970       -2.02460
H         -3.90790       -0.23920        1.25650
H         -2.34740        1.54820        1.83210
H         -0.06770        1.61340        0.82180
H         -0.98790       -1.91330       -1.31510
H         -3.24020       -1.95890       -0.30340
H          1.73060        1.47110       -0.11510
H          3.34840       -1.00650       -0.80180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

