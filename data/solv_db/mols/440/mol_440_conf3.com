%nproc=16
%mem=2GB
%chk=mol_440_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.02300        0.22220        0.28980
C          2.43600        1.45460        0.14480
C          1.08180        1.52180       -0.09540
C          0.30270        0.36650       -0.19300
C          0.90140       -0.86120       -0.04620
C          2.27560       -0.91450        0.19700
O         -1.04030        0.47770       -0.43280
C         -1.99370       -0.51010       -0.56670
C         -3.16130       -0.25940        0.37330
F         -4.10400       -1.27550        0.21140
F         -2.74240       -0.24960        1.68680
F         -2.48360       -0.44560       -1.87710
H          4.08710        0.15600        0.47890
H          3.02340        2.36910        0.21700
H          0.56590        2.46090       -0.21790
H          0.37220       -1.78710       -0.10780
H          2.72310       -1.87920        0.30850
H         -1.58520       -1.52160       -0.46070
H         -3.68150        0.67500        0.09000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

