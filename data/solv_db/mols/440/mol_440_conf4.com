%nproc=16
%mem=2GB
%chk=mol_440_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.01270       -0.30890       -0.09860
C         -2.30820        0.69980        0.51870
C         -0.92850        0.78890        0.39330
C         -0.27840       -0.15630       -0.36400
C         -0.99420       -1.18020       -0.99050
C         -2.34920       -1.23560       -0.84470
O          1.07400       -0.13310       -0.53240
C          2.00840        0.76130       -0.02780
C          3.03400       -0.04710        0.75790
F          3.59870       -0.93680       -0.14850
F          4.05220        0.76000        1.21500
F          2.65490        1.33350       -1.13370
H         -4.08570       -0.35140        0.01790
H         -2.84860        1.43400        1.11200
H         -0.42540        1.59830        0.89690
H         -0.45150       -1.92360       -1.59020
H         -2.87810       -2.05660       -1.35330
H          1.55990        1.55510        0.59500
H          2.57810       -0.60140        1.57690

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_440_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

