%nproc=16
%mem=2GB
%chk=mol_056_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.74200        0.50630        0.05890
C          0.04840       -0.74680       -0.21910
S          1.68740       -0.53750        0.57650
H         -1.04970        1.04340       -0.85500
H         -0.18890        1.22590        0.71090
H         -1.66970        0.25570        0.62250
H         -0.44730       -1.63450        0.22800
H          0.24580       -0.87650       -1.31280
H          2.11590        0.76410        0.19000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

