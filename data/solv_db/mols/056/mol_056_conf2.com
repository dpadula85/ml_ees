%nproc=16
%mem=2GB
%chk=mol_056_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.78970       -0.54880       -0.03600
C          0.04540        0.70530        0.14950
S          1.65950        0.45830       -0.67000
H         -0.30110       -1.28630       -0.69490
H         -1.75890       -0.26240       -0.49800
H         -1.03840       -1.03710        0.92490
H         -0.42570        1.58000       -0.37690
H          0.22260        0.93620        1.22420
H          2.38620       -0.54520       -0.02280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

