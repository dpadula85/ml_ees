%nproc=16
%mem=2GB
%chk=mol_056_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.02500        0.04800       -0.29390
C          0.22700        0.04010        0.58510
S          1.50400       -0.81110       -0.41100
H         -1.92170        0.29120        0.28960
H         -0.81380        0.77030       -1.11410
H         -1.11930       -0.95810       -0.75110
H          0.05310       -0.52450        1.52300
H          0.53550        1.08250        0.82230
H          2.56020        0.06170       -0.65000

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

