%nproc=16
%mem=2GB
%chk=mol_056_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.85020       -0.09500       -0.30500
C          0.47470       -0.50780        0.28070
S          1.68920        0.78160        0.00210
H         -1.66330       -0.68540        0.17690
H         -1.09000        0.96610       -0.16190
H         -0.85390       -0.29220       -1.39530
H          0.77030       -1.45920       -0.22590
H          0.43630       -0.72930        1.35500
H          1.08700        2.02110        0.27340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

