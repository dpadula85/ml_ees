%nproc=16
%mem=2GB
%chk=mol_056_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.84840       -0.42140        0.06940
C          0.15370        0.68640       -0.21460
S          1.63390        0.31430        0.75410
H         -1.58440       -0.39210       -0.75520
H         -0.27400       -1.37980        0.09010
H         -1.35850       -0.30770        1.02750
H          0.36280        0.70860       -1.30960
H         -0.29850        1.65350        0.08150
H          2.21340       -0.86180        0.25700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_056_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

