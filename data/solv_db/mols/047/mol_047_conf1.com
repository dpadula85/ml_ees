%nproc=16
%mem=2GB
%chk=mol_047_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.49900        0.81930       -0.92430
C         -3.63150        0.00790        0.18270
C         -2.54930       -0.53670        0.83780
C         -1.26760       -0.26130        0.36920
C         -1.11460        0.54740       -0.73580
C         -2.22700        1.08620       -1.38090
O          0.17500        0.81900       -1.20010
C          1.26540        0.26940       -0.53880
C          2.56890        0.51680       -0.97110
C          3.67530       -0.00970       -0.34230
C          3.46370       -0.81180        0.75810
C          2.19200       -1.08150        1.21820
C          1.08820       -0.54080        0.57010
O         -0.17680       -0.82910        1.05490
H         -4.37960        1.24320       -1.43130
H         -4.65730       -0.19100        0.52880
H         -2.71710       -1.17190        1.70940
H         -2.05170        1.72410       -2.25660
H          2.71500        1.15640       -1.84700
H          4.69430        0.19820       -0.69960
H          4.32910       -1.23730        1.26830
H          2.10440       -1.71690        2.08360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_047_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_047_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_047_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

