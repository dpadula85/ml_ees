%nproc=16
%mem=2GB
%chk=mol_517_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.42190        0.64620        0.31640
C         -2.99300       -0.75160        0.69900
C         -1.49860       -0.77650        1.00150
C         -0.71910       -0.33320       -0.24130
O          0.60820       -0.37470        0.08740
C          1.47190       -0.00740       -0.90920
C          2.87220       -0.13790       -0.29540
C          2.86950        0.68990        0.96030
O          3.85890        0.19910       -1.18030
H         -3.77370        0.67800       -0.75320
H         -2.62660        1.39010        0.51210
H         -4.32880        0.95280        0.90770
H         -3.18510       -1.48200       -0.10840
H         -3.54420       -1.11810        1.58830
H         -1.21860       -1.81240        1.27940
H         -1.27360       -0.07690        1.83090
H         -0.91520       -1.05450       -1.06430
H         -1.03410        0.68050       -0.54770
H          1.30910        1.05760       -1.16750
H          1.38410       -0.59480       -1.83460
H          2.94480       -1.21310        0.01450
H          1.83310        0.80600        1.34330
H          3.54660        0.25900        1.73630
H          3.24200        1.71020        0.74550
H          4.59220        0.66380       -0.68540

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

