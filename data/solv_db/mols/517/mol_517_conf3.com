%nproc=16
%mem=2GB
%chk=mol_517_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.60080        0.57790       -1.04680
C          2.33920        0.41260       -0.20670
C          1.79620       -0.99000       -0.39910
C          0.55010       -1.12290        0.44340
O         -0.43320       -0.20390        0.08120
C         -1.52440       -0.42790        0.92680
C         -2.66330        0.53160        0.61250
C         -3.14880        0.24260       -0.77200
O         -2.24540        1.83890        0.77870
H          4.17620       -0.37430       -0.96920
H          4.18230        1.38360       -0.57810
H          3.34890        0.85130       -2.07330
H          1.60480        1.15950       -0.59620
H          2.52970        0.61640        0.85720
H          2.53650       -1.70710        0.00350
H          1.57350       -1.16880       -1.45760
H          0.79180       -0.94820        1.50630
H          0.11260       -2.13960        0.38470
H         -1.83280       -1.47120        0.79320
H         -1.23260       -0.30910        2.00330
H         -3.47940        0.30170        1.32640
H         -2.29950       -0.15680       -1.36750
H         -3.94420       -0.51070       -0.72460
H         -3.57540        1.16000       -1.22370
H         -2.76360        2.45440        0.16950

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

