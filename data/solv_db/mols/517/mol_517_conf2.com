%nproc=16
%mem=2GB
%chk=mol_517_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.64130        1.28620        0.59040
C          2.78920       -0.20490        0.73700
C          1.46790       -0.88700        0.99090
C          0.49050       -0.63910       -0.16210
O         -0.67050       -1.29270        0.15750
C         -1.69280       -1.20840       -0.74550
C         -2.06860        0.24760       -0.92600
C         -2.48730        0.89120        0.35520
O         -3.11710        0.26650       -1.85500
H          2.02900        1.56690       -0.27290
H          2.25590        1.70520        1.54570
H          3.66940        1.68560        0.38640
H          3.28550       -0.66780       -0.11640
H          3.41590       -0.38320        1.63490
H          0.97320       -0.56550        1.90610
H          1.65940       -1.98210        0.99490
H          0.91870       -0.88460       -1.13220
H          0.31030        0.47560       -0.14560
H         -1.53180       -1.72850       -1.68680
H         -2.58410       -1.70430       -0.26420
H         -1.25240        0.83670       -1.39130
H         -1.73220        1.61160        0.72910
H         -3.44530        1.42920        0.28640
H         -2.61380        0.07260        1.11890
H         -2.71020        0.07330       -2.73550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

