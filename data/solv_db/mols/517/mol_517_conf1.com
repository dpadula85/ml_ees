%nproc=16
%mem=2GB
%chk=mol_517_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.57200        0.35200       -0.04840
C          2.13890        0.08070        0.47820
C          1.56860       -0.95990       -0.40450
C          0.18710       -1.37640       -0.04110
O         -0.78020       -0.50770       -0.14800
C         -0.98830        0.67570        0.37970
C         -2.42860        1.12740       -0.04870
C         -3.38380        0.05350        0.40840
O         -2.48940        1.18710       -1.43990
H          3.84170        1.40760        0.09790
H          4.25740       -0.27850        0.54550
H          3.62150        0.10180       -1.12850
H          1.64290        1.04730        0.52790
H          2.23340       -0.29290        1.53870
H          1.60710       -0.64730       -1.49790
H          2.26950       -1.84680       -0.39640
H         -0.02020       -2.28060       -0.73320
H          0.21420       -1.91970        0.97010
H         -0.89260        0.83200        1.45340
H         -0.37680        1.49480       -0.11670
H         -2.63570        2.11760        0.38680
H         -3.71170        0.25600        1.44460
H         -4.22900        0.03470       -0.30470
H         -2.82620       -0.91730        0.32420
H         -2.39190        0.25880       -1.77130

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

