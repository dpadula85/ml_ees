%nproc=16
%mem=2GB
%chk=mol_517_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.23890        0.35260       -0.35680
C          2.47840       -0.83450       -0.83430
C          1.45160       -1.32990        0.15390
C          0.41560       -0.26250        0.47010
O         -0.21930        0.08390       -0.69290
C         -1.18710        1.04330       -0.56410
C         -2.30870        0.65080        0.36460
C         -3.01920       -0.57050       -0.16810
O         -3.23010        1.68720        0.42350
H          2.67360        1.29020       -0.36810
H          3.63210        0.18010        0.66470
H          4.13070        0.48050       -1.01130
H          3.19770       -1.66530       -0.98920
H          2.04900       -0.60450       -1.82210
H          0.93570       -2.18780       -0.32270
H          1.97600       -1.69560        1.04070
H         -0.27160       -0.72810        1.22300
H          0.89100        0.59290        0.99270
H         -0.81060        2.03640       -0.26190
H         -1.65270        1.18240       -1.57420
H         -1.97220        0.43120        1.38610
H         -2.32510       -1.14910       -0.81960
H         -3.91720       -0.30740       -0.76410
H         -3.32050       -1.24490        0.67040
H         -2.83600        2.56860        0.32880

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_517_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

