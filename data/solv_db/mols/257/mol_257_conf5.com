%nproc=16
%mem=2GB
%chk=mol_257_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.05120        0.40990        1.10140
C          1.38120        0.35620       -0.24560
C          1.64110       -0.97400       -0.88750
C         -0.05310        0.75900       -0.20650
C         -0.63310        0.67380       -1.59160
C         -0.90400        0.04640        0.77900
C         -2.31780        0.62870        0.63020
C         -1.01310       -1.42040        0.60550
H          3.00330        1.01200        1.07730
H          2.32360       -0.61870        1.42160
H          1.35040        0.84430        1.84410
H          1.90470        1.12430       -0.88830
H          0.97700       -1.20730       -1.72230
H          1.68300       -1.76810       -0.10920
H          2.68470       -0.94910       -1.32000
H         -0.06840        1.84350        0.07180
H         -1.41040        1.44860       -1.70790
H          0.19760        0.97630       -2.29540
H         -0.93800       -0.32610       -1.89490
H         -0.60520        0.26470        1.81700
H         -2.89150        0.53600        1.55460
H         -2.28120        1.70560        0.36930
H         -2.81490        0.11980       -0.21460
H         -0.96300       -1.78390       -0.43140
H         -2.02150       -1.74550        0.98880
H         -0.28280       -1.95610        1.25450

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

