%nproc=16
%mem=2GB
%chk=mol_257_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          1.66870       -0.95950        0.84820
C          1.20630        0.41910        0.53200
C          2.30680        1.18920       -0.19340
C         -0.04640        0.52510       -0.26870
C          0.06570       -0.15670       -1.62040
C         -1.30580        0.17450        0.46890
C         -1.39270       -1.19880        1.00720
C         -2.53540        0.43000       -0.41010
H          2.77090       -1.02610        0.61720
H          1.61900       -1.20170        1.93000
H          1.20460       -1.74400        0.24760
H          1.10130        0.95430        1.52200
H          2.73160        0.53610       -0.98290
H          1.80120        2.03730       -0.70150
H          3.06310        1.57770        0.50570
H         -0.18790        1.62250       -0.54420
H          0.92690        0.32420       -2.14230
H         -0.85280        0.14570       -2.19170
H          0.15070       -1.24150       -1.55180
H         -1.41170        0.91130        1.31270
H         -1.16490       -1.96040        0.22320
H         -2.45800       -1.35340        1.36000
H         -0.75410       -1.41350        1.87140
H         -2.60610       -0.36330       -1.20380
H         -3.43240        0.34580        0.24030
H         -2.46860        1.42610       -0.87550

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

