%nproc=16
%mem=2GB
%chk=mol_257_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.53850        1.58180        0.12620
C         -1.28680        0.12710       -0.08860
C         -2.47180       -0.64110        0.47270
C          0.01260       -0.40810        0.39380
C          0.07170       -1.92370        0.09690
C          1.22950        0.14860       -0.25710
C          1.45980        1.61430       -0.12380
C          2.44200       -0.57480        0.32620
H         -1.41430        2.10390       -0.86520
H         -0.94870        2.06550        0.91410
H         -2.64280        1.72840        0.35300
H         -1.30470       -0.07900       -1.20550
H         -2.37210       -0.68730        1.56950
H         -2.64770       -1.59390       -0.02230
H         -3.37230        0.00320        0.27730
H          0.12210       -0.34370        1.50170
H          0.91750       -2.35550        0.66980
H         -0.83890       -2.43170        0.41100
H          0.31960       -2.03900       -0.97330
H          1.21430       -0.07150       -1.36140
H          2.55800        1.76710       -0.39310
H          0.93030        2.23710       -0.85480
H          1.41000        1.98150        0.92670
H          3.32660        0.09590        0.30760
H          2.57460       -1.49620       -0.26470
H          2.25010       -0.80880        1.39770

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

