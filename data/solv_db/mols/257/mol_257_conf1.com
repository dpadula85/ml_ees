%nproc=16
%mem=2GB
%chk=mol_257_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.35230        1.70480        0.00040
C         -1.23510        0.26060        0.37570
C         -2.57680       -0.38270        0.09630
C         -0.08910       -0.45760       -0.18210
C         -0.05960       -1.93710        0.21850
C          1.24630        0.06770        0.25940
C          2.36950       -0.79550       -0.36890
C          1.58150        1.46700       -0.14540
H         -2.45070        1.98770       -0.17260
H         -1.08020        2.39170        0.83800
H         -0.88130        1.98240       -0.95060
H         -1.14380        0.20420        1.51210
H         -2.64780       -0.55940       -0.98890
H         -3.34600        0.37570        0.35340
H         -2.77390       -1.26430        0.71120
H         -0.15540       -0.44680       -1.30000
H         -1.04990       -2.38320        0.24040
H          0.55570       -2.49340       -0.51960
H          0.43250       -2.07990        1.20250
H          1.34280       -0.03810        1.35450
H          3.30420       -0.20410       -0.45530
H          2.08740       -1.15270       -1.35850
H          2.56940       -1.67400        0.28530
H          1.21120        2.19470        0.56810
H          2.72090        1.54680       -0.04480
H          1.42050        1.68520       -1.21390

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

