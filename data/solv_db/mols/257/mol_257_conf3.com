%nproc=16
%mem=2GB
%chk=mol_257_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.35270        0.05850       -0.73340
C         -1.10040       -0.26140        0.10070
C         -1.37410        0.21220        1.48640
C          0.05090        0.33670       -0.59370
C          0.13880       -0.28560       -1.98510
C          1.38090        0.20710        0.04250
C          1.83250       -1.22000        0.24930
C          1.56410        1.05820        1.28460
H         -2.09310        0.34020       -1.75250
H         -3.03030       -0.81680       -0.66310
H         -2.89780        0.91560       -0.27580
H         -0.96370       -1.36480        0.14110
H         -2.48980        0.19200        1.67520
H         -1.08600        1.26350        1.60560
H         -0.96260       -0.46060        2.27300
H         -0.14730        1.41880       -0.75800
H         -0.64920       -1.05020       -2.14190
H          0.05870        0.46750       -2.78700
H          1.10650       -0.81270       -2.13420
H          2.12620        0.63540       -0.69750
H          1.32270       -1.91300       -0.43040
H          2.92320       -1.32960        0.06660
H          1.69870       -1.51760        1.33140
H          1.18140        0.60580        2.19640
H          2.65840        1.26980        1.46380
H          1.10390        2.05090        1.03610

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_257_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

