%nproc=16
%mem=2GB
%chk=mol_135_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.91450       -0.20990       -0.93100
C         -2.97180       -0.21260       -1.93530
C         -1.64490       -0.10570       -1.62670
C         -1.17320        0.00870       -0.32920
C         -2.12020        0.01100        0.67330
C         -3.46370       -0.09660        0.37140
N          0.22840        0.11720       -0.06810
C          1.10580       -0.81930       -0.47660
O          0.63550       -1.81370       -1.10980
C          2.46380       -0.73920       -0.23800
C          2.97440        0.34430        0.45160
C          2.04700        1.28560        0.85590
N          0.73430        1.14780        0.59080
N          4.36250        0.49110        0.73260
Cl         3.58610       -1.96760       -0.77800
H         -4.96720       -0.29420       -1.16780
H         -3.26850       -0.29970       -2.98700
H         -0.88050       -0.10550       -2.40410
H         -1.77750        0.09920        1.69650
H         -4.21630       -0.09600        1.15030
H          2.44210        2.14900        1.40380
H          4.76610        0.34230        1.67770
H          5.05230        0.76390       -0.02820

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

