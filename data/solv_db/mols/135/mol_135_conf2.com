%nproc=16
%mem=2GB
%chk=mol_135_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.91740       -0.49020       -0.18260
C         -3.38880        0.56680        0.51230
C         -2.02010        0.78030        0.54360
C         -1.14760       -0.06840       -0.12630
C         -1.72000       -1.11490       -0.81010
C         -3.07330       -1.33990       -0.85090
N          0.25160        0.17220       -0.07860
C          1.06520       -0.84210        0.24550
O          0.60900       -1.97310        0.50020
C          2.43930       -0.66520        0.30760
C          2.98180        0.57260        0.03220
C          2.11270        1.58730       -0.29630
N          0.79040        1.34470       -0.33750
N          4.37240        0.80740        0.08080
Cl         3.53080       -1.95770        0.72490
H         -4.98750       -0.61880       -0.18020
H         -4.04280        1.25210        1.04900
H         -1.62030        1.61490        1.09380
H         -1.07290       -1.80190       -1.34730
H         -3.46620       -2.19490       -1.41600
H          2.49970        2.57520       -0.51970
H          4.85890        0.87700        0.99810
H          4.94510        0.91630       -0.78140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

