%nproc=16
%mem=2GB
%chk=mol_135_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.93260        0.05950       -0.35850
C         -3.26920        1.25960       -0.16420
C         -1.90340        1.21090       -0.07590
C         -1.18010        0.03760       -0.17120
C         -1.85130       -1.15640       -0.36530
C         -3.22200       -1.12690       -0.45630
N          0.25790        0.06990       -0.06790
C          0.92720       -0.76160        0.75350
O          0.25860       -1.56810        1.42840
C          2.30260       -0.75440        0.87410
C          3.03350        0.14150        0.12330
C          2.32750        0.98130       -0.70910
N          0.98450        0.91990       -0.77720
N          4.45340        0.16910        0.22910
Cl         3.16990       -1.82990        1.93680
H         -4.99880        0.07520       -0.43020
H         -3.78710        2.20660       -0.08280
H         -1.36320        2.14070        0.07650
H         -1.28360       -2.09300       -0.44200
H         -3.74990       -2.07830       -0.61070
H          2.86370        1.69900       -1.31510
H          5.09550       -0.25520       -0.47660
H          4.86700        0.65320        1.08100

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

