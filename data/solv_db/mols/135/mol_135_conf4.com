%nproc=16
%mem=2GB
%chk=mol_135_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.91170       -0.20850       -0.38770
C         -3.20650        0.69420       -1.15530
C         -1.84010        0.82650       -0.99370
C         -1.14830        0.08090       -0.08320
C         -1.85450       -0.82230        0.68460
C         -3.23640       -0.96630        0.53160
N          0.25660        0.19250        0.10820
C          1.10330        0.22530       -0.94910
O          0.64010        0.15500       -2.13290
C          2.46420        0.33460       -0.74360
C          2.96710        0.41060        0.53450
C          2.07870        0.37430        1.58200
N          0.78500        0.26900        1.33510
N          4.34080        0.52370        0.82630
Cl         3.56030        0.37710       -2.10710
H         -4.99610       -0.31040       -0.51910
H         -3.72630        1.31510       -1.90130
H         -1.32490        1.55440       -1.62430
H         -1.31900       -1.42230        1.41270
H         -3.79980       -1.67610        1.13310
H          2.42520        0.43130        2.62280
H          5.03890        0.13310        0.14670
H          4.70330        0.97400        1.67980

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

