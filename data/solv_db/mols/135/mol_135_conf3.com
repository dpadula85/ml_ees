%nproc=16
%mem=2GB
%chk=mol_135_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.92080        0.48130       -0.05790
C         -3.09480        1.36930       -0.68810
C         -1.73560        1.18190       -0.62820
C         -1.16450        0.12340        0.04880
C         -2.03410       -0.75110        0.67210
C         -3.39590       -0.58100        0.62380
N          0.25950       -0.03630        0.08710
C          0.92220       -0.21490        1.24450
O          0.23510       -0.23690        2.30790
C          2.29130       -0.36890        1.28750
C          3.03910       -0.34350        0.11440
C          2.32020       -0.15820       -1.04880
N          0.98300       -0.01370       -1.02620
N          4.44540       -0.50120        0.15500
Cl         3.12610       -0.60170        2.83060
H         -5.00900        0.63830       -0.11040
H         -3.45080        2.23100       -1.24440
H         -1.08050        1.88300       -1.12580
H         -1.59120       -1.60380        1.21820
H         -4.03460       -1.28990        1.12590
H          2.87940       -0.13480       -1.97530
H          5.07270        0.34180        0.08980
H          4.93770       -1.41400        0.24750

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_135_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

