#!/usr/bin/env python

import sys
import numpy as np
from cclib.parser import ccopen

if __name__ == '__main__':

    mols = range(0,642)
    confs = range(1,6)

    for mol in mols:

        for conf in confs:

            try:
                filename = "%03d/mol_%03d_conf%d.log" % (mol, mol, conf)
                obj = ccopen(filename)
                data = obj.parse()

                try:
                    enes = np.r_[enes, np.atleast_2d(data.scfenergies)]

                except NameError:
                    enes = np.atleast_2d(data.scfenergies)

            except AttributeError:
                dummy = np.empty((1,4))
                dummy[:] = np.NAN
                enes = np.r_[enes, dummy]

    n = enes.shape[0]
    solvs = ["vac", "wat", "dmso", "cyclohex"]

    for i, solv in enumerate(solvs):
        hdr1 = "\nSCF energies in %s, in eV\n" % solv
        hdr2 = " %13s %16s %16s %16s %16s\n" % ("conf1", "conf2", "conf3", "conf4", "conf5")
        hdr = hdr1 + hdr2
        np.savetxt("%s_db.txt" % solv, enes[:,i].T.reshape((n/5,5)), fmt="%16.8f", header=hdr)
