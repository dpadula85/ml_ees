%nproc=16
%mem=2GB
%chk=mol_114_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.19500        1.15070       -0.18680
C          3.10270       -0.23240        0.40680
C          2.27950       -1.16850       -0.41860
C          0.85890       -0.71750       -0.60140
C          0.17230       -0.59090        0.71830
C         -1.23860       -0.14960        0.65380
C         -2.16640       -1.02560       -0.11220
C         -3.33610       -0.11730       -0.45730
C         -2.96570        1.26820        0.02900
C         -1.45110        1.19790        0.03150
H          2.32670        1.44780       -0.77590
H          4.11540        1.25380       -0.81360
H          3.32980        1.90220        0.64570
H          4.13740       -0.62480        0.46570
H          2.75100       -0.12460        1.44710
H          2.74020       -1.21970       -1.42750
H          2.33790       -2.17610        0.03650
H          0.79600        0.18320       -1.23930
H          0.34700       -1.54570       -1.17460
H          0.73140        0.13580        1.34550
H          0.18980       -1.60160        1.19490
H         -1.62410       -0.08600        1.70160
H         -2.50460       -1.88900        0.51460
H         -1.67630       -1.37710       -1.02650
H         -4.27700       -0.46490        0.00560
H         -3.43550       -0.05650       -1.55760
H         -3.34600        2.06620       -0.63410
H         -3.28240        1.38990        1.08480
H         -1.13060        1.14410       -1.05050
H         -0.97680        2.02800        0.56340

--Link1--
%nproc=16
%mem=2GB
%chk=mol_114_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_114_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_114_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

