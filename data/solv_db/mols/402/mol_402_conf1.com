%nproc=16
%mem=2GB
%chk=mol_402_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.15670       -0.98110       -0.18310
C          0.93750        0.25420        0.26040
N         -0.02330        1.02830       -0.53980
C         -1.00260        0.01930       -0.11070
H          0.41320       -1.28140       -1.22400
H          0.09660       -1.78600        0.55320
H          0.71880        0.44280        1.32600
H          1.96940        0.30710       -0.07030
H         -0.20580        1.97050       -0.16080
H         -1.24070        0.18930        0.95080
H         -1.81970       -0.16290       -0.80170

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

