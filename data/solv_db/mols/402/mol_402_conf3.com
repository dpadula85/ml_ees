%nproc=16
%mem=2GB
%chk=mol_402_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.01180        0.72240       -0.51320
C          0.96840       -0.40570       -0.10420
N          0.03720       -0.61210        1.02240
C         -1.00580       -0.27220        0.03680
H          0.07220        1.58010        0.16480
H         -0.04440        0.92200       -1.57640
H          1.95620       -0.04190        0.21210
H          0.92510       -1.27370       -0.75630
H          0.14970        0.24760        1.63710
H         -1.21170       -1.13610       -0.59410
H         -1.85870        0.26940        0.47110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

