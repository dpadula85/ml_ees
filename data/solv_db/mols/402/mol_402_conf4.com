%nproc=16
%mem=2GB
%chk=mol_402_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.47150        0.77390       -0.16160
C          0.76260       -0.63170        0.33820
N         -0.58090       -1.00180       -0.01600
C         -0.98800        0.36980        0.00280
H          0.73630        0.89540       -1.22390
H          0.81360        1.54670        0.53690
H          1.57730       -1.09050       -0.23260
H          0.95810       -0.69000        1.43270
H         -0.71390       -1.53340       -0.88330
H         -1.39530        0.64330        1.00850
H         -1.64140        0.71830       -0.80160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

