%nproc=16
%mem=2GB
%chk=mol_402_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.08580        0.87680       -0.14290
C         -0.94370       -0.29100        0.35290
N          0.02860       -1.14260       -0.35120
C          1.01690       -0.15710        0.12600
H         -0.06870        1.75060        0.49970
H         -0.17640        1.02970       -1.23100
H         -0.79080       -0.44130        1.44090
H         -1.95660       -0.32490       -0.02740
H         -0.09390       -0.94980       -1.38280
H          1.90020       -0.05310       -0.49890
H          1.17030       -0.29750        1.21480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_402_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

