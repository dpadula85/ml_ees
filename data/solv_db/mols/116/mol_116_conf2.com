%nproc=16
%mem=2GB
%chk=mol_116_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.51510       -0.75570        0.25890
C         -1.75630        0.04130       -0.79850
C         -0.34140        0.24810       -0.34460
C          0.02020        1.35030        0.40190
C          1.31230        1.56600        0.83250
C          2.31270        0.66450        0.52380
C          1.93570       -0.43140       -0.22250
C          0.65160       -0.65640       -0.65690
O          2.91730       -1.38200       -0.56510
H         -2.10090       -1.78700        0.32660
H         -3.58100       -0.75470       -0.07070
H         -2.39390       -0.23750        1.21380
H         -2.27840        0.98960       -0.98290
H         -1.74970       -0.53130       -1.74120
H         -0.74910        2.08220        0.66250
H          1.59150        2.44250        1.42420
H          3.33530        0.81140        0.85030
H          0.37390       -1.52530       -1.24360
H          3.01540       -2.13460        0.13150

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

