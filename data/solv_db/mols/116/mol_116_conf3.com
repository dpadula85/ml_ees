%nproc=16
%mem=2GB
%chk=mol_116_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.52730        0.46810        0.42720
C         -1.69970        0.18370       -0.80780
C         -0.30310       -0.09750       -0.39200
C          0.04590       -1.40980       -0.11400
C          1.33230       -1.73360        0.27790
C          2.31530       -0.76910        0.40760
C          1.94620        0.53230        0.12580
C          0.66460        0.86230       -0.26580
O          2.92500        1.51500        0.25190
H         -1.89090        0.46540        1.35950
H         -3.30360       -0.30250        0.60310
H         -3.01110        1.46210        0.37990
H         -2.09290       -0.68180       -1.38400
H         -1.70480        1.11340       -1.44260
H         -0.72220       -2.19340       -0.21100
H          1.61160       -2.76590        0.49760
H          3.32570       -1.02060        0.71510
H          0.38580        1.88950       -0.48500
H          2.70330        2.48250        0.05670

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

