%nproc=16
%mem=2GB
%chk=mol_116_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.62450       -0.64790        0.29070
C          1.74730        0.01870       -0.73690
C          0.35120        0.18980       -0.28080
C         -0.04090        1.33360        0.38970
C         -1.33810        1.53040        0.83230
C         -2.29520        0.55330        0.60330
C         -1.93440       -0.60360       -0.06540
C         -0.62770       -0.76940       -0.49510
O         -2.90340       -1.57310       -0.28740
H          2.16170       -1.57280        0.67450
H          2.92940        0.05460        1.09350
H          3.56120       -0.93970       -0.23010
H          1.76150       -0.51750       -1.69290
H          2.16110        1.06030       -0.86750
H          0.68540        2.11770        0.58460
H         -1.65810        2.41100        1.35340
H         -3.31410        0.66240        0.92830
H         -0.37620       -1.67770       -1.01170
H         -3.49510       -1.63020       -1.08280

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

