%nproc=16
%mem=2GB
%chk=mol_116_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.65770       -0.16060        0.18190
C         -1.59120       -0.73180       -0.76100
C         -0.25710       -0.17100       -0.38510
C          0.17750        1.00520       -0.96820
C          1.40160        1.53830       -0.63670
C          2.19950        0.89210        0.28510
C          1.78760       -0.28970        0.88540
C          0.55350       -0.79570        0.52880
O          2.59250       -0.93060        1.80750
H         -2.70770       -0.81670        1.08570
H         -2.38770        0.86300        0.48670
H         -3.61400       -0.12060       -0.35930
H         -1.85870       -0.41020       -1.77130
H         -1.60770       -1.82550       -0.60480
H         -0.42280        1.54100       -1.69690
H          1.72740        2.46830       -1.10820
H          3.18370        1.28680        0.57570
H          0.23750       -1.72810        1.00760
H          3.24370       -1.61410        1.44700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

