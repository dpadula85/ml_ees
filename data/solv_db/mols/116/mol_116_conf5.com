%nproc=16
%mem=2GB
%chk=mol_116_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.64660       -0.17490       -0.43140
C         -1.68350       -0.56390        0.63510
C         -0.31760       -0.06690        0.37420
C          0.14020        1.16260        0.81590
C          1.43190        1.58230        0.54460
C          2.30750        0.79040       -0.17480
C          1.86470       -0.43910       -0.62220
C          0.56700       -0.85480       -0.34670
O          2.75660       -1.21990       -1.34240
H         -2.21990       -0.06440       -1.43400
H         -3.47070       -0.91970       -0.49790
H         -3.13040        0.79610       -0.16990
H         -2.03120       -0.08230        1.59220
H         -1.67200       -1.68380        0.76700
H         -0.56930        1.75870        1.37690
H          1.75160        2.55990        0.91130
H          3.31530        1.11800       -0.38570
H          0.22610       -1.80960       -0.69500
H          3.38020       -1.88880       -0.91720

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_116_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

