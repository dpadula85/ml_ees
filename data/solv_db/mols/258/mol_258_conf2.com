%nproc=16
%mem=2GB
%chk=mol_258_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.12350        0.43870        1.05990
N         -1.84410       -0.34300       -0.09970
C         -3.00110       -1.10260       -0.67280
C         -0.61740       -0.50270       -0.71310
O         -0.62460       -1.23990       -1.77920
C          0.66130        0.07350       -0.35140
C          1.72260       -0.03250       -1.24620
C          3.02050        0.26450       -0.95430
C          3.32600        0.70340        0.31040
C          2.33060        0.82490        1.25390
C          1.00750        0.49770        0.91290
H         -1.92590       -0.07800        2.01930
H         -3.23050        0.71780        1.01290
H         -1.59510        1.41000        1.06220
H         -2.80170       -1.27440       -1.75070
H         -3.92320       -0.55470       -0.46500
H         -2.97350       -2.09680       -0.18350
H          1.49130       -0.35540       -2.26410
H          3.83250        0.16650       -1.69290
H          4.36090        0.94080        0.51770
H          2.57130        1.17280        2.25720
H          0.33610        0.36950        1.76640

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

