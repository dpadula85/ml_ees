%nproc=16
%mem=2GB
%chk=mol_258_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.07450        0.85910       -0.54560
N         -1.83780        0.09110       -0.30450
C         -2.02080       -1.22190        0.25920
C         -0.64160        0.80740       -0.43510
O         -0.75580        2.05730       -0.72110
C          0.72320        0.33560       -0.27710
C          1.06580       -0.98540       -0.09440
C          2.32440       -1.33850        0.33890
C          3.31640       -0.42100        0.59990
C          2.98150        0.90040        0.40110
C          1.73580        1.28220       -0.02200
H         -3.96490        0.35120       -0.18240
H         -3.17630        1.03330       -1.63520
H         -2.91870        1.83390       -0.01420
H         -3.06940       -1.26690        0.69810
H         -1.28060       -1.37850        1.07380
H         -1.91220       -2.03710       -0.46900
H          0.40960       -1.77130       -0.50750
H          2.56750       -2.39740        0.48930
H          4.30730       -0.72450        0.94360
H          3.73010        1.66670        0.58570
H          1.49100        2.32440       -0.18160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

