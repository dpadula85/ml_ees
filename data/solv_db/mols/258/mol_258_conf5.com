%nproc=16
%mem=2GB
%chk=mol_258_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.92840        1.26050        0.01420
N         -1.83390       -0.14940        0.16630
C         -3.07770       -0.85510        0.43980
C         -0.65270       -0.87560        0.02120
O         -0.83770       -2.16280        0.23780
C          0.68130       -0.44980       -0.28980
C          1.01350        0.81940       -0.65280
C          2.33570        1.30330       -0.55880
C          3.34100        0.47500       -0.10630
C          3.01290       -0.81860        0.24320
C          1.72210       -1.28320        0.15870
H         -2.01450        1.47530       -1.09040
H         -2.94250        1.58880        0.41760
H         -1.13230        1.85440        0.50590
H         -2.97070       -1.94340        0.36570
H         -3.33190       -0.62220        1.49840
H         -3.86490       -0.44180       -0.21790
H          0.31510        1.37290       -1.26930
H          2.50560        2.33750       -0.85620
H          4.34260        0.86280       -0.04130
H          3.82280       -1.45160        0.58810
H          1.49450       -2.29630        0.42570

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

