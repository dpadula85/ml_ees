%nproc=16
%mem=2GB
%chk=mol_258_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.01340        0.10670        1.23920
N         -1.81580        0.23110        0.39930
C         -2.02200        0.22140       -1.01220
C         -0.56350        0.02910        0.97750
O         -0.61420       -0.29080        2.23520
C          0.74600        0.12330        0.36270
C          0.99670        0.55550       -0.90580
C          2.23060        0.35440       -1.54230
C          3.26370       -0.28520       -0.89980
C          3.03670       -0.71590        0.38680
C          1.81710       -0.51960        1.00380
H         -3.24970       -0.97550        1.37170
H         -3.88310        0.61060        0.74180
H         -2.87050        0.54910        2.23340
H         -2.03950        1.25410       -1.47150
H         -1.33420       -0.46410       -1.55260
H         -3.05210       -0.23120       -1.19400
H          0.31000        1.24490       -1.37610
H          2.36630        0.71570       -2.56780
H          4.21500       -0.44360       -1.38510
H          3.83580       -1.22570        0.93830
H          1.64010       -0.84440        2.01760

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

