%nproc=16
%mem=2GB
%chk=mol_258_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.09360        1.31730        0.00620
N         -1.86060       -0.09590       -0.13310
C         -3.08610       -0.92370       -0.32530
C         -0.66190       -0.80400       -0.12600
O         -0.77440       -2.09940       -0.25870
C          0.70640       -0.34930        0.02870
C          1.72830       -1.31230        0.13460
C          3.04900       -0.94380        0.15040
C          3.38160        0.39000        0.06230
C          2.42920        1.34760       -0.04480
C          1.08460        0.96280       -0.06600
H         -1.85800        1.91410       -0.89210
H         -1.60580        1.74940        0.91980
H         -3.19600        1.52140        0.17220
H         -3.93280       -0.29620       -0.58030
H         -2.82030       -1.66990       -1.10360
H         -3.26560       -1.41320        0.64670
H          1.45610       -2.36160        0.23550
H          3.81920       -1.72120        0.23310
H          4.42460        0.64860        0.08140
H          2.68890        2.40260       -0.11410
H          0.38700        1.73680       -0.27960

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_258_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

