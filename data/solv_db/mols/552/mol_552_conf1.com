%nproc=16
%mem=2GB
%chk=mol_552_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          0.20460       -1.22490        0.09230
C         -1.13540       -0.98610       -0.06980
C         -1.62780        0.30120       -0.12780
C         -0.71630        1.33230       -0.01590
C          0.62820        1.07450        0.14670
C          1.13170       -0.20870        0.20600
O          2.48940       -0.46660        0.37020
F         -2.96280        0.55040       -0.28930
H          0.54780       -2.25130        0.13260
H         -1.85540       -1.80270       -0.15820
H         -1.11890        2.33980       -0.06340
H          1.32860        1.90040        0.23270
H          3.08650       -0.55840       -0.45620

--Link1--
%nproc=16
%mem=2GB
%chk=mol_552_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_552_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_552_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

