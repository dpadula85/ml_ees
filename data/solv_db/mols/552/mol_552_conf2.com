%nproc=16
%mem=2GB
%chk=mol_552_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.31190       -1.17440        0.26460
C          1.06320       -1.09160        0.15050
C          1.64260        0.13620       -0.07690
C          0.84610        1.24670       -0.18490
C         -0.54290        1.14760       -0.06730
C         -1.14010       -0.08250        0.16170
O         -2.52220       -0.17930        0.27850
F          2.99290        0.23670       -0.19180
H         -0.75420       -2.14470        0.44260
H          1.71950       -1.95480        0.23180
H          1.32150        2.20070       -0.36340
H         -1.16830        2.00170       -0.14930
H         -3.14620       -0.34230       -0.49630

--Link1--
%nproc=16
%mem=2GB
%chk=mol_552_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_552_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_552_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

