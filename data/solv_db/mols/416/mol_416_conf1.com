%nproc=16
%mem=2GB
%chk=mol_416_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.56110       -1.02170       -0.36530
C          3.66140        0.34410       -0.19710
C          2.53050        1.11600       -0.02380
C          1.25980        0.57150       -0.00980
C          0.12500        1.34010        0.16340
C         -1.14640        0.80270        0.17830
C         -2.28460        1.56380        0.35070
C         -3.55300        1.02520        0.36530
C         -3.66620       -0.34910        0.19670
C         -2.52780       -1.11340        0.02390
C         -1.25590       -0.56920        0.00980
C         -0.12010       -1.33270       -0.16270
C          1.15030       -0.79200       -0.17710
C          2.28920       -1.55790       -0.35020
H          4.45050       -1.63120       -0.50190
H          4.65290        0.81420       -0.20220
H          2.57480        2.19960        0.11200
H          0.22240        2.41300        0.29440
H         -2.19810        2.63400        0.48190
H         -4.45480        1.60650        0.49920
H         -4.65510       -0.82330        0.20110
H         -2.57170       -2.19930       -0.11230
H         -0.21560       -2.39730       -0.29280
H          2.17170       -2.64360       -0.48140

--Link1--
%nproc=16
%mem=2GB
%chk=mol_416_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_416_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_416_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

