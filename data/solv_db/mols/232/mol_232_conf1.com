%nproc=16
%mem=2GB
%chk=mol_232_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -0.53060        1.35200       -0.00540
C         -1.38170        0.33850        0.00190
C         -0.88960       -1.02520        0.00740
O         -1.69530       -2.01820        0.01440
C          0.53760       -1.34680        0.00530
C          1.38130       -0.32700       -0.00190
C          0.89570        1.04420       -0.00750
O          1.70220        2.00690       -0.01440
H         -0.89680        2.38120       -0.00960
H         -2.44650        0.53990        0.00370
H          0.87600       -2.38720        0.00970
H          2.44770       -0.55840       -0.00360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_232_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_232_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_232_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

