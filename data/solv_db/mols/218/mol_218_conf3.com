%nproc=16
%mem=2GB
%chk=mol_218_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          3.94340       -0.17360       -0.47680
C          2.44670       -0.10170       -0.49640
O          1.88760        0.59270       -1.36320
O          1.72950       -0.80330        0.44210
C          0.34360       -0.83080        0.55550
C         -0.20750        0.51670        0.86800
O         -1.61610        0.56350        1.00200
C         -2.53300        0.25500        0.02930
O         -2.12340       -0.10420       -1.09940
C         -4.01230        0.32140        0.23460
H          4.37770        0.31100       -1.35780
H          4.34320        0.32430        0.42370
H          4.29580       -1.22270       -0.44800
H          0.07040       -1.59990        1.31710
H         -0.11310       -1.25220       -0.38730
H          0.13800        1.23940        0.10330
H          0.17000        0.86820        1.87870
H         -4.40020        1.30810       -0.14590
H         -4.47780       -0.45300       -0.38390
H         -4.26270        0.24140        1.31400

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

