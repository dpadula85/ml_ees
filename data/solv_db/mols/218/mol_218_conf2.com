%nproc=16
%mem=2GB
%chk=mol_218_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.12350        0.28520       -0.04510
C          2.66520        0.46110       -0.29040
O          2.27700        1.60970       -0.57150
O          1.73960       -0.53610       -0.22890
C          0.35930       -0.43100       -0.44810
C         -0.36280        0.47780        0.46070
O         -1.73750        0.52980        0.17610
C         -2.64970       -0.47660        0.22400
O         -2.26050       -1.61870        0.56530
C         -4.10050       -0.32450       -0.10340
H          4.70030        1.05670       -0.59060
H          4.31110        0.44070        1.03710
H          4.41390       -0.74240       -0.33820
H         -0.05050       -1.45480       -0.35410
H          0.20860       -0.15180       -1.52070
H         -0.01210        1.53880        0.29170
H         -0.18690        0.16620        1.51040
H         -4.55040       -1.28560       -0.38820
H         -4.58970       -0.03310        0.86630
H         -4.29780        0.48860       -0.80740

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

