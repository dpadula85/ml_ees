%nproc=16
%mem=2GB
%chk=mol_218_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -3.30410       -0.99190        0.29290
C         -2.36560        0.15770        0.22250
O         -2.62930        1.17370        0.91720
O         -1.22800        0.13550       -0.57640
C         -0.39300        1.28610       -0.57480
C          0.78080        1.06640       -1.49850
O          1.58950       -0.02910       -1.14720
C          2.26050       -0.15550        0.03750
O          2.18460        0.74430        0.91060
C          3.11210       -1.34340        0.37240
H         -4.34450       -0.62040        0.51480
H         -3.03540       -1.72100        1.07940
H         -3.34290       -1.51150       -0.66720
H         -0.96590        2.16500       -0.99530
H         -0.07460        1.55360        0.45070
H          1.36680        2.03060       -1.47960
H          0.38540        0.88160       -2.52230
H          4.14610       -1.07150        0.05560
H          3.03620       -1.52590        1.45520
H          2.82130       -2.22440       -0.23430

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

