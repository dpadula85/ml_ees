%nproc=16
%mem=2GB
%chk=mol_218_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          4.28010       -0.71800        0.00120
C          2.97190       -0.00450        0.21220
O          2.92390        1.08870        0.82850
O          1.78000       -0.52590       -0.26370
C          0.57990        0.18710       -0.03670
C         -0.61890       -0.52090       -0.62010
O         -1.75320        0.29430       -0.32500
C         -3.03350       -0.02180       -0.69710
O         -3.24750       -1.07440       -1.32950
C         -4.13450        0.89410       -0.33960
H          4.80330       -0.25570       -0.87810
H          4.90180       -0.50590        0.88710
H          4.04030       -1.77620       -0.14650
H          0.67340        1.21400       -0.48560
H          0.37620        0.28020        1.06540
H         -0.69260       -1.51290       -0.12070
H         -0.57460       -0.59850       -1.72160
H         -4.51550        0.73500        0.69500
H         -4.96850        0.85270       -1.08720
H         -3.79190        1.96880       -0.35180

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

