%nproc=16
%mem=2GB
%chk=mol_218_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -4.03280        0.21080       -0.68170
C         -2.59480       -0.19330       -0.82590
O         -2.03260       -0.29320       -1.92410
O         -1.92680       -0.45100        0.36410
C         -0.59070       -0.83620        0.44680
C          0.29890        0.20700       -0.12460
O          1.64490       -0.22800       -0.01730
C          2.71760        0.51890       -0.45980
O          2.48570        1.63080       -0.97890
C          4.11160       -0.00500       -0.31330
H         -4.54140       -0.02270       -1.63790
H         -4.46980       -0.28860        0.18060
H         -4.02610        1.33610       -0.58920
H         -0.33010       -0.94160        1.53000
H         -0.46190       -1.83710       -0.01490
H          0.22370        1.11280        0.54110
H          0.14610        0.47910       -1.16570
H          4.79020        0.72020       -0.77910
H          4.38110       -0.09210        0.75480
H          4.20730       -1.02680       -0.74160

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_218_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

