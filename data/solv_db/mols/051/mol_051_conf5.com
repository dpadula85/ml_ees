%nproc=16
%mem=2GB
%chk=mol_051_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.52400       -0.24810        0.29690
C          1.13490        0.21120        0.04510
C          0.93520        1.52840       -0.35710
C         -0.33770        1.97140       -0.59500
C         -1.40530        1.11190       -0.43450
C         -1.25050       -0.19930       -0.03780
C          0.05160       -0.62750        0.19820
O          0.22630       -1.93200        0.59470
C         -2.41990       -1.09930        0.12580
H          2.57060       -0.92980        1.16890
H          2.85440       -0.80210       -0.61060
H          3.19260        0.63180        0.41950
H          1.78190        2.19020       -0.47860
H         -0.48240        3.00210       -0.90880
H         -2.39680        1.52320       -0.63920
H          1.13420       -2.32720        0.78750
H         -3.09920       -0.73060        0.94150
H         -2.04870       -2.12550        0.32840
H         -2.96520       -1.14870       -0.84480

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

