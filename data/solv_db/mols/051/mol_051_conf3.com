%nproc=16
%mem=2GB
%chk=mol_051_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.44650       -0.62180       -0.33420
C         -1.14700        0.05000       -0.10590
C         -1.14110        1.42590        0.11550
C          0.05640        2.07480        0.33050
C          1.21510        1.32340        0.31720
C          1.23230       -0.03660        0.10050
C          0.03360       -0.66890       -0.11200
O          0.05420       -2.04560       -0.33130
C          2.54030       -0.77530        0.10310
H         -2.74300       -0.58910       -1.41660
H         -2.47650       -1.65970        0.04480
H         -3.21300       -0.05630        0.25410
H         -2.05750        2.00730        0.12380
H          0.10810        3.14920        0.50780
H          2.18450        1.81900        0.48640
H         -0.75300       -2.60760       -0.49630
H          2.99620       -0.64140        1.10960
H          3.19560       -0.30930       -0.65370
H          2.36140       -1.83770       -0.04320

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

