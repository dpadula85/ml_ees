%nproc=16
%mem=2GB
%chk=mol_051_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.45880       -0.60560        0.13860
C          1.15470        0.07000        0.03860
C          1.12870        1.44720       -0.06370
C         -0.09520        2.07540       -0.15620
C         -1.27440        1.36090       -0.14820
C         -1.27050       -0.00850       -0.04690
C         -0.04860       -0.65160        0.04690
O         -0.02490       -2.02190        0.15110
C         -2.49760       -0.83200       -0.03260
H          2.50040       -1.52390       -0.49330
H          2.72690       -0.84450        1.20760
H          3.24690        0.09440       -0.20230
H          2.02950        2.05490       -0.07440
H         -0.10330        3.16020       -0.23630
H         -2.24730        1.84190       -0.22060
H          0.86300       -2.49570        0.22160
H         -2.62420       -1.38460        0.90850
H         -3.39540       -0.18560       -0.14810
H         -2.52770       -1.55090       -0.89030

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

