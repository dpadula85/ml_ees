%nproc=16
%mem=2GB
%chk=mol_051_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.49930       -0.50820        0.05390
C          1.13030        0.11800       -0.00200
C          1.01770        1.48990       -0.12460
C         -0.20920        2.11980       -0.18080
C         -1.32700        1.31470       -0.10890
C         -1.24210       -0.05210        0.01320
C          0.00910       -0.65410        0.06700
O          0.08770       -2.01980        0.18890
C         -2.45990       -0.91520        0.09030
H          2.62960       -1.11390       -0.87410
H          3.27660        0.25270        0.12310
H          2.48670       -1.19920        0.92330
H          1.91780        2.10320       -0.17940
H         -0.26510        3.20430       -0.27770
H         -2.28930        1.79700       -0.15200
H          0.97180       -2.46070        0.22830
H         -2.21530       -1.93880       -0.30390
H         -3.20170       -0.48490       -0.60160
H         -2.81690       -1.05280        1.11700

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

