%nproc=16
%mem=2GB
%chk=mol_051_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.39850       -0.81730        0.14060
C          1.15160       -0.01490       -0.01720
C          1.18920        1.32660       -0.33410
C          0.03950        2.07980       -0.48290
C         -1.16180        1.42560       -0.29890
C         -1.26430        0.10700        0.01420
C         -0.07290       -0.60570        0.15240
O         -0.15800       -1.94740        0.47040
C         -2.54960       -0.60750        0.21430
H          3.24820       -0.23930       -0.32400
H          2.35810       -1.77820       -0.39880
H          2.59320       -1.00140        1.21530
H          2.15140        1.81320       -0.47270
H          0.07740        3.12920       -0.73090
H         -2.08000        2.00140       -0.41160
H          0.70060       -2.46640        0.57130
H         -3.38710        0.11300        0.15920
H         -2.65970       -1.32950       -0.61770
H         -2.57440       -1.18820        1.15120

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_051_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

