%nproc=16
%mem=2GB
%chk=mol_140_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -1.45350        2.75760       -0.36960
O         -1.32540        1.37730       -0.20460
C         -0.14780        0.69270       -0.08510
C          1.07480        1.33350       -0.11860
C          2.27300        0.66270        0.00030
C          2.25100       -0.70740        0.15940
C          1.04290       -1.36300        0.19500
C         -0.14930       -0.67990        0.07490
O         -1.39220       -1.33330        0.10900
C         -1.50410       -2.72180        0.26710
H         -1.46660        2.97070       -1.45880
H         -2.44670        3.13360        0.01640
H         -0.68010        3.35110        0.17280
H          1.07580        2.40340       -0.24430
H          3.22910        1.17160       -0.02670
H          3.20040       -1.25650        0.25570
H          1.03630       -2.43810        0.32020
H         -0.73280       -3.28450       -0.30250
H         -2.51850       -3.03430       -0.10110
H         -1.36640       -3.03530        1.34050

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf1.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

