%nproc=16
%mem=2GB
%chk=mol_140_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.61380       -1.67180        0.24760
O         -1.21550       -1.51160       -0.01020
C         -0.63920       -0.25200        0.08370
C         -1.42200        0.83650        0.42560
C         -0.87240        2.09580        0.52500
C          0.48220        2.20750        0.26800
C          1.28170        1.12640       -0.07610
C          0.71220       -0.12830       -0.17060
O          1.42110       -1.27040       -0.50640
C          2.80750       -1.16450       -0.77090
H         -3.20210       -0.92860       -0.36120
H         -2.79750       -1.44840        1.30070
H         -2.96450       -2.67310        0.01150
H         -2.47400        0.70540        0.61820
H         -1.42100        2.98100        0.78500
H          0.96470        3.19500        0.33560
H          2.33400        1.22080       -0.27450
H          3.36630       -0.91940        0.17460
H          3.02270       -0.29560       -1.45460
H          3.22940       -2.10460       -1.15090

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf2.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

