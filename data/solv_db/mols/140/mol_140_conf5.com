%nproc=16
%mem=2GB
%chk=mol_140_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.77580       -1.32150       -0.05090
O          1.37420       -1.31560       -0.08190
C          0.67500       -0.13010       -0.03460
C          1.36010        1.06680        0.04430
C          0.63860        2.23640        0.09040
C         -0.75510        2.21030        0.05780
C         -1.40800        1.00280       -0.02100
C         -0.70810       -0.17870       -0.06810
O         -1.32290       -1.40510       -0.14710
C         -2.71500       -1.59560       -0.18840
H          3.18200       -0.87440       -0.98460
H          3.13070       -2.37170        0.09550
H          3.12480       -0.75980        0.84930
H          2.44710        1.11510        0.07110
H          1.17340        3.18970        0.15320
H         -1.28730        3.15850        0.09620
H         -2.48490        1.00270       -0.04520
H         -3.10850       -1.68570        0.86010
H         -3.21170       -0.77230       -0.74210
H         -2.88020       -2.57180       -0.73360

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf5.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

