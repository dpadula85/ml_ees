%nproc=16
%mem=2GB
%chk=mol_140_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C         -2.70000       -1.43020       -0.22990
O         -1.29500       -1.40210       -0.14970
C         -0.65570       -0.17160       -0.04100
C         -1.39260        1.01900       -0.01150
C         -0.77470        2.24790        0.09600
C          0.59810        2.25280        0.17300
C          1.33660        1.08370        0.14480
C          0.72340       -0.13990        0.03790
O          1.40650       -1.32810        0.00550
C          2.78350       -1.49980        0.07210
H         -3.06620       -0.74410       -1.01660
H         -3.18650       -1.12570        0.73900
H         -3.04320       -2.46000       -0.52750
H         -2.48630        0.98580       -0.07440
H         -1.37570        3.15190        0.11590
H          1.09860        3.19020        0.25660
H          2.42290        1.08810        0.20570
H          3.02300       -2.56440       -0.16100
H          3.35240       -0.90060       -0.68430
H          3.23070       -1.25290        1.04920

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf3.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

