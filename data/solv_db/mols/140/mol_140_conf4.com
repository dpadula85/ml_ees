%nproc=16
%mem=2GB
%chk=mol_140_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm

Title

0 1
C          2.92740       -0.85470        0.53780
O          1.55290       -1.04270        0.41090
C          0.66120       -0.01780        0.08420
C          1.15960        1.25190       -0.12590
C          0.33960        2.31070       -0.45110
C         -1.00790        2.03780       -0.55650
C         -1.52150        0.77070       -0.34880
C         -0.68990       -0.26830       -0.02640
O         -1.14730       -1.55480        0.19240
C         -2.48320       -1.94870        0.11430
H          3.31460       -1.70210        1.17280
H          3.49010       -0.93060       -0.41740
H          3.14180        0.06320        1.10520
H          2.21950        1.47380       -0.04440
H          0.75350        3.29480       -0.61060
H         -1.65060        2.86830       -0.81150
H         -2.58660        0.59510       -0.44080
H         -2.67750       -2.23560       -0.96130
H         -3.20810       -1.20640        0.47580
H         -2.58790       -2.90460        0.70110

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=water) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=dimethylsulfoxide) guess=read geom=check

Title

0 1

--Link1--
%nproc=16
%mem=2GB
%chk=mol_140_conf4.chk
#p B3LYP 6-31G(2df,p) nosymm scrf=(iefpcm,solvent=cyclohexane) guess=read geom=check

Title

0 1

