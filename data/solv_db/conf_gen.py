#!/usr/bin/env python

import sys
from rdkit import Chem
from rdkit.Chem import AllChem, TorsionFingerprints
from rdkit.ML.Cluster import Butina


def gen_conformers(mol, numConfs=100, maxAttempts=100, pruneRmsThresh=0.1, useExpTorsionAnglePrefs=True, useBasicKnowledge=True, enforceChirality=True):
	ids = AllChem.EmbedMultipleConfs(mol, numConfs=numConfs, maxAttempts=maxAttempts, pruneRmsThresh=pruneRmsThresh, useExpTorsionAnglePrefs=useExpTorsionAnglePrefs, useBasicKnowledge=useBasicKnowledge, enforceChirality=enforceChirality, numThreads=0)
	return list(ids)

	
def write_conformers_to_sdf(mol, ids, filename):
	w = Chem.SDWriter(filename)
        for id in ids:
            w.write(mol, confId=id)
	w.flush()
	w.close()
	

input_file = "smiles.csv"
numConfs = 5
maxAttempts = 100

suppl = Chem.SmilesMolSupplier(input_file, titleLine=False)

for i, mol in enumerate(suppl):
	if mol is None: continue
	m = Chem.AddHs(mol)
	conformerIds = gen_conformers(m, numConfs, maxAttempts)

	write_conformers_to_sdf(m, conformerIds, "mol_%03d.sdf" % i)

