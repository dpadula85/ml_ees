#!/usr/bin/env python

import pandas as pd

if __name__ == '__main__':

    db = []
    with open("solvation_database.txt") as f:
        for line in f:

            if line.startswith("#"):
                continue

            data = map(str.strip, line.split(";"))
            data[3:7] = map(float, data[3:7])
            db.append(data)

    hdrs = ["id", "SMILES", "IUPAC Name", "Exp. Kcal/mol", "Exp. Err. Kcal/mol",
            "GAFF Kcal/mol", "GAFF Err. Kcal/mol", "Exp. ref.", "Calc. ref.", "notes"]

    df = pd.DataFrame(db, columns=hdrs)
    writer = pd.ExcelWriter('db.xlsx')
    df.to_excel(writer, 'Sheet1')
    writer.save()

    df['SMILES'].to_csv("smiles.csv")
