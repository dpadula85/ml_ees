#!/usr/bin/env python

import sys
import pandas as pd

def write_xyz(conformers, db_entry):

    with open("entry_%03d_confs.xyz" % db_entry, "w") as f:
        for i, conformer in enumerate(conformers, start=1):

            natoms = len(conformer)
            f.write("%d\n" % natoms)
            f.write("Conformer %d\n" % i)

            for atom in conformer:
                f.write("%-5s %16.10f %16.10f %16.10f\n" % tuple(atom))

    return


if __name__ == '__main__':

    with open("HOPV_15_revised_2.data") as f:

        nmols = 0
        L = 1
        line = f.next()
        stop = False
        db = []
        while True:

            while L <= 5:

                # SMILES
                if L == 1:
                    smiles = line.strip()

                # InChI
                elif L == 2:
                    inchi = line.strip()

                # Experimental Data
                elif L == 3:
                    data = line.strip().split(",")

                    DOI = data[0]
                    inchikey = data[1]
                    construction = data[2]
                    architecture = data[3]
                    complement = data[4]
                    HOMO = float(data[5])
                    LUMO = float(data[6])
                    electrochemical_gap = float(data[7])
                    optical_gap = float(data[8])
                    PCE = float(data[9])
                    V_OC = float(data[10])
                    J_SC = float(data[11])
                    FF = float(data[12])

                # SMILES of cut molecule
                elif L == 4:
                    smiles_cut = line.strip()

                # Number of conformers
                elif L == 5:
                    nconfs = int(line.strip())
                    conformers = []

                    # Loop to read conformers
                    n = 1
                    while n <= nconfs:

                        conformer = []

                        # Conformer Index
                        line = f.next()
                        conf_idx = int(line.strip().split()[-1])

                        # Number of atoms
                        line = f.next()
                        natoms = int(line.strip())

                        j = 1
                        # Read xyz geometry
                        while j <= natoms:
                            line = f.next()
                            atom = line.strip().split()
                            atom[1:] = map(float, atom[1:])
                            conformer.append(atom)

                            j += 1

                        conformers.append(conformer)

                        # Computed properties
                        while j <= natoms + 4:
                            line = f.next()
                            data = line.strip().split(",")
                            method = data[0].split()[1]
                            cHOMO = float(data[1])
                            cLUMO = float(data[2])
                            cGap = float(data[3])
                            cPCE = float(data[4])
                            cV_OC = float(data[5])
                            cJ_SC = float(data[6])

                            j += 1

                        n += 1

                    # Save xyz file with conformers
                    write_xyz(conformers, nmols)

                    # Increase counter for read molecules
                    nmols += 1

                    # Create Pandas df entry with experimental data
                    data = [smiles, smiles_cut, inchi, inchikey, DOI, construction,
                            architecture, complement, HOMO, LUMO, electrochemical_gap,
                            optical_gap, PCE, V_OC, J_SC, FF, nconfs]

                    db.append(data)
                    try:
                        if n > nconfs:
                            L = 1
                            line = f.next()
                            break

                    except (NameError, StopIteration) as e:
                        pass

                L += 1

                try:
                    line = f.next()

                except StopIteration:
                    stop = True
                    break

            if stop:
                break


    hdrs = ["SMILES", "CUT SMILES", "InChI", "InChI key", "DOI",
            "construction", "architecture", "complement", "HOMO",
            "LUMO", "Electrochemical gap", "Optical gap", "PCE",
            "V_OC", "J_SC", "FF", "Conformers"]

    df = pd.DataFrame(db, columns=hdrs)
    writer = pd.ExcelWriter('output.xlsx')
    df.to_excel(writer, 'Sheet1')
    writer.save()

