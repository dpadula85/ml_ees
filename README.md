My implementation of the neural net described [here](https://www.nature.com/articles/ncomms13890)
with refinements introduced [here](https://aip.scitation.org/doi/10.1063/1.5019779)
