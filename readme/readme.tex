%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simple Sectioned Essay Template
% LaTeX Template
%
% This template has been downloaded from:
% http://www.latextemplates.com
%
% Note:
% The \lipsum[#] commands throughout this template generate dummy text
% to fill the template out. These commands should all be removed when 
% writing essay content.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------------------------------------------------------------------------------

%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%-------------------------------------------------------------------------------


\documentclass[12pt]{article} % Default font size is 12pt, it can be changed here

\usepackage{geometry} % Required to change the page size to A4
\geometry{a4paper} % Set the page size to be A4 as opposed to the default US Letter

\usepackage{graphicx} % Required for including pictures

\usepackage{float} % Allows putting an [H] in \begin{figure} to specify the exact location of the figure
\usepackage{wrapfig} % Allows in-line images such as the example fish picture
\usepackage{bm}
\usepackage[fleqn]{amsmath}
\usepackage{amsfonts}

\newcommand*\bs[1]{\boldsymbol{#1}}
\newcommand*\mr[1]{\mathrm{#1}}

\linespread{1.2} % Line spacing

%\setlength\parindent{0pt} % Uncomment to remove all indentation from paragraphs

\graphicspath{{Pictures/}} % Specifies the directory where pictures are stored

\begin{document}

%-------------------------------------------------------------------------------

%	TITLE PAGE
%-------------------------------------------------------------------------------


\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\center % Center everything on the page

%\textsc{\LARGE University Name}\\[1.5cm] % Name of your university/college
%\textsc{\Large Major Heading}\\[0.5cm] % Major heading such as course name
%\textsc{\large Minor Heading}\\[0.5cm] % Minor heading such as course title

\HRule \\[0.4cm]
{ \huge \bfseries Readme}\\ % Title of your document
\HRule \\[1.5cm]

%\begin{minipage}{0.4\textwidth}
%\begin{flushleft} \large
%\emph{Author:}\\
%John \textsc{Smith} % Your name
%\end{flushleft}
%\end{minipage}
~
%\begin{minipage}{0.4\textwidth}
%\begin{flushright} \large
%\emph{Supervisor:} \\
%Dr. James \textsc{Smith} % Supervisor's Name
%\end{flushright}
%\end{minipage}\\[4cm]

{\large \today}\\[3cm] % Date, change the \today to a set date if you want to be precise

%\includegraphics{Logo}\\[1cm] % Include a department/university logo - this will require the graphicx package

\vfill % Fill the rest of the page with whitespace

\end{titlepage}

%-------------------------------------------------------------------------------

%	TABLE OF CONTENTS
%-------------------------------------------------------------------------------


\tableofcontents % Include a table of contents

\newpage % Begins the essay on a new page instead of on the same page as the table of contents 

%-------------------------------------------------------------------------------

%	INTRODUCTION
%-------------------------------------------------------------------------------

\section{Kernel Ridge Regression}

\subsection{Training KRR} % Major section

Given a training set $(\bs{X},\bs{y})$, the linear parameters $\bs{alpha}$ are 
trained by solving:
\begin{equation}
\label{E:fitlinpar}
 \bs{\alpha} = \left(\bs{K} + \lambda \bs{1}\right)^{-1} \bs{y}
\end{equation}
where $\lambda$ is a regularisation hyperparameter and $\bs{K}$ is the kernel 
matrix with elements:
\begin{equation}
 \bs{K}_{i,j} = k(\bs{x}_i,\bs{x}_j)
\end{equation}
In the last equation, the functional form of $k$ determines the kernel type. 
$\bs{K}$ is dimensioned $(m\times m)$, where $m$ is the number of entries 
(examples) in the training set.
Necessarily, $\bs{\alpha}$ is a vector of length $m$.
It is necessary for function $k$ to be strictly positive definite in order to 
define a meaningful kernel.
Examples of used kernels are:
\begin{align}
 k(\bs{x}_i,\bs{x}_j) = 
\exp\left[-\frac{\left|\bs{x}_i-\bs{x}_j\right|^2_2}{2\sigma^2}\right] & 
\text{Gaussian kernel} \\
 k(\bs{x}_i,\bs{x}_j) = 
\exp\left[-\frac{\left|\bs{x}_i-\bs{x}_j\right|_1}{\sigma}\right] & 
\text{Laplacian kernel} 
\end{align}
where $\sigma$ is another hyperparameter.

In practice, given a training set, it is straightforward to obtain the linear 
parameters $\bs{\alpha}$. 
The more complex task is that of finding the optimal hyperparameters $\lambda$ 
and $\sigma$. 

\subsection{KRR prediction}

Once the linear parameters are found (and therefore also the hyperparameter are 
defined), 
if an input vector $\tilde{\bs{x}}$ is given, the trained KRR algorithm returns 
a prediction through:
\begin{equation}
\label{E:prediction}
 \hat{y} = \sum_{i=1}^m \alpha_i k(\bs{x_i},\tilde{\bs{x}})
\end{equation}
where $\bs{x}_i$ are the training data.

\subsection{Hyperparameter optimisation}

When one wants to optimise the hyperparameters, it is necessary to compute the 
error associated with certain values of the hyperparameters.
This can be done in various ways. The most naive is to have a hold-out set (or 
cross-validation, or development set), but other approaches work better.

\subsubsection{Hold-out set}

The hold-out set is a separate set $(\bs{X}^\mathrm{h},\bs{y}^\mathrm{h})$ the 
model is not trained on. 
This is necessary because you want to avoid to overfit the model on your 
training data, so you need a separate data set that tells you if this has 
happened.
Suppose your training error is very small, but the error on the hold-out set is 
large, that means that your model is very good for the data it has been trained 
on, but cannot generalise to examples it has not seen.
Therefore, you have a variance problem (you overfit).
Anyway, when you want to estimate if some hyperparameters work better than 
others, you decide based on the error on the hold-out set.

Ttypically the hold-out set is much smaller than the training set, but large 
enough to be statistically significant. 
Given certain hyperparameters $\Theta$ --- in KRR, $\Theta = (\lambda,\sigma)$ 
--- one first obtains the linear parameters by using equation 
\ref{E:fitlinpar}. 
Note that $\bs{\alpha}$ depend on both $\lambda$ (explicitly in equation 
\ref{E:fitlinpar}, but also on $\sigma$, through $\bs{K}$).

One then obtains predicted values $\hat{\bs{y}}_\mathrm{h}$ using equation 
\ref{E:prediction}. The error associated with the choice of hyperparameters can 
be quantified as:
\begin{equation}
\label{E:error}
 L(\Theta) = \frac{1}{m_\mathrm{h}} \sum_i^{m_\mathrm{h}} \left( y^\mathrm{h}_i 
- \hat{y}^\mathrm{h}_i\right)^2
\end{equation}

\subsubsection{Leave-one-out cross validation}

In leave-one-out (LOO) cross validation, there is no hold-out set. For each 
element $i$ in the training set, one trains linear parameters 
$\bs{\alpha}^{(i)}$ using all training data except $i$.
This means solving equation \ref{E:fitlinpar} $m$ times. For each of these, 
element $i$ practically works as a one-element hold-out set.
One can compute the error using equation \ref{E:error} and repeat the same for 
each $i$. The total error is the average of all errors.

While this procedure is better than having a hold-out set because you do not 
waste data for the hold-out set, it is more cumbersome.
Particularly, the bottleneck is the computation of the inverse of the kernel as 
in equation \ref{E:fitlinpar}. For each $i$, you train a different model and 
therefore need to solve the equation. For this reason God created the fast LOO 
version.

\subsubsection{Fast leave-one-out cross validation}

This is taken from Ying \& Keong, \emph{Fast leave-one-out evaluation and 
improvement of inference for LS-SVMs}, in \emph{Proceedings of the 17th 
International conference on Pattern Recognition (ICPR'04)}.


Assume you can solve $\bs{K}\bs{\alpha} = \bs{y}$, given a matrix $\bs{K}$ and a 
vector $\bs{y}$. Assume also you have $\bs{K}^{-1}$ stored (this means that you cannot use iterative solutions for the linear equation because you need the actual inverse).

Now you remove element $p$ as it never existed, and have vector $\bs{y}^{(p)}$ 
where the $p$-th element is removed, and likewise you have $\bs{K}^{(p)}$ where 
the $p$-th rows and columns are removed. Now you want to solve for 
$\bs{\alpha}^{(p)}$:  $\bs{K}^{(p)}\bs{\alpha}^{(p)} = \bs{y}^{(p)}$. This is 
what you do in LOO.

There is a fast way to obtain $\bs{\alpha}^{(p)}$ directly, without solving the 
reduced equation:
\begin{equation}
 \bs{\alpha}^{(p)} = \bs{\alpha}_\mathrm{sel}-\frac{K^{-1}_{\mathrm{sel},p} 
\alpha_p}{(K^{-1})_{p,p}}
\end{equation}
where $\bs{\alpha}_\mathrm{sel}$ is vector $\bs{\alpha}$ with element $p$ 
removed, $K^{-1}_{\mathrm{sel},p}$ is the $p$-th column of the inverse of 
$\bs{K}$, and with row $p$ removed.

The last equation can be vectorised to compute all $\bs{\alpha}^{(p)}$ at the 
same time, by defining:
\begin{align}
 A_{i,j} = \alpha_i \\
 W_{i,j} = (K^{-1})_{j,j}
\end{align}
and computing...

\clearpage

\section{Standard Neural Network}

\subsection{Dimensions}

\begin{equation*}
\begin{array}{ll}
 \bs{X} = \bs{a}^{[1]} & n^{[1]} \times m \\
 \bs{y}           & n^{[L]} \\
 \bs{z}^{[l]}     & n^{[l]} \times m \\
 \bs{a}^{[l]}     & n^{[l]} \times m \\
 \bs{W}^{[l]}     & n^{[l]} \times n^{[l-1]} \\
 \frac{\partial J}{\partial \bs{W}^{[l]}} & n^{[l]} \times n^{[l-1]} \\
 \bs{b}^{[l]} & n^{[l]} \\
 \frac{\partial J}{\partial \bs{b}^{[l]}} & n^{[l]} \\
\end{array}
\end{equation*}
where:
\begin{itemize}
  \item[] $\bs{X}$ is the training set input;
  \item[] $\bs{y}$ is the training set output;
  \item[] $\bs{a}^{[l]}$ are the activation values of the nodes in layer $l$;
  \item[] $\bs{W}^{[l]}$ are the weights to layer $l$;
  \item[] $\bs{b}^{[l]}$ is the bias for layer $l$;
  \item[] $J$ is the cost;
  \item[] $m$ is the number of examples;
  \item[] $L$ is the number of layers;
  \item[] $n^{[l]}$ is the number of nodes in layer $l$.
\end{itemize}

Use of indices is the following: $a^{[l]}_{\nu i}$ corresponds to the value of node $\nu$ of layer $l$ in example $i$. Nodes are always indicated with Greek subscripts, examples with Latin subscripts, and layers with Latin superscripts in square brackets.

\subsection{Forward propagation and cost}

For each layer:
\begin{equation}
 z^{[l]} = W^{l}\cdot a^{[l-1]} + b^{[l]}
\end{equation}
and then compute the new activation as 
\begin{equation}
 a^{[l]} = g\left(z^{[l]}\right)
\end{equation}

At the last layer one computes the cost function, to obtain the cost. Two types of cost are discussed here: quadratic cost (for regression) and \emph{modified} log-likelihood cost (for classification):
\begin{align}
 &J_\mathrm{Q} = \frac{1}{2m} \sum_\nu^{n^{[L]}} \sum_i^m \left| a^{[L]}_{\nu i} - y_{\nu i}\right|_2^2  \label{JQ} \\
 &J_\mathrm{MLL} = -\frac{1}{m} \sum_\nu^{n^{[L]}} \sum_i^m y_{\nu i} \log(\sigma(a^{[L]}_{\nu i}))  \label{JMLL}
\end{align}
where $\left|\cdot\right|_2$ is the L$_2$ (Euclidean) norm and $\sigma$ is the softmax function: 
\begin{equation}
\sigma(a_{\nu i}) = \sigma_{\nu i} = \frac{\exp[a_{\nu i}]}{\sum_{\mu} \exp[a_{\mu i}]}
\end{equation}

If regularisation is used:
\begin{align}
&J_\mathrm{Q}^\mathrm{R} = J_\mathrm{Q} + \lambda \mathcal{R} \\
&J_\mathrm{MLL}^\mathrm{R} = J_\mathrm{MLL} + \lambda \mathcal{R} 
\end{align}
the regularisation term being:
\begin{equation}
\mathcal{R} = \frac{1}{2} \sum_{l'=1}^{L-1} \sum_{\eta=1}^{n^{[l'+1]}} \sum_{\zeta=1}^{n^{[l']}} 
\left(W^{[l']}_{\eta\zeta}\right)^2
\end{equation}


Equation (\ref{JMLL}) is referred to as modified log likelihood because in standard log-likelihood there is no softmax function. However, it is assumed that the last activation function is softmax. Here, the softmax function is included in the cost function, and the last activation function is assumed to be linear.

\subsection{Backward propagation}

The derivation of the most relevant equations are given. Go to Section \ref{S:workingeqs} for a summary of the working equations.

The backward propagation error starts with computing the derivative of the cost function with respect to the activation of the last layer.

\subsubsection{Quadratic cost function}

\begin{equation}
\frac{\partial J_\mathrm{Q}}{\partial a_{\lambda q}} = \left(\frac{\partial J_\mathrm{Q}}{\partial \bs{a}}\right)_{\lambda q} = \frac{1}{2m} \sum_{\nu i} 2 a_{\nu i} \delta_{\nu \lambda} \delta_{iq} = \frac{a_{\lambda q}}{m} 
\end{equation}
or in matrix form:
\begin{equation}
\frac{\partial J_\mathrm{Q}}{\partial \bs{a}} = \delta \bs{a}_\mathrm{Q} = \frac{\bs{a} - \bs{y}}{m}
\end{equation}

\subsubsection{MLL cost function}

\begin{equation} \label{dJda}
\frac{\partial J_\mathrm{MLL}}{\partial a_{\lambda q}} = \left(\frac{\partial J_\mathrm{MLL}}{\partial \bs{a}}\right)_{\lambda q} = \sum_{\pi p} \left(\frac{\partial J_\mathrm{MLL}}{\partial \bs{\sigma}}\right)_{\pi p} \left( \frac{\partial \bs{\sigma}}{\partial \bs{a}} \right)_{\pi p \lambda q} = \sum_{\pi p} \frac{\partial J_\mathrm{MLL}}{\partial \sigma_{\pi p}} \frac{\partial \sigma_{\pi p}}{\partial a_{\lambda q}}
\end{equation}

By steps:
\begin{equation} \label{dJds}
\frac{\partial J_\mathrm{MLL}}{\partial \sigma_{\pi p}} = \left(\frac{\partial J_\mathrm{MLL}}{\partial \bs{\sigma}}\right)_{\pi p} = -\frac{1}{m} \sum_{\nu i} y_{\nu i} \frac{1}{\sigma_{\nu i}} \delta_{\pi\nu} \delta_{pi} = -\frac{1}{m} \frac{y_{\pi p}}{a_{\pi p}}
\end{equation}

Second step:
\begin{align} \label{dsda}
\frac{\partial \sigma_{\pi p}}{\partial a_{\lambda q}} = \left(\frac{\partial \bs{\sigma}}{\partial \bs{a}}\right)_{\pi p \lambda q} &= \frac{\exp[a_{\lambda p}] \delta_{\pi \lambda} \delta_{pq}}{\sum_\mu \exp[a_{\mu p}]} - \frac{\exp[a_{\pi p}] \sum_\mu \exp[a_{\mu p}] \delta_{\lambda \mu} \delta_{pq}}{\left[\sum_\mu \exp[a_{\mu p}]^2\right]} = \nonumber \\ 
&=\sigma_{\lambda p} \delta_{\pi \lambda} \delta_{pq} - \sigma_{\pi p} \sigma_{\lambda p} \delta_{pq}
\end{align}

Put together, from Equations (\ref{dJda}), (\ref{dJds}) and (\ref{dsda}):
\begin{align}
\frac{\partial J_\mathrm{MLL}}{\partial a_{\lambda q}} = \left(\frac{\partial J_\mathrm{MLL}}{\partial \bs{a}}\right)_{\lambda q} &= -\frac{1}{m} \sum_{\pi p} \frac{y_{\pi p}}{a_{\pi p}} \left(\sigma_{\lambda p} \delta_{\pi \lambda} \delta_{pq} - \sigma_{\pi p} \sigma_{\lambda p} \delta_{pq}\right) = \nonumber \\
&= -\frac{1}{m} \left[ y_{\lambda q} - \sigma_{\lambda q} \sum_\pi y_{\pi q}\right] = \frac{1}{m} \left[\sigma_{\lambda q} - y_{\lambda q}\right]
\end{align}
or in matrix form:
\begin{equation}
\frac{\partial J_\mathrm{MLL}}{\partial \bs{a}} = \delta\bs{a}_\mathrm{MLL} = \frac{\bs{\sigma}-\bs{y}}{m} = \frac{\sigma(\bs{a})-\bs{y}}{m}
\end{equation}

%\subsubsection{Regularisation term}
%
%There is no direct derivative of the regularisation term with respect to the activation units:
%\begin{equation}
%\frac{\partial \mathcal{R}}{\partial a_{\lambda q}} = 0
%\end{equation}

\subsubsection{Derivatives with respect to the parameters}

First of all, we back-propagate the error from $\bs{a}^{[l]}$ to $\bs{z}^{[l]}$ for any layer $l$, given the relation:
\begin{equation}
a_{\lambda q}^{[l]} = g(z_{\lambda q}^{[l]}) \Rightarrow \bs{a}^{[l]} = g(\bs{z}^{[l]})
\end{equation}
where the activation function $g$ is local, and cannot be, for instance, the softmax function, which would depend on all $\lambda$'s. Therefore:
\begin{equation}
\frac{\partial a_{\lambda q}}{\partial z_{\mu p}} = \left( \frac{\partial \bs{a}}{\partial \bs{z}}\right)_{\lambda q \mu p} = g'(z_{\lambda q}) \delta_{\lambda \mu} \delta_{qp}
\end{equation}

Then, we back-propagate the error from $\bs{z}^{[l]}$ to $\bs{W}^{[l]}$, given the relation:
\begin{equation} \label{E:fwprop}
z_{\mu p}^{[l]} = \sum_\lambda W_{\mu \lambda}^{[l]} a_{\lambda p}^{[l-1]} + b_{\mu}^{[l]} \Rightarrow \bs{z}^{[l]} = \bs{W}^{[l]} \bs{a}^{[l-1]} + \bs{b}^{[l]}
\end{equation}

\begin{equation}
\frac{\partial z_{\mu p}}{\partial W_{\alpha \beta}} = \left( \frac{\partial \bs{z}}{\partial \bs{W}}\right)_{\mu p \alpha \beta} = \sum_\lambda a_{\lambda p}^{[l-1]} \delta_{\mu\alpha} \delta_{\lambda\beta} = a_{\beta p}^{[l-1]} \delta_{\mu\alpha} 
\end{equation}

Now, we back-propagate the error from $\bs{z}^{[l]}$ to $\bs{b}^{[l]}$, given again the relation (\ref{E:fwprop}):
\begin{equation}
\frac{\partial z_{\mu p}}{\partial b_\alpha} = \left( \frac{\partial \bs{z}}{\partial \bs{b}}\right)_{\mu p \alpha} = \delta_{\mu\alpha}
\end{equation}

Finally, we back-propagate the error from $\bs{z}^{[l]}$ to $\bs{a}^{[l-1]}$, given again (\ref{E:fwprop}):
\begin{equation}
\frac{\partial z_{\mu p}^{[l]}}{\partial a_{\alpha r}^{[l-1]}} = \left( \frac{\partial \bs{z}^{[l]}}{\partial \bs{a}^{[l-1]}}\right)_{\mu p \alpha r} = \sum_\lambda W_{\mu \lambda}^{[l]} \delta_{\lambda \alpha} \delta_{p r} = W_{\mu \alpha}^{[l]} \delta_{p r}
\end{equation}

Putting everything together and explicitly indicating the layer, which we previously overlooked:
\begin{align}
\frac{\partial J}{\partial W_{\alpha\beta}^{[l]}} = \left(\frac{\partial J}{\partial \bs{W}^{[l]}}\right)_{\alpha\beta} 
&= \sum_{\lambda q}  \sum_{\mu p}
\left(\frac{\partial J}{\partial \bs{a}^{[l]}}\right)_{\lambda q}
\left(\frac{\partial \bs{a}^{[l]}}{\partial \bs{z}^{[l]}}\right)_{\lambda q \mu p}
\left(\frac{\partial \bs{z}^{[l]}}{\partial \bs{W}^{[l]}}\right)_{\mu p \alpha \beta} = \nonumber \\
&= \sum_{\lambda q}  \sum_{\mu p}
\left(\delta\bs{a}^{[l]}\right)_{\lambda q}
g'(z_{\lambda q}^{[l]}) \delta_{\lambda\mu} \delta_{qp}
a_{\beta p}^{[l-1]} \delta_{\mu \alpha} = \nonumber \\
&= \sum_p \left(\delta\bs{a}^{[l]}\right)_{\alpha p} g'(z_{\alpha p}^{[l]}) a_{\beta p}^{[l-1]}
\end{align}
or, in matrix form:
\begin{equation} \label{E:dJdWM}
\frac{\partial J}{\partial \bs{W}^{[l]}} = 
\left[\delta\bs{a}^{[l]} \otimes g'\left(\bs{z}^{[l]}\right)\right] \bs{a}^{[l-1]\dagger} 
= \delta\bs{z}^{[l]} \bs{a}^{[l-1]\dagger}
\end{equation}
where $\otimes$ is the element-wise multiplication and $\delta\bs{z}^{[l]}$ was implicitly defined. Similarly, one obtains the derivative with respect to $\bs{b}$:
\begin{align}
\frac{\partial J}{\partial b_{\alpha}^{[l]}} = \left(\frac{\partial J}{\partial \bs{b}^{[l]}}\right)_{\alpha} 
&= \sum_{\lambda q}  \sum_{\mu p}
\left(\frac{\partial J}{\partial \bs{a}^{[l]}}\right)_{\lambda q}
\left(\frac{\partial \bs{a}^{[l]}}{\partial \bs{z}^{[l]}}\right)_{\lambda q \mu p}
\left(\frac{\partial \bs{z}^{[l]}}{\partial \bs{b}^{[l]}}\right)_{\mu p \alpha} = \nonumber \\
&= \sum_{\lambda q}  \sum_{\mu p}
\left(\delta\bs{a}^{[l]}\right)_{\lambda q}
g'(z_{\lambda q}^{[l]}) \delta_{\lambda\mu} \delta_{qp} \delta_{\mu \alpha} = \nonumber \\
&= \sum_p \left(\delta\bs{a}^{[l]}\right)_{\alpha p} g'(z_{\alpha p}^{[l]}) 
\end{align}
or, in matrix form:
\begin{equation}
\frac{\partial J}{\partial \bs{b}^{[l]}} = 
\sum_p \left[\delta\bs{a}^{[l]} \otimes g'\left(\bs{z}^{[l]}\right)\right] = \sum_p \delta\bs{z}^{[l]} 
\end{equation}

Finally, in order to back-propagate to layers further back, one needs to compure the derivative with respect to $\bs{a}^{[l-1]}$:
\begin{align}
\frac{\partial J}{\partial a_{\alpha r}^{[l-1]}} = \left(\frac{\partial J}{\partial \bs{a}^{[l-1]}}\right)_{\alpha r} 
&= \sum_{\lambda q}  \sum_{\mu p}
\left(\frac{\partial J}{\partial \bs{a}^{[l]}}\right)_{\lambda q}
\left(\frac{\partial \bs{a}^{[l]}}{\partial \bs{z}^{[l]}}\right)_{\lambda q \mu p}
\left(\frac{\partial \bs{z}^{[l]}}{\partial \bs{a}^{[l-1]}}\right)_{\mu p \alpha r} = \nonumber \\
&= \sum_{\lambda q}  \sum_{\mu p}
\left(\delta\bs{a}^{[l]}\right)_{\lambda q}
g'(z_{\lambda q}^{[l]}) \delta_{\lambda\mu} \delta_{qp} W_{\mu \alpha}^{[l]} \delta_{pr} = \nonumber \\
&= \sum_\mu \left(\delta\bs{a}^{[l]}\right)_{\mu r} g'(z_{\mu r}^{[l]}) W_{\mu\alpha}^{[l]}
\end{align}
or, in matrix form:
\begin{equation}
\frac{\partial J}{\partial \bs{a}^{[l-1]}} = \delta\bs{a}^{[l-1]} =
\bs{W}^{[l]\dagger} \left[\delta\bs{a}^{[l]} \otimes g'\left(\bs{z}^{[l]}\right)\right] = \bs{W}^{[l]\dagger} \delta\bs{z}^{[l]} 
\end{equation}
and from this one back-propagates to $\bs{W}^{[l-2]}$, $\bs{b}^{[l-2]}$ and so on.

When regularisation is used, an extra term appears in the derivative with respect to $\bs{W}$:
\begin{equation}
\frac{\partial \mathcal{R}}{\partial W^{[l]}_{\alpha\beta}} = 
\frac{1}{2} \sum_{l'\eta\zeta} 
\frac{\partial}{\partial W^{[l]}_{\alpha\beta}}\left(W^{[l']}_{\eta\zeta}\right)^2 = W^{[l]}_{\alpha\beta}
\end{equation}
therefore Equation (\ref{E:dJdWM}) becomes:
\begin{equation} 
\frac{\partial J^\mathrm{R}}{\partial \bs{W}^{[l]}} = 
\frac{\partial J}{\partial \bs{W}^{[l]}} + \lambda \bs{W}^{[l]}
= \delta\bs{z}^{[l]} \bs{a}^{[l-1]\dagger} + \lambda \bs{W}^{[l]}
\end{equation}


\subsection{Working equations cheatsheet} \label{S:workingeqs}

\begin{enumerate}
\item Forward-propagation
  \begin{enumerate}
  \item Assign values to first layer neurons \\ $\bs{a}^{[1]} = \bs{X}$
  \item Propagate to next layer \\ $\bs{z}^{[l+1]} = \bs{W}^{[l+1]} \bs{a}^{[l]} + \bs{b}^{[l+1]}$ \\ $\bs{a}^{[l+1]} =  g\left(\bs{z}^{[l+1]}\right)$
  \item At the last layer, compute the cost: \\
  $J = \left\{ \begin{array}{ll} 
  \frac{1}{2m} \sum_\nu^{n^{[L]}} \sum_i^m \left| a^{[L]}_{\nu i} - y_{\nu i}\right|_2^2 & \textrm{(quadratic cost function)} \\
 -\frac{1}{m} \sum_\nu^{n^{[L]}} \sum_i^m y_{\nu i} \log(\sigma(a^{[L]}_{\nu i})) & \textrm{(modified log-likelihood cost function)} 
 \end{array} \right.$
 \item Regularise: \\ $J^\mathrm{R} = J + \frac{\lambda}{2} \sum_{l',\eta,\zeta} \left(W^{[l']}_{\eta\zeta}\right)^2$
  \end{enumerate}
\item Back-propagation
  \begin{enumerate}
  \item Compute $\delta\bs{z}$ at the last layer: \\$\delta\bs{z}^{[L]} = \left\{ 
  \begin{array}{ll}
    \frac{\bs{a}-\bs{y}}{m} \otimes g'\left(\bs{z}^{[L]}\right) & \textrm{(quadratic cost function)}\\
    \frac{\sigma\left(\bs{a}\right)-\bs{y}}{m} \otimes g'\left(\bs{z}^{[L]}\right) & \textrm{(modified log-likelihood cost function)}\\
  \end{array}    
  \right.$
  \item Back-propagate $\delta\bs{z}$ to the previous layers: \\ $\delta\bs{z}^{[l-1]} = \left[\bs{W}^{[l]\dagger}\delta\bs{z}^{[l]}\right] \otimes g'\left(\bs{z}^{[l-1]}\right)$ 
  \item Compute parameter gradients: \\
  $\frac{\partial J}{\partial \bs{W}^{[l]}} = \delta\bs{z}^{[l]} \bs{a}^{[l-1]\dagger}$, or, with regularisation: $\frac{\partial J^\mathrm{R}}{\partial \bs{W}^{[l]}} = \frac{\partial J}{\partial \bs{W}^{[l]}} + \lambda \bs{W}^{[l]}$ \\
  $\frac{\partial J}{\partial \bs{b}^{[l]}} = \frac{\partial J^\mathrm{R}}{\partial \bs{b}^{[l]}} = \sum_p \delta\bs{z}^{[l]}$ \\
  \end{enumerate}
\end{enumerate}

%% OLD EQUATIONS

%We define $\delta^{[l]}$ as the error: $\delta^{[l]}_j$ is the error of the $j$-th unit in the $l$-th layer. It can be computed as:
%
%\begin{align}
% \delta^{[L]} = (a^{[L]}-y)\circ g^\prime(z^{[L]}) & \text{at the last layer} \\
% \delta^{[l]} = \left(W^{[l+1]\dagger} \delta^{[l+1]}\right) \circ 
%g^\prime(z^{[l]}) & \text{otherwise}
%\end{align}
%where $\circ$ is the element-wise multiplication. In the last equations, 
%$\delta^{[l]} \in \mathbb{R}^{n^{[l]} \times m}$. 
%
%The partial derivatives of the overall cost function (for all examples) are:
%\begin{align}
% & \frac{\partial}{\partial b^{[l]}} J = \frac{1}{m} \sum_{j=1}^m \delta^{[l]}_{:,j} \\
% & \frac{\partial}{\partial W^{[l]}} J = \frac{1}{m} \delta^{[l]} a^{[l-1]\dagger}\\
%\end{align}
%Moreover, if regularisation is used, the derivative with respect to $W$ is replaced by:
%\begin{equation}
%  \frac{\partial}{\partial W^{[l]}} J = \frac{1}{m} \delta^{[l]} a^{[l-1]\dagger} + \lambda W^{[l]}
%\end{equation}

%\section{Prediction of molecular energies with NNs}

\clearpage

\section{Molecular Neural Network}

Modified labelling convention: each atom in a molecule at a certain stage is generally represented by a column vector. The layers are again identified by Latin superscripts in square brackets; the neurons by lowercase Greek subscripts; the atoms by lowercase Latin subscripts; the molecules (\emph{i.e.}, the examples) by uppercase Greek subscripts: $a_{\nu m \Gamma}^{[l]}$ is the value of neuron $\nu$ for atom $m$ in molecule $\Gamma$ at layer ${[l]}$.

\subsection{Architecture and forward propagation}

\subsubsection{Preliminary tasks and initialisation}

\paragraph{Atoms}

Each atom is identified by a vector $p$ which is initialised randomly. Atoms with the same atomic number $Z$ have the same starting vector:
\begin{equation}\label{E:init_p}
\bs{p}_{m\Gamma} = \bs{k}(Z_m) \in \mathbb{R}^F
\end{equation}

Vectors $\bs{k}$ are dimensioned $F$, and so are $\bs{p}_{m\Gamma}$ for all atoms, at all layers. Elements of $\bs{k}$ for each $Z$ are initialised randomly following a Gaussian distribution:
\begin{equation}\label{E:init_k}
k_i \sim \mathcal{N}\left(0, \frac{1}{\sqrt{F}}\right)
\end{equation}

\paragraph{Distances} 

Given the distance between a pair of atoms in a molecule $D_{mn\Gamma} = \left| \bs{R}_{m\Gamma} - \bs{R}_{n\Gamma}\right| \in \mathbb{R}$, one starts by expanding this distance in a basis of Gaussians. Note that positions and distances are also labelled with $\Gamma$ to specify that all are referred to that particular molecule. In fact, $\bs{D}$ is a $\left(n_\mathrm{At} \times n_\mathrm{At} \times n_\mathrm{Mol}\right)$ matrix. 
\begin{equation} \label{E:compute_e}
e_\eta\left(D_{mn\Gamma}\right) = e_{\eta m n \Gamma}= \exp\left[-\gamma \left( D_{mn\Gamma} - \mu_\eta\right)^2 \right], \, \mu_\eta = 0, \mu, 2\mu, 3\mu, \cdots R_\mathrm{max}
\end{equation}
The number of elements $G$ is thus given by $G = \frac{R_\mathrm{max}}{\mu} + 1$. As hyperparameters we consider $G$ (the dimension of the gaussian expansion layer), $R_\mathrm{max}$ and $\gamma$ (determining the range and resolution of the filter, respectively). It follows that $\mu$ is uniquely determined.
This passage gives a vector $\bs{e}$ dimensioned $\left(G \times n_\mathrm{At} \times n_\mathrm{At} \times n_\mathrm{Mol}\right)$ and will be referred to as $\mathbb{G} \left[G\right]$. It is not explicitly indicated as a function of $R_\mathrm{max}$ and $\gamma$ because these are hyperparameters. $G$ is indicated only to keep track of dimensions.

\paragraph{Weights and biases}

TBD

\subsubsection{F-block (filter-generating network)}

The filter-generating network (FGN) determines how the interaction between atoms will be modelled.
From the inter-atomic vectors $\bs{e}$ generated during the initialisation, a standard NN with at least one hidden layer is used:
\begin{equation} \label{E:F-block}
\mathrm{NN} \left[ G 
\xrightarrow[\text{SSP}]{\bs{W}^{\mr{F}_2}, \bs{b}^{\mr{F}_2}} F \xrightarrow[\text{SSP}]{\bs{W}^{\mr{F}_3}, \bs{b}^{\mr{F}_3}} \left(F \xrightarrow[\text{SSP}]{\bs{W}^{\mr{F}_i}, \bs{b}^{\mr{F}_i}} \right)_n F \right]
\end{equation}
where $(\cdot)$ indicates non-necessary layers and the activation function is the shifted softplus function at all layers:
\begin{equation}
\mr{SSP}(x) = \log\left[\frac{1}{2} e^x + \frac{1}{2}\right]
\end{equation}

The weights and biases are collectively labelled $\bs{W}^\mr{F}$ and $\bs{b}^\mr{F}$. The dimensions of all elements are:
\begin{equation*}
\begin{array}{ll}
 \bs{e} = \bs{a}^{\mr{F}_1} & G \times n_\mr{At} \times n_\mr{At} \times n_\mr{Mol} \\
 \bs{a}^{\mr{F}_{i\neq 1}}  & F \times n_\mr{At} \times n_\mr{At} \times n_\mr{Mol} \\
 \bs{W}^{\mr{F}_2}          & F \times G \\
 \bs{W}^{\mr{F}_i\neq 2}    & F \times F \\
 \bs{b}^{\mr{F}_i}          & F \\
\end{array}
\end{equation*}

\subsubsection{I-block (interaction)}

In the interaction block, the interaction between different atoms within the same molecule is considered. In practice, each atom $m$ is here convoluted with the FGN generated with all the other atoms $n$. This is the only time activations from different atoms interact. Note that atoms on different molecules never do.

First a standard NN passage is performed, with at least one layer: 
\begin{equation} \label{E:I-block-1}
\mathrm{NN} \left[ F 
\xrightarrow[\text{Lin}]{\bs{W}^{\mr{I}_1}, \bs{b}^{\mr{I}_1}}  \left(F \xrightarrow[\text{Lin}]{\bs{W}^{\mr{I}_i}, \bs{b}^{\mr{I}_i}} \right)_n F \right]
\end{equation}
using linear (no) activation function and standard weights and biases $\bs{W}^\mr{I}$ and $\bs{b}^\mr{I}$.

Then, a C-block is performed yielding vector $\bs{a}^\mr{C}_{m\Gamma}$:
\begin{equation} \label{E:C-block}
\bs{a}^\mr{C}_{m\Gamma} = \sum_n \bs{a}^\mr{I}_{n\Gamma} \otimes \bs{a}^{\mr{F}}_{mn\Gamma}
\end{equation}
where $\otimes$ indicates the element-wise multiplication.

This is again followed with a standard NN passage, not necessarily with a hidden layer:
\begin{equation} \label{E:I-block-2}
\mathrm{NN} \left[ F 
\xrightarrow[\text{Lin}]{\bs{W}^{\mr{J}_1}, \bs{b}^{\mr{J}_1}}  \left(F \xrightarrow[\text{Lin}]{\bs{W}^{\mr{J}_i}, \bs{b}^{\mr{J}_i}} \right)_n F \right]
\end{equation}
using linear (no) activation function and standard weights and biases $\bs{W}^\mr{J}$ and $\bs{b}^\mr{J}$.

The dimensions of all elements are:
\begin{equation*}
\begin{array}{ll}
 \bs{a}^{\mr{I}_i} & F \times n_\mr{At} \times n_\mr{Mol} \\
 \bs{W}^{\mr{I}_i} & F \times F \\
 \bs{b}^{\mr{F}_i} & F \\
 \bs{a}^\mr{C}     & F \times n_\mr{At} \times n_\mr{Mol} \\
 \bs{a}^{\mr{J}_i} & F \times n_\mr{At} \times n_\mr{Mol} \\
 \bs{W}^{\mr{J}_i} & F \times F \\
 \bs{b}^{\mr{J}_i} & F \\
\end{array}
\end{equation*}

\subsubsection{K-block (finalisation)}

The vectors $\bs{a}^\mr{J}$ resulting from the C-block, after several passages (polarisation), enter the final stage of the MolNN, consisting of a standard NN passage with at least two layers, where the number of neurons is reduced to 1:
\begin{equation} \label{E:K-block}
\mathrm{NN} \left[ F 
\xrightarrow[\text{SSP}]{\bs{W}^{\mr{K}_1}, \bs{b}^{\mr{K}_1}}  
\left(F' \xrightarrow[\text{SSP}]{\bs{W}^{\mr{J}_i}, \bs{b}^{\mr{J}_i}} \right)_n
F' \xrightarrow[\text{Lin}]{\bs{W}^{\mr{J}_{n+1}}, \bs{b}^{\mr{J}_{n+1}}} 1 \right]
\end{equation}
These are the predicted atomic energies $\hat{E}_{m\Gamma}$.

At this point, predicted molecular energies are obtained by summing the contributions from all atoms:
\begin{equation}
\hat{E}_\Gamma = \sum_{m\in\Gamma} \hat{E}_{m\Gamma}
\end{equation}
The error is computed from the discrepancy between the predicted molecular energies and the true ones $E$:
\begin{equation}\label{E:K-error}
  J = \sum_\Gamma \left(\hat{E}_\Gamma - E_\Gamma\right)^2 + \lambda \mathcal{R}
\end{equation}
where $\mathcal{R}$ is the regularisation cost.

\subsubsection{Procedure}

A single pass for a set of molecules is:

\begin{enumerate}
  \item Initialise
  \begin{enumerate}
    \item Initialise weights and biases
    \item Initialise atomic vectors $\bs{k}$ from atomic numbers $Z$ as in Eqs. (\ref{E:init_p}) and (\ref{E:init_k})
    \item Compute distance matrices $\bs{D}$
    \item Compute vectors $\bs{e}$ as in Eq. (\ref{E:compute_e})    
  \end{enumerate}
  \item Forward propagation
  \begin{enumerate}
    \item Run F-block once -- Eq. (\ref{E:F-block}) -- from $\bs{e}$ to $\bs{a}^\mr{F}$
    \item Polarisation
    \begin{enumerate}
      \item Run I-block -- Eqs. (\ref{E:I-block-1}), (\ref{E:C-block}), (\ref{E:I-block-2}) -- for $N_\mr{pol}$ times
    \end{enumerate}
    \item Run K-block -- Eq. (\ref{E:K-block}) and get predicted molecular energies
    \item Compute error -- Eq. (\ref{E:K-error})
  \end{enumerate}



\end{enumerate}

\subsection{Initialisation}

\subsubsection{Neurons}

\subsubsection{Weights}

Weights $\bs{W}$ are dimensioned $\left(n^{[l]} \times n^{[l-1]}\right)$ as in standard NNs and are initialised randomly following a Gaussian distribution (LeCun normal initialisation):
\begin{equation}
\bs{W}^{[l]} \sim \mathcal{N}\left(0, \frac{1}{\sqrt{n^{[l-1]}}}\right)
\end{equation}

\end{document}
