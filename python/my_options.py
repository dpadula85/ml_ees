#!/usr/bin/env python

import argparse as arg
import tensorflow as tf


def MolNN_options():

    parser = arg.ArgumentParser(formatter_class=arg.ArgumentDefaultsHelpFormatter)
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--model_dir', default="./model_dir", type=str,
                     help="Model save directory.")
    inp.add_argument('--data_dir', default="./data_dir", type=str,
                     help="Data source directory.")
    inp.add_argument('--nat', default=None, type=int, required=True,
                     help="Number of atoms of the biggest molecule.")
    inp.add_argument('--lr', default=1e-3, type=float,
                     help="Learning Rate.")
    inp.add_argument('--opt', default="AdamOptimizer", type=str,
                     help="Tensorflow Optimiser.")
    inp.add_argument('--reg', default=1e-10, type=float,
                     help="Weights Regularisation.")
    inp.add_argument('--nsteps', default=5000, type=int,
                     help="Maximum number of training steps.")
    inp.add_argument('--batch', default=50, type=int,
                     help="Training batch size.")
    inp.add_argument('--decay', default=1000, type=int,
                     help="Step frequency of the learning rate decay.")
    inp.add_argument('--savefrq', default=100, type=int,
                     help="Step frequency of checkpoint generation.")
    inp.add_argument('--evalfrq', default=10, type=int,
                     help="Time frequency of evaluation (in minutes).")
    inp.add_argument('--trainfile', default="train.tfrecords", type=str,
                     help="Name of the tfrecords file containing training \
                     data.")
    inp.add_argument('--testfile', default="validation.tfrecords", type=str,
                     help="Name of the tfrecords file containing validation \
                     data.")
    inp.add_argument('--F', default=64, type=int,
                     help="Size of the atomic density representation vector \
                     space.")


    F_parser = parser.add_argument_group("F-Block options")

    F_parser.add_argument('--G', default=300, type=int,
                          help="Size of the gaussian basis for the expansion of \
                          interatomic distances.")
    F_parser.add_argument('--nF', default=3, type=int,
                          help="Number of layers in the F-block.")
    F_parser.add_argument('--Facts', default=None,
                          help="Activation functions for the F-block.")


    I_parser = parser.add_argument_group("I-Block options")
    I_parser.add_argument('--nI', default=4, type=int,
                          help="Number of layers in the I-block.")
    I_parser.add_argument('--Cb', default=1, type=int,
                          help="Position of the Convolution Layer.")
    I_parser.add_argument('--Iacts', default=None,
                          help="Activation functions for the I-block.")
    I_parser.add_argument('--npol', default=3, type=int,
                          help="Number of polarisation loops in the I-block.")

    K_parser = parser.add_argument_group("K-Block options")
    K_parser.add_argument('--H', default=32, type=int,
                          help="Number of neurons in the penultimate layer of the \
                          K-block.")
    K_parser.add_argument('--nK', default=3, type=int,
                          help="Number of layers in the K-block.")
    K_parser.add_argument('--Kacts', default=None,
                          help="Activation functions for the K-block.")

    # Parse
    args = parser.parse_args()

    # Divide groups
    arg_groups = {}
    for group in parser._action_groups:
        group_dict = { a.dest: getattr(args, a.dest, None)  \
                       for a in group._group_actions }
        arg_groups[group.title] = group_dict

    args = arg_groups
    args_inp = args['Input Data']
    args_F = args['F-Block options']
    args_I = args['I-Block options']
    args_K = args['K-Block options']

    args_inp['evalfrq'] *= 60
    args_inp['opt'] = getattr(tf.train, args_inp['opt'])

    # Default architecture
    args_F['units'] = [ args_F['G'] ] + [ args_inp['F'] ] * (args_F['nF'] - 1)
    args_F['acts'] = ['tf_shifted_softplus'] * (args_F['nF'] - 1)

    args_I['units'] = [ args_inp['F'] ] * args_I['nI']
    args_I['acts'] = ['linear'] + ['tf_shifted_softplus'] * (args_I['nI'] - 3)
    args_I['acts'] += ['linear']

    args_K['units'] = [ args_inp['F'] ] * (args_K['nK'] - 2) \
                      + [ args_K['H'] ] + [ 1 ]
    args_K['acts'] = ['tf_shifted_softplus'] * (args_K['nK'] - 2) + ['linear']

    # Remove processed data
    del args_F['Facts']
    del args_F['nF']
    del args_F['G']
    del args_I['Iacts']
    del args_I['nI']
    del args_K['Kacts']
    del args_K['nK']
    del args_K['H']

    # Copy general data
    args_F['reg'] = args_inp['reg']
    args_I['reg'] = args_inp['reg']
    args_I['F'] = args_inp['F']
    args_I['nat'] = args_inp['nat']
    args_K['reg'] = args_inp['reg']

    # Repack options
    args = {'Input': args_inp,
            'F-Block': args_F,
            'I-Block': args_I,
            'K-Block': args_K}

    return args


if __name__ == "__main__":
    pass
