#!/usr/bin/env python

import os
import data
import pickle
import util as u
import numpy as np
import argparse as arg
import tensorflow as tf

from my_estimator import model_fn


def options():

    parser = arg.ArgumentParser(formatter_class=arg.ArgumentDefaultsHelpFormatter)
    inp = parser.add_argument_group("Input Data")

    inp.add_argument('--model_dir', default="./model_dir", type=str,
                     help="Model data directory.")
    inp.add_argument('--testfile', default="test.tfrecords", type=str,
                     help="Name of the tfrecords file containing validation \
                     data.")
    inp.add_argument('--nat', default=None, type=int,
                     help="Number of atoms of the biggest molecule.")
    inp.add_argument('--out', default=None, type=str,
                     help="Name of the output file to save predictions.")


    args = parser.parse_args()
    args = vars(args)

    return args


def infer(args):

    """Run the inference and print the results to stdout."""

    # Set the run_config where to load the model from
    run_config = tf.contrib.learn.RunConfig()
    run_config = run_config.replace(model_dir=args['model_dir'])

    # Load architecture
    arch_file = os.path.join(args['model_dir'], "arch.pk")
    with open(arch_file, "rb") as f:
        arch = pickle.load(f)

    arch_inp = arch['Input']

    # Overwrite nat if specified
    if args['nat'] is not None:
        arch_inp['nat'] = args['nat']

    arch['I-Block']['nat'] = arch_inp['nat']

    # read testfile
    testfile = os.path.join(arch_inp['data_dir'], args['testfile'])
    def ts_fn():
        return data.input_fn(testfile, arch_inp['nat'], repeat_count=1)

    feats, labels = ts_fn()
    predictions = model_fn(feats, labels, tf.estimator.ModeKeys.PREDICT,
                           arch).predictions

    saver = tf.train.Saver()
    with tf.Session() as sess:

        ckpt = tf.train.get_checkpoint_state(args['model_dir'])
        saver.restore(sess, ckpt.model_checkpoint_path)
    
        # Loop through the batches and store predictions and labels
        index_values = []
        prediction_values = []
        label_values = []
        while True:
            try:
                preds, fts, lbls = sess.run([predictions, feats, labels])
                index_values.append(fts["index"])
                prediction_values.append(preds)
                label_values.append(lbls)

            except tf.errors.OutOfRangeError:
                break

    idx = np.array(index_values).reshape(-1)
    preds = np.array(prediction_values).reshape(-1)
    labels = np.array(label_values).reshape(-1)

    return idx, labels, preds


if __name__ == "__main__":

    import sys
    from scipy.stats import pearsonr, spearmanr, kendalltau

    args = options()
    u.print_dict(args)
    idx, labels, preds = infer(args)

    # Organise the data in columns and sort by idx
    res = np.c_[ idx, labels, preds ]
    res = res[res[:,0].argsort()]

    # Statistics
    r = pearsonr(labels, preds)[0]
    rho = spearmanr(labels, preds)[0]
    tau = kendalltau(labels, preds)[0]
    rmse = np.sqrt(np.mean((labels - preds)**2))
    mse = rmse**2
    mae = np.mean(np.abs(labels - preds))

    # Header
    hdr = ("\nTest results\n"
           "r    = %10.4f\n"
           "rho  = %10.4f\n"
           "tau  = %10.4f\n"
           "RMSE = %10.4f\n"
           "MSE  = %10.4f\n"
           "MAE  = %10.4f\n"
           "%5s %14s %14s\n" % (r, rho, tau, rmse, mse, mae,
                                "Idx", "DFT", "MolNN")
           )

    if args['out'] is None:
        args['out'] = sys.stdout

    np.savetxt(args['out'], res, fmt="%-7d %14.6f %14.6f", header=hdr)
