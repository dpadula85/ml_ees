#!/usr/bin/env python

import util as u
import tensorflow as tf

from blocks import Perceptron, Interaction


class MolecularNet:
    """
    A Neural Network for the prediction of molecular energies,
    built modularly from Perceptron and Interaction blocks.
    """

    def __init__(self, params):

        # Initialise class specific parameters
        self.kdict = self._k_init(params['Input']['F'])
        self.F = params['Input']['F']
        self.nat = params['Input']['nat']

        # Build the net modularly from blocks
        self.Fb = Perceptron(params['F-Block'], name="F")
        self.Kb = Perceptron(params['K-Block'], name="K")
        self.Ib = []

        for i in range(1, params['I-Block']['npol'] + 1):
            self.Ib += [ Interaction(params['I-Block'], name="I%d" % i) ]

        return


    def _k_init(self, F):
        """
        Function to initialise a random vector representing the atomic
        density.

        Parameters
        ----------
        F: int.
            size of the vector.

        Returns
        -------
        uk: dict.
            dictionary of vector representations of atomic densities.
        """

        uk = {}
        atnos = range(0, 119)

        with tf.variable_scope("k-dict", reuse=tf.AUTO_REUSE):
            for atno in atnos:

                # Deal with dummy atoms
                if atno == 0:
                    uk[atno] = tf.Variable(tf.zeros([F]),
                                           dtype=tf.float32,
                                           name="k%d" % atno)

                else:
                    uk[atno] = tf.get_variable("k%d" % atno,
                                               [F],
                                               trainable=True,
                                               dtype=tf.float32,
                                               regularizer=None,
                                               initializer=tf.glorot_uniform_initializer())

        return uk


    def _gen_k_mat(self, Z, kdict=None):
        """
        Function to build a matrix of atomic densities.

        Parameters
        ----------
        Z: Tensor.
            row vector of atomic numbers.
        kdict: dict.
            dictionary of vector representations of atomic densities.

        Returns
        -------
        kmat: Tensor.
            matrix of atomic densities.
        """

        if kdict is None:
            kdict = self.kdict

        with tf.variable_scope("k-matrix"):
            kmat = tf.stack(map(kdict.get, sorted(kdict.keys())), axis=1)
            out = tf.transpose(tf.nn.embedding_lookup(tf.transpose(kmat), Z))

        return out


    def fwd_prop(self, inputs):

        # Initialise inputs
        Zi = inputs["numbers"]
        ei = inputs["gaussian_distances"]
        k = self._gen_k_mat(Zi)

        # Get some variables needed for calculations
        nat = self.nat
        sz = tf.size(Zi)
        nmol = sz / nat

        #
        # Create TF Graph for FWD prop
        #

        # F-Block
        with tf.variable_scope("F-Block"):
            h = self.Fb.fwd_prop(ei)

        # I-Block npol times
        for i, Ib in enumerate(self.Ib, start=1):
            with tf.variable_scope("I-Block-%d" % i):
                k = Ib.fwd_prop(k, h)

        # K-Block
        with tf.variable_scope("K-Block"):
            k = self.Kb.fwd_prop(k)

        # The energies are stored in a line vector of shape nat * nmol
        with tf.variable_scope("y_hat"):
            k = tf.transpose(tf.reshape(k, [nmol, nat]))
            y_hat = tf.reduce_sum(k, axis=0, keepdims=True)

        return y_hat


if __name__ == '__main__':
    pass
