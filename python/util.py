#!/usr/bin/env python

import sys
import numpy as np
import tensorflow as tf
from elements import ELEMENTS as periodic_table
from scipy.spatial.distance import cdist, squareform

au2ang = 0.5291771

### VARIOUS ###
def read_xyz_GDB9(filename, au=False):

    if au:
        factor = au2ang
    else:
        factor = 1.0

    mol = []
    with open(filename) as f:
        j = 0
        for i, line in enumerate(f):

            if i == 0:
                nat = int(line.strip())

            if i == 1:
                data = line.strip().split()
                label = int(data[1])
                ene = float(data[13])

            if i == 2:
                while j < nat:
                    atom = line.strip().split()
                    atom = atom[:4]
                    atom[1:] = map(float, atom[1:])
                    mol.append(atom)
                    line = next(f)
                    j += 1

    atgeo = [ x[0] for x in mol ]
    Zs = np.atleast_2d([ periodic_table[at].number for at in atgeo ]).T
    structgeo = np.array([ x[1:] for x in mol ])

    return Zs, structgeo, ene, label


def dist_mat(struct, n=None):
    '''Returns the column vector corresponding to the upper triangular distance
    matrix, excluding the diagonal.'''

    D = cdist(struct, struct)

    if n is not None:
        N = D.shape[0]
        mask = np.ones((n,n))
        mask = np.c_[mask, np.zeros((n, N-n))]
        mask = np.r_[mask, np.zeros((N-n, N))]
        D = D * mask

    # idxs = np.triu_indices(D.shape[0], k=1)
    # Du = np.atleast_2d(D[idxs]).T

    # return Du
    return D.reshape(-1,1)


def variable_summary(var, name=None):

    with tf.name_scope('%s-summaries' % name):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('%s-avg' % name, mean)

    with tf.name_scope('%s-sigma' % name):
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('%s-sigma' % name, stddev)
        tf.summary.scalar('%s-max' % name, tf.reduce_max(var))
        tf.summary.scalar('%s-min' % name, tf.reduce_min(var))
        tf.summary.histogram('%s-histogram' % name, var)

    return


# Taken from https://github.com/tensorflow/tensorflow/issues/8246
# because of the lack of a repeat method within TF
def tf_repeat(tensor, repeats):
    """
    Args:

    input: A Tensor. 1-D or higher.
    repeats: A list. Number of repeat for each dimension, length must be the same as the number of dimensions in input

    Returns:

    A Tensor. Has the same type as input. Has the shape of tensor.shape * repeats
    """
    with tf.variable_scope("repeat"):
        expanded_tensor = tf.expand_dims(tensor, -1)
        multiples = [1] + repeats
        tiled_tensor = tf.tile(expanded_tensor, multiples = multiples)
        repeated_tesnor = tf.reshape(tiled_tensor, tf.shape(tensor) * repeats)

    return repeated_tesnor


### ACTIVATION FUNCTIONS AND DERIVATIVES ###
def linear(z):

    return z


def tf_shifted_softplus(z):

    with tf.variable_scope("ShiftedSoftplus"):
        shift = tf.log(tf.constant(0.5, dtype=tf.float32))
        result = tf.nn.softplus(z) + shift

    return result


### PRINT FUNCTIONS ###
def banner(text=None, ch='=', length=78):
    """Return a banner line centering the given text.
          
    "text" is the text to show in the banner. None can be given to have
      no text.
    "ch" (optional, default '=') is the banner line character (can
      also be a short string to repeat).
    "length" (optional, default 78) is the length of banner to make.
    
    Examples:
      >>> banner("Peggy Sue")
      '================================= Peggy Sue =================================='
      >>> banner("Peggy Sue", ch='-', length=50)
      '------------------- Peggy Sue --------------------'
      >>> banner("Pretty pretty pretty pretty Peggy Sue", length=40)
      'Pretty pretty pretty pretty Peggy Sue'
    """
    
    if text is None:
      return ch * length
    
    elif len(text) + 2 + len(ch)*2 > length:
      # Not enough space for even one line char (plus space) around text.
      return text
    
    else:
      remain = length - (len(text) + 2)
      prefix_len = remain / 2
      suffix_len = remain - prefix_len
                                                                                                                                                                          
      if len(ch) == 1:
        prefix = ch * prefix_len
        suffix = ch * suffix_len
  
      else:
        prefix = ch * (prefix_len/len(ch)) + ch[:prefix_len%len(ch)]
        suffix = ch * (suffix_len/len(ch)) + ch[:suffix_len%len(ch)]

      return prefix + ' ' + text + ' ' + suffix


def transl_dict(v):

      if type(v) is str:
        w = v

      elif type(v) is int or type(v) is float:
        w = str(v)

      elif type(v) is list:
        w = ', '.join(map(str, v))

      elif type(v) is dict:
        lenv = len(v)
        w = [None]*lenv
        for kk, vv in v.items():
          w[kk - 1] = transl_dict(vv)

      elif hasattr(v, '__call__'):
        w = v.__name__

      else:
        w = v

      return w


def print_dict(opts_dict, title=None, ch="=", outstream=None):
  
    if outstream:
      sys.stdout = outstream
    
    if not title:
      title = "Options"

    print(banner(ch=ch, length=100))
    print(title)
    print
    fmt = "%-20s %-20s"
    for k, v in sorted(opts_dict.items()):
        if type(v) in [str, int, float, bool, list] or hasattr(v, '__call__'):
            w = transl_dict(v)
            print(fmt % (k, w))

        elif type(v) is dict:
            print_dict(v, title=k, ch="-")

    print(banner(ch=ch, length=100))
    print

    return


if __name__ == '__main__':
    pass
