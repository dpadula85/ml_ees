#!/usr/bin/env python

import os
import sys
import data
import pickle
import util as u
import tensorflow as tf

from model import MolecularNet


# Show debugging output
tf.logging.set_verbosity(tf.logging.DEBUG)


# Define and run experiment ###############################
def run_experiment(args):
    """Run the training experiment."""

    args_inp = args['Input']

    # Save architecture
    try:
        os.makedirs(args_inp['model_dir'])
    except OSError:
        pass

    arch_file = os.path.join(args_inp['model_dir'], "arch.pk")
    with open(arch_file, "wb") as f:
        pickle.dump(args, f)

    # Set the run_config and the directory to save the model and stats
    run_config = tf.estimator.RunConfig()
    run_config = run_config.replace(model_dir=args_inp['model_dir'])
    run_config = run_config.replace(
                                save_checkpoints_steps=args_inp['savefrq'])

    # Define the estimator
    estimator = get_estimator(run_config, args)

    # Setup data loaders
    trainfile = os.path.join(args_inp['data_dir'], args_inp['trainfile'])
    testfile = os.path.join(args_inp['data_dir'], args_inp['testfile'])

    def tr_fn():
        return data.input_fn(trainfile, args_inp['nat'],
                             batch_size=args_inp['batch'])

    def ts_fn():
        return data.input_fn(testfile, args_inp['nat'], repeat_count=1)

    train_input_fn = tr_fn
    eval_input_fn = ts_fn

    # This line to avoid crash with early stopping
    try:
        os.makedirs(estimator.eval_dir())
    except OSError:
        pass

    # Define early-stopping hook
    tol_stps = args_inp['savefrq'] * 10
    early_stopping = tf.contrib.estimator.stop_if_no_decrease_hook(
            estimator,
            metric_name='loss',
            max_steps_without_decrease=tol_stps,
            min_steps=args_inp['savefrq']
    )

    # Define the experiment
    train_spec = tf.estimator.TrainSpec(
        input_fn=train_input_fn,
        max_steps=args_inp['nsteps'],
        hooks=[early_stopping]
    )

    eval_spec = tf.estimator.EvalSpec(
        input_fn=eval_input_fn,
        steps=None,
        throttle_secs=args_inp['evalfrq'],
        start_delay_secs=args_inp['evalfrq'],
    )

    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)


# Define model ############################################
def get_estimator(run_config, params):
    """Return the model as a Tensorflow Estimator object.

    Args:
         run_config (RunConfig): Configuration for Estimator run.
         params (HParams): hyperparameters.
    """
    return tf.estimator.Estimator(
        model_fn=model_fn,  # First-class function
        params=params,  # HParams
        config=run_config  # RunConfig
    )


def model_fn(features, labels, mode, params):
    """Model function used in the estimator.

    Args:
        features (Tensor): Input features to the model.
        labels (Tensor): Labels tensor for training and evaluation.
        mode (ModeKeys): Specifies if training, evaluation or prediction.
        params (HParams): hyperparameters.

    Returns:
        (EstimatorSpec): Model to be run by Estimator.
    """
    is_training = mode == tf.estimator.ModeKeys.TRAIN

    # Define model's architecture
    preds = architecture(features, params, is_training=is_training)

    # Loss, training and eval operations are not needed during inference.
    loss_op = None
    train_op = None
    eval_metric_ops = {}

    if mode != tf.estimator.ModeKeys.PREDICT:
        with tf.variable_scope("loss"):
            loss = tf.losses.mean_squared_error(labels, preds)
            regloss = tf.losses.get_regularization_loss()
            regloss = tf.cast(regloss, dtype=tf.float32)
            loss_op = loss + regloss

        train_op = get_train_op_fn(loss_op, params['Input'])
        eval_metric_ops = get_eval_metric_ops(labels, preds)

    estim_specs = tf.estimator.EstimatorSpec(
            mode=mode,
            predictions=preds,
            loss=loss_op,
            train_op=train_op,
            eval_metric_ops=eval_metric_ops
            )

    return estim_specs


def get_train_op_fn(loss, params):
    """Get the training Op.

    Args:
         loss (Tensor): Scalar Tensor that represents the loss function.
         params (HParams): Hyperparameters (needs to have `learning_rate`)

    Returns:
        Training Op
    """
    # Define an exponential decay for the learning rate
    with tf.name_scope("learning_rate"):
        decay_steps = params['decay']
        lr = tf.train.exponential_decay(params['lr'],
                                        tf.train.get_global_step(),
                                        decay_steps,
                                        0.95)

    return tf.contrib.layers.optimize_loss(
        loss=loss,
        global_step=tf.train.get_global_step(),
        optimizer=params['opt'],
        learning_rate=lr
    )


def get_eval_metric_ops(labels, predictions):
    """Return a dict of the evaluation Ops.

    Args:
        labels (Tensor): Labels tensor for training and evaluation.
        predictions (Tensor): Predictions Tensor.

    Returns:
        Dict of metric results keyed by name.
    """
    eval_ops = {
        'MAE': tf.metrics.mean_absolute_error(
            labels=labels,
            predictions=predictions,
            name='MAE'),
        'MSE': tf.metrics.mean_squared_error(
            labels=labels,
            predictions=predictions,
            name='MSE'),
        'RMSE': tf.metrics.root_mean_squared_error(
            labels=labels,
            predictions=predictions,
            name='RMSE')
        }

    return eval_ops


def architecture(inputs, params, is_training, scope='MolNN'):
    """Return the output operation following the network architecture.

    Args:
        inputs (Tensor): Input Tensor
        is_training (bool): True iff in training mode
        scope (str): Name of the scope of the architecture

    Returns:
         Output Op for the network.
    """
    with tf.variable_scope(scope):

        net = MolecularNet(params)
        y_hat = net.fwd_prop(inputs)

    return y_hat


if __name__ == "__main__":

    from my_options import *
    args = MolNN_options()
    u.print_dict(args, title="Training and Architecture Hyperparameters")
    run_experiment(args)
