#!/usr/bin/env python

import util as u
import tensorflow as tf


def _init_act(acts):
    """
    Generic activation functions Initialiser.

    Parameters
    ----------
    acts: list.
        activation function at each layer.

    Returns
    -------
    act: dict.
        Activation functions dictionary.
    """

    act = {}
    L = len(acts)
    for l in range(L):

        # try to get TensorFlow activation function
        try:
            act[l + 1] = getattr(tf.nn, acts[l])
        except:
            act[l + 1] = getattr(u, acts[l])

    return act


class Perceptron:
    """
    A simple general implementation of a Multi-Layer Perceptron.
    """

    def __init__(self, params, name=None):

        if name is None:
            self.name = ""
        else:
            self.name = name

        self.a = {}
        self.units = params['units']
        self.act = _init_act(params['acts'])
        self.reg = tf.contrib.layers.l2_regularizer(scale=params['reg'])

        return


    def fwd_prop(self, inputs):

        self.a[0] = tf.transpose(inputs)
        L = len(self.units)
        for l in range(1, L):

            # From TF docs, weights multiply inputs from the right
            self.a[l] = tf.layers.dense(inputs=self.a[l - 1],
                                        units=self.units[l],
                                        activation=self.act[l],
                                        use_bias=True,
                                        kernel_regularizer=self.reg,
                                        trainable=True,
                                        name="%s-L%d" % (self.name, l),
                                        reuse=tf.AUTO_REUSE)

        y_hat = tf.transpose(self.a[L - 1])

        return y_hat


class Interaction:
    """
    A simple implementation of the Interaction block.
    """

    def __init__(self, params, name=None):

        if name is None:
            self.name = ""
        else:
            self.name = name

        # Reshape parameters
        self.F = params['F']
        self.nat = params['nat']

        # Convolution Block position
        self.Cb = params['Cb']

        self.a = {}
        self.units = params['units']
        self.act = _init_act(params['acts'])
        self.reg = tf.contrib.layers.l2_regularizer(scale=params['reg'])

        return


    def fwd_prop(self, k, h, Cb=None, name=None):

        if Cb is None:
            Cb = self.Cb

        F = self.F
        nat = self.nat
        sz = tf.size(k)
        nmol = sz / (F * nat)

        self.a[0] = tf.transpose(k)
        L = len(self.units)

        for l in range(1, L):

            # From TF docs, weights multiply inputs from the right
            self.a[l] = tf.layers.dense(inputs=self.a[l - 1],
                                        units=self.units[l],
                                        activation=self.act[l],
                                        use_bias=True,
                                        kernel_regularizer=self.reg,
                                        trainable=True,
                                        name="%s-L%d" % (self.name, l),
                                        reuse=tf.AUTO_REUSE)

            # C-block
            if l == Cb:

                with tf.variable_scope("%s-L%d-Conv" % (self.name, l)):

                    # Reshape matrices to have suitable dimensions
                    # k matrix
                    with tf.variable_scope("k-matrix-reshape"):
                        ai = tf.transpose(self.a[l])
                        ai = tf.reshape(ai, [F, nmol, nat])
                        ai = tf.transpose(ai, [0, 2, 1])
                        ai = tf.expand_dims(ai, 2)
                        ai = u.tf_repeat(ai, [1, 1, nat, 1])

                    # h matrix
                    with tf.variable_scope("h-matrix-reshape"):
                        h = tf.reshape(h, [F, nmol, nat, nat])
                        h = tf.transpose(h, [0, 2, 3, 1])

                    # Element-wise product
                    with tf.variable_scope("Convolution"):
                        prod = tf.multiply(ai, h)
                        prod = tf.reduce_sum(prod, axis=1)
                        prod = tf.transpose(prod, [0, 2, 1])
                        prod = tf.reshape(prod, [F, nat * nmol])
                        prod = tf.transpose(prod)

                self.a[l] = prod

        return tf.transpose(self.a[L - 1] + self.a[0])


if __name__ == '__main__':
    pass
