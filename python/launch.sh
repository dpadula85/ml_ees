#!/bin/bash -l

data_dir="./data_dir"
tr_sz=40
val_sz=5
ts_sz=5
nat=`cat ${data_dir}/nat_${tr_sz}.txt`

# Example runs
./my_estimator.py --nat ${nat} \
                  --data_dir ${data_dir} \
                  --model_dir ./train_${tr_sz} \
                  --nsteps 50000 \
                  --savefrq 1000 \
                  --decay 1000 \
                  --trainfile train_${tr_sz}.tfrecords \
                  --testfile validation_${val_sz}.tfrecords

# ./my_eval.py --model_dir ./train_${tr_sz} \
#              --testfile test_${ts_sz}.tfrecords \
#              --out test_${ts_sz}.dat
