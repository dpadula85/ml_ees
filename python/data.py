#!/usr/bin/env python

import util as u
import pandas as pd
import tensorflow as tf


feature_names = ["index", "numbers", "gaussian_distances"]
label_names = ['']


def decode(serialized_example, nats):

    nDs = nats**2
    features = tf.parse_single_example(serialized_example,
                          features = {
                            'index': tf.FixedLenFeature([1], tf.float32),
                            'numbers': tf.FixedLenFeature([nats], tf.float32),
                            'mask': tf.FixedLenFeature([nDs], tf.float32),
                            'distances': tf.FixedLenFeature([nDs], tf.float32),
                            'energy': tf.FixedLenFeature([1], tf.float32)
                            })

    index = tf.cast(features['index'], tf.int32)
    numbers = tf.cast(features['numbers'], tf.int32)
    mask = tf.cast(features['mask'], tf.float32)
    distances = tf.cast(features['distances'], tf.float32)
    energy = tf.cast(features['energy'], tf.float32)

    return index, numbers, mask, distances, energy



def input_fn(file_path, nats, perform_shuffle=True, batch_size=50,
             repeat_count=None):

    fun = lambda x: decode(x, nats)
    dataset = tf.data.TFRecordDataset(file_path).map(fun)

    if perform_shuffle:
        dataset = dataset.shuffle(buffer_size=10000)

    dataset = dataset.repeat(repeat_count)
    dataset = dataset.batch(batch_size)
    iterator = dataset.make_one_shot_iterator()

    idx_batch, Z_batch, mask_batch, D_batch, y_batch = iterator.get_next()
    features, y_batch = preprocessing_fn(idx_batch, Z_batch, mask_batch,
                                         D_batch, y_batch)

    feat = dict(zip(feature_names, features))

    return feat, y_batch


def preprocessing_fn(idx_batch, Z_batch, mask_batch, D_batch, y_batch):

    idx_batch = tf.transpose(idx_batch)
    Z_batch = tf.reshape(Z_batch, [-1])
    mask_batch = tf.reshape(mask_batch, [1,-1])
    e_batch = tf.multiply(gauss_expand(D_batch), mask_batch)
    y_batch = tf.transpose(y_batch)

    return [ idx_batch, Z_batch, e_batch ], y_batch


def gauss_expand(D, G=300, Rmax=15.10, gamma=2.5):

    # Convert Rmax, gamma to au
    Rmax /= u.au2ang
    gamma *= u.au2ang**2
    mu = Rmax / (G - 1)

    with tf.variable_scope("Gaussian_Expansion"):
        gamma = tf.constant(gamma, dtype=tf.float32)
        mus = tf.range(0, Rmax + mu, mu)
        mus = tf.reshape(mus, [-1, 1])
        D = tf.reshape(D, [1, -1])
        e_k = tf.exp(-gamma * (D - mus)**2)

    return e_k


def _get_feats_data(file_name):

    df = pd.read_excel(file_name, sheetname="Current_database", skiprows=1,
                       usecols=feature_names)
    data = df.dropna().as_matrix()

    return data.astype('float32', copy=False)


def _get_labels_data(file_name):

    df = pd.read_excel(file_name, sheetname="Current_database", skiprows=1,
                       usecols=label_names)
    data = df.dropna().as_matrix()

    return data.astype('float32', copy=False)


def pandas_input_fn(file_path, perform_shuffle=False, batch_size=50,
                    repeat_count=None):

    dataset = tf.data.Dataset.from_tensor_slices(file_path)
    features = dataset.flat_map(lambda file_name:
                                tf.data.Dataset.from_tensor_slices(
                                tf.py_func(_get_feats_data,
                                           [file_name], tf.float32)
                                ))

    dataset = tf.data.Dataset.from_tensor_slices(file_path)
    labels = dataset.flat_map(lambda file_name:
                              tf.data.Dataset.from_tensor_slices(
                              tf.py_func(_get_labels_data,
                                         [file_name], tf.float32)
                              ))

    dataset = tf.data.Dataset.zip((features, labels))

    if perform_shuffle:
        dataset = dataset.shuffle(buffer_size=10000)

    dataset = dataset.repeat(repeat_count)
    dataset = dataset.batch(batch_size)
    iterator = dataset.make_one_shot_iterator()

    feats, labels = iterator.get_next()
    feats = tf.transpose(feats)
    labels = tf.transpose(labels)

    return feats, labels


if __name__ == '__main__':

    import sys
    import numpy as np

    next_batch = input_fn("test.tfrecords", 14, repeat_count=1)
    # next_batch = input_fn("train.tfrecords", 14)
    init_op = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init_op)
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        feats, y = sess.run(next_batch)

    idx = feats["index"]
    res = np.c_[ idx.T, y.T ]
    res = res[res[:,0].argsort()]
    np.savetxt(sys.stdout, res, fmt="%10d %14.6f")
