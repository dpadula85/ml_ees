#!/usr/bin/env python

import os
import sys
import util as u
import numpy as np
import argparse as arg
import tensorflow as tf


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(formatter_class=arg.ArgumentDefaultsHelpFormatter)

    inp = parser.add_argument_group("Input Data")

    inp.add_argument('-l', '--list', default=None,
                     type=str, dest='List', required=False,
                     help='''File with the list of xyz files to parse.''')

    inp.add_argument('-f', '--frac', default=0.8,
                     type=float, dest='Frac', required=False,
                     help='''Training set fraction.''')

    inp.add_argument('-p', '--path', default=None,
                     dest='Path', required=False,
                     help='''Path from which input data should be read.''')

    out = parser.add_argument_group("Output Data")
    out.add_argument('-o', '--out', default='data_dir',
                     type=str, dest='Out', required=False,
                     help='''Name of the folder to save data.''')

    args = parser.parse_args()
    Opts = vars(args)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()

    return Opts


def parse_mols(molslist, path=None):

    if path is None:
        path = os.getcwd()

    nmax = 0
    with open(molslist) as f:
        for i, mol in enumerate(f, start=1):
            mol = mol.strip()
            mol = os.path.join(path, mol)
            # Zi, structi = u.read_xyz(mol, au=True)
            Zi, structi, enei, labeli = u.read_xyz_GDB9(mol, au=True)
            n = Zi.shape[0]

            if i > 1:
                if n < nmax:
                    diff = nmax - n
                    Zi = np.vstack((Zi, np.zeros((diff, 1))))
                    structi = np.vstack((structi, np.zeros((diff, 3))))

                elif n > nmax:
                    diff = n - nmax
                    Z = np.vstack((Z, np.zeros((diff, Z.shape[1]))))
                    struct = np.vstack((struct, np.zeros((diff,
                                        struct.shape[1]))))

            try:
                Z = np.hstack((Z, Zi))
                struct = np.hstack((struct, structi))
                ene = np.hstack((ene, enei))
                label = np.hstack((label, labeli))

            except NameError:
                Z = Zi
                struct = structi
                ene = enei
                label = labeli
            
            nmax = Z.shape[0]

    # Compute Distance Matrices
    struct = np.swapaxes(struct.reshape(nmax,-1,3), 1, 0)
    for i, s in enumerate(struct):

        nats = len(np.where(Z[:,i] != 0)[0])
        Di = u.dist_mat(s, n=nats)
        try:
            D = np.hstack((D, Di))

        except NameError:
            D = Di

    return Z, D, ene.reshape(1,-1), label.reshape(1,-1), nmax


def _floats_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def write_TFRecords(filename, idx, Z, D, y):

    writer = tf.python_io.TFRecordWriter(filename)
    
    for i in range(y.shape[1]):
    
        # The reason to store image sizes was demonstrated
        # in the previous example -- we have to know sizes
        # of images to later read raw serialized string,
        # convert to 1d array and convert to respective
        # shape that image used to have.
        idxi = idx[:,i]
        Zi = Z[:,i]
        mi = (Zi > 0).astype(int)
        mi = np.outer(mi, mi).flatten()
        Di = D[:,i]
        yi = y[:,i]
   
        # Put in the original images into array
        # Just for future check for correctness
        example = tf.train.Example(features=tf.train.Features(feature={
            'index': _floats_feature(idxi),
            'numbers': _floats_feature(Zi),
            'distances': _floats_feature(Di),
            'mask': _floats_feature(mi),
            'energy': _floats_feature(yi)}))
    
        writer.write(example.SerializeToString())
    
    writer.close()

    return


if __name__ == '__main__':

    Opts = options()

    if Opts['Path'] is None:
        Opts['Path'] = os.getcwd()
    else:
        Opts['Path'] = os.path.join(os.getcwd(), Opts['Path'])

    if Opts['List']:
        molslist = Opts['List']
        Z, D, y, labels, nat = parse_mols(molslist, path=Opts['Path'])

    # # Scale y to average 0 and and variance 1
    # scaler = StandardScaler()
    # scaler.fit(y.T)
    # yn = scaler.transform(y.T).T

    # Randomly shuffle molecules
    randomise = np.arange(Z.shape[-1])
    np.random.shuffle(randomise)
    Z_shuffled = Z[:,randomise]
    D_shuffled = D[:,randomise]
    y_shuffled = y[:,randomise]
    labels_shuffled = labels[:,randomise]

    # Split in Training, Validation, and Test set
    ntr = np.ceil(Z.shape[-1] * Opts['Frac']).astype(int)
    nval = (Z.shape[-1] - ntr) / 2
    nts = nval

    Ztr = Z_shuffled[:,:ntr]
    Zval = Z_shuffled[:,ntr:ntr+nval]
    Zts = Z_shuffled[:,ntr+nval:]

    Dtr = D_shuffled[:,:ntr]
    Dval = D_shuffled[:,ntr:ntr+nval]
    Dts = D_shuffled[:,ntr+nval:]

    ytr = y_shuffled[:,:ntr]
    yval = y_shuffled[:,ntr:ntr+nval]
    yts = y_shuffled[:,ntr+nval:]

    labelstr = labels_shuffled[:,:ntr]
    labelsval = labels_shuffled[:,ntr:ntr+nval]
    labelsts = labels_shuffled[:,ntr+nval:]

    try:
        os.makedirs(Opts['Out'])
    except OSError:
        pass

    tr_filename = os.path.join(Opts['Out'], 'train_%d.tfrecords' % ntr)
    val_filename = os.path.join(Opts['Out'], 'validation_%d.tfrecords' % nval)
    ts_filename = os.path.join(Opts['Out'], 'test_%d.tfrecords' % nts)
    natfile = os.path.join(Opts['Out'], 'nat_%d.txt' % ntr)
    np.savetxt(natfile, np.array([nat]), fmt="%d")

    write_TFRecords(tr_filename, labelstr, Ztr, Dtr, ytr)
    write_TFRecords(val_filename, labelsval, Zval, Dval, yval)
    write_TFRecords(ts_filename, labelsts, Zts, Dts, yts)
