#!/bin/bash -l

#SBATCH -p gpu
#SBATCH -c 1
#SBATCH --mem=50G
#SBATCH -t 72:00:00
#SBATCH -J MolNN_32000
#SBATCH --output MolNN_32000.log

module load apps/anaconda/2.5.0/bin
source activate my-rdkit-env

# export CUDA_VISIBLE_DEVICES="0,1,2,3"
export CUDA_VISIBLE_DEVICES="1"

data_dir="./data_dir"
tr_sz=32000
val_sz=4000
nat=`cat ${data_dir}/nat_${tr_sz}.txt`

./my_estimator.py --nat ${nat} \
                  --data_dir ${data_dir} \
                  --model_dir ./train_${tr_sz} \
                  --nsteps 10000000 \
                  --savefrq 5000 \
                  --decay 100000 \
                  --trainfile train_${tr_sz}.tfrecords \
                  --testfile validation_${val_sz}.tfrecords
